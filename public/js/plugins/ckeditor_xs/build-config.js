﻿/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/bda6e7e5839a2facf051c1efb3c90fc0
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/bda6e7e5839a2facf051c1efb3c90fc0
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moono-lisa',
	preset: 'basic',
	ignore: [
		'.bender',
		'bender.js',
		'bender-err.log',
		'bender-out.log',
		'dev',
		'.DS_Store',
		'.editorconfig',
		'.gitattributes',
		'.gitignore',
		'gruntfile.js',
		'.idea',
		'.jscsrc',
		'.jshintignore',
		'.jshintrc',
		'less',
		'.mailmap',
		'node_modules',
		'package.json',
		'README.md',
		'tests'
	],
	plugins : {
		'basicstyles' : 1,
		'clipboard' : 1,
		'colorbutton' : 1,
		'colordialog' : 1,
		'filebrowser' : 1,
		'find' : 1,
		'font' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'htmlwriter' : 1,
		'image2' : 1,
		'justify' : 1,
		'link' : 1,
		'list' : 1,
		'liststyle' : 1,
		'magicline' : 1,
		'maximize' : 1,
		'pastecode' : 1,
		'pastefromword' : 1,
		'pastetext' : 1,
		'placeholder_select' : 1,
		'resize' : 1,
		'scayt' : 1,
		'selectall' : 1,
		'smiley' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'wysiwygarea' : 1,

	},
	languages : {
		'en' : 1,
		'es' : 1,
		'pt-br' : 1
	}
};