/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.placeholder_select = {
	
	    placeholders: ['FirstName', 'LastName', 'Company', 'MyWebsiteLink']
	
	};
	
	config.codemirror = {

    		// Set this to the theme you wish to use (codemirror themes)
    		theme: 'monokai'    
    	};
    
	
};
