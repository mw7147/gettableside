<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuImages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menuID')->unsigned()->index();
            $table->integer('menuDataID')->index()->comment('menuDataID');
            $table->integer('sortOrder')->nullable();            
            $table->integer('imageHeight')->nullable();            
            $table->integer('imageWidth')->nullable();            
            $table->string('imageSize', 25)->nullable();            
            $table->string('imageType')->nullable();
            $table->integer('fileSize')->nullable();
            $table->string('fileName')->nullable();
            $table->timestamps();
        });

        Schema::table('menuImages', function (Blueprint $table) {

            $table->foreign('menuID')
                ->references('id')->on('menu')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('menuImages', function (Blueprint $table) {
            $table->dropForeign(['menuID']);
        });

        Schema::dropIfExists('menuImages');
    }
}

