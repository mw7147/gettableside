<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->id();
            $table->integer('domainID')->index()->comment('domain ID');
            $table->integer('parentDomainID')->index()->comment('parent domain ID');
            $table->integer('locationTypeID')->nullable()->index()->comment('location type table ID');
            $table->integer('numSeats')->nullable()->comment('number of seats at table');
            $table->string('name')->nullable()->comment('name - on table button');
            $table->string('note')->nullable()->comment('table description - restaurant only');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
