<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverySessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliverySession', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index();
            $table->integer('ownerID')->nullable();
            $table->integer('userID')->nullable();
            $table->string('sessionID')->index();
            $table->string('delivery')->nullable();
            $table->timestamps();

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('deliverySession', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('deliverySession');
    }
}
