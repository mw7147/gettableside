<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuOptionsDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuOptionsData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('menuOptionsID')->unsigned()->index()->nullable();
            $table->string('optionName')->comment('do not allow double quotes in name');
            $table->string('printKitchen')->nullable();
            $table->string('printOrder')->nullable();
            $table->string('optionsDefault', 5)->nullable();
            $table->decimal('upCharge', 5, 2)->nullable();
            $table->timestamps();
        });

        Schema::table('menuOptionsData', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menuOptionsData', function (Blueprint $table) {
            $table->foreign('menuOptionsID')
                ->references('id')->on('menuOptions')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuOptionsData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::table('menuOptionsData', function (Blueprint $table) {
            $table->dropForeign(['menuOptionsID']);
        });


        Schema::dropIfExists('menuOptionsData');
    }
}
