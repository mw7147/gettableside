<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->increments('id')->comment('Domain ID Number');
            $table->integer('parentDomainID')->index()->nullable()->comment('parent domainID for multi-location');
            $table->boolean('useParentMenu')->nullable()->comment('use parent menu for multi-location');
            $table->string('name')->index()->comment('Domain Name');
            $table->integer('ownerID')->index()->nullable()->comment('User ID of Domain Owner');
            $table->string('subdomain', 100)->nullable();
            $table->string('httpHost', 100)->index()->comment('HTTP_Host Server Variable Value');
            $table->string('sitename', 100)->nullable();
            $table->string('active', 5)->index();
            $table->integer('type');
            $table->string('logoPublic')->nullable();
            $table->string('logoPrivate')->nullable();
            $table->string('backgroundPublic')->nullable();
            $table->string('backgroundPrivate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('domains');
    }
}
