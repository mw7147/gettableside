<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersData', function (Blueprint $table) {
 
        //    $table->increments('id');
            $table->increments('id');
            $table->integer('usersID')->unsigned()->unique()->index()->comment('Users ID field');
            $table->string('address1', '100')->nullable();
            $table->string('address2', '100')->nullable();
            $table->string('city', '100')->nullable();
            $table->string('state', '100')->nullable();
            $table->string('office', '100')->nullable();
            $table->string('companyName', '100')->nullable();
            $table->string('title', '100')->nullable();

            $table->timestamps();

            $table->foreign('usersID')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('usersData', function (Blueprint $table) {
            $table->dropForeign(['usersID']);
        });

        Schema::drop('usersData');
    }
}
