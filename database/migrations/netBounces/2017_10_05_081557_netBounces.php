<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class netBounces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bouncedb')->create('tgmBounces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serverID', 50)->nullable()->index()->comment('ORDERS_SERVER_ID env value - server ID');            
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('domain ID field');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('users ID field');
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('Contact ID field');
            $table->integer('messageID')->unsigned()->nullable()->index()->comment('message ID field');
            $table->string('mailtext', 10)->nullable()->index();
            $table->text('rawMessage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bouncedb')->dropIfExists('netBounces');
    }
}
