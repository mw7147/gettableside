<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registeredNumbers', function (Blueprint $table) {
            $table->string('cellNumber', 20)->index();
            $table->string('formattedNumber', 20)->nullable();
            $table->integer('carrierID');
            $table->string('carrierName', 75)->index()->nullable();
            $table->string('smsGateway', 75);
            $table->string('mmsGateway', 75);
            $table->decimal('cost', 8, 4)->nullable();
            $table->timestamps();

            $table->primary('cellNumber');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registeredNumbers');
    }
}
