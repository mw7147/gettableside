<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFoodRunnerTokenToDomainSiteConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->text('foodRunnerToken')->nullable()->after('deliveryMinOrder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->dropColumn('foodRunnerToken');
        });
    }
}
