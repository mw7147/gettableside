<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuspendOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suspendOperation', function (Blueprint $table) {
            $table->string('domainID', 20)->index()->comment('domainID to suspend operation');
            $table->integer('userID')->comment('userID that suspended operation');
            $table->timestamp('startTime')->comment('suspension start time');
            $table->timestamp('stopTime')->comment('suspension stop time');
            $table->integer('numMinutes')->comment('suspension number of minutes');
            $table->timestamps();

            $table->primary('domainID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suspendOperation');
    }
}
