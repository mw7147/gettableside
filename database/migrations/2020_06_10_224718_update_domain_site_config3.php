<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDomainSiteConfig3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->integer('cartClearTimer')->default(15)->after('pickupPrepMinutes')->comment('cart clear timer ');
            $table->integer('dineInLastOrderTimeOffset')->default(0)->after('cartClearTimer')->comment('dineIn offset time for last order ');
            $table->string('timezone')->default('America/New_York')->after('mapLink')->comment(' location timezone ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->dropColumn('cartClearTimer');
            $table->dropColumn('dineInLastOrderTimeOffset');
            $table->dropColumn('timezone');
        });
    }
}
