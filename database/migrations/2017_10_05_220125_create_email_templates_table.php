<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailTemplates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index();
            $table->integer('userID')->unsigned()->nullable()->index();
            $table->string('status', 5)->index();
            $table->string('name');
            $table->string('description');
            $table->string('subject');
            $table->mediumText('body')->nullable()->commment("html message body");
            $table->mediumText('textbody')->nullable()->comment("text only message body");
            $table->string('attachmentPublic')->nullable(); 
            $table->string('attachmentPrivate')->nullable(); 
            $table->timestamps();


            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('userID')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('emailTemplates', function (Blueprint $table) {

            $table->dropForeign(['domainID']);
            $table->dropForeign(['userID']);

        });

        Schema::drop('emailTemplates');
    }
}