<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripeCards', function (Blueprint $table) {
            $table->string('stripeCardID')->unique()->index()->comment('Stripe Customer Card ID');
            $table->string('stripeCustomerID')->index()->comment('Stripe Customer ID');
            $table->integer('domainID')->index()->comment('togomeals.biz domain ID');
            $table->integer('userID')->index()->nullable()->comment('togomeals.biz user ID');
            $table->integer('contactID')->index()->comment('togomeals.biz contact ID');
            $table->string('object', 50)->nullable();
            $table->string('address1')->nullable();
            $table->string('address1Check')->nullable();
            $table->string('address2')->nullable();
            $table->string('city', 100)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('zipCode', 25)->nullable();
            $table->string('zipCodeCheck', 25)->nullable();
            $table->string('brand', 100)->nullable();
            $table->string('brandCountry', 50)->nullable();
            $table->string('cvcCheck', 50)->nullable();
            $table->string('dynamicLast4', 10)->nullable();
            $table->string('expireMonth', 10)->nullable();
            $table->string('expireYear', 10)->nullable();
            $table->string('fingerprint')->nullable();
            $table->string('funding', 25)->nullable();
            $table->string('last4', 10)->nullable();
            $table->string('name')->nullable();
            $table->string('tokenizationMethod', 50)->nullable();
            $table->timestamps();

            $table->primary('stripeCardID');

        });

        Schema::table('stripeCards', function (Blueprint $table) {
            $table->foreign('stripeCustomerID')
                ->references('stripeID')->on('stripeCustomers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('stripeCards', function (Blueprint $table) {
            $table->dropForeign(['stripeCustomerID']);
        });

        Schema::dropIfExists('stripeCards');
    }
}
