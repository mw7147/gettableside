<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDomainPaymentConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domainPaymentConfig', function (Blueprint $table) {
            $table->string('togoMerchantID', 20)->nullable()->index()->after('stripeTransactionFee')->comment('card connect merchant ID');
            $table->string('togoACHMerchantID', 20)->nullable()->index()->after('togoMerchantID')->comment('card connect ach merchant ID');  

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainPaymentConfig', function (Blueprint $table) {
            $table->dropColumn('togoMerchantID');
            $table->dropColumn('togoACHMerchantID');
        });
    }

}
