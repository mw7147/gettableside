<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('address1', '100')->nullable()->after('customerMobile');
            $table->string('address2', '100')->nullable()->after('address1');
            $table->string('city', '100')->nullable()->index()->after('address2');
            $table->string('state', '100')->nullable()->index()->after('city');
            $table->string('zipCode', '25')->nullable()->index()->after('state');
            $table->string('timezone')->default('America/New_York')->after('orderRefundAmount')->comment(' location timezone ');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('zipCode');
            $table->dropColumn('timezone');
        });
    }

}
