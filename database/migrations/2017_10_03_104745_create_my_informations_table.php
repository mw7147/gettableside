<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('myInformation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('Domain ID field');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('User ID field');
            $table->string('active', 5)->nullable()->index();
            $table->string('fname', '100')->nullable();
            $table->string('lname', '100')->nullable();
            $table->string('company', '100')->nullable();
            $table->string('title', '100')->nullable();
            $table->string('address1', '100')->nullable();
            $table->string('address2', '100')->nullable();
            $table->string('city', '100')->nullable();
            $table->string('state', '100')->nullable();
            $table->string('zip', '100')->nullable();
            $table->string('website', '100')->nullable();
            $table->string('email', '100')->nullable();
            $table->string('office', '100')->nullable();
            $table->string('mobile', '50')->nullable();
            $table->string('unformattedMobile', '50')->nullable();
            $table->string('picturePublic')->nullable(); 
            $table->string('picturePrivate')->nullable(); 
            $table->timestamps();

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('userID')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('myInformation', function (Blueprint $table) {

            $table->dropForeign(['domainID']);
            $table->dropForeign(['userID']);

        });


        Schema::drop('myInformation');
    }
}