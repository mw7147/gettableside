<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainSiteConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domainSiteConfig', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->string('managerFname', 60)->nullable();
            $table->string('managerLname', 60)->nullable();
            $table->string('sendEmail', 100)->nullable();
            $table->string('sendText', 25)->nullable();
            $table->string('sendEmailUserName', 100)->nullable();
            $table->text('sendEmailPassword', 100)->nullable();
            $table->string('orderReceiveEmail', 100)->nullable();
            $table->integer('printReceipt')->nullable()->comment('print detailed customer receipt for customer - printers ID');
            $table->integer('numReceiptCopies')->nullable()->comment('number of receipts - 1, 2, etc.');
            $table->integer('lineReceiptHeight')->nullable()->comment('receipt line height');
            $table->integer('printOrder')->nullable()->comment('print order for kitchen and staff - no prices - printers ID');
            $table->integer('numOrderCopies')->nullable()->comment('number of Orders - 1, 2, etc.');
            $table->integer('lineOrderHeight')->nullable()->comment('Order line height');
            $table->string('hpEprint')->nullable();
            $table->decimal('taxRate', 4,2)->nullable();
            $table->string('locationName')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state', 10)->nullable();
            $table->string('zipCode', 15)->nullable();
            $table->string('locationPhone', 25)->nullable();
            $table->string('locationIPAddress', 50)->nullable();
            $table->string('locationOrder', 5)->nullable();
            $table->text('orderMessage')->nullable();
            $table->text('orderFooter')->nullable();
            $table->text('pickupOIP')->nullable();
            $table->text('deliverOIP')->nullable();
            $table->text('orderAlert')->nullable();
            $table->text('deliverOrderReady')->nullable();
            $table->text('orderReady')->nullable();
            $table->text('welcomeEmail')->nullable();
            $table->integer('globalDiscount')->default(0)->comment('global order percentage discount');
            $table->decimal('transactionFee',5,2)->default(0);
            $table->string('delivery', 5)->nullable();
            $table->decimal('deliveryCharge', 5,2)->default(0);
            $table->decimal('deliveryRadius', 5,2)->default(0);
            $table->string('deliveryMessage')->nullable();
            $table->decimal('deliveryMinOrder', 4, 2)->default(0);
            $table->decimal('latitude', 15,8)->default(0);
            $table->decimal('longitude', 15,8)->default(0);
            $table->string('googleMapKey')->nullable()->comment('Google Maps API Key');
            $table->text('mapLink')->nullable()->comment('Google Map Location Link Code');
            $table->timestamps();
        });

        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('domainSiteConfig');
    }
}
