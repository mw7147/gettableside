<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDomainSiteConfig2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->text('futureOrderMessage')->nullable()->after('deliveryMessage')->comment('future pickup message ');
            $table->text('futureDeliveryMessage')->nullable()->after('futureOrderMessage')->comment('future delivery message');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->dropColumn('futureOrderMessage');
            $table->dropColumn('futureDeliveryMessage');
        });
    }
}
