<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('ordersDomainID')->nullable()->after('domainID')->comment('order domain ID');
            $table->text('orderItemsHTML')->nullable()->after('emailData')->comment('order items html');
            $table->boolean('mobile')->nullable()->after('paymentType')->comment('mobile->1 or desktop->0');
            $table->decimal('deliveryTaxAmount', 6, 2)->default(0)->after('deliveryCharge')->comment('delivery tax charge');
            $table->decimal('globalDiscountPercent', 6, 2)->default(0)->after('deliveryTaxAmount')->comment('global discount percent');
            $table->decimal('globalDiscountAmount', 7, 2)->default(0)->after('globalDiscountPercent')->comment('global discount amount');
            $table->decimal('netExtendedPrice', 7, 2)->default(0)->after('globalDiscountAmount')->comment('net extended price');
            $table->dateTime('orderPrepTime')->nullable()->after('orderRefundAmount')->comment('Order Prepare Time');
            $table->dateTime('orderReadyTime')->nullable()->after('orderPrepTime')->comment('Order Ready Time');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('orderDomainID');
            $table->dropColumn('orderItemsHTML');
            $table->dropColumn('mobile');
            $table->dropColumn('deliveryTaxAmount');
            $table->dropColumn('globalDiscountPercent');
            $table->dropColumn('globalDiscountAmount');
            $table->dropColumn('netExtendedPrice');
            $table->dropColumn('orderPrepTime');
            $table->dropColumn('orderReadyTime');

        });
    }
}
