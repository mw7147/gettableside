<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuspendOperationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suspendOperationHistory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domainID', 20)->index()->comment('domainID to suspend operation');
            $table->integer('userID')->comment('userID that suspended operation');
            $table->timestamp('startTime')->comment('suspension start time');
            $table->timestamp('stopTime')->comment('suspension stop time');
            $table->integer('numMinutes')->comment('suspension number of minutes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suspendOperationHistory');
    }
}
