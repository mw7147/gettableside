<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateccBINS extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccBIN', function (Blueprint $table) {
            $table->integer('domainID')->index()->nullable()->after('id')->comment('domain ID');
            $table->integer('userID')->index()->nullable()->after('domainID')->comment('user ID');
            $table->integer('contactID')->index()->nullable()->after('userID')->comment('contact ID');
            $table->string('token')->index()->nullable()->after('contactID')->comment('token');
            $table->integer('expireMonth')->nullable()->after('token')->comment('expire month');
            $table->integer('expireYear')->nullable()->after('expireMonth')->comment('expire year');
            $table->decimal('lastAmount', 7, 2)->default(0)->after('binHi')->comment('amount of last order ');
            $table->string('lastDate')->nullable()->after('lastAmount')->comment(' last use date ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccBIN', function (Blueprint $table) {
            $table->dropColumn('domainID');
            $table->dropColumn('userID');
            $table->dropColumn('contactID');
            $table->dropColumn('token');
            $table->dropColumn('expireMonth');
            $table->dropColumn('expireYear');
            $table->dropColumn('lastAmount');
            $table->dropColumn('lastDate');
        });
    }
}
