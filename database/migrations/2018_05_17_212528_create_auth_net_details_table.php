<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthNetDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authNetDetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index();
            $table->integer('ownerID')->unsigned()->nullable()->index();
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('contact ID of customer');
            $table->text('sessionID')->nullable();
            $table->integer('orderID')->unsigned()->nullable()->index()->comment('order ID from orders');
            $table->string('transactionID')->nullable()->index()->comment('transaction nonce');
            $table->integer('ccLastFour')->nullable();
            $table->string('ccType', 50)->nullable();
            $table->string('expiry', 50)->nullable();
            $table->string('responseCode')->nullable()->comment('authNet Response Code');
            $table->string('messageCode')->nullable()->comment('authNet Message Code');
            $table->string('authCode')->nullable()->comment('authNet Authorization Code');
            $table->decimal('amount', 7, 2)->comment('Charge Amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authNetDetail');
    }
}
