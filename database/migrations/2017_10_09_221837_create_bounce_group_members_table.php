<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBounceGroupMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounceGroupMembers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groupID')->unsigned()->index()->comment('GroupNames ID field');            
            $table->integer('contactID')->unsigned()->index()->comment('Contacts ID field');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field'); 
            $table->integer('bounceID')->unsigned()->index()->comment('bounces ID field');                 
            $table->timestamps();


            $table->foreign('groupID')
                ->references('id')->on('groupNames')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('bounceID')
                ->references('id')->on('bounces')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('bounceGroupMembers', function (Blueprint $table) {

            $table->dropForeign(['groupID']);
            $table->dropForeign(['domainID']);

        });

        Schema::dropIfExists('bounceGroupMembers');
    }
}