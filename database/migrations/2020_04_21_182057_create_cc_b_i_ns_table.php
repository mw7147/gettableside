<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcBINsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccBIN', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last4', 10)->nullable()->comment('card last 4');
            $table->string('country', 50)->nullable()->comment('card country');
            $table->string('product', 10)->nullable()->comment('card brand code');
            $table->string('brand', 25)->index()->nullable()->comment('card brand');
            $table->string('bin', 25)->nullable()->comment('card bin');
            $table->string('purchase', 10)->nullable()->comment('card purchased');
            $table->string('prepaid', 10)->nullable()->comment('card prepaid');
            $table->string('issuer', 150)->index()->nullable()->comment('card issuer');
            $table->string('cardusestring', 150)->index()->nullable()->comment('card type string');
            $table->string('funding', 25)->index()->nullable()->comment('card funding');
            $table->string('gsa', 10)->index()->nullable()->comment('gsa card');
            $table->string('corporate', 10)->index()->nullable()->comment('corporate card');
            $table->string('fsa', 10)->index()->nullable()->comment('fsa card');
            $table->string('subtype', 100)->nullable()->comment('card subtype');
            $table->string('binlo', 15)->nullable()->comment('binlo');
            $table->string('binhi', 15)->nullable()->comment('binlo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccBIN');
    }
}
