<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linkData', function (Blueprint $table) {
            $table->increments('id');
            $table->string('linkHash', '150')->index()->comment('Link hash in url');
            $table->string('linkAddress')->comment('redirect link');
            $table->integer('domainID')->unsigned()->index();
            $table->integer('userID')->unsigned()->index();
            $table->integer('contactID')->unsigned()->index();
            $table->integer('mailID')->unsigned()->index();
            $table->integer('templateID')->unsigned()->index();
            $table->string('ipAddress', '100')->nullable();
            $table->timestamps();

            $table->foreign('userID')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('linkData', function (Blueprint $table) {

            $table->dropForeign(['userID']);
            $table->dropForeign(['domainID']);

        });

        Schema::dropIfExists('linkData');
    }
}