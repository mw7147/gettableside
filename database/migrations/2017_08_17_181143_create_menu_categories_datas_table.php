<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuCategoriesDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuCategoriesData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('menuCategoriesID')->unsigned()->index()->nullable();
            $table->boolean('status')->nullable()->comment('visible/invisible');
            $table->integer('sortOrder')->nullable();
            $table->string('categoryName');
            $table->text('categoryDescription');
            $table->string('fileName', 100)->nullable();
            $table->integer('fileSize')->nullable();
            $table->integer('imageHeight')->nullable();            
            $table->integer('imageWidth')->nullable();            
            $table->string('imageType', 50)->nullable();            
            $table->timestamps();
        });

        Schema::table('menuCategoriesData', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menuCategoriesData', function (Blueprint $table) {
            $table->foreign('menuCategoriesID')
                ->references('id')->on('menuCategories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuCategoriesData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::table('menuCategoriesData', function (Blueprint $table) {
            $table->dropForeign(['menuCategoriesID']);
        });

        Schema::dropIfExists('menuCategoriesData');
    }
}
