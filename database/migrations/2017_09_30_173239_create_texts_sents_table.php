<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextsSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('textsSent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('Domain ID field');
            $table->integer('parentDomainID')->unsigned()->nullable()->index()->comment('parent domain ID field - Customers');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('User ID field - Customer User Data');
            $table->integer('groupID')->unsigned()->nullable()->index()->comment('groupNames ID field');
            $table->integer('templateID')->unsigned()->nullable()->index()->comment('textTemplate ID field');
            $table->string('messageType', '25')->index();
            $table->string('messageBody');
            $table->string('pixFile')->nullable();
            $table->string('sendIP')->nullable();
            $table->integer('totalQueue')->comment('total good messages queued');
            $table->integer('totalBad')->nullable()->comment('total contacts with no phone number not queued');
            $table->integer('totalSent')->nullable()->comment('total messages sent');
            $table->integer('totalSentFail')->nullable()->comment('total messages failed sending');
            $table->dateTime('sendStart')->nullable()->comment('text start time');
            $table->dateTime('sendStop')->nullable()->comment('text stop time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('textsSent');
    }
}
