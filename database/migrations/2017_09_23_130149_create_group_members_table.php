<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groupMembers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groupID')->unsigned()->index()->comment('GroupNames ID field');            
            $table->integer('contactID')->unsigned()->index()->comment('Contacts ID field');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');          
            $table->timestamps();


            $table->foreign('groupID')
                ->references('id')->on('groupNames')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('contactID')
                ->references('id')->on('contacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('groupMembers', function (Blueprint $table) {

            $table->dropForeign(['groupID']);
            $table->dropForeign(['contactID']);
            $table->dropForeign(['domainID']);

        });

        Schema::dropIfExists('groupMembers');
    }
}