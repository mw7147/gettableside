<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('ownerID')->index()->comment('Domain ownerID field');
            $table->integer('contactID')->unsigned()->index();
            $table->integer('userID')->index()->nullable()->comment('users id field');
            $table->string('orderType', '50')->nullable()->comment('order type - delivery / pickup /restaurant'); 
            $table->integer('waiterID')->nullable()->comment('waiter ID - in restaurant only');
            $table->integer('tableID')->nullable()->comment('table ID - in restaurant only');
            $table->integer('deliveryID')->index()->nullable()->comment('delivery id field - driver, tips, etc.');
            $table->string('sessionID')->unique()->index();
            $table->string('fname', '100');
            $table->string('lname', '100');
            $table->string('unformattedMobile', 20)->nullable()->comment('number for order text');
            $table->string('customerEmailAddress', 75);
            $table->string('customerMobile', 25);
            $table->decimal('subTotal', 6,2);
            $table->decimal('discountedSubTotal', 6,2)->nullable();
            $table->decimal('discountAmount', 6,2)->nullable();
            $table->integer('discountPercent')->nullable();
            $table->decimal('tax', 6,2);
            $table->decimal('tip', 6,2);
            $table->decimal('deliveryCharge', 6,2);
            $table->decimal('transactionFee', 5,2)->nullable();
            $table->decimal('total', 6,2);
            $table->integer('ccType')->nullable()->comment('credit card processer from ccType');
            $table->string('ccTypeTransactionID')->nullable()->comment('credit card processer transaction ID');
            $table->string('ccTypeCustomerID')->nullable()->comment('credit card processer customer ID');
            $table->string('ccTypeCardID')->nullable()->comment('credit card processer card ID');
            $table->string('ccTypeFunding')->nullable()->comment('credit card funding type - credit, debit, prepaid');
            $table->string('ccTypeLast4')->nullable()->comment('credit card last 4 digits');
            $table->decimal('ccTypeTransactionFee', 6, 2)->nullable()->comment('credit card processer transaction fee');
            $table->string('paymentType', '50')->nullable()->comment('type of payment');
            $table->string('userAgent')->index();
            $table->decimal('deliveryLatitude', 11,8)->nullable()->comment('Delivery Address Latitude');
            $table->decimal('deliveryLongitude', 11,8)->nullable()->comment('Delivery Address Longitude');
            $table->integer('deliveryFeet')->nullable()->comment('Delivery Address Feet From Location');
            $table->string('orderIP', 50)->nullable();
            $table->boolean('orderInProcess')->comment('Order in Process = 1 / Order Completed =0');
            $table->string('receiptLetterPath')->nullable()->comment('receipt letter pdf path');
            $table->string('receiptThermalPath')->nullable()->comment('receipt thermal pdf path');
            $table->string('orderLetterPath')->nullable()->comment('order letter pdf path');
            $table->string('orderThermalPath')->nullable()->comment('order thermal pdf path');
            $table->string('restaurantEmailAddress', 75);
            $table->text('emailData')->nullable();
            $table->boolean('orderRefunded')->comment('Refunded = 1 / not refunded = 0');
            $table->string('orderRefundID')->nullable()->comment('order refund ID');
            $table->decimal('orderRefundAmount', 6, 2)->nullable()->comment('order refund amount');            
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->index(['created_at']);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('orders');
    }
}
