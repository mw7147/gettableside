<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsSentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailsSentDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('domain ID field');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('user ID field');
            $table->integer('messageID')->unsigned()->nullable()->index()->comment('message ID field');
            $table->integer('groupID')->unsigned()->nullable()->index()->comment('groupNames ID field');
            $table->integer('templateID')->unsigned()->nullable()->index()->comment('textTemplate ID field');
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('contact ID field');
            $table->string('messageType', 25)->nullable()->index();
            $table->string('emailAddress')->index()->comment('recipients email address');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('company')->nullable();
            $table->string('subject')->nullable();
            $table->text('emailBody')->nullable();
            $table->text('textBody')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emailsSentDetails');
    }
}