<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardConnectDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cardConnectDetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->index()->comment('domain ID');
            $table->string('merchid', 50)->index()->comment('merchant ID');
            $table->integer('tgmOrderID')->nullable()->index()->comment('togomeals orders table ID');
            $table->string('ccOrderid', 30)->index()->comment('tgm supplied card connect orderid');
            $table->string('sessionID')->index()->comment('session ID');
            $table->string('type', 20)->comment('http type');
            $table->string('endpoint', 100)->comment('card connect api endpoint');
            $table->string('retref', 30)->index()->comment('CardConnect retrieval reference number from authorization response');
            $table->string('respstat', 10)->index()->comment('card connect response status');
            $table->string('resptext')->comment('Text description of response');
            $table->string('respcode', 10)->comment('response code from processor');
            $table->string('commcard', 5)->nullable()->comment('Commercial Card Flag');
            $table->decimal('amount', 7, 2)->comment("amount of charge/refund");
            $table->string('avsresp', 10)->nullable()->index()->comment('AVS zip code verification response code');
            $table->string('cvvresp', 10)->nullable()->index()->comment('CVV card verification value verification response code');
            $table->string('authcode', 10)->nullable()->comment('Authorization Code from the Issuer');
            $table->string('account', 30)->nullable()->comment('Account Number');
            $table->string('expiry', 10)->comment('Expiry Date');
            $table->string('token', 30)->comment('Token Number');
            $table->string('respproc', 5)->nullable()->comment('Abbreviation that represents the platform and the processor for the transaction');
            $table->text('rawResponse')->comment('card connect json response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cardConnectDetail');
    }
}
