<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->string('menuName');
            $table->text('menuHeader')->nullable()->comment('html header for menu');
            $table->string('menuDescription');
            $table->string('active', 5);            
            $table->string('defaultMenu', 5);
            $table->integer('menuCategoriesID');
            $table->string('startSunday')->nullable();
            $table->string('stopSunday')->nullable();
            $table->string('startMonday')->nullable();
            $table->string('stopMonday')->nullable();
            $table->string('startTuesday')->nullable();
            $table->string('stopTuesday')->nullable();
            $table->string('startWednesday')->nullable();
            $table->string('stopWednesday')->nullable();
            $table->string('startThursday')->nullable();
            $table->string('stopThursday')->nullable();
            $table->string('startFriday')->nullable();
            $table->string('stopFriday')->nullable();
            $table->string('startSaturday')->nullable();
            $table->string('stopSaturday')->nullable();
            $table->timestamps();
        });

        Schema::table('menu', function (Blueprint $table) {

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('menu', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('menu');
    }
}
