<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fileTypes', function (Blueprint $table) {
            $table->string('extension', 25)->index();
            $table->string('mimeType', 100)->index();
            $table->string('description')->nullable();
            $table->integer('download');
            $table->string('fileType')->nullable();
            $table->string('fileClass')->nullable();
            $table->timestamps();

            $table->primary('mimeType');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fileTypes');
    }
}
