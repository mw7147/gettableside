<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siteStyles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->unique()->index()->comment('Domain ID field');
            $table->string('backgroundColor', 10)->nullable();
            $table->string('fontColor', 10)->nullable();
            $table->string('bannerButtonBackgroundColor', 10)->nullable();
            $table->string('bannerButtonBorderColor', 10)->nullable();
            $table->string('bannerButtonColor', 10)->nullable();
            $table->string('bannerButtonColorHover', 10)->nullable();
            $table->string('completeOrderBackgroundColor', 10)->nullable();
            $table->string('completeOrderBorderColor', 10)->nullable();
            $table->string('completeOrderColor', 10)->nullable();
            $table->string('completeOrderColorHover', 10)->nullable();
            $table->string('bannerHomeMinHeight', 10)->nullable();
            $table->string('locationAddressFontColor', 10)->nullable();
            $table->integer('locationAddressTopMargin')->nullable();
            $table->string('pixBannerBackgroundColor', 10)->nullable();
            $table->string('pixBannerOpacity', 10)->nullable();
            $table->string('pixBannerPadding', 10)->nullable();
            $table->string('pixBannerWidth', 10)->nullable();
            $table->string('pixBannerColor', 10)->nullable();
            $table->string('navBarToggleBackgroundColor', 10)->nullable();
            $table->string('navBarToggleBorderColor', 10)->nullable();
            $table->string('navBarLogoMaxHeight', 10)->nullable();
            $table->string('navBarLogoMaxPadding', 10)->nullable();
            $table->string('navBarLogoMaxMarginLeft', 10)->nullable();
            $table->string('navBarLogoMaxMarginTop', 10)->nullable();
            $table->string('navBarLIColor', 10)->nullable();
            $table->string('navBarLIHoverColor', 10)->nullable();
            $table->string('calloutBannerBackgroundColor', 10)->nullable();
            $table->string('calloutBannerBorderColor', 10)->nullable();
            $table->string('calloutBannerColor', 10)->nullable();
            $table->string('calloutBannerHeight', 10)->nullable();
            $table->string('footerBackgroundColor', 10)->nullable();
            $table->string('footerColor', 10)->nullable();
            $table->string('footerLinkColor', 10)->nullable();
            $table->string('footerLinkHoverColor', 10)->nullable();
            $table->string('servicesMinHeight', 10)->nullable();
            $table->timestamps();

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siteStyles', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('siteStyles');
    }
}
