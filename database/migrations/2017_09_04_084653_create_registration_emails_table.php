<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrationEmails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->index();
            $table->integer('userID')->index();
            $table->integer('contactID')->index();
            $table->string('email', 75)->index();
            $table->text('html');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrationEmails');
    }
}
