<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodRunnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodRunner', function (Blueprint $table) {
            $table->id();
            $table->integer('domainID')->index()->comment('domainID');
            $table->integer('foodRunnerID')->index()->comment('foodRunner system id');
            $table->integer('displayOrderID')->index()->comment('Order ID');
            $table->dateTimeTz('estimatedReady')->index()->comment( "estimated order ready time");
            $table->string('pickupRestaurantName')->comment("pickup restaurant name");
            $table->string('pickupAddress')->comment("pickup address");
            $table->string('deliverName')->comment("delivery  name");
            $table->string('deliverAddress')->comment("delivery address");
            $table->integer('statusID')->index()->comment('foodRunner status id');
            $table->integer('driverID')->index()->nullable()->comment('driver id');
            $table->string('driverName')->nullable()->comment('driver name');
            $table->dateTimeTz('pickupTime')->nullable()->comment('pickup time by driver');
            $table->dateTimeTz('deliveryTime')->nullable()->comment('delivery time by driver');
            $table->decimal('duration', 6, 2)->nullable()->comment('delivery duration - decimal');
            $table->decimal('distance', 8, 2)->nullable()->comment('delivery distance - feet');
            $table->string('timezone')->comment('delivery timezone');
            $table->dateTimeTz('createdAtUTC')->comment('UTC time created');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodRunner');
    }
}
