<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCardConnectDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cardConnectDetail', function (Blueprint $table) {
            $table->string('ipAddress', '100')->nullable()->after('domainID');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cardConnectDetail', function (Blueprint $table) {
            $table->dropColumn('ipAddress');
        });
    }

}
