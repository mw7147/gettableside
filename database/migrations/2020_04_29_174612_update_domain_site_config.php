<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDomainSiteConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->boolean('orderAhead')->default(1)->after('transactionFee')->comment('order ahead 1/0 ');
            $table->integer('deliveryRecommendedTipPercent')->default(20)->after('orderAhead')->comment('delivery recommended tip percent');
            $table->boolean('deliveryRequireRecommendedTip')->default(0)->after('deliveryRecommendedTipPercent')->comment('require delivery tip');
            $table->integer('pickupRecommendedTipPercent')->default(20)->after('deliveryRequireRecommendedTip')->comment('pickup recommended tip percent');
            $table->boolean('pickupRequireRecommendedTip')->default(0)->after('pickupRecommendedTipPercent')->comment('require pickup tip');      
            $table->boolean('taxDelivery')->default(0)->after('pickupRequireRecommendedTip')->comment('tax delivery service');
            $table->decimal('deliveryTaxRate', 5,3)->default(0)->after('taxDelivery')->comment('tax rate for delivery service');   
            $table->integer('deliveryPrepMinutes')->default(45)->after('locationOrder')->comment('delivery order prep time in minutes');            
            $table->integer('pickupPrepMinutes')->default(30)->after('deliveryPrepMinutes')->comment('pickup order prep time in minutes');            
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainSiteConfig', function (Blueprint $table) {
            $table->dropColumn('orderAhead');
            $table->dropColumn('deliveryRecommendedTipPercent');
            $table->dropColumn('deliveryRequireRecommendedTip');
            $table->dropColumn('pickupRecommendedTipPercent');
            $table->dropColumn('pickupRequireRecommendedTip'); 
            $table->dropColumn('deliveryPrepMinutes');
            $table->dropColumn('pickupPrepMinutes');
            $table->dropColumn('taxDelivery');
            $table->dropColumn('deliveryTaxRate');
        });
    }
}
