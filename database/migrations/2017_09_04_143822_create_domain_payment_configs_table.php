<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainPaymentConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domainPaymentConfig', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('ccType')->nullable();
            $table->string('mode', 5)->nullable()->comment('operation - live/test');
            $table->string('accountName')->nullable();
            $table->string('saveCard', 10)->nullable();
            $table->string('stripeTestPublishableKey')->nullable();
            $table->text('stripeTestSecretKey')->nullable();
            $table->string('stripeLivePublishableKey')->nullable();
            $table->text('stripeLiveSecretKey')->nullable();
            $table->string('stripeDataImage')->nullable();
            $table->string('stripeStatementDescriptor', 20)->nullable();
            $table->decimal('stripeTransactionPercent', 4, 2)->nullable()->comment('stripe transaction percentage fee');
            $table->decimal('stripeTransactionFee', 4, 2)->nullable()->comment('stripe per transaction fee');
            $table->string('togoTestUser')->nullable();
            $table->text('togoTestPassword')->nullable();
            $table->string('togoLiveUser')->nullable();
            $table->text('togoLivePassword')->nullable();
            $table->decimal('togoMonthlyProcessingFee', 5, 2)->nullable();
            $table->decimal('togoTransactionPercent', 4, 2)->nullable()->comment('togo transaction percentage fee');
            $table->decimal('togoTransactionFee', 4, 2)->nullable()->comment('togo per transaction fee');
            $table->string('authNetTestUser')->nullable();
            $table->text('authNetTestPass')->nullable();
            $table->string('authNetLiveUser')->nullable();
            $table->text('authNetLivePass')->nullable();
            $table->timestamps();
        });

        Schema::table('domainPaymentConfig', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domainPaymentConfig', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('domainPaymentConfig');
    }
}
