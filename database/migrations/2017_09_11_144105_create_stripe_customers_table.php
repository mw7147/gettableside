<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripeCustomers', function (Blueprint $table) {
            $table->string('stripeID')->unique()->index()->comment('Stripe Customer ID');
            $table->integer('domainID')->unsigned()->index()->comment('togomeals.biz domain ID');
            $table->integer('userID')->index()->nullable()->comment('togomeals.biz user ID');
            $table->integer('contactID')->index()->comment('togomeals.biz contact ID');
            $table->string('object', 50)->nullable();
            $table->integer('accountBalance')->comment('account balance in pennies');
            $table->string('currency', 50)->nullable();
            $table->string('created', 30)->nullable();
            $table->string('defaultSource')->nullable();
            $table->boolean('delinquent')->nullable();
            $table->string('description')->nullable();
            $table->string('discount')->nullable();
            $table->string('email')->index()->nullable();
            $table->string('shipping')->nullable();
            $table->timestamps();
            $table->primary('stripeID');
        });

        Schema::table('stripeCustomers', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stripeCustomers', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('stripeCustomers');
    }
}
