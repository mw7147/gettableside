<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBounceContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounceContacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Contact Owner Domain ID');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('Contact user ID');
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('original contact ID');
            $table->integer('bounceID')->unsigned()->nullable()->index()->comment('bounces ID');
            $table->string('vip', '5')->nullable()->index()->comment('VIP status for contact');
            $table->string('fname', '100')->index();
            $table->string('lname', '100')->index();
            $table->string('email', '100')->index();
            $table->string('mobile', '100')->index();
            $table->string('unformattedMobile', '100')->index();
            $table->string('companyEmail', '100')->nullable();
            $table->string('companyName', '100')->index()->nullable();
            $table->string('companyPhone', '100')->nullable();
            $table->string('title', '100')->nullable();
            $table->string('address1', '100')->nullable();
            $table->string('address2', '100')->nullable();
            $table->string('city', '100')->nullable();
            $table->string('state', '100')->nullable();
            $table->string('zipCode', '25')->nullable()->index();
            $table->string('countryCode', '10')->index();
            $table->integer('birthDay')->index()->nullable();
            $table->integer('birthMonth')->index()->nullable();
            $table->integer('anniversaryDay')->index()->nullable();
            $table->integer('anniversaryMonth')->index()->nullable();
            $table->string('pixPublic')->nullable();
            $table->string('pixPrivate')->nullable();
            $table->string('merchantCustomerID')->nullable()->comment('credit card customer ID for contact');
            $table->timestamps();

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('bounceContacts', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::drop('bounceContacts');
    }
}
