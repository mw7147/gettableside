<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDietaryOptionsToMenuDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menuData', function (Blueprint $table) {
            $table->string('soy')->default(0)->after('spiceLevel');
            $table->string('nut')->default(0)->after('spiceLevel');
            $table->string('vegan')->default(0)->after('spiceLevel');
            $table->string('shellfish')->default(0)->after('spiceLevel');
            $table->string('meat')->default(0)->after('spiceLevel');
            $table->string('pork')->default(0)->after('spiceLevel');
            $table->string('lactose')->default(0)->after('spiceLevel');
            $table->string('gluten')->default(0)->after('spiceLevel');
            $table->string('egg')->default(0)->after('spiceLevel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuData', function (Blueprint $table) {
            $table->dropColumn('soy');
            $table->dropColumn('nut');
            $table->dropColumn('vegan');
            $table->dropColumn('shellfish');
            $table->dropColumn('meat');
            $table->dropColumn('pork');
            $table->dropColumn('lactose');
            $table->dropColumn('gluten');
            $table->dropColumn('egg');
        });
    }
}
