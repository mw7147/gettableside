<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMenuCategoriesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menuCategoriesData', function (Blueprint $table) {
            $table->boolean('categoryTax')->default(0)->after('categoryDescription')->comment('category sales tax - 1/0');
            $table->decimal('categoryTaxRate', 6, 3)->default(0)->after('categoryTax')->comment('category sales tax rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuCategoriesData', function (Blueprint $table) {
            $table->dropColumn('categoryTax');
            $table->dropColumn('categoryTaxRate');
        });
    }
}
