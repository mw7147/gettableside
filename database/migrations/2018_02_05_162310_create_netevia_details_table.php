<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeteviaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('neteviaDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('neteviaID')->unsigned()->index()->comment('id field - Represents a unique transaction identifier. This value is used to perform operations with transaction, e.g. Capture, Refund, etc.');
            $table->text('sessionID')->nullable();
            $table->integer('orderID')->unsigned()->nullable()->index()->comment('order ID from orders');
            $table->string('transactionID')->nullable()->index()->comment('transaction nonce');
            $table->integer('domainID')->unsigned()->nullable()->index();
            $table->integer('ownerID')->unsigned()->nullable()->index();
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('contact ID of customer');
            $table->string('ccName', 50)->nullable()->comment('Name on credit card');
            $table->integer('ccLastFour')->nullable();
            $table->string('ccType', 50)->nullable();
            $table->string('createdDate', 50)->nullable()->comment('Represents transaction created date');
            $table->string('operation', 50)->nullable()->comment('Represents operation type');
            $table->string('message')->nullable()->comment('Represents transaction message');
            $table->integer('code')->nullable()->comment('Represents transaction result code');
            $table->string('authorizationCode')->nullable()->comment('Represents processor authorization code');
            $table->string('processorTransactionId')->nullable()->comment('Represents processor transaction identifier');
            $table->decimal('amount', 7, 2)->comment('Represents processor transaction identifier');
            $table->string('currency', 20)->nullable()->comment('Represents requested currency code in ISO 4217 format');
            $table->decimal('paymentAmount', 7, 2)->comment('Represents payment amount');
            $table->string('paymentCurrency', 20)->nullable()->comment('Represents payment currency code in ISO 4217 format');
            $table->string('recurrentToken')->nullable()->comment('Represents token to perform recurrent operations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('neteviaDetails');
    }
}
