<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerPINsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managerPIN', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index();
            $table->integer('userID')->unsigned()->nullable()->index();
            $table->integer('managerPIN')->comment('manager pin for refunding transaction');
            $table->timestamps();
        
        $table->foreign('domainID')
            ->references('id')->on('domains')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        $table->foreign('userID')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('managerPIN', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
            $table->dropForeign(['userID']);  
        });

        Schema::dropIfExists('managerPIN');
    }
}
