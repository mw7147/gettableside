<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public function up()
    {
        Schema::create('menuData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('active')->index();
            $table->integer('menuID')->unsigned()->index();
            $table->integer('menuCategoriesDataID')->comment('menuCategoriesDataID');
            $table->integer('sortOrder')->nullable();
            $table->integer('spiceLevel')->nullable()->comment('0=>none, 1=>mild, 2=>moderate, 3=>hot, 4=>extra hot');
            $table->string('menuItem');
            $table->text('menuItemDescription')->nullable();
            $table->string('printKitchen')->nullable();
            $table->string('printOrder')->nullable();
            $table->string('menuSidesID');
            $table->string('menuOptionsID');
            $table->string('menuAddOnsID');
            $table->string('portion1');
            $table->decimal('price1', 5, 2);
            $table->string('portion2')->nullable();
            $table->decimal('price2', 5, 2)->nullable();
            $table->string('portion3')->nullable();
            $table->decimal('price3', 5, 2)->nullable();
            $table->string('portion4')->nullable();
            $table->decimal('price4', 5, 2)->nullable();
            $table->string('portion5')->nullable();
            $table->decimal('price5', 5, 2)->nullable();
            $table->string('portion6')->nullable();
            $table->decimal('price6', 5, 2)->nullable();
            $table->string('portion7')->nullable();
            $table->decimal('price7', 5, 2)->nullable();
            $table->timestamps();
        });

        Schema::table('menuData', function (Blueprint $table) {

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });

        Schema::table('menuData', function (Blueprint $table) {

            $table->foreign('menuID')
                ->references('id')->on('menu')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('menuData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::table('menuData', function (Blueprint $table) {
            $table->dropForeign(['menuID']);
        });

        Schema::dropIfExists('menuData');
    }
}

