<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuSidesDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuSidesData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('menuSidesID')->unsigned()->index()->nullable();
            $table->string('sideName');
            $table->string('printKitchen')->nullable();
            $table->string('printOrder')->nullable();
            $table->string('sidesDefault', 5)->nullable();
            $table->decimal('upCharge', 5, 2)->nullable();
            $table->timestamps();
        });

        Schema::table('menuSidesData', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menuSidesData', function (Blueprint $table) {
            $table->foreign('menuSidesID')
                ->references('id')->on('menuSides')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuSidesData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::table('menuSidesData', function (Blueprint $table) {
            $table->dropForeign(['menuSidesID']);
        });


        Schema::dropIfExists('menuSidesData');
    }
}
