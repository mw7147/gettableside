<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailsSent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('Domain ID field');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('User ID field');
            $table->integer('groupID')->unsigned()->nullable()->index()->comment('groupNames ID field');
            $table->integer('templateID')->unsigned()->nullable()->index()->comment('textTemplate ID field');
            $table->string('messageType', 25)->nullable()->index();
            $table->string('fromEmailAddress')->nullable()->comment('from email address');
            $table->string('fromName')->nullable()->comment('from name');
            $table->string('subject')->nullable();
            $table->text('emailBody')->nullable()->comment('html version');
            $table->text('textBody')->nullable()->comment('text version');
            $table->string('attachment')->nullable();
            $table->string('sendIP')->nullable();
            $table->integer('totalQueue')->comment('total good messages queued');
            $table->integer('totalBad')->nullable()->comment('total contacts with no email address not queued');
            $table->integer('totalSent')->nullable()->comment('total messages sent');
            $table->integer('totalSentFail')->nullable()->comment('total messages failed sending');
            $table->integer('totalReads')->default(0)->comment('total reads - linkTracker');
            $table->integer('totalClicks')->default(0)->comment('total clicks - linkClicks');
            $table->dateTime('sendStart')->nullable()->comment('email start time');
            $table->dateTime('sendStop')->nullable()->comment('email stop time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emailsSent');
    }
}