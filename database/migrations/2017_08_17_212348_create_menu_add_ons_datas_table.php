<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuAddOnsDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuAddOnsData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('menuAddOnsID')->unsigned()->index()->nullable();
            $table->integer('menuDataID')->index()->nullable()->comment('menuData ID');
            $table->string('addOnsName')->comment('do not allow double quote in name');
            $table->string('printKitchen')->nullable();
            $table->string('printOrder')->nullable();
            $table->string('addOnsDefault', 5)->nullable();
            $table->decimal('upCharge', 5, 2)->nullable();
            $table->timestamps();
        });

        Schema::table('menuAddOnsData', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('menuAddOnsData', function (Blueprint $table) {
            $table->foreign('menuAddOnsID')
                ->references('id')->on('menuAddOns')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuAddOnsData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::table('menuAddOnsData', function (Blueprint $table) {
            $table->dropForeign(['menuAddOnsID']);
        });


        Schema::dropIfExists('menuAddOnsData');
    }
}
