<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersForTips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('orders', 'tableID')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('tableID');
            });
        }

        if (Schema::hasColumn('orders', 'waiterID')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('waiterID');
            });
        }

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('locationID')->nullable()->after('orderType');
            $table->string('locationName', '100')->nullable()->after('locationID');
            $table->integer('tipTypeID')->nullable()->index()->after('locationName');
            $table->integer('tipDataID')->nullable()->index()->after('tipTypeID');
            $table->string('tipDataName', '100')->nullable()->index()->after('tipDataID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('locationID');
        $table->dropColumn('locationName');
        $table->dropColumn('tipTypeID');
        $table->dropColumn('tipDataID');
        $table->dropColumn('tipDataName');
    }
}
