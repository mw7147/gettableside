<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('printers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->comment('domainID');
            $table->integer('type')->comment('printer type - 1 = HP ePrint, 2=Star CloudPrint');
            $table->string('name')->comment('printer name');
            $table->string('description')->comment('printer description/location');
            $table->string('address')->comment('printer address - mac/ hp eprint, etc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('printers');
    }
}
