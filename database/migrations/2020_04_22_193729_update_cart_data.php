<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCartData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cartData', function (Blueprint $table) {
            $table->integer('quantity')->default(1)->after('printReceipt')->comment('quantity');
            $table->decimal('extendedPrice', 8, 2)->default(0)->after('price')->comment('extended price');
            $table->decimal('tax', 7, 2)->default(0)->after('extendedPrice')->comment('item sales tax');
            $table->decimal('globalDiscountPercent', 6, 2)->default(0)->after('tax')->comment('global discount percent');
            $table->decimal('globalDiscountAmount', 7, 2)->default(0)->after('globalDiscountPercent')->comment('global discount amount');
            $table->decimal('netExtendedPrice', 7, 2)->default(0)->after('globalDiscountAmount')->comment('net extended price');

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cartData', function (Blueprint $table) {
            $table->dropColumn('quantity');
            $table->dropColumn('extendedPrice');
            $table->dropColumn('tax');
            $table->dropColumn('globalDiscountPercent');
            $table->dropColumn('globalDiscountAmount');
            $table->dropColumn('netExtendedPrice');
        });
    }

}
