<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripeTransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->index()->comment('togomeals.biz domain ID');
            $table->integer('ownerID')->index()->comment('togomeals.biz domain owner ID');
            $table->integer('userID')->index()->nullable()->comment('togomeals.biz user ID');
            $table->integer('contactID')->index()->comment('togomeals.biz contact ID');
            $table->string('sessionID')->index();
            $table->string('orderID')->index()->comment('orders ID');
            $table->string('stripeChargeID')->index()->comment('Stripe Charge ID');
            $table->string('stripeCustomerID')->index()->comment('Stripe Customer ID');
            $table->string('stripeCardID')->index()->comment('Stripe Customer Card ID');
            $table->string('brand', '50');
            $table->string('last4', '5');
            $table->string('fname', '100');
            $table->string('lname', '100');
            $table->integer('subTotal')->comment('sub total in pennies');
            $table->integer('tax')->comment('tax in pennies');
            $table->integer('tip')->comment('tip in pennies');
            $table->integer('deliveryCharge')->comment('deliveryCharge in pennies');
            $table->integer('total')->comment('total in pennies');
            $table->text('stripeResponse')->comment('complete response from Stripe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripeTransactions');
    }
}
