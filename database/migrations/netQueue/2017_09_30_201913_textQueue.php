<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TextQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('maildb')->create('textQueue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('messageID')->unsigned()->nullable()->index()->comment('textsSent ID field');
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('ID field - Domains');
            $table->integer('parentDomainID')->nullable()->index();
            $table->integer('userID')->nullable()->index();
            $table->integer('groupID')->unsigned()->nullable()->index()->comment('groupNames ID field');
            $table->integer('templateID')->unsigned()->nullable()->index()->comment('textTemplate ID field');
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('contact ID field');
            $table->string('sendType', '10')->nullable();
            $table->string('fname', '100')->nullable();
            $table->string('lname', '100')->nullable();
            $table->string('messageType', '25')->index();
            $table->string('message');
            $table->string('pixFile')->nullable();
            $table->string('mobile', '50')->nullable()->comment('formatted mobile number');
            $table->string('unformattedMobile', '30')->nullable()->comment('unformatted mobile number');
            $table->string('carrierID', '100')->nullable();
            $table->string('carrierName', '100')->nullable();
            $table->string('gateway', '100')->nullable();
            $table->timestamps();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('maildb')->dropIfExists('textQueue');
    }
}
