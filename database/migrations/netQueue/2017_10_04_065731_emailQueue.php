<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('maildb')->create('emailQueue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->nullable();
            $table->integer('userID')->nullable();
            $table->integer('groupID')->nullable();
            $table->integer('templateID')->nullable();
            $table->integer('messageID')->index();
            $table->integer('contactID')->nullable();
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('company')->nullable();
            $table->string('messageType', 25);
            $table->string('emailAddress')->nullable();
            $table->string('subject')->nullable();
            $table->text('emailBody')->nullable();
            $table->text('textBody')->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('maildb')->dropIfExists('emailQueue');
    }
}