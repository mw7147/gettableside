<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TextMIDQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('maildb')->create('textMIDQueue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mid')->index();
            $table->integer('domainID')->nullable();
            $table->timestamps();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('maildb')->dropIfExists('textMIDQueue');
    }
}
