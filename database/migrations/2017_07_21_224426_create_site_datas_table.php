<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siteData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->unique()->index()->comment('Domain ID field');
            $table->string('companyName')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zipCode')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('hours')->nullable();
            $table->string('pixBannerImage')->nullable();
            $table->string('pixBannerHeading')->nullable();
            $table->string('pixBannerBlurb')->nullable();
            $table->string('calloutBannerHeading')->nullable();
            $table->string('calloutBannerblurb')->nullable();
            $table->text('calloutBannerText')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('snapchat')->nullable();
            $table->string('servicesImage')->nullable();
            $table->text('servicesText')->nullable();
            $table->string('aboutImage')->nullable();
            $table->text('aboutText')->nullable();
            $table->timestamps();

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('siteData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('siteData');
    }
}
