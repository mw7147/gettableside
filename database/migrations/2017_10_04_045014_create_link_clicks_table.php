<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linkClicks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('linkDataID')->unsigned()->index()->comment('id from linkData table');
            $table->string('linkHash', '150')->index()->comment('Link hash in url');
            $table->string('linkAddress')->comment('redirect link');
            $table->integer('contactID')->index();
            $table->integer('domainID')->unsigned()->index();
            $table->integer('mailID')->unsigned()->index();
            $table->string('ipAddress', '100')->nullable();
            $table->string('userAgent')->nullable();
            $table->timestamps();

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('linkClicks', function (Blueprint $table) {

            $table->dropForeign(['domainID']);

        });


        Schema::dropIfExists('linkClicks');
    }
}