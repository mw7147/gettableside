<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuOptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('active')->index();                        
            $table->string('name');
            $table->text('description');
            $table->integer('maxOptions')->nullable()->comment('maximum number of options allowed selected');
            $table->timestamps();
        });

        Schema::table('menuOptions', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuOptions', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('menuOptions');
    }
}
