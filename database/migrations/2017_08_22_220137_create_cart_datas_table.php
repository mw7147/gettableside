<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cartData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('ownerID')->index()->comment('Domain ownerID field');
            $table->integer('userID')->nullable()->index();
            $table->string('sessionID')->index();
            $table->integer('menuDataID');
            $table->integer('menuCategoriesDataID')->comment('for order sort order');
            $table->integer('menuCategoriesDataSortOrder')->comment('for order sort order');
            $table->integer('menuDataSortOrder')->comment('for order sort order');
            $table->string('menuItem');
            $table->text('menuDescription')->nullable();
            $table->string('portion');
            $table->string('printKitchen')->nullable();
            $table->string('printOrder')->nullable();
            $table->decimal('price', 5, 2);
            $table->string('menuOptionsDataID')->nullable()->comment('menuOptionsData IDs');
            $table->string('menuAddOnsDataID')->nullable()->comment('menuAddOnsData IDs');
            $table->string('menuSidesDataID')->nullable()->comment('menuSidesData IDs');
            $table->text('instructions')->nullable();
            $table->string('orderIP', 50)->nullable();
            $table->decimal('tipAmount', 6, 2)->nullable()->comment('first item in order tip placeholder');
            $table->timestamps();
        });
        
        Schema::table('cartData', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cartData', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('cartData');
    }
}
