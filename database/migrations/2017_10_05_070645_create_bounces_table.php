<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBouncesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('serverID')->unsigned()->nullable()->comment('server ID field');   // debug and check only - should be same value for all records         
            $table->integer('domainID')->unsigned()->nullable()->index()->comment('domain ID field');
            $table->integer('userID')->unsigned()->nullable()->index()->comment('ID field - Customers');
            $table->integer('contactID')->unsigned()->nullable()->index()->comment('Contact ID field');
            $table->integer('messageID')->unsigned()->nullable()->index()->comment('emailsDetail ID field');
            $table->string('mailtext')->nullable()->index();
            $table->text('rawMessage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bounces');
    }
}