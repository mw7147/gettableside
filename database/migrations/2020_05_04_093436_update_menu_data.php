<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMenuData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menuData', function (Blueprint $table) {
            $table->boolean('itemTax')->default(0)->after('price7')->comment('item sales tax - 1/0');
            $table->decimal('itemTaxRate', 6, 3)->default(0)->after('itemTax')->comment('item sales tax rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuData', function (Blueprint $table) {
            $table->dropColumn('itemTax');
            $table->dropColumn('itemTaxRate');
        });
    }
}
