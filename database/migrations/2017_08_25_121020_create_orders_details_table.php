<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordersDetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('ownerID')->index()->comment('Domain ownerID field');
            $table->integer('contactID')->unsigned()->index();
            $table->string('sessionID')->index();
            $table->integer('menuDataID');
            $table->integer('menuCategoriesDataID');
            $table->integer('menuCategoriesDataSortOrder');
            $table->integer('menuDataSortOrder');
            $table->string('menuItem');
            $table->text('menuDescription')->nullable();
            $table->string('portion');
            $table->string('printOrder')->nullable()->comment('menu data print Kitchen');
            $table->string('printReceipt')->nullable()->comment('menu data print receipt');
            $table->decimal('price', 5, 2);
            $table->string('menuOptionsDataID')->nullable()->comment('menuOptionsData IDs');
            $table->string('menuAddOnsDataID')->nullable()->comment('menuAddOnsData IDs');
            $table->string('menuSidesDataID')->nullable()->comment('menuSidesData IDs');
            $table->text('instructions')->nullable();
            $table->string('orderIP', 50)->nullable();
            $table->decimal('tipAmount', 6, 2)->nullable()->comment('first item in order tip placeholder');
            $table->integer('discountPercent')->default(0);
            $table->timestamps();
        });
        
        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->foreign('contactID')
                ->references('id')->on('contacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->foreign('sessionID')
                ->references('sessionID')->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
            $table->dropForeign(['contactID']);
            $table->dropForeign(['sessionID']);
        });

        Schema::dropIfExists('ordersDetail');
    }
}
