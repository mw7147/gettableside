<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->integer('quantity')->default(1)->after('printReceipt')->comment('quantity');
            $table->decimal('extendedPrice', 8, 2)->default(0)->after('price')->comment('extended price');
            $table->decimal('tax', 7, 2)->default(0)->after('extendedPrice')->comment('item sales tax');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->dropColumn('quantity');
            $table->dropColumn('extendedPrice');
            $table->dropColumn('tax');
        });
    }

}
