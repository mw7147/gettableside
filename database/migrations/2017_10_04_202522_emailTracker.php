<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailTracker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emailsSentID')->unsigned()->index()->comment('emailsSent ID field - mid');
            $table->integer('emailsSentDetailID')->unsigned()->index()->comment('ID field - emailsSentDetails');
            $table->string('userAgent')->nullable()->comment('note type');          
            $table->string('ipAddress')->nullable()->comment('note type');          
            $table->timestamps();

            $table->foreign('emailsSentDetailID')
                ->references('id')->on('emailsSentDetails')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emailTracker', function (Blueprint $table) {
            $table->dropForeign(['emailsSentDetailID']);
        });

        Schema::dropIfExists('emailTracker');
    }
}