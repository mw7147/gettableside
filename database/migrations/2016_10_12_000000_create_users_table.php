<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_auth_level')->unsigned()->nullable()->index();
            $table->integer('domainID')->unsigned()->nullable()->index();
            $table->integer('parentDomainID')->nullable()->index();
            $table->string('active', '5');
            $table->string('fname');
            $table->string('lname');
            $table->string('email');
            $table->string('mobile', '100')->index();
            $table->string('unformattedMobile', '100');
            $table->string('zipCode', '25')->nullable()->index();
            $table->string('countryCode', '10')->nullable()->index();
            $table->string('password');
            $table->string('pixPublic')->nullable();
            $table->string('pixPrivate')->nullable();
            $table->string('rescueEmail')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {

            $table->foreign('role_auth_level')
                ->references('auth_level')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });

        Schema::table('users', function (Blueprint $table) {

            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign(['role_auth_level']);
        $table->dropForeign(['domainID']);
        
        Schema::dropIfExists('users');
    }
}
