<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateordersDetailWithTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->integer('tagTier1')->nullable()->index()->after('portion')->comment('tags tier 1');
            $table->integer('tagTier2')->nullable()->index()->after('tagTier1')->comment('tags tier 2');
            $table->integer('tagTier3')->nullable()->index()->after('tagTier2')->comment('tags tier 3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordersDetail', function (Blueprint $table) {
            $table->dropColumn('tagTier1');
            $table->dropColumn('tagTier2');
            $table->dropColumn('tagTier3');
        });
    }
}
