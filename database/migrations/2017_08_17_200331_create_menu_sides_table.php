<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuSidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuSides', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domainID')->unsigned()->index()->comment('Domain ID field');
            $table->integer('active')->index();                        
            $table->string('name');
            $table->text('description');
            $table->integer('numberSides');
            $table->timestamps();
        });

        Schema::table('menuSides', function (Blueprint $table) {
            $table->foreign('domainID')
                ->references('id')->on('domains')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menuSides', function (Blueprint $table) {
            $table->dropForeign(['domainID']);
        });

        Schema::dropIfExists('menuSides');
    }
}
