<?php

use Illuminate\Database\Seeder;

class locationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Location types
        DB::table('locationTypes')->insert([
            'type' => 'Table',
            'note' => 'Table',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);


        DB::table('locationTypes')->insert([
            'type' => 'Bar Seat',
            'note' => 'Bar Seats',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);











    }
}
