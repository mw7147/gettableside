<?php

use Illuminate\Database\Seeder;

class menuOptionsData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 1,
            'optionName' => 'No Bun',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 1,
            'optionName' => 'Homemade House Bun',
            'optionsDefault' => 'yes',
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 1,
            'optionName' => 'Homemade Beer Bread Bun',
            'optionsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 2,
            'optionName' => 'No Cheese',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 2,
            'optionName' => 'American',
            'optionsDefault' => 'yes',
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 2,
            'optionName' => 'Cheddar',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 2,
            'optionName' => 'Pepper Jack',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 2,
            'optionName' => 'Swiss',
            'optionsDefault' => null,
            'upCharge' => .5,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 2,
            'optionName' => 'Irish Cheddar',
            'optionsDefault' => null,
            'upCharge' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 3,
            'optionName' => 'Rare',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    	DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 3,
            'optionName' => 'Medium Rare',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 3,
            'optionName' => 'Medium',
            'optionsDefault' => 'yes',
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 3,
            'optionName' => 'Medium Well',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
    		'domainID' => 1,
            'menuOptionsID' => 3,
            'optionName' => 'Well Done',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
            'domainID' => 1,
            'menuOptionsID' => 4,
            'optionName' => 'Mild Spice',
            'optionsDefault' => 'yes',
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
            'domainID' => 1,
            'menuOptionsID' => 4,
            'optionName' => 'Medium Spice',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
            'domainID' => 1,
            'menuOptionsID' => 4,
            'optionName' => 'Hot Spice',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
            'domainID' => 1,
            'menuOptionsID' => 5,
            'optionName' => 'Homemade Blue Cheese',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptionsData')->insert([
            'domainID' => 1,
            'menuOptionsID' => 5,
            'optionName' => 'Homemade Ranch',
            'optionsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  






    }
}
