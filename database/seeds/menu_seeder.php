<?php

use Illuminate\Database\Seeder;

class menu_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('menu')->insert([
    		'domainID' => 1,
    		'menuName' => 'Mugshots Demo Menu',
    		'menuDescription' => 'Mugshots Online Demo Menu',
            'menuCategoriesID' => 1,
            'active' => 'yes',
            'defaultMenu' => 'yes',
			'startSunday' => '12:00:00',
            'stopSunday' => '19:00:00',
            'startMonday' => '11:00:00',
            'stopMonday' => '21:00:00',
            'startTuesday' => '11:00:00',
            'stopTuesday' => '21:00:00',
            'startWednesday' => '11:00:00',
            'stopWednesday' => '21:00:00',
            'startThursday' => '11:00:00',
            'stopThursday' => '21:00:00',
            'startFriday' => '11:00:00',
            'stopFriday' => '22:00:00',
            'startSaturday' => '11:00:00',
            'stopSaturday' => '22:00:00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

    }
}
