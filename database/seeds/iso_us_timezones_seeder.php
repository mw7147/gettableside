<?php

use Illuminate\Database\Seeder;

class iso_us_timezones_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

			
		DB::table('usTimezones')->insert([
			'timezone' => "America/Adak",
			'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Anchorage',
						'description' => 'AST - Alaska Standard Time'
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Boise',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Chicago',
						'description' => 'CDT - Central Daylight Time'
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Denver',
						'description' => 'MDT - Mountain Daylight Time'
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Detroit',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Indianapolis',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Knox',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Marengo',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Petersburg',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Tell_City',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Vevay',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Vincennes',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Indiana/Winamac',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Juneau',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Kentucky/Louisville',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Kentucky/Monticello',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Los_Angeles',
						'description' => 'PDT - Pacific Daylight Time'
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Menominee',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Metlakatla',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/New_York',
						'description' => 'EDT - Eastern Daylight Time'
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Nome',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/North_Dakota/Beulah',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/North_Dakota/Center',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/North_Dakota/New_Salem',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Phoenix',
						'description' => 'MST - Mountain Standard Time'
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Sitka',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'America/Yakutat',
						'description' => ''
		]);
		
		DB::table('usTimezones')->insert([
						'timezone' =>'Pacific/Honolulu',
						'description' => 'HST - Hawaii Standard Time'
		]);
		

    }
}

