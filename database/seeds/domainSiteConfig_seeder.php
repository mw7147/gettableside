<?php

use Illuminate\Database\Seeder;

class domainSiteConfig_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('domainSiteConfig')->insert([
            'domainID' => 1,
            'managerFname' => 'Joe',
            'managerLname' => 'Owner',            
            'sendEmail' => 'mugshotsroanoke@togomeals.biz',
            'SendText' => '2053010265',
            'sendEmailUserName' => 'mugshotsroanoke@togomeals.biz',
            'sendEmailPassword' => encrypt('t4g4m2@ls'),
            'orderReceiveEmail' => 'mugshotsroanoke@togomeals.biz',
            'hpEprint' => 'riverhouse8710@hpeprint.com',
            'starPrinterMAC' => '00:11:62:0D:82:26',
            'printReceipt' => 1,
            'printOrder' => 2,
            'taxRate' => 8.25,
            'locationName' => 'Mugshots Roanoke',
            'address1' => '400 South Oak Street',
            'address2' => 'Suite 200',
            'city' => 'Roanoke',
            'state' => 'TX',
            'zipCode' => '76262',
            'locationPhone' => '(817) 975-1234',
            'locationIPAddress' => '192.168.1.60',
            'locationOrder' => 'no',
            'delivery' => 'no',
            'orderMessage' => 'Your order has been recieved and is being prepared. Pickup time is in approximately 20 minutes.',
            'deliveryMessage' => 'Your order has been recieved and is being prepared. Pickup time is in approximately 40 minutes.',
            'orderFooter' => 'Thank you for your order.',
            'orderAlert' => 'Thank you for your order. Your order has been received and is being prepared. Payment is due at pickup.',
            'orderReady' => 'Your order has been prepared and is ready for pickup. Thank you for your order.',
            'mapLink' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5398.456569442504!2d-97.22859118727322!3d32.99708679312106!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864dd0fa567d1949%3A0x4a74bafa6e377da6!2s400+S+Oak+St%2C+Roanoke%2C+TX+76262!5e0!3m2!1sen!2sus!4v1504100131156',
            'welcomeEmail' => 'Welcome to Mugshots online! You may order online for pickup during our normal business hours. Login to your account to review your order history and print receipts if needed.<br><br>Thank you for using Mugshots Online.',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  




        
    }
}