<?php

use Illuminate\Database\Seeder;

class domainPaymentConfig_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		DB::table('domainPaymentConfig')->insert([
    		'domainID' => 1,
            'ccType' => 2,
            'mode' => 'test',
            'accountName' => 'HDWRD Demo',
            'stripeTestPublishableKey' => 'pk_test_DrucgRjHOXHZNuSVMFfSynFy',
            'stripeTestSecretKey' => encrypt('sk_test_Vn8gYv96wUpuEklxDTRh7POg'),
            'stripeTransactionPercent' => '2.9',
            'stripeTransactionFee' => '.30',
            'stripeStatementDescriptor' => 'HDWRD Demo',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  


    }
}
