<?php

use Illuminate\Database\Seeder;

class siteData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siteData')->insert([
            'domainID' => 1,
            'companyName' => 'Location Corporate',
            'address1' => '22 Cowboys Way',
            'address2' => 'Suite 200',
            'city' => 'Westlake',
            'state' => 'TX',
            'zipCode' => '76262',
            'phone' => '(817) 975-1234',
            'email' => 'info@mugshotsbarandgrill.com',
            'hours' => '<table class="table"><tr><td>Monday - Thursday</td><td>11AM to 10PM</td></tr><tr><td>Friday - Saturday</td><td>11AM to 12PM</td></tr><tr><td>Sunday</td><td>12PM to 7PM</td></tr></table>',
            'pixBannerImage' => '/storage/customerImages/1/burgerFries1920.jpg',
            'pixBannerHeading' => 'Welcome To Mugshots!',
            'pixBannerBlurb' => 'Order online or call in. Delivery or takeout!<br>Or come in and enjoy our restaurant.',
            'calloutBannerHeading' => 'Musgshots',
            'calloutBannerblurb' => 'How it all started ...',
            'calloutBannerText' => '<p>Mugshots was born when two college buddies with a dream moved to Hawaii. While tending bar on Waikiki beach they dreamed of owning their own place where customers and employees would always be "Havin a Good Time." In 2004, with hard work and plenty support from friends and family, the guys made their dream a reality. Mugshots #1 opened in Hattiesburg, Mississippi. Short on cash, but full of gratitude, they showed their thanks by naming appetizers, burgers, and menu items after family and friends and decorating the walls with pictures of their customers.</p><p>Today, founders Chris McDonald and Ron Savell continue working the dream. The Mugshots values of "Havin A Good Time" and "Making Life Grand" by giving back to the community are the foundation of every location. </p>',
            'facebook' => 'https://www.facebook.com/MugshotsGrillandBar/',
            'twitter' => 'https://twitter.com/mugshotsburgers',
            'instagram' => 'https://www.instagram.com/mugshotsgrillandbar/',
            'snapchat' => 'https://www.snapchat.com/add/mugshotsburgers',
            'servicesImage' => '/storage/customerImages/1/mugshotsCarry.jpg',
            'servicesText' => '<p>Are you catering to a large family or office function? We offer a full catering menu at an affordable price. Call us or come in for pricing and arrangement information. Plan your next party or meeting with us, and we will supply unmatched food quality with no hassle nor additional cost to you.</p><p>Whether you are busy at work or just relaxing at home, you can still enjoy a great meal. Just call us, WE DELIVER!! You can order your favorite dishes and get suggestions for exploring new creations. We offer free delivery anywhere within 15 miles of the restaurant. And best of all, it typically arrives in less than 40 min unless there is bad weather which in that case may take a longer. Order online or call (205) 391-0572 during any hours of operation.</p><p>We accept all major credit cards. If you order for a delivery by calling in and would like to pay with a credit card, please let us know so we may process the payment for you conveniently.</p>',
            'aboutImage' => '/storage/customerImages/1/hamburger450.jpg',
            'aboutText' => '<p>Mugshots was born when two college buddies with a dream moved to Hawaii. While tending bar on Waikiki beach they dreamed of owning their own place where customers and employees would always be "Havin a Good Time." In 2004, with hard work and plenty support from friends and family, the guys made their dream a reality. Mugshots #1 opened in Hattiesburg, Mississippi. Short on cash, but full of gratitude, they showed their thanks by naming appetizers, burgers, and menu items after family and friends and decorating the walls with pictures of their customers.</p><p>Today, founders Chris McDonald and Ron Savell continue working the dream. The Mugshots values of "Havin A Good Time" and "Making Life Grand" by giving back to the community are the foundation of every location. </p>',    
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);




        

    }
}
