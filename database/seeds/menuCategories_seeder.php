<?php

use Illuminate\Database\Seeder;

class menuCategories_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menuCategories')->insert([
            'domainID' => 1,
            'active' => 1,
            'name' => 'Musgshots Main Menu Categories',
            'description' => 'Musgshots Demo Main Menu Categories',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

       
    }
}
