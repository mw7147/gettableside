<?php

use Illuminate\Database\Seeder;

class roles_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //HWCT Management
        DB::table('roles')->insert([
            'name' => 'HWCT Admin',
            'auth_level' => 50,
            'description' => 'HWCT Administration and Executives',
            'urlPrefix' => 'admin',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        // HWCT Staff / Customer Service
   		DB::table('roles')->insert([
            'name' => 'HWCT Staff',
            'auth_level' => 40,
            'description' => 'HWCT Staff and Customer Service',
            'urlPrefix' => 'hwct',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        // Owner Account - multilocation owner
        DB::table('roles')->insert([
            'name' => 'Owner',
            'auth_level' => 35,
            'description' => 'Location Owner / Customer Owner / Multi Location Owner',
            'urlPrefix' => 'owner',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        // Manager Account - location owner
        DB::table('roles')->insert([
            'name' => 'Manager',
            'auth_level' => 30,
            'description' => 'Location Manager',
            'urlPrefix' => 'manager',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        // Staff / Employee Account
        DB::table('roles')->insert([
            'name' => 'Staff',
            'auth_level' => 20,
            'description' => 'Location Staff',
            'urlPrefix' => 'staff',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        // Registered User / Customer
   		DB::table('roles')->insert([
            'name' => 'Customer',
            'auth_level' => 10,
            'description' => 'Customer / Contact',
            'urlPrefix' => 'customer',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        // no account
        DB::table('roles')->insert([
            'name' => 'Anonymous',
            'auth_level' => 0,
            'description' => 'Anonymous User / Contact',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
    }
}
