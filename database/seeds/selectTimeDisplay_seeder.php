<?php

use Illuminate\Database\Seeder;

class selectTimeDisplay_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '00:00:00',
            'timeString' => '00:00:00',            
            'timeDisplay' => '12:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '00:30:00',
            'timeString' => '00:30:00',            
            'timeDisplay' => '12:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '01:00:00',
            'timeString' => '01:00:00',
            'timeDisplay' => '01:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '01:30:00',
            'timeString' => '01:30:00',
            'timeDisplay' => '01:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);        
        
        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '02:00:00',
            'timeString' => '02:00:00',
            'timeDisplay' => '02:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '02:30:00',
            'timeString' => '02:30:00',
            'timeDisplay' => '02:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);        
        
        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '03:00:00',
            'timeString' => '03:00:00',
            'timeDisplay' => '03:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '03:30:00',
            'timeString' => '03:30:00',
            'timeDisplay' => '03:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '04:00:00',
            'timeString' => '04:00:00',
            'timeDisplay' => '04:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '04:30:00',
            'timeString' => '04:30:00',
            'timeDisplay' => '04:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '05:00:00',
            'timeString' => '05:00:00',
            'timeDisplay' => '05:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '05:30:00',
            'timeString' => '05:30:00',
            'timeDisplay' => '05:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '06:00:00',
            'timeString' => '06:00:00',
            'timeDisplay' => '06:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '06:30:00',
            'timeString' => '06:30:00',
            'timeDisplay' => '06:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '07:00:00',
            'timeString' => '07:00:00',
            'timeDisplay' => '07:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '07:30:00',
            'timeString' => '07:30:00',
            'timeDisplay' => '07:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '08:00:00',
            'timeString' => '08:00:00',
            'timeDisplay' => '08:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '08:30:00',
            'timeString' => '08:30:00',
            'timeDisplay' => '08:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '09:00:00',
            'timeString' => '09:00:00',
            'timeDisplay' => '09:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '09:30:00',
            'timeString' => '09:30:00',
            'timeDisplay' => '09:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '10:00:00',
            'timeString' => '10:00:00',
            'timeDisplay' => '10:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '10:30:00',
            'timeString' => '10:30:00',
            'timeDisplay' => '10:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '11:00:00',
            'timeString' => '11:00:00',
            'timeDisplay' => '11:00AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '11:30:00',
            'timeString' => '11:30:00',
            'timeDisplay' => '11:30AM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '12:00:00',
            'timeString' => '12:00:00',
            'timeDisplay' => '12:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '12:30:00',
            'timeString' => '12:30:00',
            'timeDisplay' => '12:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '13:00:00',
            'timeString' => '13:00:00',
            'timeDisplay' => '01:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '13:30:00',
            'timeString' => '13:30:00',
            'timeDisplay' => '01:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);        
        
        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '14:00:00',
            'timeString' => '14:00:00',
            'timeDisplay' => '02:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '14:30:00',
            'timeString' => '14:30:00',
            'timeDisplay' => '02:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);        
        
        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '15:00:00',
            'timeString' => '15:00:00',
            'timeDisplay' => '03:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '15:00:00',
            'timeString' => '15:00:00',
            'timeDisplay' => '03:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '16:00:00',
            'timeString' => '16:00:00',
            'timeDisplay' => '04:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '16:30:00',
            'timeString' => '16:30:00',
            'timeDisplay' => '04:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '17:00:00',
            'timeString' => '17:00:00',
            'timeDisplay' => '05:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '17:30:00',
            'timeString' => '17:30:00',
            'timeDisplay' => '05:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '18:00:00',
            'timeString' => '18:00:00',
            'timeDisplay' => '06:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '18:30:00',
            'timeString' => '18:30:00',
            'timeDisplay' => '06:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '19:00:00',
            'timeString' => '19:00:00',
            'timeDisplay' => '07:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '19:30:00',
            'timeString' => '19:30:00',
            'timeDisplay' => '07:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '20:00:00',
            'timeString' => '20:00:00',
            'timeDisplay' => '08:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '20:30:00',
            'timeString' => '20:30:00',
            'timeDisplay' => '08:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '21:00:00',
            'timeString' => '21:00:00',
            'timeDisplay' => '09:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '21:30:00',
            'timeString' => '21:30:00',
            'timeDisplay' => '09:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '22:00:00',
            'timeString' => '22:00:00',
            'timeDisplay' => '10:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '22:30:00',
            'timeString' => '22:30:00',
            'timeDisplay' => '10:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '23:00:00',
            'timeString' => '23:00:00',
            'timeDisplay' => '11:00PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('selectTimeDisplay')->insert([
            'timeValue' => '23:30:00',
            'timeString' => '23:30:00',
            'timeDisplay' => '11:30PM',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    



    }
}
