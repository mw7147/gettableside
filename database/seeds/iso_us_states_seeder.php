<?php

use Illuminate\Database\Seeder;

class iso_us_states_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
		DB::table('usStates')->insert(['stateCode' => "AL", 'stateName' =>" Alabama"]);
		DB::table('usStates')->insert(['stateCode' => "AK", 'stateName' =>" Alaska"]);
		DB::table('usStates')->insert(['stateCode' => "AZ", 'stateName' =>" Arizona"]);
		DB::table('usStates')->insert(['stateCode' => "AR", 'stateName' =>" Arkansas"]);
		DB::table('usStates')->insert(['stateCode' => "CA", 'stateName' =>" California"]);
		DB::table('usStates')->insert(['stateCode' => "CO", 'stateName' =>" Colorado"]);
		DB::table('usStates')->insert(['stateCode' => "CT", 'stateName' =>" Connecticut"]);
		DB::table('usStates')->insert(['stateCode' => "DE", 'stateName' =>" Delaware"]);
		DB::table('usStates')->insert(['stateCode' => "FL", 'stateName' =>" Florida"]);
		DB::table('usStates')->insert(['stateCode' => "GA", 'stateName' =>" Georgia"]);
		DB::table('usStates')->insert(['stateCode' => "HI", 'stateName' =>" Hawaii"]);
		DB::table('usStates')->insert(['stateCode' => "ID", 'stateName' =>" Idaho"]);
		DB::table('usStates')->insert(['stateCode' => "IL", 'stateName' =>" Illinois"]);
		DB::table('usStates')->insert(['stateCode' => "IN", 'stateName' =>" Indiana"]);
		DB::table('usStates')->insert(['stateCode' => "IA", 'stateName' =>" Iowa"]);
		DB::table('usStates')->insert(['stateCode' => "KS", 'stateName' =>" Kansas"]);
		DB::table('usStates')->insert(['stateCode' => "KY", 'stateName' =>" Kentucky"]);
		DB::table('usStates')->insert(['stateCode' => "LA", 'stateName' =>" Louisiana"]);
		DB::table('usStates')->insert(['stateCode' => "ME", 'stateName' =>" Maine"]);
		DB::table('usStates')->insert(['stateCode' => "MD", 'stateName' =>" Maryland"]);
		DB::table('usStates')->insert(['stateCode' => "MA", 'stateName' =>" Massachusetts"]);
		DB::table('usStates')->insert(['stateCode' => "MI", 'stateName' =>" Michigan"]);
		DB::table('usStates')->insert(['stateCode' => "MN", 'stateName' =>" Minnesota"]);
		DB::table('usStates')->insert(['stateCode' => "MS", 'stateName' =>" Mississippi"]);
		DB::table('usStates')->insert(['stateCode' => "MO", 'stateName' =>" Missouri"]);
		DB::table('usStates')->insert(['stateCode' => "MT", 'stateName' =>" Montana"]);
		DB::table('usStates')->insert(['stateCode' => "NE", 'stateName' =>" Nebraska"]);
		DB::table('usStates')->insert(['stateCode' => "NV", 'stateName' =>" Nevada"]);
		DB::table('usStates')->insert(['stateCode' => "NH", 'stateName' =>" New Hampshire"]);
		DB::table('usStates')->insert(['stateCode' => "NJ", 'stateName' =>" New Jersey"]);
		DB::table('usStates')->insert(['stateCode' => "NM", 'stateName' =>" New Mexico"]);
		DB::table('usStates')->insert(['stateCode' => "NY", 'stateName' =>" New York"]);
		DB::table('usStates')->insert(['stateCode' => "NC", 'stateName' =>" North Carolina"]);
		DB::table('usStates')->insert(['stateCode' => "ND", 'stateName' =>" North Dakota"]);
		DB::table('usStates')->insert(['stateCode' => "OH", 'stateName' =>" Ohio"]);
		DB::table('usStates')->insert(['stateCode' => "OK", 'stateName' =>" Oklahoma"]);
		DB::table('usStates')->insert(['stateCode' => "OR", 'stateName' =>" Oregon"]);
		DB::table('usStates')->insert(['stateCode' => "PA", 'stateName' =>" Pennsylvania"]);
		DB::table('usStates')->insert(['stateCode' => "RI", 'stateName' =>" Rhode Island"]);
		DB::table('usStates')->insert(['stateCode' => "SC", 'stateName' =>" South Carolina"]);
		DB::table('usStates')->insert(['stateCode' => "SD", 'stateName' =>" South Dakota"]);
		DB::table('usStates')->insert(['stateCode' => "TN", 'stateName' =>" Tennessee"]);
		DB::table('usStates')->insert(['stateCode' => "TX", 'stateName' =>" Texas"]);
		DB::table('usStates')->insert(['stateCode' => "UT", 'stateName' =>" Utah"]);
		DB::table('usStates')->insert(['stateCode' => "VT", 'stateName' =>" Vermont"]);
		DB::table('usStates')->insert(['stateCode' => "VA", 'stateName' =>" Virginia"]);
		DB::table('usStates')->insert(['stateCode' => "WA", 'stateName' =>" Washington"]);
		DB::table('usStates')->insert(['stateCode' => "WV", 'stateName' =>" West Virginia"]);
		DB::table('usStates')->insert(['stateCode' => "WI", 'stateName' =>" Wisconsin"]);
		DB::table('usStates')->insert(['stateCode' => "WY", 'stateName' =>" Wyoming"]);
		DB::table('usStates')->insert(['stateCode' => "DC", 'stateName' =>" District of Columbia"]);
		DB::table('usStates')->insert(['stateCode' => "AS", 'stateName' =>" American Samoa"]);
		DB::table('usStates')->insert(['stateCode' => "GU", 'stateName' =>" Guam"]);
		DB::table('usStates')->insert(['stateCode' => "MP", 'stateName' =>" Northern Mariana Islands"]);
		DB::table('usStates')->insert(['stateCode' => "PR", 'stateName' =>" Puerto Rico"]);
		DB::table('usStates')->insert(['stateCode' => "UM", 'stateName' =>" United States Minor Outlying Islands"]);
		DB::table('usStates')->insert(['stateCode' => "VI", 'stateName' =>" Virgin Islands, U.S."]);


    }
}
