<?php

use Illuminate\Database\Seeder;

class menuCategoriesData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menuCategoriesData')->insert([
    		'domainID' => 1,
            'menuCategoriesID' => 1,
            'sortOrder' => 1,
            'categoryName' => 'Chinese Food',
            'categoryDescription' => 'Wonderful Chinese Food Description Goes Here',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('menuCategoriesData')->insert([
    		'domainID' => 1,
            'menuCategoriesID' => 1,
            'sortOrder' => 2,
            'categoryName' => 'American Food',
            'categoryDescription' => 'Wonderful American Food Description Goes Here',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('menuCategoriesData')->insert([
    		'domainID' => 1,
            'menuCategoriesID' => 1,
            'sortOrder' => 3,
            'categoryName' => 'Italian Food',
            'categoryDescription' => 'Wonderful Italian Food Description Goes Here',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('menuCategoriesData')->insert([
    		'domainID' => 1,
            'menuCategoriesID' => 1,
            'sortOrder' => 4,
            'categoryName' => 'Desserts',
            'categoryDescription' => 'Awesome Desserts Description Goes Here',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);






    }
}
