<?php

use Illuminate\Database\Seeder;

class printers_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //HP ePrint
        DB::table('printers')->insert([
            'domainID' => 18,
            'type' => 1,
            'name' => 'HP ePrint',
            'description' => 'Riverhouse 8710 Printer',
            'address' => 'riverhouse8710@hpeprint.com',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        //Star Printer
        DB::table('printers')->insert([
            'domainID' => 18,
            'type' => 2,
            'name' => 'Star CloudPrint',
            'description' => 'Star TSP650II Thermal Printer',
            'address' => '00:11:62:0D:82:26',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);


    }
}
