<?php

use Illuminate\Database\Seeder;

class siteStyle_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('siteStyles')->insert([
            'domainID' => 1,
            // all domains
            'fontColor' => '#333333',
            'bannerButtonBackgroundColor' => '#5a5a5a',
            'bannerButtonBorderColor' => '#5a5a5a',
            'bannerButtonColor' => '#fff',
            'bannerButtonColorHover' => '#ccc',
            'completeOrderBackgroundColor' => '#ffffff',
            'completeOrderBorderColor' => '#bbbbbb',
            'completeOrderColor' => '#333333',
            'completeOrderColorHover' => '#cccccc',
            // web only
            'bannerHomeMinHeight' => '700',
            'pixBannerBackgroundColor' => '#dedede',
            'pixBannerOpacity' => '.70',
            'pixBannerPadding' => '20',
            'pixBannerWidth' => '46',
            'pixBannerColor' => 'black',
            'navBarToggleBackgroundColor' => '#8b2131',
            'navBarToggleBorderColor' => '#8b2131',
            'navBarLogoMaxHeight' => '100',
            'navBarLogoMaxPadding' => '15',
            'navBarLogoMaxMarginLeft' => '50',
            'navBarLogoMaxMarginTop' => '0',
            'navBarLIColor' => '#8b2131',
            'navBarLIHoverColor' => '#ffdd00',
            'calloutBannerBackgroundColor' => '#8b2131',
            'calloutBannerBorderColor' => '#8b2131',
            'calloutBannerColor' => '#fff',
            'calloutBannerHeight' => '92',
            'footerBackgroundColor' => '#2f2f2f',
            'footerColor' => '#fff',
            'footerLinkColor' => '#fff',
            'footerLinkHoverColor' => '#ccc',
            'servicesMinHeight' => '650',
            // timestamps
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);



    }
}
