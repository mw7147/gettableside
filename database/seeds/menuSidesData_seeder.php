<?php

use Illuminate\Database\Seeder;

class menuSidesData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('menuSidesData')->insert([
    		'domainID' => 1,
            'menuSidesID' => 1,
            'sideName' => 'French Fries',
            'sidesDefault' => 'yes',
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
    		'domainID' => 1,
            'menuSidesID' => 1,
            'sideName' => 'Homemade Potato Chips',
            'sidesDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
    		'domainID' => 1,
            'menuSidesID' => 1,
            'sideName' => 'Sweet Potato Fries',
            'sidesDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
    		'domainID' => 1,
            'menuSidesID' => 1,
            'sideName' => 'Mixed Vegetables',
            'sidesDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
    		'domainID' => 1,
            'menuSidesID' => 1,
            'sideName' => 'Baked Potato',
            'sidesDefault' => null,
            'upCharge' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
            'domainID' => 1,
            'menuSidesID' => 2,
            'sideName' => 'White Rice',
            'sidesDefault' => 'yes',
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
            'domainID' => 1,
            'menuSidesID' => 2,
            'sideName' => 'Brown Rice',
            'sidesDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuSidesData')->insert([
            'domainID' => 1,
            'menuSidesID' => 2,
            'sideName' => 'Fried Rice',
            'sidesDefault' => null,
            'upCharge' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 








    }
}
