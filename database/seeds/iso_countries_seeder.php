<?php

use Illuminate\Database\Seeder;

class iso_countries_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		DB::table('countries')->insert(['countryCode' => "US", 'countryName' =>" United States"]);
		DB::table('countries')->insert(['countryCode' => "CA", 'countryName' =>" Canada"]);
		DB::table('countries')->insert(['countryCode' => "AF", 'countryName' =>" Afghanistan"]);
		DB::table('countries')->insert(['countryCode' => "AX", 'countryName' =>" Åland Islands"]);
		DB::table('countries')->insert(['countryCode' => "AL", 'countryName' =>" Albania"]);
		DB::table('countries')->insert(['countryCode' => "DZ", 'countryName' =>" Algeria"]);
		DB::table('countries')->insert(['countryCode' => "AS", 'countryName' =>" American Samoa"]);
		DB::table('countries')->insert(['countryCode' => "AD", 'countryName' =>" Andorra"]);
		DB::table('countries')->insert(['countryCode' => "AO", 'countryName' =>" Angola"]);
		DB::table('countries')->insert(['countryCode' => "AI", 'countryName' =>" Anguilla"]);
		DB::table('countries')->insert(['countryCode' => "AQ", 'countryName' =>" Antarctica"]);
		DB::table('countries')->insert(['countryCode' => "AG", 'countryName' =>" Antigua and Barbuda"]);
		DB::table('countries')->insert(['countryCode' => "AR", 'countryName' =>" Argentina"]);
		DB::table('countries')->insert(['countryCode' => "AM", 'countryName' =>" Armenia"]);
		DB::table('countries')->insert(['countryCode' => "AW", 'countryName' =>" Aruba"]);
		DB::table('countries')->insert(['countryCode' => "AU", 'countryName' =>" Australia"]);
		DB::table('countries')->insert(['countryCode' => "AT", 'countryName' =>" Austria"]);
		DB::table('countries')->insert(['countryCode' => "AZ", 'countryName' =>" Azerbaijan"]);
		DB::table('countries')->insert(['countryCode' => "BS", 'countryName' =>" Bahamas"]);
		DB::table('countries')->insert(['countryCode' => "BH", 'countryName' =>" Bahrain"]);
		DB::table('countries')->insert(['countryCode' => "BD", 'countryName' =>" Bangladesh"]);
		DB::table('countries')->insert(['countryCode' => "BB", 'countryName' =>" Barbados"]);
		DB::table('countries')->insert(['countryCode' => "BY", 'countryName' =>" Belarus"]);
		DB::table('countries')->insert(['countryCode' => "BE", 'countryName' =>" Belgium"]);
		DB::table('countries')->insert(['countryCode' => "BZ", 'countryName' =>" Belize"]);
		DB::table('countries')->insert(['countryCode' => "BJ", 'countryName' =>" Benin"]);
		DB::table('countries')->insert(['countryCode' => "BM", 'countryName' =>" Bermuda"]);
		DB::table('countries')->insert(['countryCode' => "BT", 'countryName' =>" Bhutan"]);
		DB::table('countries')->insert(['countryCode' => "BO", 'countryName' =>" Bolivia"]);
		DB::table('countries')->insert(['countryCode' => "BA", 'countryName' =>" Bosnia and Herzegovina"]);
		DB::table('countries')->insert(['countryCode' => "BW", 'countryName' =>" Botswana"]);
		DB::table('countries')->insert(['countryCode' => "BV", 'countryName' =>" Bouvet Island"]);
		DB::table('countries')->insert(['countryCode' => "BR", 'countryName' =>" Brazil"]);
		DB::table('countries')->insert(['countryCode' => "IO", 'countryName' =>" British Indian Ocean Territory"]);
		DB::table('countries')->insert(['countryCode' => "BN", 'countryName' =>" Brunei Darussalam"]);
		DB::table('countries')->insert(['countryCode' => "BG", 'countryName' =>" Bulgaria"]);
		DB::table('countries')->insert(['countryCode' => "BF", 'countryName' =>" Burkina Faso"]);
		DB::table('countries')->insert(['countryCode' => "BI", 'countryName' =>" Burundi"]);
		DB::table('countries')->insert(['countryCode' => "KH", 'countryName' =>" Cambodia"]);
		DB::table('countries')->insert(['countryCode' => "CM", 'countryName' =>" Cameroon"]);
		DB::table('countries')->insert(['countryCode' => "CV", 'countryName' =>" Cape Verde"]);
		DB::table('countries')->insert(['countryCode' => "KY", 'countryName' =>" Cayman Islands"]);
		DB::table('countries')->insert(['countryCode' => "CF", 'countryName' =>" Central African Republic"]);
		DB::table('countries')->insert(['countryCode' => "TD", 'countryName' =>" Chad"]);
		DB::table('countries')->insert(['countryCode' => "CL", 'countryName' =>" Chile"]);
		DB::table('countries')->insert(['countryCode' => "CN", 'countryName' =>" China"]);
		DB::table('countries')->insert(['countryCode' => "CX", 'countryName' =>" Christmas Island"]);
		DB::table('countries')->insert(['countryCode' => "CC", 'countryName' =>" Cocos (Keeling) Islands"]);
		DB::table('countries')->insert(['countryCode' => "CO", 'countryName' =>" Colombia"]);
		DB::table('countries')->insert(['countryCode' => "KM", 'countryName' =>" Comoros"]);
		DB::table('countries')->insert(['countryCode' => "CG", 'countryName' =>" Congo"]);
		DB::table('countries')->insert(['countryCode' => "CD", 'countryName' =>" Congo"]);
		DB::table('countries')->insert(['countryCode' => "CK", 'countryName' =>" Cook Islands"]);
		DB::table('countries')->insert(['countryCode' => "CR", 'countryName' =>" Costa Rica"]);
		DB::table('countries')->insert(['countryCode' => "CI", 'countryName' =>" Cote D'ivoire"]);
		DB::table('countries')->insert(['countryCode' => "HR", 'countryName' =>" Croatia"]);
		DB::table('countries')->insert(['countryCode' => "CU", 'countryName' =>" Cuba"]);
		DB::table('countries')->insert(['countryCode' => "CY", 'countryName' =>" Cyprus"]);
		DB::table('countries')->insert(['countryCode' => "CZ", 'countryName' =>" Czechia"]);
		DB::table('countries')->insert(['countryCode' => "DK", 'countryName' =>" Denmark"]);
		DB::table('countries')->insert(['countryCode' => "DJ", 'countryName' =>" Djibouti"]);
		DB::table('countries')->insert(['countryCode' => "DM", 'countryName' =>" Dominica"]);
		DB::table('countries')->insert(['countryCode' => "DO", 'countryName' =>" Dominican Republic"]);
		DB::table('countries')->insert(['countryCode' => "EC", 'countryName' =>" Ecuador"]);
		DB::table('countries')->insert(['countryCode' => "EG", 'countryName' =>" Egypt"]);
		DB::table('countries')->insert(['countryCode' => "SV", 'countryName' =>" El Salvador"]);
		DB::table('countries')->insert(['countryCode' => "GQ", 'countryName' =>" Equatorial Guinea"]);
		DB::table('countries')->insert(['countryCode' => "ER", 'countryName' =>" Eritrea"]);
		DB::table('countries')->insert(['countryCode' => "EE", 'countryName' =>" Estonia"]);
		DB::table('countries')->insert(['countryCode' => "ET", 'countryName' =>" Ethiopia"]);
		DB::table('countries')->insert(['countryCode' => "FK", 'countryName' =>" Falkland Islands (Malvinas)"]);
		DB::table('countries')->insert(['countryCode' => "FO", 'countryName' =>" Faroe Islands"]);
		DB::table('countries')->insert(['countryCode' => "FJ", 'countryName' =>" Fiji"]);
		DB::table('countries')->insert(['countryCode' => "FI", 'countryName' =>" Finland"]);
		DB::table('countries')->insert(['countryCode' => "FR", 'countryName' =>" France"]);
		DB::table('countries')->insert(['countryCode' => "GF", 'countryName' =>" French Guiana"]);
		DB::table('countries')->insert(['countryCode' => "PF", 'countryName' =>" French Polynesia"]);
		DB::table('countries')->insert(['countryCode' => "TF", 'countryName' =>" French Southern Territories"]);
		DB::table('countries')->insert(['countryCode' => "GA", 'countryName' =>" Gabon"]);
		DB::table('countries')->insert(['countryCode' => "GM", 'countryName' =>" Gambia"]);
		DB::table('countries')->insert(['countryCode' => "GE", 'countryName' =>" Georgia"]);
		DB::table('countries')->insert(['countryCode' => "DE", 'countryName' =>" Germany"]);
		DB::table('countries')->insert(['countryCode' => "GH", 'countryName' =>" Ghana"]);
		DB::table('countries')->insert(['countryCode' => "GI", 'countryName' =>" Gibraltar"]);
		DB::table('countries')->insert(['countryCode' => "GR", 'countryName' =>" Greece"]);
		DB::table('countries')->insert(['countryCode' => "GL", 'countryName' =>" Greenland"]);
		DB::table('countries')->insert(['countryCode' => "GD", 'countryName' =>" Grenada"]);
		DB::table('countries')->insert(['countryCode' => "GP", 'countryName' =>" Guadeloupe"]);
		DB::table('countries')->insert(['countryCode' => "GU", 'countryName' =>" Guam"]);
		DB::table('countries')->insert(['countryCode' => "GT", 'countryName' =>" Guatemala"]);
		DB::table('countries')->insert(['countryCode' => "GG", 'countryName' =>" Guernsey"]);
		DB::table('countries')->insert(['countryCode' => "GN", 'countryName' =>" Guinea"]);
		DB::table('countries')->insert(['countryCode' => "GW", 'countryName' =>" Guinea-bissau"]);
		DB::table('countries')->insert(['countryCode' => "GY", 'countryName' =>" Guyana"]);
		DB::table('countries')->insert(['countryCode' => "HT", 'countryName' =>" Haiti"]);
		DB::table('countries')->insert(['countryCode' => "HM", 'countryName' =>" Heard Island and Mcdonald Islands"]);
		DB::table('countries')->insert(['countryCode' => "HN", 'countryName' =>" Honduras"]);
		DB::table('countries')->insert(['countryCode' => "HK", 'countryName' =>" Hong Kong"]);
		DB::table('countries')->insert(['countryCode' => "HU", 'countryName' =>" Hungary"]);
		DB::table('countries')->insert(['countryCode' => "IS", 'countryName' =>" Iceland"]);
		DB::table('countries')->insert(['countryCode' => "IN", 'countryName' =>" India"]);
		DB::table('countries')->insert(['countryCode' => "ID", 'countryName' =>" Indonesia"]);
		DB::table('countries')->insert(['countryCode' => "IR", 'countryName' =>" Iran"]);
		DB::table('countries')->insert(['countryCode' => "IQ", 'countryName' =>" Iraq"]);
		DB::table('countries')->insert(['countryCode' => "IE", 'countryName' =>" Ireland"]);
		DB::table('countries')->insert(['countryCode' => "IM", 'countryName' =>" Isle of Man"]);
		DB::table('countries')->insert(['countryCode' => "IL", 'countryName' =>" Israel"]);
		DB::table('countries')->insert(['countryCode' => "IT", 'countryName' =>" Italy"]);
		DB::table('countries')->insert(['countryCode' => "JM", 'countryName' =>" Jamaica"]);
		DB::table('countries')->insert(['countryCode' => "JP", 'countryName' =>" Japan"]);
		DB::table('countries')->insert(['countryCode' => "JE", 'countryName' =>" Jersey"]);
		DB::table('countries')->insert(['countryCode' => "JO", 'countryName' =>" Jordan"]);
		DB::table('countries')->insert(['countryCode' => "KZ", 'countryName' =>" Kazakhstan"]);
		DB::table('countries')->insert(['countryCode' => "KE", 'countryName' =>" Kenya"]);
		DB::table('countries')->insert(['countryCode' => "KI", 'countryName' =>" Kiribati"]);
		DB::table('countries')->insert(['countryCode' => "KP", 'countryName' =>" Korea, North"]);
		DB::table('countries')->insert(['countryCode' => "KR", 'countryName' =>" Korea, South"]);
		DB::table('countries')->insert(['countryCode' => "KW", 'countryName' =>" Kuwait"]);
		DB::table('countries')->insert(['countryCode' => "KG", 'countryName' =>" Kyrgyzstan"]);
		DB::table('countries')->insert(['countryCode' => "LA", 'countryName' =>" Lao PDR"]);
		DB::table('countries')->insert(['countryCode' => "LV", 'countryName' =>" Latvia"]);
		DB::table('countries')->insert(['countryCode' => "LB", 'countryName' =>" Lebanon"]);
		DB::table('countries')->insert(['countryCode' => "LS", 'countryName' =>" Lesotho"]);
		DB::table('countries')->insert(['countryCode' => "LR", 'countryName' =>" Liberia"]);
		DB::table('countries')->insert(['countryCode' => "LY", 'countryName' =>" Libyan Arab Jamahiriya"]);
		DB::table('countries')->insert(['countryCode' => "LI", 'countryName' =>" Liechtenstein"]);
		DB::table('countries')->insert(['countryCode' => "LT", 'countryName' =>" Lithuania"]);
		DB::table('countries')->insert(['countryCode' => "LU", 'countryName' =>" Luxembourg"]);
		DB::table('countries')->insert(['countryCode' => "MO", 'countryName' =>" Macao"]);
		DB::table('countries')->insert(['countryCode' => "MK", 'countryName' =>" Macedonia"]);
		DB::table('countries')->insert(['countryCode' => "MG", 'countryName' =>" Madagascar"]);
		DB::table('countries')->insert(['countryCode' => "MW", 'countryName' =>" Malawi"]);
		DB::table('countries')->insert(['countryCode' => "MY", 'countryName' =>" Malaysia"]);
		DB::table('countries')->insert(['countryCode' => "MV", 'countryName' =>" Maldives"]);
		DB::table('countries')->insert(['countryCode' => "ML", 'countryName' =>" Mali"]);
		DB::table('countries')->insert(['countryCode' => "MT", 'countryName' =>" Malta"]);
		DB::table('countries')->insert(['countryCode' => "MH", 'countryName' =>" Marshall Islands"]);
		DB::table('countries')->insert(['countryCode' => "MQ", 'countryName' =>" Martinique"]);
		DB::table('countries')->insert(['countryCode' => "MR", 'countryName' =>" Mauritania"]);
		DB::table('countries')->insert(['countryCode' => "MU", 'countryName' =>" Mauritius"]);
		DB::table('countries')->insert(['countryCode' => "YT", 'countryName' =>" Mayotte"]);
		DB::table('countries')->insert(['countryCode' => "MX", 'countryName' =>" Mexico"]);
		DB::table('countries')->insert(['countryCode' => "FM", 'countryName' =>" Micronesia"]);
		DB::table('countries')->insert(['countryCode' => "MD", 'countryName' =>" Moldova"]);
		DB::table('countries')->insert(['countryCode' => "MC", 'countryName' =>" Monaco"]);
		DB::table('countries')->insert(['countryCode' => "MN", 'countryName' =>" Mongolia"]);
		DB::table('countries')->insert(['countryCode' => "ME", 'countryName' =>" Montenegro"]);
		DB::table('countries')->insert(['countryCode' => "MS", 'countryName' =>" Montserrat"]);
		DB::table('countries')->insert(['countryCode' => "MA", 'countryName' =>" Morocco"]);
		DB::table('countries')->insert(['countryCode' => "MZ", 'countryName' =>" Mozambique"]);
		DB::table('countries')->insert(['countryCode' => "MM", 'countryName' =>" Myanmar"]);
		DB::table('countries')->insert(['countryCode' => "NA", 'countryName' =>" Namibia"]);
		DB::table('countries')->insert(['countryCode' => "NR", 'countryName' =>" Nauru"]);
		DB::table('countries')->insert(['countryCode' => "NP", 'countryName' =>" Nepal"]);
		DB::table('countries')->insert(['countryCode' => "NL", 'countryName' =>" Netherlands"]);
		DB::table('countries')->insert(['countryCode' => "AN", 'countryName' =>" Netherlands Antilles"]);
		DB::table('countries')->insert(['countryCode' => "NC", 'countryName' =>" New Caledonia"]);
		DB::table('countries')->insert(['countryCode' => "NZ", 'countryName' =>" New Zealand"]);
		DB::table('countries')->insert(['countryCode' => "NI", 'countryName' =>" Nicaragua"]);
		DB::table('countries')->insert(['countryCode' => "NE", 'countryName' =>" Niger"]);
		DB::table('countries')->insert(['countryCode' => "NG", 'countryName' =>" Nigeria"]);
		DB::table('countries')->insert(['countryCode' => "NU", 'countryName' =>" Niue"]);
		DB::table('countries')->insert(['countryCode' => "NF", 'countryName' =>" Norfolk Island"]);
		DB::table('countries')->insert(['countryCode' => "MP", 'countryName' =>" Northern Mariana Islands"]);
		DB::table('countries')->insert(['countryCode' => "NO", 'countryName' =>" Norway"]);
		DB::table('countries')->insert(['countryCode' => "OM", 'countryName' =>" Oman"]);
		DB::table('countries')->insert(['countryCode' => "PK", 'countryName' =>" Pakistan"]);
		DB::table('countries')->insert(['countryCode' => "PW", 'countryName' =>" Palau"]);
		DB::table('countries')->insert(['countryCode' => "PS", 'countryName' =>" Palestinian Territory"]);
		DB::table('countries')->insert(['countryCode' => "PA", 'countryName' =>" Panama"]);
		DB::table('countries')->insert(['countryCode' => "PG", 'countryName' =>" Papua New Guinea"]);
		DB::table('countries')->insert(['countryCode' => "PY", 'countryName' =>" Paraguay"]);
		DB::table('countries')->insert(['countryCode' => "PE", 'countryName' =>" Peru"]);
		DB::table('countries')->insert(['countryCode' => "PH", 'countryName' =>" Philippines"]);
		DB::table('countries')->insert(['countryCode' => "PN", 'countryName' =>" Pitcairn"]);
		DB::table('countries')->insert(['countryCode' => "PL", 'countryName' =>" Poland"]);
		DB::table('countries')->insert(['countryCode' => "PT", 'countryName' =>" Portugal"]);
		DB::table('countries')->insert(['countryCode' => "PR", 'countryName' =>" Puerto Rico"]);
		DB::table('countries')->insert(['countryCode' => "QA", 'countryName' =>" Qatar"]);
		DB::table('countries')->insert(['countryCode' => "RE", 'countryName' =>" Reunion"]);
		DB::table('countries')->insert(['countryCode' => "RO", 'countryName' =>" Romania"]);
		DB::table('countries')->insert(['countryCode' => "RU", 'countryName' =>" Russian Federation"]);
		DB::table('countries')->insert(['countryCode' => "RW", 'countryName' =>" Rwanda"]);
		DB::table('countries')->insert(['countryCode' => "SH", 'countryName' =>" Saint Helena"]);
		DB::table('countries')->insert(['countryCode' => "KN", 'countryName' =>" Saint Kitts and Nevis"]);
		DB::table('countries')->insert(['countryCode' => "LC", 'countryName' =>" Saint Lucia"]);
		DB::table('countries')->insert(['countryCode' => "PM", 'countryName' =>" Saint Pierre and Miquelon"]);
		DB::table('countries')->insert(['countryCode' => "VC", 'countryName' =>" Saint Vincent and The Grenadines"]);
		DB::table('countries')->insert(['countryCode' => "WS", 'countryName' =>" Samoa"]);
		DB::table('countries')->insert(['countryCode' => "SM", 'countryName' =>" San Marino"]);
		DB::table('countries')->insert(['countryCode' => "ST", 'countryName' =>" Sao Tome and Principe"]);
		DB::table('countries')->insert(['countryCode' => "SA", 'countryName' =>" Saudi Arabia"]);
		DB::table('countries')->insert(['countryCode' => "SN", 'countryName' =>" Senegal"]);
		DB::table('countries')->insert(['countryCode' => "RS", 'countryName' =>" Serbia"]);
		DB::table('countries')->insert(['countryCode' => "SC", 'countryName' =>" Seychelles"]);
		DB::table('countries')->insert(['countryCode' => "SL", 'countryName' =>" Sierra Leone"]);
		DB::table('countries')->insert(['countryCode' => "SG", 'countryName' =>" Singapore"]);
		DB::table('countries')->insert(['countryCode' => "SK", 'countryName' =>" Slovakia"]);
		DB::table('countries')->insert(['countryCode' => "SI", 'countryName' =>" Slovenia"]);
		DB::table('countries')->insert(['countryCode' => "SB", 'countryName' =>" Solomon Islands"]);
		DB::table('countries')->insert(['countryCode' => "SO", 'countryName' =>" Somalia"]);
		DB::table('countries')->insert(['countryCode' => "ZA", 'countryName' =>" South Africa"]);
		DB::table('countries')->insert(['countryCode' => "GS", 'countryName' =>" South Georgia and The South Sandwich Islands"]);
		DB::table('countries')->insert(['countryCode' => "ES", 'countryName' =>" Spain"]);
		DB::table('countries')->insert(['countryCode' => "LK", 'countryName' =>" Sri Lanka"]);
		DB::table('countries')->insert(['countryCode' => "SD", 'countryName' =>" Sudan"]);
		DB::table('countries')->insert(['countryCode' => "SR", 'countryName' =>" Suriname"]);
		DB::table('countries')->insert(['countryCode' => "SJ", 'countryName' =>" Svalbard and Jan Mayen"]);
		DB::table('countries')->insert(['countryCode' => "SZ", 'countryName' =>" Swaziland"]);
		DB::table('countries')->insert(['countryCode' => "SE", 'countryName' =>" Sweden"]);
		DB::table('countries')->insert(['countryCode' => "CH", 'countryName' =>" Switzerland"]);
		DB::table('countries')->insert(['countryCode' => "SY", 'countryName' =>" Syrian Arab Republic"]);
		DB::table('countries')->insert(['countryCode' => "TW", 'countryName' =>" Taiwan"]);
		DB::table('countries')->insert(['countryCode' => "TJ", 'countryName' =>" Tajikistan"]);
		DB::table('countries')->insert(['countryCode' => "TZ", 'countryName' =>" Tanzania"]);
		DB::table('countries')->insert(['countryCode' => "TH", 'countryName' =>" Thailand"]);
		DB::table('countries')->insert(['countryCode' => "TL", 'countryName' =>" Timor-leste"]);
		DB::table('countries')->insert(['countryCode' => "TG", 'countryName' =>" Togo"]);
		DB::table('countries')->insert(['countryCode' => "TK", 'countryName' =>" Tokelau"]);
		DB::table('countries')->insert(['countryCode' => "TO", 'countryName' =>" Tonga"]);
		DB::table('countries')->insert(['countryCode' => "TT", 'countryName' =>" Trinidad and Tobago"]);
		DB::table('countries')->insert(['countryCode' => "TN", 'countryName' =>" Tunisia"]);
		DB::table('countries')->insert(['countryCode' => "TR", 'countryName' =>" Turkey"]);
		DB::table('countries')->insert(['countryCode' => "TM", 'countryName' =>" Turkmenistan"]);
		DB::table('countries')->insert(['countryCode' => "TC", 'countryName' =>" Turks and Caicos Islands"]);
		DB::table('countries')->insert(['countryCode' => "TV", 'countryName' =>" Tuvalu"]);
		DB::table('countries')->insert(['countryCode' => "UG", 'countryName' =>" Uganda"]);
		DB::table('countries')->insert(['countryCode' => "UA", 'countryName' =>" Ukraine"]);
		DB::table('countries')->insert(['countryCode' => "AE", 'countryName' =>" United Arab Emirates"]);
		DB::table('countries')->insert(['countryCode' => "GB", 'countryName' =>" United Kingdom"]);
		DB::table('countries')->insert(['countryCode' => "UM", 'countryName' =>" United States Minor Outlying Islands"]);
		DB::table('countries')->insert(['countryCode' => "UY", 'countryName' =>" Uruguay"]);
		DB::table('countries')->insert(['countryCode' => "UZ", 'countryName' =>" Uzbekistan"]);
		DB::table('countries')->insert(['countryCode' => "VU", 'countryName' =>" Vanuatu"]);
		DB::table('countries')->insert(['countryCode' => "VA", 'countryName' =>" Vatican City"]);
		DB::table('countries')->insert(['countryCode' => "VE", 'countryName' =>" Venezuela"]);
		DB::table('countries')->insert(['countryCode' => "VN", 'countryName' =>" Viet Nam"]);
		DB::table('countries')->insert(['countryCode' => "VG", 'countryName' =>" Virgin Islands, British"]);
		DB::table('countries')->insert(['countryCode' => "VI", 'countryName' =>" Virgin Islands, U.S."]);
		DB::table('countries')->insert(['countryCode' => "WF", 'countryName' =>" Wallis and Futuna"]);
		DB::table('countries')->insert(['countryCode' => "EH", 'countryName' =>" Western Sahara"]);
		DB::table('countries')->insert(['countryCode' => "YE", 'countryName' =>" Yemen"]);
		DB::table('countries')->insert(['countryCode' => "ZM", 'countryName' =>" Zambia"]);
		DB::table('countries')->insert(['countryCode' => "ZW", 'countryName' =>" Zimbabwe"]);

    }
}
