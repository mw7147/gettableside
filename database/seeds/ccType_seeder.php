<?php

use Illuminate\Database\Seeder;

class ccType_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ccType')->insert([
            'description' => 'No Credit Cards Accepted - Pay On Pickup',
            'sortOrder' => 1,
            'active' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ccType')->insert([
            'description' => 'Stripe Account',
            'sortOrder' => 3,
            'active' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ccType')->insert([
            'description' => 'getTableSide Account',
            'sortOrder' => 2,
            'active' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('ccType')->insert([
            'description' => 'Authorize.net Account',
            'sortOrder' => 4,
            'active' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
