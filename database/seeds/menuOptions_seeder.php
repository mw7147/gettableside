<?php

use Illuminate\Database\Seeder;

class menuOptions_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		DB::table('menuOptions')->insert([
    		'domainID' => 1,
            'active' => 1,            
            'name' => 'Bun Options',
            'description' => 'Bun Options for Burgers and Chicken Sandwiches',
            'maxOptions' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

		DB::table('menuOptions')->insert([
    		'domainID' => 1,
            'active' => 1,            
            'name' => 'Cheese Options',
            'description' => 'Cheese Options For Burgers and  Sandwiches',
            'maxOptions' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

		DB::table('menuOptions')->insert([
    		'domainID' => 1,
            'active' => 1,            
            'name' => 'Cook Options',
            'description' => 'Cook Temperature Options',
            'maxOptions' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptions')->insert([
            'domainID' => 1,
            'active' => 1,            
            'name' => 'Spice Options',
            'description' => 'Chinese Spice Option',
            'maxOptions' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuOptions')->insert([
            'domainID' => 1,
            'active' => 1,            
            'name' => 'Dressing Options',
            'description' => 'Chicken Wing Dressing Options',
            'maxOptions' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  



    }
}
