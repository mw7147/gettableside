<?php

use Illuminate\Database\Seeder;

class menuData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menuData')->insert([
    		'domainID' => 1,
            'active' => 1,
            'menuID' => 1,
            'menuCategoriesDataID' => 1,
            'menuItem' => 'Orange Chicken',
            'menuItemDescription' => "Spicy Orange Chicken, bamboo, nuts, other stuff you don't want to know about",
            'menuSidesID' => 2,
            'menuOptionsID' => 4,
            'menuAddOnsID' => 0,
            'portion1' => 'small',
            'price1' => 6.99,
            'portion2' => 'regular',
            'price2' => 9.99,
            'portion3' => null,
            'price3' => null,
            'portion4' => null,
            'price4' => null,
            'portion5' => null,
            'price5' => null,
            'portion6' => null,
            'price6' => null,
            'portion7' => null,
            'price7' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuData')->insert([
            'domainID' => 1,
            'active' => 1,
            'menuID' => 1,
            'menuCategoriesDataID' => 2,
            'menuItem' => 'Cheeseburger',
            'menuItemDescription' => '1/2 pound ground sirloin, cheese, choice of side',
            'menuSidesID' => 1,
            'menuOptionsID' => '1,2,3',
            'menuAddOnsID' => 1,
            'portion1' => 'Single',
            'price1' => 8.99,
            'portion2' => 'Double',
            'price2' => 11.99,
            'portion3' => null,
            'price3' => null,
            'portion4' => null,
            'price4' => null,
            'portion5' => null,
            'price5' => null,
            'portion6' => null,
            'price6' => null,
            'portion7' => null,
            'price7' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('menuData')->insert([
            'domainID' => 1,
            'active' => 1,
            'menuID' => 1,
            'menuCategoriesDataID' => 4,
            'menuItem' => 'Cheesecake',
            'menuItemDescription' => 'Handmade New York Style Cheesecake with fresh strawberries or blueberries',
            'menuSidesID' => 0,
            'menuOptionsID' => 0,
            'menuAddOnsID' => 0,
            'portion1' => 'regular',
            'price1' => 5.99,
            'portion2' => null,
            'price2' => null,
            'portion3' => null,
            'price3' => null,
            'portion4' => null,
            'price4' => null,
            'portion5' => null,
            'price5' => null,
            'portion6' => null,
            'price6' => null,
            'portion7' => null,
            'price7' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('menuData')->insert([
            'domainID' => 1,
            'active' => 1,
            'menuID' => 1,
            'menuCategoriesDataID' => 3,
            'menuItem' => 'Lasagna',
            'menuItemDescription' => "Mom's Homeade Lasagna - The Best In The West",
            'menuSidesID' => 1,
            'menuOptionsID' => 0,
            'menuAddOnsID' => 0,
            'portion1' => 'small',
            'price1' => 8.99,
            'portion2' => 'large',
            'price2' => 13.99,
            'portion3' => null,
            'price3' => null,
            'portion4' => null,
            'price4' => null,
            'portion5' => null,
            'price5' => null,
            'portion6' => null,
            'price6' => null,
            'portion7' => null,
            'price7' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);    

        DB::table('menuData')->insert([
            'domainID' => 1,
            'active' => 1,
            'menuID' => 1,
            'menuCategoriesDataID' => 2,
            'menuItem' => 'Chicken Wings',
            'menuItemDescription' => 'Beer battered chicken wings',
            'menuSidesID' => 0,
            'menuOptionsID' => 5,
            'menuAddOnsID' => 0,
            'portion1' => '6 Wings',
            'price1' => 8.99,
            'portion2' => '12 Wings',
            'price2' => 13.99,
            'portion3' => '18 Wings',
            'price3' => 18.99,
            'portion4' => '24 Wings',
            'price4' => 23.99,
            'portion5' => '36 Wings',
            'price5' => 29.99,
            'portion6' => null,
            'price6' => null,
            'portion7' => null,
            'price7' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuData')->insert([
            'domainID' => 1,
            'active' => 1,
            'menuID' => 1,
            'menuCategoriesDataID' => 3,
            'menuItem' => 'Brick Oven Pizza',
            'menuItemDescription' => 'Our Famous Brick Oven Pizza! Choose your toppings and enjoy!',
            'menuSidesID' => 0,
            'menuOptionsID' => 0,
            'menuAddOnsID' => 2,
            'portion1' => '10 Inch',
            'price1' => 8.99,
            'portion2' => '12 Inch',
            'price2' => 10.99,
            'portion3' => '14 Inch',
            'price3' => 12.99,
            'portion4' => '16 Inch',
            'price4' => 14.99,
            'portion5' =>  null,
            'price5' =>  null,
            'portion6' => null,
            'price6' => null,
            'portion7' => null,
            'price7' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);   


    }    
}
