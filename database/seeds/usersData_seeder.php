<?php

use Illuminate\Database\Seeder;

class usersData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('usersData')->insert([
            'usersID' => '1',
            'address1' => '28 Neon Moon Drive',
            'address2' => '',
            'city' =>   'Eutaw',
            'state' =>   'AL',
            'office' => '(682) 514-9010',
            'companyName' => 'Riverhouse IT',
            'title' => 'CEO',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]);

        DB::table('usersData')->insert([
            'usersID' => '2',
            'address1' => '504 Main Street',
            'address2' => '',
            'city' =>   'Roanoke',
            'state' =>   'TX',
            'office' => '(682) 514-9010',
            'companyName' => 'Hardwired Creative Technologies',
            'title' => 'CEO',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]);

        DB::table('usersData')->insert([
            'usersID' => '3',
            'address1' => '69 Main',
            'address2' => 'Suite A',
            'city' =>   'Denver',
            'state' =>   'CO',
            'office' => '(682) 514-9010',
            'companyName' => 'HDWRD Demo',
            'title' => 'Owner',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]);


    }
}
