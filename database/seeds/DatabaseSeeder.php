<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(iso_countries_seeder::class);
        $this->call(iso_us_states_seeder::class);
        $this->call(fileTypes_seeder::class);
        $this->call(domains_seeder::class);
        $this->call(roles_seeder::class);
        $this->call(user_seeder::class);
        $this->call(usersData_seeder::class);
        $this->call(siteStyle_seeder::class);
        $this->call(siteData_seeder::class);
        $this->call(types_seeder::class);
        $this->call(menu_seeder::class);
        $this->call(menuData_seeder::class);
        $this->call(menuCategories_seeder::class);
        $this->call(menuCategoriesData_seeder::class);
        $this->call(menuOptions_seeder::class);
        $this->call(menuOptionsData_seeder::class);
        $this->call(menuAddOns_seeder::class);
        $this->call(menuAddOnsData_seeder::class);
        $this->call(menuSides_seeder::class);
        $this->call(menuSidesData_seeder::class);
        $this->call(domainSiteConfig_seeder::class);
        $this->call(ccType_seeder::class);
        $this->call(domainPaymentConfig_seeder::class);
        $this->call(selectTimeDisplay_seeder::class);  
        $this->call(managerPIN_seeder::class);        
        $this->call(printers_seeder::class);        

    }
}
