<?php

use Illuminate\Database\Seeder;

class menuSides_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('menuSides')->insert([
    		'domainID' => 1,
            'name' => 'Side Items',
            'active' => 1,            
            'description' => 'Sides Selection for Burgers and Chicken Sandwiches',
            'numberSides' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuSides')->insert([
            'domainID' => 1,
            'active' => 1,            
            'name' => 'Select Rice',
            'description' => 'Rice Selection for Chinese food',
            'numberSides' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  


    }
}
