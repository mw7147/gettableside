<?php

use Illuminate\Database\Seeder;

class menuAddOns_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('menuAddOns')->insert([
    		'domainID' => 1,
            'active' => 1,            
            'name' => 'Condiments',
            'description' => 'Condiment Selection for Burgers and Chicken Sandwiches',
            'maxAddOns' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuAddOns')->insert([
            'domainID' => 1,
            'active' => 1,            
            'name' => 'Add Ons',
            'description' => 'Pizza Add Ons',
            'maxAddOns' => 5,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

    }
}
