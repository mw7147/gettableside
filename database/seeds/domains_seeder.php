<?php

use Illuminate\Database\Seeder;

class domains_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // insert first domain

       	DB::table('domains')->insert([
            'name' => 'ToGo Meals',
            'ownerID' => 3,
            'parentDomainID' => 1,
            'subdomain' => 'dev',
            'httpHost' => 'dev.togoh2iq.hwct',
            'sitename' => 'togomeals.biz',
            'active' => 'yes',
            'type' => 3,
            'backgroundPublic' => '/storage/backgroundImages/1/potRoast1920.jpg',
            'backgroundPrivate' => 'public/backgroundImages/1/potRoast1920.jpg',
            'logoPublic' => '/storage/logoImages/1/bubbas.png',
            'logoPrivate' => 'public/logoImages/1/bubbas.png',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

    }
}
