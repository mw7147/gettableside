<?php

use Illuminate\Database\Seeder;

class menuAddOnsData_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('menuAddOnsData')->insert([
    		'domainID' => 1,
            'menuAddOnsID' => 1,
            'addOnsName' => 'Lettuce',
            'addOnsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuAddOnsData')->insert([
    		'domainID' => 1,
            'menuAddOnsID' => 1,
            'addOnsName' => 'Tomato',
            'addOnsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuAddOnsData')->insert([
    		'domainID' => 1,
            'menuAddOnsID' => 1,
            'addOnsName' => 'Mustard',
            'addOnsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuAddOnsData')->insert([
    		'domainID' => 1,
            'menuAddOnsID' => 1,
            'addOnsName' => 'Siracha',
            'addOnsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  

        DB::table('menuAddOnsData')->insert([
    		'domainID' => 1,
            'menuAddOnsID' => 1,
            'addOnsName' => 'Mayo',
            'addOnsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);  


        DB::table('menuAddOnsData')->insert([
    		'domainID' => 1,
            'menuAddOnsID' => 1,
            'addOnsName' => 'Double Cheese',
            'addOnsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]); 

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Pepperoni',
            'addOnsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);     

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Sausage',
            'addOnsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Ham',
            'addOnsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Shredded Chicken',
            'addOnsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Mushrooms',
            'addOnsDefault' => null,
            'upCharge' => .25,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Black Olives',
            'addOnsDefault' => null,
            'upCharge' => .25,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Green Olives',
            'addOnsDefault' => null,
            'upCharge' => .25,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Onions',
            'addOnsDefault' => null,
            'upCharge' => .50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);           

        DB::table('menuAddOnsData')->insert([
            'domainID' => 1,
            'menuAddOnsID' => 2,
            'addOnsName' => 'Extra Cheese',
            'addOnsDefault' => null,
            'upCharge' => null,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);   


    }
}
