<?php

use Illuminate\Database\Seeder;

class user_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // insert record for each user level

       	DB::table('users')->insert([
            'role_auth_level' => '50',
            'domainID' => 1,
            'active' => 'yes',
            'fname' => 'HWCT',
            'lname' => 'Admin',
            'email' => 'admin@hardwiredcreative.com',
            'password' => bcrypt('admin'),
            'zipCode' => '80112',
            'countryCode' => 'US',
            'unformattedMobile' => '2053010265',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);


       	DB::table('users')->insert([
            'role_auth_level' => '40',
            'domainID' => 1,
            'active' => 'yes',
            'fname' => 'HWCT',
            'lname' => 'Staff',
            'email' => 'staff@hardwiredcreative.com',
            'password' => bcrypt('password'),
            'zipCode' => '80112',
            'countryCode' => 'US',
            'unformattedMobile' => '2053010265',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'role_auth_level' => '35',
            'domainID' => 1,
            'active' => 'yes',
            'fname' => 'Site',
            'lname' => 'Owner',
            'email' => 'owner@hdwrd.com',
            'password' => bcrypt('password'),
            'zipCode' => '80112',
            'countryCode' => 'US',
            'sex' => 'female',
            'birthDay' => 4,
            'birthMonth' => 7,
            'birthYear' => 1984,
            'rescueEmail' => 'mw7147@me.com',
            'unformattedMobile' => '2053010265',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
 

    }
}
