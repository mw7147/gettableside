@extends('admin.admin')

@section('content')

<h2>Create New Munu Item Tag</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Add the tag information as necessary.</li>
    <li>Press "Create New Tag" to save the new tag.</li>
    <li>Press "Cancel" or select a menu item to return to cancel.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Menu Item Tag</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/tags/{{Request::get('urlPrefix')}}/view" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Tags</a>
        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif


        @if(Session::has('mobileError'))
        <div class="alert alert-warning alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('mobileError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">
            <form name="newTag" method="POST" action="/tags/{{Request::get('urlPrefix')}}/new">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Tag Information</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Tag Name:</label>
                    <input type="text" placeholder="Tag Name" class="form-control" id="tagName" name="tagName" required value="{{ old('tagName') }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Tag Tier:</label>
                    <select class="form-control" name="tagTier">
                        <option value='1'>Tier 1</option>
                        <option value='2'>Tier 2</option>
                        <option value='3'>Tier 3</option>
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                        
                            {{ csrf_field() }}
                        <a href="/owner/systemusers/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Create New Tag</button>

                </div>

            </form>
        </div>
    </div>
</div>

<!-- Page Level Scripts -->
<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>


@endsection