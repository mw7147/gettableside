<!-- Groups View -->
@extends('admin.admin')

@section('content')
<meta http-equiv="Cache-control" content="no-cache">
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

    <h2>Group Administration</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Group Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Group Instructions</button>


    <ul class="instructions">
        <li>Create a new group click the "Create New Group" button. Type the group name in the designated input area and press Create New Group.</li>
        <li>To rename the group, click the "Name" button under Actions.</li>
        <li>To view,  edit or remove group members, click the "Edit Members" button.</li>
        <li>To add and remove group members, click the "Add Members" button.</li>
        <li>To view group communication history, click "History".
        <li>To delete the group, click the "Delete" button. This will delete all group and member data. Deleting a group will NOT remove the contact from your contacts - only delete the group and group memberships.
        <li>Basic members may create with 10 contacts each. Pro Members may create groups with 50 members each.</li>
    </ul>



<div class="ibox-content col-xs-12 col-sm-12">

<h3>Group List</h3><hr>

    <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <div class="clearfix"></div>

        @if(Session::has('groupSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('groupError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div>


        <button type="button" class="btn btn-success btn-xs m-b-lg" data-toggle="modal" data-target="#newGroupModal"><i class="fa fa-plus-square"></i> Create New Group</button>


    </div>

    <div class="clearfix"></div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                                              
                    @if (Request::get('authLevel') >= 35)
                    <th>Location</th>
                    @endif

                    <th>Group Name</th>
                    <th>Members</th>

                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($groups as $group)
                <tr>   
                    @if (Request::get('authLevel') >= 35)
                    <td>{{ $group->location }}</td>
                    @endif

                    <td>{{ $group->groupName }}</td>
                    
                    <td>{{ $group->groupCount }}</td>

                    <td><span class="visible-xs visible-sm visible-md"><br></span>
                        <button class="btn btn-success btn-xs w110 m-r-xs m-t-xs" onclick="editName({{ $group->id }})"><i class="fa fa-pencil m-r-xs"></i>Name</button>
                        <a class="btn btn-success btn-xs w110 m-r-xs m-t-xs" href="/groups/{{Request::get('urlPrefix')}}/viewmembers/{{ $group->id }}"><i class="fa fa-users m-r-xs"></i>View</a>
                        <a class="btn btn-success btn-xs w110 m-r-xs m-t-xs" href="/groups/{{Request::get('urlPrefix')}}/addmembers/{{ $group->id }}"><i class="fa fa-user-plus m-r-xs"></i>Add / Remove</a>
                        <a href="/groups/{{Request::get('urlPrefix')}}/duplicate/{{$group->id}}" class="btn btn-success btn-xs w110 m-r-xs m-t-xs"><i class="fa fa-files-o m-r-xs"></i>Duplicate</a>
                        <a class="btn btn-danger btn-xs w110 m-t-xs" href="/groups/{{Request::get('urlPrefix')}}/delete/{{$group->id }}" onclick="javascript:return confirm('Are you sure you want to delete this group? All group data will be permanently deleted.')"><i class="fa fa-remove m-r-xs"></i>Delete</a> 
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>




<!-- Modals -->

<!-- New Group Name -->

<div class="modal fade" id="newGroupModal" tabindex="-1" role="dialog" aria-labelledby="newGroupModal">
  <div class="modal-dialog modal-sm" role="document">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Create New Group</h4>
        
        </div>
      
        <form name="newGroupForm" method="POST" action="/groups/{{Request::get('urlPrefix')}}/view">
        
            <div class="modal-body form-group">
                <small><label>Group Name</label></small><br>

                <input type="text" class="form-control" name="newGroupName" placeholder="New Group Name" value="">

                @if (Request::get('authLevel') >= 35)
                <div class="m-t-sm m-b-xs">
                    <small><label>Location:</small></label><br>
                    <select class="form-control" name="domainID">
                        @foreach ($locations as $location)
                        <option value='{{ $location->id }}'>{{ $location->name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif

                @if (Request::get('authLevel') < 35)
                <input type="hidden" name="domainID" value="{{$domain->id}}">
                @endif

            </div>

            <div class="modal-footer">
        
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Create New Group</button>
      
            </div>

            {{ csrf_field() }}

        </form>
    </div>

  </div>
</div>


<!-- Edit Group Name -->

<div class="modal fade" id="editGroupModal" tabindex="-1" role="dialog" aria-labelledby="editGroupModal">
  <div class="modal-dialog modal-sm" role="document">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Edit Group Name</h4>
        
        </div>
      
        <form name="newGroupForm" method="POST" action="/groups/{{Request::get('urlPrefix')}}/editname">
        
            <div class="modal-body form-group">
                <small><label>Group Name</label></small><br>

                <input type="text" id="newGroupName" class="form-control" name="newGroupName" placeholder="New Group Name" value="">


            </div>

            <div class="modal-footer">
                
                {{ csrf_field() }}
                <input type="hidden" id="groupID" name="groupID" value="">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Edit Group Name</button>
      
            </div>

        </form>
    </div>

  </div>
</div>





<!-- Page-Level Scripts -->




<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            responsive: true,
            order: [[ 0, "asc" ], [1, "asc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'GroupsList'},
                {extend: 'pdf', title: 'GroupsList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });


    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });


function editName(id) {

    $('#groupID').val(id);
    
    var fd = new FormData();    
    fd.append('id', id);

    $.ajax({
        url: "/api/v1/getGroupName",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {

                $("#newGroupName").val('New Group Name');
                $('#editGroupModal').modal('show');
                    
            } else {

                $("#newGroupName").val(jsondata["groupName"]);
                $('#editGroupModal').modal('show');


        }}
    });
}

function duplicateGroup(id) {

    $('#groupID').val(id);
    
    var fd = new FormData();    
    fd.append('id', id);

    $.ajax({
        url: "/api/v1/duplicateGroup",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {

                alert("There was an error duplicating your group - please try again.");
                    
            } else {

                alert("The group "+ jsondata["newGroupName"] + " was successfully created.");
                location.reload(true);

        }}
    });
}



</script>

 

@endsection