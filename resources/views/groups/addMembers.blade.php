<!-- Groups View -->
@extends('admin.admin')

@section('content')

    <!-- Page-Level CSS -->


<!-- center checkboxes -->
<style>
    input[type=checkbox] {
        margin: 0 auto;
        display: block;
    }
</style>

    <h2>Group Membership Administration</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Add / Remove Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Add / Remove Instructions</button>
    <div class="clearfix"></div>

    <ul class="instructions col-xs-11 col-sm-10 col-md-9">
        <li>Basic members may create with 10 contacts each. Pro Members may create groups with 50 members each.</li>
        <li>To add a contact to the group, check the checkbox next to the name.</li>
        <li>Changes to the group are real time changes.</li>
        <li>To remove a contact from the goup, uncheck the checkbox next to the name.</li>
        <li>Only contacts with checkboxes checked are members of the group.</li>
        <li>Removing the contact from the group does not delete the contact. It only removes the contact from the group.</li>
        <li>If a "add new group member campaign" is active for this group, the newly selected contact(s) will receive the campaign email as they are added to the group. If you wish to stop the campaign email, deactivate the campaign while you add and remove group memebers. Once you have completed your changes remember to reactivate the "add new group member campaign" if desired.</li>

    </ul>

<div class="ibox-content col-xs-12 col-sm-12">

<h3>{{ $group->groupName }} Member List</h3><hr>

        <a href="javascript:history.go(-1)" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/groups/{{Request::get('urlPrefix')}}/view" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-users m-r-xs"></i>Groups</a>
    <div class="clearfix"></div>

        @if(Session::has('groupSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('groupError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupError') }}
            </div>
            <div class="clearfix"></div>
        @endif
        

<div>


</div>

    <div class="col-xs-12 col-sm 12 col-md-10 col-lg-8">

        @if(Session::has('newGroupMemberCampaign'))
            <div class="alert alert-warning alert-dismissable col-md-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('newGroupMemberCampaign') }}
            </div>
            <div class="clearfix"></div>
        @endif

        <div class="pull-right">
            {{ $contacts->links() }}
        </div>

        <div>

            <p class="pagination bg-muted p-xxs pull-right m-r-sm">Page {{ $contacts->currentPage() }} of {{ $contacts->lastPage() }}</p>
            <p class="pagination bg-muted p-xxs pull-left"> <span id="groupCount">{{ $groupCount }}</span> Selected of {{ $contacts->total() }} Total Contacts</p>

        </div>


    </div>

    <div class="clearfix"></div>

    <div class="table-responsive col-xs-12 col-sm 12 col-md-10 col-lg-8">
    <table class="table table-striped">

        <thead>
            <tr>
                <th class="text-center w90">Member</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($contacts as $contact)
            <tr>
                <td>
                    <input name="groupMember" id="groupMember" type="checkbox" class="checkbox checkbox-success {{ $contact->id }}" onclick="groupMbr({{ $contact->id }})"      
                    @if (in_array($contact->id, $groupContactsID)) checked @endif>   
                </td>
                <td>
                    {{ $contact->fname }} {{ $contact->lname }}
                </td>

                <td>
                   {{ $contact->mobile }}
                </td>
                <td>
                    {{ $contact->email }}
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    </div>

    <div class="clearfix"></div>

    <div class="col-xs-12 col-sm 10 col-md-8">

        <div class="pull-right">
            {{ $contacts->links() }}
        </div>

        <div>

            <p class="pagination bg-muted p-xxs pull-right m-r-sm">Page {{ $contacts->currentPage() }} of {{ $contacts->lastPage() }}</p>

        </div>

    </div>
</div>

<!-- Page-Level Scripts -->


<script>

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });



function groupMbr(id) {

    var myurl = "/api/v1/deletegroupmember";
    var fd = new FormData();    
    fd.append('contactID', id);
    fd.append('groupID', {{ $group->id }});
    fd.append('domainID', {{ $group->domainID }});

    if ($('.'+id).prop('checked')) { 
        myurl="/api/v1/addgroupmember";
    }
 
        $.ajax({
            url: myurl,
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {

                    $('#groupCount').html(jsondata["groupCount"]);
                    $('#groupCount1').html(jsondata["groupCount"]);

                } else {

                    $('#groupCount').html(jsondata["groupCount"]);
                    $('#groupCount1').html(jsondata["groupCount"]);

            }}
        });


}

</script>

@endsection