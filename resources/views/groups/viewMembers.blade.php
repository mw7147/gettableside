<!-- Groups View -->
@extends('admin.admin')

@section('content')

    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>


<!-- center checkboxes -->
<style>
    input[type=checkbox] {
        margin: 0 auto;
        display: block;
    }
</style>


    <h2>Group Membership Administration</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Add / Remove Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Add / Remove Instructions</button>


    <ul class="instructions">
        <li>To add a contact to the group, check the checkbox next to the name.</li>
        <li>Changes to the group are real time changes.</li>
        <li>To remove a contact from the goup, uncheck the checkbox next to the name.</li>
        <li>Only contacts with checkboxes checked are members of the group.</li>
        <li>Removing the contact from the group does not delete the contact. It only removes the contact from the group.</li>
    </ul>



<div class="ibox-content col-xs-12 col-sm-12">

<h3>{{ $group[0]->groupName }} Member List</h3><hr>

    <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
    <a href="/groups/{{Request::get('urlPrefix')}}/view" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-users m-r-xs"></i>Groups</a>
    <div class="clearfix"></div>

        @if(Session::has('groupSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('groupError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div class="clearfix"></div>

<div>


</div>

    <div class="clearfix"></div>

    <div class="table-responsive col-xs-12 col-sm 12 col-md-10 col-lg-8">
        <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($groupContacts as $contact)
            <tr>
                <td>{{ $contact->fname }}</td>
                <td>{{ $contact->lname }}</td>
                <td>{{ $contact->mobile }}</td>
                <td>{{ $contact->email }}</td>
                <td><a class="btn btn-success btn-xs w90 m-r-xs" href="/contacts/{{Request::get('urlPrefix')}}/edit/{{ $contact->id}}"><i class="fa fa-pencil"></i> View/Edit</a>
                  <!--  <a class="btn btn-success btn-xs w90 m-r-xs" href="/reports/customers/textmessages/mobile/{{ $contact->id}}"><i class="fa fa-folder-open"></i> History</a> -->
                    <a class="btn btn-danger btn-xs w90" href="/groups/{{Request::get('urlPrefix')}}/delete/{{ $groupID }}/{{$contact->id}}" onclick="javascript:return confirm('Are you sure you want to remove this contact from the Group?')"><i class="fa fa-remove"></i> Remove</a> 
                </td>
                
            </tr>
            @endforeach

        </tbody>
    </table>
    </div>

    <div class="clearfix"></div>

</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                dom: '<"html5buttons"B>lTfgitp',
                order: [[ 0, "asc" ]],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'CustomerContactList'},
                    {extend: 'pdf', title: 'CustomerContactList'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>



<script>


    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });


function groupMbr(id) {
    
    var myurl = "/api/v1/deleteGroupMember";
    var fd = new FormData();    
    fd.append('contactID', id);
    fd.append('groupID', {{ $group[0]->id }});

    if ($('.'+id).prop('checked')) { myurl="/api/v1/addGroupMember"; }
 
        $.ajax({
            url: myurl,
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {

                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {

                    $('#groupCount').html(jsondata["groupCount"]);
                    $('#groupCount1').html(jsondata["groupCount"]);

                        
                } else {

                    $('#groupCount').html(jsondata["groupCount"]);
                    $('#groupCount1').html(jsondata["groupCount"]);


            }}
        });

</script>

 

@endsection