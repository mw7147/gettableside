<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="TokinMail">
    <meta name="author" content="Hardwired Creative Technologies">

    <title>TokinMail</title>

    <!-- rhit Core CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
    <link href="{{ mix('css/rhit_admin.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <!--<link rel="icon" type="image/png" href="favicon.png">-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- HWCT Core JavaScript -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/rhit_admin.js') }}"></script>
    <script src="//cdn.jsdelivr.net/jquery.metismenu/2.7.0/metisMenu.min.js"></script>
</head>

<body class="fixed-sidebar">
 
    @include('user.userNavbar')

    <div id="page-wrapper" class="gray-bg">

        @include('user.userTopnav')

        @yield('content')

        <div class="clearfix m-t-xl m-b-lg"></div>
        
        <div class="footer">
            <div class="pull-right">
               <a href="http://www.hardwiredcreative.com" target="_blank">Powered by Hardwired</a>
            </div>
            <div>&copy; {{ date("Y", time()) }} TokinMail</div>
        </div>

    </div>

    <!-- Scripts -->
    

</body>
</html>