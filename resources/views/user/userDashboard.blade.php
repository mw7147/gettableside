@extends('user.userAdmin')



@section('content')
<style>
    .dashicon:hover { opacity: .75;}
    .dashicon-white:hover { opacity: .62;}

    .widget {min-height: 210px;}
    .db-blue-bg {color: white;}
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center m-t-lg">
                <h1>
                    TokinMail
                </h1>
                <small>
                    User Dashboard
                </small>
            </div>
        </div>
    </div>


    <div class="row m-t-md">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Inbox</span>
                    <h5>Unread</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">10</h1>
                    <small>Unread Emails In Inbox</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Sent</span>
                    <h5>Sent</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">22</h1>
                    <small>Emails Sent This Month</small>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">TokinMail</span>
                    <h5>New Offers</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">3</h1>
                    <small>New Offers Received</small>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="ibox float-e-margins">
                <img class="img img-responsive" src="/storage/coupons/1/greenrush.png" style="max-height: 133px; margin-top: 4px; margin-left: 20%;">
            </div>
        </div>


    </div>


    <div class="row m-t-xs m-b-lg">
        
    <hr>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 pull-right hidden-xs" >
        
        <div class="col-md-12" >
            <h4>TokinMail Sponsor</h4>
            <iframe width="480" height="270" src="https://www.youtube.com/embed/vOYMe9OIuUA" frameborder="0" allowfullscreen></iframe><br>
            <a class="btn btn-info btn-small" href="http://LightShade.com" target="_blank">Visit LightShade Website</a>
        </div>
        <div class="col-md-12 m-t-lg" >
            <h4>TokinMail Sponsor</h4>
            <img class="img img-responsive" src="/storage/coupons/1/tokintext480.jpg"<br>
            <a class="btn btn-info btn-small" href="http://cloud.tokintext.com" target="_blank">Visit TokinText Website</a>
        </div>
        <div class="col-md-12 m-t-lg" >
            <h4>TokinMail Sponsor</h4>
            <iframe width="480" height="270" src="https://www.youtube.com/embed/IQtG5lu0_5w?ecver=1" frameborder="0" allowfullscreen></iframe>
        
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 pull-right hidden-sm hidden-md hidden-lg" >
        
        <div class="col-md-12" >
            <h4>TokinMail Sponsor</h4>
            <iframe width="400" height="225" src="https://www.youtube.com/embed/vOYMe9OIuUA" frameborder="0" allowfullscreen></iframe><br>
            <a class="btn btn-info btn-small" href="http://LightShade.com" target="_blank">Visit LightShade Website</a>
        </div>
        <div class="col-md-12 m-t-lg" >
            <h4>TokinMail Sponsor</h4>
            <img class="img img-responsive" src="/storage/coupons/1/tokintext400.jpg"<br>
            <a class="btn btn-info btn-small" href="http://cloud.tokintext.com" target="_blank">Visit TokinText Website</a>
        </div>
        <div class="col-md-12 m-t-md m-b-lg" >
            <h4>TokinMail Sponsor</h4>
            <iframe width="400" height="225" src="https://www.youtube.com/embed/WeYsTmIzjkw?ecver=1" frameborder="0" allowfullscreen></iframe><br>
            <a class="btn btn-info btn-small" href="https://www.amazon.com/Because-I-Got-High-Explicit/dp/B001O3YD0A/ref=sr_1_1?ie=UTF8&qid=1487193155&sr=8-1&keywords=afroman+because+i+got+high" target="_blank">Buy AfroMan on Amazon</a>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 pull-left" >

        <a href="/webmail/compose" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget blue-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-envelope fa-4x"></i>
                    <h1 class="m-xs">Email</h1>
                    <h3 class="font-bold no-margins">
                        Send Email
                    </h3>
                </div>
            </div>
        </a>

        <a href="/webmail/inbox" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget blue-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-envelope fa-4x"></i>
                    <h1 class="m-xs">Email</h1>
                    <h3 class="font-bold no-margins">
                        View Emails
                    </h3>
                </div>
            </div>
        </a>

        <a href="/textmsgs/sendtext" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget navy-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-commenting fa-4x"></i>
                    <h1 class="m-xs">Text</h1>
                    <h3 class="font-bold no-margins hidden-xs">
                    Send Text
                    </h3>
                </div>
            </div>
        </a>

        <a href="/textmsgs/messages/inbox" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget navy-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-commenting fa-4x"></i>
                    <h1 class="m-xs">Text</h1>
                    <h3 class="font-bold no-margins">
                        View Texts
                    </h3>
                </div>
            </div>
        </a>

        <a href="/contacts/view" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget yellow-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-envelope fa-4x"></i>
                    <h1 class="m-xs">Contacts</h1>
                    <h3 class="font-bold no-margins">
                       Manage Contacts
                    </h3>
                </div>
            </div>
        </a>
       
        <a href="/groups/view" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget yellow-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-commenting fa-4x"></i>
                    <h1 class="m-xs">Groups</h1>
                    <h3 class="font-bold no-margins hidden-xs">
                   Manage Groups
                    </h3>
                </div>
            </div>
        </a>



        <a href="#" class="col-xs-12 col-sm-6 col-md-6 dashicon" onclick="comingSoon();">
            <div class="widget db-blue-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-shopping-bag fa-4x"></i>
                    <h1 class="m-xs">Offers</h1>
                    <h3 class="font-bold no-margins">
                        View Offers
                    </h3>
                </div>
            </div>
        </a>

        <a href="#" class="col-xs-12 col-sm-6 col-md-6 dashicon" onclick="comingSoon();">
            <div class="widget db-blue-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-bullhorn fa-4x"></i>
                    <h1 class="m-xs">Community</h1>
                    <h3 class="font-bold no-margins">
                        Join Conversations
                    </h3>
                </div>
            </div>
        </a>

        <a href="/user/myinfo" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget lt-yellow-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-star fa-4x"></i>
                    <h1 class="m-xs">My Info</h1>
                    <h3 class="font-bold no-margins hidden-xs">
                        My Information
                    </h3>
                </div>
            </div>
        </a>
        
        <a href="/attache/view/home" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget lt-yellow-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-briefcase fa-4x"></i>
                    <h1 class="m-xs">Attaché</h1>
                    <h3 class="font-bold no-margins hidden-xs">
                        Attaché Folders
                    </h3>
                </div>
            </div>
        </a>



        <a href="/settings/dashboard" class="col-xs-12 col-sm-6 col-md-6 dashicon">
            <div class="widget white-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-cog fa-4x"></i>
                    <h1 class="m-xs">Settings</h1>
                    <h3 class="font-bold no-margins">
                        Site Settings
                    </h3>
                </div>
            </div>
        </a>

        <a href="#" class="col-xs-12 col-sm-6 col-md-6 dashicon" onclick="comingSoon();">
            <div class="widget white-bg p-lg text-center">
                <div class="m-b-md">
                    <i class="fa fa-question-circle fa-4x"></i>
                    <h1 class="m-xs">Help</h1>
                    <h3 class="font-bold no-margins">
                        Help
                    </h3>
                </div>
            </div>
        </a>

    </div>


</div>

<script>

function comingSoon() {

    alert("Under Development - Coming Soon");
    return false;

}


</script>


@endsection