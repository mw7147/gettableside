<!-- NAVBAR -->

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold lite-color font15px">{{ Auth::user()->fname }} {{ Auth::user()->lname }}</strong>
                             </span>

                        <hr class="hr-small"/>

                       <span class="text-muted text-xs block">TokinMail</span>
                            
                    </div>
                    <div class="logo-element">
                        TokinMail
                    </div>
                </li>

                    <li @if($breadcrumb[0] == 'dashboard')class="active" @endif>
                        <a href="/user/dashboard"><i class="fa fa-dashboard iconmenu"></i> <span class="nav-label">Dashboard</span></a><span class="fa arrow"></span></a>
                    </li>

                   <li @if($breadcrumb[0] == 'email')class="active" @endif>
                        <a href="#"><i class="fa fa-envelope iconmenu"></i> <span class="nav-ticket">Email</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'Compose')class="active"@endif >
                                <a href="/webmail/compose" alt="Compose Email">Compose</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Inbox')class="active"@endif >
                                <a href="/webmail/inbox" alt="Email Inbox">Inbox</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Sent')class="active"@endif>
                                <a href="/webmail/sent" alt="Sent Folder">Sent</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Drafts')class="active"@endif >
                                <a href="/webmail/drafts" alt="Drafts Folder">Drafts</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Junk')class="active"@endif >
                                <a href="/webmail/junk" alt="Junk Folder">Junk</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Trash')class="active"@endif>
                                <a href="/webmail/trash" alt="Trash Folder">Trash</a>
                            </li>

                        </ul>
                    </li>

                   <li @if($breadcrumb[0] == 'text')class="active" @endif>
                        <a href="#"><i class="fa fa-commenting iconmenu"></i> <span class="nav-ticket">Text</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'compose')class="active"@endif >
                                <a href="/textmsgs/sendtext" alt="Compose Text Message">Compose</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Inbox')class="active"@endif >
                                <a href="/textmsgs/messages/inbox" alt="Text Messages Received">Received</a>
                            </li>
                            <li @if($breadcrumb[1] == 'Sent')class="active"@endif>
                                <a href="/textmsgs/messages/sent" alt="Text Messages Sent">Sent</a>
                            </li>
                        </ul>
                    </li>

                   <li @if($breadcrumb[0] == 'address')class="active" @endif>
                        <a href="#"><i class="fa fa-address-book iconmenu"></i> <span class="nav-ticket">Address Book</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'contacts')class="active"@endif >
                                <a href="/contacts/view" alt="Contacts">Contacts</a>
                            </li>
                            <li @if($breadcrumb[1] == 'groups')class="active"@endif>
                                <a href="/groups/view" alt="Groups">Groups</a>
                            </li>
                        </ul>
                    </li>


                    <li @if($breadcrumb[0] == 'offers')class="active" @endif>
                        <a href=""><i class="fa fa-shopping-bag iconmenu"></i> <span class="nav-ticket">Offers</span><span class="fa arrow"></span></a>
                    </li>

                    <li>
                        <a href=""><i class="fa fa-bullhorn iconmenu"></i> <span class="nav-ticket">Community</span><span class="fa arrow"></span></a>
                    </li>
 

                    <li>
                        <a href="/user/myinfo"><i class="fa fa-star iconmenu"></i> <span class="nav-ticket">My Information</span><span class="fa arrow"></span></a>
                    </li>
 
                    <li @if($breadcrumb[0] == 'attache')class="active" @endif>
                        <a href="/attache/view/home"><i class="fa fa-briefcase iconmenu"></i> <span class="nav-ticket">Attache</span><span class="fa arrow"></span></a>
                    </li>

                    <li @if($breadcrumb[0] == 'offers')class="active" @endif>
                        <a href="/settings/dashboard"><i class="fa fa-cog iconmenu"></i> <span class="nav-ticket">Settings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'signature')class="active"@endif >
                                <a href="" alt="Contacts">My Signature</a>
                            </li>
                            <li @if($breadcrumb[1] == 'password')class="active"@endif>
                                <a href="/settings/resetpassword/{{ Auth::user()->id }}" alt="Groups">Reset Password</a>
                            </li>
                            <li @if($breadcrumb[1] == 'display')class="active"@endif>
                                <a href="" alt="Groups">Display Preferences</a>
                            </li>
                            <li @if($breadcrumb[1] == 'accounts')class="active"@endif>
                                <a href="" alt="Groups">Display Preferences</a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-question-circle iconmenu"></i> <span class="nav-ticket">Help</span><span class="fa arrow"></span></a>
                    </li>




                    <li>
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>  Logout</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        </li>

                    </li>


            </ul>

        </div>
    </nav>