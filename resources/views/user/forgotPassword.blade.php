@extends('orders.iframe')

@section('content')
<!-- Page-Level CSS -->

@include('orders.cssUpdates')

<style>

input.faChkRnd, input.faChkSqr {
  visibility: hidden;
}

@-moz-document url-prefix() { 

input.faChkRnd, input.faChkSqr {
  visibility: visible;
}

 }

.faChkSqr, .faChkRnd {
      margin-left: 0px !important;
}

input.faChkRnd:checked:after, input.faChkRnd:after,
input.faChkSqr:checked:after, input.faChkSqr:after {
  visibility: visible;
  font-family: FontAwesome;
  font-size:21px;height: 17px; width: 17px;
  position: relative;
  top: -3px;
  left: 0px;
  background-color: #FFF;
    color: {{ $styles->bannerButtonBorderColor }} !important;
  display: inline-block;
}

input.faChkRnd:checked:after {
  content: '\f058';
}

input.faChkRnd:after {
  content: '\f10c';
}

input.faChkSqr:checked:after {
  content: '\f14a';
}

input.faChkSqr:after {
  content: '\f096';
}

@media (max-width: 768px) {

  #page-wrapper {
      min-height: 1300px !important;
  }
}

@media (min-width: 769px) and (max-width: 991px) {

  #page-wrapper {
      min-height: 1150px !important;
  }
}

@media (min-width: 992px) {

  #page-wrapper {
      min-height: 950px !important;
  }
}

</style>

<div class="clearfix"></div>
   

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl" style="margin-top: 70px;">
        


    <div class="ibox float-e-margins">

      @if($domain['type'] <= 4)

      <span class="pull-right locationAddress">
      <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
      @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
      @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
      @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
      @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
      </span>


      <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

    @endif  

    <div class="clearfix"></div>
        
      <div class="ibox-title" style="border: none; margin-top: 4px;">
        <h5><i class="fa fa-user-secret m-r-xs"></i>Forgot Password</h5>
        </div>
        
        
        <div class="ibox-content" style="padding-bottom: 165px">

        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if(session()->has('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}
          </div>
        @endif

        <form name="forgotPassword" method="POST" id="forgotPassword" action="/forgot/password">
          {{ csrf_field() }}
            <div class="col-md-6 col-sm-12 col-xs-12 m-t-sm">
                <label>User Email<sup class="text-danger m-l-xs">*</sup></label>
                <input class="form-control" type="email" id="email" name="email" value="" required placeholder="Email">
            </div>


            <div class="col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2 col-sm-6 col-sm-offset-2 col-xs-10 col-xs-pull-2 m-t-md">

               
                
                <div id="returnMenu">
                  
                  <button type="submit" class="btn banner-button m-t-sm" style="min-width: 150px;" name="continue" id="continue" value="true"><i class="fa fa-check m-r-sm"></i>Continue</button>
                  
                </div>


            </div>

        </form>


        </div>
    </div>
</div>


<!-- Page Level Scripts -->
<script>

</script>




@endsection