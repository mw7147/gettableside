@extends('user.userAdmin')



@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.tokeninput/1.6.0/styles/token-input-mac.css">
<style>
.token-input-list-mac { border: 1px solid #e5e6e7 !important; }
.fa-8x {font-size: 8em;}
</style>

<h1>Send Mail Message</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-10 m-b-md instructions">
    <li>Select the group you wish to receive the message.</li>
    <li>If you wish to use your signature, check the "Use My Signature" box.</li>
    <li>Construct your email. If you wish the Text Only version to differ from the html version, type in your text only email. Otherwise a text only version will be automatically created from the html version when sending.</li>
    <li>Click "Try Test" to send a test to your registered email address.</li>
    <li>Click Send Email to queue the messages for sending.</li>
</ul>




<div class="ibox send">
{{ Session::get('attachmentPublic') }}
    <div class="ibox-title">
        <h5><i class="m-r-sm">Send Email Message</i></h5>
    </div>
        
    <div class="ibox-content">
        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/user/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10" id="templatePanel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <i class="fa fa-envelope m-r-xs"></i><span id="templateName">Send Email Message</span>
                </div>
                <div class="panel-body">

                <div class="clearfix"></div>
                
                @if(Session::has('fileError'))
                    <div class="alert alert-danger alert-dismissable col-xs-10 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('fileError') }}
                    </div>
                    <div class="clearfix"></div>
                @endif

                @if(Session::has('QuickMailTest'))
                    <div class="alert alert-success alert-dismissable col-xs-10 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('QuickMailTest') }}
                    </div>
                    <div class="clearfix"></div>
                @endif




                <form name="sendTemplate" id="sendTemplate" method="POST" action="/customers/quickmailconfirm" enctype="multipart/form-data">

                 <div class="input-group m-b col-xs-12 col-sm-11 col-md-10 col-lg-10" id="sendTo">

                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
                        <small><strong>Send To:</strong></small><br>
                        <input type="checkbox" name="sendToIndividual" id="sendToIndividual" class="checkbox checkbox-primary checkbox-inline" onclick="sendIndividual()" value="yes">
                        <label class="m-l-xs font-normal">Send To Contacts</small></label>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
                        <small><strong>Send To:</strong></small><br>
                        <input type="checkbox" name="sendToGroup" id="sendToGroup" class="checkbox checkbox-primary checkbox-inline" onclick="sendGroup()" value="yes">
                        <label class="m-l-xs font-normal">Send To Group</small></label>
                    </div>

                </div>

                <div class="input-group m-b col-xs-12 col-sm-11 col-md-10 col-lg-10">

                    <div class="col-xs-11 col-sm-9 col-md-9 col-lg-9">
                        <input type="checkbox" name="sendSingle" id="sendSingle" class="checkbox checkbox-primary checkbox-inline" value="yes">
                        <label class="m-l-xs font-normal" data-toggle="tooltip" title="When checked other recipients or group members will not be known to the reciever. (cell carrier dependent)">Send To Each Recipient or Group Member Individually</small></label>
                    </div>

                </div>

                <div class="input-group m-b col-xs-12 col-sm-11 col-md-10 col-lg-10" id="sndInd">

                    <small><strong>Contacts:</strong></small>
                    <input type="text" placeholder="Contact(s)" id="contacts" class="form-control m-t-xs" name="contacts" value="{{ old('contacts') }}">

                </div>

                <div id="sndGrp">
                    <small><strong>Groups:</strong></small><br>

                    @if (count($groups) == 0) <p><i>No groups have been created</i></p>@endif
                    @foreach ($groups as $group)

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                            <input type="checkbox" name="group[]" id="group" class="checkbox checkbox-primary checkbox-inline checkGroup" onclick="groupCount()" value="{{ $group->id }}" @if ($group->groupCount <1)disabled @endif @if( $group->id == Session::get('groupID')) checked @endif>
                            <label class="m-l-xs font-normal">{{ $group->groupName }} (<small>{{ number_format($group->groupCount) }} members)</small></label>
                        </div>
                   
                    @endforeach
                </div>

                <div class="input-group m-b col-xs-12 col-sm-11 col-md-10 col-lg-10" id="sendRecipients" style="z-index: 0;">

                    <small><strong>Additional Recipients:</strong></small>
                    <input type="email" placeholder="email addresses separated by comma" id="recipient" class="form-control m-t-xs" name="recipient" value="{{ old('recipient') }}" multiple>

                </div>

                <div class="clearfix"></div>

                 <div class="input-group m-b col-md-12 col-sm-11 col-md-10 col-lg-10">

                    <small><strong>Subject:</strong></small>
                    <input type="text" placeholder="Subject" id="subject" class="form-control m-t-xs" name="subject" required value="{{ old('subject') }}">

                </div>

                <div class="clearfix"></div>

                    <div class="tabs-container m-b-md panel-default-light" style="border: 1px solid #e7eaec;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1">HTML Message Body</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">Text Only Message Body</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <textarea class="form-control m-t-xs" name="emailBody" id="emailBody" required>{{ old('emailBody') }}</textarea>

                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <textarea class="form-control m-t-xs h-200" name="textBody" id="textBody" required>{{ old('textBody') }}</textarea>
                                </div>
                            </div>
                        </div>


                    </div>

                <div class="clearfix"></div>
                
                <small><strong>Signature:</strong></small><br>
                    <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3 m-t-xs">
                        <input type="checkbox" name="signature" id="signature" class="checkbox checkbox-primary checkbox-inline" value="signature" @if(old('signature') == "signature") checked @endif>
                        <span class="m-l-xs">Use My Signature</span>
                    </span>
                    
                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">

                <div class="input-group m-b col-xs-12 col-sm-11 col-md-10 col-lg-10">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal5">Attach File(s) From Attache</button> 
                </div>

                
                <input type="hidden" id="sendImages" name="sendImages" value=""> 
                

                <div class="input-group m-b col-xs-12 col-sm-11 col-md-10 col-lg-10" id="imageDiv"></div>


                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>

                    <input type="hidden" name="sendTest" id="sendTest" value="">
                    <input type="hidden" name="templateID" value="0">
                    <input type="hidden" name="userID" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="domainID" id="domainID" value="{{ $domain['id'] }}">
                    <input type="hidden" id="fileCheck" name="fileCheck" value="@if (Session::has('pixFile')){{Session::get('pixFile')}} @endif">
                    <input type="hidden" id="fileName" name="fileName" value="{{ old('fileName') }}">

                    {{ csrf_field() }}

                    <button type="button" class="btn btn-primary btn-xs pull-right" onclick="formSend()"><i class="fa fa-paper-plane m-r-xs"></i>Send Email</button>
                    <button type="button" class="btn btn-warning btn-xs pull-right m-r-xs" id="sendTest" onclick="formSend('yes')"><i class="fa fa-thumbs-up m-r-xs"></i>Send Test</button>
                    
                </form>

                </div>
            </div>  
        </div>


        <div class="clearfix"></div>
    </div>
</div>


@include('images.fileModal')

<!-- Page Level Scripts -->
<script src="/js/plugins/ckeditor/ckeditor.js"></script>
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.tokeninput/1.6.0/jquery.tokeninput.js"></script>

<script>

function groupCount(){
    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount >1) {
        alert("Please Select Only One Group.");
        $('input[name="group[]"]:checked').prop('checked', false);
    }
};


    $(document).ready(function () {
        $("#contacts").tokenInput("/api/v1/contactEmailTokens", {
            method: "POST",
            theme: "mac",
            hintText: "Type in a contact name",
            tokenLimit: 25
        });
        $("#sendToIndividual").prop('checked', true);
        $("#sndInd").show();
    });
</script>


<script>

function formSend(test){

    if (test == 'yes') {
        $('#testText').val('yes');
    } else {
        $('#testText').val('no');
    }
    var chk = $("#recipient")[0].checkValidity();
    if (!chk) {
        alert("Please check your recipient email addresses and try again.");
        return false;
    }
    var contacts = $("#contacts").tokenInput("get");
    var recipients = $("#recipient").val();

    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount != 1 && contacts.length == 0 && recipients.length == 0) {
        alert("You must select at one group or contact or have one recipient.");
        return false;
    }

    var email = CKEDITOR.instances.emailBody.getData();
    var text = $("#textBody").val();
    var subject = $("#subject").val();
    if (email.length == 0 && text.length == 0 && subject.length == 0) {
        alert("Please type a message or subject.");
        return false;
    }

    $('#sendTemplate').submit();
} 

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

function sendIndividual(){

    var si = $("#sendToIndividual").is(':checked');

    if (si) {
        $("#sndInd").show();
        $("#sendRecipients").show();
        $("#sndGrp").hide();
        $("#sendToGroup").prop('checked', false);
        $(".checkGroup").prop('checked', false);
    } else {
       $("#sndInd").hide();
       $("#sndGrp").hide();
    }

};

function sendGroup(){

    var sg = $("#sendToGroup").is(':checked');

    if (sg) {
        $("#sndGrp").show();
        $("#sndInd").hide();
        $("#sendRecipients").hide();
        $("#sendToIndividual").prop('checked', false);
        $("#contacts").tokenInput("clear");
        $("#recipient").val("");
    } else {
       $("#sndGrp").hide();
       $("#sndInd").hide();

    }
};

</script>

<script>

if (document.documentElement.clientWidth < 479 ) {
    CKEDITOR.replace( 'emailBody', {
        customConfig: '/js/plugins/ckeditor/config_xs.js',
        filebrowserBrowseUrl: '/attache/ckeditor/browse',
    } );
}

if (document.documentElement.clientWidth > 480 && document.documentElement.clientWidth < 767) {
    CKEDITOR.replace( 'emailBody', {
        customConfig: '/js/plugins/ckeditor/config_small.js',
        filebrowserBrowseUrl: '/attache/ckeditor/browse',
    } );
}

if (document.documentElement.clientWidth > 768 ) {
    CKEDITOR.replace( 'emailBody', {
        customConfig: '/js/plugins/ckeditor/config_basic.js',
        filebrowserBrowseUrl: '/attache/ckeditor/browse',
    } );
}



</script>

@endsection