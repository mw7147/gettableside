<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Test Page">
    <meta name="author" content="riverhouseit.com">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ elixir('css/bootfont.css') }}" />

    <!-- Custom CSS -->
    <link href="{{ elixir('css/rhit_admin.css') }}" rel="stylesheet">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

 
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ elixir('js/bootvuequery.js') }}"></script>



</head>

<style>
.loginbkgrnd { background-image: url("{{ $domain['backgroundPublic'] }}");}

</style>
<body>
    <div id="app">
        
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
