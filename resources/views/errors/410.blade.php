@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>410</h1>
        <h3 class="font-bold">Your Username or Password are incorrect. Please go back and try again.</h3>

        <div class="error-desc">
            You must log in to continue. Make sure you are at the the correct URL, are logged in and try again.
        </div>
    </div>
@endsection

