@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>500</h1>
        <h3 class="font-bold">Internal Server Error.</h3>

        <div class="error-desc">
            Whoops! Something Happened. Please go back and try again.
        </div>
    </div>
@endsection

