@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>501</h1>
        <h3 class="font-bold">Content Coming Soon.</h3>

        <div class="error-desc">
            The content you are looking for is under construction. Check back soon.
        </div>
    </div>
@endsection

