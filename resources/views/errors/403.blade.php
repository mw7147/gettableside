@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>403</h1>
        <h3 class="font-bold">You are not authorized to view this page.</h3>

        <div class="error-desc">
            You must be logged in and authorized to view this page. Make sure you are at the the correct URL, are logged in and try again.
        </div>
    </div>
@endsection

