@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>504</h1>
        <h3 class="font-bold">Processing Gateway Error.</h3>

        <div class="error-desc">
            Whoops! Something Happened. Please go back and try again.
        </div>
    </div>
@endsection

