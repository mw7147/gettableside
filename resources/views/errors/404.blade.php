@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Sorry, the page you are looking for could not be found.</h3>

        <div class="error-desc">
            Please check the URL and try again.
        </div>
    </div>
@endsection

