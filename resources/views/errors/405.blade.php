@extends('errors.error')



@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>405</h1>
        <h3 class="font-bold">Sorry, the action you are requesting cannot be completed.</h3>

        <div class="error-desc">
            Please check the URL and try again.
        </div>
    </div>
@endsection