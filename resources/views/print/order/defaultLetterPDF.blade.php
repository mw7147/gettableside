<!DOCTYPE html>
<html lang="en">
<head>
<title>{{ $domainSiteConfig->locationName }} Order</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    table{border-collapse:collapse !important; max-width: 400px !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}
</style>
</head>
<body style="margin: 0; padding: 0;">

<!-- Body -->

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
        <td bgcolor="#ffffff">
            <div style="padding: 10px 5px 0px 2px;">


                        
                <table border="0" cellpadding="0" cellspacing="0" width="400" class="wrapper">
                    <tbody>
                        <tr>
                            <td style="font-size: 24px; font-family: Arial, sans-serif;">{{ $orders->orderType }}</td>
                        </tr>
                        @if ($orders->orderType == "dineIn") 
                        <tr>
                            <td><span style="font-size: 16px; font-family: Arial, sans-serif;"> {{ $orders->locationName }}</span></td>
                        </tr>
                        @endif
                        <tr>
                            <td style="font-size: 14px; font-family: Arial, sans-serif;">{{ $domainSiteConfig->locationName}}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; font-family: Arial, sans-serif;">Order Date/Time: {{ $orders->orderPrepTime }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr></tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff">
            <div style="padding: 15px 15px 10px 2px;">

                <table border="0" cellpadding="0" cellspacing="0" width="400" class="wrapper">
                    <tbody><tr><td>

                        <span style="padding-bottom: 8px; font-size: 16px; font-family: Arial, sans-serif;"><i>Order ID: {{ $orders->id }}</i></span><br>
                        <span style="font-size: 14px; font-family: Arial, sans-serif; margin-top: 15px;"><i>Customer:</i></span>
                            <p class="orderMobile" style="margin-top: 4px !important; font-size: 14px; font-family: Arial, sans-serif; line-height: 19px;">

                                        {!! $orderName !!}

                            </p>

                    </td></tr></tbody>
                </table>

            </div>
            <hr width="100%">
        </td>
    </tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff">
            <div style="padding: 0px 5px 0px 20px;">


                        
                            {!! $orderHTML !!}


           

            </div>
        </td>
    </tr>
</table>





</body>
</html>