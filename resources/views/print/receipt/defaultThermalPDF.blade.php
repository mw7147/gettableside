<body style="font-family: Verdana, Geneva, sans-serif;">

    <p><span style="font-size: 52px; text-transform: uppercase"><b>{{ $orders->orderType }}</b></span><br>
    
    @if ($orders->orderType == "dineIn")
    <span style="font-size: 46px; text-transform: uppercase;"> {{ $orders->locationName }}</span></p>
    @else
    <span style="font-size: 46px; ">Online Order</span></p>
    @endif    
    
    <p><span style="font-size: 46px; ">{{ $domainSiteConfig->locationName}}</span><br>                   
    <span style="font-size: 32px; ">Order Date: {{ Carbon\Carbon::parse($orders->orderPrepTime)->format('n/j/y')}}</span></p>
    <span style="font-size: 32px; font-weight: 800; ">Order Time: {{ Carbon\Carbon::parse($orders->orderPrepTime)->format('g:i A') }}</span></p>


    <p><span style="font-size: 36px; ">Order ID: {{ $orders->id }}</Order></span></p>
        
    <p style="margin-top: 12px; margin-bottom: 12px;">--------------------------------------------------------------------------------------------------------------------------------------</p>

    <p><span style="font-size: 32px; font-style: italic;">Customer:</i></span><br>
        <span style="font-size: 36px;">{!! $orderName !!}</span></p>


    <p style="margin-top: 12px; margin-bottom: 12px;">--------------------------------------------------------------------------------------------------------------------------------------</p>
       


        {!! $orderHTML !!}







</body>