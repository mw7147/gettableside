<!DOCTYPE html>
<html lang="en">
<head>
<title>{{ $domainSiteConfig->locationName }} Order</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">

    /* RESET STYLES */
    table{border-collapse:collapse !important; max-width: 400px !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}


</style>
</head>
<body style="margin: 0; padding: 0;">

<!-- Body -->

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
        <td bgcolor="#ffffff">
            <div style="padding: 10px 15px 0px 15px;">


                        
                <table border="0" cellpadding="0" cellspacing="0" width="400" class="wrapper">
                    <tbody>
                        <tr>
                            <td class="table-padding" align="left" width="80%" style="font-size: 24px; font-family: Arial, sans-serif; margin-bottom: 12px;">{{ $orders->orderType }}</td>
                            <td class="table-padding" width=20%" align="right" style="font-size: 24px; font-family: Arial, sans-serif; margin-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="table-padding" align="left" width="80%" style="font-size: 14px; font-family: Arial, sans-serif;">{{ $domainSiteConfig->locationName}}</td>
                            <td class="table-padding" width=20%" align="right" style="font-size: 14px; font-family: Arial, sans-serif;">Online Order</td>
                        </tr>
                        <tr>
                            <td class="table-padding" align="left" width="80%" style="font-size: 14px; font-family: Arial, sans-serif;">Order Date/Time: {{ $orders->orderPrepTime }}</td>
                            <td class="table-padding" width=20%" align="right" style="font-size: 14px; font-family: Arial, sans-serif;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr></tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff">
            <div style="padding: 15px 15px 10px 15px;">

                <table border="0" cellpadding="0" cellspacing="0" width="400" class="wrapper">
                    <tbody><tr><td>
                    
                        @if ($orders->orderType == "dineIn") <span style="font-size: 16px; font-family: Arial, sans-serif;"> {{ $orders->locationName }}</span><br> @endif    

                        <span style="padding-bottom: 8px; font-size: 13px; font-family: Arial, sans-serif;"><i>Order ID: {{ $orders->id }}</i></span><br>
                        <span style="font-size: 13px; font-family: Arial, sans-serif;"><i>Customer:</i></span>
                            <p class="orderMobile" style="margin-top: 4px !important; font-size: 14px; font-family: Arial, sans-serif; line-height: 19px;">

                                        {!! $orderName !!}

                            </p>

                    </td></tr></tbody>
                </table>

            </div>
            <hr width="477">
        </td>
    </tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff">
            <div style="padding: 0px 15px 0px 35px;">


                        
                            {!! $orderHTML !!}


           

            </div>
        </td>
    </tr>
</table>





</body>
</html>