@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->

<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 226px;}
</style>

<h2>Send QuickText Message</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Send Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Send Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-8 m-b-md instructions">
    <li>Select the group you wish to receive the text.</li>
    <li>Type your desired text. The text will only be used for this message only - i.e. not saved as a template.</li>
    <li>Click "Try Text" to send a text to your registered mobile number.</li>
    <li>Click Send Text to queue the messages for sending.</li>
</ul>

<div class="ibox send">
    
    <div class="ibox-title">
        <h5><i class="m-r-sm">Select Group and Send</i></h5>
    </div>
        
    <div class="ibox-content">
        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-9" id="templatePanel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <i class="fa fa-commenting m-r-xs"></i><span id="templateName">QuickText</span>
                </div>
                <div class="panel-body">

                <div class="clearfix"></div>
                
                @if(Session::has('fileError'))
                    <div class="alert alert-danger alert-dismissable col-xs-8 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('fileError') }}
                    </div>
                    <div class="clearfix"></div>
                @endif

                @if(Session::has('QuickTextTest'))
                    <div class="alert alert-success alert-dismissable col-xs-8 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('QuickTextTest') }}
                    </div>
                    <div class="clearfix"></div>
                @endif

                <form name="sendTemplate" id="sendTemplate" method="POST" action="/text/{{Request::get('urlPrefix')}}/quicktextconfirm"  enctype="multipart/form-data">
                    <small><strong>Select Group:</strong></small>
                    <div class="clearfix"></div>

                    <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3"><input type="checkbox" name="group[]" id="group1" class="checkbox checkbox-primary checkbox-inline" value="all" onclick="groupCount()" @if(Session::get('groupID') ===0) checked @endif><label class="m-l-xs font-normal" @if ($totalContacts <1)disabled @endif>All Contacts (<small>{{ number_format($totalContacts) }} contacts)</small></label></span>

                    @foreach ($groups as $group)

                        <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3"><input type="checkbox" name="group[]" id="group" class="checkbox checkbox-primary checkbox-inline" onclick="groupCount()" value="{{ $group->id }}" @if ($group->groupCount <1)disabled @endif @if( $group->id == Session::get('groupID')) checked @endif><label class="m-l-xs font-normal">{{ $group->groupName }} (<small>{{ number_format($group->groupCount) }} members)</small></label></span>
                    
                    @endforeach


                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <small class="pull-right"><strong><span id="countChar">25</span> of 150 characters</strong></small>
                    <small><strong>Message:</strong></small>
                    <div class="clearfix"></div>

                    <textarea class="form-control" id="message" name="message" onkeyup="charCount()">{{ old('message') }}</textarea>
                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>
             
        
                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 m-b-md m-t-md">
                    <div class="input-group m-b"><span class="input-group-btn">
                        <button type="button" id="pictureButton" class="btn btn-primary pull-left" style="width: 20%; margin-left: -15px;">Picture</button>
                        <input type="text" class="form-control" id="picture" name="picture" style="width: 80%;" value="{{ old('picture') }}">
                        <input type="file" id="newPicture" name="fileName" style="display: none;">
                    </div>
                </div>

                <div class="clearfix"></div>

                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>
                    
                    <input type="hidden" name="testText" id="testText" value="">
                    <input type="hidden" name="templateID" value="0">
                    <input type="hidden" name="userID" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="customerID" value="{{ $cid }}">
                    <input type="hidden" name="domainID" value="{{ $domain['id'] }}">
                    <input type="hidden" name="pixFile" value="{{ Session::get('pixFile') }}">
                    <input type="hidden" name="fileName" id="fileName" value="{{ old('fileName') }}">

                    {{ csrf_field() }}

                    <button type="button" class="btn btn-primary btn-xs pull-right" onclick="formSend('no')"><i class="fa fa-paper-plane m-r-xs"></i>Send Text</button>
                    <button type="button" class="btn btn-warning btn-xs pull-right m-r-xs" id="sendTest" onclick="formSend('yes')"><i class="fa fa-thumbs-up m-r-xs"></i>Try Test</button>
                </form>

                </div>
            </div>  
        </div>


        <div class="clearfix"></div>
    </div>
</div>


<!-- Page Level Scripts -->

<script>

$("#pictureButton").on("click", function() {
    $("#newPicture").trigger("click");
});

$('#newPicture').on('change',function(){
    var pix=this.files[0];
    console.log(pix);
    if (pix.size > 250000) {
        $('#newPicture').val('');
        $("#picture").val('');
        alert("The file is over 250 Kb. The file must be smaller than 250 Kb.")
        return true;
    }

    if (pix != '') {
        var pictures = ["image/jpg", "image/jpeg", "image/png", "application/pdf"];
        var isPicture = pictures.indexOf(pix.type);
        if (isPicture == -1) {
            $('#newPicture').val('');
            $("#picture").val('');
            alert("The picture must be a jpg, png or pdf file smaller than 250 Kb.")
            return true;
        }
    }

    $("#picture").val(pix.name);
    
});

function groupCount(){
    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount >1) {
        alert("Please Select Only One Group.");
        $('input[name="group[]"]:checked').prop('checked', false);
    }
} 

function formSend(test){
    if (test == 'yes') {
        $('#testText').val('yes');
    } else {
        $('#testText').val('no');
    }

    var fn = $('.attachmentName').text();
    $('#fileName').val(fn);

    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount != 1) {
        alert("You must select at least one group.");
        return false;
    }
    $('#sendTemplate').submit();
} 



$(document).ready(function() {
    var length = $('#message').val().length;
    $('#countChar').html(length);
});

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


function charCount(){
    var maxLength = 150;
    var message = $('#message').val();
    var length = message.length;
    if (length <= maxLength) { $('#countChar').html(length); return true;}
    alert("You have reached the maximum character limit of " + maxLength + " characters." );
    var limit = message.substring(0, maxLength);
    $(this).val(limit);
    return true;
};


</script>



@endsection