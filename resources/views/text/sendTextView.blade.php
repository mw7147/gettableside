@extends('admin.admin')

@section('content')


<!-- Page Level CSS -->

<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 226px;}
</style>



<h1>Send Text Messages</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Send Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Send Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-10 col-md-10 m-b-md instructions">
    <li>Active Text Templates are shown below.</li>
    <li>Click "Use This Template" in the Text Template of your choice.</li>
    <li>The options for sending the email message will be displayed on the next page.</li>
</ul>

<div class="ibox templates">
    
    <div class="ibox-title">
        <h5><i class="m-r-sm">Select Text Template</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        @if (!$templates->count()) <p>You do not have any active Text Templates.</p> @endif

            @foreach ($templates as $template)
                    
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading panel-default-dark">
                      <i class="fa fa-commenting m-r-xs"></i><span>ID: {{ $template->id }} - {{ $template->name }}</span>
                    </div>
                    <div class="panel-body">
                        <small><strong>Template Description:</strong></small> 
                        <p>{{ $template->description }}</p>
                        <a href="/text/{{Request::get('urlPrefix')}}/templates/send/{{ $template->id }}" class="btn btn-primary btn-xs pull-right">Use This Template<i class="fa fa-long-arrow-right m-l-xs"></i></a>

                    </div>
                </div>  
            </div>

            @endforeach

        <div class="clearfix"></div>
    </div>
</div>


<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>


@endsection