@extends('admin.admin')

@section('content')

<h1>Send Text Messages Confirmation</h1>


<div class="ibox send">
    
    <div class="ibox-title">
        <h5><i class="m-r-sm">Send Text Message Confirmation for Message ID: {{ $messageID }}</i></h5>
    </div>
        
    <div class="ibox-content">
        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-9 col-md-6 col-lg-6">
            

        <p><h3>Success!</h3> Your text message has been queued for sending as follows:</p>
      <div class="clearfix"></div>
        <hr>
          <table class="table table-basic m-t-xl m-b-xl">
            <thead>
                <tr>
                    <th>Confirmed Contacts</th>
                    <th>Bad Contacts</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $goodCount }}</td>
                    <td>{{ $badCount }}</td>
                </tr>
            </tbody>
        </table>
     <div class="clearfix"></div>
        <hr>
        <p><i><small>Note: Bad Contacts are contacts without a mobile number.</small></i></p>

        </div>


        <div class="clearfix"></div>
    </div>
</div>

@endsection