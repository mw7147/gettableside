@extends('admin.admin')

@section('content')

    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>Text Template Administration</h1>
    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>
    <div class="instructions">
    	<h4>
    		Text Templates are listed below.
    	</h4>

    	<ul>
    		<li>Type in the search box to search results.</li>
    		<li>Press the "Copy" button to copy the data to your clipboard.</li>
    		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
    		<li>Press the "PDF" button to create and download a PDF file.</li>
    		<li>Press the "Print" button to print the results.</li>
    	</ul>
    </div>
<div class="ibox-content">

    <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

    <h3>Text Template List</h3><hr>

        @if(Session::has('templateSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('templateSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('templateError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('templateError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <a href="/text/{{Request::get('urlPrefix')}}/templates/new" class="btn btn-success btn-xs m-b-lg" alt="Create New Text Template"><i class="fa fa-plus-square"></i> Create New Template</a>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    <th width="10%">Template ID</th>
                    <th width="20%">Name</th>
                    <th width="10%">Active</th>
                    <th width="30%">Description</th>
                    <th width="30%">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($texts as $text)
                <tr>                
                    <td>{{ $text['id'] }}</td>
                    <td>{{ $text['name'] }}</td>
                    <td>{{ $text['status'] }}</td>
                    <td>{{ $text['description'] }}</td>
                    <td><a class="btn btn-success btn-xs w90 m-r-xs" href="/text/{{Request::get('urlPrefix')}}/templates/edit/{{ $text['id'] }}"><i class="fa fa-pencil"></i> View/Edit</a>
                        <a class="btn btn-success btn-xs w90 m-r-xs" href="/text/{{Request::get('urlPrefix')}}/templates/copy/{{ $text['id'] }}"><i class="fa fa-exchange"></i> Copy</a>
                        <a class="btn btn-danger btn-xs w90" href="/text/{{Request::get('urlPrefix')}}/templates/delete/{{$text['id'] }}" onclick="javascript:return confirm('Are you sure you want to delete this template? All template data will be permanently deleted.')"><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>
    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                dom: '<"html5buttons"B>lTfgitp',
                order: [[ 0, "desc" ]],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'TextTemplateList'},
                    {extend: 'pdf', title: 'TextTemplateList'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });


    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    </script>

 



@endsection