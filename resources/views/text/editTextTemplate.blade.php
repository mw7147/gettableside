@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->

<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 226px;}
</style>

<h2>Edit Text Template</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-8 m-b-md instructions">
    <li>Fill out the template information as necessary.</li>
    <li>The picture must be a jpg, png or pdf and less than 250kb in size.</li>
    <li>Press "Update Template" to update the template.</li>
    <li>Press "Cancel" or select a menu item to return to the Template List View without saving.</li>
</ul>

<div class="ibox send">
    
    <div class="ibox-title">
        <h5><i class="m-r-sm">Edit Template ID: {{ $template->id }} - {{ $template->name }}</i></h5>
    </div>
        
    <div class="ibox-content">
        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11" id="templatePanel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <i class="fa fa-commenting m-r-xs"></i><span id="templateName">Edit Template</span>
                </div>
                <div class="panel-body">

                <div class="clearfix"></div>
                
                @if(Session::has('fileError'))
                    <div class="alert alert-danger alert-dismissable col-xs-8 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('fileError') }}
                    </div>
                    <div class="clearfix"></div>
                @endif

                <form name="newTemplate" id="newTemplate" method="POST" action="/text/{{Request::get('urlPrefix')}}/templates/edit/{{ $id }}"  enctype="multipart/form-data">



                    <div class="input-group m-b col-xs-12 col-sm-10 col-md-10 col-lg-10">

                        <span class="input-group-addon" style="width: 20%;">Status</span>
                        <select class="form-control" name="active">
                
                            <option @if ($template->status == 'yes') seletected @endif value="yes">Active</option>
                            <option @if ($template->status == 'no') seletected @endif value="no">Inactive</option>
                    
                        </select>
                
                    </div>

                    <div class="input-group m-b col-xs-12 col-sm-10 col-md-10 col-lg-10">

                        <span class="input-group-addon" style="width: 20%;">Name</span>
                        <input type="text" placeholder="Name" class="form-control" name="name" value="{{ $template->name }}">
    
                    </div>
    
                   
                    <div class="clearfix"></div>
    
                    
                    <div class="input-group m-b col-xs-12 col-sm-10 col-md-10 col-lg-10">
    
                        <span class="input-group-addon" style="width: 20%;">Description</span>
                        <input type="text" placeholder="Description" class="form-control" name="description" value="{{ $template->description }}">
    
                    </div>

                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <small class="pull-right"><strong><span id="countChar">25</span> of 150 characters</strong></small>
                    <small><strong>Message:</strong></small>
                    <div class="clearfix"></div>

                    <textarea class="form-control" id="message" name="message" onkeyup="charCount()">{{ $template->message }}</textarea>
                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>
             
        
                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 m-b-md m-t-md">
                    <div class="input-group m-b"><span class="input-group-btn">
                        <button type="button" id="pictureButton" class="btn btn-primary pull-left" style="width: 20%; margin-left: -15px;">Picture</button>
                        <input type="text" class="form-control" id="picture" name="picture" style="width: 80%;" value="{{ basename($template->picturePublic) }}">
                        <input type="file" id="newPicture" name="fileName" style="display: none;">
                    </div>
                </div>

                <div class="clearfix"></div>

                @if (!empty($template->picturePublic))
                <div class="input-group m-b col-xs-12 col-sm-10 col-md-9 col-lg-9">

                    <img class="img" alt="offer image" src="{{ $template->picturePublic }}" style="height: 100%; max-height: 500px;">
    
                </div>
                @endif

                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>

                    {{ csrf_field() }}
                    <input type="hidden" name="imgCheck" value="{{ $template->picturePublic }}">

                    <p class="text-center">
                        <a href="/text/{{Request::get('urlPrefix')}}/templates/view" class="btn btn-primary w-150"><i class="fa fa-reply m-r-xs"></i>Cancel</a>
                        <button type="submit" class="btn btn-primary w-150"><i class="fa fa-share m-r-xs"></i>Update Template</button>
                    </p>
                </form>

                </div>
            </div>  
        </div>


        <div class="clearfix"></div>
    </div>
</div>























<!-- Page Level Scripts -->

<script>

$("#pictureButton").on("click", function() {
    $("#newPicture").trigger("click");
});

$('#newPicture').on('change',function(){
    var pix=this.files[0];
    console.log(pix);
    if (pix.size > 250000) {
        $('#newPicture').val('');
        $("#picture").val('');
        alert("The file is over 250 Kb. The file must be smaller than 250 Kb.")
        return true;
    }

    if (pix != '') {
        var pictures = ["image/jpg", "image/jpeg", "image/png", "application/pdf"];
        var isPicture = pictures.indexOf(pix.type);
        if (isPicture == -1) {
            $('#newPicture').val('');
            $("#picture").val('');
            alert("The picture must be a jpg, png or pdf file smaller than 250 Kb.")
            return true;
        }
    }

    $("#picture").val(pix.name);
    
});


$(document).ready(function() {
    var length = $('#message').val().length;
    $('#countChar').html(length);
});

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


function charCount(){
    var maxLength = 150;
    var message = $('#message').val();
    var length = message.length;
    if (length <= maxLength) { $('#countChar').html(length); return true;}
    alert("You have reached the maximum character limit of " + maxLength + " characters." );
    var limit = message.substring(0, maxLength);
    $(this).val(limit);
    return true;
};


</script>



@endsection