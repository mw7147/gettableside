@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 226px;}
</style>



<h1>Send Text Messages</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Send Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Send Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-8 m-b-md instructions">
    <li>Active Text Templates are shown below.</li>
    <li>Click "Use This Template" in the Text Template of your choice. The options for sending the text message will be displayed.</li>
    <li>Select the group you wish to receive the text.</li>
    <li>You may override the template by typing your desired text. Changes to the template will only be used for this message - i.e. not saved.</li>
    <li>If you need to change the picture or other information not in the message area, click the "Edit Template" button.</li>
    <li>Click "Try Text" to send a text to your registered mobile number.</li>
    <li>Click Send Text to queue the messages for sending.</li>
</ul>




<div class="ibox send">
    
    <div class="ibox-title">
        <h5><i class="m-r-sm">Select Group and Send</i></h5>
    </div>
        
    <div class="ibox-content">
        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11" id="templatePanel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <i class="fa fa-commenting m-r-xs"></i><span id="templateName">Template: {{ $template->name }}</span>
                </div>
                <div class="panel-body">

                @if(Session::has('textTest'))
                    <div class="alert alert-success alert-dismissable col-xs-8 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('textTest') }}
                    </div>
                    <div class="clearfix"></div>
                @endif

                <form name="sendTemplate" id="sendTemplate" method="POST" action="/text/{{Request::get('urlPrefix')}}/templates/queue">
                    <small><strong>Select Group:</strong></small>
                    <div class="clearfix"></div>

                    <span class="col-xs-12 col-sm-12 col-md-6 col-lg-4"><input type="checkbox" name="group[]" id="group1" class="checkbox checkbox-primary checkbox-inline" value="all" onclick="groupCount()"><label class="m-l-xs font-normal" @if ($totalContacts <1)disabled @endif>All Contacts (<small>{{ number_format($totalContacts) }} contacts)</small></label></span>

                    @foreach ($groups as $group)

                        <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3"><input type="checkbox" name="group[]" id="group" class="checkbox checkbox-primary checkbox-inline" onclick="groupCount()" value="{{ $group->id }}" @if ($group->groupCount <1)disabled @endif><label class="m-l-xs font-normal">{{ $group->groupName }} (<small>{{ number_format($group->groupCount) }} members)</small></label></span>
                    
                    @endforeach


                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>
                    


                    <small class="pull-right"><strong><span id="countChar">25</span> of 150 characters</strong></small>
                    <small><strong>Message:</strong></small>
                    <div class="clearfix"></div>

                    <textarea class="form-control" id="message" name="message" onkeyup="charCount()" disabled>{{ $template->message }}</textarea>
                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>


                    <small><strong>Picture:</strong></small>
                    <div class="clearfix"></div>

             
                    @if ($template->picturePublic != '')
                    <a href="{{ $template->picturePublic }}"><img class="img" alt="offer image" src="{{ $template->picturePublic }}" style="max-width: 100%; height: auto; max-height: 250px;"></a>
                    @endif
 

                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>

                    <input type="hidden" name="templateID" value="{{ $template->id }}">
                    <input type="hidden" name="testText" id="testText" value="">

                    {{ csrf_field() }}

                    <button type="button" class="btn btn-primary btn-xs pull-right" onclick="formSend('no')"><i class="fa fa-paper-plane m-r-xs"></i>Send Text</button>
                    <button type="button" class="btn btn-warning btn-xs pull-right m-r-xs" id="sendTest" onclick="formSend('yes')"><i class="fa fa-thumbs-up m-r-xs"></i>Try Test</button>
                    <a href="/text/{{Request::get('urlPrefix')}}/templates/edit/{{ $template->id }}" class="btn btn-success btn-xs m-r-xs pull-right"><i class="fa fa-pencil m-r-xs"></i>Edit Text</a>
                </form>

                </div>
            </div>  
        </div>


        <div class="clearfix"></div>
    </div>
</div>















<!-- Page Level Scripts -->


<script>

function groupCount(){
    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount >1) {
        alert("Please Select Only One Group.");
        $('input[name="group[]"]:checked').prop('checked', false);
    }
} 



$(document).ready(function() {
    var length = $('#message').val().length;
    $('#countChar').html(length);
});

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


function charCount(){
    var maxLength = 150;
    var message = $('#message').val();
    var length = message.length;
    if (length <= maxLength) { $('#countChar').html(length); return true;}
    alert("You have reached the maximum character limit of " + maxLength + " characters." );
    var limit = message.substring(0, maxLength);
    $(this).val(limit);
    return true;
};


function formSend(test){
    if (test == 'yes') {
        $('#testText').val('yes');
    } else {
        $('#testText').val('no');
    }

    var fn = $('.attachmentName').text();
    $('#fileName').val(fn);

    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount != 1) {
        alert("You must select at least one group.");
        return false;
    }
    $('#sendTemplate').submit();
} 

</script>




@endsection