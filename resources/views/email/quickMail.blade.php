@extends('admin.admin')



@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<h2>QuickMail Message</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-10 m-b-md instructions">
    <li>Select the group you wish to receive the message.</li>
    <li>If you wish to use your signature, check the "Use My Signature" box.</li>
    <li>QuickMail's are not saved as a template. All tracking and reporting systems are available with QuickMail.</li>
    <li>Construct your email. If you wish the Text Only version to differ from the html version, type in your text only email. Otherwise a text only version will be automatically created from the html version when sending.</li>
    <li>Click "Try Test" to send a test to your registered email address.</li>
    <li>Click Send Email to queue the messages for sending.</li>
</ul>




<div class="ibox send">
{{ Session::get('attachmentPublic') }}
    <div class="ibox-title">
        <h5><i class="m-r-sm">Send QuickMail</i></h5>
    </div>
        
    <div class="ibox-content">
        <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10" id="templatePanel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <i class="fa fa-envelope m-r-xs"></i><span id="templateName">QuickMail</span>
                </div>
                <div class="panel-body">

                <div class="clearfix"></div>
                
                @if(Session::has('fileError'))
                    <div class="alert alert-danger alert-dismissable col-xs-10 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('fileError') }}
                    </div>
                    <div class="clearfix"></div>
                @endif

                @if(Session::has('QuickMailTest'))
                    <div class="alert alert-success alert-dismissable col-xs-10 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('QuickMailTest') }}
                    </div>
                    <div class="clearfix"></div>
                @endif




                <form name="sendTemplate" id="sendTemplate" method="POST" action="/{{Request::get('urlPrefix')}}/quickmailconfirm" enctype="multipart/form-data">
                    <small><strong>Select Group:</strong></small>
                    <div class="clearfix"></div>

                    <span class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-xs"><input type="checkbox" name="group[]" id="group1" class="checkbox checkbox-primary checkbox-inline" value="all" @if(Session::get('groupID') === 0) checked @endif onclick="groupCount()"><label class="m-l-xs font-normal" @if ($totalContacts <1)disabled @endif>All Contacts (<small>{{ number_format($totalContacts) }} contacts)</small></label></span>

                    @foreach ($groups as $group)

                        <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3 m-t-xs"><input type="checkbox" name="group[]" id="group" class="checkbox checkbox-primary checkbox-inline" onclick="groupCount()" value="{{ $group->id }}" @if ($group->groupCount <1)disabled @endif @if(Session::get('groupID') == $group->id) checked @endif><label class="m-l-xs font-normal">{{ $group->groupName }} (<small>{{ number_format($group->groupCount) }} members)</small></label></span>
                    
                    @endforeach

                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">

                    <small><strong>Signature:</strong></small><br>
                    <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3 m-t-xs">
                        <input type="checkbox" name="signature" id="signature" class="checkbox checkbox-primary checkbox-inline" value="signature" @if(old('signature') == "signature") checked @endif>
                        <span class="m-l-xs">Use My Signature</span>
                    </span>
                    
                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">


                 <div class="input-group m-b col-md-12 col-sm-11 col-md-10 col-lg-10">

                    <small><strong>Subject:</strong></small>
                    <input type="text" placeholder="QuickMail Subject" id="subject" class="form-control m-t-xs" name="subject" required value="{{ old('subject') }}">

                </div>

                <div class="clearfix"></div>

                    <div class="tabs-container m-b-md panel-default-light" style="border: 1px solid #e7eaec;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1">HTML Message Body</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">Text Only Message Body</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <textarea class="form-control m-t-xs" name="emailBody" id="emailBody" required>{{ old('emailBody') }}</textarea>

                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <textarea class="form-control m-t-xs h-200" name="textBody" id="textBody" required>{{ old('textBody') }}</textarea>
                                </div>
                            </div>
                        </div>


                    </div>

                <div class="clearfix"></div>


                <div class="input-group m-b col-xs-12 col-sm-10 col-md-10 col-lg-10 nodisplay">
                    <div class="fileinput fileinput-new input-group displayWidth" data-provides="fileinput">
                    
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Attachment</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="attachment" />
                        </span>
                            <a href="#" class="btn btn-default fileinput-exists input-group-addon" id="remove" data-dismiss="fileinput">Remove</a>

                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename attachmentName"></span>
                        </div>
                    </div>
                </div> 

                    <div class="clearfix"></div>
                    <hr class="hr-line-dashed">
                    <div class="clearfix"></div>

                    <input type="hidden" name="sendTest" id="sendTest" value="">
                    <input type="hidden" name="templateID" value="0">
                    <input type="hidden" name="userID" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="domainID" id="domainID" value="{{ $domain['id'] }}">
                    <input type="hidden" id="fileCheck" name="fileCheck" value="@if (Session::has('pixFile')){{Session::get('pixFile')}} @endif">
                    <input type="hidden" id="fileName" name="fileName" value="">

                    {{ csrf_field() }}

                    <button type="button" class="btn btn-primary btn-xs pull-right" onclick="formSend()"><i class="fa fa-paper-plane m-r-xs"></i>Send Email</button>
                    <button type="button" class="btn btn-warning btn-xs pull-right m-r-xs" id="sendTest" onclick="formSend('yes')"><i class="fa fa-thumbs-up m-r-xs"></i>Send Test</button>
                    
                </form>

                </div>
            </div>  
        </div>


        <div class="clearfix"></div>
    </div>
</div>


<!-- Page Level Scripts -->
<script src="/js/plugins/ckeditor/ckeditor.js"></script>
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>

function groupCount(){
    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount >1) {
        alert("Please Select Only One Group.");
        $('input[name="group[]"]:checked').prop('checked', false);
    }
} 

function formSend(test){
    if (test == 'yes') {
        $('#sendTest').val('yes');
        alert("Please select your attachment again after the test.");
    } else {
        $('#sendTest').val('no');
    }

    var fn = $('.attachmentName').text();
    $('#fileName').val(fn);

    var gCount = $('input[name="group[]"]:checked').length;
    if (gCount != 1) {
        alert("You must select at least one group.");
        return false;
    }
    
    $('#sendTemplate').submit();
} 

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

<script>

    CKEDITOR.replace( 'emailBody', {
        customConfig: '/js/plugins/ckeditor/config.js',
        filebrowserBrowseUrl: '/attache/ckeditor/browse',

    } );

</script>

@endsection