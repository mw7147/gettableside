<!-- Tip Modal -->

<style>

  input.faChkRnd, input.faChkSqr {
    visibility: hidden;
  }
  
  @-moz-document url-prefix() { 
  
  input.faChkRnd, input.faChkSqr {
    visibility: visible;
  }
  
   }
  
  .faChkSqr, .faChkRnd {
        margin-left: 0px !important;
  }
  
  input.faChkRnd:checked:after, input.faChkRnd:after,
  input.faChkSqr:checked:after, input.faChkSqr:after {
    visibility: visible;
    font-family: FontAwesome;
    font-size:21px;height: 17px; width: 17px;
    position: relative;
    top: -3px;
    left: 0px;
    background-color: #FFF;
      color: {{ $styles->bannerButtonBorderColor }} !important;
    display: inline-block;
  }
  
  input.faChkRnd:checked:after {
    content: '\f058';
  }
  
  input.faChkRnd:after {
    content: '\f10c';
  }
  
  input.faChkSqr:checked:after {
    content: '\f14a';
  }
  
  input.faChkSqr:after {
    content: '\f096';
  }
  
  #menuItemDescription {
      font-style: italic;
      font-size: 14px;
  }
  
  .checkbox label {
      padding-left: 6px;
      font-size: 14px;
      margin-left: 20px;
  }
  
  .col1 {
      width: 70%;
  }
  
  td {
      border-top: none !important;
  }
  
  .modalHeading {
    font-size: 14px;
    font-style: italic;
  }
  
  </style>
  
  <div class="clearfix"></div>
  
  <!-- Modal -->
  
    <div class="modal fade" id="tipModal" tabindex="-1" role="dialog" aria-labelledby="tipModal">
  
      <div class="modal-dialog" role="document">
        
        <div class="modal-content">
          
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Change Tip</h3>
          </div>
  
          <div class="modal-body">
  
            <input id="modalTipAmount" type="number" class="form-control" value="{{ number_format($tip['tip'], 2) ?? 0 }}" step=".01" placeholder="Tip Amount">
  
            <div style="height: 12px;"></div>
          
            <div class="ibox float-e-margins">
              
              <div class="ibox-title">
                  <h5>Tip Suggestions For ${{ number_format($subTotal, 2) }}</h5>
              </div>
              
              <div class="ibox-content">
                  <table class="table table-hover">
                      <thead>
                      <tr>
                          <th>Percent</th>
                          <th>Tip Amount</th>
                          <th>Easy Tip</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <td>35%</td>
                          <td>${{ number_format(($subTotal) * .35, 2) }}</td>
                          <td><button type="button" class="btn btn-default btn-xs" onclick="$('#modalTipAmount').val('{{ number_format(($subTotal) * .35, 2) }}');"> Add 35% </button></td>
                      </tr>
                      <tr>
                          <td>30%</td>
                          <td>${{ number_format(($subTotal) * .30, 2) }}</td>
                          <td><button type="button" class="btn btn-default btn-xs" onclick="$('#modalTipAmount').val('{{ number_format(($subTotal) * .3, 2) }}');"> Add 30% </button></td>
                      </tr>
                      <tr>
                          <td>25%</td>
                          <td>${{ number_format(($subTotal) * .25, 2) }}</td>
                          <td><button type="button" class="btn btn-default btn-xs" onclick="$('#modalTipAmount').val('{{ number_format(($subTotal) * .25, 2) }}');"> Add 25% </button></td>
                      </tr>
                      <tr>
                          <td>20%</td>
                          <td>${{ number_format(($subTotal) * .20, 2) }}</td>
                          <td><button type="button" class="btn btn-default btn-xs" onclick="$('#modalTipAmount').val('{{ number_format(($subTotal) * .2, 2) }}');"> Add 20% </button></td>
                      </tr>   
                      
                      </tbody>
                  </table>
              </div>
            </div>
        </div>
  
        <div class="modal-footer">
          <button type="button" class="btn btn-default w100" data-dismiss="modal">Cancel</button>
          <button type="button" id="addTipButton" class="btn btn-order w100">Change Tip</button>
        
        </div>
      </div>
  
    </div>
  
  </div>
  <script type="text/javascript">
  
    $( window ).on('load', function() {
      var data = JSON.parse(localStorage.getItem('dineTableSide'));
      
      var tip = $('#pickupTipAmount').val();
      var newTotal = $('#pickupTotal').val();
      // $('#verifyAddressButton').hide();
      //$('#placeOrderButton').show();
      
      
      var orderTotal = parseFloat(newTotal) + parseFloat(tip);
      orderTotal = parseFloat(orderTotal).toFixed(2);
      $('#total').val(orderTotal);
      $('#tip').val(tip);
      $('#modalTipAmount').val(tip);
      var t = parseFloat(tip).toFixed(2);
      var p = '$' + t.toString();
      var tl = '$' + orderTotal.toString();
      $('#orderTip').html(p);
      $('#orderTotal').html(tl);
    });


    $('#addTipButton').click(function () {
      var tip = $('#modalTipAmount').val();
      var oldTip = $('#tip').val();
      if ( isNaN(tip) || !tip ) {
          alert("The tip must be a numerical amount - please check your tip.");
          $('#modalTipAmount').val(oldTip);
          return false;
      }
  
      tip = parseFloat(tip).toFixed(2);
      


      var newTotal = $('#pickupTotal').val();
      var require = $('#requirePickupTip').val();
      if (require == 1) {
        var requiredAmount = $('#pickupTipAmount').val();
        if (parseFloat(tip) < parseFloat(requiredAmount)) { 
          tip =  requiredAmount; 
          alert("A mininum tip of $" + tip + ' is required for pickup.');
        }
      }

      var orderTotal = parseFloat(newTotal) + parseFloat(tip);
      orderTotal = parseFloat(orderTotal).toFixed(2);
      $('#total').val(orderTotal);
      $('#tip').val(tip);
      var t = parseFloat(tip).toFixed(2);
      var p = '$' + t.toString();
      var tl = '$' + orderTotal.toString();
      $('#orderTip').html(p);
      $('#orderTotal').html(tl);
      $("#tipModal").modal("hide");
      return true


    });
  </script>
  
  
  
  
  
  <!-- end Tip Modal -->