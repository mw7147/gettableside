<style>
    .locationAddress {
        margin-top: {{ $styles->locationAddressTopMargin ?? '-36' }}px;"> }}; 
        margin-bottom: 15px; 
        margin-right: 3%; 
        font-size: 14px;
        color: {{ $styles->locationAddressFontColor ?? '' }};"> }};
    }

    @media (min-width: 400px) and (max-width: 1200px) {
        .locationAddress {
            font-size: 11px;
        }

        .m-t-xs {
            margin-top: 0px;
        }

        .yourOrder {
            font-size: 11px;
        }

        .w150 {
            min-width: 90px !important;
            font-size: 14px;
        }
    }

    @media (max-width: 499px) {
        .locationAddress {
            font-size: 10px;
        }

        .m-t-xs {
            margin-top: 0px;
        }

        .yourOrder {
            font-size: 11px;
        }

        .w150 {
            min-width: 70px !important;
        }
    }

    @media (max-width: 1024px) {
        .ibox-content {
            height: 100%;
        }
    }

    #page-wrapper {
        padding: 0px 0px;
    }

    .container {
        width: 98%;
    }


</style>