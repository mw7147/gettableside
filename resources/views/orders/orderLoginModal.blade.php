
<div class="clearfix"></div>

<!-- Login Modal -->

<!-- Modal -->
<form name="login" id="login" method="POST" action="/viewcartlogin">
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title">{{$domainSiteConfig->locationName}} Login</h3>
        </div>
        <div class="modal-body">

          <input id="email" type="email" class="form-control" name="email" value="" required autofocus placeholder="Email Address">
          <div style="height: 12px;"></div>
          <input id="password" type="password" class="form-control" name="password" value="" required autofocus placeholder="Password">
        </div>

        <div class="modal-footer">
          {{ csrf_field() }}
          <input type="hidden" name="uri" value="{{ str_replace('/', '', $_SERVER['REQUEST_URI']) }}">
          <button type="button" class="btn btn-default w100" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-order w100">Login</button>

          <p class="m-t-sm"><a href="{{ url('/forgot/password') }}"><i>Forgot Password</i></a></p>
        
        </div>
      </div>
    </div>
  </div>
</form>


<!-- end Login Modal -->