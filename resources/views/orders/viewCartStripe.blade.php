
@extends('orders.iframe')

@section('content')

@include('orders.cssUpdates')

    <style>
      .stripe-button-el { display: none }
      .w225 { width: 225px; }
      .mt-40 { margin-top: 40px; }
    </style>

    <div class="topSpace"></div>
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="view-order-vue" data="{{$vueData}}" open="{{$open}}">
    
    @if($domain['type'] <= 10)

        <span class="pull-right locationAddress">
        <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
        @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
        @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
        @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
        @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
        </span>

        <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

    @endif  

      
    <div class="clearfix"></div>

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
    <div class="ibox float-e-margins">


        @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
        @endif

        @if(Session::has('cardError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('cardError') }}
          </div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
        @endif

      <div class="clearfix"></div>

      <div class="col-md-9 col-sm-12 col-xs-12 m-b-sm">
        <a class="btn btn-xs btn-default w150 m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        @auth
        <a class="btn btn-default btn-xs w150 m-r-xs" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a>
        <a class="btn btn-default btn-xs w150" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>
        @endauth

        @guest
        <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" ><i class="fa fa-user m-r-sm"></i>Login</button>
        <a class="btn btn-default btn-xs w150" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
        @endguest

        <tgm-delivery-detail ref="deliveryDetail" :schedule="schedule" :config="config" :init="delivery" :states="states" :open="open" @delivery-saved="setDeliveryInfo"></tgm-delivery-detail>

      </div>
      <div class="clearfix"></div>
        
        <div class="ibox-title">
            <h5><i class="fa fa-pencil m-r-xs"></i>My Order</h5>
        </div>
        
        <div class="ibox-content">

            @if ($total == 0)
                <p>No items have been ordered.</p>
                <a href="/order" class="btn btn-xs banner-button" ><i class="fa fa-reply m-r-sm"></i>Return To Menu</a>

            @else  
                {!! $html !!}
            @endif

        </div>

    </div>
</div>

@if ($total > 0)
<form method="POST" id="placeOrderForm" action="/orderpayment/stripe">
    <input type="hidden" name="sid" value="{{ $sid }}">
    <input type="hidden" name="did" value="{{ $domain['id'] }}">
    <input type="hidden" name="ownerID" value="{{ $domain['ownerID'] }}">
    <input type="hidden" name="subTotal" id="subTotal" value="{{ $subTotal }}">
                            
    <input type="hidden" name="orderDiscount" id="orderDiscount" value="{{ $orderDiscount }}">
    <input type="hidden" name="orderDiscountPercent" id="orderDiscountPercent" value="{{ $orderDiscountPercent }}">

    <input type="hidden" name="userID" value="{{ Auth::id() }}">
    <input type="hidden" name="tax" id="tax" value="{{ $tax }}">
    <input type="hidden" name="tip" id="tip" value="{{ $tip }}">
    <input type="hidden" name="total" id="total" value="{{ $total }}">
    <input type="hidden" name="userAgent" value="{{ $_SERVER['HTTP_USER_AGENT'] }}">
    <input type="hidden" id="placed" value="0">
{{--  set by delivery component in tgm-hidden-fielder    
    @if ($domainSiteConfig->delivery == "yes")
    <input type="hidden" name="radius" id="radius" value="{{ $domainSiteConfig->deliveryRadius ?? 1 }}">
    <input type="hidden" name="deliveryLatitude" id="deliveryLatitude" value="">
    <input type="hidden" name="deliveryLongitude" id="deliveryLongitude" value="">
    <input type="hidden" name="deliveryFeet" id="deliveryFeet" value="">
    @endif  --}}

    <tgm-hidden-fielder :source="delivery"></tgm-hidden-fielder>

            
    {{ csrf_field() }}
            

    <a href="/order" class="btn banner-button w225" style="margin-top: 40px;" name="toMenu" ><i class="fa fa-arrow-left m-r-sm"></i>Back To Menu</a>
    @if ($domainSiteConfig->delivery == "yes")
    <button type="button" class="btn banner-button w225 mt-40" id="verifyAddressButton" name="verifyAddress" >Enter Payment Information<i class="fa fa-credit-card m-l-sm"></i></button>
    @endif

    <button type="submit" class="btn banner-button w225 mt-40" id="placeOrderButton" name="placeOrder" @if ($domainSiteConfig->delivery == "yes") style="display: none;" @endif >Enter Payment Information<i class="fa fa-credit-card m-l-sm"></i></button>

</form>
@endif
</div>
<div class="clearfix"></div>

@include('orders.orderLoginModal')

@include('orders.cartTipModal')

<div class="clearfix"></div>
<script
        src="https://checkout.stripe.com/checkout.js" 
        class="stripe-button" 
        id="stripeButton"
        data-key="{{ $key }}"
        data-amount="{{ $total*100 }}"
        data-name="{{ $domainSiteConfig->locationName }}"
        data-email="@if(isset($contact->email)){{$contact->email}}@endif"
        data-description="togomeals.biz"
        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
        data-locale="auto">
    </script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$( document ).ready(function() {

    @if ($domain['type'] > 2)
    $('#tipAmount').change(function() {

      var tip = $('#tipAmount').val();
      var noTipTotal = $('#noTipTotal').val();
      
      if (isNaN(tip)) { 
        $('#tipAmount').val(''); 
        $('#orderTotal').html("$" + noTipTotal);
        $('#total').val(noTipTotal);
        $('#tip').val('');
        alert("The tip must be a number"); 
        return false; 
      }

      var total = $('#total').val();
      var newTotal = (parseFloat(total) + parseFloat(tip)).toFixed(2);
      $('#total').val(newTotal);
      var totalPennies = newTotal * 100;
      alert(totalPennies);
      $('#tip').val(tip);
      $('#tipAmount').val('$' + parseFloat(tip).toFixed(2));
      $('#orderTotal').html("$" + newTotal);
      $('#stripeButton').attr("data-amount", totalPennies);
    });
    @endif

});

function deleteItem(itemID) {
  
    var sure = confirm("Are you sure you want to delete?");
    if (sure == false) { return false; }
    
    var fd = new FormData();
    fd.append('itemID', itemID),    

      $.ajax({
          url: "/api/v1/deleteitem",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             location.reload();
          },
          error: function() {
            alert("There was an problem deleting the item. Please try again.");
          }
      }); 

};

</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $domainSiteConfig->googleMapKey }}"></script>

<script type="text/javascript">

    $('#verifyAddressButton').click(function() {
        var total = $('#total').val(); 
        var minOrder = {{ $domainSiteConfig->deliveryMinOrder }};
        if (total < minOrder ) {
            alert("Our minimum order for delivery is $" + minOrder + ". Please increase your order or use our pickup service.");
            return false;
        }
        //let deliver = getLocation();
        let po = $('#placeOrderForm');
        po.find(':submit').click();
    });

</script>


@endsection