<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="clearfix"></div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="confirmUserMode">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="userTypePopup">
          <div class="row">
            <div class="col-sm-6 borderRight">
              <p>Proceed as Guest</p>
              <button type="button" class="btn btn-guest btn-default" onclick="guest()">
                Guest
              </button>
              <button type="button" style="visibility: hidden;" @click="validateInfo()" id="validateInfo">
              </button>
            </div>
            <div class="col-sm-6 borderLeft">
              <p class="login">Login</p>
              <form class="loginForm" id="orderCartLogin">
                <div class="form-group">
                  <label>User Name</label>
                  <input type="email" class="form-control" name="email" value="" required autofocus placeholder="Email Address">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" value="" required placeholder="Password">
                </div>
                <button type="button" class="btn btn-login btn-default" data-dismiss="modal" onclick="orderCartLogin()">
                  Login
                </button>
                <p class="m-t-sm"><a href="{{ url('/forgot/password') }}"><i>Forgot Password</i></a></p>
              </form>
            </div>
            {{--<div class="col-sm-12 col-md-12">
              <div class="socialLogin">
                <p>or login with</p>
                <div class="socialIcon">
                  <a href="/social/facebook?mobile={{$mobile}}"><i class="fa fa-facebook"></i></a>
                  <a href="/social/google?mobile={{$mobile}}"><i class="fa fa-google"></i></a>
                </div>
              </div>
            </div>--}}
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script>
  function guest(){
    $('#orderCartModal').modal('hide');
    $('#confirmUserMode').modal('hide');
    localStorage.setItem("loginOrGuest", 'guest');
    $("#validateInfo").click();
  }
  function orderCartLogin(){
    var formData = new FormData($('#orderCartLogin')[0]);
    $.ajax({
      url: "/orderviewcartlogin",
      type: "POST",
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      },
      data: formData, 
      contentType: false,
      cache: false,
      processData:false, 
      success: function(data) {
        console.log(data);
          if(data.status == 'success'){
            $("#validateInfo").click();
          }else{
            // alert(data.status);
          }
      },
      error: function() {
        alert("There was an problem login the user. Please try again.");
      }
    }); 
  }
</script>