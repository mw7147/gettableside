<style>

    #page-wrapper {
        padding: 15px 15px !important;
    }

    .locationAddress {
        margin-top: 0px; 
        margin-bottom: 15px; 
        margin-right: 3%; 
        font-size: 14px;
        color: {{ $styles->locationAddressFontColor ?? '' }};"> }};
    }

    .imgIframe {
        margin-top: 0px !important;
        max-height: 90px !important;
    }

    .w150 {
        font-size: 14px;
    }

    @media (min-width: 400px) and (max-width: 1200px) {
        .locationAddress {
            font-size: 11px;
        }

        .m-t-xs {
            margin-top: 0px;
        }

        .yourOrder {
            font-size: 11px;
        }

        .w150 {
            min-width: 90px !important;
        }

    }

    #placeOrderButton {
        margin-top: 12px;
    }

    @media (max-width: 499px) {
        .locationAddress {
            font-size: 10px;
        }

        .m-t-xs {
            margin-top: 0px;
        }

        .yourOrder {
            font-size: 11px;
        }

        .w150 {
            min-width: 70px !important;
        }

        .w200 {
            min-width: 90px !important;
        }

        .m-r-xs {
            margin-right: 2px;
        }
    }

    @media (max-width: 1024px) {
        .ibox-content {
            height: 100%;
        }
    }

    #page-wrapper {
        padding: 0px 0px;
    }

    .container {
        width: 98%;
    }

    .alert {
        margin-left: 15px;
        margin-right: 45px;
        margin-top: 15px;
        margin-bottom: 15px;
        width: 92%;
    }

    .dineInPayment {
        margin-top: -18px; 
        margin-bottom: 18px; 
        padding: 0px; 
        background-color: white;
        min-height: 325px;
    }

    .dinerInfo {
        width: 100%;
    }
    
    @media (max-width: 992px) {
        .dinerInfo {
            margin-left: 8px;
            margin-bottom: 8px;
        }

        .dineInPayment {
            min-height: 555px;
        }

        .dineInTop {
            margin-top: 8px;
        }

        #placeOrderButton {
            margin-top: 16px;
        }
        .dinerInfo {
        width: 100%;
    }
    }


</style>