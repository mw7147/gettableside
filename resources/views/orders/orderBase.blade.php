
@include('orders.cssUpdates')

{{-- Hide Close button on clear cart dialog --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .ui-dialog .ui-dialog-titlebar-close {
        display: none !important;
    }
    .ml10 {
        margin-left: 10px;
        margin-top: 10px;
    }
    .cartButtons {
        display: none;
    }
    .orderImage {
        width: 100%;
        margin-top: 15px;
    }
    .priceSection {
        display: flex;
        flex-direction: row;
        margin-bottom: 10px;
        width: 30%;
    }
    .descriptionSection span {
        margin-top: 10px;
        float:left;
        font-weight: bold;
        font-size: 16px;
    }
    .foodDescription {
        display: flex;
        flex-direction: row;
        padding: 0 10px;
    }
    .descriptionSection {
        float: left;
        width: 100%;
    }
    .descriptionSection p {
        padding: 10px 5px 5px 5px;
    }
    .descriptionSection .imageIcons{
        padding: 10px 5px 5px 5px;
    }
    @media(max-width: 767px) {
        #cartOrder{
            display: none;
        }
        body {
            overflow-x: hidden;
        }
        #page-wrapper {
            overflow-x: hidden;
        }
        .cartButtons {
            display: flex;
            float: left;
            width: 100%;
            justify-content: space-between;
            flex-direction: row;
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0px;
            padding: 10px 0;
            z-index: 9;
        }
        .w-100 {
            width: 47%;
            margin-right: auto;
            margin-left: auto;
        }
        .order-no-button {
            background: yellow;
            color: #000;
            font-weight: 600;
            padding: 5px 0;
            font-size: 14px;
        }
        .complete-order-button1 {
            background: green;
            color: #fff;
            font-weight: 600;
            padding: 5px 0;
            font-size: 14px;
        }
        .orderImage {
            width: 100%;
            margin-top: 7px;
        }
        .descriptionSection {
            float: left;
            width: 60%;
        }
        .priceSection {
            width: 40%;
        }
    }
.flexBox {
    display: flex;
    justify-content: flex-end;
}
.borderRight p {
    margin-bottom: 20px;
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    color: #333;
  }
  .borderLeft {
    border-left: 1px solid rgba(204, 204, 204, 0.42);
  }

  .socialLogin {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin-top: 20px;
  }

  .socialLogin p {
    font-size: 16px;
    margin-bottom: 10px;
    position: relative;
    width: 100%;
    text-align: center;
    color: #333;
  }

  .login {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    color: #333;
  }

  .socialIcon {
    display: flex;
    flex-direction: row;
  }
.borderLeft label {
    color: #333;
    font-weight: 700;
    font-style: normal;
    margin-bottom: 5px;
}
.borderLeft .form-group {
    margin-bottom: 0px;
}
  .socialIcon .fa-facebook {
    margin: 0 10px;
    font-size: 16px;
    width: 40px;
    height: 40px;
    background: #3b5998;
    color: #fff;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }

  .socialIcon .fa-google {
    margin: 0 10px;
    font-size: 16px;
    width: 40px;
    height: 40px;
    background: #f13a30;
    color: #fff;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }

  .socialLogin p:before {
    content: '';
    width: 41%;
    height: 1px;
    background: #bbb;
    position: absolute;
    left: 0%;
    top: 12px;
  }

  .socialLogin p:after {
    content: '';
    width: 41%;
    height: 1px;
    background: #bbb;
    position: absolute;
    right: 0%;
    top: 12px;
  }

  .btn-login {
    display: block;
    margin: 20px auto;
    border: 1px solid #084645;
    background-color: #9d7b21;
  }
  .btn-login:hover {
    border: 1px solid #084645;
    background-color: #9d7b21;
  }
  .btn-guest {
    display: block;
    margin: 20px auto;
    background: green;
  }
  .btn-guest:hover {
    background: green;
  }
  .form-control {
      color: #555555;
  }
  .dropdown {
      float: right;
  }
  .open > .dropdown-menu {
    right: 0px;
    left: unset;
  }
  @media (max-width: 767px) {
      .flexBox {
          flex-direction: column;
      }
    .socialLogin p:before {
        width: 34%;
    }
    .socialLogin p:after {
        width: 34%;
    }
    .borderLeft {
        border: medium none;
    }
    .borderRight {
        border-bottom: 1px solid rgba(204, 204, 204, 0.42);
        margin-bottom: 15px;
    }
    .borderRight p {
        margin-bottom: 10px;
    }
    .btn-guest {
        margin: 0 auto;
    }
    .panelMobile {
        margin-bottom: 60px
    }
  }
  .categoryName {
    
  }
  .categoryDescription {
    
  }
  .menuName{
    font-weight: bold;
    font-size: 16px;
  }
  .menu-item-block {
      border-bottom: 1px solid !important;
      border-radius: 4px;
  }
  .panel-menu-item {
    float: left;
    display: flex;
    align-items: center;
    width: 100%;
    flex-direction: row;
  }
  .h5-menu-item {
      margin-bottom: 0px;
  }
</style>

<div class="clearfix"></div>

<div class="row description">    
    @php
    $isMobile = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile");
    if (is_numeric($isMobile)) { $mobile = base64_encode(1); } else {$mobile = base64_encode(0); }
    @endphp


    <div class="container services" id="view-order-vue" data="{{$vueData}}" isMobile="{{$mobile}}" open="{{$open}}" auth="{{auth()->check()}}" googleMapKey="{{$domainSiteConfig->googleMapKey}}">
        @include('orders.orderCartModal')
        @include('orders.orderLoginOrGuestModal')
        <div class="col-md-8 col-sm-8 col-lg-8 col-xs-8 m-b-sm">
            @if($domain['type'] <= 10)
                <img class="imgIframe logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">
                <br>
                <span class="locationAddress">
                    @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
                    @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
                    @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
                    @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
                </span>
            @endif
        </div>  
        @auth
        <div class="col-md-4 col-sm-4 col-xs-4 m-b-sm text-right">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle m-r-xs" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->fname}} {{Auth::user()->lname}}
                    <span class="caret"></span>
                </button>
                
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <li><a class="btn btn-default btn-xs m-r-xs m-b-sm" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a></li>
                    <li><a class="btn btn-default btn-xs m-r-xs m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a></li>
                </ul>
            </div>
        </div>
        @endauth
        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 m-b-sm text-right">
            <div class="flexBox">
                @guest
                <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" >
                    <i class="fa fa-user m-r-sm"></i>Login
                </button>
                <br>
                <a class="btn btn-default btn-xs w150 m-r-xs" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
                @endguest
            </div>
        </div>
        <div class="clearfix"></div>

        @if(Session::has('loginError'))
        <div class="alert alert-danger alert-dismissable col-md-8 col-sm-12 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('loginError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-8 col-sm-12 col-xs-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
          <div class="clearfix"></div>
        @endif

      <div class="col-md-12 m-b-sm" style="margin-top: 10px;">

        <!-- <a class="btn btn-xs btn-default w150 m-r-xs m-b-sm" @click="validateInfo()"  id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a> -->

        {{--@auth
        <a class="btn btn-default btn-xs w150 m-r-xs m-b-sm" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a>
        <a class="btn btn-default btn-xs w150 m-r-xs m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>
        @endauth--}}

        @if ($domainSiteConfig->orderAhead || $open == 'open')
        <tgm-delivery-detail ref="deliveryDetail" :schedule="schedule" :config="config" :init="delivery" :states="states" :open="open" @delivery-saved="setDeliveryInfo"></tgm-delivery-detail>
        @endif

      </div>
      <div class="clearfix" ></div>
      
      @if ($showHeaderClosed == 1 || $open == 'open')
      <div class="col-md-12" style="padding-left: 15px; margin-top: -10px;">
        {!! $menuHeader !!}
      </div>
      @endif
      
      <div class="col-md-4 col-sm-12 col-xs-12 pull-right"
        @if (is_null($menuHeader)) style="margin-top: -10px;"> @endif
        >
        <div class="panel panel-default" id="cartOrder">
            <div class="panel-heading hwct-heading cartTopHeader">
                <span class="yourOrder"><i class="fa fa-pencil-square m-r-sm"></i>Items Ordered</span>
                
                @if ($domainSiteConfig->orderAhead || $open == 'open')
                    @if(auth()->user())
                        <a class="btn btn-xs complete-order-button pull-right m-t-xs" @click="validateInfo()"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                    @else
                        <a class="btn btn-xs complete-order-button pull-right" onclick="$('#confirmUserMode').modal('show');"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                    @endif
                @endif

            </div>
            <div class="panel-body hwct-border">
                <div id="orderCart">
                    {!! $orderHTML !!}
                </div>
                <div class="clearfix"></div>

                @if ($domainSiteConfig->orderAhead || $open == 'open')
                <form method="POST" id="clearCartForm" action="/clearcart">
                <a class="clearCart pull-right" id="clearCart" onclick="clearCart()">clear cart</a>
                {{ csrf_field() }}
                </form>
                @endif

            </div>

        </div>
    </div>

        <div class="col-md-8 col-sm-12 col-xs-12 pull-left">
        
            <div class="panel-body panelMobile" style="padding-top: 0px;">
                <div class="panel-group" id="accordion">

                @if (empty($categories))<h5 class="m-t-lg">Online ordering is only available while our restaurant is open.</h5>@endif
    
                @foreach ($categories as $category)
                    <div class="panel panel-default hwct-border">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                {{--<a class="aBlock" data-toggle="collapse" data-parent="#accordion" href="#{{ Str::kebab($category->categoryName) }}">{{ $category->categoryName }}<i class="fa fa-ellipsis-v pull-right"></i></a>--}}
                                <a class="aBlock categoryName" data-toggle="collapse" data-parent="#accordion" href="#category-{{$category->id}}">{{ $category->categoryName }}<i class="fa fa-ellipsis-v pull-right"></i></a>
                            </h5>
                        </div>
                        {{-- <div id="{{ Str::kebab($category->categoryName) }}" class="panel-collapse collapse"> --}}
                        <div id="category-{{$category->id}}" class="panel-collapse collapse">
                        <div class="panel-body">

                            <p class="pad15 categoryDescription">{{ $category->categoryDescription }}</p>

                            @foreach ($menu as $m)

                                @if ( intval($m->menuCategoriesDataID) == intval($category->id) )
                                
                                    <div class="panel panel-default hwct-border">
                                        <div class="panel-heading panel-menu-item menu-item-block">
                                            <h5 class="h5-menu-item">
                                            
                                            <span class="pull-left menuName">{{ $m->menuItem }}</span>
                                            {{-- <div class="col-sm-12 imageIcons"> --}}
                                                @if ($m->spiceLevel > 0)
                                                <span style="margin-left: 4px;"> 
                                                    @for ($i = 0; $i < $m->spiceLevel; $i++)
                                                        {{-- <i class="fas fa-pepper-hot"></i> --}}
                                                        <span style="margin-left: 4px;"> 
                                                            <img src="{{asset('images/icons/chilly1.png')}}" style="height: 40px">
                                                        </span> 
                                                    @endfor
                                                </span>
                                                @endif
                                                @if ($m->soy)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/soy.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->nut)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/nuts.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->vegan)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/vegan.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->shellfish)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/shellfish.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->meat)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/meat.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->pork)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/pork.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->lactose)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/lactose.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->gluten)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/gluten.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                                @if ($m->egg)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/egg.png')}}" style="height: 40px">
                                                </span>
                                                @endif
                                            {{-- </div> --}}
                                            </h5>
                                        </div>
                                        <div class="panel-body">

                                            <div class="foodDescription">
                                                <div class="descriptionSection">
                                                    <p class="pad15" style="margin-bottom: 0;">{{ $m->menuItemDescription }}</p>
                                                    <span>

                                                        @if (empty($m->price2))
                                                        ${{ number_format($m->price1, 2) }}

                                                        @else

                                                        <?php
                                                            // determin min and max price for menu item
                                                            $array = array($m->price1, $m->price2, $m->price3, $m->price4, $m->price5, $m->price6, $m->price7);
                                                            $prices = Arr::where($array, function ($value, $key) {
                                                                return is_numeric($value);
                                                            });

                                                            $min = number_format(min($prices), 2);
                                                            $max = number_format(max($prices), 2);

                                                        ?>
                                                        ${{ $min }} - ${{ $max }}
                                                        @endif
                                                    </span>
                                                    @if ($domainSiteConfig->orderAhead || $open == "open")
                                                    <button class="btn btn-order btn-xs mt10 ml10 button990" type="button" onclick="orderItem({{ $m->id}}, '{{$m->menuSidesID}}', '{{$m->menuOptionsID}}', '{{$m->menuAddOnsID }}', '{{addslashes($m->menuItem)}}', '{!! addslashes($m->menuItemDescription) !!}')">Order Item</button>
                                                    @endif
                                                </div>
                                                <div class="priceSection">
                                                    @if(count($m->menuImages)>=1)
                                                        @php 
                                                        $path = env('PRIVATE_MENU_IMAGES') . '/' . $m->domainID . '/' . $m->menuImages[0]->menuID . '/' . $m->menuImages[0]->imageSize . '/';
                                                        @endphp
                                                        <img src="{{$path.$m->menuImages[0]->fileName}}" class="orderImage">
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                                      
                                            <div class="clearfix"></div>
                                            <div class="collapse" id="{{ Str::kebab($m->menuItem) }}" style="min-height: 80px;">


                                            </div>

                                        </div>

                                    </div>


                                @endif
                            @endforeach

                            </div>
                        </div>
                    </div>
                @endforeach    

                </div>
            </div>
            <div class="cartButtons" style="background-color: {{ $styles->backgroundColor }};">
                <a class="btn btn-xs order-no-button w-100 m-t-xs" onclick="$('#orderCartModal').modal('show');" id="viewCartBtn"></i>View Cart {{$numberOfOrder}}</a>
                @if ($domainSiteConfig->orderAhead || $open == "open")
                @if(auth()->user())
                    <a class="btn btn-xs complete-order-button1 w-100 m-t-xs" @click="validateInfo()"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                @else
                    <a class="btn btn-xs complete-order-button1 w-100 m-t-xs" onclick="$('#confirmUserMode').modal('show');"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                @endif
                @endif
            </div>
        </div>
    </div>

</div>

<div class="clearfix"></div>

@include('orders.iframeOrderModal')
@include('orders.orderLoginModal')
<div id="cartConfirm" title="Continue Ordering?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Do you need more time to place your order? All items will be removed and your cart reset unless you press the "Continue Ordering" button.</p>
</div>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

function viewCart(){

}
var $ = jQuery.noConflict();
@if(session('social_error'))
alert("{{session('social_error')}}");
@endif
if ( !localStorage.getItem('tgmOrder') ) {
        localStorage['tgmOrder'] = true;
        window.location.reload();
    }  else {
        localStorage.removeItem('tgmOrder');
    }


@auth

window.localStorage.removeItem('delivery');

let delivery = {};

delivery.fname = "{{ $contact->fname ?? '' }}";
delivery.lname = "{{ $contact->lname ?? '' }}";
delivery.email = "{{ $contact->email ?? '' }}";
delivery.mobile = "{{ $contact->mobile ?? '' }}";
delivery.address1 = "{{ $contact->address1 ?? '' }}";
delivery.address2 = "{{ $contact->address2 ?? '' }}";
delivery.city = "{{ $contact->city ?? '' }}";
delivery.State = "{{ $contact->state ?? '' }}";
delivery.zip = "{{ $contact->zip ?? '' }}";

localStorage.setItem("delivery", JSON.stringify(delivery));

@endauth

$('.panel-heading').on('click', function () {
    $('.panel-heading').removeClass('active');
    $(this).addClass('active');
});

// refresh w back button
window.addEventListener( "pageshow", function ( event ) {
  var historyTraversal = event.persisted || ( typeof window.performance != "undefined" &&  window.performance.navigation.type === 2 );
  if ( historyTraversal ) { window.location.reload(); }
});

function clearCart() {
    
    var sure = confirm("Are you sure you want to clear your cart?");
    if (sure == false) { return false; }
    sessionStorage.removeItem("seconds");
    $('#clearCartForm').submit();

};

function deleteItem(itemID) {
  
    var sure = confirm("Are you sure you want to delete?");
    if (sure == false) { return false; }
    
    var fd = new FormData();
    fd.append('itemID', itemID),    

      $.ajax({
          url: "/api/v1/deleteitem",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             location.reload();
          },
          error: function() {
            alert("There was an problem deleting the item. Please try again.");
          }
      }); 

};


function cartTimer() {
    // cart timer
    var seconds = parseInt(sessionStorage.getItem("seconds"));
  
    function tick() {
        seconds--; 
        var keepCart = false;
        sessionStorage.setItem("seconds", seconds);
        if ( seconds > 0 ) {
            setTimeout(tick, 1000);
        } else {

            $( "#cartConfirm" ).dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                        "Continue Ordering": function() {
                            sessionStorage.setItem("seconds", {{ $domainSiteConfig->cartClearTimer * 60 }} );
                            $( this ).dialog( "close" );
                            keepCart = true;
                            cartTimer();
                    }
                }
            });

            if ( !keepCart && sessionStorage.getItem("seconds") == 0 ) { setTimeout(abandonCart, 60000); }

        }

        function abandonCart() {
            if (sessionStorage.getItem("seconds") == 0) {
                $( "#cartConfirm" ).dialog("close");
                sessionStorage.removeItem("seconds");
                @guest
                localStorage.removeItem("delivery");
                @endguest
                $('#clearCartForm').submit();
            }
        };

    }

    // if ( seconds > 0) { tick(); }


}

if ( sessionStorage.getItem("seconds") != null ) { cartTimer(); }

$( document ).ready(function() {
    var googleMapKey = "{{ $domainSiteConfig->googleMapKey }}"
    localStorage.setItem('googleMapKey', googleMapKey);
});
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $domainSiteConfig->googleMapKey }}&libraries=places"></script>