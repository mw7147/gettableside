
<div class="clearfix"></div>

{{-- Location ID Modal --}}


<div class="modal fade" id="tableIDModal" tabindex="-1" role="dialog" aria-labelledby="Table ID" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Select Your Location</h3>
      </div>
      <div class="modal-body">

        <select name="setTableID" id="setTableID" style="width: 80%; min-height: 32px; font-size: 18px;" onchange=selectLocation(this)>
          <option disabled selected value="">-- Select Location --</option>

          @foreach ($tables as $table)
          <option value="{{ $table->id . '_' . $table->name }}">{{ $table->name }}</option>
          @endforeach

        </select>

      </div>

      <div class="modal-footer">

        <button type="submit" class="btn btn-order w150" data-dismiss="modal">Select</button>
      
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  
  $( window ).on('load', function() {
    let table = "{{ $tableID ?? '' }}";
    if (table == '') {
      let table = sessionStorage.getItem("table");
      if (table !== null) {       
        let data = table.split("_");
        $('#showTableID').html('<i class="fa fa-qrcode m-r-sm"></i> ' + data[1] );
      }
    } else {
      let data = table.split("_");
      $('#showTableID').html('<i class="fa fa-qrcode m-r-sm"></i>' + data[1] );
      sessionStorage.setItem("table", table );
    }
  });
  
  function selectLocation(location) {
    let table = $('#setTableID').val();
    if (table == '') {
      let table = sessionStorage.getItem("table");
      let data = table.split("_");
      if (table !== null) { $('#showTableID').html('<i class="fa fa-qrcode m-r-sm"></i> ' + data[1] ); }
    } else {
      let data = table.split("_");
      $('#showTableID').html('<i class="fa fa-qrcode m-r-sm"></i>' + data[1] );
      sessionStorage.setItem("table", table );
    }
  };

</script>




<!-- end Table ID Modal -->