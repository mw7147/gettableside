
@extends('orders.iframe')

@section('content')

@include('orders.cssUpdates')

    <style>
      .stripe-button-el { display: none }
      .w225 { width: 225px; }
      .mt-40 { margin-top: 40px; }
    </style>

    <div class="topSpace"></div>
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
        @if($domain['type'] <= 4)

        <span class="pull-right locationAddress">
        <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
        @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
        @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
        @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
        @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
        </span>

        <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

    @endif  

    </div>  

    <div class="clearfix"></div>

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
    <div class="ibox float-e-margins">


        @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
        @endif

        @if(Session::has('cardError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('cardError') }}
          </div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
        @endif

      <div class="clearfix"></div>

      <div class="col-md-9 col-sm-12 col-xs-12 m-b-sm">
        <a class="btn btn-xs btn-default w150 m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        @auth
        <a class="btn btn-default btn-xs w150 m-r-xs" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Information</a>
        <a class="btn btn-default btn-xs w150" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>
        @endauth

        @guest
        <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" ><i class="fa fa-user m-r-sm"></i>Login</button>
        <a class="btn btn-default btn-xs w150" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
        @endguest
      </div>
      <div class="clearfix"></div>
        
        <div class="ibox-title">
            <h5><i class="fa fa-pencil m-r-xs"></i>My Order</h5>
        </div>
        
        <div class="ibox-content">

            @if ($total == 0)
                <p>No items have been ordered.</p>
                <a href="/order" class="btn btn-xs banner-button" ><i class="fa fa-reply m-r-sm"></i>Return To Menu</a>

            @else  
                {!! $html !!}
            @endif

        </div>

    </div>
</div>

@if ($total > 0)
<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl">
    <div class="ibox float-e-margins">
        
        <div class="ibox-title">
            <h5><i class="fa fa-id-card-o m-r-xs"></i>My Information</h5>
        </div>
        
        
        <div class="ibox-content" style="min-height: 350px;">


            <form method="POST" id="placeOrderForm" action="/orderpayment/onreceipt">
                <div class="form-group">
                    @if ($domainSiteConfig->delivery == "yes")
                    <div class="col-md-3 col-sm-6">
                            <input type="checkbox" id="pickup" name="pickup" value="pickup"><span style="margin-left: 5px;">Pickup</span>
                    </div>

                    <div class="col-md-3 col-sm-6">
                            <input type="checkbox" id="delivery" name="delivery" value="delivery" checked><span style="margin-left: 5px;">Deliver</span>
                    </div>
                    <div class="clearfix"></div>
                    @endif

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="fname" value="@if(isset($contact->fname)){{$contact->fname}}@endif" required placeholder="First Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name</label>
                        <input class="form-control" type="text" name="lname" value="@if(isset($contact->lname)){{$contact->lname}}@endif" required placeholder="Last Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email Address</label>
                        <input class="form-control" type="email" name="email" value="@if(isset($contact->email)){{$contact->email}}@endif" required placeholder="Email Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile Number</label>
                        <input class="form-control" type="tel" id="mobile" name="mobile" value="@if(isset($contact->mobile)){{$contact->mobile}}@endif" required placeholder="Mobile Phone">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address1</label>
                        <input class="form-control" type="text" id="address" name="address1" value="@if(isset($contact->address1)){{$contact->address1}}@endif" placeholder="Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address2</label>
                        <input class="form-control" type="text" name="address2" value="@if(isset($contact->address2)){{$contact->address2}}@endif" placeholder="Address2">
                    </div>

                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <label>City</label>
                        <input class="form-control" type="text" id="city" name="city" value="@if(isset($contact->city)){{$contact->city}}@endif" placeholder="City">
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <label>State</label>
                        <select class="form-control" name="State" id="state">
                            @php if (is_null($contact)) { $contactState = ''; } else { $contactState = $contact->state; } @endphp
                            @foreach ($states as $state)
                            <option value="{{ $state->stateCode }}" @if ($state->stateCode == $contactState)) selected @endif>{{ $state->stateName }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label>Zip Code</label>
                        <input class="form-control" type="text" name="zipCode" value="{{$contact->zip ?? ''}}" placeholder="Zip Code">
                    </div>

                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 m-t-md">
                        <input type="hidden" name="sid" value="{{ $sid }}">
                        <input type="hidden" name="did" value="{{ $domain['id'] }}">
                        <input type="hidden" name="ownerID" value="{{ $domain['ownerID'] }}">
                        <input type="hidden" name="subTotal" id="subTotal" value="{{ $subTotal }}">
                                                
                        <input type="hidden" name="orderDiscount" id="orderDiscount" value="{{ $orderDiscount }}">
                        <input type="hidden" name="orderDiscountPercent" id="orderDiscountPercent" value="{{ $orderDiscountPercent }}">

                        <input type="hidden" name="userID" value="{{ Auth::id() }}">
                        <input type="hidden" name="tax" id="tax" value="{{ $tax }}">
                        <input type="hidden" name="tip" id="tip" value="{{ $tip }}">
                        <input type="hidden" class="orderTotal" name="total" id="total" value="{{ $total }}">
                        <input type="hidden" name="userAgent" value="{{ $_SERVER['HTTP_USER_AGENT'] }}">
                        <input type="hidden" id="placed" value="0">
                        @if ($domainSiteConfig->delivery == "yes")
                        <input type="hidden" name="radius" id="radius" value="{{ $domainSiteConfig->deliveryRadius ?? 1 }}">
                        <input type="hidden" name="deliveryLatitude" id="deliveryLatitude" value="">
                        <input type="hidden" name="deliveryLongitude" id="deliveryLongitude" value="">
                        <input type="hidden" name="deliveryFeet" id="deliveryFeet" value="">
                        @endif
                        
                        {{ csrf_field() }}

                        <a href="/order" class="btn banner-button w225" style="margin-top: 40px;" name="toMenu" ><i class="fa fa-arrow-left m-r-sm"></i>Back To Menu</a>
                        
                        @if ($domainSiteConfig->delivery == "yes")
                        <button type="button" class="btn banner-button w225 mt-40" id="verifyAddressButton" name="verifyAddress" >Place Order<i class="fa fa-credit-card m-l-sm"></i></button>
                        @endif
                        <button type="submit" class="btn banner-button w225 mt-40" id="placeOrderButton" name="placeOrder" @if ($domainSiteConfig->delivery == "yes") style="display: none;" @endif>Place Order<i class="fa fa-credit-card m-l-sm"></i></button>


                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endif

<div class="clearfix"></div>

@include('orders.orderLoginModal')

@include('orders.cartTipModal')

<div class="clearfix"></div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$( document ).ready(function() {
    $("#mobile").mask("(999) 999-9999");

        
   
    @if ($domain['type'] > 2)
    $('#mobile').change(function() {
    var fd = new FormData();
    fd.append('mobile', $(this).val()),    

      $.ajax({
          url: "/api/v1/checkmobile",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
            var data = JSON.parse(mydata);           
            console.log(data);
            if (data["status"] != "registered") {
                $('#mobile').val('');
                alert(data["message"]);
                return false;
            }
            return true;
          },
          error: function() {
            alert("There was an problem with your mobile number. Please try again.");
          }
      }); 
    });

    $('#tipAmount').change(function() {

      var tip = $('#tipAmount').val();
      var noTipTotal = $('#noTipTotal').val();
      
      if (isNaN(tip)) { 
        $('#tipAmount').val(''); 
        $('#orderTotal').html("$" + noTipTotal);
        $('#total').val(noTipTotal);
        $('#tip').val('');
        alert("The tip must be a number"); 
        return false; 
      }

      var total = $('#total').val();
      var newTotal = (parseFloat(total) + parseFloat(tip)).toFixed(2);
      $('#total').val(newTotal);
      var totalPennies = newTotal * 100;
      alert(totalPennies);
      $('#tip').val(tip);
      $('#tipAmount').val('$' + parseFloat(tip).toFixed(2));
      $('#orderTotal').html("$" + newTotal);
      $('#stripeButton').attr("data-amount", totalPennies);
      location.reload();
    });
    @endif

});

function deleteItem(itemID) {
  
    var sure = confirm("Are you sure you want to delete?");
    if (sure == false) { return false; }
    
    var fd = new FormData();
    fd.append('itemID', itemID),    

      $.ajax({
          url: "/api/v1/deleteitem",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             location.reload();
          },
          error: function() {
            alert("There was an problem deleting the item. Please try again.");
          }
      }); 

};

$("#pickup").click(function() {
    $("#delivery").prop('checked', false);

	var deliveryFee = {{ $domainSiteConfig->deliveryCharge }};
	var orderTotal = $('#total').val();
	var newTotal = orderTotal - deliveryFee;
	newTotal = newTotal.toFixed(2);
	$('#deliveryFee').html('$0.00');
	$('#total').val(newTotal);
 	$('#orderTotal').html('$' + newTotal);

    $("#verifyAddressButton").hide();
    document.getElementById("placeOrderButton").style.display = "inline-block";
});

$("#delivery").click(function() {
    $("#pickup").prop('checked', false);
    
    var deliveryFee = {{ $domainSiteConfig->deliveryCharge }};
	var orderTotal = $('#total').val();
	var newTotal = parseFloat(orderTotal) + deliveryFee;
	newTotal = newTotal.toFixed(2);
	$('#deliveryFee').html('$' + deliveryFee.toFixed(2));
	$('#total').val(newTotal);
 	$('#orderTotal').html('$' + newTotal);
    
    $("#placeOrderButton").hide();
    document.getElementById("verifyAddressButton").style.display = "inline-block";
});

</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $domainSiteConfig->googleMapKey }}"></script>

<script type="text/javascript">

    $('#verifyAddressButton').click(function() {
        var total = $('#total').val(); 
        var deliveryFee = {{ $domainSiteConfig->deliveryCharge }};
        var minOrder = {{ $domainSiteConfig->deliveryMinOrder }};
        if (total < minOrder ) {
            alert("Our minimum order for delivery is $" + minOrder + ". Please increase your order or use our pickup service.");
            return false;
        }
        let deliver = getLocation();
    });

    function getLocation() {
        var geocoder = new google.maps.Geocoder();
        var street = document.getElementById("address").value;
        var city = document.getElementById("city").value;
        var state = document.getElementById("state").value;
        var address = street + ',' + city + ',' + state;
        geocoder.geocode({ 'address': street }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("#deliveryLatitude").val(latitude);
                $("#deliveryLongitude").val(longitude);
                console.log(latitude + ' / ' + longitude);
               // alert(latitude + ' / ' + longitude);
                var dist = distance(latitude, longitude);
                console.log(dist + " Feet from location");
                var deliveryRadiusFeet = {{ $domainSiteConfig->deliveryRadius }} * 5280;
                console.log(deliveryRadiusFeet + " Delivery Radius Feet");
                $("#deliveryFeet").val(dist);
                if (dist <= deliveryRadiusFeet) {

                   let po = $('#placeOrderForm');
                    let pov = po[0].checkValidity();
                    if (pov) {
                        po.find(':submit').click();
                    }
                    return false;
                } else {
                    alert("We're sorry - your address is outside of our delivery area. Please use our pickup option for your order.");
                    return false;
                }
            } else {
                alert("Please check your address and try again.");
            }
        });
    };

    function distance(lat2, lon2) {
    // unit default - miles
    var unit = "F";
    var lat1 = {{ $domainSiteConfig->latitude }};
    var lon1 = {{ $domainSiteConfig->longitude }};
    var radlat1 = Math.PI * lat1/180;
    var radlat2 = Math.PI * lat2/180;
    var theta = lon1-lon2;
    var radtheta = Math.PI * theta/180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist); 
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "F") { dist = parseInt(dist * 5280); }
    if (unit == "K") { dist = dist * 1.609344; }
    if (unit == "N") { dist = dist * 0.8684; }
    return dist;
}

</script>


@endsection