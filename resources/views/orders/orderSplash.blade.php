

<div class="clearfix"></div>






<div class="row description">    
    
    <div class="container col-md-12 mb-3" style="background-color: white; margin-left: 5%; margin-top: 3%;">

        <h1  class="text-center mb-4 card-title">{{ $splash->locationName ?? '' }} Locations</h1>
        <p class="text-center" style="margin-bottom: 36px;"><b><i>Select Your Order Location</i></b></p>

        
        
        @foreach ($locations as $loc)

        @if ( count($locations) == 2)

            @if ($loop->first)
                <div class="card col-md-offset-3 col-md-3  mb-3 text-center" style="margin-bottom: 20px;">
            @else 
                <div class="card col-md-3 mb-3 text-center" style="margin-bottom: 20px;">
            @endif

        @elseif ( count($locations) == 3)
        <div class="card col-md-4 mb-3 text-center" style="margin-bottom: 20px;">
        
        @else
        <div class="card col-md-3 mb-3 text-center" style="margin-bottom: 20px;">
        
        @endif
        
            <div class="card-body">
                <h4 class="card-title">{{ $loc->locationName ?? '' }}</h4>
                <p class="card-text">{{ $loc->address1 ?? '' }}<br>
                    @if ( !is_null($loc->address2) ) {{ $loc->address2 }} <br> @endif
                    {{ $loc->city ?? '' }}, {{ $loc->state ?? ''}} {{ $loc->zipCode ?? ''}}
                </p>
                    
                <a href="http://{{ $loc->httpHost }}/order" class="btn btn-primary">Select {{ $loc->locationName ?? '' }}</a>
            </div>
        </div>
        @endforeach

       




    </div>

</div>

     