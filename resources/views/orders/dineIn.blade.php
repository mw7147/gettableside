
@include('orders.cssDineInUpdates')

{{-- Hide Close button on clear cart dialog --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .ui-dialog .ui-dialog-titlebar-close {
        display: none !important;
    }
    .locationAddress {
        float: left;
        width: 100%;
    }
    .description {
        margin-top: 30px;
    }
    .panel-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    #accordion .panel-title {
        width: 100%;
    }
    .ml10 {
        margin-left: 10px;
        margin-top: 10px;
    }
    .cartButtons {
        display: none;
    }
    .orderImage {
        width: 100%;
        margin-top: 15px;
    }
    .priceSection {
        display: flex;
        flex-direction: row;
        margin-bottom: 10px;
        width: 30%;
    }
    .descriptionSection span {
        margin-top: 10px;
        float:left;
        font-weight: bold;
        font-size: 16px;
    }
    .foodDescription {
        display: flex;
        flex-direction: row;
        padding: 0 10px;
    }
    .descriptionSection {
        float: left;
        width: 100%;
    }
    .descriptionSection p {
        padding: 10px 5px 5px 5px;
    }
    .descriptionSection .imageIcons{
        padding: 10px 5px 5px 5px;
    }
    @media(max-width: 767px) {
        #cartOrder{
            display: none;
        }
        .w150 {
            min-width: 70px !important;
        }
        .cartButtons {
            display: flex;
            float: left;
            width: 100%;
            justify-content: space-between;
            flex-direction: row;
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0px;
            padding: 10px 0;
            z-index: 9;
        }
        .flexBox {
            flex-direction: column;
        }
        .flexBox .w-100 {
            font-size: 11px;
        }
        .w-100 {
            width: 49%;
        }
        .order-no-button {
            background: yellow;
            color: #000;
            font-weight: 600;
            padding: 5px 0;
            font-size: 16px;
        }
        .complete-order-button1 {
            background: green;
            color: #fff;
            font-weight: 600;
            padding: 5px 0;
            font-size: 16px;
        }
        .orderImage {
            width: 100%;
            margin-top: 7px;
        }
        .descriptionSection {
            float: left;
            width: 60%;
        }
        .priceSection {
            width: 40%;
        }
        .panelMobile {
            margin-bottom: 60px
        }
    }
    .flexBox {
        display: flex;
        justify-content: flex-end;
    }
    .dropdown {
      float: right;
  }
  .open > .dropdown-menu {
    right: 0px;
    left: unset;
  }
.categoryName {
    
}
.categoryDescription {
  
}
.menuName{
  font-weight: bold;
  font-size: 16px;
}
.menu-item-block {
    border-bottom: 1px solid !important;
    border-radius: 4px;
}
.panel-menu-item {
    float: left;
    display: flex;
    align-items: center;
    width: 100%;
    flex-direction: row;
  }
  .h5-menu-item {
      margin-bottom: 0px;
  }
</style>

<div class="clearfix"></div>

<div class="row description">    
    <div  class="col-sm-8 col-md-8 col-lg-8 col-xl-8 col-xs-8 m-b-sm">
        @php
        $isMobile = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile");
        if (is_numeric($isMobile)) { $mobile = base64_encode(1); } else {$mobile = base64_encode(0); }
        @endphp


    
        @if($domain['type'] <= 10)
            <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">
            <span class="locationAddress">
            <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
            @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
            @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
            @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
            @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
            </span>
        @endif
    </div>

        @auth
        <div class="col-md-4 col-sm-4 col-xs-4 m-b-sm text-right">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle m-r-xs" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->fname}} {{Auth::user()->lname}}
                    <span class="caret"></span>
                </button>
                
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <li><a class="btn btn-default btn-xs m-r-xs m-b-sm" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a></li>
                    <li><a class="btn btn-default btn-xs m-r-xs m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a></li>
                </ul>
            </div>
        </div>
        @endauth
        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 m-b-sm text-right">
            <div class="flexBox">
                @guest
                <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" >
                    <i class="fa fa-user m-r-sm"></i>Login
                </button>
                <br>
                <a class="btn btn-default btn-xs w150 m-r-xs" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
                @endguest
                <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="showTableID" id="showTableID" onclick="$('#tableIDModal').modal('show');" >
                    <i class="fa fa-qrcode m-r-sm"></i>
                    @if (isset($tableID)) @if ( $tableID == '') My Table @else Table: {{ $tableID }} @endif @else My Table @endif
                </button>
            </div>
        </div>
      <div class="clearfix" ></div>

      @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-8 col-sm-12 col-xs-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
          <div class="clearfix"></div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-8 col-sm-12 col-xs-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
          <div class="clearfix"></div>
        @endif
      
      @if ($showHeaderClosed == 1 || $open == 'open')
      <div class="col-md-12" style="padding-left: 15px; margin-top: 10px;">
      {!! $menuHeader !!}
      </div>
      @endif
      
      <div class="col-md-4 col-sm-12 col-xs-12 pull-right"
      @if (is_null($menuHeader)) style="margin-top: 10px;"> @endif
      >
        <div class="panel panel-default" id="cartOrder">
            <div class="panel-heading hwct-heading">
                <span class="yourOrder"><i class="fa fa-pencil-square m-r-sm"></i>Items Ordered</span>
                
                @if ($open == "open")
                <a class="btn btn-xs complete-order-button pull-right" href="/dineincart"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                @endif

            </div>
            <div class="panel-body hwct-border">
                <div id="orderCart">
                    {!! $orderHTML !!}
                </div>
                <div class="clearfix"></div>

                @if ($open == "open")
                <form method="POST" id="clearCartForm" action="/clearcart">
                <a class="clearCart pull-right" id="clearCart" onclick="clearCart()">clear cart</a>
                {{ csrf_field() }}
                </form>
                @endif

            </div>

        </div>
    </div>

        <div class="col-md-8 col-sm-12 col-xs-12 pull-left">
        
            <div class="panel-body panelMobile" style="padding-top: 0px;">
                <div class="panel-group" id="accordion">

                @if (empty($categories))<h5 class="m-t-lg">Online ordering is only available while our restaurant is open.</h5>@endif
    
                @foreach ($categories as $category)
                    <div class="panel panel-default hwct-border">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a class="aBlock categoryName" data-toggle="collapse" data-parent="#accordion" href="#category-{{$category->id}}">{{ $category->categoryName }}<i class="fa fa-ellipsis-v pull-right"></i></a>
                            </h5>
                        </div>
                        <div id="category-{{$category->id}}" class="panel-collapse collapse">
                            <div class="panel-body">

                            <p class="pad15 categoryDescription">{{ $category->categoryDescription }}</p>

                            @foreach ($menu as $m)

                                @if ( intval($m->menuCategoriesDataID) == intval($category->id) )
                                
                                    <div class="panel panel-default hwct-border">
                                        <div class="panel-heading panel-menu-item menu-item-block">
                                            <h5 class="h5-menu-item">
                                            
                                            <span class="pull-left menuName">{{ $m->menuItem }}</span>
                                            {{-- <div class="col-sm-12 imageIcons"> --}}
                                                @if ($m->spiceLevel > 0)
                                                <span style="margin-left: 4px;"> 
                                                    @for ($i = 0; $i < $m->spiceLevel; $i++)
                                                        {{-- <i class="fas fa-pepper-hot"></i>  --}}
                                                        <span style="margin-left: 4px;"> 
                                                            <img src="{{asset('images/icons/chilly1.png')}}" style="height: 40px">
                                                        </span>
                                                    @endfor
                                                </span>
                                                @endif
                                                @if ($m->soy)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/soy.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->nut)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/nuts.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->vegan)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/vegan.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->shellfish)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/shellfish.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->meat)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/meat.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->pork)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/pork.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->lactose)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/lactose.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->gluten)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/gluten.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                                @if ($m->egg)
                                                <span style="margin-left: 4px;"> 
                                                    <img src="{{asset('images/icons/egg.png')}}" style="height: 40px;">
                                                </span>
                                                @endif
                                            {{-- </div> --}}
                                            </h5>
                                        </div>
                                        <div class="panel-body">

                                        <div class="foodDescription">
                                            <div class="descriptionSection">
                                                <p class="pad15" style="margin-bottom: 0;">{{ $m->menuItemDescription }}</p>
                                                <span>

                                                @if (empty($m->price2))
                                                ${{ number_format($m->price1, 2) }}

                                                @else

                                                <?php
                                                    // determin min and max price for menu item
                                                    $array = array($m->price1, $m->price2, $m->price3, $m->price4, $m->price5, $m->price6, $m->price7);
                                                    $prices = Arr::where($array, function ($value, $key) {
                                                        return is_numeric($value);
                                                    });

                                                    $min = number_format(min($prices), 2);
                                                    $max = number_format(max($prices), 2);

                                                ?>
                                                ${{ $min }} - ${{ $max }}
                                                @endif
                                                </span>
                                                @if ($open == "open")
                                                <button class="btn btn-order btn-xs mt10 ml10 button990" type="button" onclick="orderItem({{ $m->id}}, '{{$m->menuSidesID}}', '{{$m->menuOptionsID}}', '{{$m->menuAddOnsID }}', '{{addslashes($m->menuItem)}}', '{!! addslashes($m->menuItemDescription) !!}')">Order Item</button>
                                                @endif
                                            </div>
                                            <div class="priceSection">
                                                @if(count($m->menuImages)>=1)
                                                    @php 
                                                    $path = env('PRIVATE_MENU_IMAGES') . '/' . $m->domainID . '/' . $m->menuImages[0]->menuID . '/' . $m->menuImages[0]->imageSize . '/';
                                                    @endphp
                                                    <img src="{{$path.$m->menuImages[0]->fileName}}" class="orderImage">
                                                @endif
                                            </div>
                                            </div>
                                            <div class="clearfix"></div>
                                                      
                                            <div class="clearfix"></div>
                                            <div class="collapse" id="{{ Str::kebab($m->menuItem) }}" style="min-height: 80px;">


                                            </div>

                                        </div>

                                    </div>


                                @endif
                            @endforeach

                            </div>
                        </div>
                    </div>
                @endforeach    

                </div>
            </div>
            <div class="cartButtons" style="background-color: {{ $styles->backgroundColor }};">
                <a class="btn btn-xs order-no-button w-100 m-t-xs" onclick="$('#orderCartModal').modal('show');" id="viewCartBtn"></i>View Cart {{$numberOfOrder}}</a>
                @if ($open == "open")
                <a class="btn btn-xs complete-order-button1 w-100 m-t-xs" href="/dineincart"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                @endif
            </div>
        </div>
        </div>
    </div>

</div>

<div class="clearfix"></div>

@include('orders.iframeOrderModal')
@include('orders.orderLoginModal')
@include('orders.dineInTableModal')
@include('orders.dineInCartModal')


<div id="cartConfirm" title="Continue Ordering?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Do you need more time to place your order? All items will be removed and your cart reset unless you press the "Continue Ordering" button.</p>
</div>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

var $ = jQuery.noConflict();

if ( !localStorage.getItem('dineIn') ) {
        localStorage['dineIn'] = true;
        window.location.reload();
    }  else {
        localStorage.removeItem('dineIn');
    }

@auth
@include('orders.dineLocalStorage')
@endauth





$('.panel-heading').on('click', function () {
    $('.panel-heading').removeClass('active');
    $(this).addClass('active');
});

// refresh w back button
window.addEventListener( "pageshow", function ( event ) {
  var historyTraversal = event.persisted || ( typeof window.performance != "undefined" &&  window.performance.navigation.type === 2 );
  if ( historyTraversal ) { window.location.reload(); }
});

function clearCart() {
    
    var sure = confirm("Are you sure you want to clear your cart?");
    if (sure == false) { return false; }
    sessionStorage.removeItem("seconds");
    $('#clearCartForm').submit();

};

function deleteItem(itemID) {
  
    var sure = confirm("Are you sure you want to delete?");
    if (sure == false) { return false; }
    
    var fd = new FormData();
    fd.append('itemID', itemID),    

      $.ajax({
          url: "/api/v1/deleteitem",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             location.reload();
          },
          error: function() {
            alert("There was an problem deleting the item. Please try again.");
          }
      }); 

};

$('#expiryMonth').on('change', function() {
    let em = $('#expiryMonth').val();
    $('#expireMonth').val(em);
});

$('#expiryYear').on('change', function() {
    let em = $('#expiryYear').val();
    $('#expireYear').val(em);
});


function cartTimer() {
    // cart timer
    var seconds = parseInt(sessionStorage.getItem("seconds"));
  
    function tick() {
        seconds--; 
        var keepCart = false;
        sessionStorage.setItem("seconds", seconds);
        if ( seconds > 0 ) {
            setTimeout(tick, 1000);
        } else {

            $( "#cartConfirm" ).dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                        "Continue Ordering": function() {
                            sessionStorage.setItem("seconds", {{ $domainSiteConfig->cartClearTimer * 60 }} );
                            $( this ).dialog( "close" );
                            keepCart = true;
                            cartTimer();
                    }
                }
            });

            if ( !keepCart && sessionStorage.getItem("seconds") == 0 ) { setTimeout(abandonCart, 60000); }

        }

        function abandonCart() {
            if (sessionStorage.getItem("seconds") == 0) {
                $( "#cartConfirm" ).dialog("close");
                sessionStorage.removeItem("seconds");
                @guest
                localStorage.removeItem("delivery");
                @endguest
                $('#clearCartForm').submit();
            }
        };

    }

    if ( seconds > 0) { tick(); }


}

if ( sessionStorage.getItem("seconds") != null ) { cartTimer(); }

</script>
