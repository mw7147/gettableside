$( document ).ready(function() {

    localStorage.removeItem("dineTableSide");
    var user = {

        fname: "{{ $user->fname ?? ''}}",
        lname: "{{ $user->lname ?? ''}}",
        mobile: "{{ $user->mobile ?? ''}}",
        email: "{{ $user->email ?? ''}}",
        last4: "{{ $user->last4 ?? ''}}",
        eMonth: "{{ $user->eMonth ?? ''}}",
        eDay: "{{ $user->eDay ?? ''}}",
        userID: "{{ $user->userID ?? ''}}",
        domainID: "{{ $user->domainID ?? ''}}",
        parentDomainID: "{{ $user->parentDomainID ?? ''}}",
        lastLogin: Date.now()
    }
    localStorage.setItem( "dineTableSide", JSON.stringify(user) );

});