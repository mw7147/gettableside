
<!-- Page-Level CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.min.css">

<div class="clearfix"></div>

<div class="row description">    

    <div class="container services">

    @if($domain['type'] <= 10)
    
    <div>
    <span class="pull-right" style="margin-top: -42px; margin-bottom: 15px; margin-right: 3%;">
    <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
    {{ $domainSiteConfig->address1 }}<br>
    @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
    {{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>
    <i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}</span>
    </div>  

    <img class="imgIframe pull-left logo" src="{{ $domain['logoPublic'] }}">
    @endif  
    <div class="clearfix"></div>

        @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-8 col-sm-12 col-xs-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
          <div class="clearfix"></div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-8 col-sm-12 col-xs-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
          <div class="clearfix"></div>
        @endif

      <div class="col-md-6 m-b-sm">

        <a class="btn btn-xs btn-default w150 m-r-xs" href="/viewcart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>

        @auth
        <a class="btn btn-default btn-xs w150" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Information</a>
        <a class="btn btn-default btn-xs w150" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>
        @endauth

        @guest
        <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" ><i class="fa fa-user m-r-sm"></i>Login</button>
        <a class="btn btn-default btn-xs w150" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
        @endguest
      </div>
      <div class="clearfix"></div>
      
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right">
        <div class="panel panel-default">
            <div class="panel-heading hwct-heading">
                <span class="yourOrder"><i class="fa fa-pencil-square m-r-sm"></i>Items Ordered</span>

                @if ($open == "open")
                <a class="btn btn-xs complete-order-button pull-right m-t-xs" href="/viewcart"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                @endif

            </div>
            <div class="panel-body hwct-border">
                <div id="orderCart">
                    {!! $orderHTML !!}
                </div>
                <div class="clearfix"></div>

                @if ($open == "open")
                <a class="clearCart pull-right" id="clearCart" onclick="clearCart('{{ $sid }}')">clear cart</a>
                @endif

            </div>

        </div>
    </div>

<style>
    .categoryImage { 
        height: 100%;
        width: 100%;
        margin-top: 8px;
    }
    .categoryMargin {
        margin: 6px 0px !important;
        padding: 0px 3px !important;
    }

    .aBlock {
        font-size: 16px;
        margin-bottom: 12px;
    }

    .panel-body {
        min-height: 100px;
    }

    .pad15 {
        min-height: 50px;
        font-size: 14px;
        margin-top: 12px;
        width: 95%;
        font-style: italic;
    }

    .m-t-12 {
        margin-top: 12px;
    }

    .menuImage {
        margin: 8px 12px;
    }
    .menuImage img {
        max-height: 60px;
        max-width: 107px;
        display: inline-block;
    }

    @media (min-width: 1200px) and (max-width: 1500px) {
        .button1200 {
            margin-left: -12px;
        }
    }
</style>

        <div class="col-md-8 col-sm-12 col-xs-12 pull-left">
            <div class="panel-body" style="padding-top: 0px;">
                <div class="panel-group" id="accordion">

                @if (empty($categories))<h5 class="m-t-lg">Online ordering is only available while our restaurant is open.</h5>@endif
    
                @foreach ($categories as $category)
                    <div class="panel panel-default hwct-border col-md-6 col-sm-6 col-xs-12 categoryMargin">
                        <div class="panel-heading">

                            <h5 class="panel-title">
                                @if (empty($category->fileName))
                                <a class="aBlock text-center" data-toggle="collapse" data-parent="#accordion" href="#{{ Str::kebab($category->categoryName) }}">{{ $category->categoryName }}<i class="fa fa-ellipsis-v pull-right"></i><br>    
                                <img class="categoryImage empty-icon"><span><i class="fa fa-photo fa-4x"></i></span>
                                </a>
                                @else
                                <a class="aBlock text-center" data-toggle="collapse" data-parent="#accordion" href="#{{ Str::kebab($category->categoryName) }}">{{ $category->categoryName }}<i class="fa fa-ellipsis-v pull-right"></i><br>
                                <img class="categoryImage" src="{{ env('PRIVATE_CATEGORY_IMAGES') . $category->domainID . '/' . $category->menuCategoriesID . '/' . $category->fileName }}">
                                </a>
                                @endif
                            </h5>            

                        </div>
                        <div id="{{ Str::kebab($category->categoryName) }}" class="panel-collapse collapse">
                            <div class="panel-body">

                            <p class="pad15">{{ $category->categoryDescription }}</p>

                            @foreach ($menu as $m)

                                @if ($m->menuCategoriesDataID == $category->id)
                                
                                    <div class="panel panel-default hwct-border">
                                        <div class="panel-heading panel-menu-item">
                                            <h5 class="h5-menu-item">
                                            <span class="pull-right">

                                            @if (empty($m->price2))
                                            ${{ number_format($m->price1, 2) }}

                                            @else

                                            <?php
                                                // determin min and max price for menu item
                                                $array = array($m->price1, $m->price2, $m->price3, $m->price4, $m->price5, $m->price6, $m->price7);
                                                $prices = Arr::where($array, function ($value, $key) {
                                                    return is_numeric($value);
                                                });

                                                $min = number_format(min($prices), 2);
                                                $max = number_format(max($prices), 2);

                                            ?>
                                            ${{ $min }} - ${{ $max }}
                                            @endif



                                            </span>

                                            <span class="pull-left">{{ $m->menuItem }}</span>
                                            </h5>
                                        </div>
                                        <div class="panel-body">

                                            <div class="col-md-7 col-lg-9 m-l-9 ">
                                                <p class="pad15">{{ $m->menuItemDescription }}</p>
                                            </div>           

                                            <div class="col-md-5 col-lg-3 m-t-12">

                                            @if ($open == "open")
                                            <button class="btn btn-order btn-xs mt10 button990 button1200" type="button" onclick="orderItem({{ $m->id}}, {{$m->menuSidesID}}, '{{$m->menuOptionsID}}', {{$m->menuAddOnsID }}, '{{addslashes($m->menuItem)}}', '{{addslashes($m->menuItemDescription)}}')">Order Item</button>
                              				@endif

                                            </div>

                                            <div class="clearfix"></div>

                                                @php $counter = 0; @endphp
                                                <div class="menuImage">  
                                                @foreach ($smallMenuImages as $sm)
                                               
                                                @if ($m->id == $sm->menuDataID && $sm->menuDataID == $largeMenuImages[$counter]->menuDataID)
                                                
                                                    <a data-lightbox="menuImage" data-title="{{ $m->menuItem }}" href="{{ env('PRIVATE_MENU_IMAGES') . $domain->id . '/' . $sm->menuID . '/Large/' . $largeMenuImages[$counter]->fileName}}">
                                                    <img src="{{ env('PRIVATE_MENU_IMAGES') . $domain->id . '/' . $sm->menuID . '/Small/' . $sm->fileName }}">
                                                    </a>
                                                
                                                @endif
                                                @php if ( $sm->menuDataID == $largeMenuImages[$counter]->menuDataID ) { $counter = $counter +1; } @endphp
                                                @endforeach
                                                </div>  


                                            <div class="clearfix"></div>
                                            <div class="collapse" id="{{ Str::kebab($m->menuItem) }}" style="min-height: 80px;">


                                            </div>

                                        </div>

                                    </div>


                                @endif
                            @endforeach

                            </div>
                        </div>
                    </div>
                @endforeach    

                </div>
            </div>
        </div>



    </div>
</div>

<div class="clearfix"></div>

@include('orders.iframeOrderModal')
@include('orders.orderLoginModal')

<!-- Page-Level Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js"></script>
<script>

var $ = jQuery.noConflict();

$('.panel-heading').on('click', function () {
    $('.panel-heading').removeClass('active');
    $(this).addClass('active');
});


function clearCart(sid) {
  
    var sure = confirm("Are you sure you want to clear your cart?");
    if (sure == false) { return false; }
    var chk = $('#orderCart').html();
    if (!chk.length) { return true; }

    var fd = new FormData();
    fd.append('sid', sid),    

      $.ajax({
          url: "/api/v1/clearCart",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             window.location="/order?neworder=yes";
          },
          error: function() {
            alert("There was an problem clearing your cart. Please try again.");
          }
      }); 

};

function deleteItem(itemID) {
  
    var sure = confirm("Are you sure you want to delete?");
    if (sure == false) { return false; }
    
    var fd = new FormData();
    fd.append('itemID', itemID),    

      $.ajax({
          url: "/api/v1/deleteitem",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             location.reload();
          },
          error: function() {
            alert("There was an problem deleting the item. Please try again.");
          }
      }); 

};

</script>