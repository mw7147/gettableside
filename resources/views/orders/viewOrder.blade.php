{!! html_entity_decode($html) !!}

<style>
@media print {
  .hidden-print {
    display: none !important;
  }
}
</style>

<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="hidden-print">
    <tr>
        <td bgcolor="#ffffff">
            <div style="padding: 18px 15px 36px 15px;">

                <table border="0" cellpadding="0" cellspacing="0" width="768" align="center" class="wrapper">
                    <tbody><tr><td>
   
              @if ($domain->type != 9)              
							<a href="/order" style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #666666; border-top: 8px solid #666666; border-bottom: 8px solid #666666; border-left: 12px solid #666666; border-right: 12px solid #666666; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Return To Menu</a>
              @else
              <a href="/dine" style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #666666; border-top: 8px solid #666666; border-bottom: 8px solid #666666; border-left: 12px solid #666666; border-right: 12px solid #666666; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Return To Menu</a>
              @endif
                     
                     </td></tr></tbody>
                </table>           
          

            </div>
        </td>
    </tr>
</table>
<script type='text/javascript'>
  window.__wtw_lucky_site_id = 209476; 

  (function() { var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true; wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s); })();
  window.onload = (event) => {
    localStorage.clear();
  };
</script>
