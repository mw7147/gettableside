
@extends('orders.iframe')

@section('content')

@include('orders.cssUpdates')

    <style>
      .stripe-button-el { display: none }
      .w225 { width: 225px; }
    </style>

    <div class="topSpace"></div>
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
    <span class="pull-right" style="margin-top: -42px; margin-bottom: 15px; margin-right: 3%; color: {{ $styles->bannerButtonColor}};">
    <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
    {{ $domainSiteConfig->address1 }}<br>
    @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
    {{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>
    <i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}
    </span>
    
    <a href="/"><img class="imgIframe pull-left logo" src="{{ $domain->logoPublic }}"></a>

    </div>  

      
    <div class="clearfix"></div>

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
    <div class="ibox float-e-margins">


        @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
        @endif

      <div class="clearfix"></div>

      <div class="col-md-9 col-sm-12 col-xs-12 m-b-sm">
        <a class="btn btn-xs btn-default w150 m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        @auth
        <a class="btn btn-default btn-xs w150 m-r-xs" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Information</a>
        <a class="btn btn-default btn-xs w150" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>
        @endauth

        @guest
        <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" ><i class="fa fa-user m-r-sm"></i>Login</button>
        <a class="btn btn-default btn-xs w150" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
        @endguest
      </div>
      <div class="clearfix"></div>
        
        <div class="ibox-title">
            <h5><i class="fa fa-pencil m-r-xs"></i>My Order</h5>
        </div>
        
        <div class="ibox-content">

            @if ($total == 0)
                <p>No items have been ordered.</p>
                <a href="/order" class="btn btn-xs banner-button" ><i class="fa fa-reply m-r-sm"></i>Return To Menu</a>

            @else  
                {!! $html !!}
            @endif

        </div>

    </div>
</div>

@if ($total > 0)
<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl">
    <div class="ibox float-e-margins">
        
        <div class="ibox-title">
            <h5><i class="fa fa-id-card-o m-r-xs"></i>My Information</h5>
        </div>
        
        
        <div class="ibox-content">


            <form method="POST" id="placeOrderForm" action="/orderpayment">
                <div class="form-group">

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="fname" value="@if(isset($contact->fname)){{$contact->fname}}@endif" required placeholder="First Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name</label>
                        <input class="form-control" type="text" name="lname" value="@if(isset($contact->lname)){{$contact->lname}}@endif" required placeholder="Last Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email Address</label>
                        <input class="form-control" type="email" name="email" value="@if(isset($contact->email)){{$contact->email}}@endif" required placeholder="Email Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile Number</label>
                        <input class="form-control" type="tel" id="mobile" name="mobile" value="@if(isset($contact->mobile)){{$contact->mobile}}@endif" required placeholder="Mobile Phone">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address1</label>
                        <input class="form-control" type="text" name="address1" value="@if(isset($contact->address1)){{$contact->address1}}@endif" placeholder="Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address2</label>
                        <input class="form-control" type="text" name="address2" value="@if(isset($contact->address2)){{$contact->address2}}@endif" placeholder="Address2">
                    </div>

                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <label>City</label>
                        <input class="form-control" type="text" name="city" value="@if(isset($contact->city)){{$contact->city}}@endif" placeholder="City">
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <label>State</label>
                        <select class="form-control" name="State" id="state">
                            <?php if(isset($contact->city)) {$contactState = $contact->city;} else {$contactState = '';} ?>
                            @foreach ($states as $state)
                            <option value="{{ $state->stateCode }}" @if ($state->stateCode == $contactState)selected @endif>{{ $state->stateName }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label>Zip Code</label>
                        <input class="form-control" type="text" name="zipCode" value="@if(isset($contact->zipCode)){{$contact->zipCode}}@endif" placeholder="Zip Code">
                    </div>

                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 m-t-md">
                        <input type="hidden" name="sid" value="{{ $sid }}">
                        <input type="hidden" name="did" value="{{ $domain['id'] }}">
                        <input type="hidden" name="ownerID" value="{{ $domain['ownerID'] }}">
                        <input type="hidden" name="subTotal" value="{{ $subTotal }}">
                        <input type="hidden" name="userID" value="{{ Auth::id() }}">
                        <input type="hidden" name="tax" value="{{ $tax }}">
                        <input type="hidden" name="total" value="{{ $total }}">
                        <input type="hidden" name="userAgent" value="{{ $_SERVER['HTTP_USER_AGENT'] }}">
                        <input type="hidden" id="placed" value="0">





<div class="form-row">
    <label for="card-element">
      Credit or debit card
    </label>
    <div id="card-element">
      <!-- a Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors -->
    <div id="card-errors"></div>
  </div>

  <input type="submit" class="submit" value="Submit Payment">















                        <a href="/order" class="btn banner-button w225" name="toMenu" ><i class="fa fa-arrow-left m-r-sm"></i>Back To Menu</a>
                        
                        <button type="submit" class="btn banner-button w225" name="placeOrder" >Enter Payment Infomation<i class="fa fa-credit-card m-l-sm"></i></button>

                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endif

<div class="clearfix"></div>

@include('orders.orderLoginModal')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>

$( document ).ready(function() {
    $("#mobile").mask("(999) 999-9999");
   
    @if ($domain['type'] > 2)
    $('#mobile').change(function() {
    var fd = new FormData();
    fd.append('mobile', $(this).val()),    

      $.ajax({
          url: "/api/v1/checkmobile",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
            var data = JSON.parse(mydata);           
            console.log(data);
            if (data["status"] != "registered") {
                $('#mobile').val('');
                alert(data["message"]);
                return false;
            }
            return true;
          },
          error: function() {
            alert("There was an problem with your mobile number. Please try again.");
          }
      }); 
    });
    @endif

});

function deleteItem(itemID) {
  
    var sure = confirm("Are you sure you want to delete?");
    if (sure == false) { return false; }
    
    var fd = new FormData();
    fd.append('itemID', itemID),    

      $.ajax({
          url: "/api/v1/deleteitem",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
             location.reload();
          },
          error: function() {
            alert("There was an problem deleting the item. Please try again.");
          }
      }); 

};

</script>

<script>
var stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
var elements = stripe.elements();
var card = elements.create('card');

// Add an instance of the card UI component into the `card-element` <div>
card.mount('#card-element');
</script>

@endsection