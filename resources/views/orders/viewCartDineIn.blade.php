@extends('orders.iframe')

@section('content')

@include('orders.cssDineInUpdates')

{{-- Hide Close button on clear cart dialog --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .ui-dialog .ui-dialog-titlebar-close {
        display: none !important;
    }
</style>

    <style>
      .w175 { width: 295px; }
      .mt-40 { margin-top: 40px; }
      .mb-18 { margin-bottom: 18px; }
      .mt-20 { margin-top: 20px; }
      .ml-12 { margin-left: 12px; }

    .menuTable {
        margin-left: 1%;
        width: 96%;
    }
    .create-user {
        margin-left: 15px;
        margin-top: 15px;
        position: relative;
        top: 5px;
        width: 20px;
        height: 20px;
    }
    .cartdetails {
        background-color: #ffffff;
        background-image: none;
        display: block;
        padding: 6px 12px;
        width: 100%;
        font-size: 14px;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
        margin-bottom: 1rem;
    }
    .locationAddress {
        float: left;
        width: 100%;
        margin-top: 0;
    }
    .cardInput {
        /* float: left; */
        width: 100%;
        margin-top: 1rem;
    }
    .formGroup {
        float: left;
        width: 100%;
        margin: 5px 0;
    }
    .form-check-label {
        font-weight: 700;
        font-style: normal;
        margin-right: 20px;
    }
    .topSpace {
        margin-top: 20px;
    }
    .flexBox {
        display: flex;
        justify-content: flex-end;
    }
    #cardlabel {
    float: left;
    width: 100%;
    }
    #newCard {
    float: left;
    width: 100%;
    }
    .dropdown {
        float: right;
    }
    .open > .dropdown-menu {
    right: 0px;
    left: unset;
  }
    .description {
        margin-top: 30px;
    }
    .dineInPayment {
        float: left;
        width: 100%;
        margin-top: 0;
    }
    .radio-inline {
        display: flex;
        align-items: center;
    }
    input[type="radio"] {
        margin-top: 0;
    }
    .cardDetails label {
      display: block;
  }
  .expiryDate {
    float: left;
    width: 200px;
}
    .savedCard .expiry {
        font-style: italic;
        font-size: 12px;
        font-weight: 400
    }
.cvvDeatils {
    float: left;
    width: 100px;
}
.postalCode {
    float: left;
    width: 100px;
}
.cardDetails {
    float: left;
    width: 100%;
}
.saveCardField {
    margin-left: 10px;
}
    @media (max-width: 767px) {
        .formGroup {
            float: left;
            width: 100%;
            margin-left: 8px;
            margin-bottom: 8px;
        }
        .dineInPayment {
            height: auto;
            float: left;
            width: 100%;
        }
        .flexBox {
        display: flex;
        justify-content: flex-end;
        flex-direction: column;
        align-items: flex-end;
    }
        #placeOrderButton {
            width: 98%;
        }
        .postalCode, .cvvDeatils {
            margin-top: 10px;
        }
    }
    .savedCard ul li {
        list-style-type: none;
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 10px 0;
    }
    .savedCard {
        margin-top: 15px;
    }
    .savedCard ul {
        padding-left: 0;
    }
    .savedCard ul li img {
        width: 50px;
        margin: 0 20px;
    }
    .savedCard ul li .cardNumber  {
        font-weight: bold;
        font-size: 16px;
    }
    .savedCard ul li a {
        margin-left: 30px;
        font-size: 14px;
        color: red;
        text-transform: uppercase;
    }
    .dropdown-menu > li > a {
        width: 100%;
    }
    </style>

<div class="clearfix"></div>

<div class="row description">
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 m-b-sm">
    @if($domain['type'] <= 10) 
    <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">
    <br>
    <br>
    <span class="locationAddress">
        <b>{{ $domainSiteConfig->locationName }}</b><br>
        @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif
        @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif
        @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}}{{ $domainSiteConfig->zipCode}}<br> @endif
        @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
    </span>
    @endif

    </div>
    <div class="col-md-6 col-sm-16 col-xs-6 text-right m-b-sm">
        <a class="btn btn-xs btn-default w150 m-r-xs m-b-sm" href="/dine" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        
        @auth
        <div class="dropdown m-b-sm">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{Auth::user()->fname}} {{Auth::user()->lname}}
                <span class="caret"></span>
            </button>

            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <li><a class="btn btn-default btn-xs m-r-xs m-b-sm" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a></li>
                <li><a class="btn btn-default btn-xs m-r-xs m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a></li>
            </ul>
        </div>
        @endauth
    </div>
    <div class="col-md-6 col-sm-16 col-xs-6 text-right m-b-sm">
        @guest
        <button type="button" class="btn btn-default btn-xs w150 m-r-xs m-b-sm" name="login" onclick="$('#loginModal').modal('show');"><i class="fa fa-user m-r-xs"></i>Login</button>
        <a class="btn btn-default btn-xs w150 m-r-xs m-b-sm" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
        @endguest

        <button type="button" class="btn btn-default btn-xs w150 m-r-xs m-b-sm" name="showTableID" id="showTableID" onclick="$('#tableIDModal').modal('show');" >
            <i class="fa fa-qrcode m-r-sm"></i>
            @if (isset($tableID)) @if ( $tableID == '') My Table @else Table: {{ $tableID }} @endif @else My Table @endif
        </button>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-12">
        <div class="ibox mw-285">

            @if ($errors->any())
                {{--<div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>--}}
                @foreach ($errors->all() as $error)
                    <script>alert("{{ $error }}")</script>
                @endforeach
            @endif

            @if(Session::has('loginError'))
            <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">Ã—</button>
                {{ Session::get('loginError') }}
            </div>
            @endif

            @if(Session::has('cardError'))
            <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">Ã—</button>
                {{ Session::get('cardError') }}
            </div>
            @endif

            @if(Session::has('loginSuccess'))
            <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">Ã—</button>
                {{ Session::get('loginSuccess') }}
            </div>
            @endif
            <div class="clearfix"></div>
            
            <div class="ibox-title" style="margin-top: -10px;">
                <h5><i class="fa fa-pencil-square m-r-xs" aria-hidden="true"></i>My Order</h5>
            </div>
            
            @if ($total > 0)
            <form method="POST" id="placeOrderForm" action="/orderpayment/cardconnect">
                <div class="ibox-content" style="padding-bottom: 1px;">

                    <input type="hidden" name="sid" id="sid" value="{{ $sid }}">
                    <input type="hidden" name="did" id="did" value="{{ $domain->parentDomainID }}">
                    <input type="hidden" name="ownerID" value="{{ $domain->ownerID }}">
                    {{-- subtotal includes order discount --}}  
                    <input type="hidden" name="subTotal" id="subTotal" value="{{ round($subTotal, 2) }}">           
                    <input type="hidden" name="orderDiscount" id="orderDiscount" value="{{ round($orderDiscount, 2) ?? 0 }}">
                    <input type="hidden" name="orderDiscountPercent" id="orderDiscountPercent" value="{{ $orderDiscountPercent ?? 0 }}">
                    <input type="hidden" name="userID" value="{{ Auth::id() }}">
                    <input type="hidden" name="tax" id="tax" value="{{ $tax }}">
                    <input type="hidden" name="tip" id="tip" value="{{ $tip['tip'] }}">
                    <input type="hidden" name="requirePickupTip" id="requirePickupTip" value="{{ $domainSiteConfig->pickupRequireRecommendedTip }}">
                    <input type="hidden" name="total" id="total" value="{{ round($total, 2) }}">
                    <input type="hidden" name="pickupTotal" id="pickupTotal" value="{{ round($subTotal + $tax, 2) }}">
                    <input type="hidden" name="transactionFee" id="transactionFee" value="{{ $domainSiteConfig->transactionFee ?? 0 }}">
                    <input type="hidden" name="userAgent" value="{{ $_SERVER['HTTP_USER_AGENT'] }}">
                    <input type="hidden" name="ccToken" id="ccToken" value="{{ $card->token ?? '' }}">
                    <input type="hidden" name="expireMonth" id="expireMonth" value="{{ $card->expireMonth ?? '' }}">
                    <input type="hidden" name="expireYear" id="expireYear" value="{{ $card->expireYear ?? '' }}">
                    <input type="hidden" id="placed" value="0">
                    <input type="hidden" name="cardConnectDetailID" id="cardConnectDetailID" value="">
                    <input type="hidden" name="error" id="error" value="0">
                    <input type="hidden" name="luhnCheck" id="luhnCheck" value="1">
                    <input type="hidden" name="orderTimeCheck" id="orderTimeCheck" value="">
                    <input type="hidden" name="fname" id="fname" value="{{ $user->fname ?? '' }}">
                    <input type="hidden" name="lname" id="lname" value="{{ $user->lname ?? '' }}">
                    <input type="hidden" name="email" id="email" value="{{ $user->email ?? '' }}">
                    <input type="hidden" name="mobile" id="mobile" value="{{ $user->mobile ?? '' }}">
                    <input type="hidden" name="pickupTipAmount" id="pickupTipAmount" value="{{ round($tip['pickupTipAmount'], 2) }}">
                    <input type="hidden" name="isDelivery" id="isDelivery" value="">
                    <input type="hidden" name="tableID" id="tableID" value="">
                    <input  type="hidden" name="saveCard" id="saveCard"/>
                    <input  type="hidden" name="cvv2" id="cvv2"/>

                    {{--
                    <input type="hidden" name="taxDelivery" id="taxDelivery" value="{{ $domainSiteConfig->taxDelivery }}">
                    <input type="hidden" name="deliveryTaxRate" id="deliveryTaxRate" value="{{ $domainSiteConfig->deliveryTaxRate }}">
                    <input type="hidden" name="deliveryTaxAmount" id="deliveryTaxAmount" value="{{ round($deliveryTaxAmount, 2) }}">
                    <input type="hidden" name="deliveryTipAmount" id="deliveryTipAmount" value="{{ round($tip['deliveryTipAmount'], 2) }}">
                    <input type="hidden" name="requireDeliveryTip" id="requireDeliveryTip" value="{{ $domainSiteConfig->deliveryRequireRecommendedTip }}">
                    <input type="hidden" name="deliveryTotal" id="deliveryTotal" value="{{ round($subTotal + $tax + $deliveryTaxAmount + $domainSiteConfig->deliveryCharge, 2) }}">
                    <input type="hidden" name="deliveryFee" id="deliveryFee" value="{{ $domainSiteConfig->deliveryCharge ?? 0 }}">
                    --}}

                    @php
                        $isMobile = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile");
                        if (is_numeric($isMobile)) { $mobile = 1; } else {$mobile = 0; }
                    @endphp
                    <input type="hidden" name="isMobile" value="{{ $mobile }}">
                    
                    {{ csrf_field() }}
                    @if ($total == 0)
                    <p>No items have been ordered.</p>
                    <a href="/order" class="btn btn-xs banner-button"><i class="fa fa-reply m-r-sm"></i>Return To Menu</a>

                    @else
                    {!! $html !!}
                    @endif

                </div>
            </form>
            @endif

        </div>
    </div>
@if ($total > 0)
<div class="col-md-12">
    <div class="dineInPayment">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-credit-card m-r-xs" aria-hidden="true"></i>Order and Payment Information</h5>
            </div>

            <div class="clearfix"></div>

            <div class="ibox-content">


                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div class="dinerInfo dineInTop">
                                <label>First Name</label>
                                <input class="form-control" type="text" id="formFname" value="@if(isset($user->fname)){{$user->fname}}@endif" required placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div class="dinerInfo">
                                <label>Last Name</label>
                                <input class="form-control" type="text" id="formLname" value="@if(isset($user->lname)){{$user->lname}}@endif" required placeholder="Last Name">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div class="dinerInfo">
                                <label>Email Address</label>
                                <input class="form-control" type="email" id="formEmail" value="@if(isset($user->email)){{$user->email}}@endif" required placeholder="Email Address">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div class="dinerInfo">
                                <label>Mobile Number</label>
                                <input class="form-control" type="text" id="formMobile" value="@if(isset($user->mobile)){{$user->mobile}}@endif" required placeholder="Mobile Phone">
                            </div>
                        </div>

                        @if(!auth()->user())
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div class="formGroup createUser" style="visibility: visible;">
                                <label class="form-check-label">Create User</label>
                                <input type="checkbox" name="createUser" class="create-user">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12" id="userPassword" style="display: none;">
                            <div class="formGroup">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-12">
                                        <div class="formGroup">
                                            <label>Password (min 6 characters)<sup class="text-danger">*</sup></label>
                                            <input class="cartdetails" type="password" minlength="6" name="password1" value="" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xs-12">
                                        <div class="formGroup">
                                            <label>Confirm Password<sup class="text-danger">*</sup></label>
                                            <input class="cartdetails" type="password" minlength="6" name="password2" value="" placeholder="Password">
                                        </div>
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @auth
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="row">
                    
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div class="savedCard" style="margin-left: 12px">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>Payment Method</h5>
                                        <ul>
                                            @if($usercards)
                                                @php 
                                                    $idArray = null;
                                                    foreach($usercards as $value){
                                                        if($idArray){
                                                            $idArray = $idArray.','.$value->id;
                                                        }else{
                                                            $idArray = $value->id;
                                                        }
                                                    }
                                                @endphp
                                                @foreach($usercards as $value)
                                                <li>
                                                    <ul>
                                                        <li>
                                                            <input type="radio" id="radioCard" value="{{ $value->id }}" name="radioCard" data-ids="{{$idArray}}">
                                                            {{-- <img src="img/visa.png"> --}}
                                                            <span class="cardNumber" style="margin-left:5px">xxxx xxxx xxxx {{ $value->cardNumber }}</span>
                                                        </li>
                                                        <li style="margin: -20px 0px 0px 25px;">
                                                            <span class="expiry">Expires:  {{ $value->expireMonth }}/{{ $value->expireYear }}</span>
                                                            &nbsp;&nbsp;
                                                            <form>
                                                                <input  id="expireMonth{{ $value->id }}" type="hidden" value="{{ $value->expireMonth }}">
                                                                <input  id="expireYear{{ $value->id }}" type="hidden" value="{{ $value->expireYear }}">
                                                                <input  id="token{{ $value->id }}" type="hidden" value="{{ $value->token }}">
                                                                <input  id="account{{ $value->id }}" type="hidden" value="{{ $value->account }}">
                                                                {{-- <input  id="cvv{{ $value->id }}" placeholder="CVV" type="text" size="5" maxlength="4" class="form-control" style="display: none; width: 70px;" value="">
                                                                <input  id="postalCode{{ $value->id }}" placeholder="Zip Code" type="text" name="postalCode" class="form-control" style="display: none; width: 70px;" value=""> --}}
                                                                <input  id="postalCode{{ $value->id }}" type="hidden" value="{{ $value->postalCode }}" name="postalCode">
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endauth
                        @if(auth()->user() && isset($usercards) && count($usercards) > 0)
                        <input  type="checkbox" name="addCardCheck" id="addCardCheck" value="1"/ style="margin: 10px 5px;"> New Card
                        @endif
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <div id="addCard" @if(auth()->user() && isset($usercards) && count($usercards) > 0)style="display:none;"@endif>
                                <label id="cardlabel" style="margin-left: 7px;">Card Number</label><br>

                                <div id="newCard" @if (!is_null($card)) style="display:none;" @endif>
                            
                                    @if (!$mobile)
                                    @php 
                                    $a = 'input{border: 1px solid #e5e6e7; border-radius: 1px; }#ccnumfield{background-color:#FFFFFF;background-image: none;display: block;padding: 6px 12px;width: 90%;font-size: 14px;}.error{color:red !important; border-color:red !important;}label{display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: bold;font-size: 13px;font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;}#ccexpirymonth,#ccexpiryyear{width: 84px; font-size: 14px;}#ccexpirymonth,#ccexpiryyear{height: 32px;}#cccvvfield{width: 62px; font-size: 14px; padding: 6px 12px;}#cccvvlabel{padding-left: 64px;}'; 
                                    $b = urlencode($a); 
                                    @endphp
                                    <iframe id="tokenFrame" name="tokenFrame" style="margin-top: -10px; height: 42px;" src="{{ $ccGateway . '?cardnumbernumericonly=true&formatinput=true&tokenpropname=ccToken&invalidinputevent=true&cardinputmaxlength=19&css=' . $b  }}" frameborder="0" scrolling="no">
                                    </iframe>
                                    @else
                                    @php 
                                    $a = 'input{border: 1px solid #e5e6e7; border-radius: 1px; }select{ margin-right: 8px; margin-bottom: 8px; }#ccnumfield{background-color:#FFFFFF;background-image: none;display: block;padding: 6px 12px;width: 90%;font-size: 14px;}.error{color:red !important; border-color:red !important;}label{display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: bold;font-size: 11px;font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;}#ccexpirymonth,#ccexpiryyear{width: 84px; font-size: 14px;}#ccexpirymonth,#ccexpiryyear{height: 32px;}#cccvvfield{width: 62px; font-size: 14px; padding: 6px 12px;}'; 
                                    $b = urlencode($a); 
                                    @endphp
                                    <iframe id="tokenFrame" name="tokenFrame" style="margin-top: -10px; height: 42px;" src="{{ $ccGateway . '?cardnumbernumericonly=true&formatinput=true&tokenpropname=ccToken&invalidinputevent=true&cardinputmaxlength=19&tokenizewheninactive=true&inactivityto=2000&css=' . $b  }}" frameborder="0" scrolling="no">
                                    </iframe>
                    
                                    @endif
                    
                                    @auth
                                    {{-- <a id="useSavedCard" alt="Use Saved Card" style="font-style: italic; margin-left: 12px; margin-bottom: 10px; margin-top: -1px; display:block;">Use Saved Card</a> --}}
                                    @endauth
                    
                                </div>
                                <div id="savedCard" @if (is_null($card)) style="display:none;" @endif>
                                    <input type="text" style="margin-top: 4px; height: 36px; width: 282px; margin-left: 8px; margin-bottom: 8px; display:block;" disabled id="tokenDisplay" @if (!is_null($card)) value="{{ " -- " . $card->brand . " ending in " . $card->last4 . " --"}}" @else value="" @endif>
                                    <a id="getNewCard" alt="Get New Card" style="font-style: italic; margin-left: 12px; margin-bottom: 10px; margin-top: -1px; display:block;">Use A Different Card</a>
                                </div>
                
                            
                
                                <div class="clearfix"></div>
                
                                <br>
                                <div class="cardDetails" style="margin-left: 8px; margin-bottom: 16px;">
                                    <div class="expiryDate">
                                        <label id="ccexpirylabel">Expiration Date</label>
                                        <select title="Expiration Year" id="expiryMonth" class="form-control" style="display: inline-block; width: 86px;" required>
                                            <option value="">--</option>
                                            <option @isset($card) @if ( $card->expireMonth == 1 ) selected @endif @endisset value="1">01</option>
                                            <option @isset($card) @if ( $card->expireMonth == 2 ) selected @endif @endisset value="2">02</option>
                                            <option @isset($card) @if ( $card->expireMonth == 3 ) selected @endif @endisset value="3">03</option>
                                            <option @isset($card) @if ( $card->expireMonth == 4 ) selected @endif @endisset value="4">04</option>
                                            <option @isset($card) @if ( $card->expireMonth == 5 ) selected @endif @endisset value="5">05</option>
                                            <option @isset($card) @if ( $card->expireMonth == 6 ) selected @endif @endisset value="6">06</option>
                                            <option @isset($card) @if ( $card->expireMonth == 7 ) selected @endif @endisset value="7">07</option>
                                            <option @isset($card) @if ( $card->expireMonth == 8 ) selected @endif @endisset value="8">08</option>
                                            <option @isset($card) @if ( $card->expireMonth == 9 ) selected @endif @endisset value="9">09</option>
                                            <option @isset($card) @if ( $card->expireMonth == 10 ) selected @endif @endisset value="10">10</option>
                                            <option @isset($card) @if ( $card->expireMonth == 11 ) selected @endif @endisset value="11">11</option>
                                            <option @isset($card) @if ( $card->expireMonth == 12 ) selected @endif @endisset value="12">12</option>
                                        </select>
                                        
                                        <select title="Expiration Year" id="expiryYear" class="form-control" style="display: inline-block; width: 86px; margin: 0px 9px;" required>
                                        <option value="">--</option>
                                        @php $year = intval(date("Y")); @endphp
                                        @for ($i = $year; $i <= $year+10; $i++)
                                        <option @isset($card) @if ( $card->expireYear == ($i-2000) ) selected @endif @endisset value="{{ $i-2000 }}">{{ $i }}</option>
                                        @endfor
                                        </select>
                                    </div>
                                    
                                    <div class="cvvDeatils">
                                        <label id="cccvvlabel">CVV</label>
                                        <input id="cvv" type="text" size="5" maxlength="4" class="form-control" style="display: inline-block; width: 86px;" autocomplete="off" required value="">
                                    </div>
                                    <div class="postalCode">
                                        <label id="postalCodelabel">Zip Code</label>
                                        <input id="postalCode" type="text" name="postalCode" class="form-control" style="display: inline-block; width: 86px;" autocomplete="off" required value="">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="saveCardField" @if(!auth()->user()) style="display: none" @endif>
                                    <input  type="checkbox" name="saveCardField" id="saveCardField"/> Save Card
                                </div>
                            </div>
                            <div style="padding-bottom: 6px;">
                            {{-- <a href="/dine" class="btn banner-button w175" style="margin-bottom: 6px;" name="toMenu"><i class="fa fa-arrow-left m-r-sm"></i>Back To Menu</a><br> 
                            
                            @if ($domainSiteConfig->delivery != "no")
                            <button type="button" class="btn banner-button w175" id="verifyAddressButton" name="verifyAddress">Submit Payment<i class="fa fa-credit-card m-l-sm"></i></button>
                            @endif

                            --}}

                            <button type="button" class="btn banner-button w175" id="placeOrderButton" name="placeOrder" >Submit Payment<i class="fa fa-credit-card m-l-sm"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
</div>

<div class="clearfix"></div>

@include('orders.orderLoginModal')
@include('orders.dineInTipModal')
@include('orders.dineInTableModal')

<div class="clearfix"></div>


<form method="POST" id="clearCartForm" action="/clearcart" style="display:none;">
    {{ csrf_field() }}
</form>

<div id="cartConfirm" title="Continue Ordering?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Do you need more time to place your order? All items will be removed and your cart reset unless you press the "Continue Ordering" button.</p>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


<script language="JavaScript">
$( document ).ready(function() {
    @if(!auth()->user())
        var data = JSON.parse(localStorage.getItem('dineTableSide'));
        if(data){
            $("#formFname").val(data.fname);
            $("#formLname").val(data.lname);
            $("#formEmail").val(data.email);
            $("#formMobile").val(data.mobile);
        }
    @endif
});
$("#userPassword").css('display','none');
$("input[name=createUser]").click(function(){ 
    if($(this).is(":checked")) {
        $(".saveCardField").css('display','block');
        $("#userPassword").css('display','block');
    } else {
        $(".saveCardField").css('display','none');
        $("#userPassword").css('display','none');
    }
});


    if ( !localStorage.getItem('dineInCart') ) {
        localStorage['dineInCart'] = true;
        window.location.reload();
    }  else {
        localStorage.removeItem('dineInCart');
    }



    window.addEventListener('message', function(event) {         
        var token = typeof event.data == "string" ? JSON.parse(event.data) : event.data;
        //console.log(token);
        //console.log(token.ccToken);
        if (typeof token.ccToken !== 'undefined') {
            if ( !token.ccToken  ) {
                // error
                console.log("Invalid Credit Card");
                document.getElementById("luhnCheck").value =0;
                document.getElementById("error").value = 1;
                alert('Invalid Credit Card Number. Please check your credit card number and try again.');
            } else {
                // token
                console.log('token: ' + token.ccToken);
                document.getElementById("luhnCheck").value = 1;
                document.getElementById("error").value = 0;
                document.getElementById('ccToken').value = token.ccToken;
            } 
        }
    }, false);
    $('input[name=radioCard]').click(function () {
        $("#addCardCheck").prop('checked', false);
        $("#addCard").hide();
        var cardID = $(this).val();
        // var dataIDs = $(this).attr("data-ids");
        // var res = dataIDs.split(",");
        // for(i=0;i<res.length;i++){
        //     if(res[i] == cardID){
        //         $("#cvv"+res[i]).css('display','inline-block');
        //         $("#postalCode"+res[i]).css('display','inline-block');
        //     }else{
        //         $("#cvv"+res[i]).css('display','none');
        //         $("#postalCode"+res[i]).css('display','none');
        //     }
        // }
        var expireMonth = $("#expireMonth"+cardID).val();
        var expireYear = $("#expireYear"+cardID).val();
        var token = $("#token"+cardID).val();
        $('#expireMonth').val(expireMonth);
        $('#expireYear').val(expireYear);
        $('#ccToken').val(token);
    });
    $('#saveCardField').on('change', function() {
        let em = $('#saveCardField').is(":checked");
        $('#saveCard').val(em);
    });
    $("#addCardCheck").change(function(){
        if($("#addCardCheck").prop('checked')){
            $('input[name=radioCard]').prop("checked", false);
            // var dataIDs = $('input[name=radioCard]').attr("data-ids");
            // var res = dataIDs.split(",");
            // for(i=0;i<res.length;i++){
            //     $("#cvv"+res[i]).css('display','none');
            //     $("#postalCode"+res[i]).css('display','none');
            // }
            $("#addCard").show();
        }else{
            $("#addCard").hide();
        }
    });
</script>

<script>


    @auth
    @include('orders.dineLocalStorage')
    @endauth

    // $("#formMobile").mask("(999) 999-9999");    
       
    // $('#formMobile').change(function() {
    //     var fd = new FormData();
    //     fd.append('mobile', $(this).val()),    

    //     $.ajax({
    //         url: "/api/v1/checkmobile",
    //         type: "POST",
    //         data: fd,
    //         contentType: false,
    //         cache: false,
    //         processData:false,   
    //         success: function(mydata) {
    //             var data = JSON.parse(mydata);           
    //             console.log(data);
    //             if (data["status"] != "registered") {
    //                 $('#formMobile').val('');
    //                 alert(data["message"]);
    //                 return false;
    //             }
    //             return true;
    //         },
    //         error: function() {
    //             alert("There was an problem with your mobile number. Please try again.");
    //         }
    //     }); 
    // });


    function deleteItem(itemID) {
    
        var sure = confirm("Are you sure you want to delete?");
        if (sure == false) { return false; }
        
        var fd = new FormData();
        fd.append('itemID', itemID),    

        $.ajax({
            url: "/api/v1/deleteitem",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,   
            success: function(mydata) {
                location.reload();
            },
            error: function() {
                alert("There was an problem deleting the item. Please try again.");
            }
        }); 

    };

    $('#expiryMonth').on('change', function() {
        let em = $('#expiryMonth').val();
        $('#expireMonth').val(em);
    });

    $('#expiryYear').on('change', function() {
        let ey = $('#expiryYear').val();
        $('#expireYear').val(ey);
    });

    $('#formFname').on('change', function() {
        let fn = $('#formFname').val();
        $('#fname').val(fn);
    });

    $('#formLname').on('change', function() {
        let ln = $('#formLname').val();
        $('#lname').val(ln);
    });

    $('#formEmail').on('change', function() {
        let el = $('#formEmail').val();
        $('#email').val(el);
    });

    $('#formMobile').on('change', function() {
        let fm = $('#formMobile').val();
        $('#mobile').val(fm);
    });

    $('#getNewCard').on('click', function() {
        $('#expiryMonth').val("");
        $('#expiryYear').val("");
        $('#savedCard').hide();
        $('#newCard').show();
    });

    @auth
    $('#useSavedCard').on('click', function() {
        location.reload();
    });
    @endauth

</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $domainSiteConfig->googleMapKey }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    var check = true;
    $('#placeOrderButton').click(function(event) {
        event.preventDefault();
        submitOrder();
    });


    function submitOrder() {
        var payload = {
            fname: $('#formFname').val(),
            lname: $('#formLname').val(),
            email: $('#formEmail').val(),
            mobile: $('#formMobile').val(),
        };
        localStorage.setItem("dineTableSide", JSON.stringify(payload));
        $('#fname').val($('#formFname').val());
        $('#lname').val($('#formLname').val());
        $('#email').val($('#formEmail').val());
        $('#mobile').val($('#formMobile').val());
        $("<input />").attr("type", "hidden").attr("name", "createUser").attr("value", $("input[name=createUser]").val()).appendTo("#placeOrderForm");
        $("<input />").attr("type", "hidden").attr("name", "password1").attr("value", $("#password1").val()).appendTo("#placeOrderForm");

        var cardID = $("input[name='radioCard']:checked").val();
        if (cardID) {
            // var cvv = $("#cvv"+cardID).val();
            var postalCode = $("#postalCode"+cardID).val();
            // $("#cvv2").val(cvv);
            $("<input />").attr("type", "hidden").attr("name", "postalCode").attr("value", postalCode).appendTo("#placeOrderForm");
        }else{
            var cvv = document.getElementById("cvv").value;
            var postalCode = document.getElementById("postalCode").value;
            $("#cvv2").val(cvv);
            $("<input />").attr("type", "hidden").attr("name", "postalCode").attr("value", postalCode).appendTo("#placeOrderForm");
        }

        validateFields();

        if (check) {
            validateCartItem();
            return true;
        } else {
            alert("Invalid information. Please check your name, address and credit card information."); 
            return false;
        }
        
    };

    function validateFields(){
        let po =  document.getElementById("placeOrderForm");
        let pov = po[0].checkValidity();
        check = true;
        if (pov) { 
            
            var data = JSON.parse(localStorage.getItem('dineTableSide'));
            var tableID = sessionStorage.getItem('table');
            if (tableID == '' || tableID == null) {
                alert("Please Select A Table.");
                check = false;
                return false;
            } else {
                $('#tableID').val(tableID);
            }


            // check if closed today
            var todayStart = "{{ $todayHours['startTime'] }}";
            var todayStop = "{{ $todayHours['stopTime'] }}";
            var today = moment();

            /*
            if (todayStart == 'closed' || todayStop == 'closed') {
                var status = moment(data.deliveryDate).isSameOrBefore(moment(today), 'day');
                if (status) {
                    localStorage.setItem("delivery.deliveryDate", null);
                    alert("Unfortunately, orders are no longer being accepted for today. Sorry for the inconvenience.");
                  //  alert("Pickup or delivery is not available today. Please check your pickup or delivery date.");
                    check = false;
                    return false;
                }
            }



            // check if w/in open hours today
            var isToday = moment(data.deliveryDate).isSame(moment(today), 'day');  // today 1/0
            if (isToday) {
                mstart = today.format("YYYY-MM-DD");
                mTodayStart = mstart + " " + todayStart;
                mTodayStop = mstart + " " + todayStop;
                momentStart = moment( mTodayStart );
                momentStop = moment( mTodayStop );
                isOpen = moment(data.deliveryDate).isBetween(momentStart, momentStop);
            }
            */

             // validate transaction
            if ( document.getElementById("expireMonth").value == '' ) {
                alert("Please provide a valid expiration month.");
                check = false;
                return false;
            }

            if ( document.getElementById("expireYear").value == '' ) {
                alert("Please provide a valid expiration year.");
                check = false;
                return false;
            }
            var cardID = $("input[name='radioCard']:checked").val();
            if (!cardID) {
                if ( document.getElementById("cvv2").value == '' ) {
                    alert("Please provide a valid cvv value.");
                    check = false;
                    return false;
                }
            }

            // validate form fields
            if ( document.getElementById("fname").value == '' ) {
                alert("Please provide a your first name.");
                check = false;
                $('#deliveryModal').modal("show");
                return false;
            }

            if ( document.getElementById("lname").value == '' ) {
                alert("Please provide your last name.");
                check = false;
                $('#deliveryModal').modal("show");
                return false;
            }

            if ( document.getElementById("email").value == '' ) {
                alert("Please provide a valid email address.");
                check = false;
                $('#deliveryModal').modal("show");
                return false;
            }

            if ( document.getElementById("mobile").value == '' ) {
                alert("Please provide a mobile number.");
                check = false;
                $('#deliveryModal').modal("show");
                return false;
            }

        } else { 

            alert("Invalid information. Please check your name, address and credit card information."); 
            check = false;
            return false;

        }
    }
    function validateCartItem(){
        var fd = new FormData();
        fd.append('sessionID', document.getElementById("sid").value),    

        $.ajax({
            url: "/api/v1/cardconnect/checkcartitems",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,   
            success: function(data) {   
                console.log(data);
                if (data.status != "checked") {
                    // cart items removed
                    alert(data.message);
                    window.location.href = '/order';
                } else {
                    document.getElementById("placeOrderForm").submit();
                }
            },
            error: function() {
                alert("There was an problem with the network. Please try again.");
            }
        }); 
    }

    // 
    function waitForToken(count) {
        var cc = document.getElementById("ccToken").value;
        if (cc.length !== 0 ) {
            validate();
        } else {
            
            if (count == 0) {
                // initial click - wait 1 second
                count = count + 1;
                setTimeout(logWaiting, 1000, count);
            }

            // check for error after 1 second
            var error = document.getElementById("error").value;
            var luhnCheck = document.getElementById("luhnCheck").value;
            if (error == 1 && luhnCheck == 0) {
                // remove error status
                document.getElementById("error").value = 0;
                return false;
            }

            count = count + 1;
            if (count > 5) {
                // no cc token after 5 seconds - fail and retry
                alert("There was a communication problem with the credit card processor. Please refresh the page and try again.");
                return false;
            } else {
                //wait 1 second - up to 4 seconds
                setTimeout(logWaiting, 1000, count); 
            }
        }
    };

    function logWaiting(count) {
        console.log("waiting 1 second");
        waitForToken(count);
    };

    function cartTimer() {
    // cart timer
    var seconds = parseInt(sessionStorage.getItem("seconds"));
  
    function tick() {
        seconds--; 
        var keepCart = false;
        sessionStorage.setItem("seconds", seconds);
        if ( seconds > 0 ) {
            setTimeout(tick, 1000);
        } else {

            $( "#cartConfirm" ).dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                        "Continue Ordering": function() {
                            sessionStorage.setItem("seconds", {{ $domainSiteConfig->cartClearTimer * 60 }} );
                            $( this ).dialog( "close" );
                            keepCart = true;
                            cartTimer();
                    }
                }
            });

            if ( !keepCart && sessionStorage.getItem("seconds") == 0 ) { setTimeout(abandonCart, 60000); }

        }

        function abandonCart() {
            if (sessionStorage.getItem("seconds") == 0) {
                $( "#cartConfirm" ).dialog("close");
                sessionStorage.clear();
                @guest
                localStorage.clear();
                @endguest
                $('#clearCartForm').submit();
            }
        };

    }

    // if ( seconds > 0) { tick(); }


}

if ( sessionStorage.getItem("seconds") != null ) { cartTimer(); }


</script>


@endsection