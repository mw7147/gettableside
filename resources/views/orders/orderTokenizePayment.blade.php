<html>
  <body>
    <form method="POST" action="/tokenize-payment">
      @csrf
      <div class="form-group">
          <div class="col-md-6 col-sm-12 col-xs-12">
              <label>Token</label>
              <input class="form-control" type="text" name="token" required>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
              <label>Amount</label>
              <input class="form-control" type="text" name="amount" required>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
            <label>Expiry</label>
            <input class="form-control" type="text" name="expiry" required>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <input class="form-control" type="submit" name="submit" value="Pay">
        </div>
      </div>
    </form>
  </body>
</html>