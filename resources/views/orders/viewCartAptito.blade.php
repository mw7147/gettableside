
@extends('orders.iframe')

@section('content')

@include('orders.cssUpdates')

    <style>
      .stripe-button-el { display: none }
      .w225 { width: 225px; }
      .mt-40 { margin-top: 40px; }
    </style>

    <div class="topSpace"></div>
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
        @if($domain['type'] <= 4)

        <span class="pull-right locationAddress">
        <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
        @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
        @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
        @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
        @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
        </span>

        <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

    @endif  

    </div>  

    <div class="clearfix"></div>

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
    <div class="ibox float-e-margins">


        @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
        @endif

        @if(Session::has('cardError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('cardError') }}
          </div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
        @endif

      <div class="clearfix"></div>

      <div class="col-md-9 col-sm-12 col-xs-12 m-b-sm">
        <a class="btn btn-xs btn-default w150 m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        @auth
        <a class="btn btn-default btn-xs w150 m-r-xs" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Information</a>
        <a class="btn btn-default btn-xs w150" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>
        @endauth

        @guest
        <button type="button" class="btn btn-default btn-xs w150 m-r-xs" name="placeOrder" onclick="$('#loginModal').modal('show');" ><i class="fa fa-user m-r-sm"></i>Login</button>
        <a class="btn btn-default btn-xs w150" href="/userregister"><i class="fa fa-user-plus m-r-xs"></i>Register</a>
        @endguest
      </div>
      <div class="clearfix"></div>
        
        <div class="ibox-title">
            <h5><i class="fa fa-pencil m-r-xs"></i>My Order</h5>
        </div>
        
        <div class="ibox-content">

            @if ($total == 0)
                <p>No items have been ordered.</p>
                <a href="/order" class="btn btn-xs banner-button" ><i class="fa fa-reply m-r-sm"></i>Return To Menu</a>

            @else  
                {!! $html !!}
            @endif

        </div>

    </div>
</div>

@if ($total > 0)
<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl">
    <div class="ibox float-e-margins">
        
        <div class="ibox-title">
            <h5><i class="fa fa-id-card-o m-r-xs"></i>My Information</h5>
        </div>
        
        <div class="ibox-content">


            <form method="POST" id="placeOrderForm" action="/orderpayment/aptito">
                <div class="form-group">

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="fname" value="@if(isset($contact->fname)){{$contact->fname}}@endif" required placeholder="First Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name</label>
                        <input class="form-control" type="text" name="lname" value="@if(isset($contact->lname)){{$contact->lname}}@endif" required placeholder="Last Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email Address</label>
                        <input class="form-control" type="email" name="email" value="@if(isset($contact->email)){{$contact->email}}@endif" required placeholder="Email Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile Number</label>
                        <input class="form-control" type="tel" id="mobile" name="mobile" value="@if(isset($contact->mobile)){{$contact->mobile}}@endif" required placeholder="Mobile Phone">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address1</label>
                        <input class="form-control" type="text" name="address1" value="@if(isset($contact->address1)){{$contact->address1}}@endif" placeholder="Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address2</label>
                        <input class="form-control" type="text" name="address2" value="@if(isset($contact->address2)){{$contact->address2}}@endif" placeholder="Address2">
                    </div>

                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <label>City</label>
                        <input class="form-control" type="text" name="city" value="@if(isset($contact->city)){{$contact->city}}@endif" placeholder="City">
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <label>State</label>
                        <select class="form-control" name="state" id="state">
                            <?php if(isset($contact->city)) {$contactState = $contact->city;} else {$contactState = '';} ?>
                            @foreach ($states as $state)
                            <option value="{{ $state->stateCode }}" @if ($state->stateCode == $contactState)selected @endif>{{ $state->stateName }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label>Zip Code</label>
                        <input class="form-control" type="text" name="zipCode" value="@if(isset($contact->zipCode)){{$contact->zipCode}}@endif" placeholder="Zip Code">
                    </div>

                    
                </div>

                <div class="clearfix"></div>

                <div class="ibox float-e-margins" style="margin-top: 45px;">
                    <div class="ibox-title">
                        <h5><i class="fa fa-credit-card m-r-xs"></i>Payment Information</h5>
                    </div>
                    

                    <div class='card-wrapper m-b-md'></div>
                    
                    <div class="ibox-content">  
                            
                                <div class="col-md-6 col-sm-12 col-xs-12 m-b-sm">
                                    <label>Card Number</label>
                                    <input class="form-control" type="text" id="number" name="number" value="" required placeholder="Card Number">
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12 m-b-sm">
                                    <label>Cardholder Name</label>
                                    <input class="form-control" type="text" name="name" value="" required placeholder="Cardholder Name">
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12 m-b-sm">
                                    <label>Expiry Date</label>
                                    <input class="form-control" type="text" id="expiry" name="expiry" value="" required placeholder="Expiry Date">
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12 m-b-sm">
                                    <label>CVC Number</label>
                                    <input class="form-control" type="text" name="cvc" value="" required placeholder="CVC Number">
                                </div>

                                @if (Auth::check())
                                <div class="col-md-6 col-sm-12 col-xs-12 m-t-sm">
                                    <input type="checkbox" name="save" id="save" class="checkbox checkbox-primary checkbox-inline" value="yes">
                                    <label class="m-l-xs font-normal">Save Card</small></label>
                                </div>
                                @endif

                                <div class="clearfix"></div>

                    </div>
                    
                    

                </div>

                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 m-t-md">
                    <input type="hidden" name="sid" value="{{ $sid }}">
                    <input type="hidden" name="did" value="{{ $domain['id'] }}">
                    <input type="hidden" name="ownerID" value="{{ $domain['ownerID'] }}">
                    <input type="hidden" name="subTotal" id="subTotal" value="{{ $subTotal }}">
                    <input type="hidden" name="userID" value="{{ Auth::id() }}">
                    <input type="hidden" name="tax" id="tax" value="{{ $tax }}">
                    <input type="hidden" name="tip" id="tip" value="{{ $tip }}">
                    <input type="hidden" name="total" id="total" value="{{ $total }}">
                    <input type="hidden" name="userAgent" value="{{ $_SERVER['HTTP_USER_AGENT'] }}">
                    <input type="hidden" id="placed" value="0">
                    <input type="hidden" id="ccType" name="ccType" value="">

                    
                    {{ csrf_field() }}

                    <a href="/order" class="btn banner-button w225" name="toMenu" ><i class="fa fa-arrow-left m-r-sm"></i>Back To Menu</a>
                    
                    <button type="submit" id="submitPaymentBtn" class="btn banner-button w225" name="placeOrder" >Submit Payment<i class="fa fa-credit-card m-l-sm"></i></button>

                </div>

            </form>

        </div>
    </div>
</div>
@endif

<div class="clearfix"></div>

@include('orders.orderLoginModal')

@include('orders.cartTipModal')

<div class="clearfix"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/card/2.4.0/jquery.card.min.js"></script>


<script>

    var card = new Card({
        form: '#placeOrderForm',
        container: '.card-wrapper',

        placeholders: {
            number: '**** **** **** ****',
            name: '**** ****',
            expiry: '**/****',
            cvc: '***',
        }
    });


    $('#submitPaymentBtn').click(function() {
        
        var cc = $('#number').val();
        ccChk = valid_credit_card(cc);
        
        if (!ccChk) { 
            alert("The credit card number is invalid. Please check the card number or use a different card."); 
            return false; 
        }

        var month = {{ Carbon\Carbon::now()->month }};
        var year = {{ Carbon\Carbon::now()->year - 2000 }};
        var expiry = $('#expiry').val();
        var expiry = expiry.split("/");
        var expiryMonth = parseInt(expiry[0]);
        var expiryYear = parseInt(expiry[1]);
        
        if (expiryYear < year) {
            alert("The credit card has expired. Please check the expiration date or use a different card.");
            return false;
        }

        if (expiryMonth <= month && expiryYear == year) {
            alert("The credit card has expired. Please check the expiration date or use a different card.");
            return false;
        }

        // set credit card type
        if ( $('#number').hasClass('amex') ) { $('#ccType').val('American Express'); }
        else if ( $('#number').hasClass('dinersclub') ) { $('#ccType').val('Diners Club'); }
        else if ( $('#number').hasClass('discover') ) { $('#ccType').val('Discover'); }
        else if ( $('#number').hasClass('jcb') ) { $('#ccType').val('JCB'); }
        else if ( $('#number').hasClass('mastercard') ) { $('#ccType').val('Mastercard'); }
        else if ( $('#number').hasClass('visa') ) { $('#ccType').val('Visa'); }
        else { $('#ccType').val('Other'); }



        return true;

    });


    // takes the form field value and returns true on valid number
    function valid_credit_card(value) {
    // accept only digits, dashes or spaces
        if (/[^0-9-\s]+/.test(value)) return false;

        // Luhn Algorithm
        var nCheck = 0, nDigit = 0, bEven = false;
        value = value.replace(/\D/g, "");

        for (var n = value.length - 1; n >= 0; n--) {
            var cDigit = value.charAt(n),
                nDigit = parseInt(cDigit, 10);

            if (bEven) {
                if ((nDigit *= 2) > 9) nDigit -= 9;
            }

            nCheck += nDigit;
            bEven = !bEven;
        }

        return (nCheck % 10) == 0;
    }



    $( document ).ready(function() {
        $("#mobile").mask("(999) 999-9999");
    
        @if ($domain['type'] > 2)
        $('#mobile').change(function() {
        var fd = new FormData();
        fd.append('mobile', $(this).val()),    

        $.ajax({
            url: "/api/v1/checkmobile",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,   
            success: function(mydata) {
                var data = JSON.parse(mydata);           
                console.log(data);
                if (data["status"] != "registered") {
                    $('#mobile').val('');
                    alert(data["message"]);
                    return false;
                }
                return true;
            },
            error: function() {
                alert("There was an problem with your mobile number. Please try again.");
            }
        }); 
        });

        $('#tipAmount').change(function() {

        var tip = $('#tipAmount').val();
        var noTipTotal = $('#noTipTotal').val();
        
        if (isNaN(tip)) { 
            $('#tipAmount').val(''); 
            $('#orderTotal').html("$" + noTipTotal);
            $('#total').val(noTipTotal);
            $('#tip').val('');
            alert("The tip must be a number"); 
            return false; 
        }

        var total = $('#total').val();
        var newTotal = (parseFloat(total) + parseFloat(tip)).toFixed(2);
        $('#total').val(newTotal);
        var totalPennies = newTotal * 100;
        alert(totalPennies);
        $('#tip').val(tip);
        $('#tipAmount').val('$' + parseFloat(tip).toFixed(2));
        $('#orderTotal').html("$" + newTotal);
        location.reload();
        });
        @endif

    });

    function deleteItem(itemID) {
    
        var sure = confirm("Are you sure you want to delete?");
        if (sure == false) { return false; }
        
        var fd = new FormData();
        fd.append('itemID', itemID),    

        $.ajax({
            url: "/api/v1/deleteitem",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,   
            success: function(mydata) {
                location.reload();
            },
            error: function() {
                alert("There was an problem deleting the item. Please try again.");
            }
        }); 

    };

</script>

@endsection