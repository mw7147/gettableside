
@extends('orders.iframe')

@section('content')

    @if ($categoryType == 'picture')
        @include('orders.orderCategoryImage')
    @else

        @if ( $domain->type == 9)
            {{-- dineTableSide --}}
            @include('orders.dineIn')
        @else
            {{-- getTableSide --}}
            @include('orders.orderBase')
        @endif

    @endif

@endsection
