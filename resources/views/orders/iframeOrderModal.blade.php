<!-- Order Modal -->
<style>

@if ( stripos($_SERVER['HTTP_USER_AGENT'], "Trident") == 0 && stripos($_SERVER['HTTP_USER_AGENT'], "Edge") == 0 )
input.faChkRnd, input.faChkSqr {
  visibility: hidden;
}

@else
input.faChkRnd, input.faChkSqr {
  visibility: visible !important;
}
@endif

@-moz-document url-prefix() { 

input.faChkRnd, input.faChkSqr {
  visibility: visible;
}

 }

.faChkSqr, .faChkRnd {
      margin-left: 0px !important;
}

input.faChkRnd:checked:after, input.faChkRnd:after,
input.faChkSqr:checked:after, input.faChkSqr:after {
  visibility: visible;
  font-family: FontAwesome;
  font-size:21px;height: 17px; width: 17px;
  position: relative;
  top: -3px;
  left: 0px;
  background-color: #FFF;
    color: {{ $styles->bannerButtonBorderColor }} !important;
  display: inline-block;
}

input.faChkRnd:checked:after {
  content: '\f058';
}

input.faChkRnd:after {
  content: '\f10c';
}

input.faChkSqr:checked:after {
  content: '\f14a';
}

input.faChkSqr:after {
  content: '\f096';
}

#menuItemDescription {
    font-style: italic;
    font-size: 14px;
}

.menuTable {
    margin-left: 8%;
    width: 84%;
}

.checkbox label {
    padding-left: 6px;
    font-size: 14px;
    margin-left: 20px;
}

.col1 {
    width: 70%;
}

td {
    border-top: none !important;
}

.modalHeading {
  font-size: 14px;
  font-style: italic;
}

.fas {
  color: red;
}

</style>

<div class="clearfix"></div>

<!-- Modal -->
<form name="order" id="order" method="POST">
  <div class="modal fade" id="orderItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 40px;"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="menuItem"></h2>
        </div>
        <div class="modal-body">

          <p id="menuItemDescription"></p>

          <div class="clearfix"></div>

          <div class="options">

              <div class="checkbox orderCheckbox" id="pricePortion"></div>

          </div>
          
          <textarea class="form-control" rows="3" id="instructions" placeholder="Additional Instructions" name="instructions"></textarea>

        </div>

        <div class="comments">
        </div>

        <div class="modal-footer">
          <input type="hidden" id="sid" name="sid" value="{{ $sid }}">
          <input type="hidden" name="domainID" value="{{ $domain->id }}">
          <input type="hidden" name="ownerID" value="{{ $domain['ownerID'] }}">          
          <input type="hidden" name="userID" value="{{ $user->id ?? '' }}">
          <input type="hidden" id="menuDataID" name="menuDataID" value="">
          
          <label>Quantity</label>
          <select name="quantity" id="quantity" class="form-control" style="display: inline-block; max-width: 60px; margin-right: 6px;">
            @for ($i = 1; $i < 31; $i++)
            <option>{{ $i }}</option>
            @endfor
          </select>
          <button type="button" class="btn btn-order" onclick="addCart()" style="vertical-align: baseline;">Order Item</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script>

function orderItem(id, menuSidesID, menuOptionsID, menuAddOnsID, menuItem, menuItemDescription) {
    
  $('#menuDataID').val(id);
  $('#menuItemDescription').html(menuItemDescription);
  $('#menuItemID').html(menuItem);
  $('#instructions').val('');


  var fd = new FormData();
  fd.append('menuItemID', id),    
  fd.append('menuSidesID', menuSidesID);
  fd.append('menuOptionsID', menuOptionsID);
  fd.append('menuAddOnsID', menuAddOnsID);

  $.ajax({
      url: "/api/v1/orderitem",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata)
      {
          $('#menuItem').html(mydata.item);
          $('#pricePortion').html(mydata.itemHTML);
          $('#orderItem').modal('show');
      },
      
      error: function() {
          console.log(mydata); 
          alert("There was an error ordering your item. Please try again.");
      }
  });
};


function clickCheck(className, maxOptions, classID) {
  className = '.' + className + ':checked';
  var chk = $(className).length;
  if (chk <= maxOptions) { return true; }
  classID = '.' + classID;
  $(classID).prop('checked', false);
};



function addCart() {
  
  var chk = $('.portion:checked').length;
  if (chk < 1) { 
    alert("Please select a portion size.");
    return false;
  } else if (chk > 1) {
    alert("Please select only 1 portion size.");
    return false;
  }

  $.ajax({
      url: "/api/v1/addCart",
      type: "POST",
      data: $('#order').serialize(),
      success: function(mydata) {
          console.log(mydata);
          // cart timer
          if ( sessionStorage.getItem("seconds") <= 0 || sessionStorage.getItem("seconds") == null ) { 
            sessionStorage.setItem("seconds", {{ $domainSiteConfig->cartClearTimer * 60 }} );
            cartTimer(); 
          }
          $('#quantity').val("1");
          $('#orderCart').html(mydata.html);
          $('#orderCart-mob').html(mydata.html);
          $('#viewCartBtn').html('View Cart '+mydata.numberOfOrder);
          $('#orderItem').modal('hide');
      },
      error: function(errorData) {
        alert("There was an error updating your cart. Please check your order and try again.");
        console.log(errorData);
        $('#orderItem').modal('hide');
      }
  });

};


</script>

<!-- end Order Modal -->