
<div class="clearfix"></div>

<!-- Login Modal -->

<!-- Modal -->
<div class="modal fade" id="orderCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">

        <div class="panel panel-default">
          <div class="panel-heading hwct-heading">
              <span class="yourOrder"><i class="fa fa-pencil-square m-r-sm"></i>Items Ordered</span>
              @if ($domainSiteConfig->orderAhead || $open == 'open')
                    @if(auth()->user())
                        <a class="btn btn-xs complete-order-button pull-right m-t-xs" @click="validateInfo()"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                    @else
                        <a class="btn btn-xs complete-order-button pull-right" onclick="confirmUserMode()"><i class="fa fa-shopping-cart m-r-sm"></i>Complete Order</a>
                    @endif
                @endif
          </div>
          <div class="panel-body hwct-border">
              <div id="orderCart-mob">
                  {!! $orderHTML !!}
              </div>
              <div class="clearfix"></div>

              @if ($domainSiteConfig->orderAhead || $open == "open")
              <form method="POST" id="clearCartForm" action="/clearcart">
              <a class="clearCart pull-right" id="clearCart" onclick="clearCart()">clear cart</a>
              {{ csrf_field() }}
              </form>
              @endif

          </div>

      </div>

      </div>

      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
<script>
function confirmUserMode() {
  $('#confirmUserMode').modal('show');
}
</script>

<!-- end Login Modal -->