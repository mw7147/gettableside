<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Location Administration</h2>

	<h4 class="m-b-md">
		Location are listed below.
	</h4>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>Table List</h3><hr>

    <a href="#" class="btn btn-info btn-xs m-b-lg w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
    <a href="/tables/{{Request::get('urlPrefix')}}/table-manager" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <div class="clearfix"></div>
    <a href="/tables/{{Request::get('urlPrefix')}}/new" class="btn btn-success btn-xs m-b-lg w110"><i class="fa fa-building m-r-xs"></i>New Location</a>
    
    <div class="clearfix"></div>


        @if(Session::has('tagSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('tableSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('tagError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('tableError') }}
            </div>
            <div class="clearfix"></div>
        @endif


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    {{--<th width="50">Location ID</th>--}}
                    <th width="80">Location Name</th>
                    <th width="50">Number of Seats</th>
                    <th width="120">Note</th>
                    <th width="180">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($tables as $table)
                <tr> 
                    {{--<td>{{ $table->tableID }}</td>--}}
                    <td>{{ $table->name }}</td>
                    <td>{{ $table->numSeats }}</td>
                    <td>{{ $table->note }}</td>
                    <td>
                        {{--<a class="btn btn-success btn-xs w90 m-r-xs" href="/tables/{{Request::get('urlPrefix')}}/qrcode/{{ $table->id }}"><i class="fa fa-qrcode m-r-xs"></i>QR Code</a>--}}
                        <a class="btn btn-success btn-xs w90 m-r-xs" href="/tables/{{Request::get('urlPrefix')}}/edit/{{ $table->id }}"><i class="fa fa-pencil m-r-xs"></i>Edit</a>
                        <a class="btn btn-danger btn-xs w90" href="/tables/{{Request::get('urlPrefix')}}/delete/{{ $table->id }}" onclick="javascript:return confirm('Are you sure you want to delete this table? All table references will be permanently deleted.')"><i class="fa fa-remove m-r-xs"></i> Delete</a> 
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 0, "asc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });


</script>

 



@endsection