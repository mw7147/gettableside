@extends('admin.admin')

@section('content')

<h2>Edit Table</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Edit the table information as necessary.</li>
    <li>Press "Update Table" to save the new table.</li>
    <li>Press "Cancel" or select a menu item to return to cancel.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Edit Menu Item Table ID {{ $table->id }}</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/tags/{{Request::get('urlPrefix')}}/view" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Tables</a>
        <div class="clearfix"></div>

        @if(Session::has('tableSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('tableSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('tableError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('tableError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">
            <form name="newTag" method="POST" action="/tables/{{Request::get('urlPrefix')}}/edit/{{ $table->id }}">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Table Information</p>
                </div>

                <div class="clearfix"></div>

                
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Name:</label>
                    <input type="text" placeholder="Location Name" class="form-control" id="name" required name="name" value="{{ $table->name }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Number of Seats:</label>
                    <input type="text" placeholder="Number of Seats" class="form-control" id="numSeats" name="numSeats" value="{{ $table->numSeats }}">
                </div>

                {{--
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Table ID:</label>
                    <input type="text" placeholder="Table ID" class="form-control" id="tableID" name="tableID" required value="{{ old('tableID') }}">
                </div>
                --}}

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Note:</label>
                    <input type="text" placeholder="Note" class="form-control" id="note" name="note" value="{{ $table->note }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                        
                            {{ csrf_field() }}
                        <a href="/owner/systemusers/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Update Table</button>

                </div>

            </form>
        </div>
    </div>
</div>

<!-- Page Level Scripts -->
<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>


@endsection