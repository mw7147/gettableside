@extends('admin.admin')

@section('content')

<h2>Create New Send Email Address</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Fill in the information as necessary.</li>
    <li>Sitename URL is the domain sitename.</li>
    <li>Press "Create New Email Address" to save the new information.</li>
    <li>Press "Cancel" or select a menu item to return to cancel.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Email Address</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <div class="clearfix"></div>

        @if ($errors->any())
        <div class="alert alert-danger col-md-9">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="clearfix"></div>
        @endif


        <div class="row">
            <form name="updateEmailAddress" method="POST" action="/{{Request::get('urlPrefix')}}/sendemail/sendnew">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Mailbox Information</p>
                </div>

                <div class="clearfix"></div>    

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Email Address:</label>
                    <input type="email" placeholder="Email Address" class="form-control" name="email" id="email" value="{{ old('email') ?? '' }}" required >
                </div> 

                <div class="clearfix"></div>         

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Site URL:</label>
                    <input type="text" placeholder="Location Name" class="form-control" name="siteURL" value="{{ old('siteURL') ?? '' }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Domain ID:</label>
                    <input type="text" placeholder="Domain ID" class="form-control" name="domainID" value="{{ old('domainID') ?? '' }}" required >
                </div>

                <div class="clearfix"></div>        

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">First Name:</label>
                    <input type="text" placeholder="First Name" class="form-control" name="fname" value="{{ old('fname') ?? '' }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Last Name:</label>
                    <input type="text" placeholder="Last Name" class="form-control" name="lname" value="{{ old('lname') ?? '' }}" required>
                </div>

                <div class="clearfix"></div>           
   
     
                <div class="col-md-10 col-sm-9 col-xs-12 m-t-md m-b-lg text-center">
                        
                            {{ csrf_field() }}
                        <a href="/{{Request::get('urlPrefix')}}/sendemail/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Create New Email Address</button>

                </div>



            </form>
        </div>
    </div>
</div>
<!-- Page Level Scripts -->

<script>

$('#email').on('change', function() {  // user name check
    var email = $("#email").val();
    var fd = new FormData();    
    fd.append('email', email);
    $.ajax({
        url: "/api/v1/checklocationemail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {
            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert(jsondata['message']); 
                return false;      
            } else {
                console.log(jsondata.message);
                return true;
        }},
        // validation error
        error: function(data){
        var errors = data.responseJSON;
        $('#email').val('');
        $("#email").focus();
        alert(errors.errors);
      }
    });
});


$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


</script>



@endsection