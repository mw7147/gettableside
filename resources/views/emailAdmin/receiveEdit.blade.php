@extends('admin.admin')

@section('content')

<h2>Edit Receive Email Information</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Edit the mailbox information as necessary.</li>
    <li>Press "Update Mailbox Information" to save the new information.</li>
    <li>Press "Cancel" or select a menu item to return to cancel.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Email ID {{$mailbox->id}} - {{$mailbox->mailbox}}</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/{{Request::get('urlPrefix')}}/receiveemail/view" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-share m-r-xs"></i>Receive Emails</a>
        <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">
            <form name="updateEmailAddress" method="POST" action="/{{Request::get('urlPrefix')}}/receiveemail/edit/{{$mailbox->id}}">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Mailbox Information</p>
                </div>

                <div class="clearfix"></div>         

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Domain SiteName:</label>
                    <input type="text" placeholder="Domain SiteName" class="form-control" name="siteURL" value="{{ $mailbox->siteURL }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Domain ID:</label>
                    <input type="text" placeholder="Domain ID" class="form-control" name="domainID" value="{{ $mailbox->domainID }}" required >
                </div>

                <div class="clearfix"></div>        

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">First Name:</label>
                    <input type="text" placeholder="First Name" class="form-control" name="fname" value="{{ $mailbox->fname }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Last Name:</label>
                    <input type="text" placeholder="Last Name" class="form-control" name="lname" value="{{ $mailbox->lname }}" required>
                </div>

                <div class="clearfix"></div>           
   
     
                <div class="col-md-10 col-sm-9 col-xs-12 m-t-md m-b-lg text-center">
                        
                            {{ csrf_field() }}
                        <a href="/{{Request::get('urlPrefix')}}/receiveemail/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Update Mailbox Information</button>

                </div>



            </form>
        </div>
    </div>
</div>
<!-- Page Level Scripts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$('#email').on('change', function() {  // user name check
    var email = $("#email").val();
    var did = $("#did").val();
    var fd = new FormData();    
    fd.append('email', email);
    fd.append('did', did);
    $.ajax({
        url: "/api/v1/checkuseremail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert(jsondata['message']); 
                return false;   
                    
            } else {
                
                return true;
 
        }},
        // validation error
        error: function(data){
        var errors = data.responseJSON;
        $('#email').val('');
        $("#email").focus();
        alert(errors.errors.email);

      }
    });
});


$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


</script>


<script>

$( document ).ready(function() {
  console.log("ready");
  $("#mobile").mask("(999) 999-9999");
  $("#zipCode").mask("99999?-9999");
});

  
$('#mobile').change(function() {
  var num = $(this).val();
  var intNum =  num.replace(/[^\d]/g, '');

if ( intNum.length != 10 ) {
  $("#mobile").val(''); 
  alert("Only 10 digit phone numbers are allowed.");
  return false;
}

var fd = new FormData();
fd.append('mobile', $(this).val()),    

  $.ajax({
      url: "/api/v1/checkmobile",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] != "registered") {
            $('#mobile').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});




$('#did').change(function() {
  var did = $(this).val();

if ( did < 1 ) {
    $("#did").val(''); 
    $("#did").focus(); 
    alert("Only positive integers allowed.");
    return false;
}

var fd = new FormData();
fd.append('did', did),    

  $.ajax({
      url: "/api/v1/checkdid",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] == "error") {
            $('#did').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your Customer ID. Please try again.");
      }
  });
});

</script>



@endsection