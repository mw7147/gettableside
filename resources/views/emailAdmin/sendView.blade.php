<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Send Email Accounts Administration</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Send email accounts send email only.</li>
		<li>Incoming emails are sent through "bounces" transport to process as bounced email.</li>
	</ul>

<div class="ibox-content">

<h3>Send Email Accounts</h3><hr>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
    <div class="clearfix"></div>
    <a href="/{{Request::get('urlPrefix')}}/sendemail/sendnew" class="btn btn-success btn-xs m-b-lg"><i class="fa fa-user-plus m-r-xs"></i>New Send Email Account</a>
    
    <div class="clearfix"></div>


        @if(Session::has('emailSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('emailSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('emailError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('emailError') }}
            </div>
            <div class="clearfix"></div>
        @endif


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    <th width="20">ID</th>
                    <th width="30">DomainID ID</th>
                    <th width="30">Active</th>
                    <th width="40">Transport</th>
                    <th width="90">Site URL</th>
                    <th width="120">Email</th>
                    <th width="60">First Name</th>
                    <th width="60">Last Name</th>
                    <th width="180">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($mailboxes as $mbox)
                <tr> 
                    <td>{{ $mbox->id }}</td>
                    <td>{{ $mbox->domainID }}</td>
                    <td>{{ $mbox->active }}</td>
                    <td>{{ $mbox->transport }}</td>
                    <td>{{ $mbox->siteURL }}</td>
                    <td>{{ $mbox->mailbox }}</td>
                    <td>{{ $mbox->fname }}</td>
                    <td>{{ $mbox->lname }}</td>
                    <td>
                        <a class="btn btn-success btn-xs w90 m-r-xs" href="/{{Request::get('urlPrefix')}}/sendemail/edit/{{ $mbox->id }}"><i class="fa fa-pencil m-r-xs"></i>View/Edit</a>                        
                        <a class="btn btn-success btn-xs w90 m-r-xs" href="/{{Request::get('urlPrefix')}}/sendemail/password/send/{{ $mbox->id }}"><i class="fa fa-user-secret m-r-xs"></i>Password</a>
                        <a class="btn btn-danger btn-xs w90" href="/{{Request::get('urlPrefix')}}/sendemail/delete/{{ $mbox->id }}" onclick="javascript:return confirm('Are you sure you want to delete this mailbox? All mailbox data will be permanently deleted.')"><i class="fa fa-remove m-r-xs"></i> Delete</a> 
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 4, "asc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'emailAddresses'},
                {extend: 'pdf', title: 'emailAddresses'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });


</script>

 



@endsection