@extends('admin.admin')
@section('content')
<style>
    .ibox {
        clear: both;
        margin-bottom: 25px;
        margin-top: 0;
        padding: 0;
    }
</style>
    <h1>Station List</h1>
    <div class="ibox">
        <div class="ibox-title">
            <h5><i class="m-r-sm">Create New System User</i></h5>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6">
                    <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i
                    class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 text-right">
                    <button type="button" onclick="blankForm()" class="btn btn-info btn-xs m-l-sm m-b-lg" data-toggle="modal" data-target="#exampleModal">Add New</button>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables" >
                    <thead>
                        <tr>
                            <!-- <th>User ID</th>
                            <th>Station Type</th> -->
                            <th>Sno.</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Unique Pin</th>
                            <th>Printer Model</th>
                            <th>Printer IP</th>
                            <th>Printer MAC address</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>  
                        @foreach($stationList->data->data as $key => $data)     
                        <tr>
                            <!-- <td>{{$data->id}}</td> 
                            <td>{{$data->station_type}}</td> -->
                            <td>{{$key + 1}}</td>
                            <td>{{$data->title}}</td>
                            <td>{{$data->description}}</td>
                            <td>{{$data->pin}}</td>
                            <td>{{$data->printer_model}}</td>
                            <td>{{$data->printer_ip}}</td>
                            <td>{{$data->printer_mac}}</td>
                            <td>
                                <a href="#" onclick="getStation(`{{ $key }}`)" data-id="{{ $data->id }}" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil"></i>Edit</a>        
                                <a href="#" onclick="deleteStation(`{{ $data->id }}`)" class="btn btn-danger btn-xs w90"><i class="fa fa-refresh"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>                
                </table>
        
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Station</h4>
            </div>
            <div class="modal-body">
              <form class="form" id="stationForm">
                  <div class="row">
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Station Type</label>
                            <select name="stationType" id="stationType" class="form-control">
                                <option value="">Select Station Type</option>
                                @foreach($stationTypeList->data->data as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Parent</label>
                            <select name="parent" id="parent" class="form-control">
                                <option value="">Select a Parent</option>
                                @foreach($stationList->data->data as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" placeholder="Title" name="title" id="title">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Unique Pin</label>
                            <input type="text" class="form-control" value="12345" name="pin" id="pin" disabled>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Printer Model</label>
                            <input type="text" class="form-control" placeholder="Title" id="printerModel" name="printerModel">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Printer IP</label>
                            <input type="text" class="form-control" placeholder="Title" id="printerIp" name="printerIp">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Printer MAC</label>
                            <input type="text" class="form-control" placeholder="Title" id="printerMac" name="printerMac">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="description" id="description"></textarea>
                        </div>
                    </div>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary saveBtn" onclick="createStation()">Save changes</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
<script>
function createStation(){
    let stationId= $("#id").val(); 
    let url = "/owner/station/create";
    if(stationId){
        url = "/owner/station/update/"+stationId;
    }
    var formData = new FormData($('#stationForm')[0]);
    $.ajax({
        url: url,
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData, 
        contentType: false,
        cache: false,
        processData:false, 
        dataType:"json",
        success: function(data) {
            location.reload();
        },
        error: function(err) {
            console.log(err);
        }
    });
    
}

function deleteStation(id){
    let confirmDelete = confirm("Are you sure to delete");
    if (confirmDelete){
        $.ajax({
            url: `/owner/station/delete/${id}`,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            cache: false,
            processData:false, 
            dataType:"json",
            success: function(data) {
                location.reload();
            },
            error: function(err) {
                console.log(err);
            }
        }); 
    }
}

function getStation(id){
    var js_data = '<?php echo json_encode($stationList->data->data); ?>';
    var station = JSON.parse(js_data)[id];
   $('.modal-title').text("Edit Station");
   $("#stationType > [value=" + station['station_type'] + "]").attr("selected", "true"); 
   $("#parent > [value=" + station['parent_id'] + "]").attr("selected", "true"); 
   $("#id").val(station['id']); 
   $("#title").val(station['title']); 
   $("#description").val(station['description']); 
   $("#printerMac").val(station['printer_mac']); 
   $("#printerIp").val(station['printer_id']); 
   $("#printerModel").val(station['printer_model']); 
   $("#pin").val(station['pin']); 
}


function blankForm(){
   $("#stationType > [value=" + 0 + "]").attr("selected", "true"); 
   $("#parent > [value=" + 0 + "]").attr("selected", "true"); 
   $('.modal-title').text("Add Station");
   $("#id").val(''); 
   $("#title").val(''); 
   $("#description").val(''); 
   $("#printerMac").val(''); 
   $("#printerIp").val(''); 
   $("#printerModel").val(''); 
   $("#pin").val(''); 
}


</script>
@endsection
