@extends('admin.admin')
@section('content')

<style>
    .ibox {
        clear: both;
        margin-bottom: 25px;
        margin-top: 0;
        padding: 0;
    }
</style>

    <h1>Station Location List</h1>
    <div class="ibox">
        <div class="ibox-title">
            <h5><i class="m-r-sm">Create New System User</i></h5>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6">
                    <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i
                    class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 text-right">
                    <button type="button" class="btn btn-info btn-xs m-l-sm m-b-lg" data-toggle="modal" data-target="#exampleModal">Add New</button>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables" >
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>        
                        <tr> <td>1</td>      
                            <td>Name</td>
                            <td>Description</td>
                            <td>
                            <a href="#" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil"></i>Edit</a>        
                            <a href="#" class="btn btn-danger btn-xs w90"><i class="fa fa-refresh"></i> Delete</a>
                               
                            </td>
                        </tr>
                    
                    </tbody>                
                </table>
        
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Location</h4>
            </div>
            <div class="modal-body">
              <form class="form">
                  <div class="row">
                      <div class="col-sm-6 col-md-6 col-xs-12">
                          <div class="form-group">
                              <label>Name</label>
                              <input type="text" class="form-control" placeholder="Name">
                          </div>
                      </div>
                      <div class="col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

@endsection