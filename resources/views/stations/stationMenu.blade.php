@extends('admin.admin')
@section('content')
    <style>
        .card {
            border: none;
            transition: all 0.4s ease-in-out 0s;
            margin-bottom: 24px;
            position: relative;
        }
        h5 {
            font-size: 16px;
            font-weight: 600;
            margin-top: 15px;
        }
        .dragIcon  {
            cursor: grab;
            margin: 10px 0;
            padding: .5rem;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 2px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }
        .dragIcon h6 {
            font-size: 14px;
            font-weight: 700;
        }
        .dragIcon p {
            font-weight: 600;
            margin-bottom: 0;
        }
        .dropZone {
            border: 1px dashed rgba(0, 0, 0, 0.125);
            padding: 20px;
            margin-bottom: 20px;
        }
        .taskboard-cards {
            display: flex;
            flex-direction: column;
            float: left;
            width: 100%;
        }
        .dragAll {
            float: left;
            width: 100%;
            display: flex;
        }
        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }
    </style>
    <h1>Station</h1>
    <div class="ibox">
        <div class="ibox-content">
            <div class="container-fluid">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-4">
                            <h5>Search Items</h5>
                            <div class="form-group"><input class="form-control"
                                    id="itemSearch" placeholder="Search items" type="search"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    @foreach($menuCategoriesData as $category)
                                    <div class="taskboard-cards" data-from="category" data-to="category" data-id="{{ $category->id }}" id="{{ $category->id }}category">
                                        <div id="{{ $category->id }}group" category-id="{{ $category->id }}" data-from="category" data-to="category" data-id="{{ $category->id }}">
                                        <h5 category-id="{{ $category->id }}" >{{ $category->categoryName }}</h5>
                                         @if(count($category->menuDataList) == 0)
                                            <div class="emptyDiv">
                                                <h6>No item found</h6>
                                            </div>
                                        @endif
                                       @foreach($category->menuDataList as $menu)
                                        @if(!in_array($menu->id, $stationMenuArr))
                                        <div class="dragIcon menuItem" data-id="{{ $menu->id }}">
                                            <h6>{{ $menu->menuItem }}</h6>
                                            <p>${{ $menu->price1 }}</p>
                                        </div>
                                        @endif    
                                        @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <?php $stationArr = $stationList;?>
                            @foreach($stationList as $station)
                                <h5>{{ $station->title}}</h5>
                                <div class="taskboard-wrapper dropZone" data-from="station" data-to="station" data-id="{{ $station->id }}"  id="{{ $station->id }}station">
                                    @if(count($station->menuDataList) == 0)
                                        <div class="emptyDiv">
                                            <h6>No item found</h6>
                                        </div>
                                    @endif

                                    @foreach($station->menuDataList as $menu)
                                    <div class="dragIcon" data-id="{{ $menu->id }}">
                                        <h6>{{ $menu->menuItem }}</h6>
                                        <p>${{ $menu->price1 }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1556638668/Sortable.js"></script>
<script>
    $(document).ready(function() {

        let options = {
            group: 'any',
            animation: 1150
        };

        let opts1 = {
            group: 'any',
            draggable: '.dragIcon',
            animation: 1150
        };

        events = [
            'onAdd',
            ].forEach(function (name) {
            options[name] = function (evt) {
                let menuID = $(evt.item).attr("data-id");
                let categoryID = $(evt.item).attr('category-id');

                let fromID = $(evt.from).attr("data-id");
                let fromDiv = $(evt.from).attr("data-from");

                let toID = $(evt.to).attr("data-id");
                let toDiv = $(evt.to).attr("data-to");
                console.log(` Menu ID ${menuID}`);
                    console.log(` category ID ${categoryID}`);
                    console.log(` Menu ID ${menuID}`);
                    console.log(` fromID ${fromID}`);
                    console.log(` toID ${toID}`);
                    console.log(` fromDiv ${fromDiv}`);
                    console.log(` toDiv ${toDiv}`);
                    console.log("Something goes wrong");
                if(fromDiv == "category" && toDiv == "station"){
                    $(`#${toID}station .emptyDiv`).hide();
                    if(categoryID){
                        addCategory(categoryID, toID);
                    }else{
                        addMenuItem(menuID, toID);
                    }

                }
                else if(toDiv == "category" && fromDiv == "station"){
                    removeMenuItem(menuID, fromID);
                }else{
                    console.log(` Menu ID ${menuID}`);
                    console.log(` category ID ${categoryID}`);
                    console.log(` Menu ID ${menuID}`);
                    console.log(` fromID ${fromID}`);
                    console.log(` toID ${toID}`);
                    console.log(` fromDiv ${fromDiv}`);
                    console.log(` toDiv ${toDiv}`);
                    console.log("Something goes wrong");
                }
            };
            });


            events = [
            'onAdd',
            ].forEach(function (name) {
                opts1[name] = function (evt) {
                let menuID = $(evt.item).attr("data-id");
                let categoryID = $(evt.item).attr('category-id');

                let fromID = $(evt.from).attr("data-id");
                let fromDiv = $(evt.from).attr("data-from");

                let toID = $(evt.to).attr("data-id");
                let toDiv = $(evt.to).attr("data-to");
                if(fromDiv == "category" && toDiv == "station"){
                    $(`#${toID}station .emptyDiv`).hide();
                    if(categoryID){
                        addCategory(categoryID, toID);
                    }else{
                        addMenuItem(menuID, toID);
                    }

                }
                else if(toDiv == "category" && fromDiv == "station"){
                    removeMenuItem(menuID, fromID);
                }else{
                    console.log("Something goes wrong");
                }
            };
            });

        let categories = JSON.parse(`{!! $menuCategoriesData !!}`);
        categories.forEach(el => {
            new Sortable(document.getElementById(`${el.id}category`), options);
            new Sortable(document.getElementById(`${el.id}group`), opts1);
        });

       let stations = <?php echo json_encode($stationArr); ?>;
       stations.forEach(el => {
            new Sortable(document.getElementById(`${el.id}station`), options);
        });
    });

    function addMenuItem(menuID, stationID){

        console.log(`add menu ${menuID} to station ${stationID}`);
        $.ajax('/add-station-menu', {
            type: 'GET',  // http method
            data: {
              menuID: menuID,
              stationID: stationID
            },  // data to submit
            success: function (data, status, xhr) {
                console.log(data, status);
            }
        });
    }

    function addCategory(categoryID, stationID){

        console.log(`add category ${categoryID} to station ${stationID}`);
        $.ajax('/add-station-menu', {
            type: 'GET',  // http method
            data: {
              categoryID: categoryID,
              menuID: null,
              stationID: stationID
            },  // data to submit
            success: function (data, status, xhr) {
                console.log(data, status);
            }
        });
    }

    function removeMenuItem(menuID, stationID){

        console.log(`remove menu ${menuID} from station ${stationID}`);
        $.ajax('/remove-station-menu', {
            type: 'GET',  // http method
            data: {
                menuID: menuID,
                stationID: stationID
            },  // data to submit
            success: function (data, status, xhr) {
                console.log(data, status);
            }
        });
    }

</script>
@endsection