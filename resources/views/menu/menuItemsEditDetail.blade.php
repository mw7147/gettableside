@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Menu Detail</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Update the menu item detail as necessary.</li>
    <li>An inactive menu item will not be displayed.</li>
    <li>Menu Category is the category that the item will be displayed in.</li>
    <li>Press "Update Information" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Menu Item ID:</i> {{ $menuData->id }}</h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="updateMenuItem" method="POST" action="/menu/{{Request::get('urlPrefix')}}/itemdetail/{{ $menuData->id }}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Item Information</p>
            </div>

            <div class="clearfix"></div>
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Status:</label>
                <select class="form-control" name="active" required> 
                    <option value="1">Active</option>
                    <option @if ($menuData->active == 0)selected @endif value="0">Inactive</option>
                </select>
            </div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Category:</label>
                <select class="form-control" name="menuCategoriesDataID" required>
                @foreach ($categories as $cat)
                    <option value="{{$cat->id}}" @if ($cat->id == $menuData->menuCategoriesDataID) selected @endif>{{$cat->categoryName}}</option> 
                @endforeach   
                </select>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Name:</label>
                <input type="text" placeholder="Item Name" class="form-control" name="menuItem" id="name" value="{{$menuData->menuItem}}" required>
            </div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Sort Order:</label>
                <input type="text" placeholder="Sort Order" class="form-control" name="sortOrder" value="{{$menuData->sortOrder}}" required>
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-4 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tag Tier 1:</label>
                <select class="form-control" name="tagTier1" id="tagTier1">
                    <option disabled>Select Tier 1 Tag</option>
                    <option  @if (is_null($menuData->tagTier1)) selected @endif value="">No Tier 1 Tag</option>
                    @foreach ($tags as $tier1)
                    @if ($tier1->tagTier == 1)
                    <option @if ($menuData->tagTier1 == $tier1->id)selected @endif value="{{ $tier1->id }}">{{ $tier1->tagName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="col-lg-3 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tag Tier 2:</label>
                <select class="form-control" name="tagTier2" id="tagTier2">
                    <option disabled>Select Tier 2 Tag</option>
                    <option  @if (is_null($menuData->tagTier2)) selected @endif value="">No Tier 2 Tag</option>
                    @foreach ($tags as $tier2)
                    @if ($tier2->tagTier == 2)
                    <option @if ($menuData->tagTier2 == $tier2->id)selected @endif value="{{ $tier2->id }}">{{ $tier2->tagName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
 
            <div class="col-lg-3 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tag Tier 3:</label>
                <select class="form-control" name="tagTier3" id="tagTier3">
                    <option disabled>Select Tier 3 Tag</option>
                    <option  @if (is_null($menuData->tagTier3)) selected @endif value="">No Tier 3 Tag</option>
                    @foreach ($tags as $tier3)
                    @if ($tier3->tagTier == 3)
                    <option @if ($menuData->tagTier3 == $tier3->id)selected @endif value="{{ $tier3->id }}">{{ $tier3->tagName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Kitchen (optional):</label>
                <input type="text" placeholder="Print Kitchen" class="form-control" name="printKitchen" id="name" value="{{$menuData->printKitchen}}">
            </div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Receipt (optional):</label>
                <input type="text" placeholder="Print Receipt" class="form-control" name="printReceipt" value="{{$menuData->printReceipt}}">
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Spice Level:</label>
                <select class="form-control" name="spiceLevel" required>
               
                    <option value="0" @if ($menuData->spiceLevel == 0 || is_null($menuData->spiceLevel) ) selected @endif>No Spice</option> 
                    <option value="1" @if ($menuData->spiceLevel == 1) selected @endif>Mild Spice</option> 
                    <option value="2" @if ($menuData->spiceLevel == 2) selected @endif>Medium Spice</option> 
                    <option value="3" @if ($menuData->spiceLevel == 3) selected @endif>Hot Spice</option> 
                    <option value="4" @if ($menuData->spiceLevel == 4) selected @endif>Extra Hot Spice</option> 

                </select>
            </div>

                <div class="col-lg-3 col-md-10 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Item Tax:</label>
                    <select class="form-control" name="itemTax" required> 
                        <option @if (!$menuData->itemTax) selected @endif value="0">No - Global or Category Tax</option>
                        <option @if ($menuData->itemTax) selected @endif value="1">Yes - Use Item Tax Rate</option>
                    </select>
                </div>
    
                <div class="col-lg-2 col-md-10 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Item Tax Rate:</label>
                    <input type="number" min="0" max="100" step=".001" placeholder="Item Tax Rate" class="form-control" name="itemTaxRate" value="{{$menuData->itemTaxRate}}">
                </div>


            <div class="clearfix"></div>

            <div class="col-lg-10 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Description:</label>
                <input class="form-control" type="text" id="desctiption" name="menuItemDescription" value="{{$menuData->menuItemDescription}}" required placeholder="item Description">
            </div>

            <div class="clearfix"></div>

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Dietary Options(Contains)</p>
            </div>
            <div class="clearfix"></div>  
            
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="soy" id="soy" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->soy) checked @endif>
                <label class="m-l-xs font-normal">soy</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="nut" id="nut" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->nut) checked @endif>
                <label class="m-l-xs font-normal">nut</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="vegan" id="vegan" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->vegan) checked @endif>
                <label class="m-l-xs font-normal">vegan</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="shellfish" id="shellfish" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->shellfish) checked @endif>
                <label class="m-l-xs font-normal">shellfish</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="meat" id="meat" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->meat) checked @endif>
                <label class="m-l-xs font-normal">meat</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="pork" id="pork" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->pork) checked @endif>
                <label class="m-l-xs font-normal">pork</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="lactose" id="lactose" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->lactose) checked @endif>
                <label class="m-l-xs font-normal">lactose</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="gluten" id="gluten" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->gluten) checked @endif>
                <label class="m-l-xs font-normal">gluten</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="egg" id="egg" class="checkbox checkbox-primary checkbox-inline" value="1" @if ($menuData->egg) checked @endif>
                <label class="m-l-xs font-normal">egg</small></label>
            </span>
            <div class="clearfix"></div> 

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Side Options</p>
            </div>
            <div class="clearfix"></div>  
            
            <?php $ms = explode(',', $menuData->menuSidesID); ?>

            @foreach ($menuSides as $menuSide)
            <?php $selected = in_array($menuSide->id, $ms); ?>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuSidesID[]" id="menuSidesGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuSide->id}}" onclick="groupCount()" @if ($selected) checked @endif>
                <label class="m-l-xs font-normal">{{$menuSide->name}}</small></label>
            </span>
            @endforeach

            <div class="clearfix"></div>          

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Options</p>
            </div>
            <div class="clearfix"></div>  
            
            <?php $mo = explode(',', $menuData->menuOptionsID); ?>

            @foreach ($menuOptions as $menuOption)
            <?php $selected = in_array($menuOption->id, $mo); ?>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuOptionsID[]" id="menuOptionsGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuOption->id}}" onclick="groupCount()" @if ($selected) checked @endif>
                <label class="m-l-xs font-normal">{{$menuOption->name}}</small></label>
            </span>
            @endforeach
            
            <div class="clearfix"></div>          

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Add Ons</p>
            </div>
            <div class="clearfix"></div>  
            
            <?php $mo = explode(',', $menuData->menuAddOnsID); ?>

            @foreach ($menuAddOns as $menuAddOn)
            <?php $selected = in_array($menuAddOn->id, $mo); ?>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuAddOnsID[]" id="menuAddOnsGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuAddOn->id}}" onclick="groupCount()" @if ($selected) checked @endif>
                <label class="m-l-xs font-normal">{{$menuAddOn->name}}</small></label>
            </span>
            @endforeach
            
            <div class="clearfix"></div>     

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Item Portions And Pricings</p>
            </div>
            
            <div class="clearfix"></div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 1 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion1" value="{{$menuData->portion1}} " required>
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 1 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price1" value="{{$menuData->price1}}" required>
            </div>

            <div class="clearfix"></div>     

            <div class="clearfix"></div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 2 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion2" value="{{$menuData->portion2}}">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 2 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price2" value="{{$menuData->price2}}">
            </div>

            <div class="clearfix"></div>     

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 3 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion3" value="{{$menuData->portion3}}">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 3 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price3" value="{{$menuData->price3}}">
            </div>

            <div class="clearfix"></div>    

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 4 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion4" value="{{$menuData->portion4}}">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 4 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price4" value="{{$menuData->price4}}">
            </div>

            <div class="clearfix"></div>  

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 5 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion5" value="{{$menuData->portion5}}">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 5 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price5" value="{{$menuData->price5}}">
            </div>

            <div class="clearfix"></div>   

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 6 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion6" value="{{$menuData->portion6}}">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 6 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price6" value="{{$menuData->price6}}">
            </div>

            <div class="clearfix"></div> 

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 7 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion7" value="{{$menuData->portion7}}">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 7 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price7" value="{{$menuData->price7}}">
            </div>

            <div class="clearfix"></div>     



            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <a href="/menu/{{Request::get('urlPrefix')}}/menu/list" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Information</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection