
@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>Menu Category Administration</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
        <li>To add a new item to this menu, click Add New Menu Item.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">
        
<a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
<a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>

<h3>Categories</h3><hr>

    @if(Session::has('categorySuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('categorySuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('categoryError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('categoryError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <div class="clearfix"></div>
        
    <a href="/menu/{{Request::get('urlPrefix')}}/categories/addnew" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Add New Category List</a>

    <div class="clearfix"></div>


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th style="max-width: 60px;">ID</th>
                    <th style="max-width: 60px;">Active</th>

                    @if (Request::get('authLevel') >= 35)
                    <th style="max-width: 90px;">Location Name</th>
                    @endif

                    <th style="max-width: 120px;">Name</th>
                    <th style="max-width: 300px;">Description</th>
                    <th style="max-width: 120px;">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($categories as $cat)
                <tr>                
                <td>{{$cat->id}}</td>
                <td>@if ($cat->active == "1") yes @else no @endif</td>

                @if (Request::get('authLevel') >= 35)
                <td>{{$cat->location}}</td>
                @endif

                <td>{{$cat->name}}</td>
                <td>{{$cat->description}}</td>
                <td>
                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/edit/{{ $cat->id }}" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil m-r-xs"></i>Edit Name</a>
                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/details/view/{{ $cat->id }}" class="btn btn-primary btn-xs w90"><i class="fa fa-binoculars m-r-xs"></i>Details</a>

                    @if (Request::get('urlPrefix') == "admin" || Request::get('urlPrefix') == "hwct")
                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/delete/{{ $cat->id }}" onclick="confirmDelete(event)" class="btn btn-danger btn-xs w90"><i class="fa fa-times"></i> Delete</a>
                    @endif

                </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                order: [[ 0, "asc" ]],
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });
        
        @if (Request::get('urlPrefix') == "admin" || Request::get('urlPrefix') == "hwct")
        function confirmDelete(e) {
            var cd = confirm('Are you sure you want to delete the category. All information will be deleted!');
            if (!cd) {e.preventDefault();} 
        };
        @endif

    </script>

@endsection