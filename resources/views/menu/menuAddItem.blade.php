@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Add New Menu Item</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Complete the menu item detail as necessary.</li>
    <li>An inactive menu item will not be displayed.</li>
    <li>Menu Category is the category that the item will be displayed in.</li>
    <li>Press "Update Information" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Add New Menu Item</h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

        <div class="clearfix"></div>

        <div class="row">

        <form name="updateMenuItem" method="POST" action="/menu/{{Request::get('urlPrefix')}}/additem/{{ $menuID }}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Item Information</p>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Status:</label>
                <select class="form-control" name="active" required> 
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Category:</label>
                <select class="form-control" name="menuCategoriesDataID" required>
                @foreach ($categories as $cat)
                    <option value="{{$cat->id}}">{{$cat->categoryName}}</option> 
                @endforeach   
                </select>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Name:</label>
                <input type="text" placeholder="Item Name" class="form-control" name="menuItem" id="name" value="" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Sort Order:</label>
                <input type="text" placeholder="Sort Order" class="form-control" name="sortOrder" value="" required>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Kitchen (optional):</label>
                <input type="text" placeholder="Print Kitchen" class="form-control" name="printKitchen" id="name" value="">
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Receipt (optional):</label>
                <input type="text" placeholder="Print Receipt" class="form-control" name="printOrder" value="">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Description:</label>
                <input class="form-control" type="text" id="desctiption" name="menuItemDescription" value="" required placeholder="item Description">
            </div>

            <div class="clearfix"></div>

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Side Options</p>
            </div>
            <div class="clearfix"></div>  
            

            @foreach ($menuSides as $menuSide)
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuSidesID[]" id="menuSidesGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuSide->id}}" onclick="groupCount()">
                <label class="m-l-xs font-normal">{{$menuSide->name}}</small></label>
            </span>
            @endforeach

            <div class="clearfix"></div>          

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Options</p>
            </div>
            <div class="clearfix"></div>  

            @foreach ($menuOptions as $menuOption)
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuOptionsID[]" id="menuOptionsGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuOption->id}}" onclick="groupCount()">
                <label class="m-l-xs font-normal">{{$menuOption->name}}</small></label>
            </span>
            @endforeach
            
            <div class="clearfix"></div>          

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Add Ons</p>
            </div>
            <div class="clearfix"></div>  

            @foreach ($menuAddOns as $menuAddOn)
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuAddOnsID[]" id="menuAddOnsGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuAddOn->id}}" onclick="groupCount()">
                <label class="m-l-xs font-normal">{{$menuAddOn->name}}</small></label>
            </span>
            @endforeach
            
            <div class="clearfix"></div>     

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Item Portions And Pricings</p>
            </div>
            
            <div class="clearfix"></div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 1 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion1" value="" required>
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 1 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price1" value="" required>
            </div>

            <div class="clearfix"></div>     

            <div class="clearfix"></div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 2 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion2" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 2 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price2" value="">
            </div>

            <div class="clearfix"></div>     

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 3 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion3" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 3 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price3" value="">
            </div>

            <div class="clearfix"></div>    

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 4 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion4" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 4 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price4" value="">
            </div>

            <div class="clearfix"></div>  

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 5 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion5" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 5 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price5" value="">
            </div>

            <div class="clearfix"></div>   

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 6 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion6" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 6 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price6" value="">
            </div>

            <div class="clearfix"></div> 

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 7 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion7" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 7 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price7" value="">
            </div>

            <div class="clearfix"></div>     



            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <a href="/owner/systemusers/view" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Add Menu Item</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection