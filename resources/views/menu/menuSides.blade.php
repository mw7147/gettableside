
@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Menu Sides Administration</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
        <li>To create a new side group, click Add New Side Group.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">
        
<a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
<a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-user m-r-xs"></i>Menus</a>

<h3>Menu Side Groups</h3><hr>

    @if(Session::has('sideSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('sideSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('sideError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('sideError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <div class="clearfix"></div>
        
    <a href="/menu/{{Request::get('urlPrefix')}}/sides/addnew" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Add New Side Group</a>

    <div class="clearfix"></div>


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th width="40">ID</th>
                    <th width="40">Active</th>

                    @if (Request::get('authLevel') >= 35)
                    <th width="70">Location Name</th>
                    @endif

                    <th width="90">Name</th>
                    <th width="120">Description</th>
                    <th width="60">Max Selections</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($sides as $side)
                <tr>                
                <td>{{$side->id}}</td>
                <td>@if ($side->active == "1") yes @else no @endif</td>

                @if (Request::get('authLevel') >= 35)
                <td>{{$side->location}}</td>
                @endif

                <td>{{$side->name}}</td>
                <td>{{$side->description}}</td>
                <td>{{$side->numberSides}}</td>

                <td>
                    <a href="/menu/{{Request::get('urlPrefix')}}/sides/edit/{{ $side->id }}" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil"></i> View/Edit</a>
                    <a href="/menu/{{Request::get('urlPrefix')}}/sides/details/view/{{ $side->id }}" class="btn btn-primary btn-xs w90"><i class="fa fa-binoculars m-r-xs"></i>Details</a>
                    
                    @if (Request::get('authLevel') >= 40)
                    <a href="/menu/{{Request::get('urlPrefix')}}/sides/delete/{{ $side->id }}" onclick="confirmDelete(event)" class="btn btn-danger btn-xs w90"><i class="fa fa-times"></i> Delete</a>
                    @endif

                </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                order: [[ 2, "asc" ]],
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });
        
        @if (Request::get('urlPrefix') == "admin" || Request::get('urlPrefix') == "hwct")
        function confirmDelete(e) {
            var cd = confirm('Are you sure you want to delete the side. All information will be deleted!');
            if (!cd) {e.preventDefault();} 
        };
        @endif

    </script>

@endsection