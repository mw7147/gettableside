
@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Category Details Administration</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
        <li>To add a new category item, click Add New Category Item.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">
        
<a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
<a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
<a href="/menu/{{Request::get('urlPrefix')}}/categories/list" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Categories</a>

<h3>Menu Categories For Category ID: {{$categoryID}}</h3><hr>

    @if(Session::has('categorySuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('categorySuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('categoryError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('categoryError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <div class="clearfix"></div>
        
    <a href="/menu/{{Request::get('urlPrefix')}}/categories/details/addnew/{{$categoryID}}" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Add New Category Item</a>

    <div class="clearfix"></div>


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th style="max-width: 60px;">ID</th>
                    <th style="max-width: 60px;">Sort Order</th>
                    <th style="max-width: 60px;">Status</th>
                    <th style="max-width: 120px;">Name</th>
                    <th style="max-width: 300px;">Description</th>
                    <th style="max-width: 60px;">Active Items</th>
                    <th style="max-width: 120px;">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($menuCategoriesData as $cat)
                <tr>                
                <td>{{$cat->id}}</td>
                <td>{{$cat->sortOrder}}</td>
                <td>@if ($cat->status) visible @else invisible @endif</td>
                <td>{{$cat->categoryName}}</td>
                <td>{{$cat->categoryDescription}}</td>
                <td>{{$totalItemCount[$cat->id]}}</td>
                <td>
                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/details/edit/{{ $cat->id }}" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil m-r-xs"></i>Edit</a>
                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/details/delete/{{ $cat->id }}" onclick="confirmDelete(event)" class="btn btn-danger btn-xs w90"><i class="fa fa-times"></i> Delete</a>

                </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                order: [[ 1, "asc" ]],
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });
        
        function confirmDelete(e) {
            var cd = confirm('Are you sure you want to delete this category item? All items using this category will not be displayed in your menu! This action is not recoverable.');
            if (!cd) {e.preventDefault();} 
        };

    </script>

@endsection