@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>Menu Item Administration</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<h4>
		Select the menu to view the items in the menu.
	</h4>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
        <li>To add a new item to this menu, click Add New Menu Item.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">
        
<a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
<a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-user m-r-xs"></i>Menus</a>

<h3>Menu Item List For Menu ID: {{$menu->id}}</h3><hr>

    @if(Session::has('addOnSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('addOnSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('addOnError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('addOnError') }}
        </div>
        <div class="clearfix"></div>
    @endif

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <div class="clearfix"></div>
        
    <a href="/menu/{{Request::get('urlPrefix')}}/additem/{{ $menu->id }}" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Add New Menu Item</a>

    <div class="clearfix"></div>


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th style="max-width: 60px;">ID</th>
                    <th style="max-width: 50px;">Active</th>
                    <th style="max-width: 50px;">Sort Order</th>
                    <th style="max-width: 120px;">Category</th>
                    <th style="max-width: 50px;">Spice Level</th>
                    <th style="max-width: 180px;">Menu Item</th>
                    <th style="max-width: 300px;">Item Description</th>
                    <th style="max-width: 70px;">Min Price</th>
                    <th style="max-width: 70px;">Max Price</th>
                    <th style="max-width: 150px;">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($menuItems as $item)
                <tr>                
                <td>{{$item->id}}</td>
                <td>@if ($item->active == 1) yes @else no @endif</td>
                <td>{{$item->sortOrder}}</td>
                @if (isset($categories[$item->menuCategoriesDataID]))
                <td data-toggle="tooltip" title="{{$categories[$item->menuCategoriesDataID]}}">{{$categories[$item->menuCategoriesDataID]}}</td>
                @else
                <td></td>
                @endif
                <td>@if (is_null($item->spiceLevel)) 0 @else {{$item->spiceLevel}} @endif</td>
                <td>{{$item->menuItem}}</td>
                <td>{{$item->menuItemDescription}}</td>
                <td>

                <?php
                    // determin min and max price for menu item
                    $array = array($item->price1, $item->price2, $item->price3, $item->price4, $item->price5, $item->price6, $item->price7);
                    $prices = Arr::where($array, function ($value, $key) {
                        return is_numeric($value);
                    });

                    $min = number_format(min($prices), 2);
                    $max = number_format(max($prices), 2);
                ?>
                
                
                ${{$min}}
                
                
                </td>
                <td>${{$max}}</td>

                    <td>
   
                        <a href="/menu/{{Request::get('urlPrefix')}}/itemdetail/{{ $item->id }}" class="btn btn-primary btn-xs m-t-xs w90"><i class="fa fa-pencil m-r-xs"></i>View/Edit</a>
                        <a href="/menuimages/{{Request::get('urlPrefix')}}/imagelist/{{ $item->id }}" class="btn btn-primary btn-xs m-t-xs w90"><i class="fa fa-camera m-r-xs"></i>Images</a>
                        @if ($user->role_auth_level >= 35)
                        <a href="/menu/{{Request::get('urlPrefix')}}/itemdetail/delete/{{ $item->id }}" class="btn btn-danger btn-xs m-t-xs w90" onclick="return confirm('Are you sure you want to delete this menu item?')"><i class="fa fa-times m-r-xs"></i>Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                order: [[ 3, "asc" ], [2, "asc"]],
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

    </script>

@endsection