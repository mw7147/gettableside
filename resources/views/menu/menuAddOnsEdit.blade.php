@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Edit Add On Group</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Press "Update Add On Group" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return to the Add On Group List without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Edit Menu Add On Group ID: {{$addOn->id}} - {{ $addOn->name }}</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/addons/list" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Add Ons</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="editaddOnList" method="POST" action="/menu/{{Request::get('urlPrefix')}}/addons/edit/{{$addOn->id}}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Add On Information</p>
            </div>

            <div class="clearfix"></div>

    
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Add On Name:</label>
                <input type="text" placeholder="Add On Name" class="form-control" name="name" id="name" value="{{$addOn->name}}" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Add On Status:</label>
                <select class="form-control" name="active" required> 
                    <option value="1">Active</option>
                    <option @if ($addOn->active == 0) selected @endif value="0">Inactive</option>
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Add On Description:</label>
                <input class="form-control" type="text" id="Add On Description" name="description" value="{{$addOn->description}}" required placeholder="Add On Description">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <a href="/menu/{{Request::get('urlPrefix')}}/addons/list" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Add On Group</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection