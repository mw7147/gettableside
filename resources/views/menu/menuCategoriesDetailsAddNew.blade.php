@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>New Category Item</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Menu Category is the category name that menu items will be displayed in.</li>
    <li>Press "Create New Category" to create the new category.</li>
    <li>Press "Cancel" or select a menu item to return to the Category Item View.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">New Category Item for Category ID: {{$categoryID}}</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="addNewCategoryList" method="POST" action="/menu/{{Request::get('urlPrefix')}}/categories/details/addnew/{{$categoryID}}" enctype="multipart/form-data">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Category Information</p>
            </div>

            <div class="clearfix"></div>

    
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Category Name:</label>
                <input type="text" placeholder="Category Name" class="form-control" name="name" id="name" value="" required>
            </div>

            <div class="col-lg-3 col-md-10 col-xs-5 m-b-md">
                <label class="font-normal font-italic">Status:</label>
                <select class="form-control" name="active" required> 
                    <option value="1">Visible</option>
                    <option value="0">Invisible</option>
                </select>
            </div>

            <div class="col-lg-2 col-md-10 col-xs-5 m-b-md">
                <label class="font-normal font-italic">Sort Order:</label>
                <input type="number" min="0" step="1" placeholder="Sort Order" class="form-control" name="sortOrder" value="" required>
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Category Description:</label>
                <input class="form-control" type="text" id="Category Description" name="description" value="" required placeholder="Category Description">
            </div>

            <div class="col-lg-3 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Category Tax:</label>
                <select class="form-control" name="categoryTax" required> 
                    <option value="0">No - Use Global Tax Rate</option>
                    <option value="1">Yes - Use Category Tax Rate</option>
                </select>
            </div>

            <div class="col-lg-2 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Category Tax Rate:</label>
                <input type="number" min="0" max="100" step=".001" placeholder="Category Tax Rate" class="form-control" name="categoryTaxRate" value="">
            </div>
                
            <div class="clearfix"></div>

            <div class="col-lg-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Category Image - 320 X 180 Pixel PNG or JPG file</label>
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button type="button" id="changeButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" onclick="changeImage()">Add Image</button>
                        <input type="text" class="form-control" id="categoryImage" style="max-width: 65%;" name="categoryImage" value="">
                    </span>
                    <input type="file" class="newImage" id="categoryImageClick" name="img" style="display: none;">
                </div>
            </div>
   

            <div class="clearfix"></div>
            
            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <input type="hidden" id="menuCategoriesDataID" name="menuCategoriesDataID" value="">

                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/details/view/{{$categoryID}}" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Create New Category</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    function changeImage() {
        $('#categoryImageClick').trigger("click");
    };

    $('.newImage').on('change',function(){
        var img = this.files[0];
       
       // check fileSize
        if (img.size/1000 > {{ env('MAX_IMAGE_SIZE') }} ) {
            var maxFileSize = {{ env('MAX_IMAGE_SIZE') }};
            mfs = maxFileSize.toLocaleString(undefined, {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            });            
            alert("The filesize cannot be larger than " + mfs + "Kb. Please check your file and try again.");
            return false;
        }

        // check filetype
        var imageTypes = ['image/png', 'image/jpg', 'image/jpeg'];
        var imageType = imageTypes.indexOf(img.type);
        if (imageType == -1 ) {            
            alert("The file must be a jpg or png filetype. Please check your file and try again.");
            return false;
        }
        
        console.log(img);
        $('#categoryImage').val(img.name);
                    
    });

</script>

@endsection