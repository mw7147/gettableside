@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Menu Detail</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Update the menu detail as necessary.</li>
    <li>An inactive menu will not be displayed.</li>
    <li>The default menu will be displayed when the restaurant is closed. Only the menu will be displayed - ordering is not allowed when the restaurant is closed.</li>
    <li>Menu Categories are the categories associated with this menu.</li>
    <li>Press "Update Information" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Menu ID:</i> {{ $menu->id }}</h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w90"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="updateMenu" method="POST" action="/menu/{{Request::get('urlPrefix')}}/edit/{{ $menu->id }}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Required Information</p>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Status:</label>
                <select class="form-control" name="active" required>
                    <option value="yes">Active</option>
                    <option @if ($menu->active == "no")selected @endif value="no">Inactive</option>
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Default Menu:</label>
                <select class="form-control" name="defaultMenu" required>
                    <option value="yes">Yes</option>
                    <option @if ($menu->defaultMenu == "no")selected @endif value="no">No</option>
                </select>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Name:</label>
                <input type="text" placeholder="Menu Name" class="form-control" name="menuName" id="name" value="{{ $menu->menuName }}" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Categories:</label>
                <select class="form-control" name="menuCategoriesID" required>
                    <option value="0">Unassigned - No Categories</option>
                    @foreach ($categories as $category)
                    <option @if ($category->id == $menu->menuCategoriesID)selected @endif value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Description:</label>
                <input class="form-control" type="text" id="desctiption" name="menuDescription" value="{{ $menu->menuDescription }}" required placeholder="Menu Description">
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Show Menu Header When Closed:</label>
                <select class="form-control" name="showHeaderClosed" required>
                    <option @if ($menu->showHeaderClosed) selected @endif value="1">Yes</option>
                    <option @if (!$menu->showHeaderClosed) selected @endif value="0">No</option>
                </select>
            </div>



            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Header:</label>
                <textarea class="form-control" rows="6" name="menuHeader" placeholder="Menu Header">{{ $menu->menuHeader }}</textarea>
            </div>

            <div class="clearfix"></div>

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Hours Of Operation</p>
            </div>
            <div class="clearfix"></div>          

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Sunday Start Time::</label>
                <select class="form-control" name="startSunday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startSunday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Sunday Stop Time::</label>
                <select class="form-control" name="stopSunday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopSunday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Monday Start Time::</label>
                <select class="form-control" name="startMonday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startMonday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Monday Stop Time::</label>
                <select class="form-control" name="stopMonday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopMonday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tuesday Start Time::</label>
                <select class="form-control" name="startTuesday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startTuesday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tuesday Stop Time::</label>
                <select class="form-control" name="stopTuesday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopTuesday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Wednesday Start Time::</label>
                <select class="form-control" name="startWednesday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startWednesday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Wednesday Stop Time::</label>
                <select class="form-control" name="stopWednesday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopWednesday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Thursday Start Time::</label>
                <select class="form-control" name="startThursday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startThursday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Thursday Stop Time::</label>
                <select class="form-control" name="stopThursday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopThursday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Friday Start Time::</label>
                <select class="form-control" name="startFriday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startFriday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Friday Stop Time::</label>
                <select class="form-control" name="stopFriday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopFriday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Saturday Start Time::</label>
                <select class="form-control" name="startSaturday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->startSaturday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Saturday Stop Time::</label>
                <select class="form-control" name="stopSaturday">
                    <option value="">Closed</option>
                    @foreach ($timeDisplay as $td)
                    <option @if ($td->timeString == $menu->stopSaturday)selected @endif value="{{$td->timeString}}">{{$td->timeDisplay}}</option>
                    @endforeach
                </select>
            </div>

             <div class="clearfix"></div>


            <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                    
                        {{ csrf_field() }}
                    <a href="/menu/{{Request::get('urlPrefix')}}/menu/list" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Information</button>

            </div>



</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection