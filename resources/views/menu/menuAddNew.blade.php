@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Create New Menu</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Add the menu detail as necessary.</li>
    <li>Once the menu is created, edit the menu as needed.</li>
    <li>Press "Create New Menu" to create the new mene.</li>
    <li>Press "Cancel" to return to the Menu List View.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Menu</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w90"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <div class="clearfix"></div>

        <div class="row">

        <form name="updateMenu" method="POST" action="/menu/{{Request::get('urlPrefix')}}/addnew">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Required Information</p>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Name:</label>
                <input type="text" placeholder="Menu Name" class="form-control" name="menuName" id="name" value="" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Customer Location:</label>
                <select class="form-control" name="domainID" required>
                    <option>Select Location</option>
                    @foreach ($domains as $d)
                    <option value="{{$d->id}}">{{$d->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Description:</label>
                <input class="form-control" type="text" id="desctiption" name="menuDescription" value="" required placeholder="Menu Description">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                    
                        {{ csrf_field() }}
                    <a href="/menu/{{Request::get('urlPrefix')}}/menu/list" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Create New Menu</button>

            </div>



        </form>
    </div>
</div>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection