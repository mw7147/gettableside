@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>Menu Administration</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<h4>
		Menus are listed below.
	</h4>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">
        
<a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
<a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>

<h3>Menu Administration</h3><hr>

    @if(Session::has('deleteSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('deleteError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteError') }}
        </div>
        <div class="clearfix"></div>
    @endif
    
    @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        {{ Session::get('updateSuccess') }}
    </div>
    <div class="clearfix"></div>
    @endif

    @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        {{ Session::get('updateError') }}
    </div>
    <div class="clearfix"></div>
    @endif
        
    <a href="/menu/{{Request::get('urlPrefix')}}/addnew" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Create New Menu</a>

    <div class="clearfix"></div>



    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th width="40">Menu ID</th>
                    <th width="40">Active</th>

                    @if (Request::get('authLevel') >= 40)
                    <th width="40">Parent ID</th>
                    @endif

                    @if (Request::get('authLevel') >= 35)
                    <th width="100">Location Name</th>
                    @endif

                    <th width="100">Menu Name</th>
                    <th width="100">Menu Description</th>
                    <th width="60">Default Menu</th>
                    <th width="120">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($menus as $menu)
                <tr>                
                    <td>{{ $menu->id }}</td>
                    <td>{{ $menu->active }}</td>

                    @if (Request::get('authLevel') >= 40)
                    <td>{{$menu->parentDomainID}}</td>
                    @endif

                    @if (Request::get('authLevel') >= 35)
                    <td>{{$menu->name}}</td>
                    @endif

                    <td>{{ $menu->menuName }}</td>
                    <td>{{ $menu->menuDescription }} {{$menu['lname'] }}</td>
                    <td>{{ $menu->defaultMenu }}</td>
                    <td>
   
                        <a href="/menu/{{Request::get('urlPrefix')}}/edit/{{ $menu->id }}" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil"></i> View/Edit</a>
                        
                        @if (Request::get('authLevel') >= 40)
                        <a href="/menu/{{Request::get('urlPrefix')}}/delete/{{ $menu->id }}" onclick="confirmDelete(event)" class="btn btn-danger btn-xs w90"><i class="fa fa-times"></i> Delete</a>
                        @endif

                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        @if (Request::get('authLevel') >= 40)
        function confirmDelete(e) {
            var cd = confirm('Are you sure you want to delete the menu. All information will be deleted! This action is not recoverable.');
            if (!cd) {e.preventDefault();} 
        };
        @endif

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

    </script>

@endsection