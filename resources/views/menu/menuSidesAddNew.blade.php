@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>New Side Group</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Press "Create New Side Group" to save the information.</li>
    <li>Press "Cancel" or select a menu item to return to the Side Group View without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">New Side Group</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="addNewSideGroup" method="POST" action="/menu/{{Request::get('urlPrefix')}}/sides/addnew">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Side Group Information</p>
            </div>

            <div class="clearfix"></div>

    
            <div class="col-md-5 col-sm-12 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Side Group Name:</label>
                <input type="text" placeholder="Side Group Name" class="form-control" name="name" id="name" value="" required>
            </div>      
                          
            <div class="col-md-3 col-sm-6 col-xs-6 m-b-md">
                <label class="font-normal font-italic">Max Selections:</label>
                <select class="form-control" name="numberSides" required> 
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="9">10</option>
                </select>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 m-b-md">
                <label class="font-normal font-italic">Side Group Status:</label>
                <select class="form-control" name="active" required> 
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>

            <div class="clearfix"></div>

            @if (Request::get('authLevel') >= 35)
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
            @else
            <div class="col-md-11 col-sm-12 col-xs-12 m-b-md">
            @endif
                <label class="font-normal font-italic">Side Group Description:</label>
                <input class="form-control" type="text" id="Side Group Description" name="description" value="" required placeholder="Side Group Description">
            </div>

            @if (Request::get('authLevel') >= 35)
            <div class="col-md-6 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Customer Location:</label>
                <select class="form-control" name="domainID" required>
                    <option>Select Location</option>
                    @foreach ($domains as $d)
                    <option value="{{$d->id}}">{{$d->name}}</option>
                    @endforeach
                </select>
            </div>
            @endif

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    @if (Request::get('authLevel') < 35)
                    <input type="hidden" name="domainID" value="{{$domain->id}}">s
                    @endif
                    <a href="/menu/{{Request::get('urlPrefix')}}/sides/list" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Create New Side Group List</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection