@extends('admin.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h1 class="m-b-none">
                {{ $domain['name'] }} Menu Systems
            </h1>
            <small>
                Menu and Pricing Systems
            </small>
        </div>
    </div>
</div>


<!-- page level css -->

<style>
    .dashicon { height: 56px; }
    .dashicon:hover { opacity: .75;}
    .dashicon-white:hover { opacity: .62;}
    .widget { padding: 8px 15px;}
    .widget.style1 h2 { font-size: 17px; margin-top: 2px; font-weight: 400;}
    h2 {font-size: 20px;}
    .fa-report {font-size: 22px;}
    .ibox-content {min-height: 160px;}
    .fa-1_5x {font-size: 1.5em;}
    @media (max-width: 991px) {.ibox-content { min-height: 75px;} .widget.style1 h2 { font-size: 14px; margin-top: 3px; font-weight: 300;} h1 {font-size: 23px;} h2 {font-size: 17px;} .fa-report {font-size: 18px;}}

</style>


<div class="row">

       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Menus</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/menu/{{Request::get('urlPrefix')}}/menu/list">
                        <div class="widget style1 navy-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-hourglass-start fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Menu Name / Hours Available</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/menu/{{Request::get('urlPrefix')}}/menu/itemlist">
                        <div class="widget style1 navy-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Menu Items and Prices</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>



    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Categories</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/menu/{{Request::get('urlPrefix')}}/categories/list">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-folder fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Category Groups and Names</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>



    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Sides</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/menu/{{Request::get('urlPrefix')}}/sides/list">
                        <div class="widget style1 yellow-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-pencil-square-o fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Side Groups and Items</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Options</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/menu/{{Request::get('urlPrefix')}}/options/list">
                        <div class="widget style1 lazur-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-search fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Add-on / Option Groups and Items</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>

    {{--
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Add Ons</h2>

            </div>
            
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/menu/{{Request::get('urlPrefix')}}/addons/list">
                        <div class="widget style1 lt-blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-tag fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Add On Groups and Items</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>

        </div>
    </div>
    --}}

 

</div>
<script>
function comingSoon(e) {
   
    alert("Under Development - Coming Soon");
    e.preventDefault();
    return false;

}
</script>
@endsection