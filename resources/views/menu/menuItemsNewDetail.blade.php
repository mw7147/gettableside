@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Add New Menu Item </h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Add the menu item detail as necessary.</li>
    <li>An inactive menu item will not be displayed.</li>
    <li>Menu Category is the category that the item will be displayed in.</li>
    <li>Press "Create New Menu" to save the updated information.</li>
    <li>Press "Cancel" to return to the Menu List View without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Menu Item ID:</i> New Item</h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="updateMenuItem" method="POST" action="/menu/{{Request::get('urlPrefix')}}/additem/{{ $menuID }}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Item Information</p>
            </div>

            <div class="clearfix"></div>
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Status:</label>
                <select class="form-control" name="active" required> 
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Menu Category:</label>
                <select class="form-control" name="menuCategoriesDataID" required>
                @foreach ($categories as $cat)
                    <option value="{{$cat->id}}" >{{$cat->categoryName}}</option> 
                @endforeach   
                </select>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Name:</label>
                <input type="text" placeholder="Item Name" class="form-control" name="menuItem" id="name" value="" required>
            </div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Sort Order:</label>
                <input type="text" placeholder="Sort Order" class="form-control" name="sortOrder" value="" required>
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-4 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tag Tier 1:</label>
                <select class="form-control" name="tagTier1" id="tagTier1">
                    <option disabled>Select Tier 1 Tag</option>
                    <option value="">No Tier 1 Tag</option>
                    @foreach ($tags as $tier1)
                    @if ($tier1->tagTier == 1)
                    <option value="{{ $tier1->id }}">{{ $tier1->tagName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="col-lg-3 col-md-10 col-xs-12 m-b-md>
                <label class="font-normal font-italic">Tag Tier 2:</label>
                <select class="form-control" name="tagTier2" id="tagTier2">
                    <option disabled>Select Tier 2 Tag</option>
                    <option value="">No Tier 2 Tag</option>
                    @foreach ($tags as $tier2)
                    @if ($tier2->tagTier == 2)
                    <option value="{{ $tier2->id }}">{{ $tier2->tagName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
 
            <div class="col-lg-3 col-md-10 col-xs-12 m-b-md>
                <label class="font-normal font-italic">Tag Tier 3:</label>
                <select class="form-control" name="tagTier3" id="tagTier3">
                    <option disabled>Select Tier 3 Tag</option>
                    <option value="">No Tier 3 Tag</option>
                    @foreach ($tags as $tier3)
                    @if ($tier3->tagTier == 3)
                    <option value="{{ $tier3->id }}">{{ $tier3->tagName}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Kitchen (optional):</label>
                <input type="text" placeholder="Print Kitchen" class="form-control" name="printKitchen" id="name" value="">
            </div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Receipt (optional):</label>
                <input type="text" placeholder="Print Receipt" class="form-control" name="printReceipt" value="">
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-5 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Spice Level:</label>
                <select class="form-control" name="spiceLevel" required>
               
                    <option value="0" >No Spice</option> 
                    <option value="1" >Mild Spice</option> 
                    <option value="2" >Medium Spice</option> 
                    <option value="3" >Hot Spice</option> 
                    <option value="4" >Extra Hot Spice</option> 

                </select>
            </div>

            <div class="col-lg-3 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Tax:</label>
                <select class="form-control" name="itemTax" required> 
                    <option value="0">No - Global or Category Tax</option>
                    <option value="1">Yes - Use Item Tax Rate</option>
                </select>
            </div>

            <div class="col-lg-2 col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Tax Rate:</label>
                <input type="number" min="0" max="100" step=".001" placeholder="Item Tax Rate" class="form-control" name="itemTaxRate" value="">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Item Description:</label>
                <input class="form-control" type="text" id="desctiption" name="menuItemDescription" value="" required placeholder="item Description">
            </div>

            <div class="clearfix"></div>

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Dietary Options(Contains)</p>
            </div>
            <div class="clearfix"></div>  
            
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="soy" id="soy" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">soy</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="nut" id="nut" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">nut</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="vegan" id="vegan" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">vegan</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="shellfish" id="shellfish" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">shellfish</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="meat" id="meat" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">meat</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="pork" id="pork" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">pork</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="lactose" id="lactose" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">lactose</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="gluten" id="gluten" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">gluten</small></label>
            </span>
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="egg" id="egg" class="checkbox checkbox-primary checkbox-inline" value="1">
                <label class="m-l-xs font-normal">egg</small></label>
            </span>

            <div class="clearfix"></div> 

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Side Options</p>
            </div>
            <div class="clearfix"></div>  

            @foreach ($menuSides as $menuSide)
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuSidesID[]" id="menuSidesGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuSide->id}}" onclick="groupCount()" >
                <label class="m-l-xs font-normal">{{$menuSide->name}}</small></label>
            </span>
            @endforeach

            <div class="clearfix"></div>          

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Options</p>
            </div>
            <div class="clearfix"></div>  

            @foreach ($menuOptions as $menuOption)
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuOptionsID[]" id="menuOptionsGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuOption->id}}" onclick="groupCount()">
                <label class="m-l-xs font-normal">{{$menuOption->name}}</small></label>
            </span>
            @endforeach
            
            <div class="clearfix"></div>          

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Menu Add Ons</p>
            </div>
            <div class="clearfix"></div>

            @foreach ($menuAddOns as $menuAddOn)
            <span class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <input type="checkbox" name="menuAddOnsID[]" id="menuAddOnsGroup" class="checkbox checkbox-primary checkbox-inline" value="{{$menuAddOn->id}}" onclick="groupCount()">
                <label class="m-l-xs font-normal">{{$menuAddOn->name}}</small></label>
            </span>
            @endforeach
            
            <div class="clearfix"></div>     

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Item Portions And Pricings</p>
            </div>
            
            <div class="clearfix"></div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 1 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion1" value="" required>
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 1 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price1" value="" required>
            </div>

            <div class="clearfix"></div>     

            <div class="clearfix"></div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 2 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion2" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 2 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price2" value="">
            </div>

            <div class="clearfix"></div>     

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 3 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion3" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 3 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price3" value="">
            </div>

            <div class="clearfix"></div>    

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 4 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion4" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 4 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price4" value="">
            </div>

            <div class="clearfix"></div>  

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 5 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion5" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 5 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price5" value="">
            </div>

            <div class="clearfix"></div>   

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 6 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion6" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 6 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price6" value="">
            </div>

            <div class="clearfix"></div> 

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 7 Description:</label>
                <input type="text" placeholder="Portion Description" class="form-control" name="portion7" value="">
            </div>

             <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Portion 7 Price:</label>
                <input type="text" placeholder="Portion Price" class="form-control" name="price7" value="">
            </div>

            <div class="clearfix"></div>     



            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <a href="/menu/{{Request::get('urlPrefix')}}/menu/list" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Information</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection