@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>Menu Item Administration</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<h4>
		Select the menu to view the items in the menu.
	</h4>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">
        
<a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
<a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-user m-r-xs"></i>Menus</a>

<h3>Menu Item Administration</h3><hr>

    @if(Session::has('deleteSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('deleteError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th style="max-width: 60px;">Menu ID</th>
                    <th style="max-width: 70px;">Active</th>

                    @if (Request::get('authLevel') >= 40)
                    <th>Parent ID</th>
                    @endif

                    @if (Request::get('authLevel') >= 35)
                    <th>Location Name</th>
                    @endif

                    <th>Menu Name</th>
                    <th>Menu Description</th>
                    <th style="max-width: 130px;">Total Items</th>
                    <th style="max-width: 150px;">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($menus as $menu)
                <tr>                
                    <td>{{ $menu->id }}</td>
                    <td>{{ $menu->active }}</td>

                    @if (Request::get('authLevel') >= 40)
                    <td>{{$menu->parentDomainID}}</td>
                    @endif

                    @if (Request::get('authLevel') >= 35)
                    <td>{{$menu->name}}</td>
                    @endif

                    <td>{{ $menu->menuName }}</td>
                    <td>{{ $menu->menuDescription }} {{$menu['lname'] }}</td>
                    <td>{{ $menuCount[$menu->id] }}</td>
                    <td>
   
                        <a href="/menu/{{Request::get('urlPrefix')}}/items/{{ $menu->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> View/Edit</a>
                       
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

    </script>

@endsection