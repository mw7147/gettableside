@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>Create New Add On Item</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Press "Create Add On Item" to save the information.</li>
    <li>Press "Cancel" or select a menu item to return to the Add On Group List.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Menu Add On Item For ID: {{ $menuAddOns->id }} - {{ $menuAddOns->name }}</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Menus</a>
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="editAddOnList" method="POST" action="/menu/{{Request::get('urlPrefix')}}/addons/details/addnew/{{ $menuAddOns->id }}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Add On Details</p>
            </div>

            <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">AddOn Name:</label>
                <input type="text" placeholder="Add On Name" class="form-control" name="addOnsName" value="" required>
            </div>

            <div class="clearfix"></div>
    
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Kitchen (optional):</label>
                <input type="text" placeholder="Print Kitchen" class="form-control" name="printKitchen" id="name" value="">
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Print Receipt (optional):</label>
                <input type="text" placeholder="Print Receipt" class="form-control" name="printReceipt" value="">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Default:</label>
                <select class="form-control" name="addOnsDefault" required> 
                        <option value="no">No</option>
                        <option value="yes">Yes</option>
                </select>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Upcharge:</label>
                <input class="form-control" type="text" name="upCharge" value="" placeholder="Upcharge">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <a href="/menu/{{Request::get('urlPrefix')}}/addons/details/view/{{ $menuAddOns->id }}" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Create Add On Item</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection