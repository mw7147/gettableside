@extends('admin.admin')



@section('content')

<style>
    .hide {
        display: none;
    }
    .topBox {
        height: 78px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h1 class="m-b-none">
                {{ $domain->name }} Dashboard
            </h1>
            <small>getTableSide Information Systems</small>
        </div>
    </div>
</div>




<div class="row m-t-md">
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Today</span>
                <h5>Orders</h5>
            </div>
            <div class="ibox-content topBox">
                <h3 class="no-margins">{{ $totalOrders }}<br><br></h3>
                <small>Orders Today</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Today</span>
                <h5>Sales</h5>
            </div>
            <div class="ibox-content topBox">
                <h3 class="no-margins">${{ number_format($revToday, 2) }}<br><br></h3>
                <small>Sales Today</small>
            </div>
        </div>
    </div>


    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Today</span>
                <h5>Refunds</h5>
            </div>
            <div class="ibox-content topBox">
                <h3 class="no-margins">(${{ number_format($refundsToday, 2) }})<br><br></h3>
                <small>Refunds Today</small>
            </div>
        </div>
    </div>

    {{--

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Monthly</span>
                <h5>Monthly Sales</h5>
            </div>
            <div class="ibox-content topBox">
                <h3 class="no-margins">${{ number_format($monthlySales, 2) }}<br><br></h3>
                <small>Sales This Month</small>
            </div>
        </div>
    </div>

    --}}

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Status</span>
                <h5>Onine Order Status</h5>
            </div>
            <div class="ibox-content topBox">
                @if ( $status == 'Open' || $status == 'Closed')
                <h3 class="no-margins">{{ $status }}<br><br></h3>
                @else 
                <h3 class="no-margins">{{ $status }}<br><br></h3>
                @endif
                <small>Online Order Status</small>
            </div>
        </div>
    </div>


</div>




<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 210px;}
.db-blue-bg, .tgm-orange-bg {color: white;}
</style>

<div class="row m-t-xs m-b-lg">


    <a href="/manager/orderadmin/view" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" >
        <div class="widget blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-cutlery fa-4x"></i>
                <h1 class="m-xs">Orders</h1>
                <h3 class="font-bold no-margins">
                    View / Manage
                </h3>
            </div>
        </div>
    </a>


    @if ($domain->type == 9 )
    <a href="/tables/owner/table-manager" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" >
        <div class="widget blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-qrcode fa-4x"></i>
                <h1 class="m-xs">Locations</h1>
                <h3 class="font-bold no-margins">
                    View / Manage Locations
                </h3>
            </div>
        </div>
    </a>
    @endif
    

    <a href="/menu/manager/dashboard" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white">
        <div class="widget blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-file-text-o fa-4x"></i>
                <h1 class="m-xs">Menus</h1>
                <h3 class="font-bold no-margins">
                    View / Manage Menus
                </h3>
            </div>
        </div>
    </a>



       <a href="/{{Request::get('urlPrefix')}}/contacts/view" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-user fa-4x"></i>
                <h1 class="m-xs">Contacts</h1>
                <h3 class="font-bold no-margins">
                    Manage Contacts
                </h3>
            </div>
        </div>
    </a>

    <a href="/{{Request::get('urlPrefix')}}/contacts/new" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-user-plus fa-4x"></i>
                <h1 class="m-xs">Create</h1>
                <h3 class="font-bold no-margins">
                    Create New Contact
                </h3>
            </div>
        </div>
    </a>

    <a href="/attache/view/home" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white">
        <div class="widget white-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-image fa-4x"></i>
                <h1 class="m-xs">Images</h1>
                <h3 class="font-bold no-margins hidden-xs">
                Manage Images
                </h3>
            </div>
        </div>
    </a>


    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" id="reports">
        <div class="widget white-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-file-text fa-4x"></i>
                <h1 class="m-xs">Reports</h1>
                <h3 class="font-bold no-margins">
                    Reporting Systems
                </h3>
            </div>
        </div>
    </a>

    @if ( $status == 'Open' || $status == 'closed' )
    <a href="/settings/{{Request::get('urlPrefix')}}/suspend/one" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" onclick="confirmSuspend(event)">
        <div class="widget red-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-pause fa-4x"></i>
                <h1 class="m-xs">Suspend</h1>
                <h3 class="font-bold no-margins">
                    Suspend 1 Hour
                </h3>
            </div>
        </div>
    </a>
    @else
    <a href="" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" onclick="suspended(event)">
        <div class="widget red-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-pause fa-4x"></i>
                <h1 class="m-xs">Suspended</h1>
                <h3 class="font-bold no-margins">
                    Suspended
                </h3>
            </div>
        </div>
    </a>
    @endif

    <a href="/settings/{{Request::get('urlPrefix')}}/suspend/settings" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon">
        <div class="widget red-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-pause fa-4x"></i>
                <h1 class="m-xs">Suspend</h1>
                <h3 class="font-bold no-margins">
                    Suspend Settings
                </h3>
            </div>
        </div>
    </a>

    
    <a href="/owner/systemuser/view" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon">
        <div class="widget tgm-orange-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-users fa-4x"></i>
                <h1 class="m-xs">Users</h1>
                <h3 class="font-bold no-margins">
                    User Manager
                </h3>
            </div>
        </div>
    </a>

    <a href="/settings/owner/dashboard" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon">
        <div class="widget tgm-orange-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-gear fa-4x"></i>
                <h1 class="m-xs">Settings</h1>
                <h3 class="font-bold no-margins">
                    System Settings
                </h3>
            </div>
        </div>
    </a>


</div>

<script>

function comingSoon(event) {
    event.preventDefault();
    alert("Under Development - Coming Soon");
    return true;

}

function suspended(event) {
    event.preventDefault();
    alert("System is suspended. Use Suspend Settings to update or change.");
    return true;

}

function confirmSuspend(event) {
    $c = confirm('Are you sure you wish to suspend online operations for 1 hour?');
    if (!$c) { event.preventDefault(); }
}


</script>

@endsection