
@extends('orders.iframe')

@section('content')
<!-- Page-Level CSS -->

@include('orders.cssUpdates')
<style>

input.faChkRnd, input.faChkSqr {
  visibility: hidden;
}

@-moz-document url-prefix() {

input.faChkRnd, input.faChkSqr {
  visibility: visible;
}

 }

.faChkSqr, .faChkRnd {
      margin-left: 0px !important;
}

input.faChkRnd:checked:after, input.faChkRnd:after,
input.faChkSqr:checked:after, input.faChkSqr:after {
  visibility: visible;
  font-family: FontAwesome;
  font-size:21px;height: 17px; width: 17px;
  position: relative;
  top: -3px;
  left: 0px;
  background-color: #FFF;
    color: {{ $styles->bannerButtonBorderColor }} !important;
  display: inline-block;
}

input.faChkRnd:checked:after {
  content: '\f058';
}

input.faChkRnd:after {
  content: '\f10c';
}

input.faChkSqr:checked:after {
  content: '\f14a';
}

input.faChkSqr:after {
  content: '\f096';
}

@media (max-width: 768px) {

  #page-wrapper {
      min-height: 1300px !important;
  }
}

@media (min-width: 769px) and (max-width: 991px) {

  #page-wrapper {
      min-height: 1150px !important;
  }
}

@media (min-width: 992px) {

  #page-wrapper {
      min-height: 950px !important;
  }
}

.savedCard ul li {
  list-style-type: none;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 15px 0;
}
.savedCard {
  /* margin-top: 15px; */
}
.savedCard ul {
  padding-left: 0;
}
.savedCard ul li img {
  width: 50px;
  margin: 0 20px;
}
.savedCard ul li span  {
  font-weight: bold;
  font-size: 16px;
}
.savedCard ul li a {
  margin-left: 30px;
  font-size: 14px;
  color: red;
  text-transform: uppercase;
}
</style>

<div class="clearfix"></div>


<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl" style="margin-top: 70px;">

    <div class="ibox float-e-margins">

      @if($domain['type'] <= 4)
        <span class="pull-right locationAddress">
        <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
        @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif
        @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif
        @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
        @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
        </span>
        <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

      @endif


      <div class="clearfix"></div>

      @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
      @endif

      @if ($domain->type != 9 )
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/viewcart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
      @else
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/dine" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/dineincart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
      @endif

      <a class="btn btn-default btn-xs w150 m-b-sm m-r-xs" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a>
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/orders"><i class="fa fa-list-alt m-r-xs"></i>Orders</a>
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/password/reset"><i class="fa fa-user-secret m-r-xs"></i>Password</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>


      <div class="ibox-title" style="border: none; margin-top: 8px;>
        <h5><i class="fa fa-credit-card m-r-xs"></i>Payment Method</h5>
      </div>

      <div class="ibox-content savedCard">
        <ul>
          @if($usercards)
            @foreach($usercards as $value)
              <div class="row">
                <li style="border-top:0.5px solid grey;" class="col-sm-8">
                  <label style="margin-left:5px"><b>xxxx xxxx xxxx {{ $value->cardNumber }}</b></label>
                  <label style="margin-left:5px">Expires:  {{ $value->expireMonth }}/{{ $value->expireYear }}</label>
                  <button style="margin-left:10px" class="btn btn-danger btn-xs m-r-xs"onclick="deleteUserCard( `{{$value->id}}` )" ><i class="fa fa-times m-r-xs"></i>Delete</button>
                </li>
              </div>
            @endforeach
          @endif
        </ul>
      </div> 
    </div>
</div>


<script>

function deleteUserCard(cardID) {

    var sure = confirm("Are you sure you want to delete this payment method?");
    if (sure == false) { return false; }

    var fd = new FormData();
    fd.append('userID', "{{ Auth::id() }}" ),
    fd.append('domainID', "{{ $domain->id }}" ),
    fd.append('id', cardID),

      $.ajax({
          url: "/api/v1/deleteusercard",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,
          success: function(mydata) {
            alert(mydata.status);
            location.reload();
          },
          error: function() {
            alert("There was an problem deleting the payment method. Please try again.");
          }
      });

};

function deleteCard(cardID) {

    var sure = confirm("Are you sure you want to delete this payment method?");
    if (sure == false) { return false; }

    var fd = new FormData();
    fd.append('userID', "{{ Auth::id() }}" ),
    fd.append('domainID', "{{ $domain->id }}" ),

      $.ajax({
          url: "/api/v1/deletecardconnectcard",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,
          success: function(mydata) {
            alert(mydata);
            location.reload();
          },
          error: function() {
            alert("There was an problem deleting the payment method. Please try again.");
          }
      });

};
</script>


@endsection
