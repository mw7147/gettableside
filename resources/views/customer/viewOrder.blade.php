{!! html_entity_decode($html) !!}

<style>
@media print {
  .hidden-print {
    display: none !important;
  }
}
</style>

<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="hidden-print">
    <tr>
        <td bgcolor="#ffffff">
            <div style="padding: 35px 15px 0px 15px;">

                <table border="0" cellpadding="0" cellspacing="0" width="768" align="center" class="wrapper">
                    <tbody><tr><td>
   
                                
							<a href="/customer/orders" style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #666666; border-top: 8px solid #666666; border-bottom: 8px solid #666666; border-left: 12px solid #666666; border-right: 12px solid #666666; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Return To Orders</a>
                        
                     
                     </td></tr></tbody>
                </table>           
          

            </div>
        </td>
    </tr>
</table>
