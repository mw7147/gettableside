
@extends('orders.iframe')

@section('content')
<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>
@include('orders.cssUpdates')
<style>

input.faChkRnd, input.faChkSqr {
  visibility: hidden;
}

@-moz-document url-prefix() { 

input.faChkRnd, input.faChkSqr {
  visibility: visible;
}

 }

.faChkSqr, .faChkRnd {
      margin-left: 0px !important;
}

input.faChkRnd:checked:after, input.faChkRnd:after,
input.faChkSqr:checked:after, input.faChkSqr:after {
  visibility: visible;
  font-family: FontAwesome;
  font-size:21px;height: 17px; width: 17px;
  position: relative;
  top: -3px;
  left: 0px;
  background-color: #FFF;
    color: {{ $styles->bannerButtonBorderColor }} !important;
  display: inline-block;
}

input.faChkRnd:checked:after {
  content: '\f058';
}

input.faChkRnd:after {
  content: '\f10c';
}

input.faChkSqr:checked:after {
  content: '\f14a';
}

input.faChkSqr:after {
  content: '\f096';
}

@media (max-width: 768px) {

  #page-wrapper {
      min-height: 1300px !important;
  }
}

@media (min-width: 769px) and (max-width: 991px) {

  #page-wrapper {
      min-height: 1150px !important;
  }
}

@media (min-width: 992px) {

  #page-wrapper {
      min-height: 950px !important;
  }
}

</style>

<div class="clearfix"></div>
   

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl" style="margin-top: 70px;">
        


    <div class="ibox float-e-margins">

      @if($domain['type'] <= 4)

      <span class="pull-right locationAddress">
      <b>{{ $domainSiteConfig->locationName }}</b><br>
      @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
      @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
      @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
      @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
      </span>


      <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

      @endif   
      <div class="clearfix"></div>
        
      @if(Session::has('updateSuccess'))
      <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          {{ Session::get('updateSuccess') }}
      </div>
      <div class="clearfix"></div>
      @endif

      @if ($domain->type != 9 )
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/viewcart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
      @else
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/dine" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/dineincart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
      @endif

      <a class="btn btn-default btn-xs w150 m-b-sm m-r-xs" href="/customer/dashboard"><i class="fa fa-user m-r-xs"></i>My Info</a>
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/viewpayments" id="additems"><i class="fa fa-credit-card m-r-xs"></i>Payments</a>
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/password/reset" id="additems"><i class="fa fa-user-secret m-r-xs"></i>Password</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>

      <div class="ibox-title" style="border: none; margin-top: 4px;">
        <h5><i class="fa fa-list-alt m-r-xs"></i>My Orders</h5>
      </div>
      
      <div class="ibox-content">
        <div class="table-responsive m-t-md">
          <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    <th width="100">Order ID</th>
                    <th width="100">Order Date</th>
                    <th width="100">Order Amount</th>
                    <th width="100">View Order</th>
                </tr>
            </thead>
            <tbody>

            @if (is_null($orders))
            <tr><td>There are no orders on file.</td></tr>
            @else

            @foreach ($orders as $order)
            <tr>                
                <td>{{ $order->id }}</td>
                <td>{{ $order->created_at }}</td>
                <td>${{ $order->total }}</td>
                <td><a class="btn btn-order btn-xs w90 m-r-xs" href="/customer/view/orders/{{$order->id}}"><i class="fa fa-binoculars m-r-xs"></i>View Order</a></td>
            </tr>
            @endforeach
            
            @endif
            
            </tbody>                
          </table>


        </div>
      </div>
    </div>
</div>


<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
$(document).ready(function(){
    $('.dataTables').DataTable({
        pageLength: 25,
        dom: '<"html5buttons"B>lTfgitp',
        order: [[ 1, "desc" ]],
        buttons: [

            {
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]

    });

});


</script>

@endsection
