
@extends('orders.iframe')

@section('content')

@include('orders.cssUpdates')

<style>

input.faChkRnd, input.faChkSqr {
  visibility: hidden;
}

@-moz-document url-prefix() { 

input.faChkRnd, input.faChkSqr {
  visibility: visible;
}

 }

.faChkSqr, .faChkRnd {
      margin-left: 0px !important;
}

input.faChkRnd:checked:after, input.faChkRnd:after,
input.faChkSqr:checked:after, input.faChkSqr:after {
  visibility: visible;
  font-family: FontAwesome;
  font-size:21px;height: 17px; width: 17px;
  position: relative;
  top: -3px;
  left: 0px;
  background-color: #FFF;
    color: {{ $styles->bannerButtonBorderColor ?? '' }} !important;
  display: inline-block;
}

input.faChkRnd:checked:after {
  content: '\f058';
}

input.faChkRnd:after {
  content: '\f10c';
}

input.faChkSqr:checked:after {
  content: '\f14a';
}

input.faChkSqr:after {
  content: '\f096';
}

@media (max-width: 768px) {

  #page-wrapper {
      min-height: 1250px !important;
  }
}

@media (min-width: 769px) and (max-width: 991px) {

  #page-wrapper {
      min-height: 1150px !important;
  }
}

@media (min-width: 992px) {

  #page-wrapper {
      min-height: 1150px !important;
  }
}

</style>
   

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl" style="margin-top: 62px;">
    <div class="ibox float-e-margins">

      @if($domain['type'] <= 4)

      <span class="pull-right locationAddress">
      <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
      @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
      @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
      @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
      @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
      </span>


      <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

    @endif  

    <div class="clearfix"></div>
        @if ($domain->type != 9 )
        <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        <a class="btn btn-xs btn-default w150 m-b-sm" href="/viewcart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
        @else
        <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/dine" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
        <a class="btn btn-xs btn-default w150 m-b-sm" href="/dineincart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
        @endif



    <div class="clearfix"></div>

         @if(Session::has('loginError'))
          <div class="alert alert-danger alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginError') }}
          </div>
        @endif

        @if(Session::has('loginSuccess'))
          <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('loginSuccess') }}
          </div>
        @endif

        <div class="ibox-title">
            <h5><i class="fa fa-user m-r-xs"></i>My Information</h5>
        </div>
        
        
        <div class="ibox-content">


            <form method="POST" id="userRegisterForm" name="userRegisterForm" action="/userregister">
                
                <div class="form-group">

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email Address / Username<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="email" name="email" value="" required placeholder="Email Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile Number<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="tel" id="mobile" name="mobile" autocomplete="off" value="" required placeholder="Mobile Phone">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Password (min 6 characters)<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="password" id="password1" minlength="6" name="password1" value="" required placeholder="Password">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Confirm Password<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="password" id="password2" minlength="6" name="password2" value="" required placeholder="Confirm Password">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="text" name="fname" value="" required placeholder="First Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="text" name="lname" value="" required placeholder="Last Name">
                    </div>

                    <div class="clearfix"></div>
                    <hr>
                    <p><i>Optional Information</i></p>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address1</label>
                        <input class="form-control" type="text" name="address1" value="" placeholder="Address">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address2</label>
                        <input class="form-control" type="text" name="address2" value="" placeholder="Address2">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <label>City</label>
                        <input class="form-control" type="text" name="city" value="" placeholder="City">
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <label>State</label>
                        <select class="form-control" name="state" id="state">

                            @foreach ($states as $state)
                            <option value="{{ $state->stateCode }}" @if ($state->stateCode == $domainSiteConfig->state) selected @endif>{{ $state->stateName }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label>Zip Code</label>
                        <input class="form-control" type="text" name="zipCode" value="" placeholder="Zip Code">
                    </div>


                    <div class="clearfix"></div>

                    <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                      <label class="font-normal font-italic">Birthday:</label>
                      <input type="text" placeholder="MM/DD/YYYY" class="form-control" name="birthday" id="birthday" value="{{ old('birthday') }}">
                    </div>               

                    <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                      <label class="font-normal font-italic">Anniversary:</label>
                      <input type="text" placeholder="MM/DD/YYYY" class="form-control" name="anniversary" id="anniversary" value="{{ old('anniversary') }}">
                    </div>



                    <div class="col-md-10 col-sm-12 col-xs-12 m-t-md">
                      <table class="table">
                      <tbody><tr>
                        <td style="padding-top: 21px;"><input class="faChkSqr form-control" type="checkbox" name="sendTo" value="yes" checked></td>
                        <td style="padding-top: 20px;">I want to receive discount offers and promotions from {{ $domainSiteConfig->locationName}}. I understand my information will not be shared, sold or bartered and only used to receive information from {{ $domainSiteConfig->locationName}}.</td>
                      </tr></tbody>
                      </table>
                    </div>

                    <div class="clearfix"></div>
                    
                    <div class="col-md-6 col-md-offset-2 col-sm-6 col-sm-offset-2 col-xs-11 col-xs-pull-1 m-b-xl">

                        {{ csrf_field() }}
                        <input type="hidden" name="referer" value="{{$referer}}">
                        
                        <div id="returnMenu">
                          <a href="/viewcart" class="btn banner-button pull-right m-l-md w150" name="viewOrder" id="viewOrder"><i class="fa fa-times m-r-sm"></i>Cancel</a>
                          <button type="submit" class="btn banner-button pull-right w150" name="register" id="register"><i class="fa fa-check m-r-sm"></i>Register</a></button>
                        </div>
                        

                    </div>
                    <div class="clearfix" style="height: 20px;"></div>
                </div>
            </form>

        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$( document ).ready(function() {
  console.log("ready");
  $("#mobile").mask("(999) 999-9999");
  $("#zipCode").mask("99999?-9999");
  $("#birthday").mask("99/99/9999");
  $("#anniversary").mask("99/99/9999");
});
  
$('#mobile').change(function() {
  var num = $(this).val();
  var intNum =  num.replace(/[^\d]/g, '');

if ( intNum.length != 10 ) {
  $("#mobile").val(''); 
  alert("Only 10 digit phone numbers are allowed.");
  return false;
}

var fd = new FormData();
fd.append('mobile', $(this).val()),    

  $.ajax({
      url: "/api/v1/checkmobile",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] != "registered") {
            $('#mobile').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});




$('#register').click(function(e) {

  var p1 = $('#password1').val();
  var p2 = $('#password2').val();
  if ( p1 != p2 || p1 == '') {
    alert("There is an error with your passwords. Please retype your passwords.")
    $('#password1').val('');
    $('#password2').val('');
    e.preventDefault();
    return false;
  }
 
});


</script>

@endsection