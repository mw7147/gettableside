
@extends('orders.iframe')

@section('content')
@include('orders.cssUpdates')

<style>

body {background-color: white;}

input.faChkRnd, input.faChkSqr {
  visibility: hidden;
}

@-moz-document url-prefix() { 

input.faChkRnd, input.faChkSqr {
  visibility: visible;
}

 }

.faChkSqr, .faChkRnd {
      margin-left: 0px !important;
}

input.faChkRnd:checked:after, input.faChkRnd:after,
input.faChkSqr:checked:after, input.faChkSqr:after {
  visibility: visible;
  font-family: FontAwesome;
  font-size:21px;height: 17px; width: 17px;
  position: relative;
  top: -3px;
  left: 0px;
  background-color: #FFF;
    color: {{ $styles->bannerButtonBorderColor ?? '#333' }} !important;
  display: inline-block;
}

input.faChkRnd:checked:after {
  content: '\f058';
}

input.faChkRnd:after {
  content: '\f10c';
}

input.faChkSqr:checked:after {
  content: '\f14a';
}

input.faChkSqr:after {
  content: '\f096';
}

@media (max-width: 768px) {

  #page-wrapper {
      min-height: 1400px !important;
  }
}

@media (min-width: 769px) and (max-width: 991px) {

  #page-wrapper {
      min-height: 1300px !important;
  }
}

@media (min-width: 992px) {

  #page-wrapper {
      min-height: 1050px !important;
  }
}

.ibox-content {
  padding-bottom: 175px;
}

@media (max-width: 992px) {
  .ibox-content {
    padding-bottom: 25px;
  }
}

</style>

<div class="clearfix"></div>
   

<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 m-b-xl" style="margin-top: 70px;">
        


    <div class="ibox float-e-margins">

      @if($domain['type'] <= 4)

      <span class="pull-right locationAddress">
      <!-- <b>{{ $domainSiteConfig->locationName }}</b><br> -->
      @if (!empty($domainSiteConfig->address1)){{ $domainSiteConfig->address1 }}<br> @endif 
      @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
      @if (!empty($domainSiteConfig->city)){{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>@endif
      @if (!empty($domainSiteConfig->locationPhone))<i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}@endif
      </span>


      <img class="imgIframe pull-left logo" style="max-height: 96px;" src="https://{{ $domain->httpHost }}{{ $domain->logoPublic }}">

      @endif  
    
      <div class="clearfix"></div>
        
        @if(Session::has('updateSuccess'))
          <div class="alert alert-success alert-dismissable col-md-12 m-b-xl">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              {{ Session::get('updateSuccess') }}
          </div>
          <div class="clearfix"></div>
        @endif


      @if ($domain->type != 9 )
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/order" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/viewcart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
      @else
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/dine" id="additems"><i class="fa fa-bars m-r-xs"></i>Menu</a>
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/dineincart" id="additems"><i class="fa fa-shopping-cart m-r-xs"></i>Cart</a>
      @endif

      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/orders"><i class="fa fa-list-alt m-r-xs"></i>Orders</a>
      <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/viewpayments"><i class="fa fa-credit-card m-r-xs"></i>Payments</a>
      {{-- <a class="btn btn-xs btn-default w150 m-b-sm m-r-xs" href="/customer/password/reset"><i class="fa fa-user-secret m-r-xs"></i>Password</a> --}}
      <a class="btn btn-xs btn-default w150 m-b-sm" href="/userlogout"><i class="fa fa-user m-r-xs"></i>Logout</a>


        
        <div class="ibox-title" style="border: none; margin-top: 4px;">
            <h5><i class="fa fa-id-card m-r-xs"></i>My Information</h5>
        </div>
        
        
        <div class="ibox-content">


            <form method="POST" id="myInformationForm" name="myInformationForm" action="/customer/dashboard">
                
                <div class="form-group">

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Email Address / Username<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="email" name="email" value="@if(isset($user->email)){{$user->email}}@endif" required placeholder="Email Address">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Mobile Number<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="text" id="mobile" name="mobile" autocomplete="off" value="@if(isset($user->mobile)){{$user->mobile}}@endif" required placeholder="Mobile Phone">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>First Name<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="text" name="fname" value="@if(isset($user->fname)){{$user->fname}}@endif" required placeholder="First Name">
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Last Name<sup class="text-danger m-l-xs">*</sup></label>
                        <input class="form-control" type="text" name="lname" value="@if(isset($user->lname)){{$user->lname}}@endif" required placeholder="Last Name">
                    </div>

                    <div class="clearfix"></div>
                    <hr>
                    <p><i>Optional Information</i></p>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address1</label>
                        <input class="form-control" type="text" name="address1" value="@if(isset($user->usersData->address1)){{$user->usersData->address1}}@endif" placeholder="Address">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label>Address2</label>
                        <input class="form-control" type="text" name="address2" value="@if(isset($user->usersData->address2)){{$user->usersData->address2}}@endif" placeholder="Address2">
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <label>City</label>
                        <input class="form-control" type="text" name="city" value="@if(isset($user->usersData->city)){{$user->usersData->city}}@endif" placeholder="City">
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <label>State</label>
                        <input class="form-control" type="text" name="state" value="@if(isset($user->usersData->state)){{$user->usersData->state}}@endif" placeholder="State">
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label>Zip Code</label>
                        <input class="form-control" type="text" name="zipCode" value="{{$user->usersData->zip ?? ''}}" placeholder="Zip Code">
                    </div>


                    <div class="clearfix"></div>

                    <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                      <label class="font-normal font-italic">Birthday:</label>
                      <input type="text" placeholder="MM/DD/YYYY" class="form-control" name="birthday" id="birthday" value="@isset($user->contacts->birthday){{ \Carbon\carbon::parse($user->contacts->birthday)}}@endisset ">
                  </div>
  
                  <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                      <label class="font-normal font-italic">Anniversary:</label>
                      <input type="text" placeholder="MM/DD/YYYY" class="form-control" name="anniversary" id="anniversary" value="@isset($user->contacts->anniversary){{ \Carbon\carbon::parse($user->contacts->anniversary)->format('m/d/Y') }} @endisset ">
                  </div>

                    <div class="clearfix"></div>


                    <div class="col-md-10 col-sm-12 col-xs-12 m-t-md">
                      <table class="table">
                      <tbody><tr>
                        <td style="padding-top: 21px;"><input class="faChkSqr form-control" type="checkbox" name="sendTo" value="yes" checked></td>
                        <td style="padding-top: 20px;">I want to receive discount offers and promotions from {{ $domainSiteConfig->locationName}}. I understand my information will not be shared, sold or bartered and only used to receive information from {{ $domainSiteConfig->locationName}}.</td>
                      </tr></tbody>
                      </table>
                    </div>

                    <div class="col-md-12 m-b-md">

                        {{ csrf_field() }}
                        
                        <div id="returnMenu">
                          <button type="submit" class="btn banner-button" name="register" id="register"><i class="fa fa-check m-r-sm"></i>Update My Information</a></button>
                        </div>


                    </div>

                </div>
            </form>

        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$( document ).ready(function() {
    // $("#mobile").mask("(999) 999-9999");
    $("#zipCode").mask("99999?-9999");
    $("#birthday").mask("99/99/9999");
    $("#anniversary").mask("99/99/9999");
   
    // $('#mobile').change(function() {
    // var fd = new FormData();
    // fd.append('mobile', $(this).val()),    

    //   $.ajax({
    //       url: "/api/v1/checkmobile",
    //       type: "POST",
    //       data: fd,
    //       contentType: false,
    //       cache: false,
    //       processData:false,   
    //       success: function(mydata) {
    //         var data = JSON.parse(mydata);           
    //         console.log(data);
    //         if (data["status"] != "registered") {
    //             $('#mobile').val('');
    //             alert(data["message"]);
    //             return false;
    //         }
    //         return true;
    //       },
    //       error: function() {
    //         alert("There was an problem with your mobile number. Please try again.");
    //       }
    //   }); 
    // });


});


</script>

@endsection