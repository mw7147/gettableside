<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TokinMail</title>

    </head>
    
    <style>
.btn {
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 18px;
  background: #1ab394;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

.btn:hover {
  background: #22c9a5;
  text-decoration: none;
}


    
    
    
    </style>
    <body style="background-color: #000;">
        <div class="flex-center position-ref full-height">

                <a class="btn" href="/login" alt="Login" style="position: absolute; left: 30%; top: 55%; z-index: 1;">TokinMail Login</a>


            <img src="/img/tokintechsplash.jpg" alt="Tokin Technologies" style="width: 100%; height: auto; position: relative; z-index: -1;">
            
            
        </div>
    </body>
</html>
