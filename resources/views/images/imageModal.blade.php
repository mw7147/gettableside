
    <div class="modal inmodal fade in" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-lg" style="min-width: ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Attache Images</h4>
                    <small class="font-bold">All images in your attache folder are shown below. Select "Send Image" from the actions list by an image to include the image in your text message.</small>
                </div>
                                
                <div class="col-lg-12 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
          
                            @for ($i = 1; $i <= COUNT($files); $i++)

                            <div class="file-box m-t-md" style="width: 205px;">
                                <div class="file">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <a class="pop"><img alt="image" class="img-responsive" src="{{ $files[$i]['storagePath'] }}" onclick="showPicture(this)"></a>
                                        </div>
                                        <div class="file-name  over-scroll">
                                            {{ $files[$i]['name'] }}
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                            <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">

                                            <li><a href="{{ $files[$i]['storagePath'] }}" target="_blank">View In New Window</a></li>
                                            <li><a href="#" class="copyemailpath" onclick="addImage('{{ $files[$i]['storagePath'] }}')">Send Image</a></li>


                                        </ul> 
                                    </div>   


                                </div>
                            </div>

                            @endfor

                            <div style="visibility: hidden"><input type="text" id="downloadLink" value=""></div>

                        </div>
                    </div>
                </div>





                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<script>

    function addImage(storagePath){

        /*
        var name = storagePath.split('/');
        var filenamePart = name.length-1
        var filename = name[filenamePart];
        */

        var sendImages = $('#sendImages').val();
        if (sendImages.length > 0) {
            sendImages = sendImages + ", " + storagePath;
        } else {
            sendImages = storagePath;
        }

        $('#sendImages').val(sendImages);
        var imageSrc = '<img src="' + storagePath + '" class="img-responsive col-md-2 col-sm-2 col-xs-2">';
        $('#imageDiv').append(imageSrc);

    } 

</script>
