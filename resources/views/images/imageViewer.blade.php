<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="TokinText Image Viewer">
    <meta name="author" content="riverhouseit.com">

    <title>{{ $domain['name'] }} Image Viewer</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ elixir('css/bootfont.css') }}" />

    <!-- Custom CSS -->
    <link href="{{ elixir('css/rhit_admin.css') }}" rel="stylesheet">


    <!-- Scripts -->

    <script src="{{ elixir('js/bootvuequery.js') }}"></script>
    <script>

        function setSize(){
            if (screen.width > 600 ){
                var width = screen.width *.58;
                var height = screen.height*.58;
                window.moveTo(((screen.width - width) / 2), ((screen.height - height) / 2));      
                window.resizeTo(width, height);
            } else {
                var width = window.width *.9;
                var height = window.height*.9;
                window.resizeTo(width, height);    
            }
        }

    </script>

</head>

<body class="gray-bg" onLoad="setSize();">
    <div id="wrapper">

        @yield('content')



    </div>

<!-- Scripts -->

</body>
</html>