@extends('imageViewer.imageViewer')

@section('content')

<!-- Page-Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">


<div class="ibox-content col-md-12">


    <h3 class="bg-success p-sm">{{ $domain['name'] }} Image Library</h3>


    <h3 style="margin: 20px 0">Folders</h3>
    <hr>

    @if(Session::has('fileError'))
        <div class="clearfix"></div>
        <div class="alert alert-warning alert-dismissable col-xs-12 col-sm-10 col-md-9 col-lg-7 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('fileError') }}
        </div>
        <div class="clearfix"></div>

    @endif



    <button type="button" class="btn btn-primary btn-xs m-b-lg" data-toggle="modal" data-target="#newFolderModal"><i class="fa fa-plus-square m-r-xs"></i>Create New Folder</button>

    <div class="clearfix"></div>

    <p>basefolder: {{basename($baseFolder)}}</p>

    <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn @if (basename($baseFolder) == 'home')btn-warning @else btn-info @endif dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/home?location=ckeditor';">

            @if (basename($baseFolder) == 'home')
                <i class="fa fa-folder-open fa-2x"></i>
            @else
                <i class="fa fa-folder fa-2x"></i>
            @endif

            <h5 class="text-center m-b-none" style="text-transform: none;">Home</h5>
        </button>    
    </div>

    @if (basename($baseFolder) != 'home')
   <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn btn-warning dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/{{ basename($baseFolder) }}?location=ckeditor';">
            <i class="fa fa-folder-open fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">{{ basename($baseFolder) }}</h5>
        </button>    
    </div>
    @endif

    @foreach ($directories as $directory)
    
    @if (basename($baseFolder) == 'home')
    
    <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn btn-info dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/{{ basename($directory) }}?location=ckeditor';"><i class="fa fa-folder fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">{{ basename($directory) }}</h5>
        </button>
        <div class="clearfix"></div>  
        <div class="text-center m-t-n-sm" style="width: 130px;">
            <button class="btn btn-danger btn-xs btn btn-outline text-danger b-r-md m-t-sm" onclick="deleteFolder('{{ basename($directory) }}')" style="font-size: 10px; text-transform: none;"><i class="fa fa-times m-r-xs"></i>Delete</button>
        </div>
    </div>

    @else

    <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn btn-info dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/{{ $baseFolder }}/{{ basename($directory) }}?location=ckeditor';"><i class="fa fa-folder fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">{{ basename($directory) }}</h5>
        </button>
        <div class="clearfix"></div>  
        <div class="text-center m-t-n-sm" style="width: 130px;">
            <button class="btn btn-danger btn-xs btn btn-outline text-danger b-r-md m-t-sm" onclick="deleteFolder('{{ basename($directory) }}')" style="font-size: 10px; text-transform: none;"><i class="fa fa-times m-r-xs"></i>Delete</button>
        </div>
    </div>

    @endif
    
    @endforeach








    <div class="clearfix"></div>

    <h3 style="margin-top: 40px; margin-bottom: 20px;">Images</h3>
    <p>Current Folder: <b>{{ basename($baseFolder) }}</b></p>
    <p><i><small>Select "Actions->Insert Picture" to insert picture into template.</small></i></p>
    <hr>
    
    <button type="button" class="btn btn-primary btn-xs m-b-lg" data-toggle="modal" data-target="#newImageModal"><i class="fa fa-plus-square"></i> Upload New Image</button>

    <div class="clearfix m-b-md"></div>

    @foreach ($files as $file)

    @if (strtolower(basename($file)) == '.ds_store')
        @continue
    @endif



    <div class="col-md-2 imageGallery m-b-md">

        <span style="font-size: 11px;">{{basename($file)}}</span>
        <div class="imageGallery-Pix">
            <a class="pop">
                <img src="{{ Storage::url($file) }}" onclick="showPicture(this)">
            </a>
        </div>

        
        <!-- Split button -->
        
        <div class="btn-group dropup text-center">
            <button type="button" class="btn btn-warning btn-xs">Actions</button>
            <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>

            <ul class="dropdown-menu">
                <li><a href="#" class="delete" onclick="deleteImage('{{basename($file)}}')">Delete</a></li>
                <!--<li><a href="#">Edit</a></li>-->
                <!--<li><a href="#">Duplicate</a></li>-->
                <li><a href="#" class="copyurl" data-clipboard-text="{{Storage::url($file)}}">Copy Path</a></li>
                <li><a href="#" class="copyurl" onclick="returnFileUrl('{{Storage::url($file)}}')">Insert Picture</a></li>

            </ul> 
        </div>   

    </div>

    @endforeach
    
</div>


<!-- Modals -->

<!-- New Folder -->

<div class="modal fade" id="newFolderModal" tabindex="-1" role="dialog" aria-labelledby="newGroupModal">
  <div class="modal-dialog modal-sm" role="document">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="newModalLabel"><i class="fa fa-folder m-r-sm" style="font-size: 20px;"></i>Create New Folder</h4>
        
        </div>
      
        <form name="newGroupForm" method="POST" action="/customers/images/new/folder">
        
            <div class="modal-body form-group">
                <small><label>Folder Name</label></small><br>

                <input type="text" class="form-control" name="newFolderName" placeholder="New Folder Name" value="" required>

            </div>

            <div class="modal-footer">
                <input type="hidden" name="ckeditor" value="ckeditor">
                <input type="hidden" name="baseFolder" value="{{ $baseFolder }}">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Create New Folder</button>
      
            </div>

            {{ csrf_field() }}

        </form>
    </div>

  </div>
</div>


<!-- New Image -->

<div class="modal fade" id="newImageModal" tabindex="-1" role="dialog" aria-labelledby="newModalImage">
  <div class="modal-dialog modal-sm" role="document" style="width: 400px;">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="imgModalLabel"><i class="fa fa-image m-r-sm" style="font-size: 20px;"></i>Upload New Image</h4>
        
        </div>

        <div class="modal-body form-group" style="padding-bottom: 0px;">

            <form name="newImageForm" method="POST" action="/customers/images/new/image" enctype="multipart/form-data">
                <div class="input-group m-b col-xs-11 nodisplay">
                    <div class="fileinput fileinput-new input-group m-l-md" data-provides="fileinput">

                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select Picture</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="newImage" />
                        </span>
                        
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>

                        <div class="form-control" data-trigger="fileinput" style="border: none; font-size: 11px;">
                            <span class="fileinput-filename fileinput-exists"></span>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="ckeditor" value="ckeditor">
                    <input type="hidden" name="baseFolder" value="{{ $baseFolder }}">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Upload New Image</button>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>


<!-- Image -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <span class="text-center"> <img src="" id="imagepreview" style="max-width: 100%;" ></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Page Level Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.15/clipboard.min.js"></script>
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>

    // Helper function to get parameters from the query string.
    function getUrlParam( paramName ) {
        var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
        var match = window.location.search.match( reParam );

        return ( match && match.length > 1 ) ? match[1] : null;
    }

    // Select a file to be returned to CKEditor.
    function returnFileUrl(fileUrl) {

        var funcNum = getUrlParam( 'CKEditorFuncNum' );
        // alert(funcNum);
        // set value to id of ckeditor
        if (funcNum === null) {funcNum = 1;}

        // var fileUrl = fileName;
        window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl, function() {
            // Get the reference to a dialog window.
            var dialog = this.getDialog();
            // Check if this is the Image Properties dialog window.
            if ( dialog.getName() == 'image' ) {
                // Get the reference to a text field that stores the "alt" attribute.
                var element = dialog.getContentElement( 'info', 'txtAlt' );
                // Assign the new value.
                if ( element )
                    element.setValue( 'alt text' );
            }
            // Return "false" to stop further execution. In such case CKEditor will ignore the second argument ("fileUrl")
            // and the "onSelect" function assigned to the button that called the file manager (if defined).
            // return false;
        } );
        window.close();
    }
</script>


<script>


    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    function deleteFolder(folder) {
        var sure = confirm("Are you sure you wish to delete this folder. All files within the folder will be deleted!");
        if (!sure) {return false;}
        var baseFolder = "{{ $baseFolder }}";
        var fd = new FormData();    
        fd.append('baseFolder', baseFolder);
        fd.append('folder', folder);
        fd.append('cid', {{ $cid }})
        $.ajax({
            url: "/api/v1/deleteCustomerFolder",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    alert(jsondata["message"]); 
                    return false;   
                        
                } else {
                    console.log(jsondata["message"]);
                    location.reload();
                    return true;    
                }
            }
        });
    };

    function deleteImage(fileName) {
        var sure = confirm("Are you sure you wish to delete this image? The image will be deleted and not recoverable!");
        if (!sure) {return false;}
        var baseFolder = "{{ $baseFolder }}";
        var fd = new FormData();    
        fd.append('baseFolder', baseFolder);
        fd.append('rootDir', 'home');
        fd.append('cid', {{ $cid }});
        fd.append('fileName', fileName);
        $.ajax({
            url: "/api/v1/deleteCustomerImage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    alert(jsondata["message"]); 
                    return false;   
                        
                } else {
                    console.log(jsondata["message"]);
                    location.reload();
                    return true;    
                }
            }
        });
    };



    function showPicture(param) {
        var pix = $(param).attr('src')
        var file = pix.split('\/').pop();
        $('#myModalLabel').html('<i class="fa fa-image fa-2x m-r-xs"></i>' + file);
        $('#imagepreview').attr('src', $(param).attr('src'));
        $('#imagemodal').modal('show');   
    };

new Clipboard('.copyurl');

</script>

@endsection