@extends('customer.customerAdmin')

@section('content')

<!-- Page-Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">




<div class="row wrapper m-b-md m-t-md white-bg page-heading">
    
    <div class="col-lg-10">
        
        <h2>Image Gallery</h2>
                 
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-t-sm m-b-md"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/customers/dashboard" class="btn btn-info btn-xs m-t-sm m-b-md"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        
        <div class="clearfix"></div>
        
        <a class="btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Instructions</a>
        <a class="btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Instructions</a>
    
    </div>
                
    <div class="clearfix"></div>

    <div class="instructions m-t-md m-l-md">
        <h4>
            Directories and Images are listed below
        </h4>

        <ul>
            <li>Use the dropdown under each picture to select options.</li>
            <!--<li>Select the "Edit" option to edit the selected picture.</li>-->
            <li>Select the "Copy URL" option to copy the URL to your clipboard.</li>
            <!--<li>Select the "Duplicate" option to duplicate (copy) the selected picture. Useful if you wish to edit the picture but save the original.</li>-->
            <li>Select the "Delete" option to delete the picture.</li>
        </ul>
    </div>

</div>


<div class="ibox-content col-md-12">



    <h3 style="margin: 20px 0">Folders</h3>
    <hr>

    @if(Session::has('fileError'))
        <div class="clearfix"></div>
        <div class="alert alert-warning alert-dismissable col-xs-12 col-sm-10 col-md-9 col-lg-7 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('fileError') }}
        </div>
    @endif

    <div class="clearfix"></div>

    <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn @if (basename($baseFolder) == 'home')btn-warning @else btn-info @endif dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/home';">
            <i class="fa fa-folder fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">Home</h5>
        </button>    
    </div>

    <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn @if (basename($baseFolder) == 'home')btn-warning @else btn-info @endif dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/{{ $rootDir }}';">
            <i class="fa fa-folder fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">{{ $rootDir }}</h5>
        </button>    
    </div>

   <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn btn-warning dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/{{ $rootDir }}/{{ basename($baseFolder) }}';">
            <i class="fa fa-folder-open fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">{{ basename($baseFolder) }}</h5>
        </button>    
    </div>


    @foreach ($directories as $directory)
    


    <div class="m-t-sm m-b-sm m-l-md col-xs-5 col-sm-3 col-md-2">
        <button class="btn btn-info dim btn-outline" style="width: 130px; height: 90px;" type="button" onclick="location.href='/customers/images/view/{{ $baseFolder }}/{{ basename($directory) }}';"><i class="fa fa-folder fa-2x"></i>
            <h5 class="text-center m-b-none" style="text-transform: none;">{{ basename($directory) }}</h5>
        </button>
        <div class="clearfix"></div>  
        <div class="text-center m-t-n-sm" style="width: 130px;">
            <button class="btn btn-danger btn-xs btn btn-outline text-danger b-r-md m-t-sm" onclick="deleteFolder('{{ basename($directory) }}')" style="font-size: 10px; text-transform: none;"><i class="fa fa-times m-r-xs"></i>Delete</button>
        </div>
    </div>


    
    @endforeach








    <div class="clearfix"></div>

    <h3 style="margin-top: 40px; margin-bottom: 20px;">Images</h3>
    <p>Current Folder: <b>{{ basename($baseFolder) }}</b></p>
    <hr>
    
    <button type="button" class="btn btn-primary btn-xs m-b-lg" data-toggle="modal" data-target="#newImageModal"><i class="fa fa-plus-square"></i> Upload New Image</button>

    <div class="clearfix m-b-md"></div>


        @foreach ($files as $file)

        @if (strtolower(basename($file)) == '.ds_store')
            @continue
        @endif

        
        <div class="col-md-2 imageGallery m-b-md">
        <span style="font-size: 11px;">{{basename($file)}}</span>
        <div class="imageGallery-Pix">
            <a class="pop">
                <img src="{{ Storage::url($file) }}" onclick="showPicture(this)">
            </a>
        </div>

            <div class="clearfix m-t-sm"></div>
            
            <!-- Split button -->
            
            <div class="btn-group dropup">
                <button type="button" class="btn btn-warning btn-xs">Actions</button>
                <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>

                <ul class="dropdown-menu">
                    <li><a href="" onclick="deleteImage('{{basename($file)}}')">Delete</a></li>
                    <!--<li><a href="#">Edit</a></li>-->
                    <!--<li><a href="#">Duplicate</a></li>-->
                <li><a href="#" class="copyurl" data-clipboard-text="{{Storage::url($file)}}">Copy URL</a></li>

                </ul> 

            </div>   

        </div>
        
        @endforeach



</div>


<!-- Modals -->

<!-- New Image -->

<div class="modal fade" id="newImageModal" tabindex="-1" role="dialog" aria-labelledby="newModalImage">
  <div class="modal-dialog modal-sm" role="document" style="width: 400px;">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="imgModalLabel"><i class="fa fa-image m-r-sm" style="font-size: 20px;"></i>Upload New Image</h4>
        
        </div>

        <div class="modal-body form-group" style="padding-bottom: 0px;">

            <form name="newImageForm" method="POST" action="/customers/images/new/image/level2" enctype="multipart/form-data">
                <div class="input-group m-b col-xs-11 nodisplay">
                    <div class="fileinput fileinput-new input-group m-l-md" data-provides="fileinput">

                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select Picture</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="newImage" />
                        </span>
                        
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>

                        <div class="form-control" data-trigger="fileinput" style="border: none; font-size: 11px;">
                            <span class="fileinput-filename fileinput-exists"></span>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="baseFolder" value="{{ $baseFolder }}">
                    <input type="hidden" name="rootdir" value="{{ $rootDir }}">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Upload New Image</button>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>

<!-- Image -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <span class="text-center"> <img src="" id="imagepreview" style="max-width: 100%;" ></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Page Level Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.15/clipboard.min.js"></script>
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>


    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

        function deleteImage(fileName) {
        var sure = confirm("Are you sure you wish to delete this image? The image will be deleted and not recoverable!");
        if (!sure) {return false;}
        var baseFolder = "{{ $baseFolder }}";
        var rootDir = "{{ $rootDir }}";
        var fd = new FormData();    
        fd.append('baseFolder', baseFolder);
        fd.append('rootDir', rootDir);
        fd.append('cid', {{ $cid }});
        fd.append('fileName', fileName);
        $.ajax({
            url: "/api/v1/deleteCustomerImage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    alert(jsondata["message"]); 
                    return false;   
                        
                } else {
                    console.log(jsondata["message"]);
                    location.reload();
                    return true;    
                }
            }
        });
    };


    function showPicture(param) {
        var pix = $(param).attr('src')
        var file = pix.split('\/').pop();
        $('#myModalLabel').html('<i class="fa fa-image fa-2x m-r-xs"></i>' + file);
        $('#imagepreview').attr('src', $(param).attr('src'));
        $('#imagemodal').modal('show');   
    };

    new Clipboard('.copyurl');

</script>

@endsection