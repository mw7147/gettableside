
    <div class="modal inmodal fade in" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-lg" style="min-width: ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Attache Files</h4>
                    <small class="font-bold">All files in your attache folder are shown below. Select "Send File" from the actions list below a file to attach the file to your email message.</small>
                </div>
                                
                <div class="col-lg-12 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
          
                            @for ($i = 1; $i <= COUNT($files); $i++)

                            






                            @if($files[$i]['filetype'] == 'file')
                            

                            <div class="file-box" style="max-width: 205px;">
                                <div class="file">

                                        <span class="corner"></span>

                                        <div class="icon">
                                            <i class="{{ $files[$i]['fileclass'] }}"></i>
                                        </div>

                                        <div class="file-name over-scroll">
                                            <span>{{ $files[$i]['name'] }}</span>
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                    <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li><a href="#" class="copyemailpath" onclick="addFile('{{ $files[$i]['storagePath'] }}', '{{ $files[$i]['fileclass'] }}', '{{ $files[$i]['name'] }}')">Attach File</a></li>
                                        </ul> 
                                    </div>   

                                </div>
                            </div>

                            @else





                            <div class="file-box" style="max-width: 205px;">
                                <div class="file">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <a class="pop"><img alt="image" class="img-responsive" src="{{ $files[$i]['storagePath'] }}" onclick="showPicture(this)"></a>
                                        </div>

                                        <div class="file-name  over-scroll">
                                            {{ $files[$i]['name'] }}
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                            <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li><a href="{{ $files[$i]['storagePath'] }}" target="_blank">View In New Window</a></li>
                                            <li><a href="#" class="copyemailpath" onclick="addImage('{{ $files[$i]['storagePath'] }}', '{{ $files[$i]['name'] }}')">Attach File</a></li>
                                        </ul> 
                                    </div>   


                                </div>
                            </div>



                            @endif

                            @endfor

                            <div style="visibility: hidden"><input type="text" id="downloadLink" value=""></div>

                        </div>
                    </div>
                </div>





                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<script>

    function addImage(storagePath, fileName){

        /*
        var name = storagePath.split('/');
        var filenamePart = name.length-1
        var filename = name[filenamePart];
        */

        var sendImages = $('#sendImages').val();
        if (sendImages.length > 0) {
            sendImages = sendImages + ", " + storagePath;
        } else {
            sendImages = storagePath;
        }

        $('#sendImages').val(sendImages);
        var imageSrc = '<div class="col-md-2 col-sm-2 col-xs-2" style="display: block;"><img src="' + storagePath + '" class="img-responsive"><span class="text-center"><small>' + fileName + '</small></span></div>';
        $('#imageDiv').append(imageSrc);

    } 


function addFile(storagePath, fileClass, fileName){

        var sendImages = $('#sendImages').val();
        if (sendImages.length > 0) {
            sendImages = sendImages + ", " + storagePath;
        } else {
            sendImages = storagePath;
        }

        $('#sendImages').val(sendImages);
        var imageSrc = '<div class="col-md-2 col-sm-2 col-xs-2" style="display: block;"><i class="' + fileClass + ' fa-8x"></i><span class="text-center"><small>' + fileName + '</small></span></div>';
        $('#imageDiv').append(imageSrc);

    } 
</script>
