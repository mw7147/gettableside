<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="TokinMail">
    <meta name="author" content="Hardwired Creative Technologies">

    <title>TokinMail</title>

    <!-- rhit Core CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
    <link href="{{ mix('css/rhit_admin.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <!--<link rel="icon" type="image/png" href="favicon.png">-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- HWCT Core JavaScript -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/rhit_admin.js') }}"></script>
</head>
<body style="background-color: white; margin: 15px;">
            <div class="ibox" style="background-color: white;">
              
                    <h2 class="modal-title">Attache Images</h2>
                    <p class="font-bold">All images in your attache folder are shown below. Select "Send Image" from the actions list below an image to include the image in your email message..</p>
   
                                
                <div class="ibox-content col-lg-12 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">

                            @if (count($files) == 0)There are no files in your attache folder. @endif
          
                            @for ($i = 1; $i <= COUNT($files); $i++)










                            @if($files[$i]['filetype'] == 'file')
                            

                            <div class="file-box">
                                <div class="file">

                                        <span class="corner"></span>

                                        <div class="icon">
                                            <i class="{{ $files[$i]['fileclass'] }}"></i>
                                        </div>

                                        <div class="file-name over-scroll">
                                            <span>{{ $files[$i]['name'] }}</span>
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                    <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li><a href="#" class="copyemailpath copyurl" onclick="returnFileUrl('{{ $files[$i]['storagePath'] }}')">Insert Image</a></li>
                                        </ul> 
                                    </div>   

                                </div>
                            </div>

                            @else





                            <div class="file-box m-t-md" style="max-width: 205px;">
                                <div class="file">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <a class="pop"><img alt="image" class="img-responsive" src="{{ $files[$i]['storagePath'] }}" onclick="showPicture(this)"></a>
                                        </div>

                                        <div class="file-name  over-scroll">
                                            {{ $files[$i]['name'] }}
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                            <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li><a href="{{ $files[$i]['storagePath'] }}" target="_blank">View In New Window</a></li>
                                            <li><a href="#" class="copyemailpath copyurl" onclick="returnFileUrl('{{ $files[$i]['storagePath'] }}')">Insert Image</a></li>
                                        </ul> 
                                    </div>   


                                </div>
                            </div>







                            @endif

                            @endfor

                            <div style="visibility: hidden"><input type="text" id="downloadLink" value=""></div>

                        </div>
                    </div>
                </div>

            </div>

<script>

    // Helper function to get parameters from the query string.
    function getUrlParam( paramName ) {
        var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
        var match = window.location.search.match( reParam );

        return ( match && match.length > 1 ) ? match[1] : null;
    }

    // Select a file to be returned to CKEditor.
    function returnFileUrl(fileUrl) {

        var funcNum = getUrlParam( 'CKEditorFuncNum' );
        // alert(funcNum);
        // set value to id of ckeditor
        if (funcNum === null) {funcNum = 1;}

        // var fileUrl = fileName;
        window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl, function() {
            // Get the reference to a dialog window.
            var dialog = this.getDialog();
            // Check if this is the Image Properties dialog window.
            if ( dialog.getName() == 'image' ) {
                // Get the reference to a text field that stores the "alt" attribute.
                var element = dialog.getContentElement( 'info', 'txtAlt' );
                // Assign the new value.
                if ( element )
                    element.setValue( 'alt text' );
            }
            // Return "false" to stop further execution. In such case CKEditor will ignore the second argument ("fileUrl")
            // and the "onSelect" function assigned to the button that called the file manager (if defined).
            // return false;
        } );
        window.close();
    }

</script>


<script>

    function addImage(storagePath){

        /*
        var name = storagePath.split('/');
        var filenamePart = name.length-1
        var filename = name[filenamePart];
        */

        var sendImages = $('#sendImages').val();
        if (sendImages.length > 0) {
            sendImages = sendImages + ", " + storagePath;
        } else {
            sendImages = storagePath;
        }

        $('#sendImages').val(sendImages);
        var imageSrc = '<img src="' + storagePath + '" class="img-responsive col-md-2 col-sm-2 col-xs-2">';
        $('#imageDiv').append(imageSrc);

    } 

</script>

</body>
