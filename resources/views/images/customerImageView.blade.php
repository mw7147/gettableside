@extends('admin.admin')

@section('content')

<!-- Page-Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">




<div class="row wrapper m-b-sm m-t-sm white-bg page-heading view-instructions">
    
    <div class="col-lg-10">
        
        <h2>Attaché Files & Folders</h2>
                 
        <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-t-sm m-b-md"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-t-sm m-b-md"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        
        <div class="clearfix"></div>
        
        <span v-if="instructions == 'hide'"><a class="btn-instructions m-t-sm m-b-md" v-on:click="showInstructions()"><i class="fa fa-toggle-down m-r-xs"></i>Show Instructions</a></span>
        <span v-if="instructions == 'show'"><a class="btn-instructions m-t-sm m-b-md m-r-xs" v-on:click="hideInstructions()"><i class="fa fa-toggle-up m-r-xs"></i>Hide Instructions</a></span>
    
    </div>
                
    <div class="clearfix"></div>

    <div class="m-t-md m-l-md" v-if="instructions == 'show'">
        <h4>
            Directories and Images are listed below
        </h4>

        <ul>
            <li>Use the dropdown under each picture to select options.</li>
            <!--<li>Select the "Edit" option to edit the selected picture.</li>-->
            <li>Select the "Copy URL" option to copy the URL to your clipboard.</li>
            <!--<li>Select the "Duplicate" option to duplicate (copy) the selected picture. Useful if you wish to edit the picture but save the original.</li>-->
            <li>Select the "Delete" option to delete the picture.</li>
        </ul>
    </div>

</div>


<div class="ibox-content col-md-12">



    <h3 style="margin: 20px 0">Folders</h3>
    <hr>

    @if(Session::has('fileError'))
        <div class="clearfix"></div>
        <div class="alert alert-warning alert-dismissable col-xs-12 col-sm-10 col-md-9 col-lg-7 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('fileError') }}
        </div>
        <div class="clearfix"></div>

    @endif


    <div  id="newFolder" v-if="homeDir == 'home'">
        <button type="button" class="btn btn-primary btn-xs m-b-lg" data-toggle="modal" data-target="#newFolderModal"><i class="fa fa-plus-square m-r-xs"></i>Create New Folder</button>
    </div>

    <div class="clearfix"></div>




            <div class="row" id="files">
                
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content attache-file-manager">
                            <div class="file-manager">

                                <span data-toggle="tooltip" data-placement="bottom" title="Upload A File" ><button class="btn btn-primary btn-block" data-toggle="modal" data-target="#newFileModal">Upload Files</button></span>
                                <div class="hr-line-dashed"></div>
                                <h5 class="m-b-sm"><span class="pull-left">Folders</span><span class="pull-right">{{ $storageUsed['storageUsed'] }} of {{ $storageUsed['maxStorage'] }} Used</span></h5>
                                <div class="clearfix"></div>
                                <ul class="folder-list" style="padding: 0">

                                @if ($homedir == 'home')
                                    <li>
                                        <a href="/attache/view/home"><i class="fa fa-folder-open attache-open"></i>home</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="/attache/view/home"><i class="fa fa-folder"></i>home</a>
                                    </li>
                                @endif

                                @for ($i = 0; $i < COUNT($directories); $i++)
                                    <li>
                                        <span data-toggle="tooltip" data-placement="bottom" title="Delete Folder {{ $directories[$i][1] }}"><a href="/attache/delete/{{ $directories[$i][1] }}" class="pull-right" onclick="return confirm('Are you sure you wish to delete this folder. All files within the folder will be deleted!');"><i class="fa fa-window-close size16 danger-color"></i></a></span>

                                        <a href="/attache/view/{{ $directories[$i][1] }}">
                                        @if ( $directories[$i][1] == $homedir)<i class="fa fa-folder-open attache-open"> @else<i class="fa fa-folder">@endif
                                        </i>{{ $directories[$i][1] }}</a>

                                    </li>
                                @endfor

                                </ul>


                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">


          
                            @for ($i = 1; $i <= COUNT($files); $i++)

                            @if($files[$i]['filetype'] == 'file')
                            <div class="file-box">
                                <div class="file">

                                        <span class="corner"></span>

                                        <div class="icon">
                                            <i class="{{ $files[$i]['fileclass'] }}"></i>
                                        </div>

                                        <div class="file-name over-scroll">
                                            <span>{{ $files[$i]['name'] }}</span>
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                    <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li><a href="/attache/deletefile/{{ $files[$i]['homeDir'] }}/{{ $files[$i]['name'] }}" class="delete" onclick="return confirm('Are you sure you wish to delete this file? The file will be deleted and not recoverable!');">Delete</a></li>



                                            @if ($files[$i]['extension'] == 'pdf')<li><a href="{{ $files[$i]['storagePath'] }}" target="_blank">View PDF</a></li>@endif
                                            <!--<li><a href="#">Duplicate</a></li>-->
                                            
                                            @if ( in_array($files[$i]['extension'], ["xls", "xlsx", "doc", "docx", "ppt", "pptx"]))
                                            <!--<li><a href="https://view.officeapps.live.com/op/embed.aspx?src={{ secure_url($files[$i]['storagePath']) }}" target="_blank">View In MS Office Preview</a></li>-->
                                            @endif



                                            <li><a href="#" class="copyurl" hwct-fileName="{{ $files[$i]['storagePath'] }}" hwct-hash="{{ $files[$i]['hashString'] }}">Copy Download Link</a></li>
                                        </ul> 
                                    </div>   

                                </div>
                            </div>

                            @else


                            <div class="file-box">
                                <div class="file">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <a class="pop"><img alt="image" class="img-responsive" src="{{ $files[$i]['storagePath'] }}" onclick="showPicture(this)"></a>
                                        </div>
                                        <div class="file-name  over-scroll">
                                            {{ $files[$i]['name'] }}
                                            <br/>
                                            <small>{{ $files[$i]['dateTime'] }}</small>
                                            <br/>
                                            <small>{{ $files[$i]['fileSize'] }}</small>
                                        </div>
                                    </a>

                                            <!-- Split button -->
                                    <div class="btn-group dropup text-center">
                                        <button type="button" class="btn btn-primary btn-xs">Actions</button>
                                        <button type="button" class="btn btn-white btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li><a href="/attache/deletefile/{{ $files[$i]['homeDir'] }}/{{ $files[$i]['name'] }}" class="delete" onclick="return confirm('Are you sure you wish to delete this image? The image will be deleted and not recoverable!');">Delete</a></li>
                                            <li><a href="{{ $files[$i]['storagePath'] }}" target="_blank">View In New Window</a></li>
                                            <li><a href="#" class="copyemailpath" data-clipboard-text="{{ $files[$i]['storagePath'] }}">Copy Email Image Path</a></li>
                                            <li><a href="#" class="copyurl" hwct-fileName="{{ $files[$i]['storagePath'] }}" hwct-hash="{{ $files[$i]['hashString'] }}">Copy Download Link</a></li>

                                        </ul> 
                                    </div>   


                                </div>
                            </div>

                            @endif
                            @endfor

                            <div style="visibility: hidden"><input type="text" id="downloadLink" value=""></div>

                        </div>
                    </div>
                </div>
            </div>
</div>


<!-- Modals -->

<!-- New Folder -->

<div class="modal fade" id="newFolderModal" tabindex="-1" role="dialog" aria-labelledby="newGroupModal">
  <div class="modal-dialog modal-sm" role="document">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="newModalLabel"><i class="fa fa-folder m-r-sm" style="font-size: 20px;"></i>Create New Folder</h4>
        
        </div>
      
        <form name="newGroupForm" method="POST" action="/attache/new/folder">
        
            <div class="modal-body form-group">
                <small><label>Folder Name</label></small><br>

                <input type="text" class="form-control" name="newFolderName" placeholder="New Folder Name" value="" required>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Create New Folder</button>
      
            </div>

            {{ csrf_field() }}

        </form>
    </div>

  </div>
</div>

<!-- New Image -->

<div class="modal fade" id="newFileModal" tabindex="-1" role="dialog" aria-labelledby="newFileImage">
  <div class="modal-dialog modal-sm" role="document" style="width: 400px;">
    
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="imgModalLabel"><i class="fa fa-image m-r-sm" style="font-size: 20px;"></i>Upload New File</h4>
        
        </div>

        <div class="modal-body form-group" style="padding-bottom: 0px;">

            <form name="newFileForm" method="POST" action="/attache/new/file" enctype="multipart/form-data">
                <div class="input-group m-b col-xs-11 nodisplay">
                    <div class="fileinput fileinput-new input-group m-l-md" data-provides="fileinput">

                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select File</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="newFile" />
                        </span>
                        
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>

                        <div class="form-control" data-trigger="fileinput" style="border: none; font-size: 11px;">
                            <span class="fileinput-filename fileinput-exists"></span>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="dir" value="{{ $dir }}">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Upload New File</button>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>


<!-- Image -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <span class="text-center"> <img src="" id="imagepreview" style="max-width: 100%;" ></span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<div class="clearfix"></div>

<!-- Page Level Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.15/clipboard.min.js"></script>
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>

    function showPicture(param) {
        var pix = $(param).attr('src')
        var file = pix.split('\/').pop();
        $('#myModalLabel').html('<i class="fa fa-image fa-2x m-r-xs"></i>' + file);
        $('#imagepreview').attr('src', $(param).attr('src'));
        $('#imagemodal').modal('show');   
    };

    new Clipboard('.copyemailpath');

   var clipboard = new Clipboard('.copyurl', {
    text: function(trigger) {

        var urlhost = '{{ url('public/attache/download/') }}';
        var fn = trigger.getAttribute('hwct-fileName');
        var hs = trigger.getAttribute('hwct-hash');
        var fd = new FormData();    
        fd.append('filePath', fn);
        fd.append('hashString', hs);
        fd.append( 'uid', {{ $uid }} );
        fd.append('_token', '{{csrf_token()}}');
        $.ajax({
            url: "/attache/createlink",
            type: "POST",
            data: fd,
            cache: false,
            processData: false, 
            contentType: false, 
            done: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                console.log(mydata);
            },
            error: function(mydata) {console.log(mydata); alert("There was an error updating user image. Please try again."); return false;}
        });
        var copy = urlhost + "/" + hs;
        alert("The download link has been saved to your clipboard.");
       return copy;
    }
});       

</script>

<script>
    // show/hide instructions
    new Vue({
        el: '.view-instructions',
        data: {
            instructions: 'hide'
        },
        methods: {
                showInstructions: function() {this.instructions = 'show';},
                hideInstructions: function() {this.instructions = 'hide';}
                }
    }) // end Vue
</script>

<script>
    // show/hide instructions
    new Vue({
        el: '#newFolder',
        data: {
            homeDir: '{{ $homedir }}'
        },
    
    }) // end Vue
</script>

@endsection