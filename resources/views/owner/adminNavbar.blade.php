<!-- NAVBAR -->

    <nav class="navbar-default navbar-static-side" role="navigation" >
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold lite-color font15px">{{ Auth::user()->fname }} {{ Auth::user()->lname }}</strong>
                             </span>

                        <hr class="hr-small"/>

                        <span class="text-muted text-xs block">{{ $domain['name'] }}</span>
                            
                    </div>
                    <div class="logo-element">
                        KIT
                    </div>
                </li>

                    <li @if($breadcrumb[0] == 'dashboard')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/dashboard?id={{rand(100000,10000000)}}"><i class="fa fa-dashboard iconmenu"></i> <span class="nav-label">Dashboard</span></a>
                    </li>

                    @if ($domain->type < 8)


                    <li @if($breadcrumb[0] == 'orders')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/orderadmin/view"><i class="fa fa-cutlery iconmenu"></i> <span class="nav-label">Orders</span></a>
                    </li>

                    @endif

                    @if ( $domain->type == 9 )
                    <li @if($breadcrumb[0] == 'tables')class="active"@endif>
                        <a href="/tables/{{Request::get('urlPrefix')}}/table-manager"><i class="fa fa-qrcode iconmenu"></i> <span class="nav-label">Tables</span></a>
                    </li>
                    @endif

                    <li @if($breadcrumb[0] == 'menu')class="active"@endif>
                        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-file-text iconmenu"></i> <span class="nav-label">Menus</span></a>
                    </li>

                    @if ($domain->type < 8)


                    <li @if($breadcrumb[0] == 'contacts')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/contacts/view"><i class="fa fa-user iconmenu"></i> <span class="nav-label">Contacts</span> </a>
                    </li>

                    @endif

                    <li @if($breadcrumb[0] == 'attache')class="active"@endif>
                        <a href="/attache/view/home"><i class="fa fa-image iconmenu"></i> <span class="nav-label">Images</span></a>
                    </li>

                    @if ($domain->type == 3 || $domain->type ==6) 
                    <div style="display: none;">

                    <li @if($breadcrumb[0] == 'tables')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-square iconmenu"></i> <span class="nav-label">Tables</span></a>
                    </li>
                    </div>
                    @endif



                    <li @if($breadcrumb[0] == 'reports')class="active"@endif>
                        <a href="/reports/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-newspaper-o iconmenu"></i> <span class="nav-label">Reports</span></a>
                    </li>
                    
                    <li @if($breadcrumb[0] == 'systemuser')class="active"@endif>
                        <a href=""><i class="fa fa-user iconmenu"></i> <span class="nav-label">Users</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'view')class="active"@endif><a href="/owner/systemuser/view">View / Edit</a></li>
                            <li @if($breadcrumb[1] == 'new')class="active"@endif><a href="/owner/systemuser/new">Add New</a></li>
                        </ul>
                    </li>
                    
                    <li @if($breadcrumb[0] == 'station')class="active"@endif>
                        <a href=""><i class="fa fa-home iconmenu"></i> <span class="nav-label">Station</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'type')class="active"@endif><a href="/owner/station/type/list">Station Type</a></li>
                            <li @if($breadcrumb[1] == 'stations')class="active"@endif><a href="/owner/station/list">Stations</a></li>
                            <!-- <li @if($breadcrumb[1] == 'locations')class="active"@endif><a href="/owner/station/locations">Station Location</a></li> -->
                            <li @if($breadcrumb[1] == 'menu')class="active"@endif><a href="/owner/station/menu">Station Menu</a></li>
                        </ul>
                    </li>
                    
                    <li @if($breadcrumb[0] == 'settings')class="active"@endif>
                        <a href="/settings/owner/dashboard"><i class="fa fa-gear iconmenu"></i> <span class="nav-label">Settings</span> </a>
                    </li>

            </ul>

        </div>
    </nav>