<!-- TopNav -->

       <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary adminRight " href="#"><i class="fa fa-bars"></i> </a>
                        <img class="img img-responsive topAdmin" src="/img/tgm320.png" alt="getTableSide.biz Administration" />
                </div>
                <ul class="nav navbar-top-links navbar-right hidden-xs">
                    <li>

                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>  Logout</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </li>
                </ul>

            </nav>
        </div>

<!-- End TopNav -->
