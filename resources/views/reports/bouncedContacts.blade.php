@extends('admin.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h2 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h2>
            <small>
                Bounced Contact Reporting
            </small>
        </div>
    </div>
</div>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down m-r-xs"></i>Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up m-r-xs"></i>Hide Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-8 m-b-md instructions m-l-sm">
    <li>Bounced contacts are listed below.</li>
    <li>Click "Contact" to see all details for the bounced Contact.</li>
    <li>Click "Message" to see the raw bounced message.</li>
    <li>Click "Restore" to restore the contact as an active contact.</li>
    <li>Click "Delete" to delete the bounced contact.</li>

</ul>


<!-- page level css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>



<div class="row">

    <div class="col-xs-12 m-t-sm">
        
        <div class="ibox">
            <div class="ibox-title">

                <h3>Bounced Contact List</h3>

            </div>
            
            <div class="ibox-content">

                <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
                <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
                    <thead>
                        <tr>
                            <th width="80">Contact ID</th>
                            <th width="80">Message ID</th>
                            <th width="80">Name</th>
                            <th width="80">Email Address</th>
                            <th width="80">Bounce Time</th>
                            <th width="150">Detailed Information</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($bounceData as $bounce)
                        <tr>                
                            <td>{{ $bounce->contactID }}</td>
                            <td>{{ $bounce->messageID }}</td>
                            <td>{{ $bounce->fname }} {{ $bounce->lname }}</td>
                            <td>{{ $bounce->email }}</td>
                            <td>{{ $bounce->created_at }}</td>

                            <td>
                                <button class="btn btn-success btn-xs w110 m-r-xs" onclick="rawMessage({{ $bounce->id }})"><i class="fa fa-file-text m-r-xs"></i>Raw Message</button>
                                <button class="btn btn-success btn-xs w110 m-r-xs" onclick="viewContactDetail({{ $bounce->id }})"><i class="fa fa-id-card m-r-xs"></i>Contact Data</button>
                                <button class="btn btn-warning btn-xs w110 m-r-xs" onclick="bounceContactRestore({{$bounce->id}})"><i class="fa fa-rotate-left m-r-xs"></i>Restore</button>

                                <button class="btn btn-danger btn-xs w110 m-r-xs" onclick="bounceContactDelete({{$bounce->id}})"><i class="fa fa-times-rectangle m-r-xs"></i>Delete</button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>                
                </table>
            </div>








            </div>
        </div>
 

    </div>
</div>




<!-- Modals -->

<div class="modal inmodal" id="detailModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-id-card modal-icon"></i>
                <h4 class="modal-title">Bounce Contact Detail</h4>
            </div>
            <div class="modal-body">
                

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>

                            <tr>
                                <td width="30%">First Name:</td>
                                <td id="fname"></td>
                            </tr>
                            <tr>
                                <td width="30%">Last Name:</td>
                                <td id="lname"></td>
                            </tr>
                            <tr>
                                <td width="30%">Email:</td>
                                <td id="email"></td>
                            </tr>
                            <tr>
                                <td width="30%">Company:</td>
                                <td id="company"></td>
                            </tr>
                                <td width="30%">Title:</td>
                                <td id="title"></td>
                            </tr>
                                <td width="30%">Address1:</td>
                                <td id="address1"></td>
                            </tr>
                                <td width="30%">Address2:</td>
                                <td id="address2"></td>
                            </tr>
                            </tr>
                                <td width="30%">City:</td>
                                <td id="city"></td>
                            </tr>
                            </tr>
                                <td width="30%">State:</td>
                                <td id="state"></td>
                            </tr>
                            </tr>
                                <td width="30%">Zip:</td>
                                <td id="zip"></td>
                            </tr>

                           <tr>
                                <td width="30%">Office:</td>
                                <td id="office"></td>
                            </tr>
                            <tr>
                                <td width="30%">Mobile:</td>
                                <td id="mobile"></td>
                            </tr>

                        </tbody>
                    </table>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>



<!-- Raw Message Modal -->
<div class="modal inmodal" id="rawMessage" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Bounce Message Detail</h4>
                <small>Raw Bounced Message</small>
            </div>
            <div class="modal-body">      
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>

                            <tr>
                                <td width="30%">Message ID:</td>
                                <td id="rawMessageID"></td>
                            </tr>
                            
                           <tr>
                                <td width="30%">Raw Bounce Message:</td>
                                <td id="rawBody"></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>






<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


$(document).ready(function(){
    $('.dataTables').DataTable({
        pageLength: 25,
        dom: '<"html5buttons"B>lTfgitp',
        order: [[ 1, "desc" ]],
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'EmailMessageReporting'},
            {extend: 'pdf', title: 'EmailMessageReporting'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]

    });

});


function rawMessage(bid) {
    
    var fd = new FormData();    
    fd.append('bounceID', bid);

    $.ajax({
        url: "/api/v1/getbouncerawmessage",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {

                alert(jsondata["message"]);
                return false;
                    
            } else {
                $('#rawMessageID').text(jsondata["detail"]["messageID"]);
                $('#rawBody').html(jsondata["detail"]["rawMessage"]);
                $('#rawMessage').modal('show');
                return true;

        }}
    });
}



function bounceContactRestore(bid) {
    if (!confirm("Are you sure you wish to restore the bounced contact? All data will be returned to your contacts.")) {return false;}
    var fd = new FormData();    
    fd.append('bid', bid);

    $.ajax({
        url: "/api/v1/bouncecontactrestore",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {
            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                alert(jsondata["message"]);
                return false;     
            } else {
                alert(jsondata["message"]);
                location.reload();
                return true;
        }}
    });
}

function bounceContactDelete(bid) {
    if (!confirm("Are you sure you wish to delete the bounced contact? All data will be deleted permanetely.")) {return false;}
    var fd = new FormData();    
    fd.append('bid', bid);

    $.ajax({
        url: "/api/v1/bouncecontactdelete",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {
            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                alert(jsondata["message"]);
                return false;     
            } else {
                alert(jsondata["message"]);
                location.reload();
                return true;
        }}
    });
}




function viewContactDetail(bid) {
    
    var fd = new FormData();    
    fd.append('bid', bid);

    $.ajax({
        url: "/api/v1/getbouncecontactdetail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {

                alert(jsondata["message"]);
                return false;
                    
            } else {

                $('#fname').text(jsondata["detail"]["fname"]);
                $('#lname').text(jsondata["detail"]["lname"]);
                $('#email').text(jsondata["detail"]["email"]);                
                $('#company').text(jsondata["detail"]["company"]);
                $('#title').text(jsondata["detail"]["title"]);
                $('#address1').text(jsondata["detail"]["address1"]);
                $('#address2').text(jsondata["detail"]["address2"]);
                $('#city').text(jsondata["detail"]["city"]);
                $('#state').text(jsondata["detail"]["state"]);
                $('#zip').text(jsondata["detail"]["zip"]);
                $('#office').text(jsondata["detail"]["office"]);
                $('#mobile').html(jsondata["detail"]["mobile"]);
                $('#detailModal').modal('show');
                return true;


        }}
    });
}



</script>


@endsection