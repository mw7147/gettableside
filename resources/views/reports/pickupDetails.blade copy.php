<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<style>
    .orderInProcess {
        background-color: #dfffdf;
    }

    .orderRefunded {
        background-color: #dff2ff;
    }
    
    .futureOrder {
        background-color: #f9ffa8;
    }
    

    #specificRangeHolder {
        margin-bottom: 36px;
    }

</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Pickup Order Details</h2>

	<h4 class="m-b-md">
		Customer pickup order details are shown.
	</h4>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
        <li>If selected, a quick date range selection takes precedent over a specific date range.</li>
        <li>Make sure a quick date range is not selected if you select a specific date range.</li>
        <li>To select a specific date range, click on the beginning date then click on the end date and click apply.</li>
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
        <li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>Order List</h3><hr>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/view" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-refresh m-r-xs"></i>Refresh Page</a>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Reports</a>
    
    <div class="clearfix"></div>


        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif

        <form name="ordersDateRange" method="POST" action = "/reports/{{Request::get('urlPrefix')}}/delivery-details" style="border: 1px solid #555; width: 300px; margin-bottom: 18px; padding: 8px 15px;">
        <div>
            <label class="font-normal font-italic">Quick Date Range:</label>
            <select class="form-control" name="dateRange" id="dateRange" style="min-width: 250px;">
                <option value="allFuture" @if ($dateRange == "allFuture")selected @endif>All Future Orders</option>
                <option value="tomorrow" @if ($dateRange == "tomorrow")selected @endif>Tomorrow</option>
                <option value="" @if ($dateRange == "")selected @endif>Select Quick Range</option>
                <option value="today" @if ($dateRange == "today")selected @endif>Today</option>
                <option value="yesterday" @if ($dateRange == "yesterday")selected @endif>Yesterday</option>
                <option value="week" @if ($dateRange == "week")selected @endif>This Week</option>
                <option value="lastWeek" @if ($dateRange == "lastWeek")selected @endif>Last Week</option>
                <option value="month" @if ($dateRange == "month")selected @endif>This Month</option>
                <option value="lastMonth" @if ($dateRange == "lastMonth")selected @endif>Last Month</option>
            </select>
        </div>

        <div class="clearfix"></div>
        <h4 class="m-l-sm m-t-sm"><i>Or</i></h4>

        <div class="clearfix"></div>

        <div id="specificRangeHolder">
            <label class="font-normal font-italic">Specific Date Range:</label>
            <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" name="specificRange" id="specificRange" style="min-width: 267px;" value="{{ $specificRange }}"/>
            </div>
        </div>

        <button type="submit" class="btn btn-info btn-sm" style="margin-top: -36px; min-width: 267px;">Set Range</button>
        
        {{ csrf_field() }}
        </form>
        <div class="clearfix"></div>


    <div class="table-responsive">
        <table class="table table-bordered table-hover nowrap dataTables" >
            <thead>
                <tr>
                    <th width="40">Order ID</th>
                    <th width="60">Order Time</th>
                    <th width="60">Pickup Time</th>
                    <th width="40">Contact ID</th>
                    <th width="60">First Name</th>
                    <th width="60">Last Name</th>
                    <th width="120">Address</th>
                    <th width="80">City</th>
                    <th width="40">State</th>
                    <th width="40">Zip Code</th>
                    <th width="60">Mobile</th>
                    <th width="80">Email</th>
                    <th width="40">CC Last 4</th>
                    <th width="70">Total</th>
                    <th width="70">Refunds</th>
                    <th width="80">Receipt</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($items as $item)
                <tr> 
                    <td>{{ $item->id }}</td>
                    <td>{{ Carbon\Carbon::parse($item->created_at)->format('D n/j g:i A') }}</td>
                    <td>{{ Carbon\Carbon::parse($item->orderReadyTime)->format('D n/j g:i A') }}</td>
                    <td>{{ $item->contactID }}</td>
                    <td>{{ $item->fname }}</td>
                    <td>{{ $item->lname }}</td>
                    <td>{{ trim($item->contact->address1 . " " . $item->contact->address2 ?? '') }}</td>
                    <td>{{ $item->contact->city }}</td>
                    <td>{{ $item->contact->state }}</td>
                    <td>{{ $item->contact->zip }}</td>
                    <td>{{ $item->customerMobile }}</td>
                    <td>{{ $item->customerEmailAddress }}</td>
                    <td>{{ $item->ccTypeLast4 }}</td>
                    <td>${{ number_format($item->total, 2) }}</td>
                    <td>@if ($item->orderRefundAmount > 0) ${{ number_format($item->orderRefundAmount, 2) }} @else $0.00 @endif </td>
                    <td>
                        <a class="btn btn-warning btn-xs m-r-xs" href="/reports/{{Request::get('urlPrefix')}}/receipt/{{ $item->id }}" target="popup" onclick="window.open('/reports/{{Request::get('urlPrefix')}}/receipt/{{ $item->id }}','popup', 'width=800,height=700,top=15,left=30'); return false;" ><i class="fa fa-binoculars m-r-xs"></i>View</a>
                    </td>


                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>
    <!-- Date range picker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /><script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 1, "asc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

        $('#specificRange').daterangepicker();

        $('#specificRange').on('change',function(){
            $('#dateRange').val('');
        });


    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

</script>

 

 <script>



 </script>

@endsection