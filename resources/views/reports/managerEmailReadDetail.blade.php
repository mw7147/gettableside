@extends('admin.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h2 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h2>
            <small>
                Email Message Reporting
            </small>
        </div>
    </div>
</div>


<!-- page level css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>



<div class="row">

    <div class="col-xs-12 m-t-sm">
        
        <div class="ibox">
            <div class="ibox-title">

                <h3>Email Message Read Detail For Message ID: {{ $mid }}</h3>

            </div>
            
            <div class="ibox-content">

                <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
                <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
                    <thead>
                        <tr>
                            <th width="60">First Name</th>
                            <th width="60">Last Name</th>
                            <th width="60">Email Address</th>
                            <th width="60">Send Time</th>
                            <th width="60">Read Time</th>
                            <th width="80">User Agent</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($reads as $read)
                        <tr>                
                            <td>{{ $read->fname }}</td>
                            <td>{{ $read->lname }}</td>
                            <td>{{ $read->emailAddress }}</td>
                            <td>{{ $read->sendTime }}</td>                                
                            <td>{{ $read->readTime }}</td> 
                            <td>{{ $read->userAgent }}</td>                             
                        </tr>
                    @endforeach

                    </tbody>                
                </table>
            </div>



            </div>
        </div>
 

    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


$(document).ready(function(){
    $('.dataTables').DataTable({
        pageLength: 25,
        dom: '<"html5buttons"B>lTfgitp',
        order: [[ 4, "desc" ]],
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'TextMessageReporting'},
            {extend: 'pdf', title: 'TextMessageReporting'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]

    });

});


</script>


@endsection