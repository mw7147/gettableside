@extends('reports.customerReportsMain')



@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h2 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h2>
            <small>
                Text Message Reporting
            </small>
        </div>
    </div>
</div>


<!-- page level css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>



<div class="row">

    <div class="col-xs-12 m-t-sm">
        
        <div class="ibox">
            <div class="ibox-title">

                <h3>Text Message Detail For Mobile Number: {{ $mobile }}</h3>

            </div>

            <div class="ibox-content">

                <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
                <a href="/customers/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
                    <thead>
                        <tr>
                            <th width="80">Message ID</th>
                            <th width="80">Group Name</th>
                            <th width="80">Template Name</th>
                            <th width="80">Time Sent</th>

                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($mobileDetails as $mobileDetail)
                        <tr>                
                            <td>{{ $mobileDetail->messageID }}</td>
                            <td>@if (is_null($mobileDetail->groupName)) All @else {{ $mobileDetail->groupName }} @endif </td>
                            <td>{{ $mobileDetail->name }}</td>
                            <td>{{ $mobileDetail->created_at }}</td>                                
                        </tr>
                    @endforeach

                    </tbody>                
                </table>
            </div>








            </div>
        </div>
 

    </div>
</div>




<!-- Modals -->

<div class="modal inmodal" id="mobileDetailModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-commenting modal-icon"></i>
                    <h4 class="modal-title">Message Detail</h4>
                    <small class="font-bold">Sending mobileDetails for Message ID: <span id="mid"></span> </small>
                </div>
                <div class="modal-body">
                    

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>

                                <tr>
                                    <td width="30%">Message ID:</td>
                                    <td id="messageID"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Group Name:</td>
                                    <td id="groupName"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Message Name:</td>
                                    <td id="messageName"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Total Queued:</td>
                                    <td id="queued"></td>
                                </tr>
                                    <td width="30%">Bad Numbers:</td>
                                    <td id="bad"></td>
                                </tr>
                                    <td width="30%">Total Sent:</td>
                                    <td id="sent"></td>
                                </tr>
                                    <td width="30%">Total Failed:</td>
                                    <td id="failed"></td>
                                </tr>
                                </tr>
                                    <td width="30%">Start Time:</td>
                                    <td id="start"></td>
                                </tr>
                                </tr>
                                    <td width="30%">Finish Time:</td>
                                    <td id="stop"></td>
                                </tr>
                               <tr>
                                    <td width="30%">Message Body:</td>
                                    <td id="body"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Picture:</td>
                                    <td><a href="" id="pixlink" target="_blank"><img src="" id="picture"></a></td>
                                </tr>
                                <tr>
                                    <td width="30%">Sender IP:</td>
                                    <td id="ip"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>










<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>


$(document).ready(function(){
    $('.dataTables').DataTable({
        pageLength: 25,
        dom: '<"html5buttons"B>lTfgitp',
        order: [[ 3, "desc" ]],
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'TextMessageReporting'},
            {extend: 'pdf', title: 'TextMessageReporting'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]

    });

});



</script>


@endsection