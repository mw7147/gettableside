<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<style>
    .orderInProcess {
        background-color: #dfffdf;
    }

    .orderRefunded {
        background-color: #dff2ff;
    }

</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Sales By Menu Item Report</h2>

	<h4>
		Sales by menu item is shown below.
	</h4>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>Sales By Menu Item For {{ ucwords($dates['reportDates']) }} ({{ ucwords($dates['human']) }})</h3><hr>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-file-text m-r-xs"></i>Reports</a>

    
    <div class="clearfix"></div>


        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif

        <form name="ordersDateRange" method="POST" action = "/reports/{{Request::get('urlPrefix')}}/menuitemsales">
           
            <div class="col-md-3 col-sm-5 col-xs-11 m-b-lg" style="margin-left: -15px; margin-top: -18px;">
                <label class="font-normal font-italic">Date Range:</label>
                <select class="form-control" name="dateRange" id="dateRange">
                    <option value="allFuture" @if ($dates['range'] == 'allFuture') selected @endif>All Future Orders</option>
                    <option value="tomorrow" @if ($dates['range'] == 'tomorrow') selected @endif>Tomorrow</option>
                    <option value="today" @if ($dates['range'] == 'today') selected @endif>Today</option>
                    <option value="yesterday" @if ($dates['range'] == 'yesterday') selected @endif>Yesterday</option>
                    <option value="thisWeek" @if ($dates['range'] == 'thisWeek') selected @endif>This Week</option>
                    <option value="lastWeek" @if ($dates['range'] == 'lastWeek') selected @endif>Last Week</option>
                    <option value="thisMonth" @if ($dates['range'] == 'thisMonth') selected @endif>This Month</option>
                    <option value="lastMonth" @if ($dates['range'] == 'lastMonth') selected @endif>Last Month</option>
                    <option value="last90Days" @if ($dates['range'] == 'last90Days') selected @endif>Last 90 Days</option>
                    <option value="thisYear" @if ($dates['range'] == 'thisYear') selected @endif>This Year</option>
                </select>
            </div>
            
            <div class="col-md-3 col-sm-5 col-xs-11 m-b-lg" style="margin-left: -15px; margin-top: -18px;">
                <label class="font-normal font-italic">Menu Category:</label>
                <select class="form-control" name="category" id="category">
                    <option value="%" @if ($category == '%') selected @endif>All Categories</option>
                    @foreach ($categories as $cat)
                    <option value="{{ $cat->id }}" @if ($category == $cat->id) selected @endif>{{ $cat->categoryName }}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-sm-5 col-xs-11 m-b-lg" style="margin-left: -15px;">
                <button type="submit" class="btn btn-primary btn-sm" style="margin-top: -30px;"><i class="fa fa-binoculars m-r-xs"></i>View Report</button>
            </div>

            {{ csrf_field() }}

        </form>

        <div class="clearfix"></div>

        <div class="col-lg-4 col-md-6 col-sm-9 col-xs-11 m-b-lg" style="margin-left: -15px;">
            <h3>Report Totals</h3>
            <table class="table table-bordered" >
                <tr>
                    <td style="font-size: 14px; margin-bottom: 6px;">Total Items Purchased</td>
                    <td style="font-size: 14px; margin-bottom: 6px;">{{ $totalItemsPurchased }}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px; margin-bottom: 6px;">Unique Customers</td>
                    <td style="font-size: 14px; margin-bottom: 6px;">{{ $totalCustomers }}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px; margin-bottom: 6px;">Report Total Revenue</td>
                    <td style="font-size: 14px; margin-bottom: 6px;">${{ number_format($totalRevenue, 2) }}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px; margin-bottom: 6px;">Report Total Discounts</td>
                    <td style="font-size: 14px; margin-bottom: 6px;">${{ number_format($totalDiscounts, 2) }}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px; margin-bottom: 6px;">Report Net Revenue</td>
                    <td style="font-size: 14px; margin-bottom: 6px;">${{ number_format($totalRevenue - $totalDiscounts, 2) }}</td>
                </tr>

            </table>
        </div>

        <div class="clearfix"></div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    {{--@if (Request::get('authLevel') >= 35)<th width="50">Location</th>@endif--}}
                    <th width="80">Menu Item</th>
                    <th width="80">Category</th>
                    <th width="80">Number of Customers</th>
                    <th width="80">Quantity Purchased</th>
                    <th width="80">Gross Revenue</th>
                    <th width="80">Discounts</th>
                    <th width="80">Net Revenue</th>
                    <th width="80">Last Order Date</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($items as $item)
                <tr>
                    {{--@if (Request::get('authLevel') >= 35)<td>{{ $item->name }}</td>@endif--}}
                    <td>{{ $item->MenuItem ?? '' }}</td>
                    <td>{{ $item->category->categoryName ?? '' }}</td>
                    <td>{{ $item->numCustomers ?? '' }}</td>
                    <td>{{ $item->Quantity ?? ''}}</td>
                    <td>${{ number_format($item->Revenue, 2) ?? '0.00' }}</td>
                    <td>${{ number_format($item->Discounts, 2) ?? '0.00' }}</td>
                    <td>${{ number_format($item->Revenue - $item->Discounts, 2) ?? '0.00' }}</td>
                    <td>{{ $item->LastDate ?? '' }}</td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[0, "asc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'MenuItemSalesReport'},
                {extend: 'pdf', title: 'MenuItemSalesReport'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

</script>

 

 <script>

function ePrint(orderID) {
    var fd = new FormData();
    fd.append('orderID', orderID),    

    $.ajax({
        url: "/api/v1/resendeprint",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            alert(mydata);
            return true;
        },
        error: function() {
            alert("There was an problem with your ePrint request. Please try again.");
        }
    });
};

function orderReady(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order ready text to customer and mark order ready.');
    if (!st) {
        var mr = confirm("Would you like to mark the order as ready? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderready",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function refundOrder(orderID) {

    var ro = confirm('Are you sure you wish to refund the order? This action is not reversable.');
    var pin = prompt("Please enter Manager PIN");

    if (!ro) {
        // do nothing
        return false; 
    }

    // send text and mark as read
    var fd = new FormData();
    fd.append('managerPIN', pin),
    fd.append('uid', '{{Auth::id()}}'),
    fd.append('orderID', orderID),
    $.ajax({
        url: "/api/v1/admin/refundorder",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            alert(mydata['message']);
            location.reload();
        },
        error: function() {
            alert("There was a problem refunding the order. Please try again.");
        }
    });
};

 </script>

@endsection