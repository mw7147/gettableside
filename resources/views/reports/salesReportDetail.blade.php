<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<style>

    .orderRefunded {
        background-color: #fdeaea;
    }

    .bb {
        border-bottom: 1px solid #aaa;
    }

</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Daily Sales Detail Report</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down hidden-print"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h4><i>Date: {{$orderDate}}</i></h4>

<table class="table-hover">
    <tr>   
        <td class="bb"><h5>Credit Card Total:</h5></td>
        <td class="bb" align="right"><h5>${{number_format($creditCardTotal - $refundTotal, 2)}}</h5></td>
    </tr>
    <tr>
        <td width="100" style="padding-top: 12px;"><h5>Total Orders:</h5></td>
        <td width="65" align="right" style="padding-top: 12px;"><h5>${{number_format($orderTotal, 2)}}</h5></td>
    </tr>
    <tr>   
        <td><h5>Total Discounts:</h5></td>
        <td align="right"><h5>(${{ number_format($orderDiscountAmount, 2) }})</h5></td>
    </tr>
    <tr>
        <td width="100"><h5>Total Tips:</h5></td>
        <td width="65" align="right"><h5>${{ number_format($orderTips, 2) }}</h5></td>
    </tr>
    <tr>
        <td width="100"><h5>Total Tax:</h5></td>
        <td width="65" align="right"><h5>${{ number_format($orderTax, 2) }}</h5></td>
    </tr>
    <tr>
        <td width="100"><h5>Total Delivery Charges:</h5></td>
        <td width="65" align="right"><h5>${{ number_format($orderDeliveryCharge, 2) }}</h5></td>
    </tr>
    <tr>   
        <td><h5>Total Refunds:</h5></td>
        <td class="bb" align="right"><h5>@if ($refundTotal == 0) ($.00) @else {{ '($' . number_format($refundTotal, 2) . ')' }} @endif </h5></td>
    </tr>
    <tr>
        <td width="110" style="padding-top: 4px;"><h4>Total Sales:</h4></td>
        <td style="padding-top: 4px;" align="right"><h4>${{number_format($orderTotal - $orderDiscountAmount + $orderTips + $orderTax + $orderDeliveryCharge - $refundTotal, 2)}}</h4></td>
    </tr>

</table>
<hr>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-file-text m-r-xs"></i>Reports</a>
    <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>


    <div class="clearfix"></div>


        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif


        <div class="clearfix"></div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th width="50">Date</th>
                    <th width="70">Name</th>
                    <th width="50">Total</th>
                    <th width="50">Discount</th>
                    <th width="50">Tip</th>
                    <th width="50">Tax</th>
                    <th width="50">Delivery</th>
                    <th width="50">Refund</th>
                    <th width="50">Net</th>


                    <th width="120">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($details as $detail)
                <tr @if ($detail->orderRefunded)class="orderRefunded" @endif >
                    <td>{{ $detail->id }}</td>
                    <td>{{ $detail->orderDate }}</td>
                    <td>{{ $detail->fname }} {{ $detail->lname }}</td>
                    <td>${{ number_format($detail->subTotal, 2) }}</td>
                    <td>@if ( $detail->discountAmount == 0) $.00 @else {{ '($' . number_format($detail->discountAmount, 2) . ')' }} @endif </td>
                    <td>@if ( $detail->tip == 0) $.00 @else ${{ number_format($detail->tip, 2) }} @endif </td>
                    <td>@if ( $detail->tax == 0) $.00 @else ${{ number_format($detail->tax, 2) }} @endif </td>
                    <td>@if ( $detail->deliveryCharge == 0) $.00 @else ${{ number_format($detail->deliveryCharge, 2) }} @endif </td>
                    <td>@if ( $detail->orderRefundAmount == 0) $.00 @else {{ '($' . number_format($detail->orderRefundAmount, 2) . ')' }} @endif </td>
                    <td>${{ number_format($detail->total - $detail->orderRefundAmount, 2) }} </td>

                    <td class="white-bg">
                    {{--<a href="/reports/{{Request::get('urlPrefix')}}/orderdetail/{{$detail->id}}" class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-binoculars m-r-xs"></i>Details</a>--}}
                    <a class="btn btn-warning btn-xs" href="/reports/{{Request::get('urlPrefix')}}/orderdetail/{{$detail->id}}" target="popup" onclick="window.open('/reports/{{Request::get('urlPrefix')}}/orderdetail/{{$detail->id}}','popup', 'width=800,height=700,top=15,left=30'); return false;" ><i class="fa fa-binoculars m-r-xs"></i>Details</a>

                    </td>
                </tr>
            @endforeach
            

            
            </tbody>                
        </table>


    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [ 0, "desc" ],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('#dateRange').on('change',function(){
       this.form.submit();
    });

</script>

 

 <script>

function ePrint(orderID) {
    var fd = new FormData();
    fd.append('orderID', orderID),    

    $.ajax({
        url: "/api/v1/resendeprint",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            alert(mydata);
            return true;
        },
        error: function() {
            alert("There was an problem with your ePrint request. Please try again.");
        }
    });
};

function orderReady(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order ready text to customer and mark order ready.');
    if (!st) {
        var mr = confirm("Would you like to mark the order as ready? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderready",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function refundOrder(orderID) {

    var ro = confirm('Are you sure you wish to refund the order? This action is not reversable.');
    var pin = prompt("Please enter Manager PIN");

    if (!ro) {
        // do nothing
        return false; 
    }

    // send text and mark as read
    var fd = new FormData();
    fd.append('managerPIN', pin),
    fd.append('uid', '{{Auth::id()}}'),
    fd.append('orderID', orderID),
    $.ajax({
        url: "/api/v1/admin/refundorder",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            alert(mydata['message']);
            location.reload();
        },
        error: function() {
            alert("There was a problem refunding the order. Please try again.");
        }
    });
};

 </script>

@endsection