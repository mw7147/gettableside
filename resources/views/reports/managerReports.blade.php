@extends('admin.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h1 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h1>
            <small>
                Sales Reporting
            </small>
        </div>
    </div>
</div>


<!-- page level css -->

<style>
    .dashicon { height: 56px; }
    .dashicon:hover { opacity: .75;}
    .dashicon-white:hover { opacity: .62;}
    .widget { padding: 8px 15px;}
    .widget.style1 h2 { font-size: 17px; margin-top: 2px; font-weight: 300;}
    h2 {font-size: 20px;}
    .fa-report {font-size: 22px;}
    .ibox-content {min-height: 190px;}
    .fa-1_5x {font-size: 1.5em;}
    @media (max-width: 991px) {.ibox-content { min-height: 75px;} .widget.style1 h2 { font-size: 14px; margin-top: 3px; font-weight: 300;} h1 {font-size: 23px;} h2 {font-size: 17px;} .fa-report {font-size: 18px;}}

</style>


<div class="row">

       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Sales Reports</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/dailysalessummary">
                        <div class="widget style1 navy-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-1_5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Sales Report</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/menuitemsales">
                        <div class="widget style1 navy-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-bar-chart fa-1_5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Sales By Menu Item Report</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">
                <h2>Sales Aggregates</h2>
            </div>
            
            <div class="ibox-content">
               
                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/item-aggregate">
                        <div class="widget style1 tgm-orange-bg">
                            <div class="row vertical-align" style="color: white">
                                <div class="col-xs-3">
                                    <i class="fa fa-list"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Item Aggregate</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/category-aggregate">
                        <div class="widget style1 tgm-orange-bg">
                            <div class="row vertical-align" style="color: white">
                                <div class="col-xs-3">
                                    <i class="fa fa-list"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Category Aggregate</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>                

                <div class="clearfix"></div>

            </div>
        </div>
    </div>



    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">
                <h2>Customer Order Detail</h2>
            </div>
            
            <div class="ibox-content">
               
                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/delivery-details">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align" style="color: white">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Delivery Order Details</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/pickup-details">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align" style="color: white">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Pickup Order Details</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>                

                <div class="clearfix"></div>

            </div>
        </div>
    </div>



    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Suspension Report</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/reports/{{Request::get('urlPrefix')}}/suspension">
                        <div class="widget style1 red-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-pause"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Suspension Reporting</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>




</div>


<script>
function comingSoon(e) {
   
    alert("Under Development - Coming Soon");
    e.preventDefault();
    return false;

}
</script>
@endsection