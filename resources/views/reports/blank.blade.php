@extends('reports.customerReportsMain')



@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h2 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h2>
            <small>
                Text Message Reporting
            </small>
        </div>
    </div>
</div>


<!-- page level css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>



<div class="row">

    <div class="col-xs-12 m-t-md">
        
        <div class="ibox">
            <div class="ibox-title">

                <h3>Text Message Reporting by Sent Text Messages</h3>

            </div>
            
            <div class="ibox-content">
            
            <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
            <a href="/customers/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
            

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
                    <thead>
                        <tr>
                            <th width="120">Template Name</th>
                            <th width="80">Message ID</th>
                            <th width="80">Template ID</th>
                            <th width="80">Total Queued</th>
                            <th width="80">Total Sent</th>
                            <th width="80">Total Failed</th>

                            <th width="90">Send Start</th>
                            <th width="90">Send Stop</th>


                            <th width="150">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>                
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>



                            <td><a class="btn btn-success btn-xs w90 m-r-xs" href="/customers/contacts"><i class="fa fa-pencil"></i> View/Edit</a>
                                <a class="btn btn-success btn-xs w90 m-r-xs" href="/customers/contacts"><i class="fa fa-users"></i> Groups</a>
                                <a class="btn btn-success btn-xs w90 m-r-xs" href="/customers/contacts"><i class="fa fa-folder-open"></i> History</a>
                                
                            </td>
                        </tr>
                    
                    </tbody>                
                </table>
            </div>








            </div>
        </div>
 

    </div>
</div>


<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 1, "asc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'TextMessageReporting'},
                {extend: 'pdf', title: 'TextMessageReporting'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

</script>


@endsection