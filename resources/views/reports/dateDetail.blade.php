<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<style>
    .orderInProcess {
        background-color: #dfffdf;
    }

    .orderRefunded {
        background-color: #dff2ff;
    }

</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Order Date Detail</h2>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>Order List For </h3><hr>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/view" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-refresh m-r-xs"></i>Refresh Page</a>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

    @if (Request::get('authLevel') < 35)
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview" class="btn btn-warning btn-xs m-b-lg  w110"><i class="fa fa-bullseye m-r-xs"></i>Orders Only</a>
    @endif
    
    <div class="clearfix"></div>


        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif

        <div class="col-md-3 col-sm-6 col-xs-9 m-b-lg" style="margin-left: -15px;">
        <form name="ordersDateRange" method="post" action = "/{{Request::get('urlPrefix')}}/orderadmin/view">
            <label class="font-normal font-italic">Date Range:</label>
                <select class="form-control" name="dateRange" id="dateRange">
                    <option value="today" @if ($dateRange == "today")selected @endif>Today</option>
                    <option value="week" @if ($dateRange == "week")selected @endif>This Week</option>
                    <option value="month" @if ($dateRange == "month")selected @endif>This Month</option>
                    <option value="all" @if ($dateRange == "all")selected @endif>All</option>
                </select>
            </div>
            {{ csrf_field() }}
        </form>
        <div class="clearfix"></div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover dt-responsive nowrap dataTables" >
            <thead>
                <tr>
                    @if (Request::get('authLevel') >= 35)<th width="50">Location</th>@endif
                    <th width="50">Order ID</th>
                    <th width="80">Order Time</th>
                    <th width="80">Name</th>
                    <th width="80">Mobile</th>
                    <th width="50">Amount</th>
                    <th width="120">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($orders as $order)
                <tr @if ($order->orderInProcess)class="orderInProcess" @endif
                    @if ($order->orderRefunded)class="orderRefunded" @endif>
                    @if (Request::get('authLevel') >= 35)<td>{{ $order->name }}</td>@endif
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->fname }} {{ $order->lname }}</td>
                    <td>{{ $order->customerMobile }}</td>
                    <td>${{ $order->total }}</td>
                    <td class="white-bg">
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="showDetail('{{ $order->id }}')"><i class="fa fa-binoculars m-r-xs"></i>Details</button>
                        
                        <a class="btn btn-success btn-xs w90 m-r-xs m-b-xs" href="/{{Request::get('urlPrefix')}}/vieworder/{{ $order->id }}"><i class="fa fa-binoculars m-r-xs"></i>Order</a>
                        
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="ePrint('{{$order->id}}');"><i class="fa fa-print m-r-xs"></i>ePrint</button>
                        
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="sendText('{{$order->contactID}}', '{{$order->fname}}', '{{$order->lname}}');"><i class="fa fa-commenting m-r-xs"></i>Send Text</button>
                        
                        @if ($order->orderInProcess)
                        <button type="button" class="btn btn-warning btn-xs w120 m-r-xs m-b-xs" onclick="orderReady('{{$order->id}}')"><i class="fa fa-check-circle m-r-xs"></i>Order Ready</button>
                        @endif
                        
                        @if (!$order->orderRefunded)
                        <button class="btn btn-danger btn-xs w120 m-r-xs m-b-xs" onclick="refundOrder('{{$order->id}}')"><i class="fa fa-window-close m-r-xs"></i>Refund Order</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

@include('orderAdmin.orderDetailModal')
@include('orderAdmin.sendTextModal')

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 1, "desc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('#dateRange').on('change',function(){
       this.form.submit();
    });

</script>

 

 <script>

function ePrint(orderID) {
    var fd = new FormData();
    fd.append('orderID', orderID),    

    $.ajax({
        url: "/api/v1/resendeprint",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            alert(mydata);
            return true;
        },
        error: function() {
            alert("There was an problem with your ePrint request. Please try again.");
        }
    });
};

function orderReady(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order ready text to customer and mark order ready.');
    if (!st) {
        var mr = confirm("Would you like to mark the order as ready? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderready",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function refundOrder(orderID) {

    var ro = confirm('Are you sure you wish to refund the order? This action is not reversable.');
    var pin = prompt("Please enter Manager PIN");

    if (!ro) {
        // do nothing
        return false; 
    }

    // send text and mark as read
    var fd = new FormData();
    fd.append('managerPIN', pin),
    fd.append('uid', '{{Auth::id()}}'),
    fd.append('orderID', orderID),
    $.ajax({
        url: "/api/v1/admin/refundorder",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            alert(mydata['message']);
            location.reload();
        },
        error: function() {
            alert("There was a problem refunding the order. Please try again.");
        }
    });
};

 </script>

@endsection