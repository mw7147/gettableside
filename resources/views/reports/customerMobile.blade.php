@extends('reports.customerReportsMain')



@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h2 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h2>
            <small>
                Text Message Reporting
            </small>
        </div>
    </div>
</div>


<!-- page level css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>



<div class="row">

    <div class="col-xs-12 m-t-sm">
        
        <div class="ibox">
            <div class="ibox-title">

                <h3>Text Message Recipient List by Mobile Number</h3>

            </div>
            
            <div class="ibox-content">

                <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
                <a href="/customers/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
                    <thead>
                        <tr>
                            <th>Mobile</th>
                            <th>Carrier Name</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Total Sent</th>
                            <th>Last Sent</th>
                            <th>Details</th>

                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($mobileDetails as $mobileDetail)
                        <tr>               
                            <td>{{ $mobileDetail->mobile }}</td>
                            <td>{{ $mobileDetail->carrierName }}</td>
                            <td>{{ $mobileDetail->fname }}</td>
                            <td>{{ $mobileDetail->lname }}</td>
                            <td>{{ $mobileDetail->totalSent }}</td>
                            <td>{{ $mobileDetail->lastSent }}</td>  
                            <td><a class="btn btn-success btn-xs w90 m-r-xs" href="/reports/customers/textmessages/mobile/{{$mobileDetail->contactID}}"><i class="fa fa-binoculars m-r-xs"></i>Details</a></td>                              
                        </tr>
                    @endforeach

                    </tbody>                
                </table>
            </div>


            </div>
        </div>
 

    </div>
</div>





<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>


$(document).ready(function(){
    $('.dataTables').DataTable({
        pageLength: 25,
        dom: '<"html5buttons"B>lTfgitp',
        order: [[ 0, "asc" ]],
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'TextMessageReporting'},
            {extend: 'pdf', title: 'TextMessageReporting'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]

    });

});



</script>

@endsection