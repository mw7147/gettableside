<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Keep In Text Customer Reporting">
    <meta name="author" content="RiverHouseIT.com">

    <title>{{ $domain['name'] }} Reports</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ elixir('css/bootfont.css') }}" />

    <!-- Custom CSS -->
    <link href="{{ elixir('css/rhit_admin.css') }}" rel="stylesheet">


    <!-- Favicon -->
    <link rel="icon" type="image/png" href="favicon.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{ elixir('js/bootvuequery.js') }}"></script>

    <script src="{{ elixir('js/rhit_admin.js') }}"></script>

    <script src="/js/jquery.metisMenu.js"></script>


</head>

<body class="fixed-sidebar">
    <div id="wrapper">
 
        @include('domainOwner.ownerNavbar')

        <div id="page-wrapper" class="gray-bg">

        @include('domainOwner.ownerTopnav')


        @yield('content')


        <div class="clearfix m-t-xl m-b-lg"></div>

        @include('admin.footer')

    </div>

<!-- Scripts -->

</body>
</html>