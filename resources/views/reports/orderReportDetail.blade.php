@extends('admin.admin')

@section('content')

<h2>Order Detail</h2>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>Order ID: {{ $detail->id }}</i></h5>
    </div>
        
    <div class="ibox-content">

    <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-file-text m-r-xs"></i>Reports</a>
    <a href="{{ url()->previous() }}" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

    <div class="clearfix"></div>

    {{--<a href="/{{Request::get('urlPrefix')}}/vieworderonly/{{$detail->id}}" class="btn btn-warning btn-xs m-l-sm m-b-lg w110" target="_blank"><i class="fa fa-square m-r-xs"></i>View Receipt</a>--}}
    <a class="btn btn-warning btn-xs m-r-xs" href="/reports/{{Request::get('urlPrefix')}}/receipt/{{ $detail->id }}" target="popup" onclick="window.open('/reports/{{Request::get('urlPrefix')}}/receipt/{{ $detail->id }}','popup', 'width=800,height=700,top=15,left=30'); return false;" ><i class="fa fa-binoculars m-r-xs"></i>View Receipt</a>


        <div class="clearfix"></div>
        <div class="row">

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic">Customer ID: {{$detail->contactID}}<br>
                Order Total: ${{$detail->total}}<br>
                Order Refunded:@if ($detail->orderRefunded == 1) yes<br> @else no</p> @endif
                @if ($detail->orderRefunded ==1)
                Order Refund Amount: (${{ number_format($detail->orderRefundAmount,2) }})</p>
                @endif
            </div>

            <div class="clearfix"></div>

            <div class="col-md-6 col-sm-12 col-xs-12 m-b-md">
                <label class="font-normal font-italic">First Name:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->fname }}">
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Last Name:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->lname }}">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-6 col-sm-12 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Customer Mobile:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->customerMobile }}">
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Customer Email Address:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->customerEmailAddress }}">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Total:</label>
                <input type="text" class="form-control" disabled value="${{ $detail->total }}">
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tax:</label>
                <input type="text" class="form-control" disabled value="${{ $detail->tax }}">
            </div>
     
           <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Tip:</label>
                <input type="text" class="form-control" disabled value="${{ $detail->tip }}">
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Refund:</label>
                <input type="text" class="form-control" disabled value="${{ $detail->orderRefundAmount }}">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Payment Type:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->paymentType }}">
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">CC Last 4:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->ccTypeLast4 }}">
            </div>
     
           <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">CC Transaction Fee:</label>
                <input type="text" class="form-control" disabled value="${{ $detail->ccTypeTransactionFee }}">
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">CC Transaction ID:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->ccTypeTransactionID }}">
            </div>


            <div class="clearfix"></div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Order Type:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->orderType }}">
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Delivery Latitude:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->deliveryLatitude }}">
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Delivery Longitude:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->deliveryLongitude }}">
            </div>
     
           <div class="col-md-3 col-sm-6 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Delivery Feet:</label>
                <input type="text" class="form-control" disabled value="{{ number_format($detail->deliveryFeet, 0) }}">
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 m-b-md">
                <label class="font-normal font-italic">User Agent:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->userAgent }}">
            </div>
            <div class="clearfix"></div>
            
            <div class="col-md-12 m-b-md">
                <label class="font-normal font-italic">Session ID:</label>
                <input type="text" class="form-control" disabled value="{{ $detail->sessionID }}">
            </div>
            <div class="clearfix"></div>
        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->





@endsection