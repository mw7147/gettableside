@extends('admin.admin')



@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h2 class="m-b-none">
                {{ $domain['name'] }} Reporting Systems
            </h2>
            <small>
                QuickText Message Reporting
            </small>
        </div>
    </div>
</div>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down m-r-xs"></i>Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up m-r-xs"></i>Hide Instructions</button>
<div class="clearfix"></div>
<ul class="col-xs-12 col-sm-8 m-b-md instructions m-l-sm">
    <li>Sent QuickText Messages are listed below.</li>
    <li>Click "Message" to see all details for the sent message.</li>
    <li>Click "Receipients" to see detailed recipient list for the message.</li>
</ul>


<!-- page level css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>



<div class="row">

    <div class="col-xs-12 m-t-sm">
        
        <div class="ibox">
            <div class="ibox-title">

                <h3>Message Reporting for QuickText Message</h3>

            </div>
            
            <div class="ibox-content">

                <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
                <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dt-responsive nowrap dataTables" >
                    <thead>
                        <tr>
                            <th width="80">MID</th>
                            <th width="80">Group</th>
                            <th width="80">Queued</th>
                            <th width="80">Sent</th>

                            <th width="90">Start</th>
                            <th width="90">Finish</th>


                            <th width="150">Detailed Information</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($textsSent as $textSent)
                        <tr>                
                            <td>{{ $textSent->id }}</td>
                            <td>@if ($textSent->groupID == 0) All @else {{ $textSent->groupID }} @endif</td>
                            <td>{{ $textSent->totalQueue }}</td>
                            <td>{{ $textSent->totalSent }}</td>
                            <td>{{ $textSent->sendStart }}</td>
                            <td>{{ $textSent->sendStop }}</td>


                            <td><button class="btn btn-success btn-xs w90 m-r-xs" onclick="viewDetail({{ $textSent->id }})"><i class="fa fa-file-text m-r-xs"></i>Message</button>
                                <a class="btn btn-success btn-xs w90 m-r-xs" href="/reports/{{Request::get('urlPrefix')}}/textmessages/message/{{$textSent->id}}"><i class="fa fa-address-book m-r-xs"></i>Recipients</a>
                                
                            </td>
                        </tr>
                    @endforeach

                    </tbody>                
                </table>
            </div>








            </div>
        </div>
 

    </div>
</div>




<!-- Modals -->

<div class="modal inmodal" id="detailModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-commenting modal-icon"></i>
                    <h4 class="modal-title">Message Detail</h4>
                    <small class="font-bold">Sending details for Message ID: <span id="mid"></span> </small>
                </div>
                <div class="modal-body">
                    

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>

                                <tr>
                                    <td width="30%">Message ID:</td>
                                    <td id="messageID"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Group Name:</td>
                                    <td id="groupName"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Message Name:</td>
                                    <td id="messageName"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Total Queued:</td>
                                    <td id="queued"></td>
                                </tr>
                                    <td width="30%">Bad Numbers:</td>
                                    <td id="bad"></td>
                                </tr>
                                    <td width="30%">Total Sent:</td>
                                    <td id="sent"></td>
                                </tr>
                                    <td width="30%">Total Failed:</td>
                                    <td id="failed"></td>
                                </tr>
                                </tr>
                                    <td width="30%">Start Time:</td>
                                    <td id="start"></td>
                                </tr>
                                </tr>
                                    <td width="30%">Finish Time:</td>
                                    <td id="stop"></td>
                                </tr>
                               <tr>
                                    <td width="30%">Message Body:</td>
                                    <td id="body"></td>
                                </tr>
                                <tr>
                                    <td width="30%">Picture:</td>
                                    <td><a href="" id="pixlink" target="_blank"><img src="" id="picture"></a></td>
                                </tr>
                                <tr>
                                    <td width="30%">Sender IP:</td>
                                    <td id="ip"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>










<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


$(document).ready(function(){
    $('.dataTables').DataTable({
        pageLength: 25,
        dom: '<"html5buttons"B>lTfgitp',
        order: [[ 0, "desc" ]],
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'TextMessageReporting'},
            {extend: 'pdf', title: 'TextMessageReporting'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]

    });

});


function viewDetail(mid) {
    
    var fd = new FormData();    
    fd.append('mid', mid);

    $.ajax({
        url: "/api/v1/getmessagedetail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {

                alert(jsondata["message"]);
                return false;
                    
            } else {

                $('#mid').text(jsondata["detail"]["id"]);
                $('#messageID').text(jsondata["detail"]["id"]);
                $('#messageName').text(jsondata["messageName"]);                
                $('#groupName').text(jsondata["groupName"]);                
                $('#queued').text(jsondata["detail"]["totalQueue"]);
                $('#bad').text(jsondata["detail"]["totalBad"]);
                $('#sent').text(jsondata["detail"]["totalSent"]);
                $('#failed').text(jsondata["detail"]["totalSentFail"]);
                $('#start').text(jsondata["detail"]["sendStart"]);
                $('#stop').text(jsondata["detail"]["sendStop"]);
                $('#body').text(jsondata["detail"]["messageBody"]);

                if (jsondata["detail"]["pixFile"] != '') {
                    $('#picture').attr("src", jsondata["detail"]["pixFile"]);
                    $('#picture').attr("class", "img-lg img-thumbnail");
                    $('#pixlink').attr("href", jsondata["detail"]["pixFile"]);
                } else {
                    $('#picture').attr("src", "");
                    $('#picture').attr("class", "");
                    $('#pixlink').attr("href", "");   
                }
                
                $('#ip').text(jsondata["detail"]["sendIP"]);
                $('#detailModal').modal('show');
                return true;


        }}
    });
}


</script>


@endsection