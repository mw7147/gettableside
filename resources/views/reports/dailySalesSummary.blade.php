<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<style>
    .orderInProcess {
        background-color: #dfffdf;
    }

    .orderRefunded {
        background-color: #dff2ff;
    }


    #specificRangeHolder {
        margin-bottom: 36px;
    }

</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

	<h2>Sales Reporting</h2>

	<h4>
		Sales information is shown below.
	</h4>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

    <h3>Order List</h3><hr>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-file-text m-r-xs"></i>Reports</a>

    
    <div class="clearfix"></div>


    @if(Session::has('deleteSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('deleteError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <form name="ordersDateRange" method="POST" action = "/reports/{{Request::get('urlPrefix')}}/dailysalessummary" style="margin-bottom: 36px;">
        <div>
            <label class="font-normal font-italic">Quick Date Range:</label>
            <select class="form-control" name="dateRange" id="dateRange" style="max-width: 267px;">
                <option value="allFuture" @if ($dateRange == "allFuture")selected @endif>All Future Orders</option>
                <option value="tomorrow" @if ($dateRange == "tomorrow")selected @endif>Tomorrow</option>
                <option value="" @if ($dateRange == "")selected @endif>Select Quick Range</option>
                <option value="today" @if ($dateRange == "today")selected @endif>Today</option>
                <option value="yesterday" @if ($dateRange == "yesterday")selected @endif>Yesterday</option>
                <option value="week" @if ($dateRange == "week")selected @endif>This Week</option>
                <option value="lastWeek" @if ($dateRange == "lastWeek")selected @endif>Last Week</option>
                <option value="month" @if ($dateRange == "month")selected @endif>This Month</option>
                <option value="lastMonth" @if ($dateRange == "lastMonth")selected @endif>Last Month</option>
            </select>
        </div>

        <div class="clearfix"></div>
        <h4 class="m-l-sm" style="margin-top: 13px;"><i>Or</i></h4>
        <div class="clearfix"></div>

        <div id="specificRangeHolder">
            <label class="font-normal font-italic">Specific Date Range:</label>
            <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" name="specificRange" id="specificRange" style="min-width: 267px;" value="{{ $specificRange }}"/>
            </div>
        </div>

        <button type="submit" class="btn btn-info btn-sm" style="margin-top: -36px; min-width: 267px;">Set Range</button>
        
        {{ csrf_field() }}
    </form>

    <div class="clearfix"></div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover nowrap dataTables" >
            <thead>
                <tr>
                    <th width="50">Date</th>
                    <th width="70">Total Orders</th>
                    <th width="70">Total Refunded</th>
                    <th width="70">Gross Sales</th>
                    <th width="70">Discounts</th>
                    <th width="70">Refunds</th>
                    <th width="70">Sales Tax</th>
                    <th width="70">Tips</th>
                    <th width="70">Delivery Fee</th>
                    <th width="70">Delivery Tax</th>
                    <th width="70">CC Fee</th>
                    <th width="70">Net Sales</th>
                    <th width="120">Actions</th>
                </tr>
            </thead>
            <tbody>

{{--
    "totalOrders" => 41
    "refunded" => "0"
    "amountRefunded" => "0.00"
    "total" => "712.78"
    "totalDiscounts" => "0.00"
    "totalTax" => "54.00"
    "totalTip" => "120.58"
    "TotalDelivery" => "0.00"
    "totalDeliveryTax" => "0.00"
    "totalCCFee" => "33.01"
--}}

            @foreach ($orders as $order)
                <tr>
                    <td>{{ $order->orderDate }}</td>
                    <td>{{ $order->totalOrders }}</td>
                    <td>{{ $order->refunded }}</td>
                    <td>${{ number_format($order->subTotal, 2) }}</td>
                    <td>${{ number_format($order->totalDiscounts, 2) }}</td>
                    <td>${{ number_format($order->amountRefunded, 2) }}</td>
                    <td>${{ number_format($order->totalTax, 2) }}</td>
                    <td>${{ number_format($order->totalTip, 2) }}</td>
                    <td>${{ number_format($order->totalDelivery, 2) }}</td>
                    <td>${{ number_format($order->deliveryTaxAmount, 2) }}</td>
                    <td>${{ number_format($order->totalCCFee, 2) }}</td>
                    <td>${{ number_format($order->subTotal - $order->totalDiscounts - $order->amountRefunded - $order->totalTax - $order->totalTip - $order->totalDelivery - $order->deliveryTaxAmount - $order->totalCCFee, 2) }}</td>
                    
                    <td class="white-bg">
                       {{--
                        <a href="/reports/{{Request::get('urlPrefix')}}/salesreportdaily/{{ $order->orderDate }}" target="popup" 
                            onclick="window.open('/reports/{{Request::get('urlPrefix')}}/salesreportdaily/{{ $order->orderDate }}','popup', 'width=900,height=700,top=15,left=30'); return false;"
                            class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-binoculars m-r-xs"></i>Details</a>

                        --}}
                    

                    <a href="/reports/{{Request::get('urlPrefix')}}/datedetail/{{$domain->id}}/{{\Carbon\Carbon::parse($order->orderDate)->toDateString() }}" 
                        onclick="window.open('/reports/{{Request::get('urlPrefix')}}/datedetail/{{ $domain->id }}/{{\Carbon\Carbon::parse($order->orderDate)->toDateString() }}','popup', 'width=900,height=700,top=15,left=30'); return false;"
                        class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-binoculars m-r-xs"></i>Details
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>  
            
            <tfoot>
                <tr>
                    <td><b>Total</b></td>
                    <td><b>{{ $total->totalOrders }}</b></td>
                    <td><b>{{ $total->refunded }}</b></td>
                    <td><b>${{ $total->total }}</b></td>
                    <td><b>${{ $total->totalDiscounts }}</b></td>
                    <td><b>${{ $total->amountRefunded }}</b></td>
                    <td><b>${{ $total->totalTax }}</b></td>
                    <td><b>${{ $total->totalTip }}</b></td>
                    <td><b>${{ $total->TotalDelivery }}</b></td>
                    <td><b>${{ $total->totalDeliveryTax }}</b></td>
                    <td><b>${{ $total->totalCCFee }}</b></td>
                    <td><b>${{ number_format($total->total - $total->totalDiscounts - $total->amountRefunded - $total->totalTax - $total->totalTip - $total->TotalDelivery - $total->deliveryTaxAmount - $total->totalCCFee, 2) }}</b></td>
                    <td></td>
                </tr>
            </tfoot>

              
        </table>
        


    </div>
</div>

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<!-- Date range picker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('#specificRange').daterangepicker();

    $('#specificRange').on('change',function(){
        $('#dateRange').val('');
    });

</script>

 

 <script>

function ePrint(orderID) {
    var fd = new FormData();
    fd.append('orderID', orderID),    

    $.ajax({
        url: "/api/v1/resendeprint",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            alert(mydata);
            return true;
        },
        error: function() {
            alert("There was an problem with your ePrint request. Please try again.");
        }
    });
};

function orderReady(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order ready text to customer and mark order ready.');
    if (!st) {
        var mr = confirm("Would you like to mark the order as ready? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderready",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function refundOrder(orderID) {

    var ro = confirm('Are you sure you wish to refund the order? This action is not reversable.');
    var pin = prompt("Please enter Manager PIN");

    if (!ro) {
        // do nothing
        return false; 
    }

    // send text and mark as read
    var fd = new FormData();
    fd.append('managerPIN', pin),
    fd.append('uid', '{{Auth::id()}}'),
    fd.append('orderID', orderID),
    $.ajax({
        url: "/api/v1/admin/refundorder",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            alert(mydata['message']);
            location.reload();
        },
        error: function() {
            alert("There was a problem refunding the order. Please try again.");
        }
    });
};

 </script>

@endsection