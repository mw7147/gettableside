@extends('admin.admin')



@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h1 class="m-b-none">
                {{ $domain->name }} Dashboard
            </h1>
            <small>getTableSide Information Systems</small>
        </div>
    </div>
</div>

{{--

<div class="row m-t-md">
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Current</span>
                <h5>New Orders</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{ $totalContacts }}</h1>
                <small>New Orders In Process</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Today</span>
                <h5>Completed Orders</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{ $monthlyContacts }}</h1>
                <small>Completed Orders Today</small>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">This Month</span>
                <h5>New Customers</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{ $textsSentCount }}</h1>
                <small>New Customers This Month</small>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">System Wide</span>
                <h5>Total Customers</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">0</h1>
                <small>Total Customer System Wide</small>
            </div>
        </div>
    </div>
</div>

--}}
<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 210px;}
.db-blue-bg, .tgm-orange-bg {color: white;}
</style>

<div class="row m-t-xs m-b-lg">


    <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" >
        <div class="widget blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-clock-o fa-4x"></i>
                <h1 class="m-xs">Orders</h1>
                <h3 class="font-bold no-margins">
                    Current Orders
                </h3>
            </div>
        </div>
    </a>

    <a href="/{{Request::get('urlPrefix')}}/orderadmin/view" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" >
        <div class="widget blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-cutlery fa-4x"></i>
                <h1 class="m-xs">Orders</h1>
                <h3 class="font-bold no-margins">
                    Order History
                </h3>
            </div>
        </div>
    </a>


    <a href="/contacts/{{Request::get('urlPrefix')}}/view" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-user fa-4x"></i>
                <h1 class="m-xs">Contacts</h1>
                <h3 class="font-bold no-margins">
                    Manage Contacts
                </h3>
            </div>
        </div>
    </a>

    <a href="/contacts/{{Request::get('urlPrefix')}}/new" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-user-plus fa-4x"></i>
                <h1 class="m-xs">Create</h1>
                <h3 class="font-bold no-margins">
                    Create New Contact
                </h3>
            </div>
        </div>
    </a>

<!--
    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" id="reports">
        <div class="widget white-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-file-text fa-4x"></i>
                <h1 class="m-xs">Reports</h1>
                <h3 class="font-bold no-margins">
                    Reporting Systems
                </h3>
            </div>
        </div>
    </a>


    <a href="#help" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon" id="help" onclick="comingSoon()">
        <div class="widget tgm-orange-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-question-circle fa-4x"></i>
                <h1 class="m-xs">Help</h1>
                <h3 class="font-bold no-margins">
                    Help
                </h3>
            </div>
        </div>
    </a>
-->

</div>

<script>

function comingSoon() {

    alert("Under Development - Coming Soon");
    return false;

}


</script>

@endsection