<!-- NAVBAR -->

    <nav class="navbar-default navbar-static-side" role="navigation" >
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold lite-color font15px">{{ Auth::user()->fname }} {{ Auth::user()->lname }}</strong>
                             </span>

                        <hr class="hr-small"/>

                        <span class="text-muted text-xs block">{{ $domain['name'] }}</span>
                            
                    </div>
                    <div class="logo-element">
                        KIT
                    </div>
                </li>

                    <li @if($breadcrumb[0] == 'dashboard')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-dashboard iconmenu"></i> <span class="nav-label">Dashboard</span></a>
                    </li>

                    <li @if($breadcrumb[1] == 'ordersview')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview"><i class="fa fa-clock-o iconmenu"></i> <span class="nav-label">Current Orders</span></a>
                    </li>

                    <li @if($breadcrumb[0] == 'orders')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/orderadmin/view"><i class="fa fa-cutlery iconmenu"></i> <span class="nav-label">Order History</span></a>
                    </li>

                    <li @if($breadcrumb[0] == 'contacts')class="active"@endif>
                        <a href=""><i class="fa fa-user iconmenu"></i> <span class="nav-label">Contacts</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'view')class="active"@endif><a href="/contacts/{{Request::get('urlPrefix')}}/view">View / Edit</a></li>
                            <li @if($breadcrumb[1] == 'new')class="active"@endif><a href="/contacts/{{Request::get('urlPrefix')}}/new">Add New</a></li>
                        </ul>
                    </li>
<!--
                    <li @if($breadcrumb[0] == 'reports')class="active"@endif>
                        <a href="/reports/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-newspaper-o iconmenu"></i> <span class="nav-label">Reports</span></a>
                    </li>
-->

            </ul>

        </div>
    </nav>