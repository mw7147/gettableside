<!-- TopNav -->
    <style>
        .flexBox {
            width: 100%;
        }
        .flexBox .topAdmin {
            float: left;
        }
        .flexBox .navbar-right {
            margin-right: 0;
        }
        @media(max-width: 767px) {
            .responsiveMenu .logOutButton {
                float: right;
                background-color: #ffffff !important;
            }
            .responsiveMenu .logOutButton a {
                background-color: #ffffff !important;
            }
            .top-navigation .nav > li > a {
                padding: 20px 10px;
            }
        }
    </style>
       <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg responsiveMenu" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header flexBox">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary adminRight " href="#"><i class="fa fa-bars"></i> </a>
                        <img class="img img-responsive topAdmin" src="/img/tgm320.png" alt="getTableSide.biz Administration" />
                        <ul class="nav navbar-top-links navbar-right">
                            <li class="logOutButton">
                                <a href="/userlogout"><i class="fa fa-sign-out m-r-xs"></i>Logout</a>
                            </li>
                        </ul>
                </div>
            </nav>
        </div>

<!-- End TopNav -->
