@extends('kitAdmin.kitAdmin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>System Subscribers Administration</h1>

	<h4>
		System Subscribers are listed below.
	</h4>

	<ul>
		<li>Type in the search box to search results.</li>
        <li>To delete a subscriber from the database - select delete. No further communication will occur.</li>
        <li>To view communication history - press history</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>{{ $territory['name']}} System Subscribers</h3><hr>

        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th>Sub ID</th>
                    <th>Registered Number</th>
                    <th>Carrier</th>
                    <th>Territory</th>
                    <th>Register Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($subscribers as $subscriber)
                <tr>                
                    <td>{{ $subscriber['id'] }}</td>
                    <td>{{ $subscriber['cellNumber'] }}</td>
                    <td>{{ $subscriber['carrierName'] }}</td>
                    <td>{{ $subscriber['name'] }}</td>
                    <td>{{ $subscriber['created_at'] }}</td>
                    <td>
                    <a href="/nihadmin/subscribers/{{ $subscriber['id'] }}"><i class="fa fa-pencil"></i> View/Edit</a> | 
                    <a href="/nihadmin/subscribers/events/{{ $subscriber['id'] }}"><i class="fa fa-ticket"></i> Events</a> | 
                    <a href="/nihadmin/subscribers/history/{{ $subscriber['id'] }}"><i class="fa fa-refresh"></i> History</a> | 
                    <a href="/nihadmin/subscribers/delete/{{$subscriber['id'] }}" onclick="javascript:return confirm('Are you sure you want to delete this subscriber? All subscriber data will be permanently deleted.')"><i class="fa fa-remove"></i> Delete</a>
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>


 

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                order: [[ 1, "asc" ]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });



    </script>

 





@endsection