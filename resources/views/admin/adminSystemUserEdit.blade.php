@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">



<h1>System User Information</h1>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Update the User information as necessary.</li>
    <li>The email address will be used as the username on the login screen.</li>
    <li>The password may be changed on the password reset page.</li>
    <li>Inactive users are retained in the system but lose login and viewing privileges.</li> f
    <li>Press "Update Information" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating.</li>
    <li>NOTE: If systemUseris "Inactive" no messages will be sent to users on systemUsers behalf.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Name:</i> {{ $systemUser['fname'] }} {{$systemUser['lname'] }} - User ID: {{ $systemUser->id }}</h5>
    </div>
        
    <div class="ibox-content">

        <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/admin/systemuser/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>System Users</a>
        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif


        @if(Session::has('mobileError'))
        <div class="alert alert-warning alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('mobileError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="updateSystemUser" method="POST" action="/admin/systemuser/edit/{{ $systemUser->id }}">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">User Information</p>
            </div>

            <div class="clearfix"></div>


            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">User Status:</label>
                <select class="form-control" name="active" required>
                    <option value="yes">Active</option>
                    <option @if ($systemUser['active'] == "no")selected @endif value="no">Inactive</option>
                </select>
            </div>
            
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">User Level (Role):</label>
                <select name="role_auth_level" class="form-control" required>
                    @foreach ($roles as $role)
                    <option @if($systemUser->role_auth_level == $role->auth_level)selected @endif value="{{ $role->auth_level }}">{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Email (username):</label>
                <input type="email" placeholder="Email" class="form-control" name="email" id="email" value="{{ $systemUser->email ?? '' }}" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Mobile Phone:</label>
                <input class="form-control" type="tel" id="mobile" name="mobile" autocomplete="off" value="{{ $systemUser->mobile ?? '' }}" placeholder="Mobile Phone">
            </div>

            <div class="clearfix"></div>


            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">First Name:</label>
                <input type="text" placeholder="First Name" class="form-control" name="fname" value="{{ $systemUser->fname ?? '' }}" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Last Name:</label>
                <input class="form-control" type="text" name="lname" value="{{ $systemUser->lname ?? '' }}" required placeholder="Last Name">
            </div>

            <div class="clearfix"></div>        

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Zip Code:</label>
                <input type="text" placeholder="Zip Code" minlength="5" class="form-control" id="zipCode" name="zipCode" value="{{ $systemUser->zipCode ?? '' }}">
            </div>

            <div class="clearfix"></div>
            
            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Optional Information</p>
            </div>
            <div class="clearfix"></div>           

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                <label class="font-normal font-italic">Birthday:</label>
                <input type="text" placeholder="MM/DD/YYYY" class="form-control" name="birthday" id="birthday" value="{{ $systemUser->birthday ?? '' }}">
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                <label class="font-normal font-italic">Anniversary:</label>
                <input type="text" placeholder="MM/DD/YYYY" class="form-control" name="anniversary" id="anniversary" value="{{ $systemUser->anniversary ?? '' }}">
            </div>

            <div class="clearfix"></div>

 
            <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                    
                        {{ csrf_field() }}
                    <a href="/{{Request::get('urlPrefix')}}/systemuser/view" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Information</button>

            </div>



</form>
<!-- Page Level Scripts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$('#email').on('change', function() {  // user name check
    var email = $("#email").val();
    var uid = {{$systemUser->id}}
    var did = $("#did").val();
    var fd = new FormData();    
    fd.append('email', email);
    fd.append('did', did);
    fd.append('uid', uid);
    $.ajax({
        url: "/api/v1/checkuseremail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert(jsondata['message']); 
                return false;   
                    
            } else {
                
                return true;
 
        }},
        // validation error
        error: function(data){
        var errors = data.responseJSON;
        $('#email').val('');
        $("#email").focus();
        alert(errors.errors.email);

      }
    });
});


$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


</script>


<script>

$( document ).ready(function() {
  console.log("ready");
  $("#mobile").mask("(999) 999-9999");
  $("#zipCode").mask("99999?-9999");
  $("#birthday").mask("99/99/9999");
  $("#anniversary").mask("99/99/9999");
});
  
$('#mobile').change(function() {
  var num = $(this).val();
  var intNum =  num.replace(/[^\d]/g, '');

if ( intNum.length != 10 ) {
  $("#mobile").val(''); 
  alert("Only 10 digit phone numbers are allowed.");
  return false;
}

var fd = new FormData();
fd.append('mobile', $(this).val()),    

  $.ajax({
      url: "/api/v1/checkmobile",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] != "registered") {
            $('#mobile').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});



$('#did').change(function() {
  var did = $(this).val();

if ( did < 1 ) {
    $("#did").val(''); 
    $("#did").focus(); 
    alert("Only positive integers allowed.");
    return false;
}

var fd = new FormData();
fd.append('did', did),    

  $.ajax({
      url: "/api/v1/checkdid",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] == "error") {
            $('#did').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your Customer ID. Please try again.");
      }
  });
});

</script>

@endsection