@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">



<h1>Create New Customer - Owner User Level</h1>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Add the requested information as requested.</li>
    <li>The email address will be used as the username on the login screen.</li>
    <li>The password will be set to 'p@ssword'.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Customer</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/admin/systemuser/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>System Users</a>
        <div class="clearfix"></div>

        @if ($errors->any())
            <div class="alert alert-danger col-md-9">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif


        @if(Session::has('mobileError'))
        <div class="alert alert-warning alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('mobileError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">
            <form name="createNewCustomer" method="POST" action="/admin/customer/create">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Customer Information</p>
                </div>

                <div class="clearfix"></div>


                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Customer Name (Business Name):</label>
                    <input type="text" placeholder="Customer Name" class="form-control" name="customerName" id="customerName" value="{{ old('customerName') ?? '' }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Sitename:</label>
                    <input type="text" placeholder="Site Name" class="form-control" id="siteName" name="siteName" value="{{ old('siteName') ?? '' }}" required>
                </div>

                <div class="clearfix"></div>
        
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Subdomain:</label>
                    <input type="text" placeholder="Subdoman" class="form-control" name="subDomain" id="subDomain" value="{{ old('subDomain') ?? '' }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Http Host:</label>
                    <input type="text" placeholder="Http Host - subdomain.togomeals.biz" class="form-control" id="httpHost" name="httpHost" value="{{ old('httpHost') ?? '' }}" required>
                </div>

                <div class="clearfix"></div>           

                <hr> 
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">User Information</p>
                </div>
                <div class="clearfix"></div>  

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Email (username):</label>
                    <input type="email" placeholder="Email" class="form-control" name="email" id="email" value="{{ old('email') ?? '' }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Mobile Phone:</label>
                    <input class="form-control" type="tel" id="mobile" name="mobile" autocomplete="off" value="{{ old('mobile') ?? '' }}" required placeholder="Mobile Phone">
                </div>

                <div class="clearfix"></div>


                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">First Name:</label>
                    <input type="text" placeholder="First Name" class="form-control" name="fname" value="{{ old('fname') ?? '' }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Last Name:</label>
                    <input class="form-control" type="text" name="lname" value="{{ old('lname') ?? '' }}" required placeholder="Last Name">
                </div>

                <div class="clearfix"></div>

     
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg m-t-md text-center">
                        
                            {{ csrf_field() }}
                        <a href="/owner/systemusers/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Create New Customer</button>

                </div>



            </form>
        </div>
    </div>
</div>
<!-- Page Level Scripts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$('#email').on('change', function() {  // user name check
    var email = $("#email").val();
    var did = $("#did").val();
    var fd = new FormData();    
    fd.append('email', email);
    fd.append('did', did);
    $.ajax({
        url: "/api/v1/checkuseremail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert(jsondata['message']); 
                return false;   
                    
            } else {
                
                return true;
 
        }},
        // validation error
        error: function(data){
        var errors = data.responseJSON;
        $('#email').val('');
        $("#email").focus();
        alert(errors.errors.email);

      }
    });
});


$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


</script>


<script>

$( document ).ready(function() {
  console.log("ready");
  $("#mobile").mask("(999) 999-9999");
  $("#zipCode").mask("99999?-9999");
});

  
$('#mobile').change(function() {
  var num = $(this).val();
  var intNum =  num.replace(/[^\d]/g, '');

if ( intNum.length != 10 ) {
  $("#mobile").val(''); 
  alert("Only 10 digit phone numbers are allowed.");
  return false;
}

var fd = new FormData();
fd.append('mobile', $(this).val()),    

  $.ajax({
      url: "/api/v1/checkmobile",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] != "registered") {
            $('#mobile').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});


</script>

@endsection