@extends('admin.admin')



@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>System Users Administration</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<h4>
		System Users are listed below.
	</h4>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>System Users Administration</h3><hr>

    @if(Session::has('deleteSuccess'))
        <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteSuccess') }}
        </div>
        <div class="clearfix"></div>
    @endif

    @if(Session::has('deleteError'))
        <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('deleteError') }}
        </div>
        <div class="clearfix"></div>
    @endif

    <a href="/admin/systemuser/new" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Create New System User</a>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables" >
            <thead>
                <tr>
                    <th>User ID</th>
                    <th>Active</th>
                    <th>Name</th>
                    @if ($authLevel >= 40) <th>Customer</th> @endif
                    <th>Role</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th style="max-width: 300px;">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($systemUsers as $systemUser)

                <tr>                
                    <td>{{ $systemUser['id'] }}</td>
                    <td>{{ $systemUser['active'] }}</td>
                    <td>{{ $systemUser['fname'] }} {{$systemUser['lname'] }}</td>
                    @if ($authLevel >= 40) <td>{{ $systemUser['customer'] }}</td> @endif
                    <td class="authlevel">{{ $systemUser['role'] }}</td>
                    <td>{{ $systemUser['email'] }}</td>
                    <td>{{ $systemUser['mobile'] }}</td>
                    <td>

                    <a href="/admin/systemuser/edit/{{ $systemUser['id'] }}" class="btn btn-primary btn-xs w90"><i class="fa fa-pencil"></i> View/Edit</a>
                    <a href="/admin/systemuser/password/{{ $systemUser['id'] }}" class="btn btn-primary btn-xs w90"><i class="fa fa-refresh"></i> Password</a>

                    @if ($systemUser->role_auth_level < $authLevel)
                    <a href="/admin/systemuser/delete/{{ $systemUser['id'] }}" class="btn btn-danger btn-xs w90" onclick="confirmDelete(event)"><i class="fa fa-refresh"></i> Delete</a>
                    @endif
                       
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>

    </div>
</div>

    <!-- Page-Level Scripts -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

    <script>


        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
 
    <script>

        $('.btn-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        $('.btn-up-instructions').on('click',function(){
            $('.instructions').toggle();
            $('.btn-instructions').toggle();
            $('.btn-up-instructions').toggle();
        });

        function confirmDelete(e) {
            var cd = confirm('Are you sure you want to delete the system user. All information will be deleted!');
            if (!cd) {e.preventDefault();} 
        };

    </script>

@endsection