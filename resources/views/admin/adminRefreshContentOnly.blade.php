<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="getTableSide.biz">
    <meta name="author" content="Hardwired Creative Technologies - www.hardwiredcreative.com">
    <meta http-equiv="refresh" content="60"/>

    <title>getTableSide</title>

    <!-- rhit Core CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
    <link href="{{ mix('css/rhit_admin.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/img/hwctH.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- HWCT Core JavaScript -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/rhit_admin.js') }}"></script>

</head>

<body class="top-navigation">
    <div id="wrapper">


    <div id="page-wrapper" class="gray-bg">


    @yield('content')

</div></div>
</body>
</html>