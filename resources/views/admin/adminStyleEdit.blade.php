@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h1>Edit Customer CSS Styles</h1>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>


<h4>
        {{ $domain->name }} CSS Style Information.
</h4>

<ul class="m-b-md instructions">
    <li>Fill in the style as necessary.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating the Customer Information.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>{{ $domain ->name }} Customer ID: {{ $domain->id }}</i></h5>
    </div>
        
    <div class="ibox-content">


    <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/admin/customer/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>Customers</a>
    <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif


        <div class="clearfix"></div>
        <div class="row">
            <form name="editSyle" id="editSyle" method="POST" action="/admin/customer/style/{{ $domain->id }}">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic">Required Styles For All Customers</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Background Color:</label>
                    <input type="text" placeholder="Background Color" class="form-control" name="backgroundColor" value="{{ $styles->backgroundColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Font Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="fontColor" value="{{ $styles->fontColor ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Button Background Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="bannerButtonBackgroundColor" value="{{ $styles->bannerButtonBackgroundColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Button Border Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="bannerButtonBorderColor" value="{{ $styles->bannerButtonBorderColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Button Font Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="bannerButtonColor" value="{{ $styles->bannerButtonColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Button Font Hover Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="bannerButtonColorHover" value="{{ $styles->bannerButtonColorHover ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Complete Order Button Background Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="completeOrderBackgroundColor" value="{{ $styles->completeOrderBackgroundColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Complete Order Button Border Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="completeOrderBorderColor" value="{{ $styles->completeOrderBorderColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Complete Order Button Font Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="completeOrderColor" value="{{ $styles->completeOrderColor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Complete Order Button Font Hover Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="completeOrderColorHover" value="{{ $styles->completeOrderColorHover ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Address Top Margin (pixels):</label>
                    <input type="text" placeholder="Location Address Top Margin" class="form-control" name="locationAddressTopMargin" value="{{ $styles->locationAddressTopMargin ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Address Font Color:</label>
                    <input type="text" placeholder="Location Address Font Color" class="form-control" name="locationAddressFontColor" value="{{ $styles->locationAddressFontColor ?? '' }}">
                </div>

                <div class="clearfix"></div>

                @if ($domain['type'] > 3)
                <hr>

                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic">Required Styles For Website Customers</p>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Banner Home Minumum Height (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="bannerHomeMinHeight" value="{{ $styles->bannerHomeMinHeight }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Banner Picture Background Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="pixBannerBackgroundColor" value="{{ $styles->pixBannerBackgroundColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Picture Banner Opacity:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="pixBannerOpacity" value="{{ $styles->pixBannerOpacity }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Picture Banner Padding (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="pixBannerPadding" value="{{ $styles->pixBannerPadding }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Picture Banner Width (%):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="pixBannerWidth" value="{{ $styles->pixBannerWidth }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Picture Banner Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="pixBannerColor" value="{{ $styles->pixBannerColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Toggle Background Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarToggleBackgroundColor" value="{{ $styles->navBarToggleBackgroundColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Toggle Border Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarToggleBorderColor" value="{{ $styles->navBarToggleBorderColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Logo Max Height (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarLogoMaxHeight" value="{{ $styles->navBarLogoMaxHeight }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Logo Max Padding (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarLogoMaxPadding" value="{{ $styles->navBarLogoMaxPadding }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Logo Max Left Margin (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarLogoMaxMarginLeft" value="{{ $styles->navBarLogoMaxMarginLeft }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Logo Max Top Margin (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarLogoMaxMarginTop" value="{{ $styles->navBarLogoMaxMarginTop }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Navbar Item (li) Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarLIColor" value="{{ $styles->navBarLIColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">>Navbar Item (li) Hover Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="navBarLIHoverColor" value="{{ $styles->navBarLIHoverColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Callout Banner Background Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="calloutBannerBackgroundColor" value="{{ $styles->calloutBannerBackgroundColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Callout Banner Border Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="calloutBannerBorderColor" value="{{ $styles->calloutBannerBorderColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Callout Banner Font Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="calloutBannerColor" value="{{ $styles->calloutBannerColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Callout Banner Height:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="calloutBannerHeight" value="{{ $styles->calloutBannerHeight }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Footer Background Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="footerBackgroundColor" value="{{ $styles->footerBackgroundColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Footer Font Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="footerColor" value="{{ $styles->footerColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Footer Link Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="footerLinkColor" value="{{ $styles->footerLinkColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Footer Link Hover Color:</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="footerLinkHoverColor" value="{{ $styles->footerLinkHoverColor }}">
                </div>
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Services Min Height (pixels):</label>
                    <input type="text" placeholder="Font Color" class="form-control" name="servicesMinHeight" value="{{ $styles->servicesMinHeight }}">
                </div>
                @endif

                {{ csrf_field() }}

                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <a href="/admin/customer/view" class="btn btn-white text-center" type="submit">Cancel</a>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </form>

        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->


<script>
$(document).ready(function(){ 

    $('#httpHost').change(function() {  // user name check
        var httpHost = document.getElementById("httpHost").value;  
        var fd = new FormData();    
        fd.append('httpHost', httpHost);

        $.ajax({
            url: "/api/v1/admin/checkhttphost",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {

                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $("#httpHost").val("");
                    $("#httpHost").focus();
                    alert("The httpHost URL is used by another Domain. This URL must be unique. Please use a different URL for the httpHost address."); 
                    return false;   
                        
                } else {
                    
                return true;    
            }}
        });
    });



    $('#logoChk').change(function() {  // user name check
        $("#logoCheck").val("changed");
    });

    $('#backgroundChk').change(function() {  // user name check
        $("#backgroundCheck").val("changed");
    });

});
</script>




<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection