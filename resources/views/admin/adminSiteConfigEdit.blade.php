@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h1>Edit Customer Site Information</h1>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>


<h4>
        {{ $domain->name }} Customer Site Information.
</h4>

<ul class="m-b-md instructions">
    <li>Fill in the style as necessary.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating the Customer Information.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>{{ $domain ->name }} Customer ID: {{ $domain->id }}</i></h5>
    </div>
        
    <div class="ibox-content">


    <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/admin/customer/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>Customers</a>
    <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif


        <div class="clearfix"></div>
        <div class="row">
            <form name="editSyle" id="editSyle" method="POST" action="/admin/customer/siteconfig/{{ $domain->id }}">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Location Information</p>
                </div>

                <div class="clearfix"></div>
                
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Manager First Name:</label>
                    <input type="text" placeholder="Manager First Name" class="form-control" name="managerFname" value="{{ $siteConfig->managerFname ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Manager Last Name:</label>
                    <input type="text" placeholder="Manager Last Name" class="form-control" name="managerLname" value="{{ $siteConfig->managerLname ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Name:</label>
                    <input type="text" placeholder="Location Name" class="form-control" name="locationName" value="{{ $siteConfig->locationName ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Sales Tax Rate:</label>
                    <input type="num" step=".01" min="2" max="20" placeholder="Location Tax Rate" class="form-control" name="taxRate" value="{{ $siteConfig->taxRate ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Address1:</label>
                    <input type="text" placeholder="Address 1" class="form-control" name="address1" value="{{ $siteConfig->address1 ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Address 2:</label>
                    <input type="text" placeholder="Address 2" class="form-control" name="address2" value="{{ $siteConfig->address2 ?? '' }}">
                </div>

                <div class="clearfix"></div>

                   <div class="col-md-4 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic">City:</label>
                        <input class="form-control" type="text" name="city" value="{{ $siteConfig->city ?? '' }}" placeholder="City">
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <label class="font-normal font-italic">State:</label>
                        <select class="form-control" name="state" id="state">
                            
                            @foreach ($states as $state)
                            <option value="{{ $state->stateCode }}" @isset($siteConfig->state) @if ($state->stateCode == $siteConfig->state) selected @endif @endisset>{{ $state->stateName }}</option>
                            @endforeach
                            

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label class="font-normal font-italic">Zip Code:</label>
                        <input class="form-control" type="text" name="zipCode" value="{{ $siteConfig->zipCode ?? '' }}" placeholder="Zip Code">
                    </div>

                    <div class="clearfix"></div>
                    <hr>

                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <p class="font-italic font-bold">Location Order Information</p>
                    </div>

                    <div class="clearfix"></div>
                    
                @if ( $domain->type != 9 )
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Ahead Service:</label>
                    
                        <select class="form-control" name="orderAhead" required>
                            <option value="1" @isset($siteConfig->orderAhead) @if ($siteConfig->orderAhead == 1) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->orderAhead) @if ($siteConfig->orderAhead == 0) selected @endif @endisset>No</option>
                        </select>

                </div>
                <div class="clearfix"></div>
                @else
                <input type="hidden" name="orderAhead" value="0">
                @endif

                @if ( $domain->type != 9 )
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Pickup Prepare Time in Minutes:</label>
                    <input type="number" min="0" max="120" step="1" placeholder="Pickup Prepare Time in Minutes" class="form-control" name="pickupPrepMinutes" value="{{ $siteConfig->pickupPrepMinutes ?? 0 }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Prepare Time in Minutes:</label>
                    <input type="number" min="0" max="120" step="1" placeholder="Delivery Prepare Time in Minutes" class="form-control" name="deliveryPrepMinutes" value="{{ $siteConfig->deliveryPrepMinutes ?? 0 }}">
                </div>

                <div class="clearfix"></div>
                @endif

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Cart Clear Timer In Minutes:</label>
                    <input type="number" min="0" max="60" step="1" placeholder="Cart Clear Timer in Minutes" class="form-control" name="cartClearTimer" value="{{ $siteConfig->cartClearTimer ?? 0 }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Dine In Last Order Offset Time in Minutes:</label>
                    <input type="number" min="0" max="90" step="1" placeholder="Dine In Last Order Offset Time in Minutes" class="form-control" name="dineInLastOrderTimeOffset" value="{{ $siteConfig->dineInLastOrderTimeOffset ?? 0 }}">
                </div>

                <div class="clearfix"></div>
                @if ( $domain->type != 9 )
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Require Recommended Tip:</label>
                    
                        <select class="form-control" name="deliveryRequireRecommendedTip" required>
                            <option value="1" @isset($siteConfig->deliveryRequireRecommendedTip) @if ($siteConfig->deliveryRequireRecommendedTip == 1) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->deliveryRequireRecommendedTip) @if ($siteConfig->deliveryRequireRecommendedTip == 0) selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Recommended Tip Percent:</label>
                    <input type="number" min="0" max="100" step="1" placeholder="Delivery Recommended Tip Percent" class="form-control" name="deliveryRecommendedTipPercent" value="{{ $siteConfig->deliveryRecommendedTipPercent ?? 0 }}">
                </div>

                <div class="clearfix"></div>
                @endif

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9)Pickup @else Dine In @endif Require Recommended Tip:</label>
                    
                        <select class="form-control" name="pickupRequireRecommendedTip" required>
                            <option value="1" @isset($siteConfig->pickupRequireRecommendedTip) @if ($siteConfig->pickupRequireRecommendedTip == 1) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->pickupRequireRecommendedTip) @if ($siteConfig->pickupRequireRecommendedTip == 0) selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9)Pickup @else Dine In @endif Recommended Tip Percent:</label>
                    <input type="number" min="0" max="60" step="1" placeholder="@if ($domain->type != 9)Pickup @else Dine In @endif Recommended Tip Percent" class="form-control" name="pickupRecommendedTipPercent" value="{{ $siteConfig->pickupRecommendedTipPercent ?? 0 }}">
                </div>

                <div class="clearfix"></div>

                <hr>

                @if ($domain->type != 9)

                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Location Delivery Information</p>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Delivery:</label>
                    
                        <select class="form-control" name="delivery" required>
                            <option value="fr" @isset($siteConfig->delivery) @if ($siteConfig->delivery == "fr") selected @endif @endisset>Food Runner</option>
                            <option value="yes" @isset($siteConfig->delivery) @if ($siteConfig->delivery == "yes") selected @endif @endisset>Restaurant Provided</option>
                            <option value="no" @isset($siteConfig->delivery) @if ($siteConfig->delivery == "no") selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Delivery Radius (in tenths of miles - i.e. 1.2 miles):</label>
                    <input type="text" placeholder="Dellivery Radius" class="form-control" name="deliveryRadius" value="{{ $siteConfig->deliveryRadius ?? '' }}">
                </div>

                <div class="clearfix"></div>
                
                <div class="col-md-4 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Fee:</label>
                    <input type="text" placeholder="Location Delivery Fee" class="form-control" name="deliveryCharge" value="{{ $siteConfig->deliveryCharge ?? '' }}">
                </div>

                <div class="col-md-3 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Latitude:</label>
                    <input type="text" placeholder="Location Latitude" class="form-control" name="latitude" value="{{ $siteConfig->latitude ?? '' }}">
                </div>

                <div class="col-md-3 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Longitude:</label>
                    <input type="text" placeholder="Location Longitude" class="form-control" name="longitude" value="{{ $siteConfig->longitude ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Tax Delivery:</label>
                    
                        <select class="form-control" name="taxDelivery" required>
                            <option value="1" @isset($siteConfig->taxDelivery) @if ( $siteConfig->taxDelivery == 1 ) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->taxDelivery) @if ( $siteConfig->taxDelivery == 0 ) selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Tax Rate:</label>
                    <input type="text" placeholder="Delivery Tax Rate" class="form-control" name="deliveryTaxRate" value="{{ $siteConfig->deliveryTaxRate ?? 0 }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Minimum Order:</label>
                    <input type="text" placeholder="Delivery Minimum Order" class="form-control" name="deliveryMinOrder" value="{{ $siteConfig->deliveryMinOrder ?? '' }}">
                </div>
                
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Google Maps API Key:</label>
                    <input type="text" placeholder="Google Maps API Key" class="form-control" name="googleMapKey" value="{{ $siteConfig->googleMapKey ?? '' }}">
                </div>


                <div class="clearfix"></div>

                <hr>

                @endif

                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Email and Text Settings</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Site Email Address:</label>
                    <input type="email" placeholder="Site Email Address" class="form-control" required name="sendEmail" value="{{ $siteConfig->sendEmail ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Site Phone Number:</label>
                    <input type="tel" placeholder="Site Telephone" id="phone" class="form-control" required name="sendText" value="{{ $siteConfig->sendText ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Send Email Username:</label>
                    <input type="email" placeholder="Send Email Username" class="form-control" required name="sendEmailUserName" value="{{ $siteConfig->sendEmailUserName ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Send Email Password</label>
                    <input type="text" placeholder="Send Email Password" class="form-control" required name="sendEmailPassword" value="@if (!empty($siteConfig->sendEmailPassword)){{decrypt($siteConfig->sendEmailPassword)}}@endif">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Receive Email Address:</label>
                    <input type="email" placeholder="Order Receive Email Address" class="form-control" required name="orderReceiveEmail" value="{{ $siteConfig->orderReceiveEmail ?? '' }}">
                </div>
                {{--

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">HP ePrint Email Address:</label>
                    <input type="email" placeholder="HP ePrint Email Address" class="form-control" name="hpEprint" value="{{ $siteConfig->hpEprint ?? '' }}">
                </div>
                --}}

                <div class="clearfix"></div>

                <hr>

                 <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Email Configuration</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9)Pickup @else Dine In @endif Order Message:</label>
                    <textarea placeholder="Pickup Order Message" rows="4" class="form-control" name="orderMessage">{{ $siteConfig->orderMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                @if ( $domain->type != 9 )

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Order Message:</label>
                    <textarea placeholder="Delivery Order Message" rows="4" class="form-control" name="deliveryMessage">{{ $siteConfig->deliveryMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Future Pickup Order Message - [[pickupTime]] replacement for pickup date and time:</label>
                    <textarea placeholder="Future Pickup Order Message" rows="4" class="form-control" name="futureOrderMessage">{{ $siteConfig->futureOrderMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Future Delivery Order Message - [[deliveryTime]] replacement for pickup date and time:</label>
                    <textarea placeholder="Future Delivery Order Message" rows="4" class="form-control" name="futureDeliveryMessage">{{ $siteConfig->futureDeliveryMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Footer Message:</label>
                    <textarea placeholder="Order Footer Message" rows="4" class="form-control" name="orderFooter">{{ $siteConfig->orderFooter ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                
                
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Pickup Order In Process Message:</label>
                    <textarea placeholder="Pickup Order In Process Message" rows="4" class="form-control" name="pickupOIP">{{ $siteConfig->pickupOIP ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                
                
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Order In Process Message:</label>
                    <textarea placeholder="Delivery Order In Process Message" rows="4" class="form-control" name="deliverOIP">{{ $siteConfig->deliverOIP ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div> 

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Text Order Ready Message:</label>
                    <textarea placeholder="Delivery Text Order Ready Message" rows="4" class="form-control" name="deliverOrderReady">{{ $siteConfig->deliverOrderReady ?? '' }}</textarea>
                </div>
                
                <div class="clearfix"></div>

                
                @endif
        

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9)Pickup @else Dine In @endif Text Order Ready Message:</label>
                    <textarea placeholder="Pickup Text Order Ready Message" rows="4" class="form-control" name="orderReady">{{ $siteConfig->orderReady ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                


                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Customer Registration Message (Welcome Email):</label>
                    <textarea placeholder="Customer Registration Message" rows="4" class="form-control" name="welcomeEmail">{{ $siteConfig->welcomeEmail ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                @if ( $domain->type !=9 )
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Alert (Non-Card Processed Orders Only):</label>
                    <textarea placeholder="OrderAlert Message" rows="4" class="form-control" name="orderAlert">{{ $siteConfig->orderAlert ?? '' }}</textarea>
                </div>
                @endif

                {{ csrf_field() }}

                <div class="form-group m-t-lg">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <a href="/admin/customer/view" class="btn btn-white text-center" type="submit">Cancel</a>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </form>

        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


<script>


$( document ).ready(function() {
  console.log("ready");
  $("#phone").mask("(999) 999-9999");
});


</script>




<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection