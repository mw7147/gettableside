@extends('admin.admin')

@section('content')
    
<!-- Page-Level CSS -->

<h1>Reset User Password</h1>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>


<ul class="instructions">
  <li>Type in new password.</li>
</ul>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h3><i>System User Name: </i></h3>
    </div>
        
    <div class="ibox-content">

      <div class="row">


      <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
      <a href="/admin/systemuser/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>System Users</a>

      <div class="clearfix"></div>

      @if(Session::has('updateSuccess'))
      <div class="alert alert-success alert-dismissable col-md-6 col-sm-12 col-xs-12 m-b-xl m-t-md">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          {{ Session::get('updateSuccess') }}
      </div>
      <div class="clearfix"></div>
      @endif

      <form name="passwordReset" method="POST" id="passwordReset" action="/admin/systemuser/password/{{ $userID }}">

          <div class="col-md-6 col-sm-12 col-xs-12 m-t-sm">
              <label>Password (min 6 characters)<sup class="text-danger m-l-xs">*</sup></label>
              <input class="form-control m-b-md" type="password" id="password1" minlength="6" name="password1" value="" required placeholder="Password">


              <label>Confirm Password<sup class="text-danger m-l-xs">*</sup></label>
              <input class="form-control" type="password" id="password2" minlength="6" name="password2" value="" required placeholder="Confirm Password">
          </div>

          <div class="clearfix"></div>

          <div class="col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2 col-sm-6 col-sm-offset-2 col-xs-10 col-xs-offset-1 m-t-md">

              {{ csrf_field() }}
             
              
              <div id="returnMenu">
                <a href="/admin/systemuser/view" class="btn btn-default w150 m-t-xs"><i class="fa fa-times m-r-sm"></i>Cancel</a>

                <button type="submit" class="btn btn-primary w150 m-t-xs" name="updatePassword" id="updatePassword"><i class="fa fa-check m-r-sm"></i>Update Password</button>
              </div>
          </div>
      </form>



    </div>

    </div>
  </div>
</div>


<!-- Page Level Scripts -->
<script>

$('#updatePassword').click(function(e) {

  var p1 = $('#password1').val();
  var p2 = $('#password2').val();
  if ( p1 != p2 || p1 == '') {
    alert("There is an error with your passwords. Please retype your passwords.")
    $('#password1').val('');
    $('#password2').val('');
    e.preventDefault();
    return false;
  }
 
});

</script>

<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection