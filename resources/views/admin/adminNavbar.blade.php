<!-- NAVBAR -->

    <nav class="navbar-default navbar-static-side" role="navigation" >
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold lite-color font15px">{{ Auth::user()->fname }} {{ Auth::user()->lname }}</strong>
                             </span>

                        <hr class="hr-small"/>

                        <span class="text-muted text-xs block">{{ $domain['name'] }}</span>
                            
                    </div>
                    <div class="logo-element">
                        KIT
                    </div>
                </li>

                    <li @if($breadcrumb[0] == 'dashboard')class="active"@endif>
                        <a href="/admin/dashboard"><i class="fa fa-dashboard iconmenu"></i> <span class="nav-label">Dashboard</span></a>
                    </li>

                    <li @if($breadcrumb[0] == 'customer')class="active"@endif>
                        <a href=""><i class="fa fa-address-book iconmenu"></i> <span class="nav-label">Customers</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'view')class="active"@endif><a href="/admin/customer/view">View / Edit</a></li>
                            <li @if($breadcrumb[1] == 'new')class="active"@endif><a href="/admin/customer/create">Add New</a></li>
                        </ul>
                    </li>

                    <li @if($breadcrumb[0] == 'systemuser')class="active"@endif>
                        <a href=""><i class="fa fa-user iconmenu"></i> <span class="nav-label">System Users</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'view')class="active"@endif><a href="/admin/systemuser/view">View / Edit</a></li>
                            <li @if($breadcrumb[1] == 'new')class="active"@endif><a href="/admin/systemuser/new">Add New</a></li>
                        </ul>
                    </li>
                   {{--
                    <li @if($breadcrumb[0] == 'contacts')class="active"@endif>
                        <a href="/contacts/{{Request::get('urlPrefix')}}/view"><i class="fa fa-address-card iconmenu"></i> <span class="nav-label">Contacts</span> </a>
                   </li>
                   
                    <li @if($breadcrumb[0] == 'groups')class="active"@endif>
                        <a href="/groups/{{Request::get('urlPrefix')}}/view"><i class="fa fa-users iconmenu"></i> <span class="nav-label">Groups</span> </a>
                   </li>
                    --}}

                    <li @if($breadcrumb[0] == 'menu')class="active"@endif>
                        <a href="/menu/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-cutlery iconmenu"></i> <span class="nav-label">Menus</span></a>
                    </li>

                    <li @if($breadcrumb[0] == 'orders')class="active"@endif>
                        <a href="/{{Request::get('urlPrefix')}}/orderadmin/view"><i class="fa fa-pencil-square iconmenu"></i> <span class="nav-label">Orders</span></a>
                    </li>

                    {{--
                    <li @if($breadcrumb[0] == 'contact')class="active"@endif>
                        <a href="https://dashboard.stripe.com" target="_blank"><i class="fa fa-cc-stripe iconmenu"></i> <span class="nav-label">Billing</span></a>
                    </li>
                    --}}

                    <li @if($breadcrumb[0] == 'reports')class="active"@endif>
                        <a href="/reports/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-file-text iconmenu"></i> <span class="nav-label">Reports</span></span></a>
                   </li>

                    <li @if($breadcrumb[0] == 'settings')class="active"@endif>
                        <a href="/settings/{{Request::get('urlPrefix')}}/dashboard"><i class="fa fa-cog iconmenu"></i> <span class="nav-label">Settings</span></span></a>
                   </li>


            </ul>

        </div>
    </nav>