@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h1>Edit Customer Site Payment Information</h1>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<h4>
        {{ $domain->name }} Customer Site Payment Information.
</h4>

<ul class="m-b-md instructions">
    <li>Fill in the style as necessary.</li>
    <li>Press "Cancel" or select a menu item to return to the Customer List View without updating the Payment Information.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>{{ $domain ->name }} Customer ID: {{ $domain->id }}</i></h5>
    </div>
        
    <div class="ibox-content">


    <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/admin/customer/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>Customers</a>
    <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif


        <div class="clearfix"></div>
        <div class="row">
            <form name="editSyle" id="editSyle" method="POST" action="/admin/customer/paymentconfig/{{ $domain->id }}">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Payment Information</p>
                </div>

                <div class="clearfix"></div>

                   <div class="col-md-4 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic">Account Name:</label>
                        <input class="form-control" type="text" name="accountName" value="{{ $paymentConfig->accountName ?? '' }}" required placeholder="Account Name">
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <label class="font-normal font-italic">Credit Card Processor:</label>
                        <select class="form-control" name="ccType" required>

                            @foreach ($ccTypes as $cc)
                            @if ($cc->active)
                            <option value="{{ $cc->id }}" @isset ($paymentConfig->ccType) @if ( $cc->id == intval($paymentConfig->ccType) ) selected @endif @endisset>{{ $cc->description ?? '' }}</option>
                            @endif
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <label class="font-normal font-italic">Credit Card Mode:</label>

                        <select class="form-control" name="mode" required>
                            <option value="live" @isset($paymentConfig->mode) @if ($paymentConfig->mode == "live") selected @endif @endisset>Live</option>
                            <option value="test" @isset($paymentConfig->mode) @if ($paymentConfig->mode == "test") selected @endif @endisset>Test</option>
                        </select>

                        </select>
                    </div>

                <div class="clearfix"></div>

                <hr>



                <div class="col-md-12">
                    <p class="font-italic font-bold">getTableSide Payment Processing</p>
                </div>


                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Merchant ID:</label>
                    <input type="text" placeholder="getTableSide Merchant ID" class="form-control" name="togoMerchantID" value="{{ $paymentConfig->togoMerchantID ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide ACH Merchant ID:</label>
                    <input type="text" placeholder="getTableSide ACH Merchant ID" class="form-control" name="togoACHMerchantID" value="{{ $paymentConfig->togoACHMerchantID ?? '' }}">
                </div>

                {{--
                <div class="clearfix"></div>

                 <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Test User:</label>
                    <input type="text" placeholder="getTableSide Test User" class="form-control" name="togoTestUser" value="{{ $paymentConfig->togoTestUser ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Test Password:</label>
                    <input type="text" placeholder="getTableSide Test Password" class="form-control" name="togoTestPassword" value="@if (!empty($paymentConfig->togoTestPassword)){{decrypt($paymentConfig->togoTestPassword)}}@endif">
                </div>

                --}}

                <div class="clearfix"></div>



                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Live User:</label>
                    <input type="text" placeholder="getTableSide Live User" class="form-control" name="togoLiveUser" value="{{ $paymentConfig->togoLiveUser ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Live Password:</label>
                    <input type="text" placeholder="getTableSide Live Password" class="form-control" name="togoLivePassword" value="@if (!empty($paymentConfig->togoLivePassword)){{decrypt($paymentConfig->togoLivePassword)}}@endif">
                </div>
                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Monthly Processing Fee:</label>
                    <input type="text" placeholder="getTableSide Month Processing Fee" class="form-control" name="togoMonthlyProcessingFee" value="{{ number_format($paymentConfig->togoMonthlyProcessingFee, 2) ?? '' }}">
                </div>

                <div class="col-md-3 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Transaction Percent:</label>
                    <input type="text" placeholder="getTableSide Transaction Percent" class="form-control" name="togoTransactionPercent" value="{{ number_format($paymentConfig->togoTransactionPercent, 2) ?? '' }}">
                </div>

                <div class="col-md-3 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">getTableSide Transaction Fee:</label>
                    <input type="text" placeholder="getTableSide Transaction Fee" class="form-control" name="togoTransactionFee" value="{{ number_format($paymentConfig->togoTransactionFee, 2) ?? '' }}">
                </div>

                <div class="clearfix"></div>

                


                @if ($ccTypes[2]->active)
                <hr>
                <div class="col-md-12">
                    <p class="font-italic font-bold">Stripe Information</p>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Live Secret Key:</label>
                    <input type="text" placeholder="Stripe Live Secret Key" class="form-control" name="stripeLiveSecretKey" value="@if (!empty($paymentConfig->stripeLiveSecretKey)){{decrypt($paymentConfig->stripeLiveSecretKey)}}@endif">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Live Publishable Key:</label>
                    <input type="text" placeholder="Stripe Live Publishable Key" id="phone" class="form-control" name="stripeLivePublishableKey" value="{{ $paymentConfig->stripeLivePublishableKey ?? '' }}">
                </div>
                

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Test Secret Key:</label>
                    <input type="text" placeholder="Stripe Test Secret Key" class="form-control" name="stripeTestSecretKey" value="@if (!empty($paymentConfig->stripeTestSecretKey)){{decrypt($paymentConfig->stripeTestSecretKey)}}@endif">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Test Publishable Key:</label>
                    <input type="text" placeholder="Stripe Publishable Secret Key" class="form-control" name="stripeTestPublishableKey" value="{{ $paymentConfig->stripeTestPublishableKey ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Transaction Percent:</label>
                    <input type="text" placeholder="Stripe Transaction Percent" class="form-control" name="stripeTransactionPercent" value="{{ number_format($paymentConfig->stripeTransactionPercent, 2) ?? ''}}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Transaction Fee:</label>
                    <input type="text" placeholder="Stripe Transaction Fee" class="form-control" name="stripeTransactionFee" value="{{ number_format($paymentConfig->stripeTransactionFee, 2) ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Statement Descriptor:</label>
                    <input type="text" placeholder="Stripe Statement Descriptor" maxlength="19" class="form-control" name="stripeStatementDescriptor" value="{{ $paymentConfig->stripeStatementDescriptor ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Stripe Data Image (Payment Form Logo):</label>
                    <div class="input-group m-b"><span class="input-group-btn">
                        <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 20%;" >Change</button>
                        <input type="text" class="form-control" id="sdi" style="max-width: 60%;" name="stripeDataImage" value="{{ $paymentConfig->stripeDataImage ?? '' }}">
                        <img class="m-l-md" style="max-width: 10%;" id="logo" @isset($paymentConfig->stripeDataImage) src="{{ '/storage/logoImages/' . $domain->id . '/' . $paymentConfig->stripeDataImage }}" @endisset>
                        <input type="file" id="newLogo" style="display: none;">
                    </div>
                </div>

                <div class="clearfix"></div>


                @endif


                @if($ccTypes[3]->active)

                <hr>
                <div class="col-md-12">
                    <p class="font-italic font-bold">Authorize.Net Information</p>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Authorize.net Test User:</label>
                    <input type="text" placeholder="Authorize.net Test User" class="form-control" name="authNetTestUser" value="{{ $paymentConfig->authNetTestUser ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Authorize.net Test Password:</label>
                    <input type="text" placeholder="Authorize.net Test Password" class="form-control" name="authNetTestPass" value="@if (!empty($paymentConfig->authNetTestPass)){{decrypt($paymentConfig->authNetTestPass)}}@endif">
                </div>

                 <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Authorize.net Live User:</label>
                    <input type="text" placeholder="Authorize.net Live User" class="form-control" name="authNetLiveUser" value="{{ $paymentConfig->authNetLiveUser ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Authorize.net Live Password:</label>
                    <input type="text" placeholder="Authorize.net Live Password" id="phone" class="form-control" name="authNetLivePass" value="@if (!empty($paymentConfig->authNetLivePass)){{decrypt($paymentConfig->authNetLivePass)}}@endif">
                </div>

                <div class="clearfix"></div>
                @endif

                {{ csrf_field() }}
                <input type="hidden" name="existingName" id="existingName" value="{{$paymentConfig->stripeDataImage ?? '' }}">
                <div class="form-group m-t-lg">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <a href="/admin/customer/view" class="btn btn-white text-center" type="submit">Cancel</a>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </form>

        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->


<script>


    $("#changeLogoButton").on("click", function() {
        $("#newLogo").trigger("click");
    });


    $('#newLogo').on('change',function(){
        
        var sdi = this.files[0];
        var domainID = {{$domain->id}};
        // check filesize
        if (sdi.size > 50000) {
            alert("The filesize cannot be larger than 50Kb. Please check your file.");
            $('#stripeDataImage').val('');
            return false;
        }
        // check filetype
        if (sdi.type != "image/png") {
            alert("The image must be a png. Please check your file.");
            $('#stripeDataImage').val('');
            return false;
        }

        var existingName = $("#existingName").val();
        var fd = new FormData();    
        fd.append('sdi', sdi);
        fd.append('domainID', domainID);
        fd.append('existingName', existingName)

        $.ajax({
            url: "/api/v1/admin/stripedataimage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $('#sdi').val('');
                    alert(jsondata['message']); 
                    return false;     
                } else {
                    $('#sdi').val(jsondata['fileName']);
                    src='/storage/logoImages/' + domainID + '/' + jsondata['fileName'];
                    $('#logo').attr("src", src);
                    $('#existingName').val(jsondata['fileName']);
                    return true;    
            }}
        });

    });


    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection