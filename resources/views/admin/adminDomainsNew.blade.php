@extends('kitAdmin.kitAdmin')


@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">


<h1>Create New Domain</h1>

<h4>
        Required Domain information is displayed below.
</h4>

<ul class="m-b-md">
    <li>Fill in the domain information as necessary.</li>
    <li>The Http Host field is the URL of the domain.</li>
    <li>The background image is the background image on the home page.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without creating a new domain.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Domain</i></h5>
    </div>
        
    <div class="ibox-content">
        
        <div class="input-group m-b col-xs-10 col-sm-9 col-md-8">

            @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
            <div class="clearfix"></div>
        @endif
        </div>


        <div class="row">
            <form name="newDomain" id="newDomain" method="POST" action="/kitadmin/domains/new/save" enctype="multipart/form-data">
        

                <div class="col-xs-8 col-sm-7 m-b-sm">

                    <span class="form-control-static"> <label class="control-label"><i class="m-r-sm">Required Information:</i></label></span>
                
                </div>

                <hr class="hr-line-dashed m-b-lg">
                <div class="clearfix"></div>
              
                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
                
                    <span class="input-group-addon w150">Status</span>
                    <select class="form-control m-b il-b" style="height: 32px;" name="active" required>

                        <option value="yes">Active</option>
                        <option value="no">Inactive</option>
                
                    </select>
                </div>
     
                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
                
                    <span class="input-group-addon w150">Domain Name</span>
                    <input type="name" placeholder="Domain Name" class="form-control" required name="name" value="">

                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">

                    <span class="input-group-addon w150">Subdomain</span>
                    <input type="text" placeholder="Subdomain" class="form-control" required name="subdomain" value="">

                </div>

               <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">

                    <span class="input-group-addon w150">Http Host</span>
                    <input type="text" placeholder="Site URL - site.keepintext.biz" class="form-control" name="httpHost"  required id="httpHost" value="">

                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
                
                    <span class="input-group-addon w150">Sitename</span>
                    <input type="text" placeholder="Sitename" class="form-control" required name="sitename" value="">

                </div>
               


                <div class="input-group col-xs-10 col-sm-9 col-md-6 m-b-xs nodisplay">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                    
                        <span class="input-group-addon btn btn-default btn-file w150">
                            <span class="fileinput-new">Logo File</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="logo" />
                        </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists m-l-sm" data-dismiss="fileinput">Remove</a>

                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                    </div>
                </div> 




                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 nodisplay">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                    
                        <span class="input-group-addon btn btn-default btn-file w150">
                            <span class="fileinput-new">Background Image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="picture" />
                        </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists m-l-sm" data-dismiss="fileinput">Remove</a>

                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                    </div>
                </div> 


                <div class="clearfix"></div>



                <div class="form-group m-t-lg">
                    <div class="col-sm-8 col-sm-offset-1">
                        
                            {{ csrf_field() }}
                        <a href="/kitadmin/domains/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Create New Domain</button>

                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>

$('#httpHost').on('change', function() {  // user name check
    var httpHost = document.getElementById("httpHost").value;
    var fd = new FormData();    
    fd.append('httpHost', httpHost);

    $.ajax({
        url: "/api/v1/checkhttphost",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#httpHost").val("");
                $("#httpHost").focus();
                alert("The httpHost URL is used by another Domain. This URL must be unique. Please use a different URL for the httpHost address."); 
                return false;   
                    
            } else {
                
            return true;    
        }}
    });
});

</script>




@endsection