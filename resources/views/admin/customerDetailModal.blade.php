<!-- Customer Detail Modal -->

<style>

.modalHeading {
  font-size: 14px;
  font-style: italic;
}

.custDetail p {
  margin-bottom: 10px;
  font-weight: 600;
}

.custDetail small {
  font-style: italic;
  width: 100%;
  color: #666;
  font-size: 80%;
  text-decoration: underline;
}

.custDetail img {
  margin-top: 8px;
  max-width: 90%;
}

</style>

<div class="clearfix"></div>

<!-- Modal -->

  <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="customerDetail">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title" id="name"></h3>
        </div>

        <div class="modal-body custDetail">

          <small >Name:</small>
          <p id="name1"></p>
           
          <small >Customer ID:</small>
          <p id="cid"></p>

          <small >Active:</small>
          <p id="active"></p>

          <small >Customer Type:</small>
          <p id="type"></p>

          <small >Parent Customer ID:</small>
          <p id="pid"></p>

          <small >Owner User ID:</small>
          <p id="owner"></p>

          <small >TGM Subdomain:</small>
          <p id="subdomain"></p>

          <small >Http Host URL:</small>
          <p id="host"></p>

          <small >Sitename:</small>
          <p id="sitename"></p>

          <small >Logo Image:</small><br>
          <img id="logo" src="">
          <br><br>
          <small >Background Image:</small><br>
          <img id="background" src="">


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-success w100" data-dismiss="modal">Done</button>     
        </div>

      </div>
    </div>
  </div>

<script>

  function showDetail(customerID) {

      var fd = new FormData();
      fd.append('customerID', customerID),    

      $.ajax({
          url: "/api/v1/admin/customer/detail",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
              $('#name').html(mydata['name'] + " Detail");
              $('#cid').html(mydata['id']);
              $('#active').html(mydata['active']);
              $('#type').html(mydata['type']);
              $('#pid').html(mydata['parentDomainID']);
              $('#name1').html(mydata['name']);
              $('#owner').html(mydata['ownerID']);
              $('#subdomain').html(mydata['subdomain']);
              $('#host').html(mydata['httpHost']);
              $('#sitename').html(mydata['sitename']);
              $('#logo').attr('src', mydata['logoPublic']);
              $('#background').attr('src', mydata['backgroundPublic']);
              $('#detail').modal('show');
          },
          error: function() {
              alert("There was an problem retrieving the information. Please try again.");
          }
    }); 

  };

</script>

<!-- end Customer Detail Modal -->