<!-- NAVBAR -->

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element">
                        <span class="clear"></span> <span class="block m-t-xs"> <strong class="font-bold lite-color font15px">{{ Auth::user()->fname }} {{ Auth::user()->lname }}</strong>
                             </span>

                        <hr class="hr-small"/>

                        <span class="text-muted text-xs block">{{ $domain['name'] }}</span>
                            
                    </div>
                    <div class="logo-element">
                        NIH
                    </div>
                </li>






                    @if (Auth::user()->role_auth_level >= 80 ) 
                    <li>
                        <a href="http://cloud.keepintext.biz/kitadmin/dashboard" style="color: white;"><i class="fa fa-rocket iconmenu"></i> <span class="nav-label">Return to RHIT admin</span></a>
                    </li>
                    <hr>
                    @endif


                    <li @if($breadcrumb[0] == 'dashboard')class="active"@endif>
                        <a href="/kitadmin/dashboard"><i class="fa fa-dashboard iconmenu"></i> <span class="nav-label">Dashboard</span></a>
                    </li>

                    <li @if($breadcrumb[0] == 'domains')class="active"@endif>
                        <a href="#"><i class="fa fa-map-marker iconmenu"></i> <span class="nav-label">Domains</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li @if($breadcrumb[1] == 'view')class="active"@endif><a href="/kitadmin/domains/view">View / Edit</a></li>
                            <li @if($breadcrumb[0] == 'new')class="active"@endif><a href="/kitadmin/domains/new/domain">Add New</a></li>
                        </ul>
                    </li>

<!--
                    <li @if($breadcrumb[0] == 'customers')class="active"@endif>
                        <a href="/kitadmin/client/view"><i class="fa fa-building iconmenu"></i> <span class="nav-label">Customers</span> <span class="fa arrow"></span></a>
                    </li>
-->
                    <li @if($breadcrumb[0] == 'systemUsers')class="active"@endif>
                        <a href="#"><i class="fa fa-male iconmenu"></i> <span class="nav-label">System Users</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li @if($breadcrumb[1] == 'view')class="active"@endif><a href="#">View / Edit</a></li>
                            <li @if($breadcrumb[0] == 'new')class="active"@endif><a href="#">Create New</a></li>
                        </ul>
                    </li>
<!--
                    <li @if($breadcrumb[0] == 'subscribers')class="active"@endif>
                        <a href="/kitadmin/subscribers/view"><i class="fa fa-phone-square iconmenu"></i> <span class="nav-label">Subscribers</span> <span class="fa arrow"></span></a>
                    </li>



                   <li>
                        <a href="#"><i class="fa fa-area-chart iconmenu"></i> <span class="nav-ticket">Reports</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                         <li><a href="#">Text Reporting</a></li>
                            <li><a href="#">Page View Reporting</a></li>
                          </ul>
                    </li>
-->
                   <li @if($breadcrumb[0] == 'myinfo')class="active"@endif>
                        <a href="#"><i class="fa fa-star iconmenu"></i> <span class="nav-ticket">My Information</span></a>
                    </li>
    <!--                
                   <li>
                        <a href="#"><i class="fa fa-cog iconmenu"></i> <span class="nav-ticket">Settings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="index.html">View</a></li>
                            <li><a href="dashboard_2.html">Add New</a></li>
                            <li><a href="dashboard_3.html">Update</a></li>
                            <li><a href="dashboard_4_1.html">Delete</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-question-circle iconmenu"></i> <span class="nav-ticket">Help</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="index.html">FAQ</a></li>
                            <li><a href="dashboard_2.html">Working with Clients</a></li>
                            <li><a href="dashboard_2.html">Creating Client Events</a></li>
                            <li><a href="dashboard_2.html">Managing Interests</a></li>
                            <li><a href="dashboard_3.html">Managing Contacts</a></li>
                            <li><a href="dashboard_4_1.html">Sending Text Messages</a></li>
                            <li><a href="dashboard_4_1.html">Sending Email Messages</a></li>
                            <li><a href="dashboard_4_1.html">Reporting Systems</a></li>

                        </ul>
                    </li>
-->
                    <li>
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>  Logout</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>


            </ul>





        </div>

    </nav>