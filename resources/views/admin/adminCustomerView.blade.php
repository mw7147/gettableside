@extends('admin.admin')

@section('content')
    <!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h1>Customer Administration</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>
	
    <h4>
		Customers are listed below.
	</h4>

	<ul class="instructions">
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
		<li>Press the "Print" button to print the results.</li>
	</ul>

<div class="ibox-content">

<h3>{{ $domain['name']}} Customer List</h3><hr>

        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif
    
    <a href="/admin/customer/create" class="btn btn-primary btn-xs m-l-xs m-b-md"><i class="fa fa-plus m-r-xs"></i>Create New Customer</a>

    <div class="table-responsive">
        <table class="table table-striped dataTables" >
            <thead>
                <tr>
                    <th width="30">Customer ID</th>
                    <th width="30">Active</th>
                    <th width="70">Name</th>
                    <th width="70">Owner Name</th>
                    <th width="70">Site Type</th>
                    <th width="90">URL Link</th>
                    <th width="300">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($agents as $agent)
                <tr>                
                    <td>{{ $agent->id }}</td>
                    <td>{{ $agent->active }}</td>
                    <td>{{ $agent->sitename }}</td>
                    <td>{{ $agent->fname }} {{$agent->lname }}</td>
                    <td>{{ $agent->description }}</td>  
                    <td><a href ="http://{{ $agent->httpHost }}" target="_blank">{{ $agent->httpHost }}</a></td> 
                    <td>
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="showDetail('{{ $agent->id }}')"><i class="fa fa-binoculars m-r-xs"></i>Details</button>
                        <a href="/admin/customer/edit/{{ $agent->id }}" class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-pencil m-r-xs"></i>Edit</a>
                        <a href="/admin/customer/siteconfig/{{ $agent->id }}" class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-pencil m-r-xs"></i>Options</a>
                        @if ( $agent->type < 8 )
                        <a href="/admin/customer/paymentconfig/{{ $agent->id }}" class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-pencil m-r-xs"></i>Payments</a>
                        <a href="/admin/customer/style/{{ $agent->id }}" class="btn btn-success btn-xs w90 m-r-xs m-b-xs"><i class="fa fa-pencil m-r-xs"></i>Styles</a>
                        @endif
                        @if ( $agent->id > 1 )
                        <a class="btn btn-xs w90 btn-danger m-b-xs"  href="/admin/customer/delete/{{ $agent->id }}" alt="delete" onclick="javascript:return confirm('Are you sure you want to delete this customer? ALL customer data (including menus) will be permanently deleted.')"><span class="far fa-trash-alt mr-2"></span>Delete</a>
                        @endif

                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

@include('admin.customerDetailModal')

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            responsive: true,
            order: [[ 0, "asc" ]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });
</script>

<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection