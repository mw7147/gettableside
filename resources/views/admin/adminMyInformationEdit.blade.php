@extends('kitAdmin.kitAdmin')


@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">



<h1>My Information</h1>

<h4>
        My Information is displayed below.
</h4>

<ul class="m-b-md">
    <li>Update the information as necessary.</li>
    <li>The email address will be used as the username on the login screen and must be unique to the system.</li>
    <li>The password may be changed on the password reset page.</li>
    <li>Press "Update Information" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Name:</i> {{ $users['fname'] }} {{$users['lname'] }}</h5>
    </div>
        
    <div class="ibox-content">


<form name="updateMyInformation User" method="POST" action="/nihadmin/myinformation">
    


    <div class="row">
        <div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1 m-t-md">

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-6 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-6 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif

        <div class="clearfix"></div>
        @if(Session::has('mobileError'))
            <div class="alert alert-warning alert-dismissable col-xs-10 col-sm-6 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('mobileError') }}
            </div>
        @endif





            <div class="clearfix"></div>

            <div class="form-group">

                <div class="col-xs-10 col-sm-5 col-md-3">

                    <span class="form-control-static"> <label class="control-label"><i class="m-r-sm">System User ID:</i></label>{{ $users['id'] }}</span>
            
                </div>

<!--

                <div style="height: 36px;" class="visible-xs"></div>
               
                <div class="col-xs-10 col-sm-7 col-md-5" style="margin-top: -5px;">
                    <span class="input-group-addon il-b" style="margin-top: -3px;">Status</span> 
                    <select class="form-control m-b il-b form-select-addon" style="height: 28px;" name="active">

                        <option value="yes">Active</option>
                        <option value="no">Inactive</option>
                
                    </select>
                </div>
-->

            </div>
        <div>
    </div>

    <div class="clearfix"></div>

    <hr class="hr-line-dashed m-b-lg">
    
    
        <div class="col-xs-8 col-sm-7 m-b-sm">

            <span class="form-control-static"> <label class="control-label"><i class="m-r-sm">Required Information:</i></span>
            
        </div>

        <div class="clearfix"></div>



    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">Email</span>
        <input type="email" placeholder="Email Address" class="form-control" required name="email" id="email" value="{{ $users['email'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">

        <span class="input-group-addon">First Name</span>
        <input type="text" placeholder="System User Name" class="form-control" name="fname" value="{{ $users['fname'] }}">

    </div>

   <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">

        <span class="input-group-addon">Last Name</span>
        <input type="text" placeholder="System User Name" class="form-control" name="lname" value="{{ $users['lname'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">Mobile</span>
        <input type="text" placeholder="Mobile Phone" class="form-control" name="mobile" data-mask="(999) 999-9999" required min="2011000000" max="9999999999" data-validation-required-message="Please enter your 10 digit mobile number" value="{{ $usersData['mobile'] }}">

    </div>




    <div class="clearfix"></div>

    <hr class="hr-line-dashed m-b-lg">
    
    
        <div class="col-xs-8 col-sm-7 m-b-sm">

            <span class="form-control-static"> <label class="control-label"><i class="m-r-sm">Contact Information:</i></span>
            
        </div>

        <div class="clearfix"></div>

   <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">

        <span class="input-group-addon">Title</span>
        <input type="text" placeholder="Title" class="form-control" name="title" value="{{ $usersData['title'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">Office Phone</span>
        <input type="text" placeholder="Office Phone" class="form-control" name="office" data-mask="(999) 999-9999" min="2011000000" max="9999999999" data-validation-required-message="Please enter your 10 digit office number" value="{{ $usersData['office'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">Address 1</span>
        <input type="text" placeholder="Address 1" class="form-control" name="address1" value="{{ $usersData['address1'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">Address 2</span>
        <input type="text" placeholder="Address 2" class="form-control" name="address2" value="{{ $usersData['address2'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">City</span>
        <input type="text" placeholder="City" class="form-control" name="city" value="{{ $usersData['city'] }}">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">State</span>

            <select name="state" class="form-control">
                <option value="">-- Select State --</option>
                <option @if($usersData['state'] == 'AL')selected @endif value="AL">Alabama</option>
                <option @if($usersData['state'] == 'AK')selected @endif value="AK">Alaska</option>
                <option @if($usersData['state'] == 'AZ')selected @endif value="AZ">Arizona</option>
                <option @if($usersData['state'] == 'AK')selected @endif value="AR">Arkansas</option>
                <option @if($usersData['state'] == 'CA')selected @endif value="CA">California</option>
                <option @if($usersData['state'] == 'CO')selected @endif value="CO">Colorado</option>
                <option @if($usersData['state'] == 'CT')selected @endif value="CT">Connecticut</option>
                <option @if($usersData['state'] == 'DE')selected @endif value="DE">Delaware</option>
                <option @if($usersData['state'] == 'DC')selected @endif value="DC">District of Columbia</option>
                <option @if($usersData['state'] == 'FL')selected @endif value="FL">Florida</option>
                <option @if($usersData['state'] == 'GA')selected @endif value="GA">Georgia</option>
                <option @if($usersData['state'] == 'HI')selected @endif value="HI">Hawaii</option>
                <option @if($usersData['state'] == 'ID')selected @endif value="ID">Idaho</option>
                <option @if($usersData['state'] == 'IL')selected @endif value="IL">Illinois</option>
                <option @if($usersData['state'] == 'IN')selected @endif value="IN">Indiana</option>
                <option @if($usersData['state'] == 'IA')selected @endif value="IA">Iowa</option>
                <option @if($usersData['state'] == 'KS')selected @endif value="KS">Kansas</option>
                <option @if($usersData['state'] == 'KY')selected @endif value="KY">Kentucky</option>
                <option @if($usersData['state'] == 'LA')selected @endif value="LA">Louisiana</option>
                <option @if($usersData['state'] == 'ME')selected @endif value="ME">Maine</option>
                <option @if($usersData['state'] == 'MD')selected @endif value="MD">Maryland</option>
                <option @if($usersData['state'] == 'MA')selected @endif value="MA">Massachusetts</option>
                <option @if($usersData['state'] == 'MI')selected @endif value="MI">Michigan</option>
                <option @if($usersData['state'] == 'MN')selected @endif value="MN">Minnesota</option>
                <option @if($usersData['state'] == 'MS')selected @endif value="MS">Mississippi</option>
                <option @if($usersData['state'] == 'MI')selected @endif value="MO">Missouri</option>
                <option @if($usersData['state'] == 'MT')selected @endif value="MT">Montana</option>
                <option @if($usersData['state'] == 'NE')selected @endif value="NE">Nebraska</option>
                <option @if($usersData['state'] == 'NV')selected @endif value="NV">Nevada</option>
                <option @if($usersData['state'] == 'NH')selected @endif value="NH">New Hampshire</option>
                <option @if($usersData['state'] == 'NJ')selected @endif value="NJ">New Jersey</option>
                <option @if($usersData['state'] == 'NM')selected @endif value="NM">New Mexico</option>
                <option @if($usersData['state'] == 'NY')selected @endif value="NY">New York</option>
                <option @if($usersData['state'] == 'NC')selected @endif value="NC">North Carolina</option>
                <option @if($usersData['state'] == 'ND')selected @endif value="ND">North Dakota</option>
                <option @if($usersData['state'] == 'OH')selected @endif value="OH">Ohio</option>
                <option @if($usersData['state'] == 'OK')selected @endif value="OK">Oklahoma</option>
                <option @if($usersData['state'] == 'OR')selected @endif value="OR">Oregon</option>
                <option @if($usersData['state'] == 'PA')selected @endif value="PA">Pennsylvania</option>
                <option @if($usersData['state'] == 'RI')selected @endif value="RI">Rhode Island</option>
                <option @if($usersData['state'] == 'SC')selected @endif value="SC">South Carolina</option>
                <option @if($usersData['state'] == 'SD')selected @endif value="SD">South Dakota</option>
                <option @if($usersData['state'] == 'TN')selected @endif value="TN">Tennessee</option>
                <option @if($usersData['state'] == 'TX')selected @endif value="TX">Texas</option>
                <option @if($usersData['state'] == 'UT')selected @endif value="UT">Utah</option>
                <option @if($usersData['state'] == 'VT')selected @endif value="VT">Vermont</option>
                <option @if($usersData['state'] == 'VA')selected @endif value="VA">Virginia</option>
                <option @if($usersData['state'] == 'WA')selected @endif value="WA">Washington</option>
                <option @if($usersData['state'] == 'WV')selected @endif value="WV">West Virginia</option>
                <option @if($usersData['state'] == 'WI')selected @endif value="WI">Wisconsin</option>
                <option @if($usersData['state'] == 'WY')selected @endif value="WY">Wyoming</option>
            </select>

    </div>
    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon">Zip Code</span>
        <input type="text" placeholder="Zip" class="form-control" name="zip" value="{{ $usersData['zip'] }}">

    </div>

    <div class="form-group m-t-lg">
        <div class="col-sm-6 col-sm-offset-1">
        <input type="hidden" name="ud" value="{{ $usersData['usersDataID'] }}">
            
                {{ csrf_field() }}
            <a href="/owner/dashboard" class="btn btn-white" type="submit">Cancel and Return</a>
            <button class="btn btn-success" type="submit">Update Information</button>

        </div>
    </div>


</form>
<!-- Page Level Scripts -->

<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>

$('#email').on('change', function() {  // user name check
    var email = document.getElementById("email").value;
    var fd = new FormData();    
    fd.append('email', email);

    $.ajax({
        url: "/api/v1/checkuseremail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert("The email address is already registered. Please use a different email address."); 
                return false;   
                    
            } else {
                
            return true;    
        }}
    });
});


</script>

@endsection