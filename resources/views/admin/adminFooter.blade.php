<!-- Footer -->
<div class="clearfix" style="height: 24px;"></div>
<div class="footer">
            <div class="pull-right">
                <a href="http://www.rhit.us" target="_blank" alt="Powered by Riverhouse IT">Powered by Riverhouse IT</a>
            </div>
            <div>
                <strong> © {{ date("Y") }}</strong> TokinTechnologies.com - A Tokin Technologies LLC Product
            </div>
        </div>
<!-- End Footer -->
