@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">
<style>.p-l {padding-left: 15px!important;}</style>
<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h1>Create New Customer</h1>

    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<h4>
        Customer Information is displayed below.
</h4>


<ul class="m-b-md instructions">
    <li>Fill in the Customer Information as necessary.</li>
    <li>The System User ID and Parent Customer ID is required. A System User for each is required.</li>
    <li>The Http Host field is the URL of the Customer.</li>
    <li>A Http Host name change requires a corresponding DNS change in order to be visible on the internet.</li>
    <li>The background image is the background image on the home and login pages.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h3><i>Create New Customer</i></h3>
    </div>
        
    <div class="ibox-content">

      <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
      <a href="/admin/customer/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>Customers</a>

        <div class="input-group m-b col-xs-10 col-sm-9 col-md-8 p-l">

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl p-l">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl p-l">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif

        <div class="clearfix"></div>
        @if(Session::has('fileError'))
            <div class="alert alert-warning alert-dismissable col-xs-10 col-sm-8 m-b-xl p-l">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('fileError') }}
            </div>
        @endif
        </div>


        <div class="row">
            <form name="editCustomer" id="editCustomer" method="POST" action="/admin/customer/new" enctype="multipart/form-data">

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Customer Name</span>
                    <input type="text" placeholder="Customer Name" class="form-control" required name="name" value="{{ old('name') }}">
                </div>
              
                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Status</span>
                    <select class="form-control m-b il-b" style="height: 32px;" name="active" required>
                        <option value="yes">Active</option>
                        <option @if(old('active') =="no")selected @endif value="no">Inactive</option>
                    </select>
                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Type</span>
                    <select class="form-control m-b il-b" style="height: 32px;" name="type" required>
                        @foreach ($types as $type)
                        <option @if(old('type') == $type->id)selected @endif value="{{ $type->id }}">{{ $type->description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Owner User ID</span>
                    <input type="number" step="1" placeholder="Optional Owner User ID / Leave Blank If User Not Created Yet" class="form-control" name="ownerID" value="{{ old('ownerID') }}">
                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Parent Customer ID</span>
                    <input type="number" step="1" placeholder="Optional Parent Domain ID" class="form-control" name="parentDomainID" value="{{ old('parentDomainID') }}">
                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Subdomain</span>
                    <input type="text" placeholder="Subdomain" class="form-control" required name="subdomain" value="{{ old('subdomain') }}">
                </div>

               <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Http Host</span>
                    <input type="text" placeholder="Site URL - subdomain.togomeals.biz" class="form-control" name="httpHost"  required id="httpHost" value="{{ old('httpHost') }}">
                </div>

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 p-l">
                    <span class="input-group-addon w150">Sitename</span>
                    <input type="text" placeholder="Sitename" class="form-control" required name="sitename" value="{{ old('sitename') }}">
                </div>
               
                <div class="input-group col-xs-10 col-sm-9 col-md-6 m-b-xs nodisplay p-l">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <span class="input-group-addon btn btn-default btn-file w150">
                            <span class="fileinput-new">Customer Logo</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="logo" id="logoChk"/>
                        </span>
                            <a href="#" class="input-group-addon btn btn-default m-l-sm fileinput-exists" data-dismiss="fileinput">Remove</a>
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                    </div>
                </div> 

                <div class="input-group m-b col-xs-10 col-sm-9 col-md-6 nodisplay p-l">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <span class="input-group-addon btn btn-default btn-file w150">
                            <span class="fileinput-new">Background Image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="picture" id="backgroundChk" />
                        </span>
                            <a href="#" class="input-group-addon btn btn-default m-l-sm fileinput-exists" data-dismiss="fileinput">Remove</a>
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                    </div>
                </div> 

                <div class="clearfix"></div>

                <div class="form-group m-t-lg">
                    <div class="col-sm-8 col-sm-offset-1 p-l">

                        {{ csrf_field() }}
                        <a href="/admin/customer/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Create Customer</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(){ 

    $('#httpHost').change(function() {  // user name check
        var httpHost = document.getElementById("httpHost").value;  
        var fd = new FormData();    
        fd.append('httpHost', httpHost);

        $.ajax({
            url: "/api/v1/admin/checkhttphost",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {

                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $("#httpHost").val("");
                    $("#httpHost").focus();
                    alert("The httpHost URL is used by another Domain. This URL must be unique. Please use a different URL for the httpHost address."); 
                    return false;   
                        
                } else {
                    
                return true;    
            }}
        });
    });

});
</script>

<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection