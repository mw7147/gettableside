@extends('admin.admin')

@section('content')

<style>
.db-blue-bg {
    color: white;
}
</style>

<div class="wrapper wrapper-content animated fadeInRight">
            
           <div class="row" style="margin-bottom: 15px; margin-top: -30px;">
                <div class="col-lg-12">
                    <div class="text-center m-t-lg">
                        <h1>
                            Dashboard
                        </h1>
                        <small>
                            getTableSide.com Admin 
                       </small>
                    </div>
                </div>
            </div>

       <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Today</span>
                        <h5>Orders Today</h5>
                    </div>
                    <div class="ibox-content">
                        <h3 class="no-margins">{{ $ordersToday }}</h3>
                        <small>Total Orders</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Today</span>
                        <h5>Revenue Today</h5>
                    </div>
                    <div class="ibox-content" style="padding-bottom: 36px;">
                        <h3 class="no-margins pull-left">${{ number_format($revToday,2) }}</h3>
                        <h3 class="no-margins pull-right">${{ number_format($ccToday,2) }}</h3>
                        <br>
                        <small class="pull-left">Revenue Today</small>
                        <small class="pull-right">CC Fees Today</small>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">This Month</span>
                        <h5>Total Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <h3 class="no-margins">{{ $ordersMonth }}</h3>
                        <small>Orders This Month</small>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">This Month</span>
                        <h5>Total Revenue</h5>
                    </div>
                    <div class="ibox-content" style="padding-bottom: 36px;">
                        <h3 class="no-margins pull-left">${{ number_format($revMonth,2) }}</h3>
                        <h3 class="no-margins pull-right">${{ number_format($ccMonth,2) }}</h3>
                        <br>
                        <small class="pull-left">Revenue This Month</small>
                        <small class="pull-right">CC Fees This Month</small>
                    </div>
                </div>
            </div>

        </div>


<style>
.dashicon:hover { opacity: .75;}
.dashicon-white:hover { opacity: .62;}

.widget {min-height: 226px;}
</style>

<div class="row m-t-xs m-b-lg">

    <a href="/{{Request::get('urlPrefix')}}/customer/create" class="col-xs-6 col-sm-4 col-md-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-plus-square fa-4x"></i>
                <h1 class="m-xs">Customer</h1>
                <h3 class="font-bold no-margins">
                    Create Customer
                </h3>
            </div>
        </div>
    </a>
    
    <a href="/{{Request::get('urlPrefix')}}/customer/view" class="col-xs-6 col-sm-4 col-md-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-address-book fa-4x"></i>
                <h1 class="m-xs">Customers</h1>
                <h3 class="font-bold no-margins">
                    Manage Customers
                </h3>
            </div>
        </div>
    </a>

    <a href="/{{Request::get('urlPrefix')}}/systemuser/view" class="col-xs-6 col-sm-4 col-md-3 dashicon" >
        <div class="widget lazur-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-user fa-4x"></i>
                <h1 class="m-xs">System Users</h1>
                <h3 class="font-bold no-margins">
                   Manage System Users
                </h3>
            </div>
        </div>
    </a>

 {{--
    <a href="/groups/{{Request::get('urlPrefix')}}/view" class="col-xs-6 col-sm-4 col-md-3 dashicon" >
        <div class="widget db-blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-address-card fa-4x"></i>
                <h1 class="m-xs">Contacts</h1>
                <h3 class="font-bold no-margins">
                    Manage Customer Contacts
                </h3>
            </div>
        </div>
    </a>
   
    <a href="/groups/{{Request::get('urlPrefix')}}/view" class="col-xs-6 col-sm-4 col-md-3 dashicon" >
        <div class="widget db-blue-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-users fa-4x"></i>
                <h1 class="m-xs">Groups</h1>
                <h3 class="font-bold no-margins">
                    Manage Contact Groups
                </h3>
            </div>
        </div>
    </a>
    --}}

    <a href="/menu/{{Request::get('urlPrefix')}}/dashboard" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon">
        <div class="widget yellow-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-cutlery fa-4x"></i>
                <h1 class="m-xs">Menus</h1>
                <h3 class="font-bold no-margins">
                    Manage Menus
                </h3>
            </div>
        </div>
    </a>

    <a href="/{{Request::get('urlPrefix')}}/orderadmin/view" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon">
        <div class="widget yellow-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-pencil-square fa-4x"></i>
                <h1 class="m-xs">Orders</h1>
                <h3 class="font-bold no-margins">
                    View Orders
                </h3>
            </div>
        </div>
    </a>

    <a href="/reports/{{Request::get('urlPrefix')}}/dashboard" class="col-xs-6 col-sm-4 col-md-3 dashicon" target="_blank" >
        <div class="widget navy-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-file-text fa-4x"></i>
                <h1 class="m-xs">Reports</h1>
                <h3 class="font-bold no-margins">
                    View Reports
                </h3>
            </div>
        </div>
    </a>

    {{--
    <a href="https://dashboard.stripe.com" class="col-xs-6 col-sm-4 col-md-3 dashicon" target="_blank" >
        <div class="widget navy-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-cc-stripe fa-4x"></i>
                <h1 class="m-xs">Billing</h1>
                <h3 class="font-bold no-margins">
                    Stripe Accounts
                </h3>
            </div>
        </div>
    </a>
    --}}

    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 dashicon-white" id="settings">
        <div class="widget white-bg p-lg text-center">
            <div class="m-b-md">
                <i class="fa fa-cog fa-4x"></i>
                <h1 class="m-xs">Settings</h1>
                <h3 class="font-bold no-margins">
                     System and Customer Settings
                </h3>
            </div>
        </div>
    </a>



</div>

<div class="clearfix" style="height: 24px;"></div>

<div class="footer">
    <div class="pull-right">
       <a href="http://www.h2iq.us" target="_blank">Powered by H2IQ</a>
    </div>
    <div>
        &copy; {{date("Y")}} getTableSide.com
    </div>
</div>

@endsection
