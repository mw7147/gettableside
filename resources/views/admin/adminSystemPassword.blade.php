@extends('kitAdmin.kitAdmin')


@section('content')

<!-- Page Level CSS -->

<h1>System User Password Reset</h1>

<h4>
        Password Reset Instructions.
</h4>

<ul class="m-b-md">
    <li>Enter the desired password in both fields.</li>
    <li>The password must be at least 6 characters in length.</li>
    <li>Press "Reset Password" to save the new password.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Password Reset For: </i> {{ $user['fname'] }} {{$user['lname'] }}</h5>
    </div>
        
    <div class="ibox-content">    

    <div class="row">
        <div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1 m-t-md">

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-6 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-6 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif

        <div>
    </div>
    
    <div class="clearfix"></div>

<form name="passwordReset" method="POST" id="passwordReset" action="/nihadmin/password/{{ $user['id'] }}">

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">
    
        <span class="input-group-addon" style="min-width: 140px">Password</span>
        <input type="password" required v-model="password" class="form-control" required name="password" value="">

    </div>

    <div class="input-group m-b col-xs-10 col-sm-9 col-md-6">

        <span class="input-group-addon" style="min-width: 140px">Repeat Password</span>
        <input type="password" required v-model="pass2" class="form-control" name="pass2" value="">

    </div>

    <div class="form-group m-t-lg">
        <div class="col-sm-6 col-sm-offset-1">
            
                {{ csrf_field() }}
            <a href="/nihadmin/systemusers/view" class="btn btn-white" type="submit">Cancel and Return</a>
            <button class="btn btn-success" type="submit" @click="passCheck()">Reset Password</button>

        </div>
    </div>


</form>
<!-- Page Level Scripts -->
<script>

new Vue({

    el: '#passwordReset',
    data: {

        password: '', 
        pass2: ''
    },
    
    
    methods: {

        passCheck: function() {

            var chars = this.password.length;
            if (chars < 6) {alert("Your Password Must Be At Least 6 Characters!"); this.password = ''; this.pass2 = ''; return false;}   
            if (this.password != this.pass2) {alert("Passwords do no match. Please try again."); this.password = ''; this.pass2 = ''; return false;}   

        }


    }

});

</script>

@endsection