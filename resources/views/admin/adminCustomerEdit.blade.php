@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->

<!-- Page Level Scripts -->

<h1>Edit Customer Information</h1>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>


<h4>
        Customer Information is displayed below.
</h4>

<ul class="m-b-md instructions">
    <li>Fill in the Customer Information as necessary.</li>
    <li>The Http Host field is the URL of the Customer.</li>
    <li>A Http Host name change requires a corresponding DNS change in order to be visible on the internet.</li>
    <li>The background image is the background image on the home page.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating the Customer Information.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h3><i>{{ $customer->sitename ?? '' }}</i></h3>
    </div>
        
    <div class="ibox-content">


      <a href="/admin/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
      <a href="/admin/customer/view" class="btn btn-info btn-xs m-l-xs m-b-lg"><i class="fa fa-user m-r-xs"></i>Customers</a>

        <div class="input-group m-b col-xs-10 col-sm-9 col-md-8">

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif

        <div class="clearfix"></div>
        @if(Session::has('fileError'))
            <div class="alert alert-warning alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('fileError') }}
            </div>
        @endif
        </div>

        <div class="row">
            <form name="editCustomer" id="editCustomer" method="POST" action="/admin/customer/edit/{{ $customer->id }}" enctype="multipart/form-data">

                <div class="col-xs-8 col-sm-7 m-b-sm">
                    <span class="form-control-static"> <label class="control-label"><i class="m-r-sm">Customer (Domain) ID: {{$customer->id }}</i></label></span>
                </div>

                <hr class="hr-line-dashed m-b-lg">
                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Customer Name:</label>
                    <input type="text" placeholder="Customer Name" class="form-control" required name="name" value="{{ $customer->name }}">
                </div>
              
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Status:</label>
                    <select class="form-control" name="active" required>
                        <option value="yes">Active</option>
                        <option @if($customer->active =="no")selected @endif value="no">Inactive</option>
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Type:</label>
                    <select class="form-control" name="type" required>
                        @foreach ($types as $type)
                        <option @if($customer->type ==$type->id)selected @endif value="{{ $type->id }}">{{ $type->description }}</option>
                        @endforeach
                    </select>
                </div>



                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Owner User ID:</label>
                    <input type="text" placeholder="Owner User ID" class="form-control" required name="ownerID" value="{{ $customer->ownerID }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <div class="col-md-6" style="margin-left: -15px;">
                        <label class="font-normal font-italic">Parent Customer (Domain) ID:</label>
                        <input type="text" placeholder="Parent Domain ID" class="form-control" required name="parentDomainID" value="{{ $customer->parentDomainID }}">
                    </div>
                    
                    <div class="col-md-6" style="margin-left: -15px;">
                        <label class="font-normal font-italic">Use Parent Menu:</label>
                        <select class="form-control" name="useParentMenu">
                            <option @if(!$customer->useParentMenu) selected @endif value="0">No</option>
                            <option @if($customer->useParentMenu) selected @endif value="1">Yes</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Subdomain:</label>
                    <input type="text" placeholder="Subdomain" class="form-control" required name="subdomain" value="{{ $customer->subdomain }}">
                </div>

                <div class="clearfix"></div>

               <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Http Host:</label>
                    <input type="text" placeholder="Site URL - subdomain.togomeals.biz" class="form-control" name="httpHost"  required id="httpHost" value="{{ $customer->httpHost }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Sitename:</label>
                    <input type="text" placeholder="Sitename" class="form-control" required name="sitename" value="{{ $customer->sitename }}">
                </div>

                <div class="clearfix"></div>

                <hr>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Customer Logo:</label>
                    <div class="input-group m-b"><span class="input-group-btn">
                        <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="width: 20%;">Change</button>
                        <input type="text" class="form-control" id="logo" name="logo" style="width: 80%;" value="{{ $customer->logoPublic }}">
                        <div class="clearfix"></div>
                        <img class="m-l-md m-t-md" id="logo" src="{{ $customer->logoPublic}}">
                        <input type="file" id="newLogo" name="newLogo" style="display: none;">
                    </div>
                </div>

                <div class="clearfix"></div>

                <hr>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Customer Background Image:</label>
                    <div class="input-group m-b"><span class="input-group-btn">
                        <button type="button" id="changePictureButton" class="btn btn-default pull-left" style="width: 20%;">Change</button>
                        <input type="text" class="form-control" id="picture" name="picture" style="width: 80%;" value="{{ $customer->backgroundPublic }}">
                        <div class="clearfix"></div>
                        <a href="{{ $customer->backgroundPublic}}" target="_blank"><img class="m-l-md m-t-md" id="picture" style="max-width: 500px;" src="{{ $customer->backgroundPublic}}"></a>
                        <input type="file" id="newPicture" name="newPicture" style="display: none;">
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="form-group m-t-lg">
                    <div class="col-sm-8 col-sm-offset-1">
                        {{ csrf_field() }}
                        <a href="/admin/customer/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Update Domain</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(){ 

    $('#httpHost').change(function() {  // user name check
        var httpHost = document.getElementById("httpHost").value;  
        var fd = new FormData();    
        fd.append('httpHost', httpHost);

        $.ajax({
            url: "/api/v1/admin/checkhttphost",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {

                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $("#httpHost").val("");
                    $("#httpHost").focus();
                    alert("The httpHost URL is used by another Domain. This URL must be unique. Please use a different URL for the httpHost address."); 
                    return false;   
                        
                } else {
                    
                return true;    
            }}
        });
    });

});

    $("#changeLogoButton").on("click", function() {
        $("#newLogo").trigger("click");
    });

    $("#newLogo").on('change', function() {
    var filename = $('#newLogo').val().split('\\').pop();
        $("#logo").val(filename);
    });

    $("#changePictureButton").on("click", function() {
        $("#newPicture").trigger("click");
    });

    $("#newPicture").on('change', function() {
    var filename = $('#newPicture').val().split('\\').pop();
        $("#picture").val(filename);
    });

</script>

<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection