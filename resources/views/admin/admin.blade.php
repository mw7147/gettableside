<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="getTableSide.com">

    <meta http-equiv=“Pragma” content=”no-cache”>
    <meta http-equiv=“Expires” content=”-1″>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />


    <meta name="author" content="Hospitality IQ - www.h2iq.us">

    <title>getTableSide</title>

    <!-- rhit Core CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
    <link href="{{ mix('css/rhit_admin.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/img/gtsBug.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- HWCT Core JavaScript -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/rhit_admin.js') }}"></script>
    <script src="//cdn.jsdelivr.net/jquery.metismenu/2.7.0/metisMenu.min.js"></script>

</head>

<body class="fixed-sidebar">
    <div id="wrapper">

        @if (Request::get('authLevel') == 35)
        @include('owner.adminNavbar')
        @elseif (Request::get('authLevel') == 30)
        @include('manager.adminNavbar')
        @elseif (Request::get('authLevel') == 20)
        @include('staff.adminNavbar')
        @else
        @include('admin.adminNavbar')
        @endif


        <div id="page-wrapper" class="gray-bg">

        @include('admin.adminTopnav')

        @yield('content')

        </div>
    </div>

<script type='text/javascript'>
  window.__wtw_lucky_site_id = 209476; 

  (function() { var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true; wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s); })();
</script>

</body>
</html>