<!-- TopNav -->

    <div class="row border-bottom white-bg margin-0">
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="/" class="navbar-logo"><img class="img img-responsive" src={{ $domain['logoPublic'] }}></a>
            </div>

            <div class="pull-right navbar-a" style="padding-right: 60px; padding-top: 5px;" href="#map">
                    {{ $domainSiteConfig->locationName }}<br> 
                    {{ $domainSiteConfig->address1 }}<br> 
                    @if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
                    {{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>
                    <i class="fa fa-phone m-r-xs"></i>{{ $domainSiteConfig->locationPhone }}
                </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav m-l-xl">
                    <li @if ($pageName == 'home')class="active" @endif>
                        <a aria-expanded="false" role="button" href="/">Home</a>
                    </li>
                    <li @if ($pageName == 'order')class="active" @endif>
                        <a aria-expanded="false" role="button" href="/order">Order Online</a>
                    </li>
                    <li @if ($pageName == 'services')class="active" @endif>
                        <a aria-expanded="false" role="button" href="/services">Services</a>
                    </li>
                    <li @if ($pageName == 'about')class="active" @endif>
                        <a aria-expanded="false" role="button" href="/about">About Us</a>
                    </li>
                    <li @if ($pageName == 'contact')class="active" @endif>
                        <a aria-expanded="false" role="button" href="/contact">Contact</a>
                    </li>

                    <!-- <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="">Menu item</a></li>
                            <li><a href="">Menu item</a></li>
                            <li><a href="">Menu item</a></li>
                            <li><a href="">Menu item</a></li>
                        </ul>
                    </li> -->
                    
                    
                    <li>

                        @if (Auth::check())
                        <a href="{{ url('/userlogout') }}" ><i class="fa fa-sign-out"></i>Logout</a>
                        @else
                        <a href="{{ url('/userlogin') }}" ><i class="fa fa-sign-out"></i>Login</a>
                        @endif
                        </li>
                    </li>

                </ul>
            </div>
        </nav>
    </div>

<div class="clearfix"></div>
<!-- End TopNav -->
