@extends('site.base')

@section('content')

<style>

#page-wrapper {
    min-height: 100% !important;
    margin-bottom: -15%;
}

.footer {
    min-height: 15% !important;
}

.callout-banner {
    height: 68px !important;
}

</style>

<div class="clearfix"></div>

<div class="row description">    
    <div class="container services">
       
       <div class="col-md-5 col-sm-12 col-xs-12 pull-left">
       
           <img class="img img-responsive img-services" src="/storage/customerImages/1/hamburger450.jpg">

        </div>

        <div class="col-md-6 col-sm-12 col-xs-12 pull-right">

            <div class="widget style1 callout-banner">
            <div class="row vertical-align">
                <div class="col-xs-3">
                    <i class="fa fa-bars fa-3x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <h2 class="font-bold">Contact Us</h2>
                </div>
            </div>
        </div>

            <form method="POST" name="registerForm" action="">
                <div class="form-group">
                    <label>First Name</label>
                    <input class="form-control" type="text" name="fname" value="" required placeholder="First Name">

                    <label>Last Name</label>
                    <input class="form-control" type="text" name="fname" value="" required placeholder="Last Name">
                    
                    <label>Email Address</label>
                    <input class="form-control" type="email" name="fname" value="" required placeholder="Email Address">
                    
                    <label>Comments</label>
                    <textarea class="form-control m-b-md" placeholder="Your comments"></textarea>

                    <input type="button" class="btn banner-button" name="register" value="Submit">
                </div>
            </form>

            
        </div>
    </div>
</div>

<div class="clearfix"></div>




@endsection