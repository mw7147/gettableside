@extends('site.base')

@section('content')
<style>#page-wrapper{ margin-bottom: 0px !important; .banner-row: margin-top: 18px;}</style>
<div class="row banner-row">
    <div class="jumbotron banner-home-pix banner-home" style="background-image: url({{ $data->pixBannerImage }});">
    <div class="container">
        
            <div class="col-sm-5 col-md-offset-2 col-xs-12 pixbanner">

                <h1>{{ $data->pixBannerHeading }}</h1>
                <p class="banner-p">{!! $data->pixBannerBlurb !!}</p>
                <a href="/order" class="btn banner-button">Order Online Now</a>

            </div>
      
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="row description" >    
    <div class="container">
       <div class="col-md-5 pull-left">
       
       <div class="widget style1 callout-banner">
            <div class="row vertical-align">
                <div class="col-xs-3">
                    <i class="fa fa-pencil fa-3x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <h2 class="font-bold">Sign Up</h2>
                    <h5>Get EXCLUSIVE deals and promotions!</h5>
                </div>
            </div>
        </div>
            <form method="POST" name="registerForm" action="">
                <div class="form-group">
                    <label>First Name</label>
                    <input class="form-control" type="text" name="fname" value="" required placeholder="First Name">

                    <label>Last Name</label>
                    <input class="form-control" type="text" name="lname" value="" required placeholder="Last Name">
                    
                    <label>Email Address</label>
                    <input class="form-control" type="email" name="email" value="" required placeholder="Email Address">
                    
                    <label>Mobile Number</label>
                    <input class="form-control" type="tel" id="mobile" name="mobile" value="" required placeholder="Mobile Phone">
                    
                    <label>Birth Month</label>
                    <input class="form-control" type="number" name="birthMonth" min="1" max="12" value="" required placeholder="MM">
                    
                    <label>Birth Day</label>
                    <input class="form-control" type="number" name="birthDay" min="1" max="31" value="" required placeholder="DD">

                    <input type="button" class="btn banner-button" name="register" value="Register">
                </div>
            </form>
        </div>
        
        <div class="col-md-6 pull-right">

            <div class="widget style1 callout-banner">
            <div class="row vertical-align">
                <div class="col-xs-3">
                    <i class="fa fa-spoon fa-3x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <h2 class="font-bold">{{ $data->calloutBannerHeading }}</h2>
                    <h5>{{ $data->calloutBannerBlurb }}</h5>
                </div>
            </div>
        </div>

            {!! $data->calloutBannerText !!}

        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="row map gray-bg">
    <div class="container map-container">

        <h2>Locations</h2>

        <div class="col-md-4 m-b-md">
            <h5>Tuscaloosa, Al</h5>
            <a class="clickme btn banner-button" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3338.179351974915!2d-87.56993868438491!3d33.2093939808437!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x888602e8f3c30827%3A0x875865a1640c4a25!2s511+Greensboro+Ave%2C+Tuscaloosa%2C+AL+35401!5e0!3m2!1sen!2sus!4v1500650776080" target="_blank">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3338.179351974915!2d-87.56993868438491!3d33.2093939808437!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x888602e8f3c30827%3A0x875865a1640c4a25!2s511+Greensboro+Ave%2C+Tuscaloosa%2C+AL+35401!5e0!3m2!1sen!2sus!4v1500650776080" width="100%" height="300" frameborder="0" style="border:0" ></iframe>

            </a>
        </div>

        <div class="col-md-4 m-b-md">
            <h5>Birmingham, Al</h5>
            <a class="clickme btn banner-button" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.0416404053885!2d-86.80967668437823!3d33.52630298075236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88891b9c0797ce47%3A0x96735b630335c2f4!2s2311+Richard+Arrington+Jr+Blvd+N+%23100%2C+Birmingham%2C+AL+35203!5e0!3m2!1sen!2sus!4v1500650899984" target="_blank">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.0416404053885!2d-86.80967668437823!3d33.52630298075236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88891b9c0797ce47%3A0x96735b630335c2f4!2s2311+Richard+Arrington+Jr+Blvd+N+%23100%2C+Birmingham%2C+AL+35203!5e0!3m2!1sen!2sus!4v1500650899984" width="100%" height="300" frameborder="0" style="border:0"></iframe>

            </a>
        </div>

        <div class="col-md-4 m-b-md">
        <h5>Birmingham, Al (Inverness)</h5>
            <a class="clickme btn banner-button" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3330.1801217595826!2d-86.69903448438048!3d33.4185480807831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8889164a2300afb9%3A0x4dba10bc795df0d2!2s110+Inverness+Plaza%2C+Birmingham%2C+AL+35242!5e0!3m2!1sen!2sus!4v1500651095059" target="_blank">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3330.1801217595826!2d-86.69903448438048!3d33.4185480807831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8889164a2300afb9%3A0x4dba10bc795df0d2!2s110+Inverness+Plaza%2C+Birmingham%2C+AL+35242!5e0!3m2!1sen!2sus!4v1500651095059" width="100%" height="300" frameborder="0" style="border:0"></iframe>

            </a>
        </div>

        <div class="col-md-4 m-b-md">
        <h5>Vestavia Hills, Al</h5>
            <a class="clickme btn banner-button" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6658.178456273025!2d-86.79704566689351!3d33.446981780774905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8889191cbe60db73%3A0x1d08aa4390ad0587!2s1919+Kentucky+Ave+%23101%2C+Vestavia+Hills%2C+AL+35216!5e0!3m2!1sen!2sus!4v1500651296067" target="_blank">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6658.178456273025!2d-86.79704566689351!3d33.446981780774905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8889191cbe60db73%3A0x1d08aa4390ad0587!2s1919+Kentucky+Ave+%23101%2C+Vestavia+Hills%2C+AL+35216!5e0!3m2!1sen!2sus!4v1500651296067" width="100%" height="300" frameborder="0" style="border:0" ></iframe>

            </a>
        </div>

        <div class="col-md-4 m-b-md">
        <h5>Mobile, Al</h5>
            <a class="clickme btn banner-button" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13726.065828012795!2d-88.1946071319769!3d30.675741981657037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x889bb2c07aa2eb1d%3A0x1a565455c24ebe71!2s6255+Airport+Blvd%2C+Mobile%2C+AL+36608!5e0!3m2!1sen!2sus!4v1500651402627" target="_blank">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13726.065828012795!2d-88.1946071319769!3d30.675741981657037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x889bb2c07aa2eb1d%3A0x1a565455c24ebe71!2s6255+Airport+Blvd%2C+Mobile%2C+AL+36608!5e0!3m2!1sen!2sus!4v1500651402627" width="100%" height="300" frameborder="0" style="border:0"></iframe>

            </a>
        </div>

        <div class="col-md-4 m-b-md">
        <h5>Daphne, Al</h5>
            <a class="clickme btn banner-button" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13728.674902522764!2d-87.86015533197727!3d30.65737698166353!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x889a6828e73b64f1%3A0xa9495ce0bf7798a1!2s29740+Urgent+Care+Drive%2C+Daphne%2C+AL+36526!5e0!3m2!1sen!2sus!4v1500651476386" target="_blank">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13728.674902522764!2d-87.86015533197727!3d30.65737698166353!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x889a6828e73b64f1%3A0xa9495ce0bf7798a1!2s29740+Urgent+Care+Drive%2C+Daphne%2C+AL+36526!5e0!3m2!1sen!2sus!4v1500651476386" width="100%" height="300" frameborder="0" style="border:0"></iframe>

            </a>
        </div>

        </a>
    </div>
</div>

<div class="clearfix"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>
$( document ).ready(function() {
    console.log("ready");
    $("#mobile").mask("(999) 999-9999");
});
</script>

@endsection