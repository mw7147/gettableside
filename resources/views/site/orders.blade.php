@extends('site.base')

@section('content')

<style>

#page-wrapper {
    min-height: 100% !important;
    margin-bottom: -15%;
}

.footer {
    min-height: 15% !important;
}

.callout-banner {
    height: 68px !important;
}

</style>

<div class="clearfix"></div>

<div class="row description">    
    <div class="container services">
       
       <div class="col-md-3 pull-left">
       
           left column

        </div>

        <div class="col-md-6 pull-right">

            <div class="widget style1 callout-banner">
            <div class="row vertical-align">
                <div class="col-xs-3">
                    <i class="fa fa-bars fa-3x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <h2 class="font-bold">Our Services</h2>
                </div>
            </div>
        </div>

            {!! $data->servicesText !!}
            
        </div>
    </div>
</div>

<div class="clearfix"></div>




@endsection