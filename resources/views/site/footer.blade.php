<!--  footer   -->


<div class="footer">
	<div class="container">

		<div class="col-md-3 footer-left m-t-lg">
			<h4><i class="fa fa-address-card-o m-r-sm"></i>{{ $domainSiteConfig->locationName }}</h4>
			<p class="m-l-lg">{{ $domainSiteConfig->address1 }}<br>
			@if (!empty($domainSiteConfig->address2)){{ $domainSiteConfig->address2 }}<br> @endif 
			{{ $domainSiteConfig->city }}, {{ $domainSiteConfig->state}} {{ $domainSiteConfig->zipCode}}<br>
			{{ $domainSiteConfig->locationPhone }}<br>
			<a class="footer-link" href="mailto:{{ $data->email ?? ''}}">{{ $data->email ?? '' }}</a></p>
		</div>

		<div class="col-md-3 col-md-offset-1 footer-center m-t-lg">

			<h4><i class="fa fa-clock-o m-r-sm"></i>Hours of Operation</h4>


			{!! $data->hours ?? '' !!}

		</div>

		<div class="col-md-3 col-md-offset-2 footer-right m-t-lg">

			<h4><i class="fa fa-share-square-o m-r-sm"></i>Social Connections</h4>
			@if (!empty($data->facebook))<p><a href="{{ $data->facebook ?? '' }}" target="_blank" class="footer-link"><i class="fa fa-facebook-official fa-2x m-r-sm"></i>Facebook</a></p>@endif
			@if (!empty($data->twitter))<p><p><a href="{{ $data->twitter ?? '' }}" target="_blank" class="footer-link"><i class="fa fa-twitter-square fa-2x m-r-sm"></i>Twitter</a></p>@endif
			@if (!empty($data->instagram))<p><p><a href="{{ $data->instagram ?? '' }}" target="_blank" class="footer-link"><i class="fa fa-instagram fa-2x m-r-sm"></i>Instagram</a></p>@endif
			@if (!empty($data->snapchat))<p><p><a href="{{ $data->snapchat ?? '' }}" target="_blank" class="footer-link"><i class="fa fa-snapchat-square fa-2x m-r-sm"></i>Snapchat</a></p>@endif

		</div>


		<div class="clearfix"></div>





	    <div class="pull-right m-t-lg">
	       <a href="http://www.hardwiredcreative.com" target="_blank" class="footer-link">Powered by Hardwired</a>
	    </div>
	    <div class="m-t-lg">&copy; {{ date("Y", time()) }} TogoMeals.biz</div>

	</div>
    
</div>