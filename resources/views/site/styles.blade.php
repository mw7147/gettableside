<!-- Site Styles -->

<style>

    body,
    .panel-body,
    .panel-heading {
        color: {{ $styles->fontColor }} !important;
    }

    .banner-button {
        opacity: 1;
        background-color: {{ $styles->bannerButtonBackgroundColor }};
        border-color: {{ $styles->bannerButtonBorderColor }};
        color: {{ $styles->bannerButtonColor }};
    }

    .banner-button:hover {
        color: {{ $styles->bannerButtonColorHover }};
    }

    .complete-order-button {
        opacity: 1;
        background-color: {{ $styles->completeOrderBackgroundColor }};
        border-color: {{ $styles->completeOrderBorderColor }};
        color: {{ $styles->completeOrderColor }};
    }

    .complete-order-button:hover {
        color: {{ $styles->completeOrderColorHover }};
    }

    .jumbotron {
        margin-bottom: 0px;
    }

    .banner-home {
        min-height: {{ $styles->bannerHomeMinHeight }}px;
    }


    .banner-home-pix {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        background-attachment: fixed;
        background-size: cover;
    }

    .pixbanner {
        background-color: {{ $styles->pixBannerBackgroundColor }} !important;
        opacity: {{ $styles->pixBannerOpacity }};
        margin-top: 15%;
        border-radius: 6px;
        padding: {{ $styles->pixBannerPadding }}px;
        width: {{ $styles->pixBannerWidth }}%;
        color: {{$styles->pixBannerColor }};
    }   

    h1 {
        font-size: 36px !important;
        border-bottom: 1px solid #ccc;
    }

    .banner-p {
        margin-top: 14px;
        margin-bottom: 48px;
    }

    .top-navigation .nav > li > a {
        padding: 15px 10px;
    }

    .navbar {
        position: fixed; /* Set the navbar to fixed position */
        top: 0; /* Position the navbar at the top of the page */
        width: 100%;
    }

    .margin-0 {
        margin-left: 0px;
        margin-right: 0px;
    }

     .nav > li > a,
     .navbar-right,
     .navbar-nav {
        font-size: 16px;
        margin-top: 0px;
        margin-left: 14px;
        float: right;
    }

     .nav > li.active > a,
     .navbar-right {
        color: {{ $styles->navBarLIColor }} !important;
    }

    .nav > li.active > a,
    .nav > li > a:hover {
        color: {{ $styles->navBarLIHoverColor }} !important;
    }

    .navbar-toggle {
        background-color: {{ $styles->navBarToggleBackgroundColor }};
        border-color: {{ $styles->navBarToggleBorderColor}};
    }

    .navbar-logo img {
        max-height: {{ $styles->navBarLogoMaxHeight }}px !important;
        padding: {{ $styles->navBarLogoMaxPadding }}px;
        margin-left: {{ $styles->navBarLogoMaxMarginLeft }}px;
        margin-top: {{ $styles->navBarLogoMaxMarginTop }}px;
    }

    .navbar-a {
        font-size: 14px;
        font-style: italic;
    }

    .callout-banner {
        background-color: {{ $styles->calloutBannerBackgroundColor }};
        border-color: {{ $styles->calloutBannerBorderColor }};
        color: {{ $styles->calloutBannerColor }};
        height: {{ $styles->calloutBannerHeight }}px;
    }

    .footer {
        min-height: 150px;
        background-color: {{ $styles->footerBackgroundColor }};
        color: {{ $styles->footerColor }};
        position: relative;
        margin: -72px -15px;

    }

    .footer-link,
    .footer-link:visited {
        color: {{ $styles->footerLinkColor }};
        font-size: 13px;
    }

    .footer-link:hover,
    .footer-link:active {
        color: {{ $styles->footerLinkHoverColor }};
    }

    .form-group input {
        margin-bottom: 8px;
    }

    .form-group label {
        font-weight: 600;
        font-style: italic;
        margin-bottom: 1px;
    }

    .services {
        margin-top: 72px;
        min-height: {{ $styles->servicesMinHeight }}px;
    }

    .top-spacer {
        height: 60px;
    }

    @media (min-width: 1440px) {
        .navbar-nav {
            margin-top: 20px;
            float: left !important;
        }

        .navbar-right {
            margin-top: 20px;
        }
    }

    @media (min-width: 769px) and (max-width: 1280px) {

        .nav > li > a,
        .nav > li.active > a,
        .nav > li.hover > a {
            font-size: 13px;
        }

        .pixbanner {
            margin-top: 22%;
            padding: 10px;
            width: 55%;
        }

        .banner-home {
            max-height: 500px;
            min-height: 500px;
        }
        
        .nav > li > a,
        .navbar-right,
        .navbar-nav {
            font-size: 13px;
            margin-top: 0px;
            padding: 6px 10px !important;
            float: right;
        }


        h1 {
            font-size: 24px !important;
            border-bottom: 1px solid #ccc;
        }

        .banner-p {
            margin-top: 14px;
            margin-bottom: 48px;
        }

        .jumbotron p {
            font-size: 16px;
        }
    }


    @media (max-width: 768px) {

        .nav > li > a,
        .nav > li.active > a,
        .nav > li.hover > a {
            background-color: {{ $styles->navBarToggleBackgroundColor }} !important;
            border-color: {{ $styles->navBarToggleBackgroundColor }} !important;
            margin-top: 0px;
            width: 100%;
            color: {{ $styles->bannerButtonColor }} !important;
        }

        .pixbanner {
            margin-top: 25%;
            padding: 10px;
            width: 90%;
        }

        .banner-home {
            max-height: 500px;
            min-height: 500px;
        }


        h1 {
            font-size: 22px !important;
            border-bottom: 1px solid #ccc;
        }

        .banner-p {
            margin-top: 14px;
            margin-bottom: 48px;
        }

        .jumbotron p {
            font-size: 14px;
        }

        .navbar-nav {
            width: 100%;
        }

        .navbar-a {
            display: none;
        }
    }


    .description {
        padding: 40px 0px;
        min-height: 350px;
    }

    .map iframe {
        pointer-events: none;
    }

    .clickme {
        width: 100%;
    }

    .map-container h5 {
        margin-bottom: 3px;
        font-style: italic;
    }

#page-wrapper {
    min-height: 100% !important;
    margin-bottom: -15%;
}

.container {
    width: 90%;
}

.footer {
    min-height: 15% !important;
}

.callout-banner {
    height: 68px !important;
}

.placeOrder {
    color: yellow;
}

.placeOrder:hover,
.placeOrder:active {
    color: white;
}

.clearCart {
    margin-right: 15px;
    font-style: italic;
    color: {{ $styles->bannerButtonBackgroundColor }} !important;
    min-height: 24px;
}

.clearCart:active,
.clearCart:hover {
    font-size: 14px;
}

.panel-heading:active,
.panel-heading:hover,
.active.panel-heading {
    color: {{ $styles->bannerButtonColor }} !important;
    background-color: {{ $styles->bannerButtonBackgroundColor }} !important;
    border-color: {{ $styles->bannerButtonBorderColor }} !important;
    font-weight: 600;
}


.panel-group .panel-heading + .panel-collapse > .panel-body, 
.panel-group .panel-heading + .panel-collapse > .list-group,
.hwct-border {
    border: 1px solid {{ $styles->bannerButtonBorderColor }};
    color: {{ $styles->pixBannerColor }};
    border-radius: 2px;
    border-bottom: medium none;
}

.hwct-heading {
    padding: 8px 15px;
    color: {{ $styles->bannerButtonColor }} !important;
    background-color: {{ $styles->bannerButtonBackgroundColor }} !important;
    border-color: {{ $styles->bannerButtonBorderColor }} !important;
    font-weight: 600;
}

/* .panel-menu-item {
    border-bottom: 1px solid {{ $styles->bannerButtonBorderColor }} !important;
    background-color: #f8f8f8 !important;
} */


.yourOrder {
    font-size: 18px;
}

.logo {
        max-width: 40%;
    }

@media (min-width: 768px) and (max-width: 1600px) {
    .yourOrder {
        font-size: 14px;
    }
}

@media (max-width: 767px) {
    .yourOrder,
    #orderCart,
    .panel-body {
        font-size: 12px;
    }

}

h5.panel-title a,
.aBlock {
    display: block;
}

.description {
    padding-top: 0px;
}

.imgIframe {
    max-height: {{ $styles->navBarLogoMaxHeight }}px !important;
    margin-top: -50px;
    margin-left: 15px;
    margin-bottom: 20px;
}

.h5-menu-item {
    margin-bottom: 20px;
}

.btn-order {
    border: 1px solid {{ $styles->bannerButtonBorderColor }};
    background-color: {{ $styles->bannerButtonBackgroundColor }} !important;
    color: {{ $styles->bannerButtonColor }} !important;
}
.orderDeleteIcon {
    color: {{ $styles->bannerButtonBackgroundColor }};
    font-size: 14px;
}

.totalTop {
    border-top: 1px solid {{ $styles->bannerButtonBackgroundColor }} !important;
}

.panel-body {
    padding: 0px !important;
}

.pad15 {
    padding: 10px 15px 5px 15px;
}

.mt10 {
    margin-top: 10px;
}

@media (max-width: 990px) {

    .button990 {
        margin: -10px 0px 10px 15px;
    }

}

.topSpace {
    height: 70px;
}

@media (max-width: 767px) {
    .zipcode {
        margin-top: 5px;
    }
}

#placeOrder,
#returnMenu {
    width: 100%;
}

.closed {
    font-style: italic;
    text-align: center;
    margin-top: 18px;
}

#returnMenu {
    min-height: 0px;
}

</style>