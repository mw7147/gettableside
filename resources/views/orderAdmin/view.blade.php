<!-- View Contacts -->
@extends('admin.admin')

@section('content')

<style>
    .orderInProcess {
        background-color: #dfffdf;
    }

    .orderRefunded {
        background-color: #dff2ff;
    }
    
    .futureOrder {
        background-color: #f9ffa8;
    }

    .dineInOrder {
        background-color: #f1ddfb;
    }
    

    #specificRangeHolder {
        margin-bottom: 36px;
    }

</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>

	<h2>Order Administration</h2>

	<h4>
		Orders are listed below.
	</h4>

    <button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Help</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Help</button>

	<ul class="instructions">
        <li>If selected, a quick date range selection takes precedent over a specific date range.</li>
        <li>Make sure a quick date range is not selected if you select a specific date range.</li>
        <li>To select a specific date range, click on the beginning date then click on the end date and click apply.</li>
		<li>Type in the search box to search results.</li>
		<li>Press the "Copy" button to copy the data to your clipboard.</li>
		<li>Press the "CSV" button to export the data to a Excel compatible file.</li>
		<li>Press the "PDF" button to create and download a PDF file.</li>
        <li>Press the "Print" button to print the results.</li>
        <li>Color Key: Yellow - Future Order, White - Current Order, Green - Order Completed, Red - Order Refunded</li>
	</ul>

<div class="ibox-content">

<h3>Order List</h3><hr>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/view" class="btn btn-info btn-xs m-b-lg w110"><i class="fa fa-refresh m-r-xs"></i>Refresh Page</a>

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>

    @if (Request::get('authLevel') <= 35)
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview" class="btn btn-warning btn-xs m-b-lg  w110"><i class="fa fa-bullseye m-r-xs"></i>Orders Only</a>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/pendingordersview" class="btn btn-warning btn-xs m-b-lg  w150"><i class="fa fa-bullseye m-r-xs"></i>Pending Orders View</a>
    @endif
    
    <div class="clearfix"></div>


        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif

        <form name="ordersDateRange" method="post" action = "/{{Request::get('urlPrefix')}}/orderadmin/view" style="border: 1px solid #555; width: 300px; margin-bottom: 18px; padding: 8px 15px;">
        <div>
            <label class="font-normal font-italic">Quick Date Range:</label>
            <select class="form-control" name="dateRange" id="dateRange" style="min-width: 250px;">
                <option value="allFuture" @if ($dateRange == "allFuture")selected @endif>All Future Orders</option>
                <option value="tomorrow" @if ($dateRange == "tomorrow")selected @endif>Tomorrow</option>
                <option value="" @if ($dateRange == "")selected @endif>Select Quick Range</option>
                <option value="today" @if ($dateRange == "today")selected @endif>Today</option>
                <option value="yesterday" @if ($dateRange == "yesterday")selected @endif>Yesterday</option>
                <option value="week" @if ($dateRange == "week")selected @endif>This Week</option>
                <option value="lastWeek" @if ($dateRange == "lastWeek")selected @endif>Last Week</option>
                <option value="month" @if ($dateRange == "month")selected @endif>This Month</option>
                <option value="lastMonth" @if ($dateRange == "lastMonth")selected @endif>Last Month</option>
            </select>
        </div>

        <div class="clearfix"></div>
        <h4 class="m-l-sm m-t-sm"><i>Or</i></h4>

        <div class="clearfix"></div>

        <div id="specificRangeHolder">
            <label class="font-normal font-italic">Specific Date Range:</label>
            <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" name="specificRange" id="specificRange" style="min-width: 267px;" value="{{ $specificRange }}"/>
            </div>
        </div>

        <button type="submit" class="btn btn-info btn-sm" style="margin-top: -36px; min-width: 267px;">Set Range</button>
        
        {{ csrf_field() }}
        </form>
        <div class="clearfix"></div>


    <div class="table-responsive">
        <table class="table table-bordered table-hover nowrap dataTables" >
            <thead>
                <tr>
                    
                    <th width="50">Order ID</th>
                    <th width="60">Order Placed</th>
                    <th width="60">Order Start</th>
                    <th width="60">Type</th>
                    <th width="60">Location</th>
                    <th width="80">Name</th>
                    <th width="60">Mobile</th>
                    <th width="50">Amount</th>
                    <th width="120">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($orders as $order)
                <tr 
                    @if ($order->orderInProcess == 1 || $order->orderInProcess == 2) class="orderInProcess" 
                    @elseif ($order->orderInProcess == 5) class="dineInOrder" 
                    @else ($order->orderInProcess == 9) class="futureOrder" 
                    @endif
                    
                    @if ($order->orderRefunded) class="orderRefunded" @endif> 
                    <td>{{ $order->id }}</td>
                    <td>{{ Carbon\Carbon::parse($order->created_at)->format('D n/j g:i A') }}</td>
                    <td>{{ Carbon\Carbon::parse($order->orderPrepTime)->format('D n/j g:i A') }}</td>
                    <td>{{ $order->orderType }}</td>
                    <td>{{ $order->orderDomain->name}} @if($order->locationName){{'-'.$order->locationName }}@endif</td>
                    <td>{{ $order->fname }} {{ $order->lname }}</td>
                    <td>{{ $order->customerMobile }}</td>
                    <td>${{ number_format($order->total - $order->orderRefundAmount, 2) }}</td>
                    <td class="white-bg">
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="showDetail('{{ $order->id }}')"><i class="fa fa-binoculars m-r-xs"></i>Details</button>
                        
                        {{--<a class="btn btn-success btn-xs w90 m-r-xs m-b-xs" href="/{{Request::get('urlPrefix')}}/vieworder/{{ $order->id }}"><i class="fa fa-binoculars m-r-xs"></i>Order</a>--}}
                        <a class="btn btn-success btn-xs w90 m-r-xs m-b-xs" href="/reports/{{Request::get('urlPrefix')}}/receipt/{{ $order->id }}" target="popup" onclick="window.open('/reports/{{Request::get('urlPrefix')}}/receipt/{{ $order->id }}','popup', 'width=800,height=700,top=15,left=30'); return false;" ><i class="fa fa-binoculars m-r-xs"></i>View</a>

                        @if ($printOrder)
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="rePrint('{{$order->id}}', 'order');"><i class="fa fa-print m-r-xs"></i>Kitchen</button>
                        @endif
                        
                        @if ($printReceipt)
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="rePrint('{{$order->id}}', 'receipt');"><i class="fa fa-print m-r-xs"></i>Receipt</button>
                        @endif
                        
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="sendText('{{$order->contactID}}', '{{$order->fname}}', '{{$order->lname}}');"><i class="fa fa-commenting m-r-xs"></i>Text</button>
                        
                        @if ($order->orderInProcess != 2 && $authLevel < 40 && $order->orderInProcess !== 0 && $authLevel < 40)
                        <button type="button" class="btn btn-info btn-xs w90 m-r-xs m-b-xs" onclick="orderInProcess('{{$order->id}}')"><i class="fa fa-check-circle m-r-xs"></i>In Process</button>
                        @endif

                        @if ($order->orderInProcess >= 1 && $authLevel < 40 )
                        <button type="button" class="btn btn-warning btn-xs w90 m-r-xs m-b-xs" onclick="orderReady('{{$order->id}}')"><i class="fa fa-check-circle m-r-xs"></i>Ready</button>
                        @endif
                        
                        @if ($order->total != $order->orderRefundAmount && $authLevel < 40 )
                        <button class="btn btn-danger btn-xs w90 m-r-xs m-b-xs" onclick="refundOrder('{{$order->id}}', '{{$order->total - $order->orderRefundAmount}}')"><i class="fa fa-window-close m-r-xs"></i>Refund</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

@include('orderAdmin.orderDetailModal')
@include('orderAdmin.sendTextModal')

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>
    <!-- Date range picker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /><script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 2, "desc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

        $('#specificRange').daterangepicker();

        $('#specificRange').on('change',function(){
            $('#dateRange').val('');
        });


    });

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

</script>

 

 <script>

function rePrint(orderID, type) {
    var fd = new FormData();
    fd.append('orderID', orderID),    
    fd.append('type', type),    

    $.ajax({
        url: "/api/v1/reprint",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            alert(mydata);
            return true;
        },
        error: function() {
            alert("There was an problem with your print request. Please try again.");
        }
    });
};

function orderReady(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order ready text to customer and mark order ready.');
    if (!st) {
        var mr = confirm("Would you like to mark the order as ready? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderready",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function orderInProcess(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order in process text to customer and mark order in process.');
    if (!st) {
        var mr = confirm("Would you like to mark the order in process? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderinprocess",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function refundOrder(orderID, oa) {

    orderAmount = parseFloat(oa).toFixed(2);

    var ro = confirm('Are you sure you wish to refund the order? This action is not reversable.');
    if (!ro) { return false; }
    
    var refundAmount = prompt('Please enter the amount you wish to refund or press OK for full refund.', orderAmount);
    if (!refundAmount) {
        return false;
    } else if ( parseFloat(refundAmount) > parseFloat(orderAmount) ) {
        alert("The refund amount cannot be greater than the order amount.");
        return false;
    } else {
        var pin = prompt("Please enter Manager PIN");
    }

    // send text and mark as read
    var fd = new FormData();
    fd.append('managerPIN', pin),
    fd.append('userID', '{{Auth::id()}}'),
    fd.append('orderID', orderID),
    fd.append('refundAmount', refundAmount)
    $.ajax({
        url: "/api/v1/admin/{{ $ccType }}/refundorder",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            alert(mydata['message']);
            location.reload();
        },
        error: function() {
            alert("There was a problem refunding the order. Please try again.");
        }
    });
};

 </script>

@endsection