<!-- View Contacts -->
@extends('admin.adminRefreshContentOnly')

@section('content')

<style>
    .orderInProcess {
        background-color: #dfffdf;
    }

    .orderReady {
        background-color: #23c6c8;
        color: white;
    }
    
    .orderRefunded {
        background-color: #dff2ff;
    }

    .dineInOrder {
        background-color: #f1ddfb;
    }

    .futureOrder {
        background-color: #f9ffa8;
    }
</style>

<!-- Page-Level CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.css"/>


<div class="ibox-content">

<h3>Order List</h3><hr>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview" class="btn btn-info btn-xs m-b-lg w120"><i class="fa fa-refresh m-r-xs"></i>Refresh Page</a>

    <a href="/{{Request::get('urlPrefix')}}/orderadmin/view" class="btn btn-warning btn-xs m-b-lg  w120"><i class="fa fa-reply m-r-xs"></i>Close and Return</a>
    
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/pendingordersview" class="btn btn-warning btn-xs m-b-lg  w150"><i class="fa fa-bullseye m-r-xs"></i>Pending Orders View ({{$pendingOrdersCount}})</a>

    <div class="clearfix"></div>


        @if(Session::has('deleteSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('deleteError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('deleteError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div class="table-responsive">
        <table class="table table-bordered table-hover nowrap dataTables" >
            <thead>
                <tr>
                    <th width="50">Order ID</th>
                    <th width="60">Order Placed</th>
                    <th width="60">Order Start</th>
                    <th width="60">Type</th>
                    <th width="60">Location</th>
                    <th width="80">Name</th>
                    <th width="70">Mobile</th>
                    <th width="50">Amount</th>
                    <th width="120">Actions</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($orders as $order)
                <tr                     
                    @if ($order->orderInProcess == 1 || $order->orderInProcess == 2) class="orderInProcess" 
                    @elseif ($order->orderInProcess == 5) class="dineInOrder" 
                    @else ($order->orderInProcess == 9) class="futureOrder" 
                    @endif
                    >
                    <td>{{ $order->id }}</td>
                    <td>{{ Carbon\Carbon::parse($order->created_at)->format('D n/j g:i A') }}</td>
                    <td>{{ Carbon\Carbon::parse($order->orderPrepTime)->format('D n/j g:i A') }}</td>
                    <td>{{ $order->orderType }}</td>
                    <td>{{ $order->locationName }}</td>
                    <td>{{ $order->fname }} {{ $order->lname }}</td>
                    <td>{{ $order->customerMobile }}</td>
                    <td>${{ $order->total }}</td>
                    <td class="white-bg">
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="showDetail('{{ $order->id }}')"><i class="fa fa-binoculars m-r-xs"></i>Details</button>
                        
                        {{--<a class="btn btn-success btn-xs w90 m-r-xs m-b-xs" href="/{{Request::get('urlPrefix')}}/vieworderonly/{{ $order->id }}" target="_blank"><i class="fa fa-binoculars m-r-xs"></i>Order</a>--}}
                        <a class="btn btn-success btn-xs w90 m-r-xs m-b-xs" href="/reports/{{Request::get('urlPrefix')}}/receipt/{{ $order->id }}" target="popup" onclick="window.open('/reports/{{Request::get('urlPrefix')}}/receipt/{{ $order->id }}','popup', 'width=800,height=700,top=15,left=30'); return false;" ><i class="fa fa-binoculars m-r-xs"></i>View</a>

                        @if ($printOrder)
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="rePrint('{{$order->id}}', 'order');"><i class="fa fa-print m-r-xs"></i>Kitchen</button>
                        @endif

                        @if ($printReceipt)
                        <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="rePrint('{{$order->id}}', 'receipt');"><i class="fa fa-print m-r-xs"></i>Receipt</button>
                        @endif
                        
                     {{--   <button class="btn btn-success btn-xs w90 m-r-xs m-b-xs" onclick="sendText('{{$order->contactID}}', '{{$order->fname}}', '{{$order->lname}}');"><i class="fa fa-commenting m-r-xs"></i>Send Text</button> --}}
                        

                     @if ($order->orderInProcess != 2 && $order->orderInProcess !== 0)
                     <button type="button" class="btn btn-info btn-xs w90 m-r-xs m-b-xs" onclick="orderInProcess('{{$order->id}}')"><i class="fa fa-check-circle m-r-xs"></i>In Process</button>
                     @endif

                     @if ($order->orderInProcess >= 1 )
                     <button type="button" class="btn btn-warning btn-xs w90 m-r-xs m-b-xs" onclick="orderReady('{{$order->id}}')"><i class="fa fa-check-circle m-r-xs"></i>Ready</button>
                     @endif

                    </td>
                </tr>
            @endforeach
            
            </tbody>                
        </table>
    </div>
</div>

@include('orderAdmin.orderDetailModal')
@include('orderAdmin.sendTextModal')

<!-- Page-Level Scripts -->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/r-2.1.0/sc-1.4.2/datatables.min.js"></script>

<script>
    $(document).ready(function(){
        $('.dataTables').DataTable({
            pageLength: 25,
            dom: '<"html5buttons"B>lTfgitp',
            order: [[ 2, "desc" ]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'CustomerContactList'},
                {extend: 'pdf', title: 'CustomerContactList'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]

        });

    });

</script>

 

 <script>

function rePrint(orderID, type) {
    var fd = new FormData();
    fd.append('orderID', orderID),    
    fd.append('type', type),    

    $.ajax({
        url: "/api/v1/reprint",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            alert(mydata);
            return true;
        },
        error: function() {
            alert("There was an problem with your print request. Please try again.");
        }
    });
};

function orderReady(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order ready text to customer and mark order ready.');
    if (!st) {
        var mr = confirm("Would you like to mark the order as ready? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderready",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



function orderInProcess(orderID) {

    var orderReadOnly = 'no';
    var st = confirm('Press OK to send order in process text to customer and mark order in process.');
    if (!st) {
        var mr = confirm("Would you like to mark the order in process? No text message will be sent to customer.")
        if (mr) {
            // mark as order ready only - no text
            orderReadOnly = 'yes';
        } else {
            // do nothing
            return false;
        }
    }
    // send text and mark as read
    var fd = new FormData();
    fd.append('orderID', orderID),
    fd.append('orderReadOnly', orderReadOnly),    
    $.ajax({
        url: "/api/v1/admin/sendorderinprocess",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,   
        success: function(mydata) {
            console.log(mydata)
            location.reload();
        },
        error: function() {
            alert("There was an problem sending your text message. Please try again.");
        }
    });
};



 </script>

@endsection