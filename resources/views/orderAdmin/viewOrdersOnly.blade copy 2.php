<!-- View Contacts -->
@extends('admin.adminRefreshContentOnly')

@section('content')
<style>
    .topBar {
        float: left;
        width: 100%;
        background: #fff;
        padding: 10px 0;
        margin-bottom: 10px;
    }
    .btn-instructions {
        padding: 5px 15px;
    }
    .btn-instructions .fa {
        font-size: 16px;
        font-weight: 800;
    }
    .export {
        font-size: 14px;
        font-weight: 600;
        text-transform: uppercase;
    }
    .border-bottom {
        margin-bottom: 22px;
    }
    .dispatach {
        float: left;
        width: 100%;
        margin-top: 5px;
    }
    .dispatach span {
        font-size: 14px;
        text-transform: uppercase;
        margin-bottom: 0;
    }
    .dispatach span a {
        color: red;
    }
    .logOut {
        font-size: 14px;
        font-weight: 600;
        margin-left: 20px;
    }
    .ibox-title {
        padding: 7px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }
    .ibox-title p {
        font-size: 14px;
        margin-bottom: 0px;
    }
    .cardHeader {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 10px;
    }
    .cardHeader h3 {
        margin: 0;
    }
    .blue-bg {
        border: medium none;
    }
    .dropdown-toggle {
        color: #555;
    }
    .dropdown-menu > li > a {
        color: #555;
        font-size: 14px;
        font-weight: 600;
    }
    .cardContent {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        padding: 20px;
        background: #fafafa;
    }
    .ibox {
        cursor: move;
    } 
    .cardContent p {
        font-size: 18px;
        font-weight: 600;
        margin-bottom: 0;
    }
    .location i {
        font-size: 18px;
        font-weight: 600;
    }
    .cardFooter {
        padding: 10px 15px;
        background: #1ab394;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
    }
    .cardFooter p {
        color: #fff;
        text-transform: uppercase;
        font-size: 16px;
        font-weight: 500;
        margin-bottom: 0;
    }
    .dropzone {
        min-height: 180px;
        float: left;
        height: auto;
        width: 100%;
        padding: 10px;
    }
    .dropzone .ibox {
        width: 100%;
        float: left;
        clear: unset;
    } 
    .borderLeft {
        border-left: 1px solid #ccc;
    }
    .refresh {
        display: flex;
        height: 65px;
        width: 65px;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
    }
    .dropZone .ibox {
        margin-bottom: 10px;
        margin-top: 10px;
    }

</style>

<!-- Page-Level CSS -->
<div class="row border-bottom">
    <div class="topBar navbar navbar-static-top white-bg">
        <div class="col-sm-4 col-md-4">
        <a href="#" class="btn btn-danger btn-sm export">Expo</a>
        </div>
        <div class="col-sm-4 col-md-4 text-center">
            <a href="#" class="btn btn-primary btn-xs btn-instructions m-r-xs"><i class="fa fa-angle-double-left"></i></a>
            <a href="#" class="btn btn-primary btn-xs btn-instructions m-r-xs"><i class="fa fa-circle"></i></a>
            <a href="#" class="btn btn-primary btn-xs btn-instructions"><i class="fa fa-angle-double-right"></i></a>
        </div>
        <div class="col-sm-4 col-md-4 text-right">
            <div class="dispatach">
                <span>Print On Dispatch: <a href="#" class="m-r-xs">off</a></span>
                <a href="/userlogout" class="logOut"><i class="fa fa-sign-out m-r-xs"></i>Logout</a>
            </div>
        </div>
    </div>
</div>
<div class="ibox-content">
    <h3>Order List</h3><hr>
    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-12">
            <div class="row" id="draggable">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="ibox">
                        <div class="ibox-title blue-bg">
                            <div class="cardHeader">
                                <h3>b-52</h3>
                                <a href="#" class="btn btn-xs white-bg">Pending</a>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Edit</a></li>
                                        <li><a href="#">Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Alyssa Garden #4971(GoTab)</p>
                        </div>
                        <div class="cardContent">
                            <p>Twisted Tea - Can 12oz</p>
                            <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                        </div>
                        <div class="cardFooter">
                            <p>Waiting On Others Stations</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="ibox">
                        <div class="ibox-title blue-bg">
                            <div class="cardHeader">
                                <h3>b-52</h3>
                                <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Edit</a></li>
                                        <li><a href="#">Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Alyssa Garden #4971(GoTab)</p>
                        </div>
                        <div class="cardContent">
                            <p>Twisted Tea - Can 12oz</p>
                            <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                        </div>
                        <div class="cardFooter">
                            <p>Waiting On Others Stations</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="ibox">
                        <div class="ibox-title blue-bg">
                            <div class="cardHeader">
                                <h3>b-52</h3>
                                <a href="#" class="btn btn-xs white-bg">Pending</a>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Edit</a></li>
                                        <li><a href="#">Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Alyssa Garden #4971(GoTab)</p>
                        </div>
                        <div class="cardContent">
                            <p>Twisted Tea - Can 12oz</p>
                            <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                        </div>
                        <div class="cardFooter">
                            <p>Waiting On Others Stations</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="ibox">
                        <div class="ibox-title blue-bg">
                            <div class="cardHeader">
                                <h3>b-52</h3>
                                <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Edit</a></li>
                                        <li><a href="#">Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Alyssa Garden #4971(GoTab)</p>
                        </div>
                        <div class="cardContent">
                            <p>Twisted Tea - Can 12oz</p>
                            <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                        </div>
                        <div class="cardFooter">
                            <p>Waiting On Others Stations</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-12 borderLeft">
            <div class="dropZone" id="droppable"></div>
        </div>
    </div>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview" class="btn btn-danger refresh"><i class="fa fa-refresh m-r-xs"></i>Refresh</a>
    <div class="clearfix"></div>
</div>

@endsection
<link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'></link>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1556638668/Sortable.js"></script>
<script>
    $(document).ready(function() {
        var draggable = document.getElementById('draggable');
        var droppable = document.getElementById('droppable');
        new Sortable(draggable, {
            group: 'shared',
        });
        new Sortable(droppable, {
            group: 'shared',
            animation: 1150
        });
    });
</script>