@extends('user.userAdmin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">


<div class="col-xs-12 col-sm-12 col-md-10 col-lg-9">
    <div class="ibox float-e-margins">
        
        <div class="ibox-title">
            <h5>Contact ID {{$contact->id }} - {{ $contact->fname }} {{ $contact->lname }}</h5>
        </div>
        
        <div class="ibox-content">

            <div style="padding-left: 15px;">
                <a href="#" class="btn btn-info btn-xs m-b-lg"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>
                <a href="/user/dashboard" class="btn btn-info btn-xs m-b-lg"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
            </div>

            <div class="panel-body">

                <div class="panel-group" id="accordion">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <i class="fa fa-user m-r-xs"></i>
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">Contact Information</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                        
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <h2>Contact Information</h2>

                                        <p class="m-t">
                                            <ul>
                                                <li>Required fields are noted with <span class="text-danger m-l-xs">*</span></li>
                                            </ul>
                                        </p>
                                  
                                    </div>

                                    <div class="col-md-8">

                                        <div class="clearfix"></div>
                                        @if(Session::has('mobileError'))
                                            <div class="alert alert-warning alert-dismissable col-xs-10 col-sm-6 m-b-xl">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                {{ Session::get('mobileError') }}
                                            </div>
                                        @endif

                                        <form role="form" id="contactForm" name="contactForm">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>First Name<span class="text-danger m-l-xs">*</span></label>
                                                        <input type="text" class="form-control" name="fname" placeholder="First Name" required value="{{ $contact->fname }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Last Name<span class="text-danger m-l-xs">*</span></label>
                                                        <input type="text" class="form-control" name="lname" placeholder="Last Name" required value="{{ $contact->lname }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Email Address<span class="text-danger m-l-xs">*</span></label>
                                                        <input type="email" class="form-control" name="email" placeholder="Email Address" required value="{{ $contact->email }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Mobile Number<span class="text-danger m-l-xs">*</span></label>
                                                        <input type="tel" class="form-control" name="mobile" placeholder="Mobile Number" required data-mask="(999) 999-9999" min="2011000000" max="9999999999" data-validation-required-message="Please enter your 10 digit mobile number" value="{{ $contact->mobile }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Company Name</label>
                                                        <input type="text" class="form-control" name="companyName" placeholder="Company Name" value="{{ $contact->companyName }}">
                                                    </div>
                                                </div> 
                                            </div>     
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <input type="tel" class="form-control" name="title" placeholder="Title" value="{{ $contact->title }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Company Email</label>
                                                        <input type="email" class="form-control" name="companyEmail" placeholder="Company Email" value="{{ $contact->companyEmail }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Company Phone</label>
                                                        <input type="tel" class="form-control" name="companyPhone" placeholder="Company Phone" data-mask="(999) 999-9999" min="2011000000" max="9999999999" data-validation-required-message="Please enter a 10 digit phone number" value="{{ $contact->companyPhone }}">
                                                    </div>
                                                </div>
                                            </div>

                                     
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input type="hidden" name="uid" value="{{ $userID }}">
                                                    <input type="hidden" name="domainID" value="{{ $domain['id'] }}">
                                                    <input type="hidden" name="cid" class="cid" id="cid" value="{{ $contact->id }}">
                                                    <button class="btn btn-primary" type="button" id="contactSubmit">Update Contact Information</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>                                 
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <i class="fa fa-user-circle m-r-xs"></i>

                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">Contact Picture</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            
                            <div class="panel-body">
                                
                                <div class="panel panel-white">

                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h2>Contact Picture</h2>

                                                    <p class="m-t">
                                                        <ul>
                                                            <li>Optional picture.</li>
                                                            <li>Picture must be an image and less than 750kb in size.</li>
                                                        </ul>
                                                    </p>
                                              
                                                </div>
                                                <div class="col-md-8">
                                                    <form role="form" id="newUserImage" name="newUserImage" enctype="multipart/form-data">
                                                        <div class="input-group m-b col-xs-11 nodisplay">
                                                            <div class="fileinput @if ($contact->pixPrivate == '')fileinput-new @else file-exists @endif  input-group m-l-md" data-provides="fileinput">

                                                                <span class="input-group-addon btn btn-default btn-file">
                                                                    <span class="fileinput-new">Select Picture</span>
                                                                    <span class="fileinput-exists">Change</span>
                                                                    <input type="file" id="newImage" name="newImage" required  />
                                                                </span>
                                                                
                                                                <div class="fileinput-preview fileinput-exists" ><span><img src="{{ $contact->pixPublic }}"></span></div>

                                                                <div class="form-control" data-trigger="fileinput" style="border: none; font-size: 11px;">

                                                                    <span class="fileinput-filename fileinput-exists" >{{ $contact->pixPrivate }}</span>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <div class="modal-footer">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="cid" class="cid" id="cid" value="{{ $contact->id }}">
                                                            <input type="hidden" name="uid" id="uid" value="{{ $userID }}">
                                                            <button type="button" id="imageSubmit" class="btn btn-primary">Update Picture</button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <i class="fa fa-address-card m-r-xs"></i>

                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">Contact Address</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">


                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <h2>Address Information</h2>
                                                    <p class="m-t">
                                                        <ul>
                                                            <li>Optional address information.</li>
                                                        </ul>
                                                    </p>                                  
                                    </div>
                                    <div class="col-md-8">

                                       <form role="form" id="addressInformation" name="addressInformation">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Address 1</label>
                                                        <input type="text" class="form-control" name="address1" placeholder="Address 1" value="{{ $contact->address1 }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Address 2</label>
                                                        <input type="text" class="form-control" name="address2" placeholder="Address 2" value="{{ $contact->address2 }}">
                                                    </div>
                                                </div>
                                            </div>
 

                                           <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" class="form-control" name="city" placeholder="City" value="{{ $contact->city }}">
                                                    </div>
                                                </div> 
                                            </div>                                          
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>State</label>
    
                                                        <select name="state" class="form-control">
                                                            <option value="">-- Select State --</option>

                                                            @foreach ($usStates as $usState)
                                                                <option value="{{ $usState->stateCode }}" @if ($usState->stateCode == $contact->state)selected @endif>{{ $usState->stateName }}</option>
                                                            @endforeach

                                                        </select>
                                                        



                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group m-r-lg" style="width: 30%; display: inline-block;">
                                                        <label>Zip or Postal Code</label>
                                                        <input type="text" class="form-control" name="zipCode" placeholder="Zip or Postal Code" value="{{ $contact->zipCode }}">
                                                    </div>

                                                    <div class="form-group" style="width: 40%; display: inline-block;">
                                                        <label>Country</label>
                                                        <select name="countryCode" class="form-control">
                                                            
                                                            <option value=""> Country </option>
                                                            @foreach ($countries as $country)
                                                                <option value="{{ $country->countryCode }}"@if ($country->countryCode == $contact->countryCode)selected @endif>{{ $country->countryName}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Birthday</label><br>
                                                        <select name="birthMonth" class="form-control m-r-xs" style="width: 25%; display: inline-block;">
                                                            <option value=""> Month </option>
                                                            <option value="1" @if ($contact->birthMonth == 1)selected @endif>January</option>
                                                            <option value="2" @if ($contact->birthMonth == 2)selected @endif>February</option>
                                                            <option value="3" @if ($contact->birthMonth == 3)selected @endif>March</option>
                                                            <option value="4" @if ($contact->birthMonth == 4)selected @endif>April</option>
                                                            <option value="5" @if ($contact->birthMonth == 5)selected @endif>May</option>
                                                            <option value="6" @if ($contact->birthMonth == 6)selected @endif>June</option>
                                                            <option value="7" @if ($contact->birthMonth == 7)selected @endif>July</option>
                                                            <option value="8" @if ($contact->birthMonth == 8)selected @endif>August</option>
                                                            <option value="9" @if ($contact->birthMonth == 9)selected @endif>September</option>
                                                            <option value="10" @if ($contact->birthMonth == 10)selected @endif>October</option>
                                                            <option value="11" @if ($contact->birthMonth == 11)selected @endif>November</option>
                                                            <option value="12" @if ($contact->birthMonth == 12)selected @endif>December</option>                                                        
                                                        </select>

                                                        <select name="birthDay" class="form-control m-r-xs" style="width: 10%; display: inline-block;">
                                                            
                                                            <option value=""> Day </option>
                                                            @for ($i = 1; $i <= 31; $i++)
                                                                <option value="{{ $i }}" @if ($contact->birthDay == $i)selected @endif>{{ $i }}</option>
                                                            @endfor

                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Anniversary</label><br>
                                                        <select name="anniversaryMonth" class="form-control m-r-xs" style="width: 25%; display: inline-block;">
                                                            <option value=""> Month </option>
                                                            <option value="1" @if ($contact->anniversaryMonth == 1)selected @endif>January</option>
                                                            <option value="2" @if ($contact->anniversaryMonth == 2)selected @endif>February</option>
                                                            <option value="3" @if ($contact->anniversaryMonth == 3)selected @endif>March</option>
                                                            <option value="4" @if ($contact->anniversaryMonth == 4)selected @endif>April</option>
                                                            <option value="5" @if ($contact->anniversaryMonth == 5)selected @endif>May</option>
                                                            <option value="6" @if ($contact->anniversaryMonth == 6)selected @endif>June</option>
                                                            <option value="7" @if ($contact->anniversaryMonth == 7)selected @endif>July</option>
                                                            <option value="8" @if ($contact->anniversaryMonth == 8)selected @endif>August</option>
                                                            <option value="9" @if ($contact->anniversaryMonth == 9)selected @endif>September</option>
                                                            <option value="10" @if ($contact->anniversaryMonth == 10)selected @endif>October</option>
                                                            <option value="11" @if ($contact->anniversaryMonth == 11)selected @endif>November</option>
                                                            <option value="12" @if ($contact->anniversaryMonth == 12)selected @endif>December</option>                                                        
                                                        </select>

                                                        <select name="anniversaryDay" class="form-control m-r-xs" style="width: 10%; display: inline-block;">
                                                            
                                                            <option value=""> Day </option>
                                                            @for ($i = 1; $i <= 31; $i++)
                                                                <option value="{{ $i }}" @if ($contact->anniversaryDay == $i)selected @endif>{{ $i }}</option>
                                                            @endfor

                                                        </select>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input type="hidden" name="uid" value="{{ $userID }}">
                                                    <input type="hidden" name="domainID" value="{{ $domain['id'] }}">
                                                    <input type="hidden" name="cid" class="cid" id="cid" value="{{ $contact->id }}">
                                                    <button class="btn btn-primary" type="button" id="addressSubmit">Update Address Information</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" style="height: 24px;"></div>


<!-- Page Level Scripts -->

<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<script>

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });


 $('#contactSubmit').on('click',function(){
    
    formCheck = $('#contactForm')[0].checkValidity();

    if(!formCheck) {
        
        // check form validity - return form errors

        alert("All Fields Are Required. Please Complete All Fields");

        return false;

    }
      
        $.ajax({
            url: "/api/v1/contactsave",
            type: "POST",
            data: $('#contactForm').serialize(),
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                console.log(mydata);
                if (jsondata["cellData"] == "error") {
                    alert("The mobile number given is not a valid mobile number. Please provide a valid mobile number.");
                }
                alert(jsondata["message"]);
            },
            error: function() {alert("There was an error updating your information. Please try again.");}
        });

})


 $('#addressSubmit').on('click',function(){

        $.ajax({
            url: "/api/v1/contactaddresssave",
            type: "POST",
            data: $('#addressInformation').serialize(),
            success: function(mydata)
            {

                var jsondata = JSON.parse(mydata);
                console.log(mydata);
                alert(jsondata["message"]);
            },
            error: function() {console.log(jsondata); alert("There was an error updating your address information. Please try again.");}
        });

})


 $('#imageSubmit').on('click',function(){

    var fd = new FormData();    
        fd.append( 'newImage', $('#newImage')[0].files[0] );
        fd.append( 'cid', $('#cid').val() );
        fd.append( 'uid', $('#uid').val() );
        fd.append( 'domainID', {{ $domain['id'] }});

        $.ajax({
            url: "/api/v1/imagesave",
            type: "POST",
            data: fd,
            cache: false,
            processData: false, 
            contentType: false, 
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                console.log(mydata);
                alert(jsondata["message"]);
            },
            error: function() {console.log(jsondata); alert("There was an error updating user image. Please try again.");}
        });

})


</script>

@endsection