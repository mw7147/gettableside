@extends('admin.admin')

@section('content')

<h2>Create New Contact</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Add the contact information as necessary.</li>
    <li>Press "Create New Contact" to save the new contact.</li>
    <li>Press "Cancel" or select a menu item to return to cancel.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Create New Contact</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/{{Request::get('urlPrefix')}}/contacts/view" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Contacts</a>
        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif


        @if(Session::has('mobileError'))
        <div class="alert alert-warning alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('mobileError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">
            <form name="updateContacts" method="POST" action="/{{Request::get('urlPrefix')}}/contacts/new">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Required Information</p>
                </div>

                <div class="clearfix"></div>
        
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location:</label>
                    <select class="form-control" name="domainID">
                        @foreach ($locations as $location)
                        <option value='{{ $location->id }}'>{{ $location->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Zip Code:</label>
                    <input type="text" placeholder="Zip Code" minlength="5" class="form-control" id="zipCode" name="zipCode" value="{{ old('zipCode') }}" required>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Email (username):</label>
                    <input type="email" placeholder="Email" class="form-control" name="email" id="email" value="{{ old('email') }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Mobile Phone:</label>
                    <input class="form-control" type="tel" id="mobile" name="mobile" autocomplete="off" value="{{ old('mobile') }}" required placeholder="Mobile Phone">
                </div>

                <div class="clearfix"></div>


                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">First Name:</label>
                    <input type="text" placeholder="First Name" class="form-control" name="fname" value="{{ old('fname') }}" required>
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Last Name:</label>
                    <input class="form-control" type="text" name="lname" value="{{ old('lname') }}" required placeholder="Last Name">
                </div>

                <div class="clearfix"></div>           

                <hr> 

                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Optional Information</p>
                </div>
                <div class="clearfix"></div>           

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Birth Month:</label>
                    <select class="form-control" name="birthMonth">
                        <option value=''>Month</option>
                        @for ($i = 1; $i <= 12; $i++)
                        <option value='{{ $i }}'>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">BirthDay:</label>
                    <select class="form-control" name="birthDay">
                        <option value=''>Day</option>
                        @for ($i = 1; $i <= 31; $i++)
                        <option value='{{ $i }}'>{{ $i }}</option>
                        @endfor
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Address1:</label>
                    <input type="text" placeholder="Address1" class="form-control" name="address1" value="{{ old('address1') }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Address2:</label>
                    <input type="text" placeholder="Address2" class="form-control" name="address2" value="{{ old('address2') }}" >
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">City:</label>
                    <input class="form-control" type="text" name="city" value="{{ old('city') }}" placeholder="City">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">State:</label>
                    <select class="form-control" name="state" id="state">

                        @foreach ($states as $state)
                        <option value="{{ $state->stateCode }}" @if ($state->stateCode == old('state')) selected @endif>{{ $state->stateName }}</option>
                        @endforeach

                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                    <label class="font-normal font-italic">Company Name:</label>
                    <input type="text" placeholder="Company Name" class="form-control" name="companyName" value="{{ old('companyName') }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-lg">
                    <label class="font-normal font-italic">Title:</label>
                    <input class="form-control" type="text" id="title" name="title" value="{{ old('title') }}" placeholder="Title">
                </div>

                <div class="clearfix"></div>

     
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                        
                            {{ csrf_field() }}
                        <a href="/owner/systemusers/view" class="btn btn-white" type="submit">Cancel and Return</a>
                        <button class="btn btn-success" type="submit">Create New User</button>

                </div>



            </form>
        </div>
    </div>
</div>
<!-- Page Level Scripts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$('#email').on('change', function() {  // user name check
    var email = $("#email").val();
    var did = $("#did").val();
    var fd = new FormData();    
    fd.append('email', email);
    fd.append('did', did);
    $.ajax({
        url: "/api/v1/checkuseremail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert(jsondata['message']); 
                return false;   
                    
            } else {
                
                return true;
 
        }},
        // validation error
        error: function(data){
        var errors = data.responseJSON;
        $('#email').val('');
        $("#email").focus();
        alert(errors.errors.email);

      }
    });
});


$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


</script>


<script>

$( document ).ready(function() {
  console.log("ready");
  $("#mobile").mask("(999) 999-9999");
  $("#zipCode").mask("99999?-9999");
});

  
$('#mobile').change(function() {
  var num = $(this).val();
  var intNum =  num.replace(/[^\d]/g, '');

if ( intNum.length != 10 ) {
  $("#mobile").val(''); 
  alert("Only 10 digit phone numbers are allowed.");
  return false;
}

var fd = new FormData();
fd.append('mobile', $(this).val()),    

  $.ajax({
      url: "/api/v1/checkmobile",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] != "registered") {
            $('#mobile').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});




$('#did').change(function() {
  var did = $(this).val();

if ( did < 1 ) {
    $("#did").val(''); 
    $("#did").focus(); 
    alert("Only positive integers allowed.");
    return false;
}

var fd = new FormData();
fd.append('did', did),    

  $.ajax({
      url: "/api/v1/checkdid",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] == "error") {
            $('#did').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your Customer ID. Please try again.");
      }
  });
});

</script>



@endsection