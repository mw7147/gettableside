<!-- View Contacts -->
@extends('admin.adminRefreshContentOnly')

@section('content')
<style>
    .topBar {
        float: left;
        width: 100%;
        background: #fff;
        padding: 10px 0;
        margin-bottom: 10px;
    }
    .btn-instructions {
        padding: 5px 15px;
    }
    .btn-instructions .fa {
        font-size: 16px;
        font-weight: 800;
    }
    .export {
        font-size: 14px;
        font-weight: 600;
        text-transform: uppercase;
    }
    .border-bottom {
        margin-bottom: 22px;
    }
    .dispatach {
        float: left;
        width: 100%;
        margin-top: 5px;
    }
    .dispatach span {
        font-size: 14px;
        text-transform: uppercase;
        margin-bottom: 0;
    }
    .dispatach span a {
        color: red;
    }
    .logOut {
        font-size: 14px;
        font-weight: 600;
        margin-left: 20px;
    }
    .ibox-title {
        padding: 7px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }
    .ibox-title p {
        font-size: 14px;
        margin-bottom: 0px;
    }
    .cardHeader {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 10px;
    }
    .cardHeader h3 {
        margin: 0;
    }
    .blue-bg {
        border: medium none;
    }
    .dropdown-toggle {
        color: #555;
    }
    .dropdown-menu > li > a {
        color: #555;
        font-size: 14px;
        font-weight: 600;
    }
    .cardContent {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        padding: 10px;
        background: #fafafa;
    }
    .ibox {
        cursor: move;
    } 
    .cardContent p {
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 0;
    }
    .location i {
        font-size: 18px;
        font-weight: 600;
    }
    .cardFooter {
        padding: 10px 15px;
        background: #1ab394;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
        text-align: center;
    }
    .cardFooter p {
        color: #fff;
        text-transform: uppercase;
        font-size: 14px;
        font-weight: 600;
        margin-bottom: 0;
    }
    .dropzone {
        min-height: 527px;
        float: left;
        height: auto;
        width: 100%;
        padding: 10px 0;
        height: 100%;
        border: medium none;
    }
    .dropzone .ibox {
        width: 100%;
        float: left;
        clear: unset;
    }
    .refresh {
        display: flex;
        height: 65px;
        width: 65px;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
    }
    .dropZone .ibox {
        margin-bottom: 10px;
        margin-top: 10px;
    }
    #page-wrapper {
        padding: 0;
    }
    #draggable {
        margin-left: 0;
        margin-right: 0;
        padding-top: 20px;
        position: relative;
    } 
    .ibox-content {
        padding-left: 0;
        padding-right: 0;
        /* height: 100%; */
    }
    .newOrders {
        float: left;
        width: 100%;
        height: 100%;
    }
    .borderBottom {
        border-bottom: 1px solid #eeeeee;
        margin-bottom: 0;
        padding-bottom: 15px;
        margin-top: 0px !important;
    }
    .borderRight .dropToggle {
        float: left;
        border: 1px solid;
        width: 40px;
        padding: 3px 0;
        cursor: pointer;
        text-align: center;
        margin-bottom: 20px;
    }
    .borderLeft .dropToggle {
        float: right;
        border: 1px solid;
        width: 40px;
        padding: 3px 0;
        cursor: pointer;
        text-align: center;
        margin-bottom: 20px;
    }
    .row.left {
        margin: 0;
    }
    .borderRight {
        border-right: 1px solid;
        padding: 0;
    }
    .borderRight #droppable .col-sm-3.col-md-3.col-lg-3 {
        width: 96%;
        padding: 0;
        margin: 0 auto;
        position: relative;
    }
    .borderRight .navbar-default {
        min-height: 527px;
        padding-top: 20px;
        width: 100%;
        position: relative;
        z-index: unset;
    }
    .borderRight #droppable .ibox {
        margin-bottom: 10px;
    }
    .borderRight #droppable .ibox-title {
        padding: 5px;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        min-height: unset;
    }
    .borderRight #droppable .cardHeader {
        margin-bottom: 0;
    }
    .borderRight #droppable .ibox-title p {
        font-size: 10px;
        text-align: center;
        font-weight: bold;
        display: none;
    }
    .borderRight #droppable .cardContent {
        display: none;
    }
    .borderRight #droppable .cardFooter {
        display: none;
    }
    .borderRight #droppable {
        display: flex;
        flex-direction: column;
    }

    /** **/
    .row.right {
        margin: 0;
    }
    .borderLeft {
        border-left: 1px solid;
        padding: 0;
    }
    .borderLeft #dragedItem .col-sm-3.col-md-3.col-lg-3 {
        width: 96%;
        padding: 0;
        margin: 0 0 0 2%;
    }
    .borderLeft .navbar-default {
        min-height: 527px;
        /* border-left: 1px solid; */
        padding-top: 20px;
        width: 100%;
        position: relative;
        z-index: unset;
    }
    .borderLeft #dragedItem .ibox {
        margin-bottom: 10px;
    }
    .borderLeft #dragedItem .ibox-title {
        padding: 5px;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        min-height: unset;
    }
    .borderLeft #dragedItem .cardHeader {
        margin-bottom: 0;
    }
    .borderLeft #dragedItem .ibox-title p {
        font-size: 10px;
        text-align: center;
        font-weight: bold;
        display: none;
    }
    .borderLeft #dragedItem .cardContent {
        display: none;
    }
    .borderLeft #dragedItem .cardFooter {
        display: none;
    }
    #dropedIem {
        margin-left: 0;
        margin-right: 0;
        padding-top: 20px;
    }  
    .borderRight .ibox {
        cursor: pointer;
    }
    .borderLeft .ibox {
        cursor: pointer;
    }
    .hide {
        visibility: hidden;
        opacity: 0;
        transition: 0.5s;
        z-index: 9;
    }
    .show {
        visibility: visible !important;
        opacity: 1 !important;
        z-index: 9999 !important;
        transition: 0.5s !important;
    }
    .item {
        position: relative;
    }
    .tooltip1 {
        cursor: pointer;
        padding: 0px;
        position: fixed;
        left: 29%;
        top: 50%;
        transform: translate(-50%,-50%);
        visibility: hidden;
        opacity: 0;
        transition: 0.5s;
        z-index: 9999;
        width: 260px;
    }
    .tooltip2 {
        cursor: pointer;
        padding: 0px;
        position: absolute;
        right: 66%;
        top: 50%;
        transform: translate(-50%,-50%);
        visibility: hidden;
        opacity: 0;
        z-index: 9;
        width: 100%;
    }
    .tooltip1:after {
        content: "\f00d";
        position: absolute;
        background: #fff;
        width: 25px;
        height: 25px;
        border-radius: 25px;
        display: flex;
        align-items: center;
        justify-content: center;
        top: -15px;
        right: -15px;
        padding: 5px;
        z-index:988;
        font-weight: 900;
        display: flex;
        font: normal normal normal 14px/1 FontAwesome;
        align-items: center;
        justify-content: center;
    }
    .tooltip2:after {
        content: "\f00d";
        position: absolute;
        background: #fff;
        width: 25px;
        height: 25px;
        border-radius: 25px;
        display: flex;
        align-items: center;
        justify-content: center;
        top: -15px;
        right: -15px;
        padding: 5px;
        z-index:988;
        font-weight: 900;
        display: flex;
        font: normal normal normal 14px/1 FontAwesome;
        align-items: center;
        justify-content: center;
    }
    .tooltip1 .arrow-down {
        position: absolute;
        /* right: 0; */
        bottom: 50%;
        width: 0;
        height: 0;
        border-top: 15px solid transparent;
        border-right: 20px solid #1c84c6;
        border-bottom: 15px solid transparent;
        left: -7.5%;
        margin: 0 auto;
    }
    .tooltip2 .arrow-down {
        position: absolute;
        /* right: 0; */
        bottom: 50%;
        width: 0;
        height: 0;
        border-top: 15px solid transparent;
        border-left: 20px solid #1c84c6;
        border-bottom: 15px solid transparent;
        right: -9%;
        margin: 0 auto;
    }
    @media (max-width: 767px) {
        .borderBottom {
            padding-top: 15px;
            padding-right: 15px;
            padding-left: 15px;
        }
        .dropzone {
            border: 1px dashed #1ab394;
        }
        .body-small .navbar-static-side {
            display: block;
            width: 100%;
            position: unset;
            margin-top: 0;
            min-height: unset;
        }
        .borderRight .navbar-default {
            border-right: medium none;
        }
        .dropzone {
            min-height: unset;
            height: auto;
            margin-bottom: 20px;
            padding: 50px 0;
        }
        .borderRight #droppable {
            justify-content: center;
            align-items: center;
        }
        .borderLeft .dropZone {
            height: auto;
            min-height: unset;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin-right: 0;
        }
    }
    .tooltip1 .ibox-title p {
        display: block !important;
    }
    .tooltip1 .cardContent, .tooltip1 .cardFooter {
        display: flex !important;
    }
    .overlay {
        position: relative;
    }
    .overlay:before {
        content: '';
        width: 100%;
        height: 100%;
        position: fixed;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        background: rgba(0, 0, 0, 0.7019607843137254);
        z-index: 9;
        transition: 0.5s;
    }
</style>

<!-- Page-Level CSS -->
<!-- <div class="row border-bottom">
    <div class="topBar navbar navbar-static-top white-bg">
        <div class="col-sm-4 col-md-4">
        <a href="#" class="btn btn-danger btn-sm export">Expo</a>
        </div>
        <div class="col-sm-4 col-md-4 text-center">
            <a href="#" class="btn btn-primary btn-xs btn-instructions m-r-xs"><i class="fa fa-angle-double-left"></i></a>
            <a href="#" class="btn btn-primary btn-xs btn-instructions m-r-xs"><i class="fa fa-circle"></i></a>
            <a href="#" class="btn btn-primary btn-xs btn-instructions"><i class="fa fa-angle-double-right"></i></a>
        </div>
        <div class="col-sm-4 col-md-4 text-right">
            <div class="dispatach">
                <span>Print On Dispatch: <a href="#" class="m-r-xs">off</a></span>
                <a href="/userlogout" class="logOut"><i class="fa fa-sign-out m-r-xs"></i>Logout</a>
            </div>
        </div>
    </div>
</div> -->
<div class="newOrders">
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h3 class="borderBottom">Order List</h3>
            </div>
        </div>
        <div class="row left">
            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xs-12 borderRight">
                <nav class="navbar-default navbar-static-side">
                    <div class="dropToggle blue-bg" id="leftIcon">
                        <i class="fa fa-arrow-right"></i>
                    </div>
                    <div class="dropZone" id="droppable">
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tooltip1">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                        <div class="arrow-down"></div>
                    </div>
                </nav>
            </div>
            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xs-12">
                <div class="row" id="draggable">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row right">
            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xs-12">
                <div class="row" id="dropedIem">
                <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xs-12 borderLeft">
                <nav class="navbar-default navbar-static-side">
                    <div class="dropToggle blue-bg" id="rightIcon">
                        <i class="fa fa-arrow-left"></i>
                    </div>
                    <div class="dropZone" id="dragedItem">
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">Pending</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="ibox item">
                                <div class="ibox-title blue-bg">
                                    <div class="cardHeader">
                                        <h3>b-52</h3>
                                        <a href="#" class="btn btn-xs white-bg">00:00:49</a>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>Alyssa Garden #4971(GoTab)</p>
                                </div>
                                <div class="cardContent">
                                    <p>Twisted Tea - Can 12oz</p>
                                    <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                                </div>
                                <div class="cardFooter">
                                    <p>Waiting On Others Stations</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tooltip2">
                        <div class="ibox">
                            <div class="ibox-title blue-bg">
                                <div class="cardHeader">
                                    <h3>b-52</h3>
                                    <a href="#" class="btn btn-xs white-bg">Pending</a>
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Edit</a></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p>Alyssa Garden #4971(GoTab)</p>
                            </div>
                            <div class="cardContent">
                                <p>Twisted Tea - Can 12oz</p>
                                <a href="#" class="location"><i class="fa fa-location-arrow"></i></a>
                            </div>
                            <div class="cardFooter">
                                <p>Waiting On Others Stations</p>
                            </div>
                        </div>
                        <div class="arrow-down"></div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <a href="/{{Request::get('urlPrefix')}}/orderadmin/ordersview" class="btn btn-danger refresh"><i class="fa fa-refresh m-r-xs"></i>Refresh</a>
        <!-- <div class="clearfix"></div> -->
</div>

@endsection
<!-- <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'></link> -->
<!-- <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script> -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1556638668/Sortable.js"></script>
<script>
    $(document).ready(function() {
        var draggable = document.getElementById('draggable');
        var droppable = document.getElementById('droppable');
        new Sortable(draggable, {
            group: 'shared',
            animation: 1150,
        });
        new Sortable(droppable, {
            group: 'shared',
            animation: 1150,
        });
    });
    $(document).ready(function() {
        var draggable = document.getElementById('dragedItem');
        var droppable = document.getElementById('dropedIem');
        new Sortable(draggable, {
            group: 'shared',
            animation: 1150,
        });
        new Sortable(droppable, {
            group: 'shared',
            animation: 1150,
        });
    });
    $(document).ready(function(){
        $(".right").hide(300);
        $("#leftIcon").click(function(){
            $(".left").hide(500);
            $(".right").show(500);
        });
        $("#rightIcon").click(function(){
            $(".left").show(500);
            $(".right").hide(500);
        });
        $(".item").click(function() {
            $( ".tooltip1" ).removeClass( "hide" );
            $( ".tooltip1" ).addClass( "show" );
            $(".top-navigation").addClass("overlay")
            $( ".tooltip2" ).removeClass( "hide" ); 
            $( ".tooltip2" ).addClass( "show" );
            })
            $(".tooltip1").click(function() {
                $( ".tooltip1" ).removeClass( "show" );   
                $( ".tooltip1" ).addClass( "hide" );
                $(".top-navigation").removeClass("overlay")
             })
            $(".tooltip2").click(function() {
                $( ".tooltip2" ).removeClass( "show" );   
                $( ".tooltip2" ).addClass( "hide" );
                $(".top-navigation").removeClass("overlay")
            })
    });
</script>