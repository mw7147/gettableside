<!-- Order Detail Modal -->

<style>

.modalHeading {
  font-size: 14px;
  font-style: italic;
}

.custDetail p {
  margin-bottom: 10px;
  font-weight: 600;
}

.custDetail small {
  font-style: italic;
  width: 100%;
  color: #666;
  font-size: 80%;
  text-decoration: underline;
}

.custDetail img {
  margin-top: 8px;
  max-width: 90%;
}

</style>

<div class="clearfix"></div>

<!-- Modal -->

  <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="orderDetail">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title" id="orderID"></h3>
        </div>

        <div class="modal-body custDetail">

          <small >Status:</small>
          <p id="status"></p>

          <small >Name:</small>
          <p id="name"></p>
           
          <small >Email Address:</small>
          <p id="email"></p>

          <small >Mobile Phone:</small>
          <p id="mobile"></p>

          <small >Credit Card Type:</small>
          <p id="ccType"></p>

          <small >Credit Card Last 4:</small>
          <p id="ccLast4"></p>

          <table class="table table-bordered" style="max-width: 250px;">
            <tr>
              <td width="80">Subtotal</td>
              <td width="50" id="sub"></td>
            </tr>
            <tr>
              <td>Tax</td>
              <td id="tax"></td>
            </tr>
            <tr>
              <td>Tip</td>
              <td id="tip"></td>
            </tr>
            <tr>
              <td>Delivery Charge</td>
              <td id="delivery"></td>
            </tr>
            <tr>
              <td>Refund Amount</td>
              <td id="refund"></td>
            </tr>
            <tr>
              <td>Net Order</td>
              <td id="total"></td>
            </tr>
          </table>

          <small >Waiter ID:</small>
          <p id="waiterID"></p>

          <small >Table ID:</small>
          <p id="tableID"></p>

          <small >Credit Card Transaction ID:</small>
          <p id="ccTransactionID"></p>

          <small >Order IP:</small>
          <p id="orderIP"></p>

          <small >User Agent:</small>
          <p id="userAgent"></p>

          <small >Session ID:</small>
          <p id="sessionID"></p>

          <small >Order Refunded:</small>
          <p id="refunded"></p>

          <small class="rid">Refund ID:</small>
          <p class="rid" id="refundID"></p>


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-success w100" data-dismiss="modal">Done</button>     
        </div>

      </div>
    </div>
  </div>

<script>

  function showDetail(orderID) {

      var fd = new FormData();
      fd.append('orderID', orderID),    

      $.ajax({
          url: "/api/v1/admin/orderdetail",
          type: "POST",
          data: fd,
          contentType: false,
          cache: false,
          processData:false,   
          success: function(mydata) {
              
              if (mydata['orderRefunded'] == 1) {
                $('#status').html("Refunded");
              } else if (mydata['orderInProcess'] == 1) {
                $('#status').html("Order In processData");
              } else {
                $('#status').html("Order Ready");
              }
              
              var netOrder = parseFloat(mydata['total']) - parseFloat(mydata['orderRefundAmount']);

              $('#orderID').html("Order ID: " + mydata['id']);
              $('#name').html(mydata['fname'] + ' ' + mydata['lname']);
              $('#email').html(mydata['customerEmailAddress']);
              $('#mobile').html(mydata['customerMobile']);
              $('#ccType').html(mydata['paymentType']);
              $('#ccLast4').html(mydata['ccTypeLast4']);
              $('#ccTransactionID').html(mydata['ccTypeTransactionID']);
              $('#sub').html("$" + mydata['subTotal']);
              $('#tax').html("$" + mydata['tax']);
              $('#tip').html("$" + mydata['tip']);
              $('#delivery').html("$" + mydata['deliveryCharge']);

              if ( parseFloat(mydata['orderRefundAmount']) > 0) {
                $('#refund').html("($" + mydata['orderRefundAmount'] + ')');
              } else {
                $('#refund').html("$" + mydata['orderRefundAmount']);
              }

              $('#total').html("$" + netOrder.toFixed(2));
              $('#waiterID').html(mydata['waiterID']);
              $('#tableID').html(mydata['tableID']);
              $('#userAgent').html(mydata['userAgent']);
              $('#orderIP').html(mydata['orderIP']);
              $('#sessionID').html(mydata['sessionID']);
              
              if (mydata['orderRefunded'] == 1) {
                $('#refunded').html('Yes');
              } else {
                $('#refunded').html('No');
              }
              
              if (mydata['orderRefunded'] == 1) {
                $('.rid').show();
                $('#refundID').html(mydata['stripeRefundID']);
              } else {
                $('.rid').hide();
              }


              $('#detail').modal('show');

          },
          error: function() {
              alert("There was an problem retrieving the order information. Please try again.");
          }
    }); 

  };

</script>

<!-- end Order Detail Modal -->