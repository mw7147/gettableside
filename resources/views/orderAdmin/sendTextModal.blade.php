<!-- Send Text Message Modal -->

<style>

.modalHeading {
  font-size: 14px;
  font-style: italic;
}

.custDetail p {
  margin-bottom: 10px;
  font-weight: 600;
}

.custDetail small {
  font-style: italic;
  width: 100%;
  color: #666;
  font-size: 80%;
  text-decoration: underline;
}

.custDetail img {
  margin-top: 8px;
  max-width: 90%;
}

</style>

<div class="clearfix"></div>

<!-- Modal -->

  <div class="modal fade" id="sendText" tabindex="-1" role="dialog" aria-labelledby="sendTextMessage">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title" id="modalContactID"></h3>
        </div>

        <div class="modal-body custDetail">

          <form method="post" name="sendTextMessage">
            <label class="font-normal font-italic">Enter Text Message (100 Characters Max):</label>

            <textarea class="form-control" id="message" name="message" onkeyup="charCount()"></textarea>
                    <div class="clearfix"></div>

            <input type="hidden" name="cid" id="cid" value=''>
          </form>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-success w100" data-dismiss="modal">Cancel</button>
          <button type="button" id="sendTextButton" class="btn btn-primary w100">Send Text</button>      
        </div>

      </div>
    </div>
  </div>

<script>

function sendText(contactID, fname, lname){
  $('#modalContactID').html("Send Text Message To " + fname + ' ' + lname);
  $('#cid').val(contactID);
  $('#sendText').modal('show');
}

function charCount(){
    var maxLength = 100;
    var message = $('#message').val();
    var length = message.length;
    if (length <= maxLength) { $('#countChar').html(length); return true;}
    alert("You have reached the maximum character limit of " + maxLength + " characters." );
    var limit = message.substring(0, maxLength);
    $(this).val(limit);
    return true;
};

$('#sendTextButton').click(function() {

var msg = $('#message').val();
var cid = $('#cid').val();
$('#sendText').modal('hide');

var fd = new FormData();
fd.append('msg', msg),    
fd.append('cid', cid),    
  $.ajax({
      url: "/api/v1/admin/sendcontacttext",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
          console.log(mydata);
          $('#message').val('');
          alert("A text message has been sent.");

          return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});


</script>

<!-- end Send Text Message Modal -->