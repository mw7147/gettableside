<!-- Groups View -->
@extends('admin.admin')

@section('content')
<meta http-equiv="Cache-control" content="no-cache">
    
<!-- Page-Level CSS -->


<h2>Menu Image Management</h2>

<button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>

<ul class="instructions">
    <li>Add and delete images as desired.</li>
    <li>Small images should be 320 X 180 pixels in size.</li>
    <li>Large images should be 1280 X 720 pixels.</li>
</ul>

<div class="ibox-content col-xs-12 col-sm-12">

    <h3>Product Images for Menu Item ID: {{ $menuDataID }}</h3>

    <hr>
    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

    <div class="clearfix"></div>

        @if(Session::has('groupSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('groupError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div>

    <style>
    .empty-icon {
        background-color: #efefef;
        width: 320px;
        height: 180px;
    }

    .empty-icon i {
        text-align: center;
        width: 320px;
        font-size: 128px;
        margin-top: 8%;
    }

    .cat-image-bottom {
        border-bottom: none !important;
    }

    @media (max-width: 480px) {

        .ibox-content {
            padding: 0px 0px 0px 5px;
        }

        .empty-icon,
        .empty-icon i {
            width: 100%;
        }
    }
    </style>

    @php if (count($menuImages) > 1) {$counter = count($menuImages);} else {$counter = 1;} @endphp
    @for ($i = 1; $i <= env('MAX_MENU_ITEM_IMAGES'); $i++)


    <div class="col-md-12 image{{ $i }}">
        <h4>Product Image {{ $i }}</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">
                            <button type="button" id="changeButton{{ $i }}" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" onclick="changeSmall({{ $i }})" >Change</button>
                            <input type="text" class="form-control" id="imageSmall{{ $i }}" style="max-width: 65%;" name="imageSmall{{ $i }}" value="">
                            </span>
                            <input type="file" class="newImage" data-size="small" data-imageID="{{ $i }}" id="imageSmallClick{{ $i }}" style="display: none;">
                        </div>
                        <div class="input-group m-b">
                            <label class="font-normal font-italic m-t-sm">Sort Order</label>
                            <select name="imageOrder{{ $i }}" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">
                            <button type="button" id="changeButton{{ $i }}" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" onclick="changeLarge({{ $i }})">Change</button>
                            <input type="text" class="form-control" id="imageLarge{{ $i }}" data-menuImageID="{{ " style="max-width: 65%;" name="imageLarge{{ $i }}" value="">
                           </span>
                            <input type="file" class="newImage" data-size="large" data-imageID="{{ $i }}" id="imageLargeClick{{ $i }}" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (env('MAX_MENU_ITEM_IMAGES') > 1 && $i >1)
    <div class="clearfix"></div>
    <button type="button" class="btn btn-primary m-l-md imageButton{{ $i }}" onclick="addImage({{ $i }})"><i class="fa fa-plus-square m-r-xs"></i>Add Image</button>
    <div class="clearfix"></div>   
    @endif

    @endfor






</div></div>

<!-- Page-Level Scripts -->

<script type="text/javascript" >

$(document).ready(function(){
    
    @if (env('MAX_MENU_ITEM_IMAGES') > 1)
    @for ($i = $counter+1; $i <= env('MAX_MENU_ITEM_IMAGES'); $i++)
        $('.image{{$i}}').hide();
        @if ($i > 2) $('.imageButton{{$i}}').hide(); @endif
    @endfor
    @endif

    });


    function addImage(imageID){
        var i = 'image' + imageID;
        var jb = 'imageButton' +(imageID + 1);
        var ib = 'imageButton' + imageID;
        $( '.' + i ).toggle();
        $( '.' + jb ).show();
        $( '.' + ib ).toggle();

    }

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    function changeSmall(imageID) {
        var image = '#imageSmallClick' + imageID; 
        $(image).trigger("click");
    };

    function changeLarge(imageID) {
        var image = '#imageLargeClick' + imageID; 
        $(image).trigger("click");
    };


    $('.newImage').on('change',function(){
        var imageSize = this.getAttribute('data-size');
        var imageID = this.getAttribute('data-imageID');
        var img = this.files[0];
        if (img.size/1000 > {{ env('MAX_IMAGE_SIZE') }} ) {
            var maxFileSize = {{ env('MAX_IMAGE_SIZE') }};
            mfs = maxFileSize.toLocaleString(undefined, {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            });            
            alert("The filesize cannot be larger than " + mfs + "Kb. Please check your file and try again.");
            return false;
        }

        // check filetype
        var imageTypes = ['image/png', 'image/jpg', 'image/jpeg'];
        var imageType = imageTypes.indexOf(img.type);
        if (imageType == -1 ) {            
            alert("The file must be a jpg or png filetype. Please check your file and try again.");
            return false;
        }

        console.log(img);
        return false;


        var existingName = $("#existingName").val();
        var fd = new FormData();    
        fd.append('img', img);
        fd.append('domainID', domainID);
        fd.append('existingName', existingName)

        $.ajax({
            url: "/api/v1/admin/menuImage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $('#img').val('');
                    alert(jsondata['message']); 
                    return false;     
                } else {
                    $('#img').val(jsondata['fileName']);
                    src='/storage/logoImages/' + domainID + '/' + jsondata['fileName'];
                    $('#logo').attr("src", src);
                    $('#existingName').val(jsondata['fileName']);
                    return true;    
            }}
        });

    });


</script>

 

@endsection