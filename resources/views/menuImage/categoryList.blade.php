<!-- Groups View -->
@extends('admin.admin')

@section('content')
<meta http-equiv="Cache-control" content="no-cache">
    
<!-- Page-Level CSS -->


<h2>Menu Image Management</h2>

<button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>

<ul class="instructions">
    <li>Add and delete images as desired.</li>
    <li>Small images should be 320 X 180 pixels in size.</li>
    <li>Large images should be 1280 X 720 pixels.</li>
</ul>

<div class="ibox-content col-xs-12 col-sm-12">

    <h3>Product Images for Menu Item ID:</h3>

    <hr>
    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

    <div class="clearfix"></div>

        @if(Session::has('groupSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('groupError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div>

    <style>
    .empty-icon {
        background-color: #efefef;
        width: 320px;
        height: 180px;
    }

    .empty-icon i {
        text-align: center;
        width: 320px;
        font-size: 128px;
        margin-top: 8%;
    }

    .cat-image-bottom {
        border-bottom: none !important;
    }

    @media (max-width: 480px) {

        .ibox-content {
            padding: 0px 0px 0px 5px;
        }

        .empty-icon,
        .empty-icon i {
            width: 100%;
        }
    }
    </style>


    <div class="col-md-12">
        <h4>Category Image</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeCategoryImage" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-md-12">
        <h4>Product Image 1</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <button type="button" class="btn btn-primary m-l-md imageButton2" onclick="addImage(2)"><i class="fa fa-plus-square m-r-xs"></i>Add Image</button>
    <div class="clearfix"></div>


    <div class="col-md-12 image2">
        <h4>Product Image 2</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="clearfix"></div>
    <button type="button" class="btn btn-primary m-l-md imageButton3" onclick="addImage(3)"><i class="fa fa-plus-square m-r-xs"></i>Add Image</button>
    <div class="clearfix"></div>


    <div class="col-md-12 image3">
        <h4>Product Image 3</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <button type="button" class="btn btn-primary m-l-md imageButton4" onclick="addImage(4)"><i class="fa fa-plus-square m-r-xs"></i>Add Image</button>
    <div class="clearfix"></div>


    <div class="col-md-12 image4">
        <h4>Product Image 4</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <button type="button" class="btn btn-primary m-l-md imageButton5" onclick="addImage(5)"><i class="fa fa-plus-square m-r-xs"></i>Add Image</button>
    <div class="clearfix"></div>


    <div class="col-md-12 image5">
        <h4>Product Image 5</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">

                            <button type="button" id="changeLogoButton" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" >Change</button>

                            <input type="text" class="form-control" id="catImage" style="max-width: 65%;" name="catImage" value="">
                            </span>
                            <input type="file" id="categoryImage" style="display: none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



















</div></div>

<!-- Page-Level Scripts -->

<script type="text/javascript" >

$(document).ready(function(){
        $('.image2').hide();
        $('.image3').hide();
        $('.imageButton3').hide();
        $('.image4').hide();
        $('.imageButton4').hide();
        $('.image5').hide();
        $('.imageButton5').hide();

    });


    function addImage(imageID){
        var i = 'image' + imageID;
        var jb = 'imageButton' +(imageID + 1);
        var ib = 'imageButton' + imageID;
        $( '.' + i ).toggle();
        $( '.' + jb ).show();
        $( '.' + ib ).toggle();

    }

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });


    $("#changeCategoryImage").on("click", function() {
        $("#categoryImage").trigger("click");
    });


    $('#newLogo').on('change',function(){
        
        var sdi = this.files[0];
        var domainID = {{$domain->id}};
        // check filesize
        if (sdi.size > 50000) {
            alert("The filesize cannot be larger than 50Kb. Please check your file.");
            $('#stripeDataImage').val('');
            return false;
        }
        // check filetype
        if (sdi.type != "image/png") {
            alert("The image must be a png. Please check your file.");
            $('#stripeDataImage').val('');
            return false;
        }

        var existingName = $("#existingName").val();
        var fd = new FormData();    
        fd.append('sdi', sdi);
        fd.append('domainID', domainID);
        fd.append('existingName', existingName)

        $.ajax({
            url: "/api/v1/admin/stripedataimage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $('#sdi').val('');
                    alert(jsondata['message']); 
                    return false;     
                } else {
                    $('#sdi').val(jsondata['fileName']);
                    src='/storage/logoImages/' + domainID + '/' + jsondata['fileName'];
                    $('#logo').attr("src", src);
                    $('#existingName').val(jsondata['fileName']);
                    return true;    
            }}
        });

    });


</script>

 

@endsection