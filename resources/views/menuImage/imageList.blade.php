<!-- Groups View -->
@extends('admin.admin')

@section('content')
<meta http-equiv="Cache-control" content="no-cache">
    
<!-- Page-Level CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css" />

<h2>Menu Image Management</h2>

<button class="btn btn-primary btn-xs btn-instructions m-b-sm" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-b-sm" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>

<ul class="instructions">
    <li>Add and delete images as desired.</li>
    <li>Small images should be 320 X 180 pixels in size.</li>
    <li>Large images should be 1280 X 720 pixels.</li>
</ul>

<div class="ibox-content col-xs-12 col-sm-12">

    <h3>Product Images for Menu Item ID: {{ $menuDataID }}</h3>

    <hr>
    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-b-lg  w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="#" class="btn btn-info btn-xs m-b-lg  w110"  onclick="history.back(-1);"><i class="fa fa-reply m-r-xs"></i>Previous Page</a>

    <div class="clearfix"></div>

        @if(Session::has('groupSuccess'))
            <div class="alert alert-success alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupSuccess') }}
            </div>
            <div class="clearfix"></div>
        @endif

        @if(Session::has('groupError'))
            <div class="alert alert-danger alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('groupError') }}
            </div>
            <div class="clearfix"></div>
        @endif

    <div>

    @php if ($menuImages > 1) {$counter = $menuImages;} else {$counter = 1;} @endphp
    @for ($i = 1; $i <= env('MAX_MENU_ITEM_IMAGES'); $i++)

    <div class="col-md-12 image{{ $i }}">
        <h4>Product Image {{ $i }}</h4>
        <div class="ibox-content">
            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">

                    @if (empty($imagesSmall[$i]['id']))
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    @else
                    <div class="pull-left menuImageListSmall col-md-5 col-sm-12 col-xs-12">
                        <a data-lightbox="image-1" data-title="Small Product Image {{ $i }}" href="{{ env('PRIVATE_MENU_IMAGES') . $domain->id . '/' . $imagesSmall[$i]['menuID'] . '/Small/' . $imagesSmall[$i]['fileName']}}">
                            <img src="{{ env('PRIVATE_MENU_IMAGES') . $domain->id . '/' . $imagesSmall[$i]['menuID'] . '/Small/' . $imagesSmall[$i]['fileName']}}" >
                        </a>
                    </div>
                    @endif


                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Small - 320 X 180 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <div class="panel panel-default">
                                <div class="panel-heading">Select Profile Image</div>
                                <div class="panel-body" align="center">
                                    <input type="file" data-size="Small" data-imageID="{{ $i }}" data-menuImageID="{{ $imagesSmall[$i]['id'] }}" name="upload_image" id="imageSmallClick{{ $i }}" onchange="changeSmall({{ $i }})" accept="image/*" />
                                    <br />
                                    <div id="uploaded_image{{ $i }}"></div>
                                </div>
                                <a id="deleteButton{{ $i }}" class="btn btn-default pull-left" style="min-width: 35%; margin-left: 0px;" onclick="deleteImage({{ $imagesSmall[$i]['id'].',' . $imagesLarge[$i]['id'] }})" ><i class="fa fa-times m-r-xs"></i>Delete Product Image {{ $i }}</a>
                            </div>
                        </div>
                        <div class="input-group m-b">
                            <label class="font-normal font-italic m-t-sm">Sort Order</label>
                            <select id="sortOrder{{ $i }}" name="sortOrder{{ $i }}" class="form-control">
                                @for ($j = 1; $j <= env('MAX_MENU_ITEM_IMAGES'); $j++)
                                <option value="{{ $j }}" @if ($j == $i) selected @endif>{{ $j }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="feed-activity-list cat-image-bottom">
                <div class="feed-element cat-image-bottom">

                    @if (empty($imagesLarge[$i]['id']))
                    <div class="pull-left empty-icon col-md-5 col-sm-12 col-xs-12">
                        <span><i class="fa fa-photo"></i></span>
                    </div>
                    @else
                    <div class="pull-left menuImageListLarge col-md-5 col-sm-12 col-xs-12">
                        <a data-lightbox="image-1" data-title="Large Product Image {{ $i }}" href="{{ env('PRIVATE_MENU_IMAGES') . $domain->id . '/' . $imagesLarge[$i]['menuID'] . '/Large/' . $imagesLarge[$i]['fileName']}}">
                            <img src="{{ env('PRIVATE_MENU_IMAGES') . $domain->id . '/' . $imagesLarge[$i]['menuID'] . '/Large/' . $imagesLarge[$i]['fileName']}}" >
                        </a>
                    </div>
                    @endif

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic m-t-sm">Large - 1280 X 720 Pixel PNG or JPG file</label>
                        <div class="input-group m-b">
                            <span class="input-group-btn">
                            <button type="button" id="changeButton{{ $i }}" class="btn btn-default pull-left" style="max-width: 35%; margin-left: 0px;" onclick="changeLarge({{ $i }})">Change</button>
                            <input type="text" class="form-control" id="imageLarge{{ $i }}" data-menuImageID="{{ $imagesLarge[$i]['id'] }}" style="max-width: 65%;" name="imageLarge{{ $i }}" value="{{ $imagesLarge[$i]['fileName'] }}">
                           </span>
                            <input type="file" class="newImage" data-size="Large" data-imageID="{{ $i }}" id="imageLargeClick{{ $i }}" style="display: none;">
                        </div>

                            <a id="deleteButton{{ $i }}" class="btn btn-default pull-left" style="min-width: 35%; margin-left: 0px;" onclick="deleteImage({{ $imagesSmall[$i]['id'].',' . $imagesLarge[$i]['id'] }})" ><i class="fa fa-times m-r-xs"></i>Delete Product Image {{ $i }}</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="uploadimageModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title">Upload & Crop Image</h4>
               </div>
               <div class="modal-body">
                 <div class="row">
              <div class="col-md-8 text-center">
               <div id="imageCrop" style="width:350px; margin-top:30px"></div>
              </div>
              <div class="col-md-4" style="padding-top:30px;">
               <br />
               <br />
               <br/>
              <button class="btn btn-success" onclick="cropImage({{ $i }})">Crop & Upload Image</button>
            </div>
           </div>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
        </div>
    </div>

    @if (env('MAX_MENU_ITEM_IMAGES') > 1 && $i>=$counter && $i<env('MAX_MENU_ITEM_IMAGES'))
    <div class="clearfix"></div>
    <button type="button" class="btn btn-primary m-l-md imageButton{{ $i+1 }}" onclick="addImage({{ $i+1 }})"><i class="fa fa-plus-square m-r-xs"></i>Add Image</button>
    <div class="clearfix"></div>   
    @endif

    @endfor






</div>

<!-- Page-Level Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js"></script>
<script type="text/javascript" >

$(document).ready(function(){
    
    @if (env('MAX_MENU_ITEM_IMAGES') > 1)
    @for ($i = $counter+1; $i <= env('MAX_MENU_ITEM_IMAGES'); $i++)
    console.log({{$i}});
        $('.image{{$i}}').hide();
        @if ($i > $counter+1) $('.imageButton{{$i}}').hide(); @endif
    @endfor
    @endif

    });


    function addImage(imageID){
        var i = 'image' + imageID;
        var jb = 'imageButton' +(imageID + 1);
        var ib = 'imageButton' + imageID;
        $( '.' + i ).toggle();
        $( '.' + jb ).show();
        $( '.' + ib ).toggle();

    }

    $('.btn-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    $('.btn-up-instructions').on('click',function(){

        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();

    });

    function changeSmall(imageID) {
        var image = document.getElementById("imageSmallClick"+imageID);
        var reader = new FileReader();
        reader.onload = function (event) {
          $image_crop.croppie('bind', {
            url: event.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(image.files[0]);
        $('#uploadimageModal').modal('show');
    };

    function cropImage(imageID){
        var image = document.getElementById("imageSmallClick"+imageID);
        var imageSize = image.getAttribute('data-size');
        var imageID = image.getAttribute('data-imageID');
        var eid = $("#imageSmallClick"+imageID).attr('data-menuImageID');
        var so = $("#sortOrder"+imageID).val();
        $image_crop.croppie('result', {
          type: 'canvas',
          size: 'viewport'
        }).then(function(response){
            var fd = new FormData();    
            fd.append('image', response);
            fd.append('imageSize', imageSize);
            fd.append('eid', eid);
            fd.append('domainID', {{ $domain->id }});    
            fd.append('menuDataID', {{ $menuDataID }});
            fd.append('sortOrder', so);
            $.ajax({
                url: "/api/v1/admin/menuImage",
                type: "POST",
                data: fd,
                contentType: false,
                cache: false,
                processData:false,
                success: function(mydata)
                {
                    console.log(mydata);
                    $('#uploadimageModal').modal('hide');
                    var jsondata = JSON.parse(mydata);
                    if (jsondata["status"] == "error") {
                        $('#img').val('');
                        alert(jsondata['message']); 
                        return false;     
                    } else {
                        location.reload();
                        return true;    
                    }
                }
            });
        });
    }

    function changeLarge(imageID) {
        var image = '#imageLargeClick' + imageID; 
        $(image).trigger("click");
    };


    $('.newImage').on('change',function(){
        var imageSize = this.getAttribute('data-size');
        var imageID = this.getAttribute('data-imageID');
        var img = this.files[0];
       
       // check fileSize
        if (img.size/1000 > {{ env('MAX_IMAGE_SIZE') }} ) {
            var maxFileSize = {{ env('MAX_IMAGE_SIZE') }};
            mfs = maxFileSize.toLocaleString(undefined, {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            });            
            alert("The filesize cannot be larger than " + mfs + "Kb. Please check your file and try again.");
            return false;
        }

        // check filetype
        var imageTypes = ['image/png', 'image/jpg', 'image/jpeg'];
        var imageType = imageTypes.indexOf(img.type);
        if (imageType == -1 ) {            
            alert("The file must be a jpg or png filetype. Please check your file and try again.");
            return false;
        }
        
        // get existing image ID
        var existingID = "#image" + imageSize + imageID;
        var eid = $(existingID).attr('data-menuImageID');;

        // get sort order
        var sortOrder = "#sortOrder" + imageID;
        var so = $(sortOrder).val();
        
        console.log(img);
        var domainID = {{ $domain->id }};
        var fd = new FormData();    
        fd.append('img', img);
        fd.append('imageSize', imageSize);
        fd.append('eid', eid);
        fd.append('domainID', domainID);    
        fd.append('menuDataID', {{ $menuDataID }});
        fd.append('sortOrder', so);

        $.ajax({
            url: "/api/v1/admin/menuImage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                console.log(mydata);
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $('#img').val('');
                    alert(jsondata['message']); 
                    return false;     
                } else {
                    location.reload();
                    return true;    
            }}
        });

    });

    function deleteImage(smallID, largeID) {
        alert("deleting");
        var fd = new FormData();    
        fd.append('smallID', smallID);
        fd.append('largeID', largeID);
        fd.append('domainID', {{ $domain->id }});    
        fd.append('menuDataID', {{ $menuDataID }});

        $.ajax({
            url: "/api/v1/admin/deleteMenuImage",
            type: "POST",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(mydata)
            {
                console.log(mydata);
                var jsondata = JSON.parse(mydata);
                if (jsondata["status"] == "error") {
                    $('#img').val('');
                    alert(jsondata['message']); 
                    return false;     
                } else {
                   // location.reload();
                    return true;    
            }}
        });    
    };


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.js"></script>
 
<script>  
    $(document).ready(function(){
    
     $image_crop = $('#imageCrop').croppie({
        enableExif: true,
        viewport: {
          width:200,
          height:200,
          type:'square' //circle
        },
        boundary:{
          width:300,
          height:300
        }
      });
    });  
</script>
@endsection