@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h2>Restore Receipt</h2>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Provide requested information.</li>
    <li>Press "Cancel" or select a menu item to return to the Settings Dashboard.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>Restore Receipt</i></h5>
    </div>
        
    <div class="ibox-content">

    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w90"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-cog m-r-xs"></i>Settings</a>
    <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif


        <div class="clearfix"></div>
        <div class="row">
            <form name="restoreReceipt" id="restoreReceipt" method="POST" action="/admin/restore">
                
                <div class="col-md-4 col-sm-6 col-xs-6 m-b-md">
                    <label class="font-normal font-italic">Order ID (Stripe Only):</label>
                    <input type="number"  placeholder="Order ID" min="0" max="99999999" class="form-control" required name="orderID" value="">
                </div>

                <div class="clearfix"></div>
                

                {{ csrf_field() }}

                <div class="form-group m-t-lg">
                    <div class="col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-white text-center" type="submit">Cancel</a>
                        <button class="btn btn-primary" type="submit">Restore Receipt</button>
                    </div>
                </div>
            </form>

        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->

<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection