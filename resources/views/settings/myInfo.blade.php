@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">



<h1>System User Information</h1>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Update the User information as necessary.</li>
    <li>The email address will be used as the username on the login screen.</li>
    <li>The password may be changed on the password reset page.</li>
    <li>Inactive users are retained in the system but lose login and viewing privileges.</li> f
    <li>Press "Update Information" to save the updated information.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating.</li>
    <li>NOTE: If systemUseris "Inactive" no messages will be sent to users on systemUsers behalf.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Name:</i> {{ $user->fname }} {{ $user->lname }} - User ID: {{ $user->id }}</h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-cog m-r-xs"></i>Settings</a>
        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif


        @if(Session::has('mobileError'))
        <div class="alert alert-warning alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('mobileError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="updateSystemUser" method="POST" action="/settings/{{Request::get('urlPrefix')}}/myinfo">
            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Required Information</p>
            </div>

            <div class="clearfix"></div>

            


            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">First Name:</label>
                <input type="text" placeholder="First Name" class="form-control" name="fname" value="{{ $user->fname }}" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Last Name:</label>
                <input class="form-control" type="text" name="lname" value="{{ $user->lname }}" required placeholder="Last Name">
            </div>

            <div class="clearfix"></div>  

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Email (username):</label>
                <input type="email" placeholder="Email" class="form-control" name="email" id="email" value="{{ $user->email }}" required>
            </div>

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Mobile Phone:</label>
                <input class="form-control" type="tel" id="mobile" name="mobile" autocomplete="off" value="{{ $user->mobile }}" required placeholder="Mobile Phone">
            </div>

            <div class="clearfix"></div>

         

            <hr> 

            <div class="col-md-6 col-sm-9 col-xs-12">
                <p class="font-italic font-bold">Optional Information</p>
            </div>
            <div class="clearfix"></div>           

            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Birth Month:</label>
                <select class="form-control" name="birthMonth">
                    <option value=''>Month</option>
                    @for ($i = 1; $i <= 12; $i++)
                    <option value="{{ $i }}" @if ($user->birthMonth == $i)selected @endif>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            
            <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                <label class="font-normal font-italic">BirthDay:</label>
                <select class="form-control" name="birthDay">
                    <option value=''>Day</option>
                    @for ($i = 1; $i <= 31; $i++)
                    <option value="{{ $i }}" @if ($user->birthDay == $i)selected @endif>{{ $i }}</option>
                    @endfor
                </select>
            </div>

 
            <div class="col-md-10 col-sm-9 col-xs-12 m-b-lg text-center">
                    
                        {{ csrf_field() }}
                    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Information</button>

            </div>



</form>
<!-- Page Level Scripts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>

$('#email').on('change', function() {  // user name check
    var email = $("#email").val();
    var uid = {{$user->id}}
    var did = $("#did").val();
    var fd = new FormData();    
    fd.append('email', email);
    fd.append('did', did);
    fd.append('uid', uid);
    $.ajax({
        url: "/api/v1/checkuseremail",
        type: "POST",
        data: fd,
        contentType: false,
        cache: false,
        processData:false,
        success: function(mydata)
        {

            var jsondata = JSON.parse(mydata);
            if (jsondata["status"] == "error") {
                $("#email").val("");
                $("#email").focus();
                alert(jsondata['message']); 
                return false;   
                    
            } else {
                
                return true;
 
        }},
        // validation error
        error: function(data){
        var errors = data.responseJSON;
        $('#email').val('');
        $("#email").focus();
        alert(errors.errors.email);

      }
    });
});


$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});


</script>


<script>

$( document ).ready(function() {
  console.log("ready");
  $("#mobile").mask("(999) 999-9999");
  $("#zipCode").mask("99999?-9999");
});
  
$('#mobile').change(function() {
  var num = $(this).val();
  var intNum =  num.replace(/[^\d]/g, '');

if ( intNum.length != 10 ) {
  $("#mobile").val(''); 
  alert("Only 10 digit phone numbers are allowed.");
  return false;
}

var fd = new FormData();
fd.append('mobile', $(this).val()),    

  $.ajax({
      url: "/api/v1/checkmobile",
      type: "POST",
      data: fd,
      contentType: false,
      cache: false,
      processData:false,   
      success: function(mydata) {
        var data = JSON.parse(mydata);           
        console.log(data);
        if (data["status"] != "registered") {
            $('#mobile').val('');
            alert(data["message"]);
            return false;
        }
        return true;
      },
      error: function() {
        alert("There was an problem with your mobile number. Please try again.");
      }
  });
});

</script>

@endsection