@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h1>Edit Customer Email Settings</h1>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>


<h4>
        {{ $domain->name }} Email Settings.
</h4>

<ul class="m-b-md instructions">
    <li>Fill in the style as necessary.</li>
    <li>Press "Cancel" or select a menu item to return to the settings menu.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>{{ $domain ->name }} Customer ID: {{ $domain->id }}</i></h5>
    </div>
        
    <div class="ibox-content">


    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w90"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-cog m-r-xs"></i>Settings</a>
    <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif


        <div class="clearfix"></div>
        <div class="row">
            <form name="system" id="system" method="POST" action="/settings/{{Request::get('urlPrefix')}}/system">
                {{-- 
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Email and Text Settings</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Site Email Address:</label>
                    <input type="email" placeholder="Font Color" class="form-control" required name="sendEmail" value="{{ $siteConfig->sendEmail }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Site Text Number (@togomeals.biz):</label>
                    <input type="tel" placeholder="Site Telephone" id="phone" class="form-control" required name="sendText" value="{{ $siteConfig->sendText }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Send Email Username:</label>
                    <input type="email" placeholder="Font Color" class="form-control" required name="sendEmailUserName" value="{{ $siteConfig->sendEmailUserName }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Send Email Password</label>
                    <input type="text" placeholder="emailPassword" class="form-control" required name="sendEmailPassword" value="@if (!empty($siteConfig->sendEmailPassword)){{decrypt($siteConfig->sendEmailPassword)}}@endif">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Receive Email Address:</label>
                    <input type="email" placeholder="Font Color" class="form-control" required name="orderReceiveEmail" value="{{ $siteConfig->orderReceiveEmail }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">HP ePrint Email Address:</label>
                    <input type="email" placeholder="Font Color" class="form-control" name="hpEprint" value="{{ $siteConfig->hpEprint }}">
                </div>

                <div class="clearfix"></div>

                <hr>

                --}}

                 <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Email Configuration</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9) Pickup @else Dine In @endif Order Message:</label>
                    <textarea placeholder="Pickup Order Message" rows="4" class="form-control" name="orderMessage">{{ $siteConfig->orderMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                @if ($domain->type != 9)

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Order Message:</label>
                    <textarea placeholder="Delivery Order Message" rows="4" class="form-control" name="deliveryMessage">{{ $siteConfig->deliveryMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                @if ($siteConfig->orderAhead)

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Future Pickup Order Message - [[pickupTime]] replacement for pickup date and time:</label>
                    <textarea placeholder="Future Pickup Order Message" rows="4" class="form-control" name="futureOrderMessage">{{ $siteConfig->futureOrderMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Future Delivery Order Message - [[deliveryTime]] replacement for pickup date and time:</label>
                    <textarea placeholder="Future Delivery Order Message" rows="4" class="form-control" name="futureDeliveryMessage">{{ $siteConfig->futureDeliveryMessage ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>

                @endif

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Footer Message:</label>
                    <textarea placeholder="Order Footer Message" rows="4" class="form-control" name="orderFooter">{{ $siteConfig->orderFooter ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                
                
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Pickup Order In Process Message:</label>
                    <textarea placeholder="Pickup Order In Process Message" rows="4" class="form-control" name="pickupOIP">{{ $siteConfig->pickupOIP ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                
                
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Order In Process Message:</label>
                    <textarea placeholder="Delivery Order In Process Message" rows="4" class="form-control" name="deliverOIP">{{ $siteConfig->deliverOIP ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div> 
                        

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Text Order Ready Message:</label>
                    <textarea placeholder="Delivery Text Order Ready Message" rows="4" class="form-control" name="deliverOrderReady">{{ $siteConfig->deliverOrderReady ?? '' }}</textarea>
                </div>
                
                <div class="clearfix"></div>
                
                @endif

                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9) Pickup @else Dine In @endif Text Order Ready Message:</label>
                    <textarea placeholder="Pickup Text Order Ready Message" rows="4" class="form-control" name="orderReady">{{ $siteConfig->orderReady ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                


                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Customer Registration Message (Welcome Email):</label>
                    <textarea placeholder="Customer Registration Message" rows="4" class="form-control" name="welcomeEmail">{{ $siteConfig->welcomeEmail ?? '' }}</textarea>
                </div>


                <div class="clearfix"></div>

                @if ($domain->type != 9)
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Alert (Non-Card Processed Orders Only):</label>
                    <textarea placeholder="OrderAlert Message" rows="4" class="form-control" name="orderAlert">{{ $siteConfig->orderAlert ?? '' }}</textarea>
                </div>
                @endif

                {{ csrf_field() }}

                <div class="form-group m-t-lg">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-white text-center" type="submit">Cancel</a>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </form>

        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection