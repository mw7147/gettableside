@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->

<h2>System Suspension Settings</h2>


<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md w90" ><i class="fa fa-toggle-up"></i> Hide Help</button>

<ul class="m-b-md instructions">
    <li>Select Online Operations Suspended or Normal Operations under Current Status.</li>
    <li>If suspending operations, select the number of minutes to suspend operations.</li>
    <li>Normal Operation will automatically resume once the selected number of minutes has passed.</li>
    <li>Suspended By, Start Time and Stop Time are informational fields and set by the system. These fields cannot be changed.</li>

</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">Current Suspension Status - {{ $status }}</i></h5>
    </div>
        
    <div class="ibox-content">

        <a href="/{{Request::get('urlPrefix')}}/dashboard?id={{rand(100000,10000000)}}" class="btn btn-info btn-xs m-l-sm m-b-lg w110"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
        <a href="/settings/{{Request::get('urlPrefix')}}/dashboard?id={{rand(100000,10000000)}}" class="btn btn-info btn-xs m-l-xs m-b-lg w110"><i class="fa fa-user m-r-xs"></i>Settings</a>

        <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
        <div class="alert alert-success alert-dismissable col-md-9 col-sm-10 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateSuccess') }}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(Session::has('updateError'))
        <div class="alert alert-danger alert-dismissable col-md-9 col-sm-10 col-xs-12 m-b-xl">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('updateError') }}
        </div>
        <div class="clearfix"></div>
        @endif

        <div class="row">

        <form name="editaddOnList" method="POST" action="/settings/{{Request::get('urlPrefix')}}/suspend/settings">


            <div class="clearfix"></div>
    
            <div class="col-md-3 col-sm-5 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Current Status:</label>
                <select class="form-control" name="domainID" required> 
                    <option @if ($status == 'Open' || $status == 'closed') selected @endif value="0">Normal Operations (Open or Closed)</option>
                    <option @if ( substr($status, 0,3) == 'Sus') selected @endif value="{{ $domain->id }}">Online Operations Suspended</option>
                </select>
            </div>

            @if ( substr($status, 0,3) == 'Sus')
            <div class="col-md-3 col-sm-5 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Suspended By:</label>
                <input type="text" placeholder="Suspended By" class="form-control" name="userName" id="userName" value="{{ $user->fname . ' ' . $user->lname }}" disabled style="background-color: white;">
                <input type="hidden" name="userID" id="userID" value="{{ $user->id }}">
            </div>
            @endif


            <div class="clearfix"></div>

            <div class="col-md-3 col-sm-5 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Number of Minutes:</label>
                <select class="form-control" name="numMinutes" required> 
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 15) selected @endif @endisset value="15">15</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 30) selected @endif @endisset value="30">30</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 45) selected @endif @endisset value="45">45</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 60) selected @endif @endisset value="60">60</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 90) selected @endif @endisset value="90">90</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 120) selected @endif @endisset value="120">120</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 180) selected @endif @endisset value="180">180</option>
                    <option @isset($suspendOperation) @if ($suspendOperation->numMinutes == 999) selected @endif @endisset value="999">Until Midnight</option>
                </select>
            </div>

            @if ( substr($status, 0, 3) == 'Sus')
            <div class="col-md-3 col-sm-5 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Start Time:</label>
                <input type="text" placeholder="Add On Name" class="form-control" name="name" id="name" value="@isset($suspendOperation->startTime) {{ \Carbon\Carbon::parse($suspendOperation->startTime)->format('h:i A') ?? '' }} @endisset"  disabled style="background-color: white;">
            </div>
                
            <div class="col-md-3 col-sm-5 col-xs-12 m-b-md">
                <label class="font-normal font-italic">Stop Tme:</label>
                <input type="text" placeholder="Add On Name" class="form-control" name="name" id="name" value="@isset($suspendOperation->stopTime) {{ \Carbon\Carbon::parse($suspendOperation->stopTime)->format('h:i A') ?? '' }} @endisset"  disabled style="background-color: white;">
            </div>
            @endif



            <div class="clearfix"></div>

            <div class="col-md-10 col-sm-9 col-xs-12 m-t-lg m-b-lg text-center">
                    
                    {{ csrf_field() }}
                    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-white" type="submit">Cancel and Return</a>
                    <button class="btn btn-success" type="submit">Update Suspension Settings</button>

            </div>

</form>
<!-- Page Level Scripts -->

<script>

$('.btn-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

$('.btn-up-instructions').on('click',function(){
    $('.instructions').toggle();
    $('.btn-instructions').toggle();
    $('.btn-up-instructions').toggle();
});

</script>

@endsection