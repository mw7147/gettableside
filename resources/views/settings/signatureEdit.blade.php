@extends('admin.admin')


@section('content')

<!-- Page Level CSS -->


<h2>My Email Signature</h2>
    
    <button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Instructions</button>
    <button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Instructions</button>


<ul class="m-b-md instructions">
    <li>Complete your signature as desired.</li>
    <li>A "Text Only" version is needed for text based emails.</li>
    <li>Press "Update Signature" to update the signature.</li>
    <li>Press "Cancel" or select a menu item to exit without saving.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i class="m-r-sm">My Signature</i></h5>
    </div>
        
    <div class="ibox-content">
    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w90"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-cog m-r-xs"></i>Settings</a>


        <form name="mySignature" id="mySignature" method="POST" action="/settings/{{Request::get('urlPrefix')}}/signature">
            


            <div class="row">
                <div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1 m-t-md">

                <div class="clearfix"></div>
                @if(Session::has('fileError'))
                    <div class="alert alert-warning alert-dismissable col-xs-10 col-sm-8 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('fileError') }}
                    </div>
                @endif

                @if(Session::has('updateSuccess'))
                    <div class="alert alert-success alert-dismissable col-xs-12 col-sm-10 col-md-9 col-lg-7 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('updateSuccess') }}
                    </div>
                @endif

                @if(Session::has('updateError'))
                    <div class="alert alert-danger alert-dismissable col-xs-12 col-sm-10 col-md-9 col-lg-7 m-b-xl">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{ Session::get('updateError') }}
                    </div>
                @endif

                <div class="clearfix"></div>


                <div class="input-group m-b col-xs-12 col-sm-10 col-md-9 col-lg-7">

                <p class="m-b-md"><i>Your email signature to optionally include when sending emails.</i></p>
                <div class="clearfix"></div>


                <div class="input-group m-b col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="bg-success" style="display: block; padding: 8px 10px;">HTML Signature:</span>

                    <textarea class="form-control m-t-xs" name="signature" id="signature" required>@if (isset($signature->signature)) {{ $signature->signature }} @endif</textarea>
                    

                </div>

                <div class="clearfix"></div>

                <div class="input-group m-b col-xs-12 col-sm-12 col-md-12 col-lg-12 m-t-lg">
                <span class="bg-success" style="display: block; padding: 8px 10px;">Text Signature:</span>

                    <textarea class="form-control h-200" name="textSignature" id="textSignature" required>@if (isset($signature->textSignature)) {{ $signature->textSignature }} @endif</textarea>

                    

                </div>



                <div class="form-group m-t-lg" style="margin-top: 50px;"></div>
                <div class="input-group m-b col-xs-11 col-sm-10 col-md-9 col-lg-11 text-center">
                        
                    {{ csrf_field() }}
                    <a href="/customers/dashboard" class="btn btn-white w-150" type="submit">Cancel and Return</a>
                    <span class="space-7 hidden-xs hidden-sm hidden-md"></span>

                    <button class="btn btn-success w-150" type="submit">Update Signature</button>

                </div>

            </div>
        </form>
    </div>
</div>


<!-- Page Level Scripts -->
<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

<script>

    CKEDITOR.replace( 'signature', {
        filebrowserBrowseUrl: '/attache/ckeditor/browse',
    } );

</script>

@endsection