@extends('admin.admin')

@section('content')

<!-- Page Level CSS -->
<link rel="stylesheet" href="/libraries/jasny-bootstrap/css/jasny-bootstrap.min.css">

<!-- Page Level Scripts -->
<script src="/libraries/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>

<h2>Edit Customer Site Information</h2>

<button class="btn btn-primary btn-xs btn-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-down"></i> Show Help</button>
<button class="btn btn-primary btn-xs btn-up-instructions m-t-sm m-b-md" ><i class="fa fa-toggle-up"></i> Hide Help</button>


<h4>
        {{ $domain->name }} Customer Site Information.
</h4>

<ul class="m-b-md instructions">
    <li>Fill in the style as necessary.</li>
    <li>Press "Cancel" or select a menu item to return to the System User List View without updating the Customer Information.</li>
</ul>

<div class="ibox">
    <div class="ibox-title">
        <h5><i>{{ $domain ->name }} Customer ID: {{ $domain->id }}</i></h5>
    </div>
        
    <div class="ibox-content">


    <a href="/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-sm m-b-lg w90"><i class="fa fa-dashboard m-r-xs"></i>Dashboard</a>
    <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-info btn-xs m-l-xs m-b-lg w90"><i class="fa fa-cog m-r-xs"></i>Settings</a>
    <div class="clearfix"></div>

        @if(Session::has('updateSuccess'))
            <div class="alert alert-success alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateSuccess') }}
            </div>
        @endif

        @if(Session::has('updateError'))
            <div class="alert alert-danger alert-dismissable col-md-5 col-sm-9 col-xs-12 m-b-xl">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ Session::get('updateError') }}
            </div>
        @endif


        <div class="clearfix"></div>
        <div class="row">
            <form name="location" id="location" method="POST" action="/settings/{{Request::get('urlPrefix')}}/location">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Location Information</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Manager First Name:</label>
                    <input type="text" placeholder="Manager First Name" class="form-control" name="managerFname" value="{{ $siteConfig->managerFname }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Manager Last Name:</label>
                    <input type="text" placeholder="Manager Last Name" class="form-control" name="managerLname" value="{{ $siteConfig->managerLname }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Name:</label>
                    <input type="text" placeholder="Location Name" class="form-control" name="locationName" value="{{ $siteConfig->locationName }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Site Phone Number:</label>
                    <input type="tel" placeholder="Site Telephone" id="phone" class="form-control" required name="locationPhone" value="{{ $siteConfig->locationPhone }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Address1:</label>
                    <input type="text" placeholder="Address1" class="form-control" name="address1" value="{{ $siteConfig->address1 }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Address 2:</label>
                    <input type="text" placeholder="Address2" class="form-control" name="address2" value="{{ $siteConfig->address2 }}">
                </div>

                <div class="clearfix"></div>

                   <div class="col-md-4 col-sm-12 col-xs-12">
                        <label class="font-normal font-italic">City:</label>
                        <input class="form-control" type="text" name="city" value="{{ $siteConfig->city }}" placeholder="City">
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <label class="font-normal font-italic">State:</label>
                        <select class="form-control" name="state" id="state">

                            @foreach ($states as $state)
                            <option value="{{ $state->stateCode }}" @if ($state->stateCode == $siteConfig->state) selected @endif>{{ $state->stateName }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 zipcode">
                        <label class="font-normal font-italic">Zip Code:</label>
                        <input class="form-control" type="text" name="zipCode" value="{{ $siteConfig->zipCode }}" placeholder="Zip Code">
                    </div>
                <div class="clearfix"></div>
                    <hr>

                <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Location Order Information</p>
                </div>

                <div class="clearfix"></div>


                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Sales Tax Rate:</label>
                    <input type="num" step=".01" min="2" max="20" placeholder="Location Tax Rate" class="form-control" name="taxRate" value="{{ $siteConfig->taxRate ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Global Discount Percent:</label>
                    <input type="num" step="1" min="0" max="50" placeholder="Location Tax Rate" class="form-control" name="globalDiscount" value="{{ $siteConfig->globalDiscount ?? '' }}">
                </div>
                
                <div class="clearfix"></div>

                
                @if ($domain->type != 9)
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Order Ahead Service:</label>
                    
                        <select class="form-control" name="orderAhead" required>
                            <option value="1" @isset($siteConfig->orderAhead) @if ($siteConfig->orderAhead == 1) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->orderAhead) @if ($siteConfig->orderAhead == 0) selected @endif @endisset>No</option>
                        </select>

                </div>
                <div class="clearfix"></div>
                @else 
                    <input type="hidden" name="orderAhead" value="0">
                @endif

                @if ($domain->type != 9 )
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Pickup Prepare Time in Minutes:</label>
                    <input type="number" min="0" max="120" step="1" placeholder="Pickup Prepare Time in Minutes" class="form-control" name="pickupPrepMinutes" value="{{ $siteConfig->pickupPrepMinutes ?? 0 }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Prepare Time in Minutes:</label>
                    <input type="number" min="0" max="120" step="1" placeholder="Delivery Prepare Time in Minutes" class="form-control" name="deliveryPrepMinutes" value="{{ $siteConfig->deliveryPrepMinutes ?? 0 }}">
                </div>

                <div class="clearfix"></div>

                @endif

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Cart Clear Timer In Minutes:</label>
                    <input type="number" min="0" max="60" step="1" placeholder="Cart Clear Timer in Minutes" class="form-control" name="cartClearTimer" value="{{ $siteConfig->cartClearTimer ?? 0 }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Dine In Last Order Offset Time in Minutes:</label>
                    <input type="number" min="0" max="90" step="1" placeholder="Dine In Last Order Offset Time in Minutes" class="form-control" name="dineInLastOrderTimeOffset" value="{{ $siteConfig->dineInLastOrderTimeOffset ?? 0 }}">
                </div>

                <div class="clearfix"></div>

                @if ($domain->type != 9)

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Require Recommended Tip:</label>
                    
                        <select class="form-control" name="deliveryRequireRecommendedTip" required>
                            <option value="1" @isset($siteConfig->deliveryRequireRecommendedTip) @if ($siteConfig->deliveryRequireRecommendedTip == 1) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->deliveryRequireRecommendedTip) @if ($siteConfig->deliveryRequireRecommendedTip == 0) selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Recommended Tip Percent:</label>
                    <input type="number" min="0" max="100" step="1" placeholder="Delivery Recommended Tip Percent" class="form-control" name="deliveryRecommendedTipPercent" value="{{ $siteConfig->deliveryRecommendedTipPercent ?? 0 }}">
                </div>

                <div class="clearfix"></div>

                @endif

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9) Pickup @else Dine In @endif Require Recommended Tip:</label>
                    
                        <select class="form-control" name="pickupRequireRecommendedTip" required>
                            <option value="1" @isset($siteConfig->pickupRequireRecommendedTip) @if ($siteConfig->pickupRequireRecommendedTip == 1) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->pickupRequireRecommendedTip) @if ($siteConfig->pickupRequireRecommendedTip == 0) selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">@if ($domain->type != 9) Pickup @else Dine In @endif Recommended Tip Percent:</label>
                    <input type="number" min="0" max="60" step="1" placeholder="@if ($domain->type != 9) Pickup @else Dine In @endif Recommended Tip Percent" class="form-control" name="pickupRecommendedTipPercent" value="{{ $siteConfig->pickupRecommendedTipPercent ?? 0 }}">
                </div>

                <div class="clearfix"></div>

                @if ($domain->type != 9)
                    <hr>

 
                 <div class="col-md-6 col-sm-9 col-xs-12">
                    <p class="font-italic font-bold">Delivery Data<Datal></Datal></p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Delivery:</label>
                    
                        <select class="form-control" name="delivery" required>
                            <option value="yes" @if ($siteConfig->delivery == "yes") selected @endif>Yes</option>
                            <option value="no" @if ($siteConfig->delivery == "no") selected @endif>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Radius: (miles w tenths decimal)</label>
                    <input type="num" step=".1" min="0" max="30" placeholder="Delivery Radius" class="form-control" name="deliveryRadius" value="{{ $siteConfig->deliveryRadius ?? '' }}">
                </div>

                <div class="clearfix"></div>
                
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Charge</label>
                    <input type="num" step=".01" min=".00" max="50" placeholder="Delivery Charge" class="form-control" name="deliveryCharge" value="{{ $siteConfig->deliveryCharge ?? '' }}">
                </div>
                
                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Minimum Order</label>
                    <input type="num" step=".01" min=".00" max="50" placeholder="Delivery Minimum" class="form-control" name="deliveryMinOrder" value="{{ $siteConfig->deliveryMinOrder ?? '' }}">
                </div>

                <div class="clearfix"></div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Tax Delivery:</label>
                    
                        <select class="form-control" name="taxDelivery" required>
                            <option value="1" @isset($siteConfig->taxDelivery) @if ( $siteConfig->taxDelivery == 1 ) selected @endif @endisset>Yes</option>
                            <option value="0" @isset($siteConfig->taxDelivery) @if ( $siteConfig->taxDelivery == 0 ) selected @endif @endisset>No</option>
                        </select>

                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Delivery Tax Rate:</label>
                    <input type="text" placeholder="Delivery Tax Rate" class="form-control" name="deliveryTaxRate" value="{{ $siteConfig->deliveryTaxRate ?? 0 }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Food Runner Token</label>
                    <input type="text" placeholder="Food Runner Token" class="form-control" name="foodRunnerToken" value="{{ $siteConfig->foodRunnerToken ?? null }}">
                </div>

                <div class="clearfix"></div>


                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Latitude:</label>
                    <input type="text" placeholder="Location Latitude" class="form-control" name="latitude" value="{{ $siteConfig->latitude ?? '' }}">
                </div>

                <div class="col-md-5 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Location Longitude:</label>
                    <input type="text" placeholder="Location Longitude" class="form-control" name="longitude" value="{{ $siteConfig->longitude ?? '' }}">
                </div>

                <div class="clearfix"></div>
                
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Google Maps API Key:</label>
                    <textarea placeholder="Google Maps API Key" rows="2" class="form-control" name="googleMapKey">{{ $siteConfig->googleMapKey ?? '' }}</textarea>
                </div>

                <div class="clearfix"></div>
                
                <div class="col-md-10 col-sm-9 col-xs-12 m-b-md">
                    <label class="font-normal font-italic">Google Maps Location Link:</label>
                    <textarea placeholder="Google Maps Link" rows="4" class="form-control" name="mapLink">{{ $siteConfig->mapLink ?? ''}}</textarea>
                </div>

                <div class="clearfix"></div>
                @endif

                {{ csrf_field() }}

                <div class="form-group m-t-lg">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <a href="/settings/{{Request::get('urlPrefix')}}/dashboard" class="btn btn-white text-center" type="submit">Cancel</a>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>
            </form>

        </div> <!-- div row -->
    </div> <!-- div ibox-content-->
</div> <!-- div ibox -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


<script>


$( document ).ready(function() {
  console.log("ready");
  $("#phone").mask("(999) 999-9999");
});


</script>




<script>

    $('.btn-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

    $('.btn-up-instructions').on('click',function(){
        $('.instructions').toggle();
        $('.btn-instructions').toggle();
        $('.btn-up-instructions').toggle();
    });

</script>

@endsection