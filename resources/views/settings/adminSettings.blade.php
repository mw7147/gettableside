@extends('admin.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h1 class="m-b-none">
                {{ $domain['name'] }} Settings
            </h1>
            <small>
                System and Customer Settings
            </small>
        </div>
    </div>
</div>


<!-- page level css -->

<style>
    .dashicon { height: 56px; }
    .dashicon:hover { opacity: .75;}
    .dashicon-white:hover { opacity: .62;}
    .widget { padding: 8px 15px;}
    .widget.style1 h2 { font-size: 17px; margin-top: 2px; font-weight: 300;}
    h2 {font-size: 20px;}
    .fa-report {font-size: 22px;}
    .ibox-content {min-height: 180px;}
    .fa-1_5x {font-size: 1.5em;}
    @media (max-width: 991px) {.ibox-content { min-height: 75px;} .widget.style1 h2 { font-size: 14px; margin-top: 3px; font-weight: 300;} h1 {font-size: 23px;} h2 {font-size: 17px;} .fa-report {font-size: 18px;}}

</style>


<div class="row">

       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Email Settings</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/{{Request::get('urlPrefix')}}/sendemail/view">
                        <div class="widget style1 yellow-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-2">
                                    <i class="fa fa-reply fa-report"></i>
                                </div>
                                <div class="col-xs-10 text-right">
                                    <h2 class="font-bold">Send Email Accounts</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/{{Request::get('urlPrefix')}}/receiveemail/view">
                        <div class="widget style1 yellow-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-2">
                                    <i class="fa fa-share fa-report"></i>
                                </div>
                                <div class="col-xs-10 text-right">
                                    <h2 class="font-bold">Receive Email Accounts</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>





    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Restore Receipt</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/admin/restore">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-sticky-note"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Restore Receipt</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
  



{{--

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Website Settings</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-report fa-safari"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Website Settings</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-sitemap fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Website Pages</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
--}}  

</div>

@endsection