@extends('admin.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="text-center">
            <h1 class="m-b-none">
                {{ $domain['name'] }} Settings
            </h1>
            <small>
                System and Location Settings
            </small>
        </div>
    </div>
</div>


<!-- page level css -->

<style>
    .dashicon { height: 56px; }
    .dashicon:hover { opacity: .75;}
    .dashicon-white:hover { opacity: .62;}
    .widget { padding: 8px 15px;}
    .widget.style1 h2 { font-size: 17px; margin-top: 2px; font-weight: 300;}
    h2 {font-size: 20px;}
    .fa-report {font-size: 22px;}
    .ibox-content {min-height: 180px;}
    .fa-1_5x {font-size: 1.5em;}
    @media (max-width: 991px) {.ibox-content { min-height: 75px;} .widget.style1 h2 { font-size: 14px; margin-top: 3px; font-weight: 300;} h1 {font-size: 23px;} h2 {font-size: 17px;} .fa-report {font-size: 18px;}}

</style>



<div class="clearfix"></div>

@if(Session::has('updateSuccess'))
<div style="margin-top: 24px;">
    <div class="alert alert-success alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-sm">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        {{ Session::get('updateSuccess') }}
    </div>
</div>
@endif

@if(Session::has('updateError'))
<div style="margin-top: 24px;">
    <div class="alert alert-danger alert-dismissable col-md-6 col-sm-9 col-xs-12 m-b-xl">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        {{ Session::get('updateError') }}
    </div>
</div>
@endif


<div class="clearfix"></div>


<div class="row">

       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>System Settings</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/settings/{{Request::get('urlPrefix')}}/location">
                        <div class="widget style1 yellow-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-building fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Location Settings</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/settings/{{Request::get('urlPrefix')}}/system">
                        <div class="widget style1 yellow-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-building fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Site Email Settings</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/tags/{{Request::get('urlPrefix')}}/tag-manager">
                        <div class="widget style1 yellow-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-building fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Menu Item Tag Manager</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>

    @if ($domain->type < 8)

    
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Payment Settings</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                        <a href="/settings/{{Request::get('urlPrefix')}}/payment">
                            <div class="widget style1 navy-bg">
                                <div class="row vertical-align">
                                    <div class="col-xs-3">
                                        <i class="fa fa-credit-card fa-report"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <h2 class="font-bold">Payment Settings</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-xs-12 dashicon m-b-n-sm">
                        <a href="/settings/{{Request::get('urlPrefix')}}/managerpin">
                            <div class="widget style1 navy-bg">
                                <div class="row vertical-align">
                                    <div class="col-xs-3">
                                        <i class="fa fa-credit-card fa-report"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <h2 class="font-bold">Refund PIN</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="clearfix"></div>

                </div>
        </div>
    </div> 



    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>Suspend Operations</h2>

            </div>
            
            <div class="ibox-content">
                @if ( $status == 'Open' || $status == 'closed' )
                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/settings/{{Request::get('urlPrefix')}}/suspend/one">
                        <div class="widget style1 red-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-pause fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Suspend 1 Hour</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @else 
                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="" onclick="suspended(event)">
                        <div class="widget style1 red-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-pause fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Suspended</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endif


                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/settings/{{Request::get('urlPrefix')}}/suspend/settings">
                        <div class="widget style1 red-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-pause fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">Suspend Settings</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>


                <div class="clearfix"></div>

            </div>
        </div>
    </div>

    @endif





    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 m-t-md pull-left reportBox">
        
        <div class="ibox">
            <div class="ibox-title">

                <h2>QR Code Settings</h2>

            </div>
            
            <div class="ibox-content">

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/settings/{{Request::get('urlPrefix')}}/qrfooter">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-qrcode fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">QR Tent Card Footer</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 dashicon m-b-n-sm">
                    <a href="/settings/{{Request::get('urlPrefix')}}/qremail">
                        <div class="widget style1 blue-bg">
                            <div class="row vertical-align">
                                <div class="col-xs-3">
                                    <i class="fa fa-qrcode fa-report"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <h2 class="font-bold">QR Code Email Address</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
    

</div>

<script>

    function comingSoon() {

        event.preventDefault();
        alert("Under Development - Coming Soon");
        return true;

    }

    function suspended(event) {
        event.preventDefault();
        alert("System is suspended. Use Suspend Settings to update or change.");
        return true;

    }


</script>

@endsection