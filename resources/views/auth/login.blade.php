@extends('layouts.login')

@section('content')

<div class="loginbkgrnd">

<style>
.panel {
    margin-top: 10%;
}

.poweredBy {
    font-size: 10px;
}

.panel {
    opacity: .92;
}

</style>

    <div class="panel panel-success text-center col-xs-10 col-sm-8 col-md-4 col-lg-3 pull-right col-xs-pull-1 col-sm-pull-2 col-md-pull-1 col-lg-pull-1 topmargin">
        <div class="panel-heading">
            <a href="/"><h3 style="color:white;">{{ $data['locationName'] ?? 'Site Admin'}}</h3></a>
        </div>    
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif              
        <div class="panel-body">
            <p class="m-b">Login in for account access.</p>
            <form class="m-t" role="form" method="POST" action="{{ url('/userlogin') }}">
                <div class="form-group">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif  
                </div>

                <div class="form-group">
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif                
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a class="logintext" href="{{ url('/forgot/password') }}"><small>Forgot password?</small></a>
                <a class="btn btn-sm btn-info btn-block m-t" href="/userregister">Create An Account</a>

                {{ csrf_field() }}

            </form>
            <p class="m-t"> <small>getTableSide.com - &copy; {{ date("Y", time()) }}</small><br>
            <a class="text-center poweredBy" href="http://www.h2iq.us" target="_blank">Powered by Hospitality IQ</a> </p>
        </div>
    </div>
</div>




@endsection
