import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        order: {
            schedule_date: '',
            schedule_time: '',
            schedule_date_time: '',
            is_pickup: false,
            is_delivery: false,
            time_list: ''
        }
    },
    mutations: {
        setOrderScheduleDate(state, date) {
            state.order.schedule_date = date;
        },
        setOrderScheduleTime(state, time) {
            state.order.schedule_time = time;
        },
        setIsPickup(state, status) {
            state.order.is_pickup = status;
        },
        setIsDelivery(state, status) {
            state.order.is_delivery = status;
        },
        setOrderScheduleDateTime(state, dateTime) {
            state.order.schedule_date_time = dateTime;
        },
        setTimeList(state, timeList) {
            state.order.time_list = timeList;
        },
    },
    actions: {
        setOrderScheduleDate({ commit }, date) {
            commit('setOrderScheduleDate', date);
        },
        setOrderScheduleTime({ commit }, time) {
            commit('setOrderScheduleTime', time);
        },
        setIsPickup({ commit }, status) {
            commit('setIsPickup', status);
        },
        setIsDelivery({ commit }, status) {
            commit('setIsDelivery', status);
        }
    },
    getters: {
        getOrderScheduleDate: (state) => {
            return state.order.schedule_date;
        },
        getOrderScheduleTime: (state) => {
            return state.order.schedule_time;
        },
        getIsPickup: (state) => {
            return state.order.is_pickup;
        },
        getIsDelivery: (state) => {
            return state.order.is_delivery;
        },
        getOrderScheduleDateTime: (state) => {
            return state.order.schedule_date_time;
        },
        getTimeList: (state) => {
            return state.order.time_list;
        }
    }
});

export default store;