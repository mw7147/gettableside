import moment from "moment";

export const DATETIMEVALUEFORMAT = 'YYYYMMDDTHHmm'
export const DATEVALUEFORMAT = 'YYYYMMDD'
export const DATETEXTFORMAT = 'ddd MMM Do, hh:mm A'
export const TIMEVALUEFORMAT = 'H:mm'
export const TIMETEXTFORMAT = 'h:mm A'

export const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

export const setMomentTime = (dateInstance, time) => {
    if (!time) return

    const [hour, minute, second] = time.split(":");
    return dateInstance.set({ 'hour': hour, 'minute': minute, 'second': second });
}

export const getMenuTimeListForDate = (schedule, date, orderPrepMinutes) => {
    let weekday = date.format('dddd')
    let timeList = []
    let todayStart = setMomentTime(
        date.clone(),
        schedule[`start${weekday}`]
    );
    let todayEnd = setMomentTime(
        date.clone(),
        schedule[`stop${weekday}`]
    );

    let timePointer = todayStart;
    let curTime = moment();
    if (curTime.isSameOrAfter(timePointer)) {
        timePointer = curTime;
    }
    let rem = timePointer.minutes() % 15;
    if (rem) {
        rem = 15 - rem;
    }
    timePointer.add(rem, "m");
    rem = orderPrepMinutes % 15;
    if (rem) {
        rem = 15 - rem;
    }
    orderPrepMinutes = orderPrepMinutes + rem;
    todayEnd = todayEnd.subtract(15, "m");
    timePointer.add(orderPrepMinutes, "m");
    while (timePointer.isBefore(todayEnd)) {
        //only add as option if after 45 minutes from NOW 
        let isItAfterNow = timePointer.isSameOrAfter(moment());
        if (isItAfterNow) {
            timeList.push({
                value: timePointer.format(TIMEVALUEFORMAT),
                text: timePointer.format(TIMETEXTFORMAT)
            });
        }
        timePointer.add(15, "m");
    }
    return timeList
}