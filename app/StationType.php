<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationType extends Model
{
    protected $table = 'station_types';

    protected $guarded = [];
}
