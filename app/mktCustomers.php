<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mktCustomers extends Model
{
    protected $connection = 'marketingdb';
    protected $table = 'customers';
    protected $guarded = [];
}
