<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emailQueue extends Model
{
    protected $connection = 'maildb';
    protected $table = 'emailQueue';
}
