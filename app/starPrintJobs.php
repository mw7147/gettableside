<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class starPrintJobs extends Model
{
    protected $connection = 'starPrint';
    protected $table = 'printQueue';
}
