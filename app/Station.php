<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $table = 'stations';

    protected $guarded = [];

    public function menuDataList()
    {
        return $this->belongsToMany('App\menuData', 'station_menu_items', 
        'station_id', 'menu_item_id');
      
    }

    
}
