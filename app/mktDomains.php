<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mktDomains extends Model
{
    protected $connection = 'marketingdb';
    protected $table = 'domains';
    protected $guarded = [];
}
