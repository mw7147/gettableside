<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationMenuItem extends Model
{
    protected $table = 'station_menu_items';

    protected $guarded = [];
}
