<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mktMyInformation extends Model
{
    protected $connection = 'marketingdb';
    protected $table = 'myInformation';
    protected $guarded = [];
}
