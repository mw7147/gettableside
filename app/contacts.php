<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contacts extends Model
{
    protected $connection = 'marketingdb';
    protected $table = 'contacts';
    protected $guarded = [];
}
