<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class marketing extends Model
{
    protected $table = 'marketing';
    protected $primaryKey = 'domainID';
    protected $guarded = [];
}
