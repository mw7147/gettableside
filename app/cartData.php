<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cartData extends Model
{
    protected $table = 'cartData';
    
    public function menuData(){
        return $this->belongsTo('App\menuData','menuDataID');
    }

    public function menuCategoriesData(){
        return $this->belongsTo('App\menuCategoriesData','menuCategoriesDataID');
    }
}
