<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registrationEmails extends Model
{
    protected $table = 'registrationEmails';
    protected $guarded = [];
}
