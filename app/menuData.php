<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\menuCategoriesData;

class menuData extends Model
{
    protected $table = 'menuData';

    public function menuImages(){
        return $this->hasmany('App\menuImages','menuDataID');
    }
    /**
     * Get the menuCategoriesData that owns the menuData.
     */
    public function menuCategoriesData()
    {
        return $this->belongsTo('menuCategoriesData','menuCategoriesDataID');
    }
    public function menu(){
        return $this->belongsTo('App\menu','menuID');
    }
}
