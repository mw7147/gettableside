<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deliverySession extends Model
{
    protected $table = 'deliverySession';
    protected $guarded = [];
}
