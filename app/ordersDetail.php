<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ordersDetail extends Model
{
    protected $table = 'ordersDetail';

    public function category()
    {
        return $this->hasOne('App\menuCategoriesData', 'id', 'menuCategoriesDataID');
    }
}
