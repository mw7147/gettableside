<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tables extends Model
{
    protected $table = 'tables';
    protected $guarded = [];
}
