<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sendEmailDomains extends Model
{
    protected $connection = 'emailAdminDB';
    protected $table = 'domains';
}
