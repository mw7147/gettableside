<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class domain extends Model
{
    protected $table = 'domains';
    protected $guarded = [];

    public function domainSiteConfig(){
        return $this->hasOne('App\domainSiteConfig','domainID','parentDomainID');
    }
}
