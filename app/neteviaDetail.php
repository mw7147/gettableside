<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class neteviaDetail extends Model
{
    protected $table = 'neteviaDetails';
    protected $guarded = [];
}
