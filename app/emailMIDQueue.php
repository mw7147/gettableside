<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emailMIDQueue extends Model
{
    protected $connection = 'maildb';
    protected $table = 'emailMIDQueue';
}
