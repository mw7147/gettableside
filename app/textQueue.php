<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class textQueue extends Model
{
    protected $connection = 'maildb';
    protected $table = 'textQueue';
}
