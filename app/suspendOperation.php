<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suspendOperation extends Model
{
    protected $table = 'suspendOperation';
    protected $primaryKey = 'domainID';
    protected $guarded = [];
}
