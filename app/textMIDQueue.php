<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class textMIDQueue extends Model
{
    protected $connection = 'maildb';
    protected $table = 'textMIDQueue';
}
