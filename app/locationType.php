<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class locationType extends Model
{
    protected $table = 'locationType';
    protected $guarded = [];
}
