<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\menuData;

class menuCategoriesData extends Model
{
    protected $table = 'menuCategoriesData';

    /**
     * Get the menuData for the menuCategoriesData.
     */
    public function menuDataList()
    {
        //$this->hasMany('App\Product','shop_id','id');
        return $this->hasMany('App\menuData', 'menuCategoriesDataID');
    }
}
