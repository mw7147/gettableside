<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    protected $table = 'orders';
    protected $guarded = [];

    public function contact()
    {
        return $this->hasOne('App\contacts', 'id', 'contactID');
    }

    public function domain()
    {
        return $this->belongsTo('App\domain', 'domainID', 'id');
    }

    public function orderDomain()
    {
        return $this->belongsTo('App\domain', 'orderDomainID', 'id');
    }
}
