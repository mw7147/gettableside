<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $table = 'user_cards';

    protected $fillable = ['user_id', 'account', 'token', 'response', 'cardNumber', 'expireMonth', 'expireYear'];
    
}
