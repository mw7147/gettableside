<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mktUsers extends Model
{
    protected $connection = 'marketingdb';
    protected $table = 'users';
    protected $guarded = [];
}
