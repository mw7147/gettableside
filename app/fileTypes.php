<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fileTypes extends Model
{
    protected $table = 'fileTypes';
    protected $primaryKey = 'mimetype';
}
