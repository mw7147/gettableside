<?php

namespace App\Console\Commands;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Facades\App\hwct\foodRunnerDelivery;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Facades\App\hwct\PrintOrders;
use Illuminate\Console\Command;
use App\domainSiteConfig;
use Carbon\Carbon;
use App\printers;
use App\domain;
use App\orders;


class processFutureOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'H2IQ:processFutureOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process future orders - set future orders to order in process';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        Log::info('Start processFutureOrders');
        
        $timezone = orders::where('orderInProcess', '=', 9)->pluck('timezone');
        $timezone = array_unique ($timezone->toArray());
        $orderID = [];
        foreach($timezone as $tz){
            $now = Carbon::now($tz);
            $id = orders::where('orderInProcess', '=', 9)->where('timezone',$tz)->where('orderPrepTime', '<=', $now->toDateTimeString())->pluck('id');       
            $orderID = array_merge($orderID,$id->toArray());
        }
        $orderID = array_unique($orderID);
        $orders = orders::whereIn('id', $orderID)->get();
    
        $numOrders = $orders->count();
        
        foreach ( $orders as $o ) {

            Log::info('Processing Order ID: ' . $o->id);

            $domainSiteConfig = domainSiteConfig::where('domainID', '=', $o->domainID)->first();
            //send customer text
            $textSent = HWCTMailer::sendOrderText($o->unformattedMobile, $domainSiteConfig, $o->orderType, 8, $o->orderReadyTime);
            Log::info('Text Sent - orders processed: ' . $o->customerMobile);
            try{
                // print receipt
                if ($domainSiteConfig->printReceipt != 0 ) {
                    // get printer info
                    $printer = printers::find($domainSiteConfig->printReceipt);
                                
                    // print receipt
                    if (!is_null($printer)) { $printReceipt = PrintOrders::printReceipt($printer, $o, $domainSiteConfig, 'new'); }
                } // end print receipt

                // print order
                if ($domainSiteConfig->printOrder != 0 ) {
                    // get printer info
                    $printer = printers::find($domainSiteConfig->printOrder);
                                
                    // print receipt
                    if (!is_null($printer)) { $printOrder = PrintOrders::printOrder($printer, $o, $domainSiteConfig, 'new'); }
                    
                } // end print order
            } catch(\Exception $e){
                $o->orderInProcess = 1;
                $o->save();
				\Log::info($e->getMessage());
			}

            // send to foodRunner system if current delivery
		    if ($domainSiteConfig->delivery == 'fr' && $o->orderType == 'deliver' ) {
                $domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $o->domainID)->first();
			    foodRunnerDelivery::schedule($domainSiteConfig, $order, $domainPaymentConfig->mode);
		    }

            // set orderInProcess = 1
            $o->orderInProcess = 1;
            $o->save();


        } // end foreach


        Log::info('Stop processFutureOrders - orders processed: ' . $numOrders);
        
    } // end handle
}
