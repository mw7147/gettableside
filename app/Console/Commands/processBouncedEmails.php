<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\bounceGroupMembers;
use App\bounceContacts;
use App\groupMember;
use Carbon\Carbon;
use App\contacts;
use App\bounces;


class processBouncedEmails extends Command
{
    /**
     * The name and signature of the console comlaravelmand.
     *
     * @var string
     */
    protected $signature = 'HWCTMail:processBouncedEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Bounced Emails From bounces@togomeals.biz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Starting Bounce Processing For Bounced Messages');
        $serverID = env('ORDERS_SERVER_ID'); // only get bounced messages for domains on specific server

        DB::connection('bouncedb')->table('tgmBounces')->where('serverID', '=', $serverID)->orderBy('id')->chunk(100, function ($bounces) {

            foreach ($bounces as $bounce) {

                // save to bounces table, move contact record to bounceContacts then delete on tokinmail server
                // save to bounces table
                // bc -> bounce data from tokinmail bounce
                $bd = new bounces();
                $bd->serverID = $bounce->serverID;
                $bd->domainID = $bounce->domainID;
                $bd->userID = $bounce->userID;
                $bd->contactID = $bounce->contactID;
                $bd->messageID = $bounce->messageID;
                $bd->mailtext = $bounce->mailtext;
                $bd->rawMessage = $bounce->rawMessage;
                $bd->save();

                // move contact data
                // bc - > bounce contact data from contacts
                $cd = contacts::find($bounce->contactID);
                if (is_null($cd)) {
                    // contact already processed as bounce
                    // delete and move on
                    DB::connection('bouncedb')->table('tgmBounces')->where('id', '=', $bounce->id)->delete();
                    continue;
                }

                $bc = new bounceContacts();
                    $bc->bounceID = $bd->id;
                    $bc->domainID = $cd->domainID;
                    $bc->userID = $cd->userID;
                    $bc->contactID = $bounce->contactID;
                    $bc->vip = $cd->vip;
                    $bc->fname = $cd->fname;
                    $bc->lname = $cd->lname;
                    $bc->email = $cd->email;
                    $bc->mobile = $cd->mobile;
                    $bc->unformattedMobile = $cd->unformattedMobile;
                    $bc->companyEmail = $cd->companyEmail;
                    $bc->companyName = $cd->companyName;
                    $bc->companyPhone = $cd->companyPhone;
                    $bc->title = $cd->title;
                    $bc->address1 = $cd->address1;
                    $bc->address2 = $cd->address2;
                    $bc->city = $cd->city;
                    $bc->state = $cd->state;
                    $bc->zipCode = $cd->zipCode;
                    $bc->countryCode = $cd->countryCode;
                    $bc->birthDay = $cd->birthDay;
                    $bc->birthMonth = $cd->birthMonth;
                    $bc->anniversaryDay = $cd->anniversaryDay;
                    $bc->anniversaryMonth = $cd->anniversaryMonth;
                    $bc->pixPublic = $cd->pixPublic;
                    $bc->pixPrivate = $cd->pixPrivate;
                    $bc->merchantCustomerID = $cd->merchantCustomerID;
                $bc->save();

                // move group memberships
                $contactGroups = groupMember::where('contactID', '=', $bounce->contactID)->get();
                if (!is_null($contactGroups)) {
                    foreach ($contactGroups as $cg) {
                        $bounceGroup = new bounceGroupMembers();
                            $bounceGroup->groupID = $cg->groupID;
                            $bounceGroup->contactID = $cg->contactID;
                            $bounceGroup->domainID = $cg->domainID;
                            $bounceGroup->bounceID = $bd->id; // bounce ID
                        $bounceGroup->save();
                        Log::info($bounceGroup);

                    }  // end foreach $contactGroups
                $deletGroups = groupMember::where('contactID', '=', $bounce->contactID)->delete();

                } // end !is_null $contactGroups   

                // delete contact from contacts table
                $cd->delete();

                // delete bounce record on mail.hdwrd.com
                DB::connection('bouncedb')->table('tgmBounces')->where('id', '=', $bounce->id)->delete();

            } // end foreach
        });

        Log::info('Completed Bounce Processing For Bounced Messages');

    }

 
}