<?php

namespace App\Console\Commands;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\domainSiteConfig;
use App\emailsSentDetail;
use App\myInformation;
use App\emailMIDQueue;
use App\emailQueue;
use App\emailsSent;
use Carbon\Carbon;
use App\domain;

class sendMailQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'HWCTQueue:sendMailQueue ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Messages In tgm_queue:emailQueue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        //get queued text message ID's
        Log::info('Starting Cron Run For Email Messages');
        $MIDQueue = emailMIDQueue::whereNull('updated_at')->first();

        $carbon = new Carbon();
        $dt = Carbon::now();

        while (!empty($MIDQueue)) {

            $domainSiteConfig = domainSiteConfig::where('domainID', '=', $MIDQueue->domainID)->first(); // 1 MID per execution       
            $carbon = new Carbon();
            $dt = Carbon::now();
            $mid = $MIDQueue->mid;
            $id = $MIDQueue->id;
            $tl = 0;

            $emailsSent = emailsSent::find($mid);
            $messages = emailQueue::where('messageID', '=', $mid)->get();
            $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
            
            Log::info('Processing email message ID: '. $mid);
            DB::table('emailsSent')->where('id', '=', $mid)->update(['sendStart' => $dt]);

            // set updated_at value to keep from overlapping processes
            DB::connection('maildb')->table('emailMIDQueue')->where('id', '=', $id)->update(['updated_at' => $dt]);

            foreach ($messages as $emailMessage) {
               
                $dt3 = Carbon::now();

                // emailMessageID - used in tracker
                $emailMessageID = DB::table('emailsSentDetails')->insertGetID(
                  [
                    'domainID' => $emailMessage->domainID,
                    'userID' => $emailMessage->userID,
                    'groupID' => $emailMessage->groupID,
                    'templateID' => $emailMessage->templateID,
                    'contactID' => $emailMessage->contactID,
                    'messageID' => $mid,
                    'fname' => $emailMessage->fname,
                    'lname' => $emailMessage->lname,
                    'company' => $emailMessage->company,                   
                    'messageType' => $emailMessage->messageType,
                    'emailAddress' => $emailMessage->emailAddress,
                    'subject' => $emailMessage->subject,
                    'emailBody' => $emailMessage->emailBody,
                    'textBody' => $emailMessage->textBody,
                    'attachment' => $emailMessage->attachment,
                    'created_at' => $dt3
                  ]
                );

                if (!is_null($emailMessage->emailBody)) {
                     // add tracker - email message format -> http:// $domain->httphost /t/m/   $emailMessageID
                    $type = 'html';
                    $httpHost = domain::find($emailMessage->domainID);
                    $linkURL = $httpHost->httpHost;
                    $trackerLink = "http://" . $linkURL . "/t/m/" . $mid . '/' . $emailMessageID;
                    $trackerIMG = '<img src="' . $trackerLink . '">';
                    Log::info($trackerIMG);

                    $emailMessage->emailBody = str_replace("[[trackerIMG]]", $trackerIMG, $emailMessage->emailBody);

                } else { 
                    $type = 'text'; 
                } // end if ! html 

                $to['email'] = $emailMessage->emailAddress;
                $to['name'] = $emailMessage->fname . " " . $emailMessage->lname;
                // Create the Transport using HWCTMailer
                $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);
                $mailer = HWCTMailer::createMailer($transport);
                $bounceAddress = $domainSiteConfig->sendEmail;
                
                // createEmailMessage($subject, $from, $html, $text, $type) {
                $message = HWCTMailer::createEmailMessage($emailMessage->subject, $sender , $bounceAddress, $emailMessage->emailBody, $emailMessage->textBody, $type);

                if (!empty($emailMessage->attachment)) {
                    $attachment = $emailMessage->attachment;
                    HWCTMailer::addAttachment($message, $attachment);
                }

                // setBounceHeaderInfo($message, $domainID, $userID, $contactID, $messageID, $mailtext)
                $message = HWCTMailer::setBounceHeaderInfo($message, $emailMessage->domainID, $emailMessage->userID, $emailMessage->contactID, $mid, "mail");

                
                // sendEmailMessage($mailer, $message, $to, $type) {
                $numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, $type);

                $tl = $tl + 1;
                emailQueue::destroy($emailMessage->id);
                
                // debug helper
                $this->info('Email Sent to: ' . $emailMessage->emailAddress . ' --> Queue ID: ' . $emailMessage->id );
                // set finish time as send time

            } // end while

            $dt2 = Carbon::now();
            $queueAmount = emailsSent::select('totalQueue')->where('id', '=', $mid)->first();
            $totalSentFail = $queueAmount->totalQueue - $tl;
            // update mid with tracker email
            $emailsSent->emailBody = $emailMessage->emailBody;
            $emailsSent->sendStop = $dt2;
            $emailsSent->totalSent = $tl;
            $emailsSent->totalSentFail = $totalSentFail;
            $emailsSent->save();

            // DB::table('emailsSent')->where('id', '=', $mid)->update(['sendStop' => $dt2, 'totalSent' => $tl, 'totalSentFail' => $totalSentFail]);
            Log::info('Completed email message ID: '. $mid . ' -> Total Email Messages Sent: ' . $tl);
            emailMIDQueue::destroy($id);
            $MIDQueue = emailMIDQueue::whereNull('updated_at')->first();
 
        } // end while
        
      Log::info('Stoping Cron Run For Email Messages');
                 
    } // end handle
    
} // end sendMailQueue