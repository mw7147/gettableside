<?php

namespace App\Console\Commands;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\textsSentDetails;
use App\domainSiteConfig;
use App\textMIDQueue;
use App\textsSent;
use Carbon\Carbon;
use App\textQueue;
use App\domain;

class sendTextQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'HWCTQueue:sendTextQueue ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Text Messages In DB tgm_queue:textQueue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        $name = $this->ask('What is your name?');
        $this->info('Your Name Is: ' . $name);
        */

        //get queued text message ID's
        Log::info('Starting Cron Run For textQueue');
        $MIDQueue = textMIDQueue::whereNull('updated_at')->first();
        $subject = '';

        while (!is_null($MIDQueue)) {
            
            $domainSiteConfig = domainSiteConfig::where('domainID', '=', $MIDQueue->domainID)->first(); // 1 MID per execution       
            $carbon = new Carbon();
            $dt = Carbon::now();
            $mid = $MIDQueue->mid;
            $id = $MIDQueue->id;
            $domain = domain::find($MIDQueue->domainID);
            $tl = 0;

            $messages = textQueue::where('messageID', '=', $mid)->get();

            Log::info('Processing Text Message MID: '. $mid);
            textMIDQueue::where('id', '=', $id)->update(['updated_at' => $dt]);

            // create the text
            $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
            
            // Create the Transport using HWCTMailer
            $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

            // Create the Mailer using created Transport
            $mailer = HWCTMailer::createMailer($transport);

            foreach ($messages as $textMessage) {

              $type = 'sms';

              // Create a message, $subject, $from, $message
              $message = HWCTMailer::createMessage($subject, $sender , $textMessage->message, $domainSiteConfig->sendEmail);

              if (!empty($textMessage->pixFile)) {
                $pixPath = 'public/' . $textMessage->pixFile;
                HWCTMailer::addAttachment($message, $pixPath);
                $type = 'mms';
              }

              // setBounceHeaderInfo($message, $domainID, $userID, $contactID, $messageID, $mailtext)
              $message = HWCTMailer::setBounceHeaderInfo($message, $textMessage->domainID, $textMessage->userID, $textMessage->contactID, $textMessage->messageType, "text");

          //    Log::info('textMessage->gateway: '. $textMessage->gateway);
              $numSent = HWCTMailer::sendTextQueueMessage($mailer, $message, $textMessage->gateway);

              $tl = $tl + 1;

              $t =  new textsSentDetails(); 
                $t->contactID = $textMessage->contactID;
                $t->domainID = $textMessage->domainID;
                $t->groupID = $textMessage->groupID;
                $t->templateID = $textMessage->templateID;
                $t->parentDomainID = $domain->parentDomainID;
                $t->messageID = $mid;
                $t->fname = $textMessage->fname;
                $t->lname = $textMessage->lname;
                $t->messageType = $textMessage->messageType;
                $t->message = $textMessage->message;
                $t->pixFile = $textMessage->pixFile;
                $t->mobile = $textMessage->mobile;
                $t->unformattedMobile = $textMessage->unformattedMobile;
                $t->carrierID = $textMessage->carrierID;
                $t->carrierName = $textMessage->carrierName;
                $t->gateway = $textMessage->gateway;
                $t->created_at = $dt;
              $t->save();

              textQueue::destroy($textMessage->id);


            } // end for each messages

            $dt2 = Carbon::now();
            $queueAmount = textsSent::select('totalQueue')->where('id', '=', $mid)->first();
            $totalSentFail = $queueAmount->totalQueue - $tl;
            
            $ts = textsSent::where('id', '=', $mid)->first();
              $ts->sendStart = $dt;
              $ts->sendStop = $dt2;
              $ts->totalSent = $tl;
              $ts->totalSentFail = $totalSentFail;
            $ts->save();

//            update(['sendStop' => $dt2, 'totalSent' => $tl, 'totalSentFail' => $totalSentFail]);
            textMIDQueue::destroy($id);
            
            Log::info('Completed text message ID: '. $mid . ' -> Total Text Messages Sent: ' . $tl);
            $MIDQueue = textMIDQueue::whereNull('updated_at')->first();
            
        }  // end while
        
        Log::info('Stoping Cron Run For textQueue');


    } // end handle







}
