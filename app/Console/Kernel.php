<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        Commands\sendTextQueue::class,
        Commands\sendMailQueue::class,
        Commands\processBouncedEmails::class,
        Commands\processFutureOrders::class

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

       // $schedule->command('HWCTQueue:sendTextQueue')->everyMinute();
       // $schedule->command('HWCTQueue:sendMailQueue')->everyMinute();
       // $schedule->command('HWCTMail:processBouncedEmails')->everyMinute();
    $schedule->command('H2IQ:processFutureOrders')->everyMinute();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
