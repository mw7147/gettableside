<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sendEmailAdmin extends Model
{
    protected $connection = 'emailAdminDB';
    protected $table = 'mailboxes';
}
