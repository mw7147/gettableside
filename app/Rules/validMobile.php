<?php

namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;
use Facades\App\hwct\CellNumberData;
use App\registeredNumbers;

class validMobile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $numOnly = CellNumberData::numbersOnly($value);
        $check = registeredNumbers::find($value);
        return !is_null($check);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The mobile number is not a valid mobile number. Please check your mobile number.';
    }
}
