<?php

namespace App\hwct;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Facades\App\hwct\printers\receiptLetter;
use Facades\App\hwct\printers\receiptThermal;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\printers\hpEprint;
use Facades\App\hwct\printers\pdfOnly;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\starPrintJobs;
use App\ordersDetail;
use App\siteStyles;
use App\starPrint;
use Carbon\Carbon;
use App\cartData;
use App\contacts;
use App\printers;
use App\orders;
use App\domain;

use Dompdf\Dompdf;

class PrintOrders {
	/*
		Functions for printing orders

		4 types of receipts are generated for each order.
			1. Letter Sized Receipt with pricing
			2. Thermal Sized Receipt with Pricing
			3. Letter sized order only - foldable vertically - suitable for kitchen
			4. Thermal sized order only - suitable for kitchen

		PDF is made for each receipt type. PDF path stored with order.
			NOTE -> need procedure to delete pdf's over x days old from system
		
		Thermal printers -> convert PDF to png or jpg image as needed - depending upon printer support required.

		Star Cloud Printers - png created from stored thermal pdf and copied to URL: http://starprint.togomeals.biz
			StarPrint protocol implemented on this server. StarPrint server handles printing job.

		HP ePrint printers - send email with attachment to printers email address for printing.

		Custom printing by domain ID and printer type - naming convention - EXTREMELY IMPORTANT to follow naming convention
			
			File name path -> env('PRINT_ROOT')

			File Names(s) ->	.../receipt/{domainID}/letterItems.php
								.../receipt/{domainID}/letterPDF.blade.php
								.../receipt/{domainID}/thermalItems.php
								.../receipt/{domainID}/thermalPDF.blade.php
								.../order/{domainID}/letterItems.php
								.../order/{domainID}/letterPDF.blade.php
								.../order/{domainID}/thermalItems.php
								.../order/{domainID}/thermalPDF.blade.php

		depending upon customer needs. If all 4 receipt types - create 4 receipt files (items and pdf for each - 8 total files for all 4), if 1, create 1, etc.
		place the custom print file in the custom directory to enable custom printing for customer.

	*/
/*------------------------------------------------------------------------------- RECEIPT LETTER ---------------------------------------------------------------------------------------------------------------------------------*/


    // Receipt Letter
    
	/*---------------- receipt letter PDF  ----------------------------------*/
	public function receiptLetterPDF($contact, $order, $domainSiteConfig) {

		$dt = Carbon::now();
		$folder = $dt->format('MY');

		// pdf for Letter Receipt
		$orderHTML = $this->receiptLetterItems($order);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);	
		
		// check for custom print pdf file
		$customPrintCheck = env('PRINT_ROOT') . '/receipt/' . $order->domainID . '/letterPDF.blade.php'; 
		if (file_exists($customPrintCheck)) {
			// custom pdf
			$pdf = \View::make('print.receipt.' . $order->domainID . '.letterPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		} else {
			// default pdf
			$pdf = \View::make('print.receipt.defaultLetterPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		}

		$pdfHTML = $pdf->render();
		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		
		// pdf_root/domainID/MMYYYY/orderID/receiptletter.pdf
	    $fp = env('PDF_ROOT') . $order->domainID . "/" . $folder . '/' . $order->id . "/receiptLetter.pdf";
	    //file_put_contents($fp, $dompdf->output() );
	    Storage::put( $fp, $dompdf->output(), 'public' );
		unset($dompdf);

		return $fp;

	} // end receiptLetterPDF


	/*----------------  Receipt Letter Items ----------------------------------*/
	// returns html for receipt pdf

	public function receiptLetterItems($order) {


		$cart = ordersDetail::where('sessionID', '=', $order->sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		$siteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
		$total = 0;
		$tax = 0;
		$tip = 0;
		$orderTotal = 0;
		$options = '';
		$addOns = '';

		$customItems = env('PRINT_ROOT') . '/receipt/' . $order->domainID . '/letterItems.php'; // check for custom print items file
		if (file_exists($customItems)) {
			include($customItems);
		} else {

			$html = $order->orderItemsHTML;

		} // end default items


	return $html;

	} //end receiptLetterItems




/*------------------------------------------------------------------------------- RECEIPT THERMAL ---------------------------------------------------------------------------------------------------------------------------------*/



    // Receipt Thermal
    
	/*---------------- receipt thermal PDF  ----------------------------------*/
	public function receiptThermalPDF($contact, $order, $domainSiteConfig) {

		$dt = Carbon::now();
		$folder = $dt->format('MY');

		$cart = ordersDetail::where('sessionID', '=', $order->sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		$orderHTML = $this->receiptThermalItems($cart, $order->domainID, $domainSiteConfig, $order);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);		

		// check for custom print pdf file
		$customPrintCheck = env('PRINT_ROOT') . '/receipt/' . $order->domainID . '/thermalPDF.blade.php'; 
		if (file_exists($customPrintCheck)) {
			// custom pdf
			$pdf = \View::make('print.receipt.' . $order->domainID . '.thermalPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		} else {
			// default pdf
			$pdf = \View::make('print.receipt.defaultThermalPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		}

		$pdfHTML = $pdf->render();
		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);

		// set paper size in points - Width, Height

		if (!is_null($domainSiteConfig->lineReceiptHeight)) {
			$height = $domainSiteConfig->lineReceiptHeight;
		} else {
			$height = 150;
		}

		// if (count($cart) == 2 || count($cart) == 3) { $base = 900; } else { $base = 750; }
		if (count($cart) <= 3) { $base = 900; } else { $base = 950; }

		$pl = (count($cart) * $height) + $base;
		$customPaper = array(0,0,600,$pl);
		
		
		$dompdf->setPaper($customPaper);

		//$dompdf->set_option( 'dpi' , '300' );
		//$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
	    $fp = env('PDF_ROOT') . $order->domainID . "/" . $folder . '/' . $order->id . "/receiptThermal.pdf";
		Storage::put( $fp, $dompdf->output(), 'public' );
		unset($dompdf);

		// convert to png - symfony process shell command
		$pp = env('PDF_ROOT') . $order->domainID . "/" . $folder . '/' . $order->id . "/receiptThermal.png";
		$command = 'convert ' . env('PRINT_STORAGE_PATH') . $fp . ' +profile "*" ' . env('PRINT_STORAGE_PATH') . $pp;

		$process = Process::fromShellCommandline($command);
		$process->run(null, [] );

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}

		return $pp;

	} // end receiptThermalPDF


	/*----------------  Receipt Thermal Items ----------------------------------*/
	// returns html for receipt pdf

	public function receiptThermalItems($cart, $domainID, $siteConfig, $order) {

		$total = 0;
		$tax = 0;
		$tip = 0;
		$orderTotal = 0;
		
		$options = '';
		$addOns = '';

		$customItems = env('PRINT_ROOT') . '/receipt/' . $domainID . '/thermalItems.php'; // check for custom print items file
		if (file_exists($customItems)) {
			include($customItems);
		} else {
			// default thermal items
			$html = '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
			foreach ($cart as $item) {
						
						// calculate menu options price
					//	$addMenuOptions = ProcessOrders::menuOptions($item->menuOptionsDataID);
					//	$addMenuAddOns = ProcessOrders::menuAddOns($item->menuAddOnsDataID);
					//	$addMenuSides = ProcessOrders::menuSides($item->menuSidesDataID);
					//	$price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;
					//	$price = $item->price + $addMenuOptions + $addMenuSides;
						$price = $item->price;
						$extendedPrice = $price * $item->quantity;

						$options = ProcessOrders::viewOptions($item->menuOptionsDataID, 'receipt');
					//	$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID, 'receipt');
						$sides = ProcessOrders::viewSides($item->menuSidesDataID, 'receipt');
						
						if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
						if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}
		
					//	$allOptions = $options . $addOns . $sides;
						$allOptions = $options . $sides;

						// strip any comma's and spaces from right side
						$allOptions = rtrim($allOptions, ', ');
		
						if (!empty($item->instructions)) {
							if ($allOptions == '' ) {
								$allOptions = '<span>Customer Instructions: ' . $item->instructions . '<br></span>';
							 } else {
								 $allOptions = $allOptions . '<br><span>Customer Instructions: ' . $item->instructions . '<br></span>';
							 }
						}
						
						$html .= '<tr>';
						$html .= '<td class="table-padding" align="left" style="font-size: 28px;">' . $item->quantity . '</td>';   			
						$html .= '<td class="table-padding" align="left" style="font-size: 28px;">' . $item->printReceipt . '</td>';   			
						$html .= '<td class="table-padding" align="left" style="font-size: 28px;">ea $' . number_format($price, 2) . '</td>';
						$html .= '<td class="table-padding" align="right" style="font-size: 28px;">$' . number_format($extendedPrice, 2) . '</td></tr>';
						$html .= '<tr><td colspan="1" class="table-bottom"></td><td class="table-bottom" style="padding-bottom: 4px; font-size: 28px;" colspan="3"><i>' . $item->portion . '</i></td><</tr>';
						$html .= '<tr><td colspan="1" class="table-bottom"></td><td class="table-bottom" style="padding-bottom: 10px; font-size: 28px;" colspan="3"><i>' . $allOptions . '</i></td><</tr>';
						
						/*
						if (!empty($item->instructions)) {
						 $html .= '<tr><td colspan="1"><td colspan="2"><div style="margin-left: 48px;"><span style="font-size: 28px; font-style: italic; margin-top: 8px; margin-bottom: 12px;">Note: ' . $item->instructions . '</span></div></td></tr>';
						}
						*/

						$total = $total + $extendedPrice;
						if (!empty($item->tipAmount)) { $tip = $tip + $item->tipAmount; } // add tip if any
		
			} // end foreach $cart
			
		
			if ($total > 0) {
							// don't display 0 total
			$orderDiscount = $order->discountAmount;
			$tax = $order->tax ?? 0;
			$orderDiscount = $order->discountAmount ?? 0;
			$tip = $order->tip ?? 0;
			

			if ($order->orderType == 'pickup') {
				// pickup
				$orderTotal = number_format($total + $tip + $tax + $order->transactionFee - $orderDiscount, 2);
			} else {
				// deliver
				$orderTotal = number_format($total + $tip + $tax + $order->transactionFee + $order->deliveryCharge - $orderDiscount, 2);
			}
			
				
				$html .= '<tr><td colspan="4" width="100%">--------------------------------------------------------------------------------------------------------------------------------------</td></tr>';
				
				$html .= '<tr>
						<td class="table-padding" align="left" width="70%" style="font-size: 28px;" colspan="3">Sub-Total</td>
						<td style="padding-top: 28px; padding-bottom: 12px; font-size: 28px;" width="30%" align="right">$' . number_format($total, 2) . '</td>
						</tr>';
						   
				if ($orderDiscount > 0) {
					$html .= '<tr>
						<td class="table-padding" align="left" width="70%" style="font-size: 28px;"  colspan="3">Order Discount</td>
						<td style="padding-bottom: 12px; font-size: 28px;" width="30%" align="right">($' . number_format($orderDiscount, 2) . ')</td>
						</tr>';
						
					$html .= '<tr>
						<td class="table-padding" align="left" width="70%" style="font-size: 28px;"  colspan="3">Sub Total After Discount</td>
						<td style="padding-bottom: 12px; font-size: 28px;" width="30%" align="right">$' . number_format($total - $orderDiscount, 2) . '</td>
						</tr>';
				}
		
				$html .= '<tr>
						<td class="table-padding" align="left" width="70%" style="font-size: 28px;"  colspan="3">Tax</td>
						<td style="padding-bottom: 12px; font-size: 28px;" width="30%" align="right" colspan="1">$' . number_format($tax, 2) . '</td>
						</tr>';
		
				$html .= '<tr>
						<td class="table-padding" align="left" width="70%" style="font-size: 28px;"  colspan="3">Tip Amount</td>
						<td style="padding-bottom: 12px; font-size: 28px;" width="30%" align="right" colspan="1">$' . number_format($tip, 2) . '</td>
						</tr>';
		
				$html .= '<tr>
							<td class="table-padding" align="left" width="70%" style="font-size: 28px;"  colspan="3"><b>Total</b></td>
							<td align="right" colspan="1" style="font-size: 28px;" width="30%" ><b>$' . number_format($orderTotal, 2) . '</b></td>
							</tr>';
		
		
			}
		
		$html .= '</table>';

		
		}	

		return $html;


	} //end receiptThermalItems




/*------------------------------------------------------------------------------- ORDER LETTER ---------------------------------------------------------------------------------------------------------------------------------*/



    // Order Letter - 1/2 fold vertically

	/*---------------- order letter PDF  ----------------------------------*/
	public function orderLetterPDF($contact, $order, $domainSiteConfig) {

		$dt = Carbon::now();
		$folder = $dt->format('MY');

		$orderHTML = $this->orderLetterItems($order->sessionID, $order->domainID);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);	

		// check for custom print pdf file
		$customPrintCheck = env('PRINT_ROOT') . '/order/' . $order->domainID . '/letterPDF.blade.php'; 
		if (file_exists($customPrintCheck)) {
			// custom pdf
			$pdf = \View::make('print.order.' . $order->domainID . '.letterPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		} else {
			// default pdf
			$pdf = \View::make('print.order.defaultLetterPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		}

		$pdfHTML = $pdf->render();

		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		$fp = env('PDF_ROOT') . $order->domainID . "/" . $folder . '/' . $order->id . "/orderLetter.pdf";
		//file_put_contents($fp, $dompdf->output() );
		Storage::put( $fp, $dompdf->output(), 'public' );
		unset($dompdf);
		
		return $fp;

	} // end orderLetterPDF



	/*----------------  Order Letter Items ----------------------------------*/
	
	// returns html for order letter pdf
	public function orderLetterItems($sessionID, $domainID) {

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="475" align="center" class="wrapper" style="font-family: Arial, sans-serif;">';
		$siteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
		$options = '';
		$addOns = '';

		$customItems = env('PRINT_ROOT') . '/order/' . $domainID . '/letterItems.php'; // check for custom print items file
		if (file_exists($customItems)) {
			include($customItems);
		} else {
			// default items
			foreach ($cart as $item) {

					// menu options
	 				$options = ProcessOrders::viewOptions($item->menuOptionsDataID, 'order');
	 			//	$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID, 'order');
	 				$sides = ProcessOrders::viewSides($item->menuSidesDataID, 'order');
	 				
	 				if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
					if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

				//	$allOptions = $options . $addOns . $sides;
					$allOptions = $options . $sides;

					// strip any comma's and spaces from right side
					$allOptions = rtrim($allOptions, ', ');

	 				if (!empty($item->instructions)) {
	 					$allOptions .= '<br><br><span>Customer Instructions: ' . $item->instructions . '</span>';
	 				}

					$html .= '<tr>';
					$html .= '<td class="table-padding" align="left" width="10%" style="font-size: 16px; font-family: Arial, sans-serif;">[&nbsp;]&nbsp;&nbsp;' . $item->quantity . '</td>';   			
	 				$html .= '<td class="table-padding" align="left" width="50%" style="font-size: 16px; font-family: Arial, sans-serif;">' . $item->printOrder . '</td>';   			
					$html .= '<td class="table-padding" width="40%" align="left" style="font-size: 16px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
	 				$html .= '<tr><td colspan="1" class="table-bottom"></td><td class="table-bottom" style="padding-bottom: 10px; font-size: 13px; " colspan="2"><i>' . $allOptions . '</i></td></tr>';

			} // end foreach $cart

			$html .= '</table><hr>';

		} // end if custom

	    return $html;

	} //end orderLetterItems






/*------------------------------------------------------------------------------- ORDER (KITCHEN) THERMAL -----------------------------------------------------------------------------------------------------------------------*/







    // Order Thermal - 3 inch wide paper

	/*---------------- order thermal PDF  ----------------------------------*/
	public function orderThermalPDF($contact, $order, $domainSiteConfig) {

		$dt = Carbon::now();
		$folder = $dt->format('MY');

		$cart = ordersDetail::where('sessionID', '=', $order->sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();

		$orderHTML = $this->orderThermalItems($cart, $order->domainID);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);	

		// check for custom print pdf file
		$customPrintCheck = env('PRINT_ROOT') . '/order/' . $order->domainID . '/thermalPDF.blade.php'; 
		if (file_exists($customPrintCheck)) {
			// custom pdf
			$pdf = \View::make('print.order.' . $order->domainID . '.thermalPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		} else {
			// default pdf
			$pdf = \View::make('print.order.defaultThermalPDF', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		}

		$pdfHTML = $pdf->render();

		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);

		// set paper size in points - Width, Height
		if (!is_null($domainSiteConfig->lineOrderHeight)) {
			$height = $domainSiteConfig->lineOrderHeight;
		} else {
			$height = 120;
		}

		if (count($cart) <= 3) { $base = 700; } else { $base = 800; }
		$pl = (count($cart) * $height) + $base;
		$customPaper = array(0,0,600,$pl);
		$dompdf->setPaper($customPaper);

		//$dompdf->setPaper('letter', 'portrait');
		// $dompdf->set_option( 'dpi' , '150' );  for wider printing
		$dompdf->render();
		$fp = env('PDF_ROOT') . $order->domainID . "/" . $folder . '/' . $order->id . "/orderThermal.pdf";
		//file_put_contents($fp, $dompdf->output() );
		Storage::put( $fp, $dompdf->output(), 'public' );
		unset($dompdf);

		// create png
		$pp = env('PDF_ROOT') . $order->domainID . "/" . $folder . '/' . $order->id . "/orderThermal.png";
		$command = 'convert ' . env('PRINT_STORAGE_PATH') . $fp . ' +profile "*" ' . env('PRINT_STORAGE_PATH') . $pp;
		
		// convert to png - symfony process shell command
		$process = Process::fromShellCommandline($command);
		$process->run(null, [] );

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}
		
		return $pp;

	} // end orderThermalPDF



	/*----------------  Order Thermal Items ----------------------------------*/
	
	// returns html for order thermal pdf - 3 inch wide thermal
	public function orderThermalItems($cart, $domainID) {

		//$cart = ordersDetail::where('sessionID', '=', $sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		//$html = '<table border="0" cellpadding="0" cellspacing="0" width="275" align="center" class="wrapper" style="font-family: Arial, sans-serif;">';
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="100%">';

		// $siteConfig = domainSiteConfig::find($domainID)->first();
		$options = '';
		$addOns = '';

		$customItems = env('PRINT_ROOT') . '/order/' . $domainID . '/thermalItems.php'; // check for custom print items file
		if (file_exists($customItems)) {
			include($customItems);
		} else {
			// default items
			foreach ($cart as $item) {

					// menu options
	 				$options = ProcessOrders::viewOptions($item->menuOptionsDataID, 'order');
	 		//		$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID, 'order');
	 				$sides = ProcessOrders::viewSides($item->menuSidesDataID, 'order');
	 				
	 				if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
					if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

	 			//	$allOptions = $options . $addOns . $sides;
	 				$allOptions = $options . $sides;

	 				if (!empty($item->instructions)) {
	 					$allOptions .= '<br><br><span>Customer Instructions: ' . $item->instructions . '</span>';
	 				}

					$html .= '<tr>';
					$html .= '<td class="table-padding" align="left" style="font-size: 42px; font-family: Arial, sans-serif;">[&nbsp;]&nbsp;&nbsp;' . $item->quantity . '</td>';   			
	 				$html .= '<td class="table-padding" align="left" style="font-size: 42px; font-family: Arial, sans-serif;">' . $item->printOrder . '</td></tr>';   			
					$html .= '<tr><td>&nbsp;</td><td class="table-padding" align="left" style="font-size: 42px; font-family: Arial, sans-serif;">' . $item->portion . '</td></tr>';
	 				$html .= '<tr><td>&nbsp;</td><td class="table-bottom" style="padding-bottom: 10px; font-size: 36px; " colspan="3"><i>' . $allOptions . '</i></td></tr>';

			} // end foreach $cart

			$html .= '</table>';

		} // end if custom

	    return $html;

	} //end orderThermalItems







/*------------------------------------------------------------------------------- PRINT RECEIPT ------------------------------------------------------------------------------------------------------------------------*/

	public function printReceipt($printer, $order, $domainSiteConfig, $type) {

		// add new printer types here
		if ( !is_null($domainSiteConfig->numOrderCopies) && $type == 'new') {
			$numCopies = $domainSiteConfig->numReceiptCopies;
		} else {
			$numCopies = 1;
		}

		if ($printer->type == 1) {
			// hp ePrint
			$print = $this->sendHpPdf($printer, $order, $domainSiteConfig, 'receipt', $numCopies);

		} elseif ($printer->type == 2) {
			// starPrint

			// print receipt
			$print = $this->starPrint($printer, $order, 'receipt', $numCopies);
		
		} else {
			// type unknown - don't print
			return false;
		}

		return $print;

	}
	// $printReceipt = PrintOrders::printReceipt($printer, $order, $domainSiteConfig);



/*------------------------------------------------------------------------------- PRINT ORDER --------------------------------------------------------------------------------------------------------------------------*/


	// add new printer types here
	public function printOrder($printer, $order, $domainSiteConfig, $type) {

		// number of copies of order to print
		if ( !is_null($domainSiteConfig->numOrderCopies) && $type == 'new') {
			// print multiple copies for new order
			$numCopies = $domainSiteConfig->numOrderCopies;
		} else {
			// single copy or reprint 1 copy
			$numCopies = 1;
		}

		if ($printer->type == 1) {
			// hp ePrint
			$print = $this->sendHpPdf($printer, $order, $domainSiteConfig, 'order', $numCopies);

		} elseif ($printer->type == 2) {
			// starPrint						
			// print order
			$print = $this->starPrint($printer, $order, 'order', $numCopies);

		} else {
			// type unknown - don't print
			return false;
		}

		return $print;

	}





/*------------------------------------------------------------------------------- HP EPRINT ----------------------------------------------------------------------------------------------------------------------------*/




	/*---------------- Send Hp PDF to hp ePrint service  ----------------------------------*/

	public function sendHpPdf($printer, $order, $domainSiteConfig, $type, $numCopies) {

		// create the swiftMessage
		$to['email'] = $printer->address ?? $domainSiteConfig->orderReceiveEmail;// printer address or order receive email if not set
		$to['name'] = $domainSiteConfig->locationName;
		$sender = $domainSiteConfig->sendEmail;
		
		// type -> receipt or order
		if ($type == 'receipt') {
			$pdfPath = env("PRINT_STORAGE_PATH") . $order->receiptLetterPath;
		} else {
			$pdfPath = env('PRINT_STORAGE_PATH') . $order->orderLetterPath;
		}

		for( $i = 1 ; $i <= $numCopies ; $i++ ) {

			$subject = $domainSiteConfig->locationName . "Online Order - " . $i;
			
			// Create the Transport using HWCTMailer
			$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

			// Create the Mailer using your created Transport
			$mailer = HWCTMailer::createMailer($transport);

			// Create a message, $subject, $from, $message
			$message = HWCTMailer::createPDFMessage($subject, $sender, $pdfPath);

			// Send the message
			$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email' );

		}

		if ($numSent > 0) {
			return true;
		} else {
			return false;
		}

	} // end sendOrderEmail






/*------------------------------------------------------------------------------- STARPRINT ----------------------------------------------------------------------------------------------------------------------------*/



	public function starPrint($printer, $order, $type, $numCopies) {

		// put png file to starPrint server for printing - update starPrint db
		$dt = Carbon::now();
		$folder = $dt->format('Ymd');

		$chkFolder = $this->checkFolder($folder);

		if (!$chkFolder) { abort(507); } // directory not created
		
		// type -> receipt or order
		for( $i = 1 ; $i <= $numCopies ; $i++ ) {

			if ($type == 'receipt') {
				$pngPath = $order->receiptThermalPath;
				$fn = $folder . '/' . $printer->id . microtime() . '-' . $i . '_R.png'; // receipt name
			} else {
				// order
				$pngPath = $order->orderThermalPath;
				$fn = $folder . '/' . $printer->id . microtime() . '-' . $i .'_O.png'; // order name
			}

			$putFile = Storage::get($pngPath);
			//$fn = $folder . '/' . $printer->id . microtime() . '.png';
			$pf = Storage::disk('starPrint')->getDriver()->put($fn, $putFile);

			Storage::disk('starPrint')->setVisibility($fn, 'public');
					

			if ($pf) {
				// write to db to trigger printing

				// set print file
				$job = new starPrintJobs();
					$job->domainID = $printer->domainID;
					$job->printerID = $printer->id;
					$job->macAddress = $printer->address;
					$job->orderID = $order->id ?? '';
					$job->filename = $fn;
					$job->sentToPrinter = 0;
				$job->save();

				// set print queue
				$print = starPrint::where('printerID', '=', $printer->id)->first();
					$print->printJobs = $print->printJobs + 1;
				$print->save();

			}  // end if $pf

		} // end for i

		return $pf;


	} // end starPrint




/*------------------------------------------------------------------------------- STARPRINT ----------------------------------------------------------------------------------------------------------------------------*/



	public function checkFolder($folder) {

		$viz = Storage::disk('starPrint')->exists($folder);

		if ( !$viz ) {

			$dir = Storage::disk('starPrint')->makeDirectory($folder);
			$dirViz = Storage::disk('starPrint')->setVisibility($folder, 'public');
			if ($dir) { $viz = true; }
		}
		
		return $viz;

	}


} // end Class PrintOrders


