<?php


namespace App\hwct;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\registeredNumbers;
use App\groupMember;
use App\groupName;
use Carbon\Carbon;
use App\contacts;

class messageHelpers {


	/*--------------------------------- Group Names -----------------------------------------------------*/   

	public function groupNames($domainID) {

      	$groups = groupName::leftJoin('groupMembers', 'groupNames.id', '=', 'groupMembers.groupID')
		    ->select('groupNames.id', 'groupNames.groupName', DB::raw('count(groupMembers.id) as groupCount'))
		    ->where('groupNames.domainID', '=', $domainID)
		    ->groupBy('groupNames.id', 'groupNames.groupName')
		    ->get();

		return $groups;

	} // end groupNames






} // end messageHelpers