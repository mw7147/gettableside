<?php

namespace App\hwct;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use Carbon\Carbon;
use App\contacts;
use App\domain;
use Config;



class SecureCard {

// helper functions for storing encrypted credit cards

	/*---------------- Luhn Check Credit Card  ----------------------------------*/

	function isValidCard($number) {
		// Strip any non-digits (useful for credit card numbers with spaces and hyphens)
		$number=preg_replace('/\D/', '', $number);
		// Set the string length and parity
		$number_length=strlen($number);
		$parity=$number_length % 2;
		// Loop through each digit and do the maths
		$total=0;
		for ($i=0; $i<$number_length; $i++) {
		$digit=$number[$i];
		// Multiply alternate digits by two
		if ($i % 2 == $parity) {
		$digit*=2;
		// If the sum is two digits, add them together (in effect)
		if ($digit > 9) {
		$digit-=9;
		}
		}
		// Total up the digits
		$total+=$digit;
		}
		// If the total mod 10 equals 0, the number is valid
		return ($total % 10 == 0) ? TRUE : FALSE;
    
    } // end isValidCard



    /*---------------- Encrypt Number  ----------------------------------*/

	function encryptNumber($number) {
        
        $key = env('APP_NUMBER');   
        $newEncrypter = new \Illuminate\Encryption\Encrypter( $key, Config::get( 'app.cipher' ) );
        $en = $newEncrypter->encrypt( $number );
        
        return $en;
    
    } // end encryptNumber




    /*---------------- Decrypt Number  ----------------------------------*/

	function decryptNumber($number) {
        
        $key = env('APP_NUMBER');   
        $newEncrypter = new \Illuminate\Encryption\Encrypter( $key, Config::get( 'app.cipher' ) );
        $de = $newEncrypter->decrypt( $number );
        
        return $de;
    
    } // end decryptNumber


        /*---------------- Encrypt Data  ----------------------------------*/

	function encryptData($data) {
        
        $key = env('APP_NUMBER');   
        $newEncrypter = new \Illuminate\Encryption\Encrypter( $key, Config::get( 'app.cipher' ) );
        $en = $newEncrypter->encrypt( $data );
        
        return $en;
    
    } // end encryptCard




    /*---------------- Decrypt Data  ----------------------------------*/

	function decryptData($data) {
        
        $key = env('APP_NUMBER');   
        $newEncrypter = new \Illuminate\Encryption\Encrypter( $key, Config::get( 'app.cipher' ) );
        $de = $newEncrypter->decrypt( $data );
        
        return $de;
    
    } // end encryptCard



} // end CardData

