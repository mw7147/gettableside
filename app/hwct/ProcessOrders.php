<?php

namespace App\hwct;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\menuOptionsData;
use App\menuAddOnsData;
use App\menuSidesData;
use App\ordersDetail;
use App\siteStyles;
use Carbon\Carbon;
use App\cartData;
use App\contacts;
use App\orders;
use App\domain;

use Dompdf\Dompdf;

class ProcessOrders {

// helper functions for order processing with credit cards

/*---------------- Send Order Email - Customer  ----------------------------------*/

	public function sendOrderEmail($contact, $order, $domainSiteConfig, $send = true) {

		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
		$domain = domain::where('id', '=', $order->domainID)->first();
		$siteStyles = siteStyles::where('domainID', '=', $domain->id)->first();

		$orderHTML = $this->emailHTML($order->sessionID, $order->domainID);
		$orderName = $this->orderName($contact);
		$ipPayment = $this->ipPayment($order);
		
		// set future pickup/deliver date in message
		$puTime = Carbon::parse($order->orderReadyTime);
		$futurePickupMessage = '';
		$futureDeliveryMessage = '';
		if ($order->orderInProcess == 9 ) {
			// future Order
			if ($order->orderType == "pickup") {
				// pickup
				$futurePickupMessage = str_replace("[[pickupTime]]", $puTime->format('l F jS Y \a\t g:i A'), $domainSiteConfig->futureOrderMessage);
			} else {
				// delivery
				$futureDeliveryMessage = str_replace("[[deliveryTime]]", $puTime->format('l F jS Y \a\t g:i A'), $domainSiteConfig->futureDeliveryMessage);

			}
		}

		$view = \View::make('orders.viewEmail', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'ipPayment' => $ipPayment, 'orders' => $order, 'siteStyles' => $siteStyles, 'futurePickupMessage' => $futurePickupMessage, 'futureDeliveryMessage' => $futureDeliveryMessage ]);

		// create the email html
		$type = "html";
		$textBody = '';
		$to['email'] = $order->customerEmailAddress;
		$to['name'] = $contact->fname . " " . $contact->lname;
		$sender = $domainSiteConfig->sendEmail;
        $html = $view->render();
        $numSent = 0;
        
        if ($send) {
			$subject = $domainSiteConfig->locationName . " Online Order Confirmation";
			// Create the Transport using HWCTMailer
			$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

			// Create the Mailer using your created Transport
			$mailer = HWCTMailer::createMailer($transport);

			// Create a message, $subject, $from, $message
			$message = HWCTMailer::createOrderMessage($subject, $sender , $html, $textBody, $type);

			$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email' );
		} // end if $send
		
		$data = [];
		$data['numSent'] = $numSent;
		$data['html'] = htmlentities($html);

		// set HP ePrint width in html
		$orderItemsHTML = str_replace('width="768"', 'width="475"', $orderHTML);
		$data['orderItemsHTML'] = $orderItemsHTML;

	    return $data;

	} // end sendOrderEmail



/*---------------- Send Domain Email - Domain Owner  ----------------------------------*/

public function sendDomainEmail($contact, $order, $domainSiteConfig) {

	$domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
	$domain = domain::where('id', '=', $order->domainID)->first();
	$siteStyles = siteStyles::where('domainID', '=', $domain->id)->first();

	$orderHTML = $this->emailHTML($order->sessionID, $order->domainID);
	$orderName = $this->orderName($contact);
	$ipPayment = $this->ipPayment($order);


	$puTime = Carbon::parse($order->orderReadyTime);
	$futurePickupMessage = '';
	$futureDeliveryMessage = '';
	if ($order->orderInProcess == 9 ) {
		// future Order
		if ($order->orderType == "pickup") {
			// pickup
			$futurePickupMessage = str_replace("[[pickupTime]]", $puTime->format('l F jS Y \a\t g:i A'), $domainSiteConfig->futureOrderMessage);
		} else {
			// delivery
			$futureDeliveryMessage = str_replace("[[deliveryTime]]", $puTime->format('l F jS Y \a\t g:i A'), $domainSiteConfig->futureDeliveryMessage);
		}
	}

	$view = \View::make('orders.viewEmail', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'ipPayment' => $ipPayment, 'orders' => $order, 'siteStyles' => $siteStyles, 'futurePickupMessage' => $futurePickupMessage, 'futureDeliveryMessage' => $futureDeliveryMessage ]);

	// create the email html
	$type = "html";
	$textBody = '';
	$to['email'] = $domainSiteConfig->orderReceiveEmail;
	$to['name'] = $domainSiteConfig->managerFname . " " . $domainSiteConfig->managerLname;
	$sender = $domainSiteConfig->sendEmail;
	$orderReceiveEmail = $domainSiteConfig->orderReceiveEmail;
	$html = $view->render();
	$subject = $order->fname . " " . $order->lname . " - Order ID: " . $order->id . " - Online Order";
	// Create the Transport using HWCTMailer
	$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

	// Create the Mailer using your created Transport
	$mailer = HWCTMailer::createMailer($transport);

	// Create a message, $subject, $from, $message
	$message = HWCTMailer::createOrderMessage($subject, $sender , $html, $textBody, $type);
	$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email' );

	return $numSent;

} // end sendDomainEmail


/*---------------- View Order Email  ----------------------------------*/

	public function viewEmail($ordersID) {

		$orders = orders::find($ordersID);
		if (empty($orders)) { abort(404); }

 		$domain = \Request::get('domain');
 		if ($orders->domainID != $domain['id'] ) { abort(404); }
		
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();	
		//dd($domainSiteConfig);
		$orders = orders::find($ordersID);
		$contact = contacts::where('id', '=', $orders->contactID)->first();

	    $orderHTML = $this->emailHTML($orders->sessionID, $orders->domainID);
		$orderName = $this->orderName($contact);
		$ipPayment = $this->ipPayment($order);

		$contact = contacts::find($orders->contactID);
		$orderName = $this->orderName($contact);

		// uncomment to test pdf
		//$pdf = $this->createPDF($contact, $orders, $domainSiteConfig);
		$sendPDF = $this->sendPDF($contact, $orders, $domainSiteConfig);

	    return view('orders.viewEmail')->with([ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'ipPayment' => $ipPayment, 'orderName' => $orderName, 'orders' => $orders ]);

	} // end viewEmail





	/*----------------  Email HTML ----------------------------------*/
	// returns html for order email

	public function emailHTML($sessionID, $domainID) {

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="768" align="center" class="wrapper">';
		$siteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
		$order = orders::where('sessionID', '=', $sessionID)->first();
		$total = 0;
		$orderTotal = 0;
		$options = '';
		$addOns = '';
		foreach ($cart as $item) {
						 				
					// calculate menu options price
				//	$addMenuOptions = ProcessOrders::menuOptions($item->menuOptionsDataID);
				//	$addMenuAddOns = ProcessOrders::menuAddOns($item->menuAddOnsDataID);
				//	$addMenuSides = ProcessOrders::menuSides($item->menuSidesDataID);
				//	$price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;
				//	$price = $item->price + $addMenuOptions + $addMenuSides;
					$price = $item->price;
					$extendedPrice = $price * $item->quantity;

					$options = ProcessOrders::viewOptions($item->menuOptionsDataID, 'receipt');
				//	$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID, 'receipt');
					$sides = ProcessOrders::viewSides($item->menuSidesDataID, 'receipt');
				
					if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
					if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

				//	$allOptions = $options . $addOns . $sides;
					$allOptions = $options . $sides;
					// strip any comma's and spaces from right side
					$allOptions = rtrim($allOptions, ', ');

	 				if (!empty($item->instructions)) {
						if ($allOptions == '' ) {
							$allOptions = '<span>Customer Instructions: ' . $item->instructions . '<br></span>';
						 } else {
						 	$allOptions = $allOptions . '<br><span>Customer Instructions: ' . $item->instructions . '<br></span>';
						 }
	 				}

					$html .= '<tr>';
					$html .= '<td class="table-padding" align="left" width="10%" style="font-size: 14px; font-family: Arial, sans-serif;">' . $item->quantity . '</td>';   			
	 				$html .= '<td class="table-padding" align="left" width="35%" style="font-size: 14px; font-family: Arial, sans-serif;">' . $item->menuItem . '</td>';   			
					$html .= '<td class="table-padding" align="left" width="35%" style="font-size: 14px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
					$html .= '<td class="table-padding" align="left" width="10%" style="font-size: 14px; font-family: Arial, sans-serif;">ea $' . number_format($price, 2) . '</td>';
	 				$html .= '<td class="table-padding" align="right" width="10%" style="font-size: 14px; font-family: Arial, sans-serif;">$' . number_format($extendedPrice, 2) . '</td></tr>';
	 				$html .= '<tr><td class="table-bottom" width="10%"></td><td class="table-bottom" style="padding-bottom: 10px; font-size: 14px; font-family: Arial, sans-serif;" colspan="4"><i>' . $allOptions . '</i></td></tr>';

	 				$total = $total + $extendedPrice;

		} // end foreach $cart

		if ($total > 0) {
			// don't display 0 total
			$orderDiscount = $order->discountAmount;
			$tax = $order->tax ?? 0;
			$orderDiscount = $order->discountAmount ?? 0;
			$tip = $order->tip ?? 0;
			

			if ($order->orderType == 'pickup') {
				// pickup
				$orderTotal = number_format($total + $tip + $tax + $order->transactionFee - $orderDiscount, 2);
			} else {
				// deliver
				$orderTotal = number_format($total + $tip + $tax + $order->transactionFee + $order->deliveryCharge - $orderDiscount, 2);
			}
			
			$html .= '<tr><td style="padding-top: 28px; padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Sub-Total</td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($total, 2) . '</td></tr>';
					
			if ($orderDiscount > 0) {
				
				$html .= '<tr><td style="padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Order Discount (' . $siteConfig->globalDiscount . ' Percent)</td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">($' . number_format($orderDiscount, 2) . ')</td></tr>';
				
				$html .= '<tr><td style="padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Order Discount SubTotal</td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($total - $orderDiscount, 2) . '</td></tr>';
			}	
			
			if ($order->orderType == "deliver") {
				$html .= '<tr><td style="padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Delivery Fee</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($order->deliveryCharge, 2) . '</td></tr>';	
			}
			
			if ($order->transactionFee > 0) {
				$html .= '<tr><td style="padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Tax</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($order->transactionFee, 2) . '</td></tr>';	
			}
			

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Tax</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tax, 2) . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="2"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2">Tip Amount</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tip, 2) . '</td></tr>';

			$html .= '<tr><td colspan="2"></td><td colspan="2" style="font-size: 14px; font-family: Arial, sans-serif;"><b>Total</b></td><td align="right" colspan="1" style="font-size: 14px; font-family: Arial, sans-serif;"><b>$' . number_format($orderTotal, 2) . '</b></td></tr>';

		}

	    $html .= '</table>';

	    return $html;

	} //end emailHTML




	/*---------------- Calculate Menu Options Price ----------------------------------*/

	public function menuOptions($ids) {

		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); } // if ajax - $ids is array - otherwise json
		$tl = 0;

		for ($i = 0; $i < count($ids); $i++) {
			$upCharge = menuOptionsData::where('id', '=', $ids[$i])->first()->upCharge;
			if (empty($upCharge)) { continue; }
			$tl = $tl + $upCharge;
		}

		return $tl;

	} // end menuOption


	/*---------------- Calculate Menu Add Ons Price ----------------------------------*/

	public function menuAddOns($ids) {

		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); }  // if ajax - $ids is array - otherwise json
		$tl = 0;

		for ($i = 0; $i < count($ids); $i++) {
			$upCharge = menuAddOnsData::where('id', '=', $ids[$i])->first()->upCharge;
			if (empty($upCharge)) { continue; }
			$tl = $tl + $upCharge;
		}

		return $tl;

	} // end menuAddOns



	/*---------------- Calculate Menu Sides Price ----------------------------------*/

	public function menuSides($ids) {

		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); }  // if ajax - $ids is array - otherwise json
		$tl = 0;

		for ($i = 0; $i < count($ids); $i++) {
			$upCharge = menuSidesData::where('id', '=', $ids[$i])->first()->upCharge;
			if (empty($upCharge)) { continue; }
			$tl = $tl + $upCharge;
		}

		return $tl;

	} // end menuSides




	/*---------------- Get Menu Options ----------------------------------*/

	public function viewOptions($ids, $type) {


		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); } // if ajax - $ids is array - otherwise json
		$options = '';

		for ($i = 0; $i < count($ids); $i++) {

			$id = menuOptionsData::where('id', '=', $ids[$i])->first();

			if ($type == 'order') {
				// set printOrder
				if (is_null($id->printKitchen)) {
					$tmp = $id->optionName;
				} else {
					$tmp = $id->printKitchen;
				}
			} elseif ($type == 'receipt') {
				// set printReceipt
				if (is_null($id->printReceipt)) {
					$tmp = $id->optionName;
				} else {
					$tmp = $id->printReceipt;
				}	
			} else {
				// something else
				$tmp = $id->optionName;
			}

			/*
			// remove peppers
			$pepper = '<i class="fas fa-pepper-hot"></i>';
			$tmp = str_replace($pepper, '', $tmp, $numPeppers);
			if ($numPeppers >0 ) { $tmp = str_replace(' - ', '', $tmp); }
			*/

			if (empty($tmp)) { continue; }

			if ($i == 0) {
				$options = $tmp;
			} else {
				$options .= ", " . $tmp;
			} // end if

		} // end for

		return $options;

	} // end viewOptions



	/*---------------- Get Menu Add Ons  ----------------------------------*/

	public function viewAddOns($ids, $type) {

		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); } // if ajax - $ids is array - otherwise json
		$addOns = '';

		for ($i = 0; $i < count($ids); $i++) {

			$id = menuAddOnsData::where('id', '=', $ids[$i])->first();

			if ($type == 'order') {
				// set printOrder
				if (is_null($id->printKitchen)) {
					$tmp = $id->optionName;
				} else {
					$tmp = $id->printKitchen;
				}
			} elseif ($type == 'receipt') {
				// set printReceipt
				if (is_null($id->printReceipt)) {
					$tmp = $id->optionName;
				} else {
					$tmp = $id->printReceipt;
				}	
			} else {
				// something else
				$tmp = $id->optionName;
			}

			if (empty($tmp)) { continue; }

			if ($i == 0) {
				$addOns = $tmp;
			} else {
				$addOns .= ", " . $tmp;
			} // end if

		} // end for

		return $addOns;

	} // end viewAddOns

	/*---------------- Get Side Items  ----------------------------------*/

	public function viewSides($ids, $type) {

		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); } // if ajax - $ids is array - otherwise json
		$sides = '';

		for ($i = 0; $i < count($ids); $i++) {

			$id = menuSidesData::where('id', '=', $ids[$i])->first();

			if ($type == 'order') {
				// set printOrder
				if (is_null($id->printKitchen)) {
					$tmp = $id->optionName;
				} else {
					$tmp = $id->printKitchen;
				}
			} elseif ($type == 'receipt') {
				// set printReceipt
				if (is_null($id->printReceipt)) {
					$tmp = $id->optionName;
				} else {
					$tmp = $id->printReceipt;
				}	
			} else {
				// something else
				$tmp = $id->optionName;
			}

			if (empty($tmp)) { continue; }

			if ($i == 0) {
				$sides = $tmp;
			} else {
				$sides .= ", " . $tmp;
			} // end if

		} // end for

		return $sides;

	} // end viewSides






	/*---------------- Luhn Check Credit Card  ----------------------------------*/

	function is_valid_card($number) {
		// Strip any non-digits (useful for credit card numbers with spaces and hyphens)
		$number=preg_replace('/\D/', '', $number);
		// Set the string length and parity
		$number_length=strlen($number);
		$parity=$number_length % 2;
		// Loop through each digit and do the maths
		$total=0;
		for ($i=0; $i<$number_length; $i++) {
		$digit=$number[$i];
		// Multiply alternate digits by two
		if ($i % 2 == $parity) {
		$digit*=2;
		// If the sum is two digits, add them together (in effect)
		if ($digit > 9) {
		$digit-=9;
		}
		}
		// Total up the digits
		$total+=$digit;
		}
		// If the total mod 10 equals 0, the number is valid
		return ($total % 10 == 0) ? TRUE : FALSE;
	}


	/*---------------- orderName  ----------------------------------*/


	public function orderName($contact) {

		$orderName = $contact->fname . " " . $contact->lname . "<br>";
		if (!empty($contact->address1)) { $orderName .= $contact->address1 . "<br>"; }
		if (!empty($contact->address2)) { $orderName .= $contact->address2 . "<br>"; }
		if (!empty($contact->city)) { $orderName .= $contact->city . ", " . $contact->state . " " . $contact->zip . "<br>"; }
		$orderName .= $contact->mobile;

		return $orderName;

	} // end orderName



	/*---------------- orderNameMobile  ----------------------------------*/


	public function orderNameMobile($contact) {

		$orderNameMobile = $contact->fname . " " . $contact->lname . "<br>";
		$orderNameMobile .= $contact->mobile;

		return $orderNameMobile;

	} // end orderNameMobile




	/*---------------- ipPayment  ----------------------------------*/


	public function ipPayment($order) {

		$ipPayment = "IP Address: " . $order->orderIP . "<br>";
		$ipPayment .= "Payment Method: " . $order->paymentType . " " . $order->ccTypeLast4 . "<br>";
		
		return $ipPayment;

	} // end orderName

	public function orderEmail($contact, $order, $status) {

		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
		$domain = domain::where('id', '=', $order->domainID)->first();
		$siteStyles = siteStyles::where('domainID', '=', $domain->id)->first();

		$orderHTML = $this->emailHTML($order->sessionID, $order->domainID);
		$orderName = $this->orderName($contact);
		$ipPayment = $this->ipPayment($order);
		
		// set future pickup/deliver date in message
		$puTime = Carbon::parse($order->orderReadyTime);
		$futurePickupMessage = '';
		$futureDeliveryMessage = '';

		$view = \View::make('orders.viewEmail', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'ipPayment' => $ipPayment, 'orders' => $order, 'siteStyles' => $siteStyles, 'futurePickupMessage' => $futurePickupMessage, 'futureDeliveryMessage' => $futureDeliveryMessage ]);

		// create the email html
        $html = $view->render();
       
		$data['html'] = htmlentities($html);

		// set HP ePrint width in html
		$orderItemsHTML = str_replace('width="768"', 'width="475"', $orderHTML);
		$data['orderItemsHTML'] = $orderItemsHTML;

	    return $data;

	} // end orderEmail



} // end Class ProcessOrders


