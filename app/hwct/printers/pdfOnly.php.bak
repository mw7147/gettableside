<?php

namespace App\hwct\printers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\domainSiteConfig;
use App\ordersDetail;
use App\siteStyles;
use Carbon\Carbon;
use App\cartData;
use App\contacts;
use App\printers;
use App\orders;
use App\domain;

use Dompdf\Dompdf;

class hpEprint {

    // pdf only print Helper functions
    
	/*---------------- receipt PDF  ----------------------------------*/
	public function receiptPDF($contact, $order, $domainSiteConfig) {

		// pdf for pdf Receipt
		$orderHTML = $this->receiptPDFHTML($order->sessionID, $order->domainID);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);		

		$pdf = \View::make('orders.print.pdfOnly.pdfReceipt', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		$pdfHTML = $pdf->render();

		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);
		$dompdf->setPaper('letter', 'portrait');
	    $dompdf->render();
	    $fp = env('PDF_ROOT') . $order->domainID . "/" . $order->id . ".pdf";
	    Storage::put( $fp, $dompdf->output() );
	    unset($dompdf);
	    
		return $fp;

	} // end receiptPDF


	/*----------------  receipt PDF HTML ----------------------------------*/
	// returns html for receipt pdf

	public function receiptPDFHTML($sessionID, $domainID) {

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="475" align="center" class="wrapper">';
		$siteConfig = domainSiteConfig::find($domainID)->first();
		$total = 0;
		$tax = 0;
		$tip = 0;
		$orderTotal = 0;
		$options = '';
		$addOns = '';
		foreach ($cart as $item) {

					// calculate menu options price
	 				$addMenuOptions = ProcessOrders::menuOptions($item->menuOptionsDataID);
	 				$addMenuAddOns = ProcessOrders::menuAddOns($item->menuAddOnsDataID);
	 				$addMenuSides = ProcessOrders::menuSides($item->menuSidesDataID);
	 				$price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;

	 				$options = ProcessOrders::viewOptions($item->menuOptionsDataID);
	 				$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID);
	 				$sides = ProcessOrders::viewSides($item->menuSidesDataID);
	 				
	 				if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
					if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

	 				$allOptions = $options . $addOns . $sides;

	 				if (!empty($item->instructions)) {
	 					$allOptions .= '<br><br><span">Customer Instructions: ' . $item->instructions . '</span>';
	 				}

					$html .= '<tr>';
	 				$html .= '<td class="table-padding" align="left" width="35%" style="font-size: 13px; font-family: Arial, sans-serif;">' . $item->menuItem . '</td>';   			
					$html .= '<td class="table-padding" width="50%" align="left" style="font-size: 13px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
	 				$html .= '<td class="table-padding" align="right" width="15%" style="font-size: 13px; font-family: Arial, sans-serif;">$' . number_format($price, 2) . '</td></tr>';
	 				$html .= '<tr></td><td class="table-bottom" style="padding-left: 30px; padding-bottom: 10px; font-size: 13px; font-family: Arial, sans-serif;" colspan="2"><i>' . $allOptions . '</i></td><td class="table-bottom"></tr><tr><td colspan="3"></td></tr>';

	 				$total = $total + $price;
	 				if (!empty($item->tipAmount)) { $tip = $tip + $item->tipAmount; } // add tip if any

		} // end foreach $cart

		if ($total > 0) {
			// don't display 0 total
			$tax = $total * $siteConfig->taxRate/100;
			$orderTotal = number_format($total + $tip + $tax, 2);
			$html .= '<hr><tr><td style="padding-top: 28px; padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 13px; font-family: Arial, sans-serif;" colspan="1">Sub-Total</td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 13px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($total, 2) . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Tax</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tax, 2) . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Tip Amount</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tip, 2) . '</td></tr>';

			$html .= '<tr><td colspan="1"></td><td colspan="1" style="font-size: 13px; font-family: Arial, sans-serif;"><b>Total</b></td><td align="right" colspan="1" style="font-size: 14px; font-family: Arial, sans-serif;"><b>$' . number_format($orderTotal, 2) . '</b></td></tr>';


		}

	    $html .= '</table>';

	    return $html;

	} //end receiptPDFHTML






	/*---------------- create order PDF  ----------------------------------*/


	public function orderPDF($contact, $order, $domainSiteConfig) {


		$orderHTML = $this->orderPDFHTML($order->sessionID, $order->domainID);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);		

		$pdf = \View::make('orders.print.pdfOnly.pdfOrder', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);
		$pdfHTML = $pdf->render();
		
		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		$fp = env('PDF_ROOT') . $order->domainID . "/small-" . $order->id . ".pdf";
		Storage::put( $fp, $dompdf->output() );
		unset($dompdf);
		
		return $fp;

	} // end orderPDF



	/*----------------  narrow pdf HTML ----------------------------------*/
	// returns html for order pdf - 3 inch wide printer - no pricing

	public function orderPDFHTML($sessionID, $domainID) {

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="275" align="center" class="wrapper" style="font-family: Arial, sans-serif;">';
		$siteConfig = domainSiteConfig::find($domainID)->first();
		$options = '';
		$addOns = '';
		foreach ($cart as $item) {

					// menu options
	 				$options = ProcessOrders::viewOptions($item->menuOptionsDataID);
	 				$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID);
	 				$sides = ProcessOrders::viewSides($item->menuSidesDataID);
	 				
	 				if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
					if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

	 				$allOptions = $options . $addOns . $sides;

	 				if (!empty($item->instructions)) {
	 					$allOptions .= '<br><br><span>Customer Instructions: ' . $item->instructions . '</span>';
	 				}

					$html .= '<tr>';
	 				$html .= '<td class="table-padding" align="left" width="75%" style="font-size: 12px; font-family: Arial, sans-serif;">' . $item->menuItem . '</td>';   			
					$html .= '<td class="table-padding" width="25%" align="left" style="font-size: 12px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
	 				$html .= '<tr></td><td class="table-bottom" style="padding-left: 15px; padding-bottom: 10px; font-size: 10px; " colspan="2"><i>' . $allOptions . '</i></td><td class="table-bottom"></tr><tr><td colspan="3"></td></tr>';

		} // end foreach $cart

	    $html .= '</table><hr>';

	    return $html;

	} //end orderPDFHTML



	/*---------------- Send PDF to hp ePrint service  ----------------------------------*/

	public function sendPDF($printer, $order, $domainSiteConfig) {

		// create the swiftMessage
		$to['email'] = $printer->address ?? $domainSiteConfig->orderReceiveEmail;// printer address or order receive email if not set
		$to['name'] = $domainSiteConfig->locationName;
		$sender = $domainSiteConfig->sendEmail;
		$pdfPath = env("PDF_PUBLIC_ROOT") . $order->pdfPath;
		$subject = $domainSiteConfig->locationName . "Online Order";
		
		// Create the Transport using HWCTMailer
		$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

		// Create the Mailer using your created Transport
		$mailer = HWCTMailer::createMailer($transport);

		// Create a message, $subject, $from, $message
		$message = HWCTMailer::createPDFMessage($subject, $sender, $pdfPath);

		// Send the message
		$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email' );

		return $numSent;

	} // end sendOrderEmail


} // end hpEprint