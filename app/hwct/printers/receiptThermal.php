<?php

namespace App\hwct\printers;

use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\domainSiteConfig;
use App\ordersDetail;
use App\siteStyles;
use Carbon\Carbon;
use App\cartData;
use App\contacts;
use App\orders;
use App\domain;

use Dompdf\Dompdf;

class receiptThermal {

    // Receipt Thermal


	/*---------------- create thermal PDF  ----------------------------------*/
	public function thermalPDF($contact, $order, $domainSiteConfig) {


		$orderHTML = $this->orderPDFHTML($order->sessionID, $order->domainID);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = ProcessOrders::orderName($contact);		

		$pdf = \View::make('orders.pdfNarrowEmail', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order ]);

		$pdfHTML = $pdf->render();

		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		$fp = env('PDF_ROOT') . $order->domainID . "/small-" . $order->id . ".pdf";
		//file_put_contents($fp, $dompdf->output() );
		Storage::put( $fp, $dompdf->output() );
		unset($dompdf);
		
		return $fp;

	} // end thermalPDF



	/*----------------  thermal pdf HTML ----------------------------------*/
	// returns html for receipt thermal pdf - 3 inch wide printer

	public function thermalPDFHTML($sessionID, $domainID) {

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->orderBy('menuCategoriesDataSortOrder', 'asc')->orderBy('menuDataSortOrder')->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="275" align="center" class="wrapper" style="font-family: Arial, sans-serif;">';
		$siteConfig = domainSiteConfig::find($domainID)->first();
		$options = '';
		$addOns = '';
		foreach ($cart as $item) {

					// menu options
	 				$options = ProcessOrders::viewOptions($item->menuOptionsDataID);
	 				$addOns = ProcessOrders::viewAddOns($item->menuAddOnsDataID);
	 				$sides = ProcessOrders::viewSides($item->menuSidesDataID);
	 				
	 				if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
					if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

	 				$allOptions = $options . $addOns . $sides;

	 				if (!empty($item->instructions)) {
	 					$allOptions .= '<br><br><span>Customer Instructions: ' . $item->instructions . '</span>';
	 				}

					$html .= '<tr>';
	 				$html .= '<td class="table-padding" align="left" width="75%" style="font-size: 12px; font-family: Arial, sans-serif;">' . $item->menuItem . '</td>';   			
					$html .= '<td class="table-padding" width="25%" align="left" style="font-size: 12px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
	 				$html .= '<tr></td><td class="table-bottom" style="padding-left: 15px; padding-bottom: 10px; font-size: 10px; " colspan="2"><i>' . $allOptions . '</i></td><td class="table-bottom"></tr><tr><td colspan="3"></td></tr>';

		} // end foreach $cart

	    $html .= '</table><hr>';

	    return $html;

	} //end thermalPDFHTML


} // end receiptThermal