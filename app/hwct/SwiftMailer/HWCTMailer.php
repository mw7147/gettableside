<?php

 /*
  |----------------------------------------------------------------------------------------------
  | HWCT SwiftMailer Wrapper for Laravel 5.4+ - http://swiftmailer.org
  |----------------------------------------------------------------------------------------------
  |
  */

namespace App\hwct\SwiftMailer;
//require_once env('SITE_ROOT') . 'vendor/swiftmailer/swiftmailer/lib/swift_required.php';

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\emailUsers;
use Carbon\Carbon;
use App\domain;

class HWCTMailer {


  	/*
  	|----------------------------------------------------------------------------------------------
  	| create swiftmailer transport - $transport
  	|----------------------------------------------------------------------------------------------
  	|
  	*/

  	function createTransport($userID) {

  		$emailUser = emailUsers::where('usersID', '=', $userID)->first();

  		// not authorized user or user not found
  		if (is_null($emailUser)) {abort(404);}

		// Create the Transport
		$transport = (new \Swift_SmtpTransport($emailUser->mailServer, $emailUser->mailServerPort, $emailUser->encryption))
		  ->setUsername($emailUser->userName)
		  ->setPassword(decrypt($emailUser->password))
		;

		return $transport;
	
  	} // end createTransport

    /*
    |----------------------------------------------------------------------------------------------
    | create swiftmailer order transport - $transport
    |----------------------------------------------------------------------------------------------
    |
    */

    function createOrderTransport($fromUserName, $fromPassword) {

    // Create the Transport
    $transport = (new \Swift_SmtpTransport(env('ORDERS_MAIL_SERVER'), env('ORDERS_MAIL_SERVER_PORT'), env('ORDERS_MAIL_SERVER_ENCRYPTION')))
      ->setUsername($fromUserName)
      ->setPassword(decrypt($fromPassword))
    ;

    return $transport;
  
    } // end createTransport



  	/*
  	|----------------------------------------------------------------------------------------------
  	| create swiftmailer mailer - $mailer
  	|----------------------------------------------------------------------------------------------
  	|
  	*/
	

	function createMailer($transport) {

		$mailer = new \Swift_Mailer($transport);

		return $mailer;
		
  	} // end createMailer




  	/*
  	|----------------------------------------------------------------------------------------------
  	| create swiftmailer mailer - $message
  	|----------------------------------------------------------------------------------------------
  	|
  	*/

  	function createMessage($subject, $from, $body, $returnEmail) {

      if ($subject = 'no') {

        $message = (new \Swift_Message())
          //->setReturnPath(env('ORDERS_BOUNCE_ADDRESS'))
          ->setReturnPath($returnEmail)          
          ->setFrom($from)
          ->setBody($body)
        ;

      } else {

  		  $message = (new \Swift_Message())
         //->setReturnPath(env('ORDERS_BOUNCE_ADDRESS'))
         ->setReturnPath($returnEmail)
		      ->setSubject($subject)
		      ->setFrom($from)
		      ->setBody($body)
        ;

      }

		return $message;

    } // end createMessage




    /*
    |----------------------------------------------------------------------------------------------
    | create swiftmailer add attachment - $message => Swift_Message instance
    |----------------------------------------------------------------------------------------------
    |
    */

    function addAttachment($message, $attachment) {
     // /storage/attache/3/cannibisjpg.jpeg
     // PUBLIC_ROOT=/var/www/tokinmail-web-client/storage/app/
      $attachment = env("PUBLIC_ROOT") . str_replace("/storage", "public", $attachment);
      $message->attach(\Swift_Attachment::fromPath($attachment));

    return $message;

    } // end addAttachment




    /*
    |----------------------------------------------------------------------------------------------
    | create swiftmailer mailer - $message - no subject
    |----------------------------------------------------------------------------------------------
    |
    */

    function createTextMessage() {

      $message = (new \Swift_Message())
        ->setFrom(['2053010265@hardwiredcreative.com' => 'Mark Wood'])
        ->setTo(['2053010265@vtext.com' => 'Missy Wood'])
        ->setBody('Here is the wonderful test message itself from textmessage')
        ;

    return $message;

    } // end createMessage



 
  	/*
  	|----------------------------------------------------------------------------------------------
  	| send swiftmailer message - $numSent - text messages
  	|----------------------------------------------------------------------------------------------
  	|
  	*/

  	function sendMessage($mailer, $message, $to, $type) {

    $data = [];

    if ($type == 'sms') {
      $message = $message->setTo($to->smsGateway);
      $data["sendAddress"] = $to->smsGateway;
    } else {
      $message = $message->setTo($to->mmsGateway);
      $data["sendAddress"] = $to->mmsGateway;
    }

    // Send the message
	  $numSent = $mailer->send($message, $failures);
	  $data["successNumbers"][0] = $to->cellNumber;
    $data["failNumbers"][0] = [];
	  $data["numSent"] = 1;
	  $data["failures"] = 0;

		return $data;

	} // end sendMessage




 
    /*
    |----------------------------------------------------------------------------------------------
    | send swiftmailer message - $numSent - text messages
    |----------------------------------------------------------------------------------------------
    |
    */

    function sendTextQueueMessage($mailer, $message, $to) {

    $data = [];

    // Send the message
    $message = $message->setTo($to);
    $numSent = $mailer->send($message, $failures);
    
    $data["sendAddress"] = $to;
    $data["successNumbers"][0] = $to;
    $data["failNumbers"][0] = [];
    $data["numSent"] = 1;
    $data["failures"] = 0;

    return $data;

  } // end sendMessage

    /*
    |----------------------------------------------------------------------------------------------
    | send swiftmailer groupmessage - $numSent - text messages
    |----------------------------------------------------------------------------------------------
    |
    */

    function sendGroup($mailer, $message, $sendTo, $type) {

    $data = [];
    $tmp = [];
    $data["failures"] = 0;
    $data["failNumbers"] = [];
 //   $data["failures"] = 0;
    $failures = 0;
    $countFail = 0;
    $countSuccess = 0;

    for ($i=0; $i < count($sendTo); $i++) {
      // create send array
      $to = registeredNumbers::find($sendTo[$i]['cellNumber']);
      // if not wireless number
      if (is_null($to)) {
        $data['failures'] = $failures + 1;
        $data['failNumbers'][$countFail] = $sendTo[$i]['cellNumber'];
        $countFail = $countFail + 1;
        continue;
      } // end if
      if ($type == 'sms') {
        $tmp[$i] = $to->smsGateway;
      } else {
        $tmp[$i] = $to->mmsGateway;
      } // end if
     $data['successNumbers'][$countSuccess] = $sendTo[$i]['cellNumber'];
     $countSuccess = $countSuccess + 1;
    } // end foreach

    // Send the message
    $group = array_flatten($tmp);

    $message = $message->setTo($group);
    $numSent = $mailer->send($message, $failures);

    if ($numSent) {
      $data["numSent"] = $numSent;
      $data["failures"] = count($data["failNumbers"]);
      $data["group"] = $group;
    } else {
      $data["numSent"] = $numSent;
      $data["failures"] = count($data["failNumbers"]);
      $data["group"] = $group;
    }

    return $data;

  } // end sendGroup



    /*
    |----------------------------------------------------------------------------------------------
    | create swiftmailer mailer - $message
    |----------------------------------------------------------------------------------------------
    |
    */

    function createEmailMessage($subject, $from, $bounceAddress, $html, $text, $type) {

      if ($subject == 'no') {

        if ($type == 'html') {
          // html/text -  no subject
          $message = (new \Swift_Message())
            ->setReturnPath($bounceAddress)
            ->setFrom($from)
            ->setBody($html, 'text/html');
          if (is_null($text)) { $text = strip_tags($html); }
          $message->addPart($text, 'text/plain');
        } else {
          // text only - no subject
          if (is_null($text)) { $text = ''; }
          $message = (new \Swift_Message())
          ->setReturnPath($bounceAddress)
          ->setFrom($from)
            ->setBody($text, 'text/plain');
        } // end if html

      } else {

        if ($type == 'html') {
          // html/text -  subject
          $message = (new \Swift_Message())
          ->setReturnPath($bounceAddress)
          ->setSubject($subject)
            ->setFrom($from)
            ->setBody($html, 'text/html');
          if (is_null($text)) { $text = strip_tags($html); }
          $message->addPart($text, 'text/plain');
        } else {
          // text only - no subject
          if (is_null($text)) { $text = ''; }
          $message = (new \Swift_Message())
          ->setReturnPath($bounceAddress)
          ->setSubject($subject)
            ->setFrom($from)
            ->setBody($text, 'text/plain');
        } // end if html

      } // end if suject = no

    return $message;

    } // end createEmailMessage
         




    /*
    |----------------------------------------------------------------------------------------------
    | create swiftmailer mailer - $message - send restaurant order message
    |----------------------------------------------------------------------------------------------
    |
    */

    function createOrderMessage($subject, $from, $html, $text, $type) {

      if ($subject == 'no') {

        if ($type == 'html') {
          // html/text -  no subject
          $message = (new \Swift_Message())
            ->setFrom($from)
            ->setBody($html, 'text/html');
          if (is_null($text)) { $text = strip_tags($html); }
          $message->addPart($text, 'text/plain');
        } else {
          // text only - no subject
          if (is_null($text)) { $text = ''; }
          $message = (new \Swift_Message())
            ->setFrom($from)
            ->setBody($text, 'text/plain');
        } // end if html

      } else {

        if ($type == 'html') {
          // html/text -  no subject
          $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($from)
            ->setBody($html, 'text/html');
          if (is_null($text)) { $text = strip_tags($html); }
          $message->addPart($text, 'text/plain');
        } else {
          // text only - no subject
          if (is_null($text)) { $text = ''; }
          $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($from)
            ->setBody($text, 'text/plain');
        } // end if html

      } // end if subject = no

    return $message;

    } // end createEmailMessage 



    /*
    |----------------------------------------------------------------------------------------------
    | send message to hp ePrint service
    |----------------------------------------------------------------------------------------------
    |
    */

    function createPDFMessage($subject, $from, $pdfPath) {  
      
      $text = '';
      
      $message = (new \Swift_Message())
        ->setSubject($subject)
        ->setFrom($from)
        ->setBody($text, 'text/plain');

      $message->attach(\Swift_Attachment::fromPath($pdfPath));

      return $message;

    } // end createEmailMessage 





    /*
    |----------------------------------------------------------------------------------------------
    | send swiftmailer message - $numSent - email messages
    |----------------------------------------------------------------------------------------------
    |
    */

    function sendEmailMessage($mailer, $message, $to, $type) {

    $data = [];
    $message = $message->setTo([$to['email'] => $to['name']]);
    $data["sendAddress"] = $to['email'];
    
    // Send the message
    $numSent = $mailer->send($message, $failures);
    $data["successNumbers"][0] = $to['email'];
    $data["failNumbers"][0] = [];
    $data["numSent"] = 1;
    $data["failures"] = 0;

    return $data;

  } // end sendMessage




  /*
  |----------------------------------------------------------------------------------------------
  | send swiftmailer group email message
  |----------------------------------------------------------------------------------------------
  |
  */

  function sendEmailGroup($mailer, $message, $sendTo, $type) {

    $data = [];
    $tmp = [];
    $data["failures"] = 0;

    $data["failNumbers"] = [];
    //   $data["failures"] = 0;
    $failures = 0;
    $countFail = 0;
    $countSuccess = 0;

    for ($i=0; $i < count($sendTo); $i++) {
      $tmp[$i] = $sendTo[$i]['email'];
    } // end foreach

    // Send the message
    $group = array_flatten($tmp);

    $message = $message->setTo($group);
    $numSent = $mailer->send($message, $failures);

    if ($numSent) {
      $data["numSent"] = $numSent;
      $data["failures"] = count($data["failNumbers"]);
      $data["group"] = $group;
    } else {
      $data["numSent"] = $numSent;
      $data["failures"] = count($data["failNumbers"]);
      $data["group"] = $group;
    }

    return $data;

  } // end sendGroup



  /*
  |----------------------------------------------------------------------------------------------
  | Send Order Text Message
  |----------------------------------------------------------------------------------------------
  |
  */

  function sendOrderText($contactNumberOnly, $domainSiteConfig, $delivery, $orderInProcess = 1, $orderReadyTime = '') {

    $subject = $domainSiteConfig->locationName . " Online Order";
    
    $contactNumber = registeredNumbers::find($contactNumberOnly)->smsGateway;
    if (is_null($contactNumber)) { return 0; } // no text sent - no valid cell number provided

    $puTime = Carbon::parse($orderReadyTime);
    $futurePickupMessage = '';
    $futureDeliveryMessage = '';
		if ($orderInProcess == 9 ) {
			// future Order
			if ($delivery == "pickup") {
				// pickup
				$futurePickupMessage = str_replace("[[pickupTime]]", $puTime->format('l F jS Y \a\t g:i A'), $domainSiteConfig->futureOrderMessage);
			} else {
				// delivery
				$futureDeliveryMessage = str_replace("[[deliveryTime]]", $puTime->format('l F jS Y \a\t g:i A'), $domainSiteConfig->futureDeliveryMessage);
			}
		}
			
    // Create the Transport using HWCTMailer
    $transport = $this->createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

    // Create the Mailer using your created Transport
    $mailer = $this->createMailer($transport);

    // set order message
    if ( $delivery == 'pickup' && $orderInProcess == 9 ) {
      $orderMessage = $futurePickupMessage;
    } elseif ( $delivery == 'deliver' && $orderInProcess == 9 ) {
      $orderMessage = $futureDeliveryMessage;
    } elseif ( $delivery == 'pickup' && $orderInProcess == 8 ) {
      $orderMessage = $domainSiteConfig->pickupOIP;
    } elseif ( $delivery == 'deliver' && $orderInProcess == 8 ) {
      $orderMessage = $domainSiteConfig->deliverOIP;
    } elseif ( $delivery == 'pickup' ) {
        $orderMessage = $domainSiteConfig->orderMessage;
    } elseif ( $delivery == 'deliver' ) {
        $orderMessage = $domainSiteConfig->deliveryMessage;
    } else {
      $orderMessage = "Thank you for placing the order.";
    }

    // Create a message, $subject, $from, $message
    $message = $this->createOrderTextMessage($subject, $domainSiteConfig->sendEmail, $orderMessage);

     // Send the message
     $message = $message->setTo($contactNumber);
     $numSent = $mailer->send($message);

    return $numSent;

    } // end sendOrderText




    /*
    |----------------------------------------------------------------------------------------------
    | Create order text message
    |----------------------------------------------------------------------------------------------
    |
    */

    function createOrderTextMessage($subject, $from, $text) {  
      
      $message = (new \Swift_Message())
        ->setSubject($subject)
        ->setFrom($from)
        ->setBody($text, 'text/plain');

      return $message;

    } // end createOrderTextMessage 


  /*
  |----------------------------------------------------------------------------------------------
  | Bounce Message Header Information
  |----------------------------------------------------------------------------------------------
  |
  */

  function setBounceHeaderInfo($message, $domainID, $userID, $contactID, $messageID, $mailtext) {

    // add domainID.userID.contactID.MessageID - for bounce tracking
    // contactID = 0 -> messageID = test ==> test email
    // ORDER_SERVERID = id of server - needed to determine domain server
    $newHeader = env('ORDERS_SERVER_ID') . "." . $domainID . "." . $userID . "." . $contactID . "." . $messageID . "." . $mailtext;
    $headers = $message->getHeaders();
    $headers->addTextHeader(env('ORDERS_BOUNCE_HEADER'), $newHeader);

    return $message;

  } // setBounceHeaderInfo


} // end HWCTMailer