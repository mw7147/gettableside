<?php

namespace App\hwct;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\menuOptionsData;
use App\menuAddOnsData;
use App\menuSidesData;
use App\ordersDetail;
use App\siteStyles;
use Carbon\Carbon;
use App\cartData;
use App\contacts;
use App\orders;
use App\domain;

use Dompdf\Dompdf;

class MultipleSites {

// helper functions for locations with multiple sites

/*----------------  Location Names  ----------------------------------*/
// return array of customer (domain) ID's and location names for owners role

	public function locationNames($userID, $ownerID, $domainID, $parentDomainID) {
		// get location names array - returns domainID, domain name
		if ($domainID == $parentDomainID && $userID == $ownerID) {
			// parent domain or single location
			$data = domain::select('id', 'name')->where('parentDomainID', '=', $parentDomainID)->get();
		} else {
			// single location of parent
			$data = domain::select('id', 'name')->where('id', '=', $domainID)->get();
		}
		
	    return $data;

	} // end locationNames




    /*-------------------- Get Domains  ----------------------------------------*/
    public function getDomains($domainID, $authLevel) {
		
		if ($authLevel >= 40) {
			$domains = domain::select('id', 'name')->orderBy('name')->get();
		} elseif ($authLevel >= 35 && $authLevel < 40) {
			$domains = domain::select('id', 'name')->where('parentDomainID', '=', $domainID)->orderBy('name')->get();
		} else {
			$domains = domain::select('id', 'name')->where('id', '=', $domainID)->orderBy('name')->get();
		}
  
		return $domains;
	} // end getDomains
  


} // end Class MultipleSites


