<?php


namespace App\hwct;

use Illuminate\Support\Facades\Log;

use App\registeredNumbers;
use Carbon\Carbon;


class CellNumberData {



	function cellData($phoneNumber) {

		if ($phoneNumber == '') {
			$data = array("status" => "error", "message" =>"no number");
			return $data;
		}

		// strip any non-numeric characters
		$phoneNumber = $this->numbersOnly($phoneNumber);

		// check if in registered number database
		$numCheck = registeredNumbers::where('cellNumber', '=', $phoneNumber)->first();
		if ( is_null($numCheck) ) {
			// not in registeredNumbers table - get data and store in registeredNumbers
			$cellData = $this->getData($phoneNumber, 'new');
			return $cellData;
		}

		// found in registeredNumbers
		$updateDays = $this->daysSinceUpdate($numCheck->updated_at);
		
		if ($updateDays > env('REGISTERED_NUMBERS_UPDATE_PERIOD')) {

			// update registeredNumbers data
			$cellData = $this->getData($phoneNumber, 'existing');
			return $cellData;
		}

		// format Number
        $fn = $this->formatUSPhone($phoneNumber);

		// within update period - return data array from registeredNumbers
		$cellData = array("status" => "success", "message" => $numCheck->carrierName, "cellNumber" => $phoneNumber, "formattedNumber" => $fn);

		return $cellData;

	}

	/*
	|----------------------------------------------------------------------------------------------
	| get Cell Number Data from data24-7.com API
	|----------------------------------------------------------------------------------------------
	|
	*/

	function getData($phoneNumber, $status) {

		// Build the URL - API Version 3
		// https://api.data247.com/v3.0?key={my_api_key}&api=MT&phone={phone_number}&addfields=cost
       	$url = "https://api.data247.com/v3.0?key=" . env('REGISTERED_NUMBERS_API_KEY') . "&api=MT&phone=" . $phoneNumber . "&addfields=cost";
 
		$result = file_get_contents($url) or die("feed not loading");
		
		// check response
		$data = json_decode($result);
		if ( $data->response->status != 'OK' || $data->response->results[0]->wless != 'y'){

            // if not a wireless number - bailout and return error
            $data = array("status" => "error", "message" =>"non-wireless", "cellNumber" => $phoneNumber);
            if ($status = 'existing') {
            	// remove from registeredNumbers table (very unlikely but just in case)
            	registeredNumbers::where('cellNumber', '=', $phoneNumber)->delete();
            }
            return $data;

        } 

        // format Number
        $fn = $this->formatUSPhone($phoneNumber);
        // update or create record
        $num = registeredNumbers::updateOrCreate([
        	// match field(s)
        	'cellNumber' => $this->numbersOnly($phoneNumber),
        ],[
        	// new record or update data
    	    "carrierID" => trim($data->response->results[0]->carrier_id),
            "carrierName" => trim($data->response->results[0]->carrier_name),
            "smsGateway" => trim($data->response->results[0]->sms_address),
            "mmsGateway" => trim($data->response->results[0]->mms_address),
            "cost" => trim($data->response->results[0]->cost),
            "formattedNumber" => $fn
        ]);


        $data = array("status" => "success", "message" => strip_tags($num->carrierName), "cellNumber" => $phoneNumber, "formattedNumber" => $fn);
        
        return $data;

    }  // end getData



	/*
	|----------------------------------------------------------------------------------------------
	| Numbers Only - return the numbers only from a phone number string
	|----------------------------------------------------------------------------------------------
	|
	*/

	function numbersOnly($phoneString) {

		$num = preg_replace("/[^0-9]/","", $phoneString);
		return $num;

	} // end numbersOnly



	/*
	|----------------------------------------------------------------------------------------------
	| formatted Number - return the US format phone number from 10 digit phone number string
	|----------------------------------------------------------------------------------------------
	|
	*/

	function formatUSPhone($phoneString) {

		$num = preg_match("/(\d{3})(\d{3})(\d{4})/", $phoneString, $matches);
		$formatNumber = "(" . $matches[1] . ") " . $matches[2] . "-" . $matches[3];
		return $formatNumber;

	} // end numbersOnly



	/*
	|----------------------------------------------------------------------------------------------
	| Number of Days From Last Update - returns the numbers days from last registered number update
	|----------------------------------------------------------------------------------------------
	|
	*/

	function daysSinceUpdate($lastUpdate) {

	    $dt1 = Carbon::parse($lastUpdate);
	    $dt2 = Carbon::now();
	    $diff = $dt2->diffInDays($dt1);
	    
	    return $diff;

	} // end daysSinceUpdate



} // end Class CellNumberData