<?php

namespace App\hwct;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use App\foodRunner;
use Carbon\Carbon;
use App\domain;
use Config;



class foodRunnerDelivery {

	// helper functions for FoodRunner Delivery Service


	/*
	request
	"display_order_id": "1331312321",
	"estimated_ready": "2020-05-12 19:00",
	"pickup_restaurant_name": "My Best Restaurant",
	"pickup_address": "6707 Teal Court, Lino Lakes, MN 55038",
	"deliver_name": "Jereme Allen",
	"deliver_address": "595 Chippewa Trail, Lino Lakes, MN 55036",
	"timezone": "America/Chicago"
	*/

	/*---------------- schedule foodRunner  ----------------------------------*/

	function schedule( $domainSiteConfig, $order, $mode ) {

		$response = Http::withToken(config('services.foodRunner')['Bearer'])->post($this->foodRunnerAPIURL($mode), [
			"display_order_id" => $order->id,
			"estimated_ready" => Carbon::parse($order->orderReadyTime)->format('Y-m-d H:i'),
			"pickup_restaurant_name" => $domainSiteConfig->locationName,
			"pickup_address" => $this->pickupAddress($domainSiteConfig),
			"deliver_name" => $order->fname . " " . $order->lname,
			"deliver_address" => $this->deliveryAddress($order),
			"timezone" => $order->timezone,
		]);

		$res = $response->json();

		$data = new foodRunner;
			$data->domainID = $order->domainID;
			$data->foodRunnerID = $res['id'];
			$data->displayOrderID = $order->id;
			$data->estimatedReady = $res['estimated_ready'];
			$data->pickupRestaurantName = $res['pickup_restaurant_name'];
			$data->pickupAddress = $res['pickup_address'];
			$data->deliverName = $res['deliver_name'];
			$data->deliverAddress = $res['deliver_address'];
			$data->statusID = $res['status_id'];
			$data->createdAtUTC = $res['created_at'];
			$data->timezone = $order->timezone;
			$data->distance = $order->deliveryFeet;
		$d = $data->save();

		return $d;

    
    } // end schedule


	/*---------------- Live or Test Delivery URL   ----------------------------------*/

	public function foodRunnerAPIURL($mode) {
		// get url for foodRunner
		
		if ( $mode == "live") {
			return config('services.foodRunner')['orderLiveURL'];
		} else {
			return config('services.foodRunner')['orderTestURL'];
		}

	} // end foodRunnerAPIURL



	/*---------------- pickupAddress   ----------------------------------*/

	public function pickupAddress($domainSiteConfig) {
		
		if (is_null($domainSiteConfig->address2)) {
			$pickupAddress = $domainSiteConfig->address1 . ", " . $domainSiteConfig->city . ", " . $domainSiteConfig->state . " " . $domainSiteConfig->zipCode;
		} else {
			$pickupAddress = $domainSiteConfig->address1 . " " . $domainSiteConfig->address2 . ", " . $domainSiteConfig->city . ", " . $domainSiteConfig->state . " " . $domainSiteConfig->zipCode;
		}

		return $pickupAddress;

	} // end pickupAddress



	/*---------------- deliveryAddress   ----------------------------------*/

	public function deliveryAddress($order) {
		
		if (is_null($order->address2)) {
			$deliveryAddress = $order->address1 . ", " . $order->city . ", " . $order->state . " " . $order->zipCode;
		} else {
			$deliveryAddress = $order->address1 . " " . $order->address2 . ", " . $order->city . ", " . $order->state . " " . $order->zipCode;
		}

		return $deliveryAddress;

	} // end deliveryAddress



} // end foodRunner

