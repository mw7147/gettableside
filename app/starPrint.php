<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class starPrint extends Model
{
    protected $connection = 'starPrint';
    protected $table = 'printers';
}
