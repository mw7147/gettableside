<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suspendOperationHistory extends Model
{
    protected $table = 'suspendOperationHistory';
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'userID');
    }
}
