<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registeredNumbers extends Model
{
    protected $table = 'registeredNumbers';
    protected $primaryKey = 'cellNumber';
    protected $guarded = [];
}
