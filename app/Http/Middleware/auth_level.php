<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use App\usersData;
use App\roles;
use App\User;
use Closure;

class auth_level
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $authLevel)
    {
        $user = Auth::user();
        $domain = \Request::get('domain');

        if (is_null($user)) {return redirect('/login');} // no authorized user
        // allow for parent domain or domain
/*
        if ($user->role_auth_level < 20 ) {
            if (!($domain->id != $user->domainID || $domain->id != $user->parentDomainID)) { Auth::logout(); session()->regenerate(); abort(403); } // not authorized on this domain
        } else {
            if ( $domain->id != $user->domainID || $domain->id != $user->parentDomainID ) { Auth::logout(); session()->regenerate(); abort(403); } // not authorized on this domain
        }
        
        if ($user->role_auth_level < $authLevel || $user->active != 'yes') {Auth::logout(); session()->regenerate(); abort(403);} // not authorized or not active
*/
        $urlPrefix = roles::select('urlPrefix')->where('auth_level', '=', $user->role_auth_level)->first();
        $request->attributes->add(['urlPrefix' => $urlPrefix->urlPrefix]);
        $request->attributes->add(['authLevel' => $user->role_auth_level]);
        return $next($request);
        
    }
}