<?php
/*

Copyright 2016 Riverhouse IT

set $domain variable for all pages
all items from db table domain

*/


namespace App\Http\Middleware;

use Closure;

use \App\domain; // model for db table domains

class GetDomainId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // get domain
        $server = $_SERVER['HTTP_HOST']; 
        // get url
        $domain = domain::where([['httpHost', '=', $server], ['active', '=', 'yes']])->first();
        // active domain not found
        if (is_null($domain)) {abort(404);} 

       // $domain = $domain[0]; // data part
        date_default_timezone_set($domain->domainSiteConfig->timezone);
        $request->attributes->add(['domain' => $domain]);

        return $next($request);

    }
}
