<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Facades\App\hwct\SecureCard;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use App\deliverySession;
use App\authNetDetail;
use App\ordersDetail;
use App\managerPIN;
use App\siteStyles;
use Carbon\Carbon;
use App\cardData;
use App\cartData;
use App\usStates;
use App\contacts;
use App\orders;
use App\domain;
use Config;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class authNetController extends Controller
{

/*---------------- POST - viewcart submit payment  ----------------------------------*/

	public function orderPayment(Request $request) {
        // process payment - Authorize.net - http://developer.authorize.net
	//	dd($request);
        // check sessionID - make sure order does not exist with same sessionID
		$orderCheck = orders::where('sessionID', '=', $request->sid)->first();
		if (!empty($orderCheck)) { 
			session()->flash('cardError', 'An order has already been processed for this session. Please reset your cart and try again.');
			return redirect()->action('cartController@viewCart');
        }
        
        $userID = Auth::id();
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain['id'])->first();
		$styles = siteStyles::find($domain['id']);
        $numOnly = CellNumberData::numbersOnly($request->mobile);
        $ipAddress = \Request::ip();
		$customerTransactionID = $this->getMicrotimeString();
		$splitName = explode(' ',$request->name, 2);
		$ccNumber = preg_replace('[\D]', '', $request->number); // remove non numbers from credit card number
		
		if (isset($request->delivery)) { $delivery = $request->delivery; } else { $delivery = 0; }

		// update or create contact based on email and domainID
		$contact = contacts::updateOrCreate([
			'email' => $request->email,
			'domainID' => $domain['id']
			], [
			'fname' => $request->fname,
			'lname' => $request->lname,
			'userID' => $request->userID,
			'mobile' => $request->mobile,
			'unformattedMobile' => $numOnly,
			'address1' => $request->address1,
			'address2' => $request->address2,
			'city' => $request->city,
			'state' => $request->State,
			'zipCode' => $request->zipCode
        ]);

			// $request is reset upon validation - save non-contact request to variables
			if (isset($request->number)) { $number = $request->number; } else { $number = ''; }

			if (isset($request->name)) { $name = $request->name; } else { $name = ''; }
			if (isset($request->expiry)) { $expiry = $request->expiry; } else { $expiry = ''; }
			if (isset($request->cvc)) { $cvc = $request->cvc; } else { $cvc = ''; }
			if (isset($request->sid)) { $sid = $request->sid; } else { $sid = ''; }
			if (isset($request->did)) { $did = $request->did; } else { $did = ''; }
			if (isset($request->ownerID)) { $ownerID = $request->ownerID; } else { $ownerID = ''; }
			if (isset($request->subTotal)) { $subTotal = $request->subTotal; } else { $subTotal = ''; }
			if (isset($request->userID)) { $userID = $request->userID; } else { $userID = ''; }
			if (isset($request->tax)) { $tax = $request->tax; } else { $tax = ''; }
			if (isset($request->tip)) { $tip = $request->tip; } else { $tip = ''; }
			if (isset($request->total)) { $total = $request->total; } else { $total = ''; }
			if (isset($request->userAgent)) { $userAgent = $request->userAgent; } else { $userAgent = ''; }
			if (isset($request->ccType)) { $ccType = $request->ccType; } else { $ccType = ''; }
			if (isset($request->placeOrder)) { $placeOrder = $request->placeOrder; } else { $placeOrder = ''; }
			if (isset($request->save)) { $save = $request->save; } else { $save = ''; }
			if (isset($request->formDelivery)) { $formDelivery = $request->formDelivery; } else { $formDelivery = 'pickup'; }
			if (is_null($domainSiteConfig->deliveryCharge)) { $deliveryCharge = 0; } else { $deliveryCharge = $domainSiteConfig->deliveryCharge; }



        
        
		// Begin Authorize.net API	
		
		if ($domainPaymentConfig->mode == 'live') {

			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName($domainPaymentConfig->authNetLiveUser);
				$merchantAuthentication->setTransactionKey($domainPaymentConfig->authNetLivePass);
				
		} else {

			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName($domainPaymentConfig->authNetTestUser);
				$merchantAuthentication->setTransactionKey($domainPaymentConfig->authNetTestPass);
				
		}
	
		// Set the transaction's refId
		$refId = $customerTransactionID;

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
			$creditCard->setCardNumber($ccNumber);
			$creditCard->setExpirationDate($request->expiry);
			$creditCard->setCardCode($request->cvc);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
			$paymentOne->setCreditCard($creditCard);

		// Create order informationmi
		$order = new AnetAPI\OrderType();
			$order->setInvoiceNumber($customerTransactionID);
			$order->setDescription($domainSiteConfig->locationName . " ToGo Order");

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
			$customerAddress->setFirstName($splitName[0]);
			$customerAddress->setLastName($splitName[1]);

		// Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
			$customerData->setType("individual");
			$customerData->setId($numOnly);
			$customerData->setEmail($request->email);

		// Add values for transaction settings
		$duplicateWindowSetting = new AnetAPI\SettingType();
			$duplicateWindowSetting->setSettingName("duplicateWindow");
			$duplicateWindowSetting->setSettingValue("60");
		
		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
			$transactionRequestType->setTransactionType("authCaptureTransaction");
			$transactionRequestType->setAmount($total);
			$transactionRequestType->setOrder($order);
			$transactionRequestType->setPayment($paymentOne);
			$transactionRequestType->setBillTo($customerAddress);
			$transactionRequestType->setCustomer($customerData);
			$transactionRequestType->addToTransactionSettings($duplicateWindowSetting);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
			$request->setMerchantAuthentication($merchantAuthentication);
			$request->setRefId($refId);
			$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response - validate the transaction
		$controller = new AnetController\CreateTransactionController($request);
			if ($domainPaymentConfig->mode == 'live') {
				$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);				
			} else {
				$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
			}
		
		// end Authorize.net API

        // check response from Authorize.Net
		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == 'Ok') {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();
		
				if ($tresponse != null && $tresponse->getMessages() != null) {
					$transId = $tresponse->getTransId();
					$responseCode = $tresponse->getResponseCode();
					$messageCode = $tresponse->getMessages()[0]->getCode();
					$authCode = $tresponse->getAuthCode();
					$messageDescription = $tresponse->getMessages();
					$errorCode = 'none';
				} else {
					//Transaction Failed
					if ($tresponse->getErrors() != null) {
						$errorCode = $tresponse->getErrors()[0]->getErrorCode();
						$errorMessage = $tresponse->getErrors()[0]->getErrorText();
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				
				$tresponse = $response->getTransactionResponse();
		
				if ($tresponse != null && $tresponse->getErrors() != null) {
						$errorCode = $tresponse->getErrors()[0]->getErrorCode();
						$errorMessage = $tresponse->getErrors()[0]->getErrorText();
				} else {
				
						$errorCode = $response->getMessages()->getMessage()[0]->getCode();
						$errorMessage = $response->getMessages()->getMessage()[0]->getText();

				}
			}
		} else {
			$errorCode = "1000";
			$errorMessage = "Card Processor Not Responding";
		} // end check response from Authnet


	if ($errorCode != 'none') {
	
		// Charge Declined or Error Processing - return to cart - display message
		session()->flash('cardError', 'Charge Declined - ' . $errorCode . ' - ' . $errorMessage);
		Log::info('Card Declined - ' . $contact->fname . ' ' . $contact->lname . ' - contact ID: ' . $contact->id . ' - ErrorMessage: ' . $errorCode . ' - ' .  $errorMessage);
		return redirect()->action('cartController@viewCart');
	
	} // end if errorcode

		$lastFour = substr($number, -4);
		
		// encrypt and save card 
        if ($save == "yes" && $userID != null) {
            // save card data for userID if customer desires
            if ($domainPaymentConfig->saveCard == 'yes') {
            	$cc = cardData::firstOrNew(['domainID' => $domain->id, 'userID' => $userID, 'lastFour' => $lastFour]);
					$cc->ccType = $ccType;
					$cc->ccNumber = SecureCard::encryptNumber($number);
					$cc->ccName = SecureCard::encryptData($name);
					$cc->ccExpiry = SecureCard::encryptData($expiry);
				$cc->save();
            } // end if saveCard
        } // end if save
        


        // charge approved - create transaction and order

		// create new order
		$order = new orders;
			$order->domainID = $domain['id'];
			$order->ownerID = $domain['ownerID'];
			$order->contactID = $contact->id;
			$order->userID = $contact->userID;
			$order->sessionID = $sid;
			$order->fname = $contact->fname;
			$order->lname = $contact->lname;
			$order->unformattedMobile = $contact->unformattedMobile;
            $order->customerEmailAddress = $contact->email;
			$order->customerMobile = $contact->mobile;
			$order->subTotal = $subTotal;
			$order->tax = $tax;
			$order->tip = $tip;
			$order->deliveryCharge = $deliveryCharge; // holder for delivery charge
			$order->total = $total;
			$order->orderType = $formDelivery;
            $order->ccType = $domainPaymentConfig->ccType;
			$order->ccTypeTransactionID = $transId; // authnet transId
            $order->ccTypeCustomerID = 'unknown'; // card processor stored customer ID
            $order->ccTypeCardID = 'unknown'; // stored card ID with card processor
            $order->ccTypeFunding = $responseCode; // responseCode from authnet
            $order->ccTypeLast4 = $lastFour;
            $order->ccTypeTransactionFee = 0;
            $order->paymentType = $ccType;
            $order->waiterID = 0; // add waiters
            $order->tableID = 0; // add tables
            $order->userAgent = $userAgent;
            $order->orderIP = $ipAddress;
            $order->restaurantEmailAddress = $domainSiteConfig->sendEmail;
            $order->orderInProcess = 1; // set order in process
		$order->save();

		$details = cartData::where('sessionID', '=', $sid)->get();
		foreach ($details as $detail) {
			$od = new ordersDetail;
				$od->domainID = $detail->domainID;
				$od->ownerID = $detail->ownerID;
				$od->contactID = $contact->id;
				$od->sessionID = $detail->sessionID;
				$od->menuDataID = $detail->menuDataID;
				$od->menuItem = $detail->menuItem;
				$od->menuDescription = $detail->menuDescription;
				$od->portion = $detail->portion;
				$od->printOrder = $detail->printOrder;
				$od->printReceipt = $detail->printReceipt;
				$od->price = $detail->price;
				$od->menuOptionsDataID = $detail->menuOptionsDataID;
				$od->menuAddOnsDataID = $detail->menuAddOnsDataID;
				$od->menuSidesDataID = $detail->menuSidesDataID;
				$od->instructions = $detail->instructions;
				$od->orderIP = $detail->orderIP;
				$od->tipAmount = $detail->tipAmount;
				$od->discountPercent = $request->orderDiscountPercent ?? 0;


			$od->save();
			cartData::destroy($detail->id);
		} // end foreach      
            
		// save authnet repsonse detail
		$ad = new authNetDetail;
		    $ad->domainID = $domain['id'];
		    $ad->ownerID = $domain['ownerID'];
            $ad->contactID = $contact->id;
            $ad->sessionID = $sid;
            $ad->orderID = $order->id;
            $ad->transactionID = $transId;
            $ad->ccLastFour = $lastFour;
            $ad->ccType = $ccType;
            $ad->expiry = $expiry;
            $ad->responseCode = $responseCode;
            $ad->messageCode = $messageCode;
            $ad->authCode = $authCode;
            $ad->amount = $total;
        $ad->save();

		//create PDF - create pdf and return pdf file path
		$pdf = ProcessOrders::createPDF($contact, $order, $domainSiteConfig);

		// send customer email
		$sendMail = ProcessOrders::sendOrderEmail($contact, $order, $domainSiteConfig);

		$order->emailData = $sendMail['html'];
		$order->pdfPath = $pdf;
		$order->save();	

		// send to hp ePrint service if subscribed
		if (!empty($domainSiteConfig->hpEprint)) { $sendPDF = ProcessOrders::sendPDF($contact, $order, $domainSiteConfig); }

		session()->regenerate();
	    $message['orderAlert'] = $domainSiteConfig->orderAlert;
	    $message['orderID'] = $order->sessionID;
	    $message['numSent'] = $sendMail['numSent'];
		
	    return view('orders.viewOrder')->with([ 'html' => $order->emailData ]);



    } // end orderPayment







	/*---------------- microtime string - transaction id  ----------------------------------*/


	function getMicrotimeString() {
		//Get raw microtime (with spaces and dots and digits)
		$mt = microtime();
	
		//Remove all non-digit (or non-integer) characters
		$r = "";
		$length = strlen($mt);
		for($i = 0; $i < $length; $i++) {
			if(ctype_digit($mt[$i])) {
				$r .= $mt[$i];
			}
		}
	
		//Return
		return $r;
	} // end getMicrotimeString
	
	
	/*---------------- strip all chanracters - numbers only string  ----------------------------------*/


	function numOnlyString($string) {
	
		//Remove all non-digit (or non-integer) characters
		$r = "";
		$length = strlen($string);
		for($i = 0; $i < $length; $i++) {
			if(ctype_digit($string[$i])) {
				$r .= $string[$i];
			}
		}
	
		//Return
		return $r;
	} // end getMicrotimeString




/*---------------- split name on credit card  ----------------------------------*/


	function splitName($name) {
		
		$splitName = explode(' ',$name, 2);
		return $splitName;
	} // end getMicrotimeString





/*---------------- Refund Transaction Post Batch ----------------------------------*/

	function refundTransactionPostBatch(Request $request) {
		/* Create a merchantAuthenticationType object with authentication details
		   retrieved from the constants file */
		
		$order = orders::find($request->orderID);
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $order->domainID)->first();
		
		// manager PIN check
		$chk = managerPIN::where([['domainID', '=', $order->domainID], ['managerPIN', '=', $request->managerPIN]])->first();
		if (is_null($chk)) {
			$data=[];
			$data['status'] = 'error';
			$data['message'] = 'The Manager PIN was not correct. Please enter the correct PIN.';
			return $data;
		}
		
		$authNetDetail = authNetDetail::where('orderID', '=', $order->id)->first();
		$maskedExpire = $this->numOnlyString($authNetDetail->expiry);
		
		// Begin Authorize.net API	
		
		if ($domainPaymentConfig->mode == 'live') {

			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName($domainPaymentConfig->authNetLiveUser);
				$merchantAuthentication->setTransactionKey($domainPaymentConfig->authNetLivePass);
				
		} else {

			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName($domainPaymentConfig->authNetTestUser);
				$merchantAuthentication->setTransactionKey($domainPaymentConfig->authNetTestPass);
				
		}

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
			// $creditCard->setCardNumber($order->lastFour);
			$creditCard->setCardNumber('4111111111111111');
			$creditCard->setExpirationDate($maskedExpire);
	
		$paymentOne = new AnetAPI\PaymentType();
			$paymentOne->setCreditCard($creditCard);
		//create a transaction
		$transactionRequest = new AnetAPI\TransactionRequestType();
			$transactionRequest->setTransactionType( "refundTransaction");
			$transactionRequest->setAmount($order->total);
			$transactionRequest->setPayment($paymentOne);
			$transactionRequest->setRefTransId($order->ccTypeTransactionID);
 

		$request = new AnetAPI\CreateTransactionRequest();
			$request->setMerchantAuthentication($merchantAuthentication);
			$request->setRefId($order->ccTypeTransactionID);
			$request->setTransactionRequest( $transactionRequest);
	
	
	
		$controller = new AnetController\CreateTransactionController($request);
		if ($domainPaymentConfig->mode == 'live') {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		if ($response != null)
		{
		  if($response->getMessages()->getResultCode() == 'Ok')
		  {
			$tresponse = $response->getTransactionResponse();
		
			  if ($tresponse != null && $tresponse->getMessages() != null)   
			{
			  echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
			  echo "Refund SUCCESS: " . $tresponse->getTransId() . "\n";
			  echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
				echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
			}
			else
			{
			  echo "Transaction Failed \n";
			  if($tresponse->getErrors() != null)
			  {
				echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
				echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";            
			  }
			}
		  }
		  else
		  {
			echo "Transaction Failed \n";
			$tresponse = $response->getTransactionResponse();
			if($tresponse != null && $tresponse->getErrors() != null)
			{
			  echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
			  echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";                      
			}
			else
			{
			  echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
			  echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
			}
		  }      
		}
		else
		{
		  echo  "No response returned \n";
		}

		return $response;
	  } // end refundTransaction




function refundTransaction(Request $request) {

    /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
       
		$order = orders::find($request->orderID);
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $order->domainID)->first();
		
		// manager PIN check
		$chk = managerPIN::where([['domainID', '=', $order->domainID], ['managerPIN', '=', $request->managerPIN]])->first();
		if (is_null($chk)) {
			$data=[];
			$data['status'] = 'error';
			$data['message'] = 'The Manager PIN was not correct. Please enter the correct PIN.';
			return $data;
		}
		
		$authNetDetail = authNetDetail::where('orderID', '=', $order->id)->first();
		$maskedExpire = $this->numOnlyString($authNetDetail->expiry);
		
		// Begin Authorize.net API	
		
		if ($domainPaymentConfig->mode == 'live') {

			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName($domainPaymentConfig->authNetLiveUser);
				$merchantAuthentication->setTransactionKey($domainPaymentConfig->authNetLivePass);
				
		} else {

			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
				$merchantAuthentication->setName($domainPaymentConfig->authNetTestUser);
				$merchantAuthentication->setTransactionKey($domainPaymentConfig->authNetTestPass);
				
		}
    
    // Set the transaction's refId
    $refId = 'ref' . time();

    //create a transaction
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    	$transactionRequestType->setTransactionType( "voidTransaction"); 
    	$transactionRequestType->setRefTransId($order->ccTypeTransactionID);

    $request = new AnetAPI\CreateTransactionRequest();
    	$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($order->ccTypeTransactionID);
    	$request->setTransactionRequest( $transactionRequestType);
    
    $controller = new AnetController\CreateTransactionController($request);   
    if ($domainPaymentConfig->mode == 'live') {
		$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
	} else {
		$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
	}
        
    if ($response != null) {
    	
    	$check = $response->getMessages()->getResultCode();
    	
      	if( $check == 'Ok') {
      
      		// success
        	$tresponse = $response->getTransactionResponse();
    		$transID = $tresponse->getTransId();
    		$status = "success";
			$message = "The refund was successfully processed.";
        
        } else {
          
          // Transaction Failed 
            $errorCode = $tresponse->getErrors()[0]->getErrorCode();
            $errorText = $tresponse->getErrors()[0]->getErrorText();
            $status = 'error';
			$message = 'There was an error refunding the charge. Please try again or check with Authorize.net.';
    	}
    
    	if ($status == "success") {
			$order->orderRefunded = 1;
			$order->orderInProcess = 0;
			$order->orderRefundID = $transID;
			$order->orderRefundAmount = $order->total;
			$order->save();
		}

		$data=[];
		$data['status'] = $status;
		$data['message'] = $message;
		$data['authnet'] = $transID;
	}
		
	return $data;

  } // end voidTransaction








} // end authNetController
