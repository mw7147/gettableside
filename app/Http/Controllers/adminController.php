<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Facades\App\hwct\PrintOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use Illuminate\Support\Str;
use App\domainSiteConfig;
use App\registeredNumbers;
use App\sendEmailAdmin;
use App\Http\Requests;
use App\mktMyInformation;
use App\managerPIN;
use App\mktCustomers;
use App\mktDomains;
use App\mktUsers;
use App\siteStyles;
use Carbon\Carbon;
use App\usersData;
use App\marketing;
use App\printers;
use App\usStates;
use App\contacts;
use App\Billing;
use App\orders;
use App\ccType;
use App\domain;
use App\roles;
use Validator;
use App\User;
use DateTime;
use App\type;



class adminController extends Controller
{
    
/*-------------------- Admin Dashboard  ----------------------------------------*/

	public function dashboard(Request $request) {

    $dt = Carbon::now();

    $ordersToday = orders::select('id')->whereDate('orderReadyTime', $dt->toDateString())->count();
    $revToday = orders::select('total')->whereDate('orderReadyTime', $dt->toDateString())->sum('total');
    $ccToday = orders::select('ccTypeTransactionFee')->whereDate('orderReadyTime', $dt->toDateString())->sum('ccTypeTransactionFee');

    //$ordersMonth = orders::select('id')->whereDate('created_at', '>=', $dt->startOfMonth()->toDateString() )->whereDate('created_at', '<=', $dt->toDateString() )->toSql();

    $ordersMonth = orders::select('id')->whereBetween(DB::raw('date(orderReadyTime)'), [ Carbon::now()->startOfMonth()->toDateString(), Carbon::now()->toDateString()] )->count();
    $revMonth = orders::select('total')->whereBetween(DB::raw('date(orderReadyTime)'), [ Carbon::now()->startOfMonth()->toDateString(), Carbon::now()->toDateString()] )->sum('total');
    $ccMonth = orders::select('ccTypeTransactionFee')->whereBetween(DB::raw('date(orderReadyTime)'), [ Carbon::now()->startOfMonth()->toDateString(), Carbon::now()->toDateString()] )->sum('ccTypeTransactionFee');

		$breadcrumb = ['dashboard', ''];
    $domain = \Request::get('domain');

	  return view('admin.adminDashboard')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'ordersToday' => $ordersToday, 'revToday' => $revToday, 'ordersMonth' => $ordersMonth, 'revMonth' => $revMonth, 'ccToday' => $ccToday, 'ccMonth' => $ccMonth ]);
    
	} // end dashboard


/*--------------------- Admin Customer View  ----------------------------------------*/

public function customerView(Request $request) {

      $breadcrumb = ['customer', 'view'];
      $domain = \Request::get('domain');

      $agents = domain::leftJoin('users', 'domains.ownerID', '=', 'users.id')
        ->leftJoin('type', 'domains.type', '=', 'type.id')
        ->select('domains.id', 'domains.httpHost', 'domains.active', 'domains.sitename', 'domains.type', 'users.fname', 'users.lname', 'type.description')
        ->get();

      return view('admin.adminCustomerView')->with(['domain' => $domain, 'agents' => $agents, 'breadcrumb' => $breadcrumb]);
    
    }  // end customerView



  /*-------------------- Customer Detail Modal  ----------------------------------------*/

  public function customerDetail(Request $request) {

    $domain = domain::find($request->customerID);
    $type = type::find($domain->type);
    $domain->type = $type->description;
    return $domain;
    
  } // end dashboard


  /*-------------------- Customer Edit  ----------------------------------------*/

  public function customerEdit(Request $request, $customerID) {
    
    $breadcrumb = ['customer', 'view'];
    $domain = \Request::get('domain');
    $customer = domain::find($customerID);
    $types = type::all();

    return view('admin.adminCustomerEdit')->with([ 'domain' => $domain, 'customer' => $customer, 'types' => $types, 'breadcrumb' => $breadcrumb ]);
    
  } // end Customer Edit



  /*-------------------- Customer Edit Save  ----------------------------------------*/

  public function CustomerEditSave(Request $request, $id) {

    $domain = \Request::get('domain');
    $breadcrumb = ['customer', 'view'];
    $agentdomain = domain::find($id);
    $types = type::all();

    // update domain record
    $agentdomain->active = $request->active;
    $agentdomain->name = $request->name;
    $agentdomain->subdomain = $request->subdomain;
    $agentdomain->httpHost = $request->httpHost;
    $agentdomain->sitename = $request->sitename;
    $agentdomain->parentDomainID = $request->parentDomainID;
    $agentdomain->useParentMenu = $request->useParentMenu;
    $agentdomain->ownerID = $request->ownerID;
    $agentdomain->type = $request->type;

    if (!is_null($request->file('newLogo'))) {
      // save background image
      $pictureName = $request->file('newLogo')->getClientOriginalName();
      $pictureSize = round($request->file('newLogo')->getSize()/1024,1); // file size in Kilobytes
      $ext = $request->file('newLogo')->guessClientExtension();
      $imageTypes= array("jpeg" => "jpeg", "jpg" => "jpg", "png" => "png");
      $chk = array_search($ext, $imageTypes);

        if ($chk == FALSE || $pictureSize > 1500) {
            $request->session()->flash('fileError', 'The customer was saved. The logo file ' . $pictureName . '. was not a JPG or PNG file or the file size exceeded 1.5 Mb. Please check your picture file and save the new image.');
        } else {
          //set file paths
          $path = $request->file('newLogo')->storeAs('public/logoimages/' . $id, $pictureName);
          if ($path != $agentdomain->logoPrivate && $agentdomain->logoPrivate != '') {
            // delete old file unless file is same name or does not exist
            Storage::delete($agentdomain->logoPrivate);
          }

          // create public storage path - set updated file paths
          $agentdomain->logoPublic = '/storage/logoimages/' . $id . '/' . $pictureName;
          $agentdomain->logoPrivate = $path;

        }
    }

    // if existing file is deleted and no file added
    if ($request->logoCheck == "changed" && is_null($request->file('logo'))) {
        // delete old picture if any
        Storage::delete($agentdomain->logoPrivate);
        $agentdomain->logoPrivate = ''; 
        $agentdomain->logoPublic = '';
    }

    if (!is_null($request->file('newPicture'))) {
      // save background image
      $pictureName = $request->file('newPicture')->getClientOriginalName();
      $pictureSize = round($request->file('newPicture')->getSize()/1024,1); // file size in Kilobytes
      $ext = $request->file('newPicture')->guessClientExtension();
      $imageTypes= array("jpeg" => "jpeg", "jpg" => "jpg", "png" => "png");
      $chk = array_search($ext, $imageTypes);

        if ($chk == FALSE || $pictureSize > 5000) {

            $request->session()->flash('fileError', 'The customer was saved. The background file ' . $pictureName . '. was not a JPG or PNG file or the file size exceeded 5 Mb. Please check your picture file and save the new image.');

        } else {
          //set file paths
          $path = $request->file('newPicture')->storeAs('public/backgroundimages/' . $id, $pictureName);

          if ($path != $agentdomain->backgroundPrivate && $agentdomain->logoPrivate != '') {
            // delete old file unless file is same name or does not exist
            Storage::delete($agentdomain->backgroundPrivate);
          }

          // create public storage path - set updated file paths
          $agentdomain->backgroundPublic = '/storage/backgroundimages/' . $id . '/' . $pictureName;
          $agentdomain->backgroundPrivate = $path;

        }

    }

    // if existing file is deleted and no file added
    if ($request->backgroundCheck == "changed" && is_null($request->file('picture'))) {
        // delete old picture if any
        Storage::delete($agentdomain->backgroundPrivate);
        $agentdomain->backgroundPrivate = ''; 
        $agentdomain->backgroundPublic = '';

    }

    try {
      $agentdomain->save();
      User::where('domainID', '=', $id)->update(['parentDomainID' => $request->parentDomainID]);
      $request->session()->flash('updateSuccess', 'Customer: ' . $agentdomain->name . ' was successfully updated!');
    }

    catch (Exception $e) {
      $request->session()->flash('updateError', 'There was an error updating the customer. Please try again.');
    }




    return view('admin.adminCustomerEdit')->with([ 'domain' => $domain, 'customer' => $agentdomain, 'breadcrumb' => $breadcrumb, 'types' => $types ]);

  } // end Customer Edit Save



  /*-------------------- New Customer  ----------------------------------------*/

     public function customerNew(Request $request) {

        $breadcrumb = ['customer', 'new'];
        $domain = \Request::get('domain');
        $types = type::all();

        return view('admin.adminCustomerNew')->with(['domain' => $domain, 'types' => $types, 'breadcrumb' => $breadcrumb]);
      
      }



  /*-------------------- New Customer Save (Create) ----------------------------------------*/

  public function customerCreateNew(Request $request) {
    
    $created = "no";
    $domain = \Request::get('domain');
    $breadcrumb = ['customer', 'view'];
    $types = type::all();

    $chkDomain = domain::select('subdomain')->where('subdomain', '=', $request->subdomain)->get();

    if (!isset($chkDomain->subdomain)) {

      //proceed - subdomain does not exist
      $newDomain = new domain();

      // create domain record
      $newDomain->name = $request->name;
      $newDomain->subdomain = $request->subdomain;
      $newDomain->httpHost = $request->httpHost;
      $newDomain->sitename = $request->sitename;
      $newDomain->active = $request->active;
      $newDomain->parentDomainID = $request->parentDomainID;
      $newDomain->useParentMenu = 0; // default setting - must change in edit if using parent menu
      $newDomain->ownerID = $request->ownerID;
      $newDomain->type = $request->type;

      if ($request->picture == '') {
          $newDomain->backgroundPrivate = ''; 
          $newDomain->backgroundPublic = '';

      }

      if ($request->logo == '') {
          $newDomain->logoPrivate = ''; 
          $newDomain->logoPublic = '';

      }

      try {
        $newDomain->save();
        $request->session()->flash('updateSuccess', 'Customer ID: ' . $newDomain->id . ' was successfully created!');
        $created = "yes";

        if (is_null($request->parentDomainID)) { 
          // parent ID = domain ID -> single location
          $newDomain->parentDomainID = $newDomain->id; 
          $newDomain->save();
        }

        // save logo image
        if (!is_null($request->file('logo'))) {
        // add new logo
        $pictureName = $request->file('logo')->getClientOriginalName();
        $pictureSize = round($request->file('logo')->getSize()/1024,1); // file size in Kilobytes
        $ext = $request->file('logo')->guessClientExtension();
        $imageTypes= array("jpeg" => "jpeg", "jpg" => "jpg", "png" => "png");
        $chk = array_search($ext, $imageTypes);

          if ($chk == FALSE || $pictureSize > 1000) {

              $request->session()->flash('fileError', 'The customer was saved. The logo image file ' . $pictureName . '. was not a JPG or PNG file or the file size exceeded 1 Mb. Please check your picture file and edit the new domain.');
              $chk = FALSE;
              $path = '';

          } else {

            //save new file
            // path newDomainImages/user->id/filename
            $path = $request->file('logo')->storeAs('public/logoimages/' . $newDomain->id, $pictureName);

            // create public storage path
            $newDomain->logoPublic = '/storage/logoimages/' . $newDomain->id . '/' . $pictureName;
            $newDomain->logoPrivate = $path;
            $newDomain->save();

          }
        } // end if logo image

        // save background image
        if (!is_null($request->file('picture'))) {
        // add new picture
        $pictureName = $request->file('picture')->getClientOriginalName();
        $pictureSize = round($request->file('picture')->getSize()/1024,1); // file size in Kilobytes
        $ext = $request->file('picture')->guessClientExtension();
        $imageTypes= array("jpeg" => "jpeg", "jpg" => "jpg", "png" => "png");
        $chk = array_search($ext, $imageTypes);

          if ($chk == FALSE || $pictureSize > 5000) {

              $request->session()->flash('fileError', 'The customer was saved. The background image file ' . $pictureName . '. was not a JPG or PNG file or the file size exceeded 5 Mb. Please check your picture file and update the customer.');
              $chk = FALSE;
              $path = '';

          } else {

            //save new file
            // path newDomainImages/user->id/filename
            $path = $request->file('picture')->storeAs('public/backgroundimages/' . $newDomain->id, $pictureName);

            // create public storage path
            $newDomain->backgroundPublic = '/storage/backgroundimages/' . $newDomain->id . '/' . $pictureName;
            $newDomain->backgroundPrivate = $path;
            $newDomain->save();

          }
        } // end if background image

        // create default styles - copy from domainID = 1
        $defaultStyles = siteStyles::find(1);
        $style = new siteStyles;
          $style->domainID = $newDomain->id;
          $style->fontColor = $defaultStyles->fontColor;
          $style->bannerButtonBackgroundColor = $defaultStyles->bannerButtonBackgroundColor;
          $style->bannerButtonBorderColor = $defaultStyles->bannerButtonBorderColor;
          $style->bannerButtonColor = $defaultStyles->bannerButtonColor;
          $style->bannerButtonColorHover = $defaultStyles->bannerButtonColorHover;
          $style->completeOrderBackgroundColor = $defaultStyles->completeOrderBackgroundColor;
          $style->completeOrderBorderColor = $defaultStyles->completeOrderBorderColor;
          $style->completeOrderColor = $defaultStyles->completeOrderColor;
          $style->completeOrderColorHover = $defaultStyles->completeOrderColorHover;
          $style->bannerHomeMinHeight = $defaultStyles->bannerHomeMinHeight;
          $style->locationAddressFontColor = $defaultStyles->locationAddressFontColor;
          $style->locationAddressTopMargin = $defaultStyles->locationAddressTopMargin;
          $style->pixBannerBackgroundColor = $defaultStyles->pixBannerBackgroundColor;
          $style->pixBannerOpacity = $defaultStyles->pixBannerOpacity;
          $style->pixBannerPadding = $defaultStyles->pixBannerPadding;
          $style->pixBannerWidth = $defaultStyles->pixBannerWidth;
          $style->pixBannerColor = $defaultStyles->pixBannerColor;
          $style->navBarToggleBackgroundColor = $defaultStyles->navBarToggleBackgroundColor;
          $style->navBarToggleBorderColor = $defaultStyles->navBarToggleBorderColor;
          $style->navBarLogoMaxHeight =  $defaultStyles->navBarLogoMaxHeight;
          $style->navBarLogoMaxPadding = $defaultStyles->navBarLogoMaxPadding;
          $style->navBarLogoMaxMarginLeft = $defaultStyles->navBarLogoMaxMarginLeft;
          $style->navBarLogoMaxMarginTop = $defaultStyles->navBarLogoMaxMarginTop;
          $style->navBarLIColor = $defaultStyles->navBarLIColor;
          $style->navBarLIHoverColor = $defaultStyles->navBarLIHoverColor;
          $style->calloutBannerBackgroundColor = $defaultStyles->calloutBannerBackgroundColor;
          $style->calloutBannerBorderColor = $defaultStyles->calloutBannerBorderColor;
          $style->calloutBannerColor = $defaultStyles->calloutBannerColor;
          $style->calloutBannerHeight = $defaultStyles->calloutBannerHeight;
          $style->footerBackgroundColor = $defaultStyles->footerBackgroundColor;
          $style->footerColor = $defaultStyles->footerColor;
          $style->footerLinkColor = $defaultStyles->footerLinkColor;
          $style->footerLinkHoverColor = $defaultStyles->footerLinkHoverColor;
          $style->servicesMinHeight = $defaultStyles->servicesMinHeight;
        $style->save();

        $siteConfig = new domainSiteConfig;
          $siteConfig->domainID = $newDomain->id;
          $siteConfig->orderMessage = "Your order has been received and is being prepared. Pickup time is in approximately 30 minutes.";
          $siteConfig->deliveryMessage = "Your order has been received and is being prepared. Delivery time is in approximately 55 minutes.";
          $siteConfig->futureOrderMessage = "Your order has been received and will be ready for pickup on [[pickupTime]].";
          $siteConfig->futureDeliveryMessage = "Your order has been received and will be delivered on [[deliveryTime]].";
          $siteConfig->pickupOIP = 'Your order is now being prepared. You may pickup up your order in approximately 30 minutes.';
        	$siteConfig->deliverOIP = 'Your order is now being prepared. Your order will be prepared and out for delivery in approximately 30 minutes.';
        	$siteConfig->deliverOrderReady = 'Your order has been prepared and is out for delivery. Your order should arrive in the next 15 - 30 minutes.';
          $siteConfig->orderFooter = "Thank you for your order.";
          $siteConfig->orderAlert = "Your order has been received and is being prepared. Payment is due on pickup. Thank you.";
          $siteConfig->orderReady = "Your order has been prepared and is ready for pickup. Thank you for your order.";
          $siteConfig->welcomeEmail = "Welcome to " . $domain->name . " online! You may order online for pickup during our normal business hours. Login to your account to review your order history and print receipts if needed.<br><br>Thank you for your order.";     
          $siteConfig->cartClearTimer = 0;
          $siteConfig->dineInLastOrderTimeOffset = 15;
          $siteConfig->save();

        $paymentConfig = new domainPaymentConfig;
          $paymentConfig->domainID = $newDomain->id;
        $paymentConfig->save();

        // TODO - create marketing system user

        // set table for updating by marketing system
        $marketing = new marketing();
          $marketing->domainID = $newDomain->id;
          $marketing->mktDomainID = 0;
          $marketing->mktCustomerID = 0;
          $marketing->mktUserID = 0;
          $marketing->mktRepID = 0;
        $marketing->save();


      } // end try

      catch (Exception $e) {
        $request->session()->flash('updateError', 'There was an error creating the new customer. Please try again.');
      }

    } else {
        $request->flash();
        $request->session()->flash('updateError', 'The Subdomain already exists. Please try again.');
        return view('admin.adminCustomerNew')->with(['domain' => $domain, 'types' => $types, 'breadcrumb' => $breadcrumb]);

    }

    return view('admin.adminCustomerEdit')->with(['domain' => $domain, 'customer' => $newDomain, 'types' => $types, 'breadcrumb' => $breadcrumb]);

  }  // end customerCreateNew


    /*-------------------- System Site Styles  ----------------------------------------*/

    public function adminStyle(Request $request, $domainID) {

    // $domainID = domain ID of customer (domain) style to edit
    $breadcrumb = ['customer', 'view'];
    $domain = domain::find($domainID);
    $styles = siteStyles::where('domainID', '=', $domainID)->first();

    return view('admin.adminStyleEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'styles' => $styles ]);
    
    } // end adminSiteStyles


    /*-------------------- System Site Styles Save (d0) ----------------------------------------*/

    public function adminStyleDo(Request $request, $domainID) {

    // $domainID = domain ID of customer (domain) style to edit
    $breadcrumb = ['customer', 'view'];
    $domain = domain::find($domainID);
    $style = siteStyles::where('domainID', '=', $domainID)->first();
      $style->domainID = $domainID;
      $style->backgroundColor = $request->backgroundColor;
      $style->fontColor = $request->fontColor;
      $style->bannerButtonBackgroundColor = $request->bannerButtonBackgroundColor;
      $style->bannerButtonBorderColor = $request->bannerButtonBorderColor;
      $style->bannerButtonColor = $request->bannerButtonColor;
      $style->bannerButtonColorHover = $request->bannerButtonColorHover;
      $style->completeOrderBackgroundColor = $request->completeOrderBackgroundColor;
      $style->completeOrderBorderColor = $request->completeOrderBorderColor;
      $style->completeOrderColor = $request->completeOrderColor;
      $style->completeOrderColorHover = $request->completeOrderColorHover;
      $style->bannerHomeMinHeight = $request->bannerHomeMinHeight;
      $style->pixBannerBackgroundColor = $request->pixBannerBackgroundColor;
      $style->locationAddressFontColor = $request->locationAddressFontColor;
      $style->locationAddressTopMargin = $request->locationAddressTopMargin;
      $style->pixBannerOpacity = $request->pixBannerOpacity;
      $style->pixBannerPadding = $request->pixBannerPadding;
      $style->pixBannerWidth = $request->pixBannerWidth;
      $style->pixBannerColor = $request->pixBannerColor;
      $style->navBarToggleBackgroundColor = $request->navBarToggleBackgroundColor;
      $style->navBarToggleBorderColor = $request->navBarToggleBorderColor;
      $style->navBarLogoMaxHeight =  $request->navBarLogoMaxHeight;
      $style->navBarLogoMaxPadding = $request->navBarLogoMaxPadding;
      $style->navBarLogoMaxMarginLeft = $request->navBarLogoMaxMarginLeft;
      $style->navBarLogoMaxMarginTop = $request->navBarLogoMaxMarginTop;
      $style->navBarLIColor = $request->navBarLIColor;
      $style->navBarLIHoverColor = $request->navBarLIHoverColor;
      $style->calloutBannerBackgroundColor = $request->calloutBannerBackgroundColor;
      $style->calloutBannerBorderColor = $request->calloutBannerBorderColor;
      $style->calloutBannerColor = $request->calloutBannerColor;
      $style->calloutBannerHeight = $request->calloutBannerHeight;
      $style->footerBackgroundColor = $request->footerBackgroundColor;
      $style->footerColor = $request->footerColor;
      $style->footerLinkColor = $request->footerLinkColor;
      $style->footerLinkHoverColor = $request->footerLinkHoverColor;
      $style->servicesMinHeight = $request->servicesMinHeight;
    $style->save();

    if (!$style) {
        $request->session()->flash('updateError', 'The Subdomain already exists. Please try again.'); 
    } else {
      $request->session()->flash('updateSuccess', 'Customer ID: ' . $domainID . ' styles were successfully updated!');
    }


    return view('admin.adminStyleEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'styles' => $style ]);
    
    } // end adminSiteStylesDo



    /*-------------------- Domain (Customer) Site Config  ----------------------------------------*/

    public function adminSiteConfig(Request $request, $domainID) {

    // $domainID = domain ID of customer (domain) style to edit
    $breadcrumb = ['customer', 'view'];
    $domain = domain::find($domainID);
    $siteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
    $states = usStates::all();


    return view('admin.adminSiteConfigEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'siteConfig' => $siteConfig, 'states' => $states ]);
    
    } // end adminSiteConfig

        /*-------------------- Domain (Customer) Site Config Save (Do) ----------------------------------------*/

    public function adminSiteConfigDo(Request $request, $domainID) {

    // $domainID = domain ID of customer (domain) style to edit
    $breadcrumb = ['customer', 'view'];
    $domain = domain::find($domainID);
    $states = usStates::all();
    
    if ($request->taxDelivery) {
      // tax delivery
      $deliveryTaxRate = $request->deliveryTaxRate;
    } else {
      // don't tax delivery - set tax = 0
      $deliveryTaxRate = 0;
    }

    $siteConfig = domainSiteConfig::firstOrNew(['domainID' => $domainID]);
      $siteConfig->sendEmail = $request->sendEmail;
      $siteConfig->sendText = $request->sendText;
      $siteConfig->sendEmailUserName = $request->sendEmailUserName;
      $siteConfig->sendEmailPassword = encrypt($request->sendEmailPassword);
      $siteConfig->orderReceiveEmail = $request->orderReceiveEmail;
      $siteConfig->hpEprint = $request->hpEprint;
      $siteConfig->taxRate = $request->taxRate;
      $siteConfig->managerFname = $request->managerFname;
      $siteConfig->managerLname = $request->managerLname;      
      $siteConfig->locationName = $request->locationName;
      $siteConfig->address1 = $request->address1;
      $siteConfig->address2 = $request->address2;
      $siteConfig->city = $request->city;
      $siteConfig->state = $request->state;
      $siteConfig->zipCode = $request->zipCode;
      $siteConfig->locationPhone = $request->locationPhone;
      $siteConfig->locationIPAddress = $request->locationIPAddress;
      $siteConfig->orderAhead = $request->orderAhead;
      $siteConfig->deliveryPrepMinutes = $request->deliveryPrepMinutes;            
      $siteConfig->pickupPrepMinutes = $request->pickupPrepMinutes;
      $siteConfig->deliveryRecommendedTipPercent = $request->deliveryRecommendedTipPercent;
      $siteConfig->deliveryRequireRecommendedTip = $request->deliveryRequireRecommendedTip;
      $siteConfig->pickupRecommendedTipPercent = $request->pickupRecommendedTipPercent;
      $siteConfig->pickupRequireRecommendedTip = $request->pickupRequireRecommendedTip;      
      $siteConfig->delivery = $request->delivery;
      $siteConfig->deliveryCharge = $request->deliveryCharge;
      $siteConfig->deliveryRadius = $request->deliveryRadius;
      $siteConfig->latitude = $request->latitude;
      $siteConfig->longitude = $request->longitude;
      $siteConfig->taxDelivery = $request->taxDelivery;
      $siteConfig->deliveryTaxRate = $deliveryTaxRate;
      $siteConfig->deliveryMinOrder = $request->deliveryMinOrder;
      $siteConfig->locationOrder = $request->locationOrder;
      $siteConfig->orderMessage = $request->orderMessage;
      $siteConfig->deliveryMessage = $request->deliveryMessage;
      $siteConfig->futureOrderMessage = $request->futureOrderMessage ?? null;
      $siteConfig->futureDeliveryMessage = $request->futureDeliveryMessage ?? null;
    	$siteConfig->pickupOIP = $request->pickupOIP;
      $siteConfig->deliverOIP = $request->deliverOIP;
      $siteConfig->deliverOrderReady = $request->deliverOrderReady;
      $siteConfig->googleMapKey = $request->googleMapKey;
      $siteConfig->orderFooter = $request->orderFooter;
      $siteConfig->orderReady = $request->orderReady;
      $siteConfig->orderAlert = $request->orderAlert;
      $siteConfig->welcomeEmail = $request->welcomeEmail;
      $siteConfig->mapLink = $request->mapLink;
      $siteConfig->cartClearTimer = $request->cartClearTimer;
      $siteConfig->dineInLastOrderTimeOffset = $request->dineInLastOrderTimeOffset;
    $sc = $siteConfig->save();

    if ($sc) {
      $request->session()->flash('updateSuccess', 'Update Successful.');
    } else {
      $request->session()->flash('updateError', 'There was an error updating the site configuration information. Please try again.');
    }

    return view('admin.adminSiteConfigEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'siteConfig' => $siteConfig, 'states' => $states ]);
    
    } // end adminSiteConfigDo


    /*-------------------- Domain (Customer) Payment Config  ----------------------------------------*/

    public function adminPaymentConfig(Request $request, $domainID) {

    // $domainID = domain ID of customer (domain) style to edit
    $breadcrumb = ['customer', 'view'];
    $domain = domain::find($domainID);
    $paymentConfig = domainPaymentConfig::where('domainID', '=', $domainID)->first();
    $ccTypes = ccType::orderBy('sortOrder', 'asc')->get();

    return view('admin.adminPaymentConfigEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'paymentConfig' => $paymentConfig, 'ccTypes' => $ccTypes ]);
    
    } // end adminPaymentConfig


    /*-------------------- Domain (Customer) Payment Config Save (Do) ----------------------------------------*/

    public function adminPaymentConfigDo(Request $request, $domainID) {
// dd($request);
    // $domainID = domain ID of customer (domain) style to edit
    $breadcrumb = ['customer', 'view'];
    $domain = domain::find($domainID);
    $paymentConfig = domainPaymentConfig::where('domainID', '=', $domainID)->first();
      $paymentConfig->accountName = $request->accountName;
      $paymentConfig->ccType = $request->ccType;
      $paymentConfig->mode = $request->mode;
      $paymentConfig->accountName = $request->accountName;
      $paymentConfig->stripeTestPublishableKey = $request->stripeTestPublishableKey;
      $paymentConfig->stripeLivePublishableKey = $request->stripeLivePublishableKey;
      $paymentConfig->stripeDataImage = $request->stripeDataImage;
      $paymentConfig->stripestatementDescriptor = $request->stripestatementDescriptor;
      $paymentConfig->stripeTransactionPercent = $request->stripeTransactionPercent;
      $paymentConfig->stripeTransactionFee = $request->stripeTransactionFee;
      $paymentConfig->togoMerchantID = $request->togoMerchantID;
      $paymentConfig->togoACHMerchantID = $request->togoACHMerchantID;
      $paymentConfig->togoTestUser = $request->togoTestUser;
      $paymentConfig->togoLiveUser = $request->togoLiveUser;
      $paymentConfig->togoMonthlyProcessingFee = $request->togoMonthlyProcessingFee;
      $paymentConfig->togoTransactionPercent = $request->togoTransactionPercent;
      $paymentConfig->togoTransactionFee = $request->togoTransactionFee;
      $paymentConfig->authNetTestUser = $request->authNetTestUser;
      $paymentConfig->authNetLiveUser = $request->authNetLiveUser;
            
      if (is_null($request->stripeTestSecretKey)) {
        $paymentConfig->stripeTestSecretKey = $request->stripeTestSecretKey;
      } else {
        $paymentConfig->stripeTestSecretKey = encrypt($request->stripeTestSecretKey);
      }

      if (is_null($request->stripeLiveSecretKey)) {
        $paymentConfig->stripeLiveSecretKey = $request->stripeLiveSecretKey;
      } else {
        $paymentConfig->stripeLiveSecretKey = encrypt($request->stripeLiveSecretKey);
      }

      if (is_null($request->togoTestPassword)) {
        $paymentConfig->togoTestPassword = $request->togoTestPassword;
      } else {
        $paymentConfig->togoTestPassword = encrypt($request->togoTestPassword);
      }

      if (is_null($request->togoLivePassword)) {
        $paymentConfig->togoLivePassword = $request->togoLivePassword;
      } else {
        $paymentConfig->togoLivePassword = encrypt($request->togoLivePassword);
      }

      if (is_null($request->authNetTestPass)) {
        $paymentConfig->authNetTestPass = $request->authNetTestPass;
      } else {
        $paymentConfig->authNetTestPass = encrypt($request->authNetTestPass);
      }

      if (is_null($request->authNetLivePass)) {
        $paymentConfig->authNetLivePass = $request->authNetLivePass;
      } else {
        $paymentConfig->authNetLivePass = encrypt($request->authNetLivePass);
      }

    $pc = $paymentConfig->save();
    $ccTypes = ccType::orderBy('sortOrder', 'asc')->get();

    if ($pc) {
      $request->session()->flash('updateSuccess', 'Update Successful.');
    } else {
      $request->session()->flash('updateError', 'There was an error updating the site payment information. Please try again.');
    }

    return view('admin.adminPaymentConfigEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'paymentConfig' => $paymentConfig, 'ccTypes' => $ccTypes ]);
    
    } // end adminPaymentConfig



/*-------------------- Check httphost  ----------------------------------------*/

    public function checkHttpHost(Request $request)
    {
        $hostCheck = domain::select('httpHost')->where('httpHost', '=', $request->httpHost)->first();
        if (is_null($hostCheck) || $hostCheck->httpHost == $request->httpHost) {$status = "pass";} else {$status = "error";}
        $data = json_encode(array('status' => $status, 'httpHost' => $request->httpHost));
        return $data;
    } // end checkHttpHost



/*-------------------------------- upload stripe data image (logo) ---------------------------------------------------*/   


    public function stripeDataImage(Request $request) {
      


      $domain = domain::find($request->domainID);
      if (is_null($request->sdi)) {
        $message['status'] = "error";
        $message['message'] = "There was an error processing your Stripe Data Image file. Please try again.";
        return json_encode($message);
      }
      
      // add new file
      $fileName = $request->file('sdi')->getClientOriginalName();
      $fileName = str_replace(' ', '', $fileName);
      $fileSize = $request->file('sdi')->getSize(); // file size in Kilobytes
      // check filesize
      if ($fileSize > 50000) {
        $message['status'] = "error";
        $message['message'] = "The Stripe Data Image filesize cannot be larger than 50Kb. Please check your file and try again.";
        return json_encode($message);
      }

      // check filetype
      $fileExt = $request->file('sdi')->guessClientExtension(); // file type
      if ($fileExt != 'png') {
        $message['status'] = "error";
        $message['message'] = "The Stripe Data Image filetype must be a png. Please check your file and try again.";
        return json_encode($message);
      }

      //save new file
      if($request->existingName != '') {Storage::delete('public/logoImages/' . $domain->id . '/' . $request->existingName);}
      $path = $request->file('sdi')->storeAs('public/logoImages/' . $domain->id . '/', $fileName);
      $up = domainPaymentConfig::where('domainID', '=', $domain->id)->update([ 'stripeDataImage' => $fileName]);
      
      
      // return data
      $message['status'] = "success";
      $message['fileName'] = $fileName;
      $message['fileSize'] = $fileSize;
      $message['fileExt'] = $fileExt;
      $message['domainID'] = $domain->id;
      return json_encode($message);

    }     // end stripeDataImage



/*--------------------------------View Order Detail  ---------------------------------------------------*/   


    public function orderDetail(Request $request) {
     
      $order = orders::find($request->orderID);
      return ($order);

    } // end orderDetail

/*--------------------------------View Order HTML  ---------------------------------------------------*/   


    public function viewOrder(Request $request, $orderID) {

      $domainID = \Request::get("domain")->id;
      $user = Auth::user();

      if ($user->role_auth_level >=40 ) {
        // view all orders
        $orders = orders::where('id', '=', $orderID)->first();
        if (empty($orders)) { abort(404); }
      } else {
        // only view owner orders
        $orders = orders::where('id', '=', $orderID)->where('domainID', '=', $domainID)->first();
        if (empty($orders)) { abort(404); }
      }

      return view('admin.viewOrder')->with([ 'html' => $orders->emailData ]);

    } // end viewOrder


/*--------------------------------View Order HTML  ---------------------------------------------------*/   


    public function viewOrderOnly(Request $request, $orderID) {

      $domainID = \Request::get("domain")->id;
      $user = Auth::user();

      if ($user->role_auth_level >=40 ) {
        // view all orders
        $orders = orders::where('id', '=', $orderID)->first();
        if (empty($orders)) { abort(404); }
      } else {
        // only view owner orders
        $orders = orders::where('id', '=', $orderID)->where('domainID', '=', $domainID)->first();
        if (empty($orders)) { abort(404); }
      }

      return view('admin.viewOrderOnly')->with([ 'html' => $orders->emailData ]);

    } // end viewOrderOnly


/*--------------------------------Send Contact Text  ---------------------------------------------------*/   


    public function sendContactText(Request $request) {

    $contact = contacts::find($request->cid);
    $domainSiteConfig = domainSiteConfig::where('domainID', '=', $contact->tgmDomainID)->first();
    $to = registeredNumbers::find($contact->unformattedMobile);

    // create the text
    $type = "sms";
    $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
    $subject = $domainSiteConfig->locationName . "Online Order Message";
    
    // Create the Transport using HWCTMailer
    $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

    // Create the Mailer using created Transport
    $mailer = HWCTMailer::createMailer($transport);

    // Create a message, $subject, $from, $message
    $message = HWCTMailer::createMessage($subject, $sender , $request->msg, $domainSiteConfig->sendEmail);

    $numSent = HWCTMailer::sendMessage($mailer, $message, $to, $type );

    return $numSent;

    } // end sendContactText


  /*--------------------------------Send Order In Process  ---------------------------------------------------*/   


  public function sendOrderInProcess(Request $request) {

    $printOrder = '';
    $order = orders::find($request->orderID);
    if ($order->orderInProcess == 9) { $printOrder = "yes"; }
    $order->orderInProcess = 2; // order in process
    $os = $order->save();
    if ($request->orderReadOnly == "yes") { 
      // don't send text
      return "complete";
    }

    $domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
    $to = registeredNumbers::find($order->unformattedMobile);

    if ( $order->orderType == 'deliver' ) {
      // delivery order in process message
      $textMessage = $domainSiteConfig->deliverOIP;
    } else {
      // pickup order in process message
      $textMessage = $domainSiteConfig->pickupOIP;
    }

    // create the text
    $type = "sms";
    $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
    $subject = $domainSiteConfig->locationName . "Online Order Message";
    
    // Create the Transport using HWCTMailer
    $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

    // Create the Mailer using created Transport
    $mailer = HWCTMailer::createMailer($transport);

    // Create a message, $subject, $from, $message
    $message = HWCTMailer::createMessage($subject, $sender , $textMessage, $domainSiteConfig->sendEmail);

    $numSent = HWCTMailer::sendMessage($mailer, $message, $to, $type );

    if ($printOrder == 'yes') {

            // print receipt
            if ($domainSiteConfig->printReceipt != 0 ) {
                // get printer info
                $printer = printers::find($domainSiteConfig->printReceipt);
                            
                // print receipt
                if (!is_null($printer)) { $printReceipt = PrintOrders::printReceipt($printer, $order, $domainSiteConfig, 'new'); }
            } // end print receipt

            // print order
            if ($domainSiteConfig->printOrder != 0 ) {
                // get printer info
                $printer = printers::find($domainSiteConfig->printOrder);
                            
                // print receipt
                if (!is_null($printer)) { $printOrder = PrintOrders::printOrder($printer, $order, $domainSiteConfig, 'new'); }
                
            } // end print order

    } // end if printOrder

    return $numSent;

  } // end sendOrderInProcess


  /*--------------------------------Send Order Ready  ---------------------------------------------------*/   


    public function sendOrderReady(Request $request) {

      $order = orders::find($request->orderID);
      $order->orderInProcess = 0; // order ready
      $os = $order->save();
      if ($request->orderReadOnly == "yes") { 
        // don't send text
        return "complete";
      }

      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
      $to = registeredNumbers::find($order->unformattedMobile);

      // create the text
      $type = "sms";
      $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
      $subject = $domainSiteConfig->locationName . "Online Order Message";

      if ( $order->orderType == 'deliver' ) {
        // delivery order in process message
        $textMessage = $domainSiteConfig->deliverOrderReady;
      } else {
        // pickup order in process message
        $textMessage = $domainSiteConfig->orderReady;
      }
      
      // Create the Transport using HWCTMailer
      $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

      // Create the Mailer using created Transport
      $mailer = HWCTMailer::createMailer($transport);

      // Create a message, $subject, $from, $message
      $message = HWCTMailer::createMessage($subject, $sender , $textMessage, $domainSiteConfig->sendEmail);

      $numSent = HWCTMailer::sendMessage($mailer, $message, $to, $type );

      return $numSent;

    } // end sendOrderReady




    /*-------------------- Create New Customer  ----------------------------------------*/

    public function customerCreate(Request $request) {

      $breadcrumb = ['customer', 'new'];
      $domain = \Request::get('domain');

      return view('admin.adminCustomerCreate')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
    
    }

    /*-------------------- Create New Customer Do (Save) ----------------------------------------*/

    public function customerCreateDo(Request $request) {

      // validate form fields
      Validator::make($request->all(), [
        'customerName' => 'required|max:255',
        'siteName' => 'required|max:255',
        'subDomain' => 'required|max:255',
        'httpHost' => 'required|unique:domains,httpHost|max:255',
        'email' => 'required|email|max:255',
        'fname' => 'required|max:255',
        'lname' => 'required|max:255',
        'mobile' => 'required|max:35',
      ])->validate();

      // create new domain
      $domain1 = domain::find(1); // default domain
      $domain = new domain();
        $domain->parentDomainID = 1; //placeholder
        $domain->name = $request->customerName;
        $domain->ownerID = 1; //placeholder
        $domain->subdomain = $request->subDomain;
        $domain->httpHost = $request->httpHost;
        $domain->siteName = $request->siteName;
        $domain->active = 'yes';
        $domain->useParentMenu = 0;
        $domain->type = 3;
        $domain->logoPublic = $domain1->logoPublic;;
        $domain->logoPrivate = $domain1->logoPrivate;;
        $domain->backgroundPublic = $domain1->backgroundPublic;;
        $domain->backgroundPrivate = $domain1->backgroundPrivate;;
      $domain->save();

      // create new user
      $user = new User();
        $user->role_auth_level = 35;
        $user->domainID = $domain->id;
        $user->parentDomainID = $domain->id;
        $user->active = 'yes';
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->unformattedMobile = CellNumberData::numbersOnly($request->mobile);
        $user->password = Hash::make('p@ssword');
      $user->save();

      // update domain with ownerID
        $domain->parentDomainID = $domain->id;
        $domain->ownerID = $user->id;
      $domain->save();
        
      // scaffold domainSiteConfig table
      $siteConfig = new domainSiteConfig();
          $siteConfig->domainID = $domain->id;
          $siteConfig->managerFname = $request->fname;
          $siteConfig->managerLname = $request->lname;
          $siteConfig->locationName = $request->customerName;
          $siteConfig->sendText = $request->mobile;
          $siteConfig->globalDiscount = 0;
          $siteConfig->transactionFee = 0;
          $siteConfig->delivery = 'no';
          $siteConfig->googleMapKey = 'AIzaSyDktZq8WwR2WyKTOuK1ge0nmJCQQptNgE8';
          $siteConfig->printReceipt = 0;
          $siteConfig->numReceiptCopies = 1;
          $siteConfig->lineReceiptHeight = 75;
          $siteConfig->printOrder = 0;
          $siteConfig->numOrderCopies = 1;
          $siteConfig->lineOrderHeight = 85;      
          $siteConfig->taxRate = 10;
          $siteConfig->cartClearTimer = 0;
          $siteConfig->dineInLastOrderTimeOffset = 15;  
          $siteConfig->locationOrder = 'no'; // in restaurant order placeholder
          $siteConfig->orderMessage = "Your order has been received and is being prepared. Pickup time is in approximately 30 minutes.";
          $siteConfig->deliveryMessage = "Your order has been received and is being prepared. Delivery time is in approximately 55 minutes.";
          $siteConfig->futureOrderMessage = "Your order has been received and will be ready for pickup on [[pickupTime]].";
          $siteConfig->futureDeliveryMessage = "Your order has been received and will be delivered on [[deliveryTime]].";
          $siteConfig->pickupOIP = 'Your order is now being prepared. You may pickup up your order in approximately 30 minutes.';
        	$siteConfig->deliverOIP = 'Your order is now being prepared. Your order will be prepared and out for delivery in approximately 30 minutes.';
        	$siteConfig->deliverOrderReady = 'Your order has been prepared and is out for delivery. Your order should arrive in the next 15 - 30 minutes.';
          $siteConfig->orderFooter = "Thank you for your order.";
          $siteConfig->orderAlert = "Your order has been received and is being prepared. Payment is due on pickup. Thank you.";
          $siteConfig->orderReady = "Your order has been prepared and is ready for pickup. Thank you for your order.";
          $siteConfig->welcomeEmail = "Welcome to " . $domain->name . " online! You may order online for pickup during our normal business hours. Login to your account to review your order history and print receipts if needed.<br><br>Thank you for your order.";
      $siteConfig->save();

      // scaffold domainPaymentConfig table
      $paymentConfig = new domainPaymentConfig();
          $paymentConfig->domainID = $domain->id;
          $paymentConfig->ccType = 3;
          $paymentConfig->mode = "test";
          $paymentConfig->stripeStatementDescriptor = $request->customerName;
          $paymentConfig->stripeTransactionPercent = 2.9;
          $paymentConfig->stripeTransactionFee = .3;
          $paymentConfig->togoMonthlyProcessingFee = 0;
          $paymentConfig->togoTransactionPercent = 2.9;
          $paymentConfig->togoTransactionFee = .3;
          $paymentConfig->accountName = $request->customerName;
      $paymentConfig->save();

      // scaffold default styles from default record 1
      $styles = siteStyles::find(1);
      $newStyles = $styles->replicate();
      $newStyles->domainID = $domain->id;
      $newStyles->save();

      // default managerPIN - 1234
      $pin = new managerPIN();
        $pin->domainID = $domain->id;
        $pin->userID = $user->id;
        $pin->managerPIN = 1234;
      $pin->save();

      // MARKETING SYSTEM SETUP

      // create new marketing domain
      $mktDomain1 = mktDomains::find(1); // default domain
      $mktDomain = new mktDomains();
        $mktDomain->orderDomainID = $domain->id;
        $mktDomain->name = $request->customerName;
        $mktDomain->ownerID = 1; //placeholder
        $mktDomain->subdomain = $request->subDomain;
        $mktDomain->httpHost = $request->subDomain . '.togomarketing.biz';
        $mktDomain->siteName = $request->siteName;
        $mktDomain->active = 'yes';
        $mktDomain->sendLargeList = 0;
        $mktDomain->logoPublic = $mktDomain1->logoPublic;;
        $mktDomain->logoPrivate = $mktDomain1->logoPrivate;;
        $mktDomain->backgroundPublic = $mktDomain1->backgroundPublic;;
        $mktDomain->backgroundPrivate = $mktDomain1->backgroundPrivate;;
      $mktDomain->save();

      // create new marketing customer
      // set userID after creating user
      $mktCustomer = new mktCustomers();
        $mktCustomer->domainID = $mktDomain->id;
        $mktCustomer->repID = 1;
        $mktCustomer->userID = 1; //placeholder
        $mktCustomer->active = 'yes';
        $mktCustomer->fname = $request->fname;
        $mktCustomer->lname = $request->lname;
        $mktCustomer->company = $request->customerName;
        $mktCustomer->title = 'owner';
        $mktCustomer->email = $request->email;
      $mktCustomer->save();

      // create mktUser
      $mktUser = new mktUsers();
        $mktUser->role_auth_level = 30; // owner role
        $mktUser->domainID = $mktDomain->id;
        $mktUser->customerID = $mktCustomer->id;
        $mktUser->repID = 1;
        $mktUser->active = 'yes';
        $mktUser->fname = $request->fname;
        $mktUser->lname = $request->lname;
        $mktUser->email = $request->email;
        $mktUser->mobile = $user->mobile;
        $mktUser->unformattedMobile = $user->unformattedMobile;
        $mktUser->password = Hash::make('p@ssword');
      $mktUser->save();

      // update mktCustomer with user ID
      $mktCustomer->userID = $mktUser->id;
      $mktCustomer->save();

      // update mktDomain with userID(ownerID)
      $mktDomain->ownerID = $mktUser->id;
      $mktDomain->save();

      // update marketing myInformation table
      $myInfo = new mktMyInformation();
        $myInfo->domainID = $mktDomain->id;
        $myInfo->customerID = $mktCustomer->id;
        $myInfo->userID = $mktUser->id;
        $myInfo->repID = 1;
        $myInfo->active = 'yes';
        $myInfo->fname = $request->fname;
        $myInfo->lname = $request->lname;
        $myInfo->company = $request->customerName;
        $myInfo->title = 'Owner';
        $myInfo->website = 'https://' . $request->httpHost;
        $myInfo->email = $request->email;;
        $myInfo->mobile = $user->mobile;
        $myInfo->unformattedMobile = $user->unformattedMobile;
      $myInfo->save();

      // END MARKETING SYSTEM SETUP

      // SET SEND ACCOUNT ON MAILSERVER

      // create send email from httpHost name
      $pos = strpos($request->httpHost, '.');
      if ($pos !== false) {
          $email = substr_replace($request->httpHost, '@', $pos, strlen('.'));
      }

      $address = explode("@", $email);

      $new = new sendEmailAdmin();
          $new->active = "yes";
          $new->mailbox = $email;
          $new->fname = $request->fname;
          $new->lname = $request->lname;
          $new->domainID = $request->domainID;
          $new->siteURL = $request->httpHost;
          $new->localPart = trim($address[0]);
          $new->domainPart = trim($address[1]);
          $new->transport = "bounces";
          $new->ratePlan = "send";
          $new->billingStatus = "current";
          $new->serverID = env('ORDERS_SERVER_ID');
      $new->save();

      // dovecot password
      $password = Str::random(12); // generate random password
      $query = "update mailboxes set password = ENCRYPT('" . $password . "', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16))) where id = " . $new->id;
      $ok = DB::connection('emailAdminDB')->update($query);

        $siteConfig->sendEmail = $email;
        $siteConfig->sendEmailUserName = $email;
        $siteConfig->sendEmailPassword = encrypt($password);
        $siteConfig->orderReceiveEmail = $request->email;
      $siteConfig->save();

      // END SEND ACCOUNT ON MAILSERVER

      // set table for order system to update marketing system
      $marketing = new marketing();
        $marketing->domainID = $domain->id;
        $marketing->mktDomainID = $mktDomain->id;
        $marketing->mktCustomerID = $mktCustomer->id;
        $marketing->mktUserID = $mktUser->id;
        $marketing->mktRepID = 1;
      $marketing->save();

      // create first contact
      $contact = new contacts();
        $contact->tgmDomainID = $domain->id;
        $contact->tgmUserID = $user->id;
        $contact->domainID = $mktDomain->id;
        $contact->customerID = $mktCustomer->id;
        $contact->userID = $mktUser->id;
        $contact->repID = 1;
        $contact->fname = $request->fname;
        $contact->lname = $request->lname;
        $contact->company = $request->customerName;
        $contact->title = 'Owner';
        $contact->email = $request->email;;
        $contact->mobile = $user->mobile;
        $contact->unformattedMobile = $user->unformattedMobile;
      $contact->save();

      $request->session()->flash('deleteSuccess', 'The new customer was successfully created');

      return redirect()->route('customerView');
    
    }

    /*-------------------- Delete Customer  ----------------------------------------*/

    // deletes ALL customer data including menus!!!!!

    public function customerDelete(Request $request, $domainID) {
      

      $marketing = marketing::find($domainID);
      mktDomains::destroy($marketing->mktDomainID); // delete domain, user, and customer on marketing
      domain::destroy($domainID); // deletes order system domain, users, menus, etc

      $request->session()->flash('deleteSuccess', 'The customer was successfully deleted.');

      $breadcrumb = ['customer', 'view'];
      $domain = \Request::get('domain');
      
      $agents = domain::leftJoin('users', 'domains.ownerID', '=', 'users.id')
        ->leftJoin('type', 'domains.type', '=', 'type.id')
        ->select('domains.id', 'domains.httpHost', 'domains.active', 'domains.sitename', 'users.fname', 'users.lname', 'type.description')
        ->get();



      return view('admin.adminCustomerView')->with(['domain' => $domain, 'agents' => $agents, 'breadcrumb' => $breadcrumb]);   
    }



} // end adminController
