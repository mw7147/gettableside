<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class authLevelController extends Controller
{
    
  /*
  |----------------------------------------------------------------------------------------------
  | Auth Level Check
  |----------------------------------------------------------------------------------------------
  |
  */

    public function authLevel(Request $request) {

    $authLevel = $this->role($request);
    $domain = \Request::get('domain');

    if (intval($authLevel) == 10) {
    	return redirect()->action('customerController@dashboard')->with(['domain' => $domain]);    

    } elseif (intval($authLevel) == 20) {
        return redirect()->action('staffController@dashboard')->with(['domain' => $domain]);    
   
    } elseif ($authLevel == 30) {
        return redirect()->action('managerController@dashboard')->with(['domain' => $domain]);    

    } elseif ($authLevel == 35) {
        return redirect()->action('ownerController@dashboard')->with(['domain' => $domain]);    
    
    } elseif ($authLevel == 40) {
        return redirect()->action('hwctController@dashboard')->with(['domain' => $domain]);    
    
    } elseif ($authLevel == 50) {
        return redirect()->action('adminController@dashboard')->with(['domain' => $domain]);       

    } else {
      Auth::logout(); abort(410);

    } // end if

  } // end function authLevel
 
    
  /*
  |----------------------------------------------------------------------------------------------
  | Determine Role - set for admin
  |----------------------------------------------------------------------------------------------
  |
  */


  // determine role

  protected function role(Request $request) {

      $user = $request->user();
      $domain = \Request::get('domain');

      if ($domain['id'] >2 && $user->role_auth_level >= 35 && $user->domainID <= 2) {return 35;} // HWCT Employee

      if ($user->domainID != $domain['id']) {
          if (Auth::check()) {Auth::logout();}

          abort(403);
      }

      return $user->role_auth_level;

  } // end function role


} // end authLevelController
