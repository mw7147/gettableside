<?php

namespace App\Http\Controllers;

use Facades\App\hwct\ProcessOrders;
use Facades\App\hwct\PrintOrders;
use Illuminate\Http\Request;
use App\domainSiteConfig;
use App\contacts;
use App\domain;
use App\orders;

class reprintController extends Controller {
    
    /*----------------------------- Restore Receipt ------------------------------------------*/
    
    function restore(Request $request) {
    
    	$domain = \Request::get('domain');
    	$breadcrumb = ['settings', ''];

    	return view('settings.restore')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
    
    } //end restore
    
    
    
    
    function restoreDo(Request $request) {
    
    	
    	$order = orders::find($request->orderID);
    		
    	if (is_null($order)) {
    		$request->session()->flash('updateError', 'The Order ID was not found. Please try again.');
    		return redirect()->route('restoreReceipt');
    	}
    	
    	$contact = contacts::find($order->contactID);
    		
    	if (is_null($contact)) {
    		$request->session()->flash('updateError', 'The Contact ID was not found. Please try again.');
    		return redirect()->route('restoreReceipt');
    	}
    	
    	$domainSiteConfig = domainSiteConfig::find($order->domainID);
    		
    	if (is_null($domainSiteConfig)) {
    		$request->session()->flash('updateError', 'The Domain Site Config ID was not found. Please try again.');
    		return redirect()->route('restoreReceipt');
    	}
    	
    	
    	// print receipt letter and return pdf file path
		$order->receiptLetterPath = PrintOrders::receiptLetterPDF($contact, $order, $domainSiteConfig);
	
		// print receipt thermal and return pdf file path
		$order->receiptThermalPath = PrintOrders::receiptThermalPDF($contact, $order, $domainSiteConfig);
		
		// print order letter and return pdf file path - kitchen
		$order->orderLetterPath = PrintOrders::orderLetterPDF($contact, $order, $domainSiteConfig);

		// print  order thermal and return pdf file path - kitchen
		$order->orderThermalPath = PrintOrders::orderThermalPDF($contact, $order, $domainSiteConfig);

		// update Order -  don't send customer email
		$sendMail = ProcessOrders::sendOrderEmail($contact, $order, $domainSiteConfig, false);
		$order->emailData = $sendMail['html'];

		// update order
		$o = $order->save();
		
		if ($o) {		
			$request->session()->flash('updateSuccess', 'Order ID: ' . $order->id . ' - The receipt was restored.');
		} else {
		    $request->session()->flash('updateError', 'There was an error retoring the receipt. Please try again.');
		}
    	
    	return redirect()->route('restoreReceipt');
    
    	} //end restore
    
    
}
