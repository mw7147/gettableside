<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Facades\App\hwct\SecureCard;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use App\neteviaDetail;
use App\ordersDetail;
use App\managerPIN;
use App\siteStyles;
use Carbon\Carbon;
use App\cartData;
use App\usStates;
use App\contacts;
use App\orders;
use App\domain;
use Config;

use GuzzleHttp\Client;

class aptitoController extends Controller {
    
	/*---------------- POST - viewcart submit payment  ----------------------------------*/

	public function orderPayment(Request $request) {
        // process payment - aptito - netevia - https://dashboard.netevia.com

        // dd($request);
        
        // check sessionID - make sure order does not exist with same sessionID
		$orderCheck = orders::where('sessionID', '=', $request->sid)->first();
		if (!empty($orderCheck)) { 
			session()->flash('cardError', 'An order has already been processed for this session. Please reset your cart and try again.');
			return redirect()->action('cartController@viewCart');
        }
        
        $userID = Auth::id();
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain['id'])->first();
		$styles = siteStyles::find($domain['id']);
        $numOnly = CellNumberData::numbersOnly($request->mobile);
        $ipAddress = \Request::ip();


		// update or create contact based on email and domainID
		$contact = contacts::updateOrCreate([
			'email' => $request->email,
			'domainID' => $domain['id']
			], [
			'fname' => $request->fname,
			'lname' => $request->lname,
			'userID' => $request->userID,
			'mobile' => $request->mobile,
			'unformattedMobile' => $numOnly,
			'address1' => $request->address1,
			'address2' => $request->address2,
			'city' => $request->city,
			'state' => $request->state,
			'zipCode' => $request->zipCode
        ]);

        if ($request->save == "yes" && $userID != null) {
            // save card data for userID
            if (isset($request->ccType)) { $type = $request->ccType; } else { $type = null; }
            
            $cc = new cardData();
                $cc->domainID = $domain->id;
                $cc->userID = $userID;
                $cc->lastFour = substr($request->number, -4);
                $cc->ccType = $type;
                if (env('APP_SAVE') == 'YES') { $cc->ccNumber = SecureCard::encryptNumber($request->number); }
                $cc->ccName = SecureCard::encryptData($request->number);
                $cc->ccExpiry = SecureCard::encryptData($request->expiry);
            $cc->save();
            
        }

        // charge the card
        $timestamp = time();
        $nonce = $this->getNonce();
        $requestURL = 'https://dashboard.netevia.com/api/transaction/pos/payment';

        $requestURI = '/api/transaction/pos/payment';
        if (isset($request->delivery)) { $delivery = $request->delivery; } else { $delivery = 0; }
        if (!is_null($request->address2)) { $address = $request->address1 . " " . $request->address2; } else { $address = $request->address1; }

        // set content array
        $content = [];
        $content['Card'] = $this->getCard($request->number, $request->name, $request->expiry, $request->cvc, null );
        $content['Order'] = $this->getOrder($nonce, $request->total, 'USD', $domainPaymentConfig->accountName);
        $content['Customer'] = $this-> getCustomer($request->fname, $request->lname, $request->email, $ipAddress, $request->mobile);
        $content['ShoppingCard'] = $this->getShoppingCard($request->subTotal, $request->tax, $request->tip, $delivery);
        $content['ShippingAddress'] = $this->getShippingAddress('US', $address, $request->zipCode, $request->state, $request->city);
        $content['AutoCapture'] = $this->getAutoCapture();
        $jsonContent = json_encode($content);
        // sdd($jsonContent);

        // get hmac authorization header
        $authorizationHeader = $this->authorizationHeader(env('APP_ID'), env('API_KEY'), $requestURL, $nonce, $timestamp, 'POST', $jsonContent);

        // request card validation
        $client = new Client();
        $res = $client->request('POST', $requestURL, [ 
            'body' => $jsonContent,
            'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => $authorizationHeader,
            'Content-Encoding' => 'utf-8'
            ]
         ]
        );
 
        // check response from gateway
        $statusCode = $res->getStatusCode();
        if ($statusCode != 200) { abort(504); }

        // get response body
        $res = $res->getBody();
        $charge = json_decode($res);

        // charge not approved
        if ( $charge->Result != 'PreAuthorized') {
            
            // return to cart - display message
            session()->flash('cardError', 'Charge Declined - ' . $charge->Message);
            Log::info('Charge Declined - ' . $request->fname . ' ' . $request->lname . ' - contact ID: ' . $contact->id . ' - Netevia ID: ' . $charge->Id . ' - ' .  $charge->Message);
            return redirect()->action('cartController@viewCart');
        } // end if not approved

        // abort if no transaction ID
        if (!isset($charge->Id)) { abort(504); }

        // charge approved with transaction ID - create transaction and order

		// create new order
		$order = new orders;
			$order->domainID = $domain['id'];
			$order->ownerID = $domain['ownerID'];
			$order->contactID = $contact->id;
			$order->userID = $contact->userID;
			$order->sessionID = $request->sid;
			$order->fname = $contact->fname;
			$order->lname = $contact->lname;
			$order->unformattedMobile = $numOnly;
            $order->customerEmailAddress = $request->email;
			$order->customerMobile = $request->mobile;
			$order->subTotal = $request->subTotal;
			$order->tax = $request->tax;
			$order->tip = $request->tip;
			$order->deliveryCharge = 0; // holder for delivery charge
			$order->total = $request->total;
            $order->ccType = $domainPaymentConfig->ccType;
			$order->ccTypeTransactionID = $charge->Id; // netevia Id
            $order->ccTypeCustomerID = 'unknown'; // card processor stored customer ID
            $order->ccTypeCardID = 'unknown'; // stored card ID with card processor
            $order->ccTypeFunding = $charge->Result; // result from netevia gateway
            $order->ccTypeLast4 = substr($request->number, -4);
            $order->ccTypeTransactionFee = 0;
            $order->paymentType = $request->ccType;
            $order->orderType = 'pickup'; // add delivery and in restaurant options
            $order->waiterID = 0; // add waiters
            $order->tableID = 0; // add tables
            $order->userAgent = $request->userAgent;
            $order->orderIP = \Request::ip();
            $order->restaurantEmailAddress = $domainSiteConfig->sendEmail;
            $order->orderInProcess = 1; // set order in process
            $order->orderRefundAmount = 0; // set refund to 0
		$order->save();

		$details = cartData::where('sessionID', '=', $request->sid)->get();
		foreach ($details as $detail) {
			$od = new ordersDetail;
				$od->domainID = $detail->domainID;
				$od->ownerID = $detail->ownerID;
				$od->contactID = $contact->id;
				$od->sessionID = $detail->sessionID;
				$od->menuDataID = $detail->menuDataID;
				$od->menuItem = $detail->menuItem;
				$od->menuDescription = $detail->menuDescription;
                $od->portion = $detail->portion;
                $od->printOrder = $detail->printOrder;
				$od->printReceipt = $detail->printReceipt;
				$od->price = $detail->price;
				$od->menuOptionsDataID = $detail->menuOptionsDataID;
				$od->menuAddOnsDataID = $detail->menuAddOnsDataID;
				$od->menuSidesDataID = $detail->menuSidesDataID;
				$od->instructions = $detail->instructions;
				$od->orderIP = $detail->orderIP;
                $od->tipAmount = $detail->tipAmount;
                $od->discountPercent = $request->orderDiscountPercent ?? 0;


			$od->save();
			cartData::destroy($detail->id);
		} // end foreach
        

		// save netevia repsonse detail
		$nd = new neteviaDetail;
		    $nd->domainID = $domain['id'];
		    $nd->ownerID = $domain['ownerID'];
            $nd->contactID = $contact->id;
            $nd->sessionID = $request->sid;
            $nd->orderID = $order->id;
            $nd->transactionID = $nonce;
            $nd->neteviaID = $charge->Id;
            $nd->ccName = $request->name;
            $nd->ccLastFour = substr($request->number, -4);
            $nd->createdDate = $charge->CreatedDate;
            $nd->operation = $charge->Operation;
            $nd->message = $charge->Message;
            $nd->code = $charge->Code;
            $nd->authorizationCode = $charge->AuthorizationCode;
            $nd->processorTransactionID = $charge->ProcessorTransactionId;
            $nd->amount = $charge->Amount;
            $nd->currency = $nd->currency;
            $nd->paymentAmount = $charge->PaymentAmount;
            $nd->paymentCurrency = $charge->PaymentCurrency;
            if (isset($charge->RecurrentToken)) { $nd->recurrentToken = $charge->RecurrentToken; }

        $nd->save();

		//create PDF - create pdf and return pdf file path
		$pdf = ProcessOrders::createPDF($contact, $order, $domainSiteConfig);

		// send customer email
		$sendMail = ProcessOrders::sendOrderEmail($contact, $order, $domainSiteConfig);

		$order->emailData = $sendMail['html'];
		$order->pdfPath = $pdf;
		$order->save();	

		// send to hp ePrint service if subscribed
		if (!empty($domainSiteConfig->hpEprint)) { $sendPDF = ProcessOrders::sendPDF($contact, $order, $domainSiteConfig); }

		session()->regenerate();
	    $message['orderAlert'] = $domainSiteConfig->orderAlert;
	    $message['orderID'] = $order->sessionID;
	    $message['numSent'] = $sendMail['numSent'];
		
	    return view('orders.viewOrder')->with([ 'html' => $order->emailData ]);

        

    } // end orderPayment
    

/*---------------- Authorization Header  ----------------------------------*/

    public function authorizationHeaderold($requestURI, $nonce, $timestamp, $method, $content) {
        
        if (empty($content)) {
            $content = null;
        } else {
            $contentMD5 = md5($content);
            $content = base64_encode($contentMD5);
        }

        $appID = env('APP_ID');
        $hmacSHA256Hash = $this->computeHMACSHA256Hash($requestURI, $method, $timestamp, $nonce, $content);

        //amx ${appId}:${requestSignatureBase64String}:${nonce}:${requestTimeStamp};
        $authHeader = 'hwct ' . $appID . ':' . $hmacSHA256Hash . ':' . $nonce . ':' . $timestamp; 
        return $authHeader;

    } // end authorizationHeader


    /*---------------- Authorization Header  ----------------------------------*/

    public function authorizationHeader($appID, $apiKey, $requestURL, $nonce, $timestamp, $method, $content) {
        if (empty($content)) {
            $content = null;
        } else {
            $contentMD5 = md5($content, true);
            $content = base64_encode($contentMD5);
        }

        $hmacSHA256Hash = $this->computeHMACSHA256Hash($appID, $apiKey, $requestURL, $method, $timestamp, $nonce, $content);

        $authHeader = 'amx ' . $appID . ':' . $hmacSHA256Hash . ':' . $nonce . ':' . $timestamp; 
		
        return $authHeader;

    } // end authorizationHeader




    /*---------------- computeHMACSHA256Hash  ----------------------------------*/
    
    public function computeHMACSHA256Hash($appID, $apiKey, $requestURL, $method, $timestamp, $nonce, $content) {

        $signatureRawData = $appID . $method . $requestURL . $timestamp . $nonce . $content;
        $signature = utf8_encode($signatureRawData);
        $hmac = hash_hmac('sha256', $signature, $apiKey, true);
        $hmacSHA256Hash = base64_encode($hmac);

    return $hmacSHA256Hash;

    } // end computeHMACSHA256Hash



    /*---------------- get Nonce  ----------------------------------*/
    public function getNonce() {
        
        $nonce = uniqid("", true);
        $nonce = str_replace('.', '', $nonce);
        // need to check against transactions
        return $nonce;

    } // end getNonce





    /*---------------- get getCard  ----------------------------------*/
    public function getCard($number, $name, $expiry, $cvv, $issuer) {

        $card = [];
        $expiration = explode('/', $expiry);
        $card['number'] = $number;
        $card['expirationMonth'] = trim($expiration[0]);
        $card['expirationYear'] = trim($expiration[1]);       
        $card['cardholder'] = $name;
        $card['cvv'] = $cvv;
        $card['issuer'] = $issuer;

        return $card;

    } // end getCard


     /*---------------- get getOrder  ----------------------------------*/
     public function getOrder($nonce, $amount, $currency, $description) {
        
        $order = [];
        $order['orderId'] = $nonce;
        $order['amount'] = $amount;
        $order['currency'] = $currency;       
        $order['description'] = $description;

        return $order;

    } // end getOrder


    /*---------------- get getCustomer  ----------------------------------*/
    public function getCustomer($fname, $lname, $email, $ipAddress, $phone) {
    
        $customer = [];
        $customer['firstName'] = $fname;
        $customer['lastName'] = $lname;
        $customer['email'] = $email;       
        $customer['ipAddress'] = $ipAddress;
        $customer['phone'] = "+1 " . $phone;
        
        return $customer;

    } // end getCustomer



    /*---------------- getShoppingCard  ----------------------------------*/
    public function getShoppingCard($subTotal, $tax, $tip, $delivery) {
        
        $shoppingCard = [];


        $shoppingCard['items'][0]['amount'] = $subTotal;
        $shoppingCard['items'][0]['quantity'] = 1;
        $shoppingCard['items'][0]['description'] = 'Sub-Total';
        $shoppingCard['items'][1]['amount'] = $tax;
        $shoppingCard['items'][1]['quantity'] = 1;
        $shoppingCard['items'][1]['description'] = 'Tax';
        if ($tip > 0) {
        $shoppingCard['items'][2]['amount'] = $tip;
        $shoppingCard['items'][2]['quantity'] = 1;
        $shoppingCard['items'][2]['description'] = 'Tip';
        }
        if ($delivery>0) {
        $shoppingCard['items'][3]['amount'] = $delivery;
        $shoppingCard['items'][3]['quantity'] = 1;
        $shoppingCard['items'][3]['description'] = 'Delivery';
        }
        
        return $shoppingCard;

    } // end getShoppingCard



    /*---------------- getShippingAddress  ----------------------------------*/
    public function getShippingAddress($country, $streetAddress, $zipCode, $state, $city) {
        
        $shippingAddress = [];
        $shippingAddress['country'] = $country;
        $shippingAddress['streetAddress'] = $streetAddress;
        $shippingAddress['zipCode'] = $zipCode;       
        $shippingAddress['state'] = $state;
        $shippingAddress['city'] = $city;
        
        return $shippingAddress;

    } // end shippingAddress

    /*---------------- getAutoCapture  ----------------------------------*/
    public function getAutoCapture() {
        
        $autoCapture = false;
    
        return $autoCapture;

    } // end getAutoCapture






    public function getRequest(){
        $request = '{
            "Card":{"Number":"4111111111111111","ExpirationMonth":10,"ExpirationYear":2020,"CVV":"123","Cardholder":"JOHN SMITH","Issuer":"Bank of America"},
            "Order":{"OrderId":"Order #12353800","Amount":20.00,"Currency":"USD","Description":"Amazing order"},
            "Customer":{"FirstName":"John","LastName":"Smith","Email":"j.shith@example.com","IPAddress":"127.0.0.1","Phone":"+1 555 1270001"},
            "ShoppingCard":{"Items":[{"Description":"Hello Kitty T-shirt","Quantity":1.0,"Amount":19.85},{"Description":"Bag","Quantity":1.0,"Amount":0.15}]},
            "BillingAddress":{"Country":"US","StreetAddress":"1st Avenue, 13","ZipCode":"22222","State":"CA","City":"Los Angeles"},
            "ShippingAddress":{"Country":"US","StreetAddress":"1st Avenue, 13","ZipCode":"22222","State":"CA","City":"Los Angeles"},
            "AutoCapture":false}';

            return $request;
    }

/*
    {
    "Card":{"number":"4111 1111 1111 1111","expirationMonth":"11","expirationYear":"22","cardholder":"Mark Wood","cvv":"123","issuer":null},
    "Order":{"orderId":"5a78b51cd9256248337558","amount":"15.55","currency":"USD","description":"Bubbas Restaurant"},
    "Customer":{"firstName":"Mark","lastName":"Wood","email":"mark@markmissy.com","ipAddress":"127.0.0.1","phone":"+1 (205) 301-0265"},
    "ShoppingCard":{"items":[{"amount":"12.49","quantity":1,"description":"Sub-Total"},{"amount":"1.03","quantity":1,"description":"Tax"},{"amount":"2.03","quantity":1,"description":"Tip"}]},
    "ShippingAddress":{"country":"US","streetAddress":"504 Main Street, Suite 111, Suite 111 Suite 111","zipCode":"76262","state":null,"city":"Roanoke"},
    "AutoCapture":false}
*/



} // end aptitoController
