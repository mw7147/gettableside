<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Facades\App\hwct\PrintOrders;
use App\suspendOperationHistory;
use Illuminate\Http\Request;
use App\bounceGroupMembers;
use App\menuCategoriesData;
use App\emailsSentDetails;
use App\textsSentDetails;
use App\domainSiteConfig;
use App\bounceContacts;
use App\textTemplates;
use App\Http\Requests;
use App\emailTemplate;
use App\emailTracker;
use App\ordersDetail;
use App\groupMember;
use App\linkClicks;
use App\emailsSent;
use App\textsSent;
use App\groupName;
use Carbon\Carbon;
use App\contacts;
use App\printers;
use App\bounces;
use App\orders;
use App\roles;
use App\User;


class reportsController extends Controller {

 /*------------------------- Reports Dashboard ------------------------------------------------------------------*/   
    

  public function dashboard(Request $request, $urlPrefix) {

    $authLevel = \Request::get('authLevel');
    if ($authLevel <  30) { abort(404); }

    $breadcrumb = ['reports', ''];
    $domain = \Request::get('domain');

  if ( $urlPrefix == 'manager') {
    	return view('reports.managerReports')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
	} elseif ($urlPrefix == 'owner') {
    return view('reports.ownerReports')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
  } elseif ($urlPrefix == 'admin') {
		return view('reports.adminReports')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
	} else {
		abort(404);
	}
  
  } // end dashboard
 

	/*-------------------- Sales Reports  ----------------------------------------*/

	public function salesReport(Request $request) {

    $domain = \Request::get('domain');
    $breadcrumb = ['reports', ''];
    
    // make sure can only see reports for user domain
    $user = Auth::user();
    if ($user->domainID != $domain->id) { abort(403); }

    // set dateRange
    if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = ''; } else { $dateRange = $request->dateRange; }
   
    if (is_null($dateRange)) {
      // specific dates
      $dates = $this->aggregateSpecificRange($request->specificRange, $domain->id);
      $dateRange='';
    } else {
      // quick range
      $dates = $this->aggregateDateRange($dateRange, $domain->id);
    }

    $orders = orders::select('orders.id', DB::raw('DATE(orders.orderReadyTime) as orderDate'),  DB::raw('COUNT(orders.domainID) as totalOrders'), DB::raw('SUM(orders.orderRefunded) as refunded'),  DB::raw('SUM(orders.orderRefundAmount) as amountRefunded'), DB::raw('SUM(orders.subTotal) as subTotal'), DB::raw('SUM(orders.discountAmount) as totalDiscounts'),
        DB::raw('SUM(orders.tax) as totalTax'), DB::raw('SUM(orders.tip) as totalTip'), DB::raw('SUM(orders.deliveryCharge) as TotalDelivery'), DB::raw('SUM(orders.deliveryTaxAmount) as totalDeliveryTax'), DB::raw('SUM(orders.ccTypeTransactionFee) as totalCCFee') )
      ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])
      ->groupBy( 'orderDate' )
      ->orderBy('orderDate', 'desc')              
      ->get();

    // set specific range return
    if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
      $specificRange = $request->specificRange;
    } else {
      $specificRange='';
    }
    
    return view('reports.salesReport')->with(['domain' => $domain, 'orders' => $orders, 'breadcrumb' => $breadcrumb, 'dateRange' => $dateRange, 'specificRange' => $specificRange]);


  } // end salesReport




	/*-------------------- Admin Daily Sales Total  ----------------------------------------*/

	public function salesReportDaily(Request $request, $urlPrefix, $orderDate) {

    $domain = \Request::get('domain');
    $breadcrumb = ['reports', ''];
    
    // make sure can only see reports for user domain
    $user = Auth::user();
    if ($user->domainID != $domain->id) { abort(403); }

    // set dateRange
    if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = ''; } else { $dateRange = $request->dateRange; }

    $orders = orders::leftJoin('domains', 'orders.domainID', '=', 'domains.id')
      ->select('orders.id', DB::raw('DATE(orders.orderReadyTime) as orderDate'),  DB::raw('COUNT(orders.domainID) as totalOrders'), DB::raw('SUM(orders.orderRefunded) as refunded'),  DB::raw('SUM(orders.orderRefundAmount) as amountRefunded'), DB::raw('SUM(orders.subTotal) as subTotal'), DB::raw('SUM(orders.discountAmount) as totalDiscounts'),
        DB::raw('SUM(orders.tax) as totalTax'), DB::raw('SUM(orders.tip) as totalTip'), DB::raw('SUM(orders.deliveryCharge) as TotalDelivery'), DB::raw('SUM(orders.deliveryTaxAmount) as totalDeliveryTax'), DB::raw('SUM(orders.ccTypeTransactionFee) as totalCCFee'), 'domains.id as domainID', 'domains.parentDomainID', 'domains.name as restaurant' )
      ->where( DB::Raw('date(orders.orderReadyTime)'), '=', $orderDate )
      ->groupBy('orders.domainID', 'orderDate' )
      ->orderBy('orderDate', 'desc')              
      ->get();
   

    // set specific range return
    if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
      $specificRange = $request->specificRange;
    } else {
      $specificRange='';
    }
    
    return view('reports.salesReportDaily')->with(['domain' => $domain, 'orders' => $orders, 'breadcrumb' => $breadcrumb, 'dateRange' => $dateRange, 'specificRange' => $specificRange]);


  } // end salesReportDaily



  /*-------------------- Daily Sales Summary  ----------------------------------------*/

	public function dailySalesSummary(Request $request) {

    $domain = \Request::get('domain');
    $breadcrumb = ['reports', ''];
    
    // make sure can only see reports for user domain
    $user = Auth::user();
    if ($user->domainID != $domain->id) { abort(403); }

    // set dateRange
    if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = ''; } else { $dateRange = $request->dateRange; }
   
    if (is_null($dateRange)) {
      // specific dates
      $dates = $this->aggregateSpecificRange($request->specificRange, $domain->id);
      $dateRange='';
    } else {
      // quick range
      $dates = $this->aggregateDateRange($dateRange, $domain->id);
    }

    $orders = orders::select('orders.id', DB::raw('DATE(orders.orderReadyTime) as orderDate'),  DB::raw('COUNT(orders.domainID) as totalOrders'), DB::raw('SUM(orders.orderRefunded) as refunded'),  DB::raw('SUM(orders.orderRefundAmount) as amountRefunded'), DB::raw('SUM(orders.total) as subTotal'), DB::raw('SUM(orders.discountAmount) as totalDiscounts', 'orders.domainID'),
        DB::raw('SUM(orders.tax) as totalTax'), DB::raw('SUM(orders.tip) as totalTip'), DB::raw('SUM(orders.deliveryCharge) as TotalDelivery'), DB::raw('SUM(orders.deliveryTaxAmount) as totalDeliveryTax'), DB::raw('SUM(orders.ccTypeTransactionFee) as totalCCFee') )
      ->where('orders.domainID', '=', $domain->id)
      ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])
      ->groupBy( 'orderDate' )
      ->orderBy('orderDate', 'desc')              
      ->get();

    $total = orders::select(DB::raw('COUNT(orders.domainID) as totalOrders'), DB::raw('SUM(orders.orderRefunded) as refunded'),  DB::raw('SUM(orders.orderRefundAmount) as amountRefunded'), DB::raw('SUM(orders.total) as total'), DB::raw('SUM(orders.discountAmount) as totalDiscounts'),
        DB::raw('SUM(orders.tax) as totalTax'), DB::raw('SUM(orders.tip) as totalTip'), DB::raw('SUM(orders.deliveryCharge) as TotalDelivery'), DB::raw('SUM(orders.deliveryTaxAmount) as totalDeliveryTax'), DB::raw('SUM(orders.ccTypeTransactionFee) as totalCCFee') )
      ->where('orders.domainID', '=', $domain->id)
      ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])          
      ->first();

    // set specific range return
    if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
      $specificRange = $request->specificRange;
    } else {
      $specificRange='';
    }
    
    return view('reports.dailySalesSummary')->with(['domain' => $domain, 'orders' => $orders, 'total' => $total, 'breadcrumb' => $breadcrumb, 'dateRange' => $dateRange, 'specificRange' => $specificRange]);


  } // end dailySalesSummary



  /*-------------------- Sales Report Details  ----------------------------------------*/

  public function salesReportDetail($urlPrefix, $domainID, $dayOfYear) {

    $breadcrumb = ['reports', ''];    
    $domain = \Request::get('domain');
    
    // make sure can only see reports for user domain
    $user = Auth::user();
    if ($user->domainID != $domain->id) { abort(403); }

    $orderDate = Carbon::createFromFormat('Y-m-d', $dayOfYear)->toDateString();
    $rawSD = "DATE(orders.orderReadyTime) = '" . $orderDate . "'";
    
    $details = orders::leftJoin('domains', 'orders.domainID', '=', 'domains.id')
      ->select(DB::raw('DATE(orders.orderReadyTime) as orderDate'), 'orders.*' , 'domains.id as domainID', 'domains.parentDomainID', 'domains.name')
      ->where('orders.domainID', '=', $domainID)   
      ->whereRaw($rawSD)
      ->get();

    $orderTotal = orders::select(DB::raw('SUM(orders.subTotal) as subTotal'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->first();

    $orderDiscountAmount = orders::select(DB::raw('SUM(orders.discountAmount) as discountAmount'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->first();

    $orderTips = orders::select(DB::raw('SUM(orders.tip) as tips'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->first();

    $orderTax = orders::select(DB::raw('SUM(orders.tax) as tax'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->first();

    $orderDeliveryCharge = orders::select(DB::raw('SUM(orders.deliveryCharge) as deliveryCharge'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->first();
    
    $refundTotal = orders::select(DB::raw('SUM(orders.orderRefundAmount) as refundTotal'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->where('orders.orderRefunded', '=', 1)
    ->first(); 

    $creditCardTotal = orders::select(DB::raw('SUM(orders.total) as total'))
    ->where('orders.domainID', '=', $domainID)   
    ->whereRaw($rawSD)
    ->first();

    return view('reports.salesReportDetail')->with(['domain' => $domain, 'details' => $details, 'breadcrumb' => $breadcrumb, 'creditCardTotal' => $creditCardTotal->total, 'orderDate' => $orderDate, 'orderTotal' => $orderTotal->subTotal, 'orderDiscountAmount' => $orderDiscountAmount->discountAmount, 'orderTips' => $orderTips->tips, 'orderTax' => $orderTax->tax, 'orderDeliveryCharge' => $orderDeliveryCharge->deliveryCharge, 'refundTotal' => $refundTotal->refundTotal ]);
    
    
  } // end salesReportDetail




  /*-------------------- Order Report Detail  ----------------------------------------*/

  public function orderReportDetail($urlPrefix, $orderID) {
    
    $breadcrumb = ['reports', ''];    
    $domain = \Request::get('domain');
    
    $detail = orders::find($orderID);

    return view('reports.orderReportDetail')->with(['domain' => $domain, 'detail' => $detail, 'breadcrumb' => $breadcrumb]);
    
    
  } // end orderReportDetail



  /*-------------------------------------------------------------------------------------------*/  


  // Helper Functions
  // Get Customer ID from User ID
    public function getCID () {

      $user = Auth::user();
      $customerID = customer::where('userID', '=', $user->id)->pluck('id');
      if ($customerID[0] == '') {abort(404);}
      return $customerID[0];

    }




	/*-------------------- Menu Item Sales Reports - GET ----------------------------------------*/

	public function menuItemSales(Request $request) {

          
    // make sure can only see reports for user domain
    $domain = \Request::get('domain');
    $user = Auth::user();
    if ($user->domainID != $domain->id) { abort(403); }

    if (isset($request->dateRange)) {$dateRange = $request->dateRange;} else {$dateRange = 'today';}
    if (isset($request->category)) {$category = $request->category;} else {$category = '%';}

    $dates = $this->menuItemDates($dateRange);

    $totalItemsPurchased = ordersDetail::select('quantity')
        ->where('domainID', '=', $domain->id)
        ->where('menuCategoriesDataID', 'like', $category)
        ->whereRaw("DATE(created_at) BETWEEN '" . $dates['date1'] . "' AND'" . $dates['date2'] . "'")
        ->get()->sum('quantity');

    $totalCustomers = ordersDetail::select(DB::raw('distinct contactID'))
        ->where('domainID', '=', $domain->id)
        ->where('menuCategoriesDataID', 'like', $category)
        ->whereRaw("DATE(created_at) BETWEEN '" . $dates['date1'] . "' AND'" . $dates['date2'] . "'")
        ->get();

    $totalCustomers = $totalCustomers->count();

    $totalRevenue = ordersDetail::select('extendedPrice')
        ->where('domainID', '=', $domain->id)
        ->where('menuCategoriesDataID', 'like', $category)
        ->whereRaw("DATE(created_at) BETWEEN '" . $dates['date1'] . "' AND'" . $dates['date2'] . "'")
        ->get()->sum('extendedPrice');
        
    $totalDiscounts = ordersDetail::select(DB::raw('SUM(ROUND(price * discountPercent/100, 2)) as discount'))
        ->where('domainID', '=', $domain->id)
        ->where('menuCategoriesDataID', 'like', $category)
        ->whereRaw("DATE(created_at) BETWEEN '" . $dates['date1'] . "' AND'" . $dates['date2'] . "'")
        ->first(); 

    
    $items = ordersDetail::select( 'id',  'menuCategoriesDataID', DB::raw('menuItem as MenuItem'), DB::raw('SUM(quantity) AS Quantity'), DB::raw('SUM(extendedPrice) AS Revenue'), DB::raw('MAX(created_at) AS LastDate'), DB::raw('SUM(ROUND(ordersDetail.Price * ordersDetail.discountPercent/100, 2)) AS Discounts'), DB::raw('COUNT(DISTINCT(ordersDetail.contactID)) AS numCustomers') )
        ->where('domainID', '=', $domain->id)
        ->where('menuCategoriesDataID', 'like', $category)
        ->whereRaw("DATE(created_at) BETWEEN '" . $dates['date1'] . "' AND'" . $dates['date2'] . "'")
        ->groupBy('menuItem')
        ->orderBy('menuItem', 'asc')
        ->get();

    $categories = menuCategoriesData:: where('domainID', '=', $domain->id)->orderBy('sortOrder', 'asc')->get();
    $breadcrumb = ['reports', ''];

    return view('reports.menuItemSalesReport')->with(['domain' => $domain, 'items' => $items, 'breadcrumb' => $breadcrumb, 'category' => $category, 'dates' => $dates, 'categories' => $categories, 'totalItemsPurchased' => $totalItemsPurchased, 'totalCustomers' => $totalCustomers, 'totalRevenue' => $totalRevenue, 'totalDiscounts' => $totalDiscounts->discount ]);


  } // end menuItemSales





	/*-------------------- Menu Item Sales Reports - Post ----------------------------------------*/

	public function menuItemDates($range) {

    /*
    DATE RANGES:
    today
    yesterday
    thisWeek
    lastWeek
    thisMonth
    lastMonth
    last90Days
    */

    $dates = [];

    if ($range == 'tomorrow') {

      $dates['date1'] = Carbon::now()->addDay()->toDateString();
      $dates['date2'] = Carbon::now()->addDay()->toDateString();
      $dates['reportDates'] = Carbon::now()->addDay()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'tomorrow';

    } elseif ($range == 'yesterday') {

      $dates['date1'] = Carbon::now()->subDay()->toDateString();
      $dates['date2'] = Carbon::now()->subDay()->toDateString();
      $dates['reportDates'] = Carbon::now()->subDay()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'yesterday';


    } elseif ($range == 'thisWeek') {
      
      $dt = Carbon::now();
      $dates['date1'] = $dt->startOfWeek()->toDateString();
      $dates['date2'] = $dt->endOfWeek()->toDateString();
      $dates['reportDates'] = $dt->startOfWeek()->format('M j, Y') . ' to ' . $dt->endOfWeek()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'this week';

    } elseif ($range == 'lastWeek') {

      $dt = Carbon::now()->subWeek();
      $dates['date1'] = $dt->startOfWeek()->toDateString();
      $dates['date2'] = $dt->endOfWeek()->toDateString();
      $dates['reportDates'] = $dt->startOfWeek()->format('M j, Y') . ' to ' . $dt->endOfWeek()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'last week';

    } elseif ($range == 'thisMonth') {

      $dt = Carbon::now();
      $dates['date1'] = $dt->startOfMonth()->toDateString();
      $dates['date2'] = $dt->endOfMonth()->toDateString();
      $dates['reportDates'] = $dt->startOfMonth()->format('M j, Y') . ' to ' . $dt->endOfMonth()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'this month';

    } elseif ($range == 'lastMonth') {

      $dt = Carbon::now()->subMonth();
      $dates['date1'] = $dt->startOfMonth()->toDateString();
      $dates['date2'] = $dt->endOfMonth()->toDateString();
      $dates['reportDates'] = $dt->startOfMonth()->format('M j, Y') . ' to ' . $dt->endOfMonth()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'last month';

    } elseif ($range == 'last90Days') {

      $dt = Carbon::now();
      $dates['date1'] = Carbon::now()->subDays(90)->toDateString();
      $dates['date2'] = Carbon::now()->toDateString();
      $dates['reportDates'] = Carbon::now()->subDays(90)->format('M j, Y') . ' to ' . Carbon::now()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'last 90 days';

    } elseif ($range == 'thisYear') {

      $dt = Carbon::now();
      $dates['date1'] = $dt->startOfYear()->toDateString();
      $dates['date2'] = $dt->endOfYear()->toDateString();
      $dates['reportDates'] = $dt->startOfYear()->format('M j, Y') . ' to ' . $dt->endOfYear()->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'this year';

    } elseif ($range == 'allFuture') {

      $dt = Carbon::now();
      $dates['date1'] = $dt->now()->toDateTimeString();
      $dates['date2'] = $dt->addDays(7)->toDateTimeString();
      $dates['reportDates'] = $dt->format('M j, Y') . ' to ' . $dt->addDays(7)->format('M j, Y');
      $dates['range'] = $range;
      $dates['human'] = 'All Future Orders';

    } else {
      // return today
      $dt = Carbon::now()->toDateString();
      $reportDates = Carbon::now()->format('M j, Y');
      $dates['date1'] = $dt;
      $dates['date2'] = $dt;
      $dates['reportDates'] = $reportDates;
      $dates['range'] = 'today';
      $dates['human'] = 'today';
    }
    
    return $dates;
  } // end menuItemDates






/*-------------------- rePrint Receipt  ----------------------------------------*/

  public function rePrint(Request $request) {

    $order = orders::find($request->orderID);
    if ( is_null($order)) { return "There was as problem reprinting the order. Please try again."; }
    
    $domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();

    if ($request->type == 'order') {
      // order / kitchen
      $printer = printers::find($domainSiteConfig->printOrder);
      $reprint = PrintOrders::printOrder($printer, $order, $domainSiteConfig, 'reprint');

    } else {
      //receipt / customer
      $printer = printers::find($domainSiteConfig->printReceipt);
      $reprint = PrintOrders::printReceipt($printer, $order, $domainSiteConfig, 'reprint');

    }

    
    if ($reprint) { 
			$message = "The print request has been sent to the printer.";
		} else {
			$message = "There was a problem with the printer. Please check the printer and try again";
    }
    
    return $message;

  } // end rePrint



  /*-------------------- Suspension Report  ----------------------------------------*/

  public function suspensionReport(Request $request, $urlPrefix) {

    $domain = \Request::get('domain');
    $breadcrumb = ['reports', ''];
    
    // get suspension for last 90 days

    $suspensions = suspendOperationHistory::where('domainID', '=', $domain->id)->whereDate('startTime', '>=', Carbon::now()->subMonths(3)->toDateString())->orderBy('starttime', 'desc')->get();

		return view('reports.suspensionReport')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'suspensions' => $suspensions]);

  } // end suspensionReport




  /*-------------------- Item Aggregate  ----------------------------------------*/

  public function itemAggregate(Request $request, $urlPrefix) {

		$domain = \Request::get('domain');
    $authLevel = \Request::get('authLevel');
    
    // set dateRange
    if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = 'today'; } else { $dateRange = $request->dateRange; }
    if (is_null($dateRange)) {
      // specific dates
      $dates = $this->aggregateSpecificRange($request->specificRange, $domain->id);
    } else {
      // quick range
      $dates = $this->aggregateDateRange($dateRange, $domain->id);
    }

    $items = ordersDetail::select('menuDataID', 'menuItem', 'menuCategoriesData.categoryName', 'portion', DB::Raw('sum(ordersDetail.quantity) AS quantity'), DB::Raw('sum(ordersDetail.extendedPrice) AS extendedPrice'), DB::Raw('sum(ordersDetail.tax) AS tax'), DB::Raw('orders.orderReadyTime as readyTime'), DB::Raw('max(orders.orderReadyTime) as lastDate') )
      ->leftJoin('orders', 'ordersDetail.sessionID', '=', 'orders.sessionID')
      ->leftJoin('menuCategoriesData', 'ordersDetail.menuCategoriesDataID', '=', 'menuCategoriesData.id')
      ->where('ordersDetail.domainID', '=', $domain->id)
      ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])
      ->groupBy('ordersDetail.menuitem', 'ordersDetail.portion')
      ->orderBy('ordersDetail.menuItem')
      ->get();

    $dateRange = '';

    // set specific range return
		if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
			$specificRange = $request->specificRange;
		} else {
			$specificRange='';
    }
    
    $breadcrumb = ['reports', ''];
    
    return view('reports.itemAggregate')->with(['domain' => $domain, 'authLevel' => $authLevel, 'items' => $items, 'breadcrumb' => $breadcrumb, 'dateRange' => $request->dateRange, 'specificRange' => $specificRange ]);


  } // end itemAggregate



  /*-------------------- Category Aggregate  ----------------------------------------*/

  public function categoryAggregate(Request $request, $urlPrefix) {

    $domain = \Request::get('domain');
    $authLevel = \Request::get('authLevel');
    
    // set dateRange
    if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = 'today'; } else { $dateRange = $request->dateRange; }
    if (is_null($dateRange)) {
      // specific dates
      $dates = $this->aggregateSpecificRange($request->specificRange, $domain->id);
    } else {
      // quick range
      $dates = $this->aggregateDateRange($dateRange, $domain->id);
    }

    $items = ordersDetail::select('ordersDetail.menuCategoriesDataID', 'menuCategoriesData.categoryName', DB::Raw('sum(ordersDetail.quantity) AS quantity'), DB::Raw('sum(ordersDetail.extendedPrice) AS extendedPrice'), DB::Raw('sum(ordersDetail.tax) AS tax'), DB::Raw('orders.orderReadyTime as readyTime'), DB::Raw('max(orders.orderReadyTime) as lastDate') )
      ->leftJoin('orders', 'ordersDetail.sessionID', '=', 'orders.sessionID')
      ->leftJoin('menuCategoriesData', 'ordersDetail.menuCategoriesDataID', '=', 'menuCategoriesData.id')
      ->where('ordersDetail.domainID', '=', $domain->id)
      ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])
      ->groupBy('menuCategoriesData.categoryName')
      ->orderBy('menuCategoriesData.categoryName')
      ->get();
    $dateRange = '';

    // set specific range return
    if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
      $specificRange = $request->specificRange;
    } else {
      $specificRange='';
    }
    
    $breadcrumb = ['reports', ''];
    
    return view('reports.categoryAggregate')->with(['domain' => $domain, 'authLevel' => $authLevel, 'items' => $items, 'breadcrumb' => $breadcrumb, 'dateRange' => $request->dateRange, 'specificRange' => $specificRange ]);


  } // end categoryAggregate





    /*-------------------- Delivery Details  ----------------------------------------*/

    public function deliveryDetails(Request $request, $urlPrefix) {

      $domain = \Request::get('domain');
      $authLevel = \Request::get('authLevel');
      
      // set dateRange
      if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = 'today'; } else { $dateRange = $request->dateRange; }
      if (is_null($dateRange)) {
        // specific dates
        $dates = $this->aggregateSpecificRange($request->specificRange, $domain->id);
      } else {
        // quick range
        $dates = $this->aggregateDateRange($dateRange, $domain->id);
      }
  
      $items = orders::select( 'orders.id', 'orders.total', 'orders.orderRefundAmount', 'orders.ccTypeLast4', 'orders.receiptLetterPath', 'orders.receiptThermalPath', 'orders.contactID', 'orders.customerEmailAddress', 'orders.customerMobile', 'orders.created_at', 'orders.orderReadyTime', 'orders.fname', 'orders.lname' )
        ->where('orders.orderType', '=', 'deliver')
        ->where('orders.domainID', '=', $domain->id)
        ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])
        ->orderBy('orders.lname')
        ->get();

        $dateRange = '';
  
      // set specific range return
      if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
        $specificRange = $request->specificRange;
      } else {
        $specificRange='';
      }
      
      $breadcrumb = ['reports', ''];

      return view('reports.deliveryDetails')->with(['domain' => $domain, 'authLevel' => $authLevel, 'items' => $items, 'breadcrumb' => $breadcrumb, 'dateRange' => $request->dateRange, 'specificRange' => $specificRange ]);
  
  
    } // end deliveryDetails





    /*-------------------- Pickup Details  ----------------------------------------*/

    public function pickupDetails(Request $request, $urlPrefix) {

      $domain = \Request::get('domain');
      $authLevel = \Request::get('authLevel');
      
      // set dateRange
      if ( $_SERVER['REQUEST_METHOD'] == "GET" ) { $dateRange = 'today'; } else { $dateRange = $request->dateRange; }
      if (is_null($dateRange)) {
        // specific dates
        $dates = $this->aggregateSpecificRange($request->specificRange, $domain->id);
      } else {
        // quick range
        $dates = $this->aggregateDateRange($dateRange, $domain->id);
      }
  
      $items = orders::select( 'orders.id', 'orders.total', 'orders.orderRefundAmount', 'orders.ccTypeLast4', 'orders.receiptLetterPath', 'orders.receiptThermalPath', 'orders.contactID', 'orders.customerEmailAddress', 'orders.customerMobile', 'orders.created_at', 'orders.orderReadyTime', 'orders.fname', 'orders.lname' )
        ->where('orders.orderType', '=', 'pickup')
        ->where('orders.domainID', '=', $domain->id)
        ->whereBetween( DB::Raw('date(orders.orderReadyTime)'), [ $dates[1], $dates[2] ])
        ->orderBy('orders.lname')
        ->get();

        $dateRange = '';
  
      // set specific range return
      if ( !is_null($request->specificRange) || !is_null($request->dateRage)) {
        $specificRange = $request->specificRange;
      } else {
        $specificRange='';
      }
      
      $breadcrumb = ['reports', ''];

      return view('reports.pickupDetails')->with(['domain' => $domain, 'authLevel' => $authLevel, 'items' => $items, 'breadcrumb' => $breadcrumb, 'dateRange' => $request->dateRange, 'specificRange' => $specificRange ]);
  
  
    } // end pickupDetails




  



   /*-------------------- Aggregate Date Range  ----------------------------------------*/

   public function aggregateDateRange($dateRange, $domainID) {

    $dates = [];
    Carbon::setWeekStartsAt(Carbon::SUNDAY);
    Carbon::setWeekEndsAt(Carbon::SATURDAY);

		switch ($dateRange) {
      case "today":
        $d = Carbon::now();
        $dates[1] = $d->toDateString();
        $dates[2] = $dates[1];
      break;

      case "yesterday":
        $d = Carbon::now()->subDay();
        $yesterday = $d->toDateString();
        $dates[1] = $yesterday;
        $dates[2] = $dates[1];
      break;

      case "tomorrow":
        $d = Carbon::now()->addDay();
        $tomorrow = $d->toDateString();
        $dates[1] = $tomorrow;
        $dates[2] = $dates[1];
      break;

      case "week":
        $dates[1] = Carbon::now()->startOfWeek()->toDateString();
        $dates[2] = Carbon::now()->endOfWeek()->toDateString();				
      break;

      case "lastWeek":
        $lastWeekDay = Carbon::now()->subDays(7);
        $dates[1] = $lastWeekDay->startOfWeek()->toDateString();
        $dates[2] = $lastWeekDay->endOfWeek()->toDateString();
      break;

      case "month":
        $d = Carbon::now();
        $dates[1] = $d->startOfMonth()->toDateString(); // start of month
        $dates[2] = $d->endOfMonth()->toDateString(); // end of month
      break;

			case "lastMonth":
				$d = Carbon::now()->subMonth();
				$dates[1] = $d->startOfMonth()->toDateString(); // start of month
				$dates[2] = $d->endOfMonth()->toDateString(); // end of month
			break;

      case "allFuture":
        $d = Carbon::now();
        $d2 = Carbon::now()->addDays(8);
        $dates[1] = $d->toDateTimeString();
        $dates[2] = $d2->toDateTimeString();
      break;
				
		  case "all":
        $d = Carbon::now();
        $dates[1] = '2020-3-01'; // year started
        $dates[2] = $d->toDateString();
		  break;

      default:
        // today only
        $d = Carbon::now();
        $dates[1] = $d->toDateString();
        $dates[2] = $dates[1];
		        
		} // end switch
    
    return $dates;

	} // end aggregateDateRange




	/*-------------------- Aggregate Specific Range  ----------------------------------------*/

	public function aggregateSpecificRange($specificRange, $domainID) {
    
    $dates = [];
		$days = explode(" - ", $specificRange);

		$dates[1] = Carbon::parse($days[0])->toDateString();
		$dates[2] = Carbon::parse($days[1])->toDateString();
		
		return $dates;

	} // end aggregateSpecificRange



  /*--------------------------------View Order HTML  ---------------------------------------------------*/   


  public function viewReceipt(Request $request, $urlPrefix, $orderID) {

    $domainID = \Request::get("domain")->id;
    $user = Auth::user();

    // only view owner receipts
    $orders = orders::where('id', '=', $orderID)->where('domainID', '=', $domainID)->first();
    if (empty($orders)) { abort(404); }


    return view('reports.viewReceipt')->with([ 'html' => $orders->emailData ]);

  } // end viewReceipt


      
} // end reportsController
