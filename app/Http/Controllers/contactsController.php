<?php

namespace App\Http\Controllers;

use Facades\App\hwct\CellNumberData;
use Facades\App\hwct\MultipleSites;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\groupMember;
use App\marketing;
use App\groupName;
use Carbon\Carbon;
use App\countries;
use App\contacts;
use App\usStates;
use App\domain;
use App\roles;


class contactsController extends Controller
{

  /*---------------- Contact View  ----------------------------------*/

  
    public function view() {
  
      $breadcrumb = ['contacts', 'view'];
      $domain = \Request::get('domain');
      $authLevel = \Request::get('authLevel');
      
      // make sure correct domain
      if ($authLevel >= 40) {
          $contacts = contacts::leftJoin('domains', 'contacts.domainID', '=', 'domains.id')
          ->select('contacts.*', 'domains.parentDomainID', 'domains.name as location')
          ->get();  
      } elseif ($authLevel >= 35 && $authLevel < 40) {
          $contacts = contacts::leftJoin('domains', 'contacts.domainID', '=', 'domains.id')
          ->select('contacts.*', 'domains.parentDomainID', 'domains.name as location')
          ->where('domains.parentDomainID', '=', $domain->id)            
          ->get();
      } else {
          $contacts = contacts::where('domainID', '=', $domain->id)->get();
      }

      return view('contacts.view')->with(['domain' => $domain, 'contacts' => $contacts, 'breadcrumb' => $breadcrumb]);
    
    }  // end ownerView

    
  /*---------------- Contacts Owner View  ----------------------------------*/

	
    public function ownerView() {

		$user = Auth::user();
      $breadcrumb = ['contacts', 'view'];
      $domain = \Request::get('domain');
      // get all contacts for all domains of user (owner - level 35)
      $contacts = contacts::where('tgmDomainID', '=', $domain->id)->get();

      return view('contacts.view')->with(['domain' => $domain, 'contacts' => $contacts, 'breadcrumb' => $breadcrumb]);
    
    }  // end ownerView

  /*------------------------------------- New Contact ------------------------------------------------------*/   

  public function newContact(Request $request) {

    $usStates = usStates::select('stateCode', 'stateName')->get();
    $authLevel = \Request::get('authLevel');
    $breadcrumb = ['contacts', 'new'];
    $domain = \Request::get('domain');

    $locations = MultipleSites::getDomains($domain->id, $authLevel);

    return view('contacts.new')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb, 'states' => $usStates, 'locations' => $locations ]);
    
  
  } // end newContact



  /*------------------------------------- New Contact Save------------------------------------------------------*/   

  public function newContactSave(Request $request, $urlPrefix) {

    $numOnly = CellNumberData::numbersOnly($request->mobile);

    // get data for marketing system
    $marketing = marketing::findOrFail($request->domainID);

    // set dates
    if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
    if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }

    $c = new contacts;
      $c->tgmDomainID = $request->domainID;
      $c->tgmLocationID = $request->locationID;
      $c->domainID = $marketing->mktDomainID;
      $c->customerID = $marketing->mktCustomerID;
      $c->userID = $marketing->mktUserID;      
      $c->repID = $marketing->mktRepID;
      $c->email = $request->email;
      $c->zip = $request->zipCode;
      $c->mobile = $request->mobile;
      $c->unformattedMobile = $numOnly;
      $c->fname = $request->fname;
      $c->lname = $request->lname;
      $c->birthday = $bd;
      $c->anniversary = $an;
      $c->address1 = $request->address1;
      $c->address2 = $request->address2;
      $c->city = $request->city;
      $c->state = $request->state;
      $c->company = $request->companyName;
      $c->title = $request->title;
    $cs = $c->save();
    
    if (!$cs) {
      $request->session()->flash('updateError', 'There was an error creating the contact. Please try again.'); 
    } else {
      $request->session()->flash('updateSuccess', 'Contact Name: '. $c->fname . ' ' . $c->lname . ' was successfully created!');
    }

    return redirect()->route('contactEdit', ['urlPrefix' => $urlPrefix, 'contactID' => $c->id]);        
  
  } // end newContactSave



 /*----------------------------Delete Contact---------------------------------------------------------------*/   

  public function contactDelete(Request $request, $urlPrefix, $contactID) {
   
    $authLevel = \Request::get('authLevel');
    $domain = \Request::get('domain');    

    // make sure correct domain
    if ($authLevel >= 40) {
      $contact = contacts::where(['id', '=', $contactID])->first();
    } else {
      $contact = contacts::where([['id', '=', $contactID], ['tgmDomainID', '=', $domain->id]])->first();
    }

    if (is_null($contact)) { abort(404);}    
    $c = contacts::destroy($contactID);

    if (!$c) {
      $request->session()->flash('deleteError', 'There was an error deleting the contact. Please try again.'); 
    } else {
      $request->session()->flash('deleteSuccess', 'The contact was successfully deleted!');
    }

    return redirect()->back();
    
  } // end contactDelete



  /*------------------------------------- Edit Contact------------------------------------------------------*/   

  public function contactEdit($urlPrefix, $contactID) {

    $usStates = usStates::select('stateCode', 'stateName')->get();
    $breadcrumb = ['contacts', 'view'];
    $domain = \Request::get('domain');
    $authLevel = \Request::get('authLevel');    

    $locations = MultipleSites::getDomains($domain->id, $authLevel);

    // make sure correct domain
    $contact = contacts::where([['id', '=', $contactID], ['tgmDomainID', '=', $domain->id]])->first();

    if (is_null($contact)) { abort(404);}

    return view('contacts.edit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb, 'states' => $usStates, 'locations' => $locations, 'contact' => $contact ]);

  
  } // end contactEdit



  /*------------------------------------- Edit Contact Save ------------------------------------------------------*/   

  public function contactEditSave(Request $request, $urlPrefix, $contactID) {

    $authLevel = \Request::get('authLevel');
    $domain = \Request::get('domain');    
    $numOnly = CellNumberData::numbersOnly($request->mobile);

    // id and domainID -> make sure authorized domain accessing contact
    $contact = contacts::where([['id', '=', $contactID], ['tgmDomainID', '=', $domain->id]])->firstOrFail();

    // get data for marketing system
    $marketing = marketing::findOrFail($request->domainID);

    // set dates
    if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
    if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }

   // if (is_null($contact)) { abort(404);}

      $contact->tgmDomainID = $request->domainID;
      $contact->domainID = $marketing->mktDomainID;
      $contact->customerID = $marketing->mktCustomerID;
      $contact->userID = $marketing->mktUserID;      
      $contact->repID = $marketing->mktRepID;
      $contact->zip = $request->zipCode;
      $contact->email = $request->email;
      $contact->mobile = $request->mobile;
      $contact->unformattedMobile = $numOnly;
      $contact->fname = $request->fname;
      $contact->lname = $request->lname;
      $contact->anniversary = $an;
      $contact->birthDay = $bd;
      $contact->address1 = $request->address1;
      $contact->address2 = $request->address2;
      $contact->city = $request->city;
      $contact->state = $request->state;
      $contact->company = $request->companyName;
      $contact->title = $request->title;
    $check = $contact->save();

    if ($check) {
      $request->session()->flash('updateSuccess', 'Contact '. $contact->fname . ' ' . $contact->lname . ' was successfully updated!');
    } else {
      $request->session()->flash('updateError', 'There was an error updating the contact. Please try again.'); 
    }

    return redirect()->route('contactEdit', ['urlPrefix' => $urlPrefix, 'contactID' => $contactID]);        
    
  
  } // end contactEditSave


} // end contactsController
