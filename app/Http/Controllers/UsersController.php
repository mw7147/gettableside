<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\CellNumberData;
use Illuminate\Http\Request;
use App\registrationEmails;
use App\domainSiteConfig;
use App\siteStyles;
use App\marketing;
use App\usersData;
use Carbon\Carbon;
use App\siteData;
use App\cartData;
use App\usStates;
use App\contacts;
use App\Orders;
use App\User;
use App\UserCard;
use App\Repositories\StationRepository;
class UsersController extends Controller
{
    

/*---------------------User Login  ----------------------------------------*/

   public function userLogin(Request $request)
    {

    $domain = \Request::get('domain');
    $data = domainSiteConfig::where('domainID', '=', $domain->id)->first();
    
    return view('auth.login')->with(['domain' => $domain, 'data' => $data]);


    }


/*--------------------- Process Login  ----------------------------------------*/

   public function userLoginDo(Request $request) {

        // login generates new sessionID - update cart with new sessionID and userID
        $domain = \Request::get('domain');
        $oldSessionID = session()->getId();

        $checkSocialUser = User::where('email',$request->email)->select('type','source')->first();
        if($checkSocialUser){
            if($checkSocialUser->type == 'social'){
               return '<h3 style="color: red">Please login with your '. $checkSocialUser->source .' account </h3>';
            }
        }
        
        // login user
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'domainID' => $domain->id ])) {

            // route to proper auth level controller
            $user = Auth::user();
            /** Station User Login */

            /*$station = new StationRepository;
            $data = array(
                'email' => $request->email, 
                // 'password' => $request->password, 
                'password' => 'p@ssword',
                'userId' => $user->id,
                'domainId' => $domain->parentDomainID
            );
            $response = $station->userLogin($data);
            session()->put('access_token',$response->data->token);
*/
            if (intval($user->role_auth_level) == 20) {
                return redirect()->action('staffController@dashboard')->with(['domain' => $domain]);    
           
            } elseif ($user->role_auth_level == 30) {
                return redirect()->action('managerController@dashboard')->with(['domain' => $domain]);    

            } elseif ($user->role_auth_level == 35) {
                return redirect()->action('ownerController@dashboard')->with(['domain' => $domain]);    
            
            } elseif ($user->role_auth_level == 40) {
                return redirect()->action('hwctController@dashboard')->with(['domain' => $domain]);    
            
            } elseif ($user->role_auth_level == 50) {

                return redirect()->action('adminController@dashboard')->with(['domain' => $domain]);       
            } else {
                 // customer
                $data = siteData::find($domain->id);
                $cartData = cartData::where( 'sessionID', '=', $oldSessionID )->count();
                $newSessionID = session()->getId();

                if ($cartData == 0) {
                    // return to dashboard
                    return redirect()->action('customerController@dashboard');
                } else {    
                    //update cartData and return to viewCart
                    cartData::where('sessionID', '=', $oldSessionID)->update([ 'sessionID' => $newSessionID, 'userID' => $user->id ]);
                    if ($domain->type != 9) {
                        return redirect('/viewcart');
                    } else {
                        return redirect('/dineincart');
                    }
                } // end if cartdata

            } // end role_auth_level
        } // end if auth attempt

        // auth failed
        abort(410);

    } // end userLoginDo

/*---------------------User Logout  ----------------------------------------*/

   public function userLogout(Request $request)
    {

        Auth::logout();
        session()->flush();
    	$domain = \Request::get('domain');
        $data = siteData::find($domain->id);
        session()->regenerate();
        if ($domain->type != 9) {
            return redirect('/order');
        } else {
            return redirect('/dine');
        }

    }


/*--------------------- Process Login  ----------------------------------------*/

   public function cartLogin(Request $request)
    {
        // login generates new sessionID - update cart with new sessionID and userID
        $domain = \Request::get('domain');
        $oldSessionID = session()->getId();
        $checkSocialUser = User::where('email',$request->email)->select('type','source')->first();
        if($checkSocialUser){
            if($checkSocialUser->type == 'social'){
               return '<h3 style="color: red">Please login with your '. $checkSocialUser->source .' account </h3>';
            }
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'parentDomainID' => $domain->parentDomainID])) {

            // route to proper auth level controller
            $user = Auth::user();

            /** Station User Login */

            /*$station = new StationRepository;
            $data = array(
                'email' => $request->email, 
                // 'password' => $request->password, 
                'password' => 'p@ssword',
                'userId' => $user->id,
                'domainId' => $domain->parentDomainID
            );
            $response = $station->userLogin($data);
            session()->put('access_token',$response->data->token);
            */
            if (intval($user->role_auth_level) == 20) {
                return redirect()->action('staffController@dashboard')->with(['domain' => $domain]);    

            } elseif ($user->role_auth_level == 30) {
                return redirect()->action('managerController@dashboard')->with(['domain' => $domain]);    

            } elseif ($user->role_auth_level == 35) {
                return redirect()->action('ownerController@dashboard')->with(['domain' => $domain]);    
            
            } elseif ($user->role_auth_level == 40) {
                return redirect()->action('hwctController@dashboard')->with(['domain' => $domain]);    
            
            } elseif ($user->role_auth_level == 50) {
                return redirect()->action('adminController@dashboard')->with(['domain' => $domain]);       
            } else {

                // Authentication passed...
                $newSessionID = session()->getId();
                cartData::where('sessionID', '=', $oldSessionID)->update(['sessionID' => $newSessionID, 'userID' => Auth::id()]);
                session()->flash('loginSuccess', 'You have been successfully logged in.');

                if ($request->uri == 'order' || $request->uri == 'order?neworder=yes') {
                    return redirect('/order');
                } elseif ($request->uri == 'dine') {
                    return redirect('/dine');
                } elseif ($request->uri == 'dineincart') {
                    return redirect('/dineincart');
                } else {
                return redirect()->action('cartController@viewCart');
                }
            }
        }
 
        // Authentication Failed
        session()->flash('loginError', 'There was an error with your username or password. Please try again.');

        if ($request->uri == 'order' && $domain->type < 8) {
            return redirect('/order');
        } 
        
        if ($request->uri == 'dine' && $domain->type >= 8) {
            return redirect('/dine');
        }

        return redirect()->action('cartController@viewCart');
        

    }

    /*---------------------User Registration  ----------------------------------------*/

   public function userRegister() {

    $domain = request()->get('domain');
    $data = siteData::where('domainID', '=', $domain->id)->first();
    $states = usStates::all();
    if ($domain->type < 8) {
        $styles = siteStyles::where('domainID', '=', $domain->id)->first();
    } else {
        $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();
    }
    $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();

    if (isset($_SERVER['HTTP_REFERER'])) {
        $referer = $_SERVER['HTTP_REFERER'];
        $referer = parse_url($referer);
        $referer = $referer['path'];
    } elseif ($domain->type >= 8) { 
        $referer = '/dine';
    } else {
        $referer = '/order';
    }


    return view('customer.register')->with(['domain' => $domain, 'data' => $data, 'states' => $states, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles, 'pageName' => 'User Registration', 'referer' => $referer ]);


    }



    /*---------------------User Registration Completion  ----------------------------------------*/

   public function userRegisterDo(Request $request)
    {

        $domain = \Request::get('domain');

        // get data for marketing system
        $marketing = marketing::findOrFail($domain->parentDomainID);

        // set dates
        if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
        if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }


        $numOnly = CellNumberData::numbersOnly($request->mobile);

        // update or create contact based on email and domainID
        $newUser = User::updateOrCreate([
            'email' => $request->email,
            'parentDomainID' => $domain->parentDomainID
            ], [
            'domainID' => $domain->id,
            'role_auth_level' => '10',
            'password' => Hash::make($request->password1),
            'active' => 'yes',
            'fname' => $request->fname,
            'lname' => $request->lname,
            'mobile' => $request->mobile,
            'unformattedMobile' => $numOnly,
            'email' => $request->email,
            'countryCode' => 'US',
            'zipCode' => $request->zipCode
        ]);
                
                
        $newUsersData = usersData::updateOrCreate([
            'usersID' => $newUser->id,
            ], [
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state
        ]);

        // update or create contact based on email and domainID
        $contact = contacts::updateOrCreate([
            'email' => $request->email,
            'tgmDomainID' => $domain->parentDomainID
            ], [
            'tgmUserID' => $newUser->id,
            'domainID' => $marketing->mktDomainID,
            'tgmLocationID' => $domain->id,
            'customerID' => $marketing->mktCustomerID,
            'userID' => $marketing->mktUserID,   
            'repID' => $marketing->mktRepID,
            'fname' => $request->fname,
            'lname' => $request->lname,
            'mobile' => $request->mobile,
            'unformattedMobile' => $numOnly,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zipCode,
            'birthDay' => $bd,
            'anniversary' => $an,
        ]);

        $oldSessionID = session()->getId();
        $cartData = cartData::where( 'sessionID', '=', $oldSessionID )->count();
        $referer = $request->referer;

        if (Auth::attempt(['email' => $newUser->email, 'password' => $request->password1, 'parentDomainID' => $newUser->parentDomainID])) {
            // login user

            session()->flash('loginSuccess', 'You have been successfully registered and logged in.');
            $authPages = array('/order', '/viewcart', '/customer/dashboard', '/dine', '/dineincart');
            
            if (in_array($referer, $authPages)) {

                $numSent = $this->sendWelcomeEmail($contact, $domain);

                if ($cartData == 0) {
                    // return to referring page
                    return redirect($referer);
                } else {    
                    //update cartData and return to referring page
                    $newSessionID = session()->getId();
                    cartData::where('sessionID', '=', $oldSessionID)->update(['sessionID' => $newSessionID, 'userID' => Auth::id()]);
                    return redirect($referer);
                }
            } // end if in_array authPages

        } else {

            // Authentication Failed
            session()->flash('loginError', 'There was an error with your username or password. Please try again.');
            return redirect($referer);
        }

    } // end userRegisterDo





/*---------------- Send eMail upon registration  ----------------------------------*/

    public function sendWelcomeEmail($contact, $domain) {

        // create the swiftMessage
        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
        $view = \View::make('customer.welcomeEmail', [ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'fname' => $contact->fname ]);
        $to['email'] = $contact->email;
        $to['name'] = $contact->fname . " " . $contact->lname;
        $sender = $domainSiteConfig->sendEmail;
        $subject = $domainSiteConfig->locationName . "Online Registration";
        $bounceAddress = $domainSiteConfig->sendEmail;
        
        // Create the Transport using HWCTMailer
        $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

        // Create the Mailer using your created Transport
        $mailer = HWCTMailer::createMailer($transport);

        // Create a message -> createEmailMessage($subject, $from, $html, $text, $type)
        $message = HWCTMailer::createEmailMessage($subject, $sender, $bounceAddress, $view, '', 'html');

        // Send the message -> sendEmailMessage($mailer, $message, $to, $type)
        $numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email' );

        // save (log) email sent
        if ($numSent['numSent']) {
            $log = new registrationEmails;
                $log->domainID = $domain->id;
                $log->userID = $contact->userID;
                $log->contactID = $contact->id;
                $log->email = $contact->email;
                $log->html = htmlentities($view);
            $log->save();
        }

        return $numSent;

    } // end sendOrderEmail



 /*---------------------Guest User Registration Completion  ----------------------------------------*/

 public function guestUserRegister(Request $request)
 {
    $domain = \Request::get('domain');
    // get data for marketing system
    $marketing = marketing::findOrFail($domain->parentDomainID);

    // set dates
    if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
    if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }

    $numOnly = CellNumberData::numbersOnly($request->mobile);

    // update or create contact based on email and domainID
    $newUser = User::firstOrCreate([
        'email' => $request->email,
        'parentDomainID' => $domain->parentDomainID
        ], [
        'domainID' => $domain->id,
        'role_auth_level' => '10',
        'password' => Hash::make($request->password1),
        'active' => 'yes',
        'fname' => $request->fname,
        'lname' => $request->lname,
        'mobile' => $request->mobile,
        'unformattedMobile' => $numOnly,
        'email' => $request->email,
        'countryCode' => 'US',
        'zipCode' => $request->zip
    ]);
            
    $newUsersData = usersData::firstOrCreate([
        'usersID' => $newUser->id,
        ], [
        'address1' => $request->address1,
        'address2' => $request->address2,
        'city' => $request->city,
        'state' => $request->state
    ]);
    
    return response()->json([
        'status' => 'success',
        'newUser' => $newUser,
        'newUsersData' => $newUsersData
    ]);
  } // end guestUserRegister


/*--------------------- Order Process Login  ----------------------------------------*/

    public function orderCartLogin(Request $request)
    {
        $domain = \Request::get('domain');
        $oldSessionID = session()->getId();
        $checkSocialUser = User::where('email',$request->email)->select('type','source')->first();
        if($checkSocialUser){
            if($checkSocialUser->type == 'social'){
                return response()->json([
                    'status' => 'Please login with your '. $checkSocialUser->source .' account',
                    'authUser' => null
                ]);
            }
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'parentDomainID' => $domain->parentDomainID])) {
            $user=Auth::user();
            // Authentication passed...
            $newSessionID = session()->getId();
            cartData::where('sessionID', '=', $oldSessionID)->update(['sessionID' => $newSessionID, 'userID' => Auth::id()]);
            session()->flash('loginSuccess', 'You have been successfully logged in.');
            return response()->json([
                'status' => 'success',
                'authUser' => $user
            ]);
        }
        return response()->json([
            'status' => 'something went wrong',
            'authUser' => null
        ]);
    }// end orderCartLogin

/*---------------------User Password Reset  ----------------------------------------*/

   public function userPasswordReset($token)
   {
        $domain = \Request::get('domain');
        $user = User::where('domainID', '=', $domain->id)->where('email_varification_token',$token)->first();
        if(is_null($user)){
            abort('404');
        }

        $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
        $userID = $user->id;      

        return view('user.passwordReset')->with([ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles, 'userID' => $userID, 'token' => $token ]);
   }// end userPasswordReset

   public function userPasswordResetDo($token, Request $request)
   {
        $domain = \Request::get('domain');
        $user = User::find($request->userID);
        $user->password = bcrypt($request->password1);
        $user->email_varification_token = null;
        $user->save();
        return redirect('/login')->with('success', 'Password has been changed, please do login.');
   }// end userPasswordResetDo

/*---------------------Forgot Password----------------------------------------*/

   public function forgotPassword()
   {
        $domain = \Request::get('domain');
        $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
        $userID = Auth::id();      

        return view('user.forgotPassword')->with([ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles ]);
   }// end forgotPassword

   public function forgotPasswordDo(Request $request)
   {
       $request->validate([
           'email' => 'required',
        ]);
        $domain = \Request::get('domain');
        $user = User::where('domainID', '=', $domain->id)->where('email',$request->email)->first();
        if(is_null($user)){
            return redirect()->back()->withErrors(['Invalid User']);
        }
        $emailVarificationToken = $this->getToken(32);
        $emailVarificationToken = $emailVarificationToken.$user->id;
        $user->email_varification_token = $emailVarificationToken;
        $user->save();

        $resetPasswordLink = 'https://'.$request->getHttpHost().'/'.$emailVarificationToken.'/password/reset';
        $email = $request->email;
        $name = $user->fname . " " . $user->lname;
        $domainID = $domain->id;

        $this->sendResetPasswordEmail($resetPasswordLink, $email, $name, $domainID);
       
        return redirect()->back()->with('success', 'Please check your email to reset your password.');
   }// end forgotPassword

    private function getToken($length){    
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return $token;
    }

    private function sendResetPasswordEmail($resetPasswordLink, $email, $name, $domainID) {
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
		$view = \View::make('user.resetPasswordEmail', ['resetPasswordLink' => $resetPasswordLink]);
		// create the email html
		$type = "html";
		$textBody = '';
		$to['email'] = $email;
		$to['name'] = $name;
		$sender = $domainSiteConfig->sendEmail;
        $html = $view->render();
        $numSent = 0;
        
        $subject = $domainSiteConfig->locationName . " Reset Password";
		// Create the Transport using HWCTMailer
		$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

		// Create the Mailer using your created Transport
		$mailer = HWCTMailer::createMailer($transport);

		// Create a message, $subject, $from, $message
		$message = HWCTMailer::createOrderMessage($subject, $sender , $html, $textBody, $type);

		$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email' );
       
	    return $numSent;

	} // end sendOrderEmail

    public function deleteusercard(Request $request)
   {        
        UserCard::find($request->id)
            ->delete();
        return  response()->json([
            'status' => 'Card Removed Successfully',
        ]);
    
    }// end forgotPassword

} // end UsersController
