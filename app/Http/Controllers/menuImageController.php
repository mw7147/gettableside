<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\menuCategoriesData;
use App\fileDownloads;
use App\menuImages;
use Carbon\Carbon;
use App\menuData;

class menuImageController extends Controller
{
    

    /*-------------------- Image List  ----------------------------------------*/
    public function imageList($urlPrefix, $menuDataID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $imagesSmall = $this->getImagesBySize($menuDataID, 'Small');
        $imagesLarge = $this->getImagesBySize($menuDataID, 'Large');
        $menuImages = $this->getImageCount($menuDataID);

        return view('menuImage.imageList')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'imagesSmall' => $imagesSmall, 'imagesLarge' => $imagesLarge, 'menuImages' => $menuImages, 'menuDataID' => $menuDataID]);

    } //end imageList



    /*-------------------------------- Upload Image -----------------------------------------------------------*/   

    public function uploadImage(Request $request) {
        $image_64 = $request->input('image');
        if (is_null($image_64)) {
            $message['status'] = "error";
            $message['message'] = "There was an error processing your menu image. Please try again.";
            return json_encode($message);
        }
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];

        $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
        $image = str_replace($replace, '', $image_64); 
        $image = str_replace(' ', '+', $image); 
        $image = base64_decode($image);

        $fileSize = strlen($image);

        if ($fileSize/1000 > env('MAX_IMAGE_SIZE')) {
            $message['status'] = "error";
            $message['message'] = "The filesize cannot be larger than " . number_format(env('MAX_IMAGE_SIZE')) . "Kb. Please check your file and try again.";
            return json_encode($message);
        }
        
        // check filetype
        $fileTypes = ['png', 'jpg', 'jpeg'];
        if (!in_array($extension, $fileTypes)) {
            $message['status'] = "error";
            $message['message'] = "The menu image filetype must be a jpg or png. Please check your file and try again.";
            return json_encode($message);
        }

        $fileName = \Str::random(10).'.'.$extension;

        // file metadata
        $menuID = menuData::where('id', '=', $request->menuDataID)->value('menuID');        
        $path = env('PUBLIC_MENU_IMAGES') . '/' . $request->domainID . '/' . $menuID . '/' . $request->imageSize . '/';
        list($width, $height, $type, $attr) = getimagesize($image_64);
       
        // save file info - get menuImageID
        $menuImage = new menuImages();
            $menuImage->menuID = $menuID;
            $menuImage->menuDataID = $request->menuDataID;
            $menuImage->sortOrder = $request->sortOrder;
            $menuImage->imageWidth = $width;
            $menuImage->imageHeight = $height;
            $menuImage->imageSize = $request->imageSize;
            $menuImage->imageType = $extension;
            $menuImage->fileSize = $fileSize;
            $menuImage->fileName = $fileName;
        $mn = $menuImage->save();

        // save new file
        $saveFile = Storage::put($path . $fileName, $image);;

        //delete old file
        if($request->eid != '') {
            $oldMenuImage = menuImages::find($request->eid);
            if (!is_null($oldMenuImage)) {
                menuImages::destroy($oldMenuImage->id);
                Storage::delete($path . $oldMenuImage->fileName);
            } 
        }
        
        // return data
        $message['status'] = "success";
        $message['fileName'] = $fileName;
        $message['fileSize'] = $fileSize;
        $message['fileExt'] = $extension;
        $message['path'] = $path;
        $message['oldExistingID'] = $request->eid;
        return json_encode($message);
    }     // end uploadImage



    /*-------------------------------- Delete Image -----------------------------------------------------------*/   

    public function deleteImage(Request $request) {

        // delete images
        if ($request->smallID != '') {$deleteSmall = $this->deleteImageByID($request->smallID, $request->domainID);}
        if ($request->largeID != '') {$deleteLarge = $this->deleteImageByID($request->largeID, $request->domainID);}
        
        // re-sort
        if ($request->smallID != '') {$reSortSmall = $this->reSortImageByType($request->menuDataID, "Small");}
        if ($request->largeID != '') {$reSortLarge = $this->reSortImageByType($request->menuDataID, "Large");}
        
        // return data
        $message['status'] = "success";
        $message['menuDataID'] = $request->menuDataID;

        return json_encode($message);

    }     // end deleteImage




    /*-------------------------------- Get Images By Size -----------------------------------------------------------*/   

    // Return image array for imageSize - large or small
    protected function getImagesBySize($menuDataID, $imageSize) {

        $images = [];
        $data = menuImages::where([ ['menuDataID', '=', $menuDataID], ['imageSize', '=', $imageSize] ])->orderBy('sortOrder')->get();

        for ($i = 0; $i < env('MAX_MENU_ITEM_IMAGES'); $i++) {
            if (isset($data[$i])) {
                $key = $i+1;
                $images[$key] = Array('fileName' => $data[$i]->fileName, 'sortOrder' => $data[$i]->sortOrder, 'menuDataID' => $data[$i]->menuDataID, 'id' => $data[$i]->id, 'imageSize' => $imageSize, 'menuID' =>$data[$i]->menuID);
            } else {
                $key = $i+1;
                $images[$key] = Array('fileName' => '', 'sortOrder' => '', 'menuDataID' => '', 'id' => '', 'menuID' => '', 'imageSize' => $imageSize);
            }
        }

        return $images;

    } // end getImagesBySize


    /*-------------------------------- Image Count -----------------------------------------------------------*/   

    // Returns number of images for menu item
    protected function getImageCount($menuDataID) {
       
        $small = menuImages::where([ ['menuDataID', '=', $menuDataID], ['imageSize', '=', 'Small'] ])->count();
        $large = menuImages::where([ ['menuDataID', '=', $menuDataID], ['imageSize', '=', 'Large'] ])->count();
        $imageCount = max($small, $large);
    
        return $imageCount;
    
    } // end getImageCount


    /*-------------------------------- Delete Image By ID -----------------------------------------------------------*/   

    // Returns number of images for menu item
    protected function deleteImageByID($imageID, $domainID) {

        $image = menuImages::find($imageID);   
        if (is_null($image)) { return "image not found";}
        
        $path = env('PUBLIC_MENU_IMAGES') . '/' . $domainID . '/' . $image->menuID . '/' . $image->imageSize . '/';
        // delete Image
        Storage::delete($path . $image->fileName);
        // delete db record
        menuImages::destroy($imageID);
        
        return "deleted";
    } // end deleteImageByID


    /*-------------------------------- Re-Sort Image By Type -----------------------------------------------------------*/   

    // resort menu Images by Type
    protected function reSortImageByType($menuDataID, $type) {
        
        $counter = 1;
        $data = menuImages::where([ ['menuDataID', '=', $menuDataID], ['imageSize', '=', $type] ])->orderBy('sortOrder')->get();
        if (is_null($data)) { return "no images";}
        
        foreach ($data as $d) {
            $d->sortOrder = $counter;
            $d->save();
            $counter = $counter + 1;
        }
        
        return "sorted";
    } // end reSortImageByType


    /*-------------------------------- New Category Image -----------------------------------------------------------*/   

    public function newCategoryImage(Request $request) {
        
        if (is_null($request->img)) {
        $message['status'] = "error";
        $message['message'] = "There was an error processing your category image. Please try again.";
        return json_encode($message);
        }
        
        // add new filelist($width, $height, $type, $attr) = getimagesize($request->file('img'));
        $fileName = $request->file('img')->getClientOriginalName();
        $fileName = str_replace(' ', '', $fileName);
        $fileSize = $request->file('img')->getSize(); // file size in Kilobytes
    
        // check filesize
        if ($fileSize/1000 > env('MAX_IMAGE_SIZE')) {
        $message['status'] = "error";
        $message['message'] = "The filesize cannot be larger than " . number_format(env('MAX_IMAGE_SIZE')) . "Kb. Please check your file and try again.";
        return json_encode($message);
        }

        // check filetype
        $fileTypes = ['png', 'jpg', 'jpeg'];
        $fileExt = $request->file('img')->guessClientExtension(); // file type
        if (!in_array($fileExt, $fileTypes)) {
        $message['status'] = "error";
        $message['message'] = "The menu image filetype must be a jpg or png. Please check your file and try again.";
        return json_encode($message);
        }

        // file metadata
        $path = env('PUBLIC_CATEGORY_IMAGES') . '/' . $request->domainID . '/' . $request->menuCategoriesID . '/';
        list($width, $height, $type, $attr) = getimagesize($request->file('img'));

        // save file info - get menuImageID
        $menuCategoriesData = menuCategoriesData::find($request->menuCategoriesDataID);
            $menuCategoriesData->imageWidth = $width;
            $menuCategoriesData->imageHeight = $height;
            $menuCategoriesData->imageType = $fileExt;
            $menuCategoriesData->fileSize = $fileSize;
            $menuCategoriesData->fileName = $fileName;
        $mn = $menuCategoriesData->save();

        // save new file
        $saveFile = $request->file('img')->storeAs($path, $fileName);

        //delete old file
        if($request->oldFileName != '') {
            Storage::delete($path . $request->oldFileName);
        }
        
        // return data
        $message['status'] = "success";
        $message['fileName'] = $fileName;
        $message['fileSize'] = $fileSize;
        $message['fileExt'] = $fileExt;
        $message['path'] = $path;
        $message['oldExistingID'] = $request->eid;
        return json_encode($message);

    }     // end newCategoryImage
        




    /*-------------------------------- Delete Category Image -------------------------------------------*/   

    // Delete category image by ID
    protected function deleteCategoryImage(Request $request) {
        
        $image = menuCategoriesData::find($request->menuCategoriesDataID);   
        if (is_null($image)) { return "image not found";}
        
        $path = env('PUBLIC_MENU_IMAGES') . '/' . $request->domainID . '/' . $request->menuCategoriesID . '/';
        // delete Image
        Storage::delete($path . $request->fileName);
        // delete db record
       $menuCategoriesData = menuCategoriesData::find($request->menuCategoriesDataID);
            $menuCategoriesData->imageWidth = null;
            $menuCategoriesData->imageHeight = null;
            $menuCategoriesData->fileSize = null;
            $menuCategoriesData->fileName = null;
        $menuCategoriesData->save();


        return "deleted";
    } // end deleteCategoryImage




} // end menuImageController
