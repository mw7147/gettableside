<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use Carbon\Carbon;
use App\orders;

class orderAdminController extends Controller
{
    /*-------------------- Order Admin Dashboard  ----------------------------------------*/

	public function locationView(Request $request) {

		//dd($request);
		//if (isset($request->dateRange)) {$dateRange = $request->dateRange;} else {$dateRange = '';}
		$domain = \Request::get('domain');
		$authLevel = \Request::get('authLevel');
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
		if ( !is_null($request->specificRange) || !is_null($request->dateRange)) {
			if (is_null($request->dateRange)) {
				$orders = $this->specificRange($request->specificRange, $domain->id, $authLevel);
			} else {
				$orders = $this->dateRange($request->dateRange, $domain->id);
			}
			$specificRange = $request->specificRange;
		} else {
			$orders = $this->dateRange("today", $domain->id);
			$specificRange='';
		}
		$breadcrumb = ['orders', 'view'];	
		
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
		if ( $domainSiteConfig->printReceipt > 0 ) { $printReceipt = true; } else { $printReceipt = false; }
		if ( $domainSiteConfig->printOrder > 0 ) { $printOrder = true; } else { $printOrder = false; }

      	return view('orderAdmin.view')->with(['domain' => $domain, 'authLevel' => $authLevel, 'orders' => $orders, 'breadcrumb' => $breadcrumb, 'dateRange' => $request->dateRange, 'ccType' => $domainPaymentConfig->ccType, 'printReceipt' => $printReceipt, 'printOrder' => $printOrder, 'specificRange' => $specificRange ]);

	} // end location view




    /*-------------------- Orders Only Admin  ----------------------------------------*/

	public function ordersView(Request $request) {

		$dateRange = 'today';
		//if (isset($request->dateRange)) {$dateRange = $request->dateRange;} else {$dateRange = 'today';}
		$domain = \Request::get('domain');
		$authLevel = \Request::get('authLevel');
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
		$orders = $this->currentOrdersOnly($domain->id, $authLevel);
		$breadcrumb = ['orders', 'ordersview'];
		
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
		if ( $domainSiteConfig->printReceipt > 0 ) { $printReceipt = true; } else { $printReceipt = false; }
		if ( $domainSiteConfig->printOrder > 0 ) { $printOrder = true; } else { $printOrder = false; }

		$pendingOrdersOnly = $this->pendingOrdersOnly($domain->id, $authLevel);
		$pendingOrdersCount = count($pendingOrdersOnly);

      	return view('orderAdmin.viewOrdersOnly')->with(['domain' => $domain, 'orders' => $orders, 'breadcrumb' => $breadcrumb, 'dateRange' => $dateRange, 'ccType' => $domainPaymentConfig->ccTyp, 'printReceipt' => $printReceipt, 'printOrder' => $printOrder, 'pendingOrdersCount' => $pendingOrdersCount]);

	} // end ordersView

	/*-------------------- Orders Only Admin  ----------------------------------------*/

	public function pendingOrdersView(Request $request) {

		$dateRange = 'today';
		//if (isset($request->dateRange)) {$dateRange = $request->dateRange;} else {$dateRange = 'today';}
		$domain = \Request::get('domain');
		$authLevel = \Request::get('authLevel');
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
		$orders = $this->pendingOrdersOnly($domain->id, $authLevel);
		$breadcrumb = ['orders', 'ordersview'];
		
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
		if ( $domainSiteConfig->printReceipt > 0 ) { $printReceipt = true; } else { $printReceipt = false; }
		if ( $domainSiteConfig->printOrder > 0 ) { $printOrder = true; } else { $printOrder = false; }

      	return view('orderAdmin.viewPendingOrdersOnly')->with(['domain' => $domain, 'orders' => $orders, 'breadcrumb' => $breadcrumb, 'dateRange' => $dateRange, 'ccType' => $domainPaymentConfig->ccTyp, 'printReceipt' => $printReceipt, 'printOrder' => $printOrder]);

	} // end ordersView

	/*-------------------- Confirm Order  ----------------------------------------*/

	public function confirmOrder(Request $request) {
		$order = orders::find($request->orderID);
		$order->status = 'valid';
		$order->save();
		return  response()->json([
            'status' => 'Order Confirmed Successfully',
        ]);
	} // end confirmOrder

    /*-------------------- date Range  ----------------------------------------*/

	public function dateRange($dateRange, $domainID) {

		$authLevel = \Request::get('authLevel');

		switch ($dateRange) {
		    case "today":
		    	$d = Carbon::now();
		    	$today = $d->toDateString();
				$rawToday = "DATE(orders.orderPrepTime) = '" . $today . "'";

				if ($authLevel >= 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->whereRaw($rawToday)
						->orderBy('orderPrepTime', 'desc')
						->get();
				} elseif ($authLevel >= 35 && $authLevel < 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
					->select('orders.*', 'domains.parentDomainID', 'domains.name')
					->where('domains.parentDomainID', '=', $domainID)            
					->whereRaw($rawToday)
					->orderBy('orderPrepTime', 'desc')
					->get();
				} else {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('orders.domainID', '=', $domainID)            
						->whereRaw($rawToday)
						->orderBy('orderPrepTime', 'desc')
						->get();
				}

				break;


			case "yesterday":
					$d = Carbon::now()->subDay();
					$yesterday = $d->toDateString();
					$rawToday = "DATE(orders.orderPrepTime) = '" . $yesterday . "'";

					if ($authLevel >= 40) {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->whereRaw($rawToday)
							->orderBy('orderPrepTime', 'desc')
							->get();
					} elseif ($authLevel >= 35 && $authLevel < 40) {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('domains.parentDomainID', '=', $domainID)            
						->whereRaw($rawToday)
						->orderBy('orderPrepTime', 'desc')
						->get();
					} else {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->where('orders.domainID', '=', $domainID)            
							->whereRaw($rawToday)
							->orderBy('orderPrepTime', 'desc')
							->get();
					}
	
					break;


				case "tomorrow":
						$d = Carbon::now()->addDay();
						$tomorrow = $d->toDateString();
						$rawToday = "DATE(orders.orderPrepTime) = '" . $tomorrow . "'";
		
						if ($authLevel >= 40) {
							$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
								->select('orders.*', 'domains.parentDomainID', 'domains.name')
								->whereRaw($rawToday)
								->orderBy('orderPrepTime', 'desc')
								->get();
						} elseif ($authLevel >= 35 && $authLevel < 40) {
							$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->where('domains.parentDomainID', '=', $domainID)            
							->whereRaw($rawToday)
							->orderBy('orderPrepTime', 'desc')
							->get();
						} else {
							$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
								->select('orders.*', 'domains.parentDomainID', 'domains.name')
								->where('orders.domainID', '=', $domainID)            
								->whereRaw($rawToday)
								->orderBy('orderPrepTime', 'desc')
								->get();
						}
		
						break;




				

		    case "week":
		    	Carbon::setWeekStartsAt(Carbon::SUNDAY);
				Carbon::setWeekEndsAt(Carbon::SATURDAY);
				$sd = Carbon::now()->startOfWeek()->toDateString();
				$ed = Carbon::now()->endOfWeek()->toDateString();
				$rawSD = "DATE(orders.orderPrepTime) >= '" . $sd . "'";
				$rawED = "DATE(orders.orderPrepTime) <= '" . $ed . "'";

				if ($authLevel >= 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
				} elseif ($authLevel >= 35 && $authLevel < 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
					->select('orders.*', 'domains.parentDomainID', 'domains.name')
					->where('domains.parentDomainID', '=', $domainID)            
					->whereRaw($rawSD)
					->whereRaw($rawED)
					->orderBy('orderPrepTime', 'desc')
					->get();
				} else {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('orders.domainID', '=', $domainID)            
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
				}			
				
				break;


				case "lastWeek":
					Carbon::setWeekStartsAt(Carbon::SUNDAY);
					Carbon::setWeekEndsAt(Carbon::SATURDAY);
					$lastWeekDay = Carbon::now()->subDays(7);
					$sd = $lastWeekDay->startOfWeek()->toDateString();
					$ed = $lastWeekDay->endOfWeek()->toDateString();
					$rawSD = "DATE(orders.orderPrepTime) >= '" . $sd . "'";
					$rawED = "DATE(orders.orderPrepTime) <= '" . $ed . "'";
	
					if ($authLevel >= 40) {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->whereRaw($rawSD)
							->whereRaw($rawED)
							->orderBy('orderPrepTime', 'desc')
							->get();
					} elseif ($authLevel >= 35 && $authLevel < 40) {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('domains.parentDomainID', '=', $domainID)            
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
					} else {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->where('orders.domainID', '=', $domainID)            
							->whereRaw($rawSD)
							->whereRaw($rawED)
							->orderBy('orderPrepTime', 'desc')
							->get();
					}			
					
					break;


		    case "month":
		    	$d = Carbon::now();
				$sd = $d->startOfMonth()->toDateString(); // start of month
				$ed = $d->endOfMonth()->toDateString(); // end of month
				$rawSD = "DATE(orders.orderPrepTime) >= '" . $sd . "'";
				$rawED = "DATE(orders.orderPrepTime) <= '" . $ed . "'";
				
				if ($authLevel >= 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
				} elseif ($authLevel >= 35 && $authLevel < 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
					->select('orders.*', 'domains.parentDomainID', 'domains.name')
					->where('domains.parentDomainID', '=', $domainID)            
					->whereRaw($rawSD)
					->whereRaw($rawED)
					->orderBy('orderPrepTime', 'desc')
					->get();
				} else {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('orders.domainID', '=', $domainID)            
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
				}	

				break;

			case "lastMonth":
				$d = Carbon::now()->subMonth();
				$sd = $d->startOfMonth()->toDateString(); // start of month
				$ed = $d->endOfMonth()->toDateString(); // end of month
				$rawSD = "DATE(orders.orderPrepTime) >= '" . $sd . "'";
				$rawED = "DATE(orders.orderPrepTime) <= '" . $ed . "'";
				
				if ($authLevel >= 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
				} elseif ($authLevel >= 35 && $authLevel < 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
					->select('orders.*', 'domains.parentDomainID', 'domains.name')
					->where('domains.parentDomainID', '=', $domainID)            
					->whereRaw($rawSD)
					->whereRaw($rawED)
					->orderBy('orderPrepTime', 'desc')
					->get();
				} else {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('orders.domainID', '=', $domainID)            
						->whereRaw($rawSD)
						->whereRaw($rawED)
						->orderBy('orderPrepTime', 'desc')
						->get();
				}	

				break;

				case "allFuture":
					$d = Carbon::now();
					$today = $d->toDateTimeString();
					$rawToday = "orders.orderPrepTime > '" . $today . "'";
	
					if ($authLevel >= 40) {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->whereRaw($rawToday)
							->orderBy('orderPrepTime', 'desc')
							->get();
					} elseif ($authLevel >= 35 && $authLevel < 40) {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('domains.parentDomainID', '=', $domainID)            
						->whereRaw($rawToday)
						->orderBy('orderPrepTime', 'desc')
						->get();
					} else {
						$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
							->select('orders.*', 'domains.parentDomainID', 'domains.name')
							->where('orders.domainID', '=', $domainID)            
							->whereRaw($rawToday)
							->orderBy('orderPrepTime', 'desc')
							->get();
					}
	
					break;
				

		     case "all":
		     	$d = Carbon::now();
				$today = $d->toDateString();


				if ($authLevel >= 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->orderBy('orderPrepTime', 'desc')
						->get();
				} elseif ($authLevel >= 35 && $authLevel < 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
					->select('orders.*', 'domains.parentDomainID', 'domains.name')
					->where('domains.parentDomainID', '=', $domainID)   
					->orderBy('orderPrepTime', 'desc')         
					->get();
				} else {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('orders.domainID', '=', $domainID)  
						->orderBy('orderPrepTime', 'desc')          
						->get();
				}	



		        break;

		      default:
		      	// today only
		    	$d = Carbon::now();
		    	$today = $d->toDateString();
				$rawToday = "DATE(orders.orderPrepTime) = '" . $today . "'";
				if ($authLevel >= 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->whereRaw($rawToday)
						->orderBy('orderPrepTime', 'desc')
						->get();
				} elseif ($authLevel >= 35 && $authLevel < 40) {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
					->select('orders.*', 'domains.parentDomainID', 'domains.name')
					->where('domains.parentDomainID', '=', $domainID)            
					->whereRaw($rawToday)
					->orderBy('orderPrepTime', 'desc')
					->get();
				} else {
					$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
						->select('orders.*', 'domains.parentDomainID', 'domains.name')
						->where('orders.domainID', '=', $domainID)            
						->whereRaw($rawToday)
						->orderBy('orderPrepTime', 'desc')
						->get();
				}
		        
		} // end switch
		
		// dd($orders);

      	return $orders;


	} // end dateRange




	/*-------------------- specific Range  ----------------------------------------*/

	public function specificRange($specificRange, $domainID, $authLevel) {
		
		$days = explode(" - ", $specificRange);

		$sd = Carbon::parse($days[0])->toDateString(); // start of month
		$ed = Carbon::parse($days[1])->toDateString(); // end of month
		$rawSD = "DATE(orders.orderPrepTime) >= '" . $sd . "'";
		$rawED = "DATE(orders.orderPrepTime) <= '" . $ed . "'";
		
		if ($authLevel >= 40) {
			$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
				->select('orders.*', 'domains.parentDomainID', 'domains.name')
				->whereRaw($rawSD)
				->whereRaw($rawED)
				->orderBy('orderPrepTime', 'desc')
				->get();
		} elseif ($authLevel >= 35 && $authLevel < 40) {
			$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
			->select('orders.*', 'domains.parentDomainID', 'domains.name')
			->where('domains.parentDomainID', '=', $domainID)            
			->whereRaw($rawSD)
			->whereRaw($rawED)
			->orderBy('orderPrepTime', 'desc')
			->get();
		} else {
			$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
				->select('orders.*', 'domains.parentDomainID', 'domains.name')
				->where('orders.domainID', '=', $domainID)            
				->whereRaw($rawSD)
				->whereRaw($rawED)
				->orderBy('orderPrepTime', 'desc')
				->get();
		}	
		
		return $orders;

	} // end specificRange



	/*-------------------- Current Orders Only  ----------------------------------------*/

	public function currentOrdersOnly($domainID, $authLevel) {

		$d = Carbon::now();
		$now = $d->toDateTimeString();
		$today = $d->toDateString();
		$rawToday = "orders.orderPrepTime <= '" . $now . "'";
		$rawDate = "DATE(orders.orderPrepTime) = '" . $today . "'";

		if ($authLevel >= 40) {
			$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
				->select('orders.*', 'domains.parentDomainID', 'domains.name')
				->whereRaw($rawToday)
				->whereRaw($rawDate)
				->where('orderInProcess', '<', 9)
				->orderBy('orderPrepTime', 'desc')
				->get();
		} elseif ($authLevel >= 35 && $authLevel < 40) {
			$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
			->select('orders.*', 'domains.parentDomainID', 'domains.name')
			->where('domains.parentDomainID', '=', $domainID)  
			->where('orderInProcess', '<', 9)          
			->whereRaw($rawToday)
			->whereRaw($rawDate)
			->orderBy('orderPrepTime', 'desc')
			->get();
		} else {
			$orders = orders::where(function ($query) {$query->where('status','=','valid')->orWhereNull('status');})->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
				->select('orders.*', 'domains.parentDomainID', 'domains.name')
				->where('orders.domainID', '=', $domainID)       
				->where('orderInProcess', '<', 9)     
				->whereRaw($rawToday)
				->whereRaw($rawDate)
				->orderBy('orderPrepTime', 'desc')
				->get();
		}
		
		return $orders;

	} // end currentOrdersOnly

	/*-------------------- Pending Orders Only  ----------------------------------------*/

	public function pendingOrdersOnly($domainID, $authLevel) {
		
		if ($authLevel >= 40) {
			$orders = orders::where('status','=','invalid')->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
				->select('orders.*', 'domains.parentDomainID', 'domains.name')
				->orderBy('orderPrepTime', 'desc')
				->get();
		} elseif ($authLevel >= 35 && $authLevel < 40) {
			$orders = orders::where('status','=','invalid')->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
			->select('orders.*', 'domains.parentDomainID', 'domains.name')
			->where('domains.parentDomainID', '=', $domainID)  
			->orderBy('orderPrepTime', 'desc')          
			->get();
		} else {
			$orders = orders::where('status','=','invalid')->leftJoin('domains', 'orders.domainID', '=', 'domains.id')
				->select('orders.*', 'domains.parentDomainID', 'domains.name')
				->where('orders.domainID', '=', $domainID)
				->orderBy('orderPrepTime', 'desc')       
				->get();
		}
		
		return $orders;

	} // end pendingOrdersOnly

} // end orderAdminController
