<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tables;

class tableController extends Controller{
    

    /*----------------  viewTables  ----------------------------------*/

    public function viewTables($urlPrefix) {
        
        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');

        $tables = tables::where('domainID', '=', $domain->id)->get();

        return view('tables.view')->with([ 'domain' => $domain, 'tables' => $tables, 'breadcrumb' => $breadcrumb ]);

    } // end viewTables



    /*----------------  newTable  ----------------------------------*/

    public function newTable() {
        
        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');

        return view('tables.new')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb ]);

    } // end newTable



    /*----------------  newTableDo  ----------------------------------*/

    public function newTableDo(Request $request, $urlPrefix) {

        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');

        $table = new tables();
            $table->domainID = $domain->id;
            $table->parentDomainID = $domain->parentDomainID;
            //$table->tableID = $request->tableID;
            $table->name = $request->name;
            $table->note = $request->note;
            $table->numSeats = $request->numSeats ?? 0;
        $t = $table->save();

        if ($t) {
            $request->session()->flash('tableSuccess', 'The table was successfully created.');
            return redirect()->route( 'tables', [ 'urlPrefix' => $urlPrefix ] );
        } else {
            $request->session()->flash('tableError', 'There was an error creating the table. Please check your data and try again.');
            return redirect()->back()->withInput();
        }


    } // end newTableDo


    /*----------------  deleteTable  ----------------------------------*/

    public function deleteTable( Request $request, $urlPrefix, $id) {

        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');
        $table = tables::findOrFail($id);

        if ($table->domainID == $domain->id) {
            // only delete your tables
            $table->delete();
            $request->session()->flash('tableSuccess', 'The table was successfully deleted.');
        } else {
            // not your table
            $request->session()->flash('tableError', 'There was an error deleting the table. Please check your data and try again.');
        }

        return redirect()->route( 'tables', [ 'urlPrefix' => $urlPrefix ] );


    } // end deleteTag



    /*----------------  editTable  ----------------------------------*/

    public function editTable( Request $request, $urlPrefix, $id) {
    
        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');
        $table = tables::findOrFail($id);

        if ($table->domainID == $domain->id) {
            // only edit your tables
            return view('tables.edit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb, 'table' => $table ]);
        } else {
            // not your table
            abort(403);
        }

    } // end editTable


    /*----------------  editTableDo  ----------------------------------*/

    public function editTableDo( Request $request, $urlPrefix, $id) {

        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');
        $table = tables::findOrFail($id);

        if ($table->domainID == $domain->id) {
            // only edit your tables
            // $table->tableID = $request->tableID;
            $table->numSeats = $request->numSeats ?? 0;
            $table->name = $request->name;
            $table->note = $request->note;
            $t = $table->save();
            if ($t) {
                $request->session()->flash('tableSuccess', 'The table was successfully updated.');
                return redirect()->route( 'tables', [ 'urlPrefix' => $urlPrefix ] );
            } else {
                $request->session()->flash('tableError', 'There was an error updating the table. Please check your data and try again.');
                return redirect()->back()->withInput();
            }  

        } else {
            // not your table
            abort(403);
        }

    } // end editTableDo


    /*----------------  qrCode  ----------------------------------*/

    public function qrCode( Request $request, $urlPrefix, $id) {
    
        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');
        $table = tables::findOrFail($id);

        if ($table->domainID == $domain->id) {
            // only edit your tables
            return view('tables.qrcode')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb, 'table' => $table ]);
        } else {
            // not your table
            abort(403);
        }

    } // end qrCode



    /*----------------  qrCodeDo  ----------------------------------*/

    public function qrCodeDo( Request $request, $urlPrefix, $id) {

        $breadcrumb = ['tables', ''];    
        $domain = \Request::get('domain');
        $table = tables::findOrFail($id);

        if ($table->domainID == $domain->id) {
            // only edit your tables
            $table->tagName = $request->tagName;
            $table->tagTier = $request->tagTier;
            $t = $table->save();
            if ($t) {
                $request->session()->flash('tableSuccess', 'The table was successfully updated.');
                return redirect()->route( 'tables', [ 'urlPrefix' => $urlPrefix ] );
            } else {
                $request->session()->flash('tableError', 'There was an error updating the table. Please check your data and try again.');
                return redirect()->back()->withInput();
            }  

        } else {
            // not your table
            abort(403);
        }

    } // end qrCodeDo




} // end tablesController