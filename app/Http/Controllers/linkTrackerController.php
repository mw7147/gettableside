<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\campaignLinkClicks;
use App\campaignLinkData;
use App\Http\Requests;
use App\linkClicks;
use App\emailsSent;
use App\linkData;

class linkTrackerController extends Controller {

  /*---------------------------------link tracker display--------------------------------------------*/   


	public function linkTrackRedirect($linkHash, $contactID) {
		$domain = \Request::get('domain');
		$linkData = linkData::where('linkHash', '=', $linkHash)->get();
		
		// make sure link is found - otherwise abort not found
		if ( $linkData->count() != 0 ) {
			$linkData = $linkData[0];
   	 	} else {
      		abort(404);
    	}

    	// do not save test email clicks or do not track -> cid = 0
    	if ($contactID > 0) {
	    	$linkClick = new linkClicks();
	    	$linkClick->linkDataID = $linkData->id;
	    	$linkClick->linkHash = $linkHash;
	    	$linkClick->linkAddress = $linkData->linkAddress;
	    	$linkClick->contactID = $contactID;
	    	$linkClick->domainID = $linkData->domainID;
	    	$linkClick->mailID = $linkData->mailID;
	    	$linkClick->ipAddress = $_SERVER['REMOTE_ADDR'];
	    	$linkClick->userAgent = $_SERVER['HTTP_USER_AGENT'];
			$linkClick->save();
		}

		// increment total click counter
		$click = emailsSent::find($linkData->mailID);
		if (!is_null($click)) {
			$click->totalClicks = $click->totalClicks +1;
			$click->save();
		}

		// redirect to 
		if (!filter_var($linkData->linkAddress, FILTER_VALIDATE_URL) === false) {
    		return redirect()->away($linkData->linkAddress);
		} else {
    		abort(403);
		}
		

	}     


  /*---------------------------------Campaign Link Click--------------------------------------------*/   

    /* Campaign Link Click
      Recipient clicked link in email
        1. track click by  recipient
        2. process link click action
        3. redirect link to Link
    */
	public function campaignLinkTrackRedirect($linkHash, $contactID) {
		$domain = \Request::get('domain');
		$linkData = campaignLinkData::where('linkHash', '=', $linkHash)->get();
		
		// make sure link is found - otherwise abort not found
		if ( $linkData->count() != 0 ) {
			$linkData = $linkData[0];
   	 	} else {
      		abort(404);
    	}

    	// do not save test email clicks or do not track -> cid = 0
    	if ($contactID > 0) {
	    	$clc = new campaignLinkClicks();
	    	$clc->campaignLinkDataID = $linkData->id;
	    	$clc->linkHash = $linkHash;
	    	$clc->linkAddress = $linkData->linkAddress;
	    	$clc->contactID = $contactID;
	    	$clc->domainID = $linkData->domainID;
	    	$clc->campaignEventSummaryID = $linkData->campaignEventSummaryID;
	    	$clc->ipAddress = $_SERVER['REMOTE_ADDR'];
	    	$clc->userAgent = $_SERVER['HTTP_USER_AGENT'];
			$clc->save();

			// pass campaignEventSummaryID, contactID
			$linkClickEvent = event(new CampaignLInkClickEvent( $linkData->campaignEventSummaryID, $contactID ));

		}
		// redirect to 

		if (!filter_var($linkData->linkAddress, FILTER_VALIDATE_URL) === false) {
    		return redirect()->away($linkData->linkAddress);
		} else {
    		abort(403);
		}
		

	}     


} // end LinkTracker controller