<?php

namespace App\Http\Controllers;

use Facades\App\Http\Controllers\UsersController;
use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Facades\App\hwct\foodRunnerDelivery;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Facades\App\hwct\PrintOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\cardConnectDetail;
use App\Rules\validMobile;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\deliverySession;
use GuzzleHttp\Client;
use App\ordersDetail;
use App\managerPIN;
use App\siteStyles;
use App\marketing;
use Carbon\Carbon;
use App\printers;
use App\cartData;
use App\usStates;
use App\contacts;
use App\menuData;
use App\orders;
use App\domain;
use Validator;
use App\ccBIN;
use App\menu;
use App\UserCard;
use Config;

// process payment - CardConnect - https://developer.cardconnect.com/cardconnect-api


class cardConnectController extends Controller {



	/*----------------  order payment  ----------------------------------*/


	public function orderPayment(Request $request) {
		// dd($request);
		//$request->ccToken = "9446999458721111";
		// validate form fields
		if ($request->delivery == 'true') {
			// delivery
			Validator::make($request->all(), [
				'email' => 'required|email|max:255',
				'fname' => 'required|max:255',
				'lname' => 'required|max:255',
				'mobile' => 'required|max:35',
				'address1' => 'required|max:255',
				'city' => 'required|max:255',
				'State' => 'required|max:50',
				'zip'=> 'required|max:25',
			])->validate();
		} else {
			// pickup or dine
			Validator::make($request->all(), [
				'email' => 'required|email|max:255',
				'fname' => 'required|max:255',
				'lname' => 'required|max:255',
				'mobile' => 'required|max:35'
			])->validate();
		} 

		// offset for vueComponent timezone offset - set to EDT
		// for pickup orders - set invalid date = dtset
		if ($request->deliveryDate == 'Invalid date' || !isset($request->deliveryDate)) {
			$ddate = $request->dtSet;
		} else {
			$ddate = $request->deliveryDate;
		}
		$carbonDeliveryDate = Carbon::parse($ddate);
		//Log::info('Carbon Delivery Date: ' . $carbonDeliveryDate->timezoneName);
		if ($carbonDeliveryDate->timezoneName == "z") {
			$deliveryDate = $carbonDeliveryDate->subHours(5)->toDateTimeString();
		} else {
			$deliveryDate = $carbonDeliveryDate->toDateTimeString();
		} 
		
		// charge approved - create transaction and order
		$domain = \Request::get('domain');
		$requestedTime = $deliveryDate;
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->parentDomainID)->first();
		$numOnly = CellNumberData::numbersOnly($request->mobile);
		$rnCheck = registeredNumbers::find($numOnly);


		if (is_null($rnCheck)) { $numOnly = '1111111111'; }
        
		$ipAddress = \Request::ip();
		$ccFee = $this->calcCCFee($domainPaymentConfig->togoTransactionPercent, $domainPaymentConfig->togoTransactionFee, $request->total);
		$orderInProcess = 0;
		$table = [];
		if (isset($request->tableID)) {
			$table = explode("_", $request->tableID);
		} else {
			$table[0] = null;
			$table[1] = null;
		}
		/* Future -> Add Payment Profiles  */

		
		if ($domain->type == 9) {
			// dine in
			$delivery = "dineIn";
			$deliveryTaxAmount = 0;
			$deliveryFee = 0;
			$orderPrepTime = Carbon::parse($requestedTime)->subMinutes($domainSiteConfig->pickupPrepMinutes);
		} else if ($request->delivery == "true") {
			// delivery
			$delivery = "deliver";
			$deliveryTaxAmount = $request->deliveryTaxAmount;
			$deliveryFee = $request->deliveryFee;
			$orderPrepTime = Carbon::parse($requestedTime)->subMinutes($domainSiteConfig->deliveryPrepMinutes);
		} else {
			// pickup
			$delivery = "pickup";
			$deliveryTaxAmount = 0;
			$deliveryFee = 0;
			$orderPrepTime = Carbon::parse($requestedTime)->subMinutes($domainSiteConfig->pickupPrepMinutes);
		}

		// set asap orders to prepare now
		$now = Carbon::now();
		if ($domain->type == 9) {
			// dine in
			$orderPrepTime = $now;
			$orderInProcess = 5; // dine in status
		} else if ($orderPrepTime->lessThanOrEqualTo($now)) { 
			$orderPrepTime = $now; 
			$orderInProcess = 1; // current order in process
		} else {
			$orderInProcess = 9; // future order
		}
		// get data for marketing system
		try{
			$marketing = marketing::find($domain->parentDomainID);
			// update or create contact based on email and domainID
			$contact = contacts::updateOrCreate([
				'email' => $request->email,
				'tgmDomainID' => $domain->parentDomainID
				], [
				'tgmUserID' => Auth::id() ?? null,
				'domainID' => $marketing->mktDomainID,
				'customerID' => $marketing->mktCustomerID,
				'userID' => $marketing->mktUserID,   
				'repID' => $marketing->mktRepID,
				'fname' => $request->fname,
				'lname' => $request->lname,
				'mobile' => $request->mobile,
				'unformattedMobile' => $numOnly,
				'address1' => $request->address1,
				'address2' => $request->address2,
				'city' => $request->city,
				'state' => $request->state,
				'zip' => $request->zip,
				'lastDate' => Carbon::now()->toDateTimeString(),
				'lastAmount' => $request->total
			]);
		}catch(\Exception $e){
			\Log::info($e->getMessage());
			$contact = [
				'id' => null,
				'userID' => null,
				'fname' => $request->fname,
				'lname' => $request->lname,
				'mobile' => $request->mobile,
				'address1' => $request->address1,
				'address2' => $request->address2,
				'city' => $request->city,
				'state' => $request->state,
				'zip' => $request->zip,
			];
			$contact = (object)$contact;
		}
		$orderUserID = $contact->userID;
		if($request->userID){
			$orderUserID = $request->userID;
		}
		// create new order
		$order = orders::where('orderDomainID',$domain->id)->where('sessionID',$request->sid)->first();
		if(is_null($order)){
			$order = new orders;
		}
			$order->domainID = $domain->parentDomainID;
			$order->orderDomainID = $domain->id;
			$order->ownerID = $domain->ownerID;
			$order->contactID = $contact->id;
			$order->userID = $orderUserID;
			$order->sessionID = $request->sid;
			$order->fname = $request->fname;
			$order->lname = $request->lname;
			$order->unformattedMobile = $numOnly;
            $order->customerEmailAddress = $request->email;
			$order->customerMobile = $request->mobile;
			$order->address1 = $request->address1;
			$order->address2 = $request->address2;
			$order->city = $request->city;
			$order->state = $request->state;
			$order->zipCode = $request->zip;
			$order->subTotal = $request->subTotal;
			$order->discountedSubTotal = $request->subTotal - $request->orderDiscount;
			$order->discountAmount = $request->orderDiscount ?? 0;
			$order->discountPercent = $request->orderDiscountPercent ?? 0;
			$order->tax = $request->tax;
			$order->deliveryTaxAmount = $deliveryTaxAmount;
			$order->tip = $request->tip ?? 0;
			$order->deliveryCharge = $deliveryFee;
			$order->transactionFee = $domainSiteConfig->transactionFee ?? 0;
			$order->total = $request->total;
			$order->locationID = $table[0];
			$order->locationName = $table[1];
			$order->ccType = $domainPaymentConfig->ccType;
            $order->ccTypeTransactionFee = $ccFee;
			$order->orderType = $delivery; // add delivery and in restaurant options
			$order->userAgent = $request->userAgent;
            $order->mobile = $request->isMobile;
            $order->orderIP = \Request::ip();
            $order->restaurantEmailAddress = $domainSiteConfig->sendEmail;
			$order->orderInProcess = $orderInProcess; // set order in process
			$order->orderRefundAmount = 0; // set refund to 0
			$order->timezone = $domainSiteConfig->timezone;
			$order->orderPrepTime = $orderPrepTime;
			$order->orderReadyTime = $deliveryDate;
			$order->deliveryInstructions = $request->deliveryInstructions;
			
			$order->status = 'invalid';

			if ($delivery == "deliver") {
				$order->deliveryLatitude = $request->deliveryLatitude;
				$order->deliveryLongitude = $request->deliveryLongitude;
				$order->deliveryFeet = $request->deliveryFeet;
			}

		$order->save();

		ordersDetail::where('domainID', $domain->parentDomainID)->where('sessionID', $request->sid)->delete();
		$details = cartData::where('sessionID', '=', $request->sid)->get(); // cart details
		foreach ($details as $detail) {

			// calculate menu options and sides additional price
			$optionsPrice = ProcessOrders::menuOptions($detail->menuOptionsDataID);
			$sidesPrice = ProcessOrders::menuSides($detail->menuSidesDataID);

			$od = new ordersDetail;
				$od->domainID = $domain->parentDomainID;
				$od->ownerID = $detail->ownerID;
				$od->contactID = $contact->id;
				$od->sessionID = $detail->sessionID;
				$od->menuDataID = $detail->menuDataID;
				$od->menuCategoriesDataID = $detail->menuCategoriesDataID;
				$od->menuCategoriesDataSortOrder = $detail->menuCategoriesDataSortOrder;
				$od->menuDataSortOrder = $detail->menuDataSortOrder;
				$od->menuItem = $detail->menuItem;
				$od->menuDescription = $detail->menuDescription;
				$od->portion = $detail->portion;
				$od->printOrder = $detail->printOrder;
				$od->printReceipt = $detail->printReceipt;
				$od->quantity = $detail->quantity;
				$od->price = $detail->price + $optionsPrice + $sidesPrice;
				$od->extendedPrice = $detail->extendedPrice + (($optionsPrice + $sidesPrice) * $detail->quantity); 
				$od->tax = $detail->tax;
				$od->menuOptionsDataID = $detail->menuOptionsDataID;
				$od->menuAddOnsDataID = $detail->menuAddOnsDataID;
				$od->menuSidesDataID = $detail->menuSidesDataID;
				$od->instructions = $detail->instructions;
				$od->orderIP = $detail->orderIP;
				$od->tipAmount = $detail->tipAmount;
				$od->discountPercent = $request->orderDiscountPercent ?? 0;
			$od->save();
		} // end foreach



		/** payment */
		$request->bin = 'Y';
		$ccPayment = $this->ccPayment($request);
		if($ccPayment['status'] == "approved"){
			$userID = $request->userID;
			if($request->createUser){
				$res = UsersController::guestUserRegister($request);
				$res = json_decode($res->getContent());
				$userID = $res->newUser->id;
				$order->userID = $userID;
			}
			$bin = $this->ccBIN( $domainPaymentConfig, $request->ccToken, $userID, $contact->id, $request->expireMonth, $request->expireYear, $request->total );
			$binData = $bin['data'];
			$cardConnectDetail = cardConnectDetail::find($ccPayment['cardConnectDetailID']);
			$order->status = 'valid';
			$order->ccTypeTransactionID = $cardConnectDetail->retref;
			if ($bin['status'] == 'success') {
				$order->ccTypeCardID = $binData->id;
				$order->ccTypeFunding = $binData->funding;
				$order->ccTypeLast4 = $binData->last4;
				$order->paymentType = $binData->brand;
			}
			$order->save();
			cartData::where('sessionID', '=', $request->sid)->delete();
			session()->regenerate();
			$status = 'success';
			$sendMail = ProcessOrders::orderEmail($contact, $order, $status);
			$order->emailData = $sendMail['html'];
			$order->orderItemsHTML = $sendMail['orderItemsHTML'];
			$order->save();
			try{
				if($order->orderType == 'deliver'){
					$data = [
						"display_order_id" => $order->id,
						"estimated_ready" => \Carbon\Carbon::parse($order->orderReadyTime)->format('Y-m-d G:i'),
						"pickup_restaurant_name" => $domainSiteConfig->locationName,
						"pickup_address" => "$domainSiteConfig->address1, $domainSiteConfig->city, $domainSiteConfig->state $domainSiteConfig->zipCode",
						"deliver_name" => "$order->fname $order->lname",
						"deliver_address" => "$order->address1, $order->address2, $order->city, $order->state $order->zipCode",
						"restaurant_address" => "$domainSiteConfig->address1,$domainSiteConfig->address2,$domainSiteConfig->city,$domainSiteConfig->state,$domainSiteConfig->zipCode,$domainSiteConfig->locationPhone",
						"delivery_fee" => $order->deliveryCharge,
						"order_total" => $order->total,
						"tip" => $order->tip,
						"timezone" => $domainSiteConfig->timezone,
						"delivery_instructions" => $order->deliveryInstructions,
						"order_type" => $orderInProcess,
						"delivery_prep_minutes" => $domainSiteConfig->deliveryPrepMinutes,
						"pickup_prep_minutes" => $domainSiteConfig->pickupPrepMinutes,
						'customer_mobile' => $order->customerMobile,
						"order_start_time" => \Carbon\Carbon::parse($order->orderPrepTime)->format('Y-m-d G:i'),
						'coordinates' => json_encode([ 'lat' => $domainSiteConfig->latitude, 'lng' => $domainSiteConfig->longitude])
					];
					$data = (object)$data;
					$token = $domainSiteConfig->foodRunnerToken;
					$foodRunner = $this->foodRunner($data, $token);
			            
					}
			} catch(\Exception $e){
				\Log::info($e->getMessage());
			}
			if($userID){
				$dataJson = json_decode($ccPayment['data']['data']);
				if($request->saveCard == 'true'){
					UserCard::firstOrCreate([
							'user_id' => $userID,
							'account' => $dataJson->account,
							'token' => $dataJson->token
						],
						[
							'response' => json_encode($dataJson),
							'cardNumber' => substr($dataJson->token, -4),
							'expireMonth' => $request->expireMonth,
							'expireYear' => $request->expireYear,
							'postalCode' => $request->postalCode,
						]
					);
				}
			}	
		}else {
			$status = 'failed';
			$sendMail = ProcessOrders::orderEmail($contact, $order, $status);
			$order->emailData = $sendMail['html'];
			$order->orderItemsHTML = $sendMail['orderItemsHTML'];
			$order->save();
			return redirect()->back()->withErrors(['Payment Failed']);
		}
		
		// send customer email
		$sendMail = ProcessOrders::sendOrderEmail($contact, $order, $domainSiteConfig);
		// save email html
		$order->emailData = $sendMail['html'];
		$order->orderItemsHTML = $sendMail['orderItemsHTML'];
		$order->save();

		// Send Customer Text Confirmation 
		$textSent = HWCTMailer::sendOrderText($numOnly, $domainSiteConfig, $delivery, $orderInProcess, $order->orderReadyTime);

		// Send Email to restaurant
		$emailSent = ProcessOrders::sendDomainEmail($contact, $order, $domainSiteConfig);
		
		// generate receipts and save paths
		// print receipt letter and return pdf file path
		$order->receiptLetterPath = PrintOrders::receiptLetterPDF($contact, $order, $domainSiteConfig);
		
		// print receipt thermal and return pdf file path
		$order->receiptThermalPath = PrintOrders::receiptThermalPDF($contact, $order, $domainSiteConfig);
		
		// print order letter and return pdf file path - kitchen
		$order->orderLetterPath = PrintOrders::orderLetterPDF($contact, $order, $domainSiteConfig);

		// print  order thermal and return pdf file path - kitchen
		$order->orderThermalPath = PrintOrders::orderThermalPDF($contact, $order, $domainSiteConfig);
	
		// save receipt paths
		$order->save();

		// print receipt
		if ( $domainSiteConfig->printReceipt != 0 && $orderInProcess != 9 ) {
			// get printer info
			$printer = printers::find($domainSiteConfig->printReceipt);
			
			// print receipt
			if (!is_null($printer)) { $printReceipt = PrintOrders::printReceipt($printer, $order, $domainSiteConfig, 'new'); }

		} // end print receipt

		// print order (kitchen)
		if ( $domainSiteConfig->printOrder != 0 && $orderInProcess != 9 ) {
			// get printer info
			$printer = printers::find($domainSiteConfig->printOrder);
			
			// print receipt
			if (!is_null($printer)) { $printOrder = PrintOrders::printOrder($printer, $order, $domainSiteConfig, 'new'); }

		} // end print order

		// send to foodRunner system if current delivery
		if ($domainSiteConfig->delivery == 'fr' && $orderInProcess != 9 && $order->orderType == 'deliver' ) {
			foodRunnerDelivery::schedule($domainSiteConfig, $order, $domainPaymentConfig->mode);
		}

	    return view('orders.viewOrder')->with([ 'html' => $order->emailData, 'domain' => $domain ]);


	} // end orderPayment



	/*---------------- Check Cart Items - Ensure Available  ----------------------------------*/

	public function checkCartItems(Request $request) {
		
		//check cart - make sure items still active before charge
		$data = [];
		$bad = [];
		$status = "checked";

		$cart = cartData::where('sessionID', '=', $request->sessionID)->get();
		if(count($cart)==0){
			$status = "noItem";
			$message = "Cart is blank";
			$data['status'] = $status;
			$data['message'] = $message;
			return $data;
		}
		foreach ($cart as $item) {
			if($item->menuData){
				if($item->menuData->menu){
					if ($item->menuData->menu->active == 'no'){
						$bad[] = $item->menuItem;
						$item->delete();
						$status = "badItem";
						continue;
					}
				}else{
					$bad[] = $item->menuItem;
					$item->delete();
					$status = "badItem";
					continue;
				}
				if($item->menuData->active == 0){
					$bad[] = $item->menuItem;
					$item->delete();
					$status = "badItem";
					continue;
				}
			}else {
				$bad[] = $item->menuItem;
				$item->delete();
				$status = "badItem";
				continue;
			}

			if($item->menuCategoriesData){
				if ($item->menuCategoriesData->status == 0) {
					$bad[] = $item->menuItem;
					$item->delete();
					$status = "badItem";
					continue;
				}
			}else{
				$bad[] = $item->menuItem;
				$item->delete();
				$status = "badItem";
				continue;
			}

		} //end foreach
		
		// get string list of removed items
		$first = reset($bad);
		$last = end($bad);
		foreach ($bad as $b) {
			if ($b == $first) {
				$list = $b;
			} elseif ($b == $last) {
				$list = $list . ', and ' . $b;
			} else {
				$list = $list . ', ' . $b;
			}
		} // end foreach

		if ($status != "checked") {
			if (count($bad) == 1) {
				$message = "The item " . $list . " was removed from your cart. The item is currently unavailable for purchase at this time. We apologize for this inconvenience. Please add additional items and/or resubmit your order for payment.";
			} else {
				$message = "The items " . $list . " were removed from your cart. The items are currently unavailable for purchase at this time. We apologize for this inconvenience. Please add additional items and/or resubmit your order for payment.";
			}
		} else {
			$message = "All items confirmed";
		}

		$data['status'] = $status;
		$data['message'] = $message;
		
		return $data;

	} // end checkCartItems




	/*---------------- Card Connect AuthCapture - credit card  ----------------------------------*/

	public function authCapture(Request $request) {
		//credit card only
		$res = [];
		// duplicate transaction check
		$dup = $this->duplicateTransactionCheck($request->domainID, $request->sessionID);

		if ($dup) {
			// duplicate transaction
			$res["status"] = "duplicate";
			$res['message'] ='A order for this session has been placed. To place a order please return to the menu and clear your cart then place your order.';
			return $res;
		}
		
		//proceed with transaction
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $request->domainID)->first();
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);

		// card connect expiry format MMYY
		if ($request->expiryMonth < 10) {
			$expiryMonth = '0' . $request->expiryMonth;
		} else {
			$expiryMonth = $request->expiryMonth;
		}

		$expiry = $expiryMonth . $request->expiryYear;
		
		// cardConnect authCapture data
		$data = array(
			'merchid' => $merchantID,
			'amount' => intval($request->amount*100),
			'account' => $request->ccToken,
			'expiry' => $expiry,
			'cvv2' => $request->cvv2,
			'orderid' => uniqid($request->domainID),
			'capture' => 'Y',
			'currency' => 'USD',
			'ecomind' => 'E'
		);

		if($request->postalCode){
			$data['postal'] = $request->postalCode;
		}
		
		// cardConnect requirement -> $type, $url, $credentials, $data
		// type authCapture
		$typeConfig = config('services.cardConnectURL')['authCapture'];
		$url = $this->getBaseURL($domainPaymentConfig) . $typeConfig['endpoint'];
		
		$charge = $this->cardConnect($typeConfig['type'], $url, $this->getCredentials($domainPaymentConfig), $data);
		//return $charge;
		
		if ($charge['status'] == 'error' || $charge['data'] == '') {
			$res["status"] = "error";
			$res['message'] = ' There was a communication problem with the credit card processor. Please try again.';
			return $res;
		} 
		
		$jsonCharge = json_decode($charge['data']);
		
		if ($jsonCharge->respstat == "A") {
			// charge approved
			$ccd = new cardConnectDetail();
				$ccd->domainID = $request->domainID;
				$ccd->merchid = $jsonCharge->merchid;
				$ccd->sessionID = $request->sessionID;
				$ccd->type = $typeConfig['type'];
				$ccd->endpoint = $typeConfig['endpoint'];
				$ccd->ccOrderID = $data['orderid'];
				$ccd->retref = $jsonCharge->retref;
				$ccd->respstat = $jsonCharge->respstat;
				$ccd->resptext = $jsonCharge->resptext;
				$ccd->respcode = $jsonCharge->respcode;
				$ccd->amount = $jsonCharge->amount;
				$ccd->avsresp = $jsonCharge->avsresp;
				$ccd->cvvresp = $jsonCharge->cvvresp;
				$ccd->authcode = $jsonCharge->authcode;
				$ccd->expiry = $jsonCharge->expiry;
				$ccd->token = $jsonCharge->token;
				$ccd->respproc = $jsonCharge->respproc;
				$ccd->ipAddress = $request->ipAddress ?? null;
				$ccd->rawResponse = $charge['data'];
			$ccd->save();

			$res['status'] = "approved";
			$res['message'] = 'Transaction Approved';
			$res['ccdID'] = $ccd->id;
			$res['cardConnectDetailID'] = $ccd->id;
			$res['data'] = $charge;

		} else {
			// charge declined
			$res["status"] = "declined";
			$res['message'] =$jsonCharge->resptext;
			$res['data'] = $charge;
		}
		return $res;

	} // end authCapture


	public function orderTokenizePayment(Request $request)
	{
		return view('orders.orderTokenizePayment');
	} // end orderTokenizePayment

	public function cardsecureTokenizePayment(Request $request)
	{
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->parentDomainID)->first();
		$type = 'POST';
		$typeConfig = config('services.cardConnectURL')['authCapture'];
		// if ($domainPaymentConfig->mode == 'live') {
		// 	$tokenizeURL = config('services.cardConnect')["liveTokenizeURL"];
		// } else {
		// 	$tokenizeURL = config('services.cardConnect')["testTokenizeURL"];
		// } 
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);
		$credentials = $this->getCredentials($domainPaymentConfig);
		// $tokenize = $this->cardConnect('POST', $tokenizeURL, $credentials, array('account' => $request->token));
		// $tokenizeData = json_decode($tokenize['data']);
		// cardConnect authCapture data
		$data = array(
			'merchid' => $merchantID,
			'amount' => $request->amount*100,
			'account' => $request->token,
			'expiry' => $request->expiry,
			// 'cvv2' => null,
			'capture' => 'Y',
			'currency' => 'USD',
			'ecomind' => 'E'
		);
		
		$typeConfig = config('services.cardConnectURL')['auth'];
		$url = $this->getBaseURL($domainPaymentConfig) . $typeConfig['endpoint'];
		
		$charge = $this->cardConnect('PUT', $url, $credentials, $data);
	
		return $charge;
	} // end cardsecureTokenizePayment


	/*---------------- Card Connect refundCharge  ----------------------------------*/

	public function refundCharge(Request $request) {


		$order = orders::findOrFail($request->orderID);

		// manager PIN check
		$chk = managerPIN::where([['domainID', '=', $order->domainID], ['userID', '=', $request->userID], ['managerPIN', '=', $request->managerPIN]])->first();
		
		// manager pin check
		if (is_null($chk)) {
			$data=[];
			$data['status'] = 'error';
			$data['message'] = 'The Manager PIN was not correct. Please enter the correct PIN.';
			return $data;
		}

		// refund amount check
		if ($order->refundAmount + $request->refundAmount > $order->total) {
			$data=[];
			$data['status'] = 'error';
			$data['message'] = 'The total refund amount cannot exceed the original amount charged.';
			return $data;
		}

		// inquire transaction - determine void or refund
		$inquire = $this->inquire($order->domainID, $order->ccTypeTransactionID);

		if ($inquire['status'] == 'error') {
			$data=[];
			$data['status'] = 'error';
			$data['message'] = $inquire['data'];
			return $data;
		}

		$jsonInquire = $inquire['data'];
		if ($order->orderRefundAmount + $request->refundAmount < $order->total && $jsonInquire->voidable == "Y") {
			// partial refund before settlement
			$refund = $this->refund($order->domainID, $order->ccTypeTransactionID, $request->refundAmount);
		} elseif ($jsonInquire->refundable == 'Y') {
			// transaction refundable
			$refund = $this->refund($order->domainID, $order->ccTypeTransactionID, $request->refundAmount);
		} elseif ($jsonInquire->voidable == "Y") {
			// transaction voidable - full transaction amount
			$refund = $this->void($order->domainID, $order->ccTypeTransactionID, $request->refundAmount);
		} else {
			$refund['status'] = 'error';
			$refund['message'] = 'There was an error processing the refund. Please check with Card Connect.';
		}

		// update order table
		if ($refund['status'] == "success") {
			$order->orderRefunded = 1;
			if ($order->total == $order->orderRefundAmount + $request->refundAmount) {
				$order->orderInProcess = 0;
			}
			$order->orderRefundID = $refund['code'];
			$order->orderRefundAmount = $order->orderRefundAmount + $request->refundAmount;
			$order->save();
		}

		 return $refund;

	}


	/*---------------- Card Connect Inquire  ----------------------------------*/

	public function inquire($domainID, $retref) {
		
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domainID)->first();
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);

		// cardConnectNoData($type, $url, $credentials
		// type inquire
		$typeConfig = config('services.cardConnectURL')['inquire'];
		$url = $this->getBaseURL($domainPaymentConfig) . $typeConfig['endpoint'] . "/" . $retref . "/" . $merchantID;
		$inquire = $this->cardConnectNoData($typeConfig['type'], $url, $this->getCredentials($domainPaymentConfig));
		
		//return $inquire;
		if ($inquire['status'] == 'error') {
			$res["status"] = "error";
			$res['data'] = ' There was a communication problem with the credit card processor. Please try again.';
			return $res;
		} 

		$jsonInquire = json_decode($inquire['data']);
		
		if ($jsonInquire->respproc == "PPS") {
			$res["status"] = "error";
			$res['data'] = 'There was a problem locating the transaction. Please check the transaction with Card Connect.';
			return $res;
		}

		$res["status"] = "success";
		$res['data'] = $jsonInquire;
		return $res;

	} // end inquire



	/*---------------- Card Connect Void  ----------------------------------*/

	public function void($domainID, $retref, $amount) {

		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domainID)->first();
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);

		// cardConnect authCapture data
		$data = array(
			'merchid' => $merchantID,
			'amount' => intval($amount * 100),
			'retref' => $retref
		);

		// cardConnect requirement -> $type, $url, $credentials, $data
		// type refund
		$typeConfig = config('services.cardConnectURL')['void'];
		$url = $this->getBaseURL($domainPaymentConfig) . $typeConfig['endpoint'];
		$void = $this->cardConnect($typeConfig['type'], $url, $this->getCredentials($domainPaymentConfig), $data);
		
		//return $void;
		if ($void['status'] == 'error') {
			$res["status"] = "error";
			$res['message'] = ' There was a communication problem with the credit card processor. Please try again.';
			return $res;
		} 

		$jsonVoid = json_decode($void['data']);

		if ($jsonVoid->respstat == "A") {
			$status = "success";
			$message = "The refund was successfully processed.";
			$code = $jsonVoid->retref;
		} else {
			$status = 'error';
			$message = 'There was an error refunding the charge. Please try again or check with Card Connect.';
			$code = $jsonVoid->resptext;
		}

		$data=[];
		$data['status'] = $status;
		$data['message'] = $message;
		$data['code'] = $code;

		return $data;

	} // end refundCharge



	/*---------------- Card Connect Refund  ----------------------------------*/

	public function refund($domainID, $retref, $amount) {

		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domainID)->first();
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);

		// cardConnect authCapture data
		$data = array(
			'merchid' => $merchantID,
			'amount' => intval($amount * 100),
			'retref' => $retref
		);

		// cardConnect requirement -> $type, $url, $credentials, $data
		// type refund
		$typeConfig = config('services.cardConnectURL')['refund'];
		$url = $this->getBaseURL($domainPaymentConfig) . $typeConfig['endpoint'];
		$refund = $this->cardConnect($typeConfig['type'], $url, $this->getCredentials($domainPaymentConfig), $data);
		
		//return $refund;
		if ($refund['status'] == 'error') {
			$res["status"] = "error";
			$res['message'] = ' There was a communication problem with the credit card processor. Please try again.';
			return $res;
		} 

		$jsonRefund = json_decode($refund['data']);

		if ($jsonRefund->respstat == "A") {
			$status = "success";
			$message = "The refund was successfully processed.";
			$code = $jsonRefund->retref;
		} else {
			$status = 'error';
			$message = 'There was an error refunding the charge. Please try again or check with Card Connect.';
			$code = $jsonRefund->resptext;
		}

		$data=[];
		$data['status'] = $status;
		$data['message'] = $message;
		$data['code'] = $code;
		
		return $data;

	} // end refundCharge


    /*---------------- Get Card Connect Response  ----------------------------------*/
    public function cardConnect($type, $url, $credentials, $data) {
		/*
		$type = PUT, GET, etc.
		$url = complete endpoint url
		$credentials = base64 encoded username:password
		$data = data array
		*/

		$resCC = [];
		$jsonData = json_encode($data);
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_CUSTOMREQUEST => $type,
			CURLOPT_POSTFIELDS => $jsonData,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic " . $credentials,
				"Cache-Control: no-cache",
				"Content-Length: " . strlen($jsonData),
				"Content-Type: application/json",
			),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		if ($err) {
			$resCC['status'] = "error";
			$resCC['data'] = "Error #:" . $err;
			return $resCC;
		} else {
			$resCC['status'] = "success";
			$resCC['data'] = $response;
			return $resCC;
		}

	} // end cardConnect



	/*---------------- Get Card Connect Response - No Data  ----------------------------------*/
	public function cardConnectNoData($type, $url, $credentials) {
		/*
		$type = PUT, GET, etc.
		$url = complete endpoint url
		$credentials = base64 encoded username:password
		*/
		
		$resCC = [];
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_CUSTOMREQUEST => $type,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic " . $credentials,
				"Cache-Control: no-cache",
				"Content-Type: application/json",
			),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		if ($err) {
			$resCC['status'] = "error";
			$resCC['data'] = "Error #:" . $err;
			return $resCC;
		} else {
			$resCC['status'] = "success";
			$resCC['data'] = $response;
			return $resCC;
		}

	} // end cardConnect




	/*---------------- Duplicate Transaction Check  ----------------------------------*/

	public function duplicateTransactionCheck($domainID, $sessionID) {

		$dupCheck = cardConnectDetail::where('domainID', '=', $domainID)->where('sessionID', '=', $sessionID)->first();

		if (is_null($dupCheck)) {
			// not duplicate transaction
			return false;
		} else {
			//duplicate transaction
			return true;
		}

	} // end duplicateTransactionCheck

	/*---------------- Get Merchant ID  ----------------------------------*/

	public function getMerchantID($mode, $mid) {
	
		if ($mode == 'test') {
			$merchantID = config('services.cardConnect')["testMID"];
		} else {
			$merchantID = $mid;
		}

		if (is_null($merchantID)) { abort(400); }
		
		return $merchantID;

	} // end getMerchantID




    /*---------------- Get Credentials  ----------------------------------*/

    public function getCredentials($domainPaymentConfig) {
		
		if ($domainPaymentConfig->mode == 'live') {
			$ccUser = $domainPaymentConfig->togoLiveUser;
			$ccPassword = decrypt($domainPaymentConfig->togoLivePassword);
			if (is_null($ccUser) || is_null($ccPassword)) { abort(400); }
			$credentials = base64_encode($ccUser . ":" . $ccPassword);	
		} else {
			$ccUser = config('services.cardConnect')["testUsername"];
			$ccPassword = config('services.cardConnect')["testPassword"];
			$credentials = base64_encode($ccUser . ":" . $ccPassword);
		}
		
        return $credentials;

	} // end getCredentials



	/*---------------- Get Base URL  ----------------------------------*/

	public function getBaseURL($domainPaymentConfig) {
	
		if ($domainPaymentConfig->mode == 'live') {
			$baseURL = config('services.cardConnect')["liveURL"];
		} else {
			$baseURL = config('services.cardConnect')["testURL"];
		} 
		
		return $baseURL;

	} // end getCredentials

	

	/*---------------- Card Connect Transaction Fees   ----------------------------------*/
		
	public function calcCCFee($ccPercent, $ccFixed, $total) {

		$variableFee = number_format($total * $ccPercent/100, 2);
		$fixedFee = number_format($ccFixed, 2);
		$ccFee = $variableFee + $fixedFee;

		return $ccFee;

	} // end calcCCFee



	/*---------------- Card Connect BIN Service   ----------------------------------*/
	
	public function ccBIN($domainPaymentConfig, $token, $userID, $contactID, $expireMonth, $expireYear, $amount) {
		//https://<site>.cardconnect.com/cardconnect/rest/bin/<merchid>/<token>
		
		$config = config('services.cardConnectURL')['bin'];
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);
		$baseURL = $this->getBaseURL($domainPaymentConfig);
		$url = $baseURL . $config['endpoint'] . '/' . $merchantID . '/' . $token;

		//cardConnectNoData($type, $url, $credentials)
		$bin = $this->cardConnectNoData( $config['type'], $url, $this->getCredentials($domainPaymentConfig) );

		$jsonBin = json_decode($bin['data']);
		if (!isset($jsonBin->errormsg)) {

			// update or create credit card based on token and parent domainID
			$binData = ccBIN::updateOrCreate([
				'token' => $token,
				'domainID' => $domainPaymentConfig->domainID
				], [
				'userID' => $userID,
				'contactID' => $contactID,
				'expireMonth' => $expireMonth,
				'expireYear' => $expireYear,
				'last4' => substr($token, -4),
				'country' => $jsonBin->country,
				'product' => $jsonBin->product,
				'brand' => $this->cardBrand($jsonBin->product),
				'bin' => $jsonBin->bin,
				'purchase' => $jsonBin->purchase,
				'prepaid' => $jsonBin->prepaid,
				'issuer' => $jsonBin->issuer,
				'cardusestring' => $jsonBin->cardusestring,
				'funding' => $this->debitOrCredit($jsonBin->cardusestring),
				'gsa' => $jsonBin->gsa,
				'corporate' => $jsonBin->corporate,
				'fsa' => $jsonBin->fsa,
				'subtype' => $jsonBin->subtype,
				'binlo' => $jsonBin->binlo,
				'binhi' => $jsonBin->binhi,
				'lastAmount' => $amount,
				'lastDate' => Carbon::now(),
			]);

			$jsonData['status'] = 'success';
			$jsonData['data'] = $binData;

		} else {

			$jsonData['status'] = 'error';
			$jsonData['data'] = $jsonBin->errormsg;

		}

		return $jsonData;

	} // end ccBIN



	/*---------------- Card Connect Brand   ----------------------------------*/
	
	public function cardBrand($product) {
		//https://developer.cardconnect.com/cardconnect-api#bin
		
		if ($product == "A") {
			return 'American Express';
		} elseif ($product == 'D') {
			return 'Discover';
		} elseif ($product == 'M') {
			return 'MasterCard';
		} elseif ($product == 'N') {
			return 'Non-Branded Debit Card';
		} elseif ($product == 'V') {
			return 'Visa';
		} else {
			return 'Other';
		}

	} // end cardBrand



	/*---------------- Card Connect Debit Or Credit Card   ----------------------------------*/

	public function debitOrCredit($cardusestring) {
		//https://developer.cardconnect.com/cardconnect-api#bin
		
		if (!strpos($cardusestring, 'Debit')) {
			return 'credit';
		} else {
			return 'debit';
		}

	} // end debitOrCredit



	/*---------------- Delete card connect card ----------------------------------*/

	protected function deletecard(Request $request) {
		
		$domain = domain::find($request->domainID);
		$card = ccBIN::where( 'userID', '=', $request->userID )->where('domainID', '=', $domain->parentDomainID)->first();
		if (!is_null($card)) {
			$card->userID = null;
			$card->save();
		}

		return "Your payment method was successfully deleted.";

	} // end deleteItem

	/*---------------- Card Connect Payment  ----------------------------------*/

	public function ccPayment(Request $request) {
		//credit card only
		$res = [];

		$ipAddress = \Request::ip();

		//proceed with transaction
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $request->did)->first();
		$merchantID = $this->getMerchantID($domainPaymentConfig->mode, $domainPaymentConfig->togoMerchantID);

		// card connect expiry format MMYY
		if ($request->expireMonth < 10) {
			$expireMonth = '0' . $request->expireMonth;
		} else {
			$expireMonth = $request->expireMonth;
		}

		$expiry = $expireMonth . $request->expireYear;

		// cardConnect authCapture data
		$data = array(
			'merchid' => $merchantID,
			'amount' => intval($request->total*100),
			'account' => $request->ccToken,
			'expiry' => $expiry,
			'cvv2' => $request->cvv2,
			'orderid' => uniqid($request->did),
			'capture' => 'Y',
			'currency' => 'USD',
			'ecomind' => 'E'
		);

		if($request->postalCode){
			$data['postal'] = $request->postalCode;
		}

		// cardConnect requirement -> $type, $url, $credentials, $data
		// type authCapture
		$typeConfig = config('services.cardConnectURL')['authCapture'];
		$url = $this->getBaseURL($domainPaymentConfig) . $typeConfig['endpoint'];
		$charge = $this->cardConnect($typeConfig['type'], $url, $this->getCredentials($domainPaymentConfig), $data);
		//return $charge;
		if ($charge['status'] == 'error' || $charge['data'] == '') {
			$res["status"] = "error";
			$res['message'] = ' There was a communication problem with the credit card processor. Please try again.';
			return $res;
		} 

		$jsonCharge = json_decode($charge['data']);

		if ($jsonCharge->respstat == "A") {
			// charge approved
			$ccd = new cardConnectDetail();
				$ccd->domainID = $request->did;
				$ccd->merchid = $jsonCharge->merchid;
				$ccd->sessionID = $request->sid;
				$ccd->type = $typeConfig['type'];
				$ccd->endpoint = $typeConfig['endpoint'];
				$ccd->ccOrderID = $data['orderid'];
				$ccd->retref = $jsonCharge->retref;
				$ccd->respstat = $jsonCharge->respstat;
				$ccd->resptext = $jsonCharge->resptext;
				$ccd->respcode = $jsonCharge->respcode;
				$ccd->amount = $jsonCharge->amount;
				$ccd->avsresp = $jsonCharge->avsresp;
				$ccd->cvvresp = $jsonCharge->cvvresp;
				$ccd->authcode = $jsonCharge->authcode;
				$ccd->expiry = $jsonCharge->expiry;
				$ccd->token = $jsonCharge->token;
				$ccd->respproc = $jsonCharge->respproc;
				$ccd->ipAddress = $ipAddress ?? null;
				$ccd->rawResponse = $charge['data'];
			$ccd->save();

			$res['status'] = "approved";
			$res['message'] = 'Transaction Approved';
			$res['ccdID'] = $ccd->id;
			$res['cardConnectDetailID'] = $ccd->id;
			$res['data'] = $charge;

		} else {
			// charge declined
			$res["status"] = "declined";
			$res['message'] =$jsonCharge->resptext;
			$res['data'] = $charge;
		}
		
		return $res;

	} // end ccPayment

	/*---------------- Food Runner Order  ----------------------------------*/

	private function foodRunner($data, $token) {
		$url = env('FOOD_RUNNER_URL');
		$jsonData = json_encode($data);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);

		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = "Authorization: Bearer $token";
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// dd($headers);
		$response = curl_exec($ch);
		//dd($response);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);

		return $response;
		
	} // end foodRunner


} // end cardConnectController
