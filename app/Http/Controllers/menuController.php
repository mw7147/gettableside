<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\menuCategoriesData;
use App\selectTimeDisplay;
use App\menuOptionsData;
use App\menuCategories;
use App\menuAddOnsData;
use App\menuSidesData;
use App\menuOptions;
use App\menuAddOns;
use App\menuSides;
use App\menuData;
use App\domain;
use App\menu;
use App\tags;

class menuController extends Controller
{
    
    /*-------------------- Menu Dashboard  ----------------------------------------*/
    public function dashboard(Request $request) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];


        return view('menu.managerDashboard')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);

    } //end dashboard


    /*-------------------- Menu List  ----------------------------------------*/
    public function menuList($urlPrefix) {

        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');

        // make sure correct domain
        if ($authLevel >=  40) {
            $menus = menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.name')
            ->get();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $menus = menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*',  'domains.name')
            ->where('menu.domainID', '=', $domain->id)
            ->get();
        } else {
            $menus = menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*',  'domains.name')
            ->where('menu.domainID', '=', $domain->id)
            ->get();
            //$menus = menu::where('domainID', '=', $domain->id)->get();
        }
        
        if (is_null($menus)) {abort(404);}
        

        return view('menu.menu')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menus' => $menus]);

    } //end menuList




        /*-------------------- Menu Delete  ----------------------------------------*/
        public function menuDelete($urlPrefix, $menuID) {
            
            $authLevel = \Request::get('authLevel');
            if ($authLevel <  40) { abort(404); }
    
            $chk = menu::destroy($menuID);
    
            if ($chk) {
                session()->flash('deleteSuccess', 'The menu has been deleted.');
            } else {
                session()->flash('deleteError', 'There was an error deleting the menu. Please try again.');     
            }
    
            return redirect()->action('menuController@menuList', ['urlPrefix' => $urlPrefix]);        
    
        } //end menuDelete




    /*-------------------- Menu Add New  ----------------------------------------*/
    public function menuAddNew($urlPrefix) {

        $authLevel = \Request::get('authLevel');
        if ($authLevel <  30) { abort(404); }
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $domains = $this->getDomains($domain, $authLevel);   

        return view('menu.menuAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'domains' => $domains]);

    } //end menuAddNew

    
    
    /*-------------------- Menu Add New  ----------------------------------------*/
    public function menuAddNewSave(Request $request, $urlPrefix) {

        $authLevel = \Request::get('authLevel');
        if ($authLevel <  30) { abort(404); }

        $m = new menu();
            $m->domainID = $request->domainID;
            $m->active = 'no';
            $m->defaultMenu = 'no';
            $m->menuCategoriesID = 0;
            $m->menuName = $request->menuName;
            $m->menuDescription = $request->menuDescription;
        $chk = $m->save();

        if ($chk) {
            session()->flash('updateSuccess', 'The menu has been created.');
        } else {
            session()->flash('updateError', 'There was an error creating the menu. Please try again.');     
        }

        return redirect()->action('menuController@menuList', ['urlPrefix' => $urlPrefix]);        

    } //end menuAddNewSave



    /*-------------------- Menu Edit  ----------------------------------------*/
    public function menuEdit($urlPrefix, $menuID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');

        // make sure correct domain
        if ($authLevel >= 40) {
            $menu =  menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.parentDomainID', 'domains.name')
            ->where('menu.id', '=', $menuID)            
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $menu = menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*',  'domains.name')
            ->where([['menu.id', '=', $menuID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $menu = menu::where([['id', '=', $menuID],['domainID', '=', $domain->id]])->first();
        }

        if (is_null($menu)) {abort(404);}

        $breadcrumb = ['menu', ''];
        $categories = menuCategories::select('id', 'name')->where('domainID', '=', $menu->domainID)->get();
        $timeDisplay = selectTimeDisplay::all();

        return view('menu.menuDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menu' => $menu, 'categories' => $categories, 'timeDisplay' => $timeDisplay]);

    } //end menuEdit


    /*-------------------- Menu Edit Save ----------------------------------------*/
    public function menuEditSave(Request $request, $urlPrefix, $menuID) {

        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $menu =  menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.parentDomainID', 'domains.name')
            ->where('menu.id', '=', $menuID)            
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $menu = menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.id', 'menu.active', 'menu.active', 'menu.domainID', 'menu.menuName', 'menu.menuDescription', 'menu.menuCategoriesID',  'domains.name')
            ->where([['menu.id', '=', $menuID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $menu = menu::where([['id', '=', $menuID],['domainID', '=', $domain->id]])->first();
        }

        if (is_null($menu)) {abort(404);}
        
        if ($request->defaultMenu == 'yes') {
            // set only 1 default menu - set all to no - update $menu model w yes
            $setDefault = menu::where('domainID', '=', $domain->id)->update(['defaultMenu' => 'no']);
        }

        $menu->menuName = $request->menuName;
        $menu->menuDescription = $request->menuDescription;
        $menu->menuHeader = $request->menuHeader;
        $menu->showHeaderClosed = $request->showHeaderClosed;
        $menu->active = $request->active;
        $menu->defaultMenu = $request->defaultMenu;
        $menu->menuCategoriesID = $request->menuCategoriesID;
        $menu->startSunday = $request->startSunday;
        $menu->stopSunday = $request->stopSunday;
        $menu->startMonday = $request->startMonday;
        $menu->stopMonday = $request->stopMonday;
        $menu->startTuesday = $request->startTuesday;
        $menu->stopTuesday = $request->stopTuesday;
        $menu->startWednesday = $request->startWednesday;
        $menu->stopWednesday = $request->stopWednesday;
        $menu->startThursday = $request->startThursday;
        $menu->stopThursday = $request->stopThursday;
        $menu->startFriday = $request->startFriday;
        $menu->stopFriday = $request->stopFriday;
        $menu->stopSaturday = $request->stopSaturday;
        $menu->startSaturday = $request->startSaturday;
        $menu->save();
        
       // dd($menu);
        // Authenticatton Failed
        session()->flash('updateSuccess', 'The menu information has been updated.');
        $breadcrumb = ['menu', ''];
        $categories = menuCategories::select('id', 'name')->where('domainID', '=', $domain->id)->get();
        $timeDisplay = selectTimeDisplay::all();

        $request=[];

        return view('menu.menuDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menu' => $menu, 'categories' => $categories, 'timeDisplay' => $timeDisplay]);       

        

    } //end menuEditSave


    /*-------------------- Menu Item List  ----------------------------------------*/
    public function menuItem(Request $request, $urlPrefix) {

        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');

        if ($authLevel >= 40) {
            $menus = domain::leftJoin('menu', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.parentDomainID', 'domains.name')
            ->get();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $menus = domain::leftJoin('menu', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.name')
            ->where('menu.domainID', '=', $domain->id)            
            ->get();
        } else {
            $menus = domain::leftJoin('menu', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.name')
            ->where('menu.domainID', '=', $domain->id)            
            ->get();
        }
        
        // get item count
        $menuCount = [];
        foreach ($menus as $menu) {
            $menuCount[$menu->id] = menuData::where('menuID', '=', $menu->id)->count();
        } // end foreach
            
        return view('menu.menuItemsDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menus' => $menus, 'menuCount' => $menuCount]);

    } //end menuList

    /*-------------------- Menu Items Edit  ----------------------------------------*/
    public function menuItemsEdit($urlPrefix, $menuID) {
 
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        $breadcrumb = ['menu', ''];
        $user = Auth::user();
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $menu =  menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*', 'domains.name')
            ->where('menu.id', '=', $menuID)            
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $menu = menu::leftJoin('domains', 'menu.domainID', '=', 'domains.id')
            ->select('menu.*',  'domains.name')
            ->where([['menu.id', '=', $menuID],['menu.domainID', '=', $domain->id]])
            ->first();
        } else {
            $menu = menu::where([['id', '=', $menuID],['domainID', '=', $domain->id]])->first();
        }

        if (is_null($menu)) {abort(404);}
        
        $menuItems = menuData::where('menuID', '=', $menu->id)->get();
        $categories = $this->getCategories($menu->menuCategoriesID);
  
        return view('menu.menuItemsEdit')->with(['domain' => $domain, 'user' => $user, 'breadcrumb' => $breadcrumb, 'menu' => $menu, 'menuItems' => $menuItems, 'categories' => $categories]);

    } //end menuItemsEdit



    /*-------------------- Menu Items Detail Delete  ----------------------------------------*/
    public function menuItemsDetailDelete($urlPrefix, $menuDataID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        $user = Auth::user();
        if ( $authLevel < 35 ) { abort(405); }
        if ($authLevel == 35 ) {
            // check to make sure user is only on auth domain
            if ($domain->id != $user->domainID ) { abort(403); }
        }
        
        $menuData = menuData::findOrFail($menuDataID);
        $menuID = $menuData->menuID;
        $chk = $menuData->delete();
        if ($chk) {
            session()->flash('updateSuccess', 'The menu item has been deleted.');
        } else {
            session()->flash('updateError', 'There was an error deleting the menu item. Please try again.');     
        }

        return redirect()->route('menuItems', ['urlPrefix' => $urlPrefix, 'menuID' => $menuID]);

    } //end menuItemsDetailDelete



    /*-------------------- Menu Items Detail Edit ----------------------------------------*/
    public function menuItemsDetailEdit($urlPrefix, $menuDataID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >=  40) {
            $menuData = menuData::leftJoin('domains', 'menuData.domainID', '=', 'domains.id')
            ->select('menuData.*',  'domains.name')
            ->where('menuData.id', '=', $menuDataID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $menuData = menuData::leftJoin('domains', 'menuData.domainID', '=', 'domains.id')
            ->select('menuData.*',  'domains.name')
            ->where([['menuData.id', '=', $menuDataID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $menuData = menuData::where([['id', '=', $menuDataID],['domainID', '=', $domain->id]])->first();
        }

        if (is_null($menuData)) {abort(404);}

        $menuCategoriesID = menu::where('id', '=', $menuData->menuID)->pluck('menuCategoriesID')->first();
        $categories = menuCategoriesData::select('id', 'categoryName')->where('menuCategoriesID', '=', $menuCategoriesID)->get();
        $breadcrumb = ['menu', ''];

        $menuSides = menuSides::select('id', 'name')->where('domainID', '=', $domain->id)->get();
        $menuOptions = menuOptions::select('id', 'name')->where('domainID', '=', $domain->id)->get();
        $menuAddOns = menuAddOns::select('id', 'name')->where('domainID', '=', $domain->id)->get();

        $tags = tags::where('domainID', '=', $menuData->domainID)->orderBy('tagName')->get();

        return view('menu.menuItemsEditDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menuData' => $menuData, 'categories' => $categories, 'menuSides' => $menuSides, 'menuOptions' => $menuOptions, 'menuAddOns' => $menuAddOns, 'tags' => $tags]);

    } //end menuItemsDetailEdit




        /*-------------------- Menu Items Detail Edit ----------------------------------------*/
        public function menuItemsDetailEditSave(Request $request, $urlPrefix, $menuDataID) {
            
            $domain = \Request::get('domain');
            $authLevel = \Request::get('authLevel');
            
            // make sure correct domain
            if ($authLevel >=  40) {
                $menuData = menuData::leftJoin('domains', 'menuData.domainID', '=', 'domains.id')
                ->select('menuData.*',  'domains.name')
                ->where('menuData.id', '=', $menuDataID)
                ->first();
            } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
                $menuData = menuData::leftJoin('domains', 'menuData.domainID', '=', 'domains.id')
                ->select('menuData.*',  'domains.name')
                ->where([['menuData.id', '=', $menuDataID],['domains.parentDomainID', '=', $domain->id]])
                ->first();
            } else {
                $menuData = menuData::where([['id', '=', $menuDataID],['domainID', '=', $domain->id]])->first();
            }

            if (is_null($menuData)) {abort(404);}

                // set Sides, Options and AddOns to 0 if none selected
                if (isset($request->menuSidesID)) {
                    $menuSidesID = $request->menuSidesID;
                } else {
                    $menuSidesID[0] = 0;
                }

                if (isset($request->menuOptionsID)) {
                    $menuOptionsID = $request->menuOptionsID;
                } else {
                    $menuOptionsID[0] = 0;
                }

                if (isset($request->menuAddOnsID)) {
                    $menuAddOnsID = $request->menuAddOnsID;
                } else {
                    $menuAddOnsID[0] = 0;
                }

                $menuData->active = $request->active;
                $menuData->menuCategoriesDataID = $request->menuCategoriesDataID;
                $menuData->sortOrder = $request->sortOrder;
                $menuData->tagTier1 = $request->tagTier1;
                $menuData->tagTier2 = $request->tagTier2;
                $menuData->tagTier3 = $request->tagTier3;
                $menuData->spiceLevel = $request->spiceLevel;
                $menuData->menuItem = $request->menuItem;
                $menuData->menuItemDescription = $request->menuItemDescription;
                $menuData->printKitchen = $request->printKitchen;
                $menuData->printReceipt = $request->printReceipt;
                $menuData->itemTax = $request->itemTax;
                $menuData->itemTaxRate = $request->itemTaxRate;
                $menuData->portion1 = $request->portion1;
                $menuData->price1 = $request->price1;
                $menuData->portion2 = $request->portion2;
                $menuData->price2 = $request->price2;
                $menuData->portion3 = $request->portion3;
                $menuData->price3 = $request->price3;
                $menuData->portion4 = $request->portion4;
                $menuData->price4 = $request->price4;
                $menuData->portion5 = $request->portion5;
                $menuData->price5 = $request->price5;
                $menuData->portion6 = $request->portion6;
                $menuData->price6 = $request->price6;
                $menuData->portion7 = $request->portion7;
                $menuData->price7 = $request->price7;
                $menuData->menuSidesID = implode(',', $menuSidesID);
                $menuData->menuOptionsID = implode(',', $menuOptionsID);
                $menuData->menuAddOnsID = implode(',', $menuAddOnsID);

                /** Dietary Options */
                $menuData->soy = $request->soy ?? 0;
                $menuData->nut = $request->nut ?? 0;
                $menuData->vegan = $request->vegan ?? 0;
                $menuData->shellfish = $request->shellfish ?? 0;
                $menuData->meat = $request->meat ?? 0;
                $menuData->pork = $request->pork ?? 0;
                $menuData->lactose = $request->lactose ?? 0;
                $menuData->gluten = $request->gluten ?? 0;
                $menuData->egg = $request->egg ?? 0;
                
            $chk = $menuData->save();

            if ($chk) {
                session()->flash('updateSuccess', 'The menu item information has been updated.');
            } else {
                session()->flash('updateError', 'There was an error updating the menu item. Please try again.');     
            }

            return redirect()->action('menuController@menuItemsEdit', ['urlPrefix' => $urlPrefix, 'menuID' => $menuData->menuID]);        
    
        } //end menuItemsDetailEditSave




    /*-------------------- Add Menu Item ----------------------------------------*/
    public function menuAddItem(Request $request, $urlPrefix, $menuID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');        
        $breadcrumb = ['menu', ''];
        $menu = menu::find($menuID);

        if ($authLevel >= 40) {
            $menuCategoriesID = menu::where('id', '=', $menuID)->pluck('menuCategoriesID')->first();            
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
        //    $menuCategoriesID = menu::where([['id', '=', $menuID], ['parentDomainID', '=', $domain->id]])->pluck('menuCategoriesID')->first();
            $menuCategoriesID = menu::where([['id', '=', $menuID], ['domainID', '=', $domain->id]])->pluck('menuCategoriesID')->first();
        } else {
            $menuCategoriesID = menu::where([['id', '=', $menuID], ['domainID', '=', $domain->id]])->pluck('menuCategoriesID')->first();
        }
        if (is_null($menuCategoriesID)) {abort(404);}
        
        $categories = menuCategoriesData::select('id', 'categoryName')->where('menuCategoriesID', '=', $menuCategoriesID)->get();
        //dd($menuCategoriesID);
        $menuSides = menuSides::select('id', 'name')->where('domainID', '=', $domain->id)->get();
        $menuOptions = menuOptions::select('id', 'name')->where('domainID', '=', $domain->id)->get();
        $menuAddOns = menuAddOns::select('id', 'name')->where('domainID', '=', $domain->id)->get();

        $tags = tags::where('domainID', '=', $menu->domainID)->orderBy('tagName')->get();
        
        return view('menu.menuItemsNewDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'categories' => $categories, 'menuSides' => $menuSides, 'menuOptions' => $menuOptions, 'menuAddOns' => $menuAddOns, 'menuID' => $menuID, 'tags' => $tags]);

    } //end menuAddItem


    /*-------------------- Add Menu Item Save ----------------------------------------*/
    public function menuAddItemSave(Request $request, $urlPrefix, $menuID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];

        $menuData = new menuData();

            // set Sides, Options and AddOns to 0 if none selected
            if (isset($request->menuSidesID)) {
                $menuSidesID = $request->menuSidesID;
            } else {
                $menuSidesID[0] = 0;
            }

            if (isset($request->menuOptionsID)) {
                $menuOptionsID = $request->menuOptionsID;
            } else {
                $menuOptionsID[0] = 0;
            }

            if (isset($request->menuAddOnsID)) {
                $menuAddOnsID = $request->menuAddOnsID;
            } else {
                $menuAddOnsID[0] = 0;
            }

            $menuData->domainID = $domain->id;
            $menuData->menuID = $menuID;
            $menuData->sortOrder = $request->sortOrder;
            $menuData->spiceLevel = $request->spiceLevel;
            $menuData->active = $request->active;
            $menuData->menuCategoriesDataID = $request->menuCategoriesDataID;
            $menuData->menuItem = $request->menuItem;
            $menuData->tagTier1 = $request->tagTier1;
            $menuData->tagTier2 = $request->tagTier2;
            $menuData->tagTier3 = $request->tagTier3;
            $menuData->printKitchen = $request->printKitchen;
            $menuData->printReceipt = $request->printReceipt;
            $menuData->menuItemDescription = $request->menuItemDescription;
            $menuData->itemTax = $request->itemTax;
            $menuData->itemTaxRate = $request->itemTaxRate;
            $menuData->portion1 = $request->portion1;
            $menuData->price1 = $request->price1;
            $menuData->portion2 = $request->portion2;
            $menuData->price2 = $request->price2;
            $menuData->portion3 = $request->portion3;
            $menuData->price3 = $request->price3;
            $menuData->portion4 = $request->portion4;
            $menuData->price4 = $request->price4;
            $menuData->portion5 = $request->portion5;
            $menuData->price5 = $request->price5;
            $menuData->portion6 = $request->portion6;
            $menuData->price6 = $request->price6;
            $menuData->portion7 = $request->portion7;
            $menuData->price7 = $request->price7;
            $menuData->menuSidesID = implode(',', $menuSidesID);
            $menuData->menuOptionsID = implode(',', $menuOptionsID);
            $menuData->menuAddOnsID = implode(',', $menuAddOnsID);
            /** Dietary Options */
            $menuData->soy = $request->soy ?? 0;
            $menuData->nut = $request->nut ?? 0;
            $menuData->vegan = $request->vegan ?? 0;
            $menuData->shellfish = $request->shellfish ?? 0;
            $menuData->meat = $request->meat ?? 0;
            $menuData->pork = $request->pork ?? 0;
            $menuData->lactose = $request->lactose ?? 0;
            $menuData->gluten = $request->gluten ?? 0;
            $menuData->egg = $request->egg ?? 0;

        $chk = $menuData->save();

        if ($chk) {
            session()->flash('addOnSuccess', 'The menu item was successfully created.');
        } else {
            session()->flash('addOnError', 'There was an error creating the menu item. Please try again.');     
        }

        return redirect()->action('menuController@menuItemsEdit', ['urlPrefix' => $urlPrefix, 'menuID' => $menuID]);        
        
    } //end menuAddItemSave



    /*-------------------- Menu Categories  ----------------------------------------*/
    public function menuCategories($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $categories = menuCategories::join('domains', 'menuCategories.domainID', '=', 'domains.id')
            ->select('menuCategories.id', 'menuCategories.active', 'menuCategories.domainID', 'menuCategories.name', 'menuCategories.description', 'domains.parentDomainID', 'domains.name as location')
            ->get();  
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $categories = menuCategories::leftJoin('domains', 'menuCategories.domainID', '=', 'domains.id')
            ->select('menuCategories.id', 'menuCategories.active', 'menuCategories.domainID', 'menuCategories.name', 'menuCategories.description', 'domains.parentDomainID', 'domains.name as location')
            ->where('domains.parentDomainID', '=', $domain->id)            
            ->get();
        } else {
            $categories = menuCategories::leftJoin('domains', 'menuCategories.domainID', '=', 'domains.id')
            ->select('menuCategories.id', 'menuCategories.active', 'menuCategories.domainID', 'menuCategories.name', 'menuCategories.description', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuCategories.domainID', '=', $domain->id)            
            ->get();
        }

        return view('menu.menuCategories')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'categories' => $categories]);

    } //end menuCategories


    /*-------------------- Menu Categories Add New  ----------------------------------------*/
    public function menuCategoriesAddNew($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $domains = $this->getDomains($domain, $authLevel);

        return view('menu.menuCategoriesAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'domains' => $domains]);

    } //end menuCategoriesAddNew


    /*-------------------- Menu Categories Add New Save ----------------------------------------*/
    public function menuCategoriesAddNewSave(Request $request, $urlPrefix) {

        $new = new menuCategories();
            $new->domainID = $request->domainID;
            $new->active = $request->active;

            $new->name = $request->name;
            $new->type = $request->type;
            $new->description = $request->description;
        $chk = $new->save();

        if ($chk) {
            session()->flash('categorySuccess', 'The new category name was successfully created.');
        } else {
            session()->flash('categoryError', 'There was an error creating the category name. Please try again.');     
        }

        return redirect()->action('menuController@menuCategories', ['urlPrefix' => $urlPrefix]);


    } //end menuCategoriesAddNewSave



    /*-------------------- Menu Categories Edit ----------------------------------------*/
    public function menuCategoriesEdit($urlPrefix, $categoryID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        
        $authLevel = \Request::get('authLevel');
        $domains = $this->getDomains($domain, $authLevel);   

        // make sure correct domain
        if ($authLevel >= 40) {
            $category = menuCategories::where('id', '=', $categoryID)->first();              
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $category = menuCategories::leftJoin('domains', 'menuCategories.domainID', '=', 'domains.id')
            ->select('menuCategories.*',  'domains.name')
            ->where([['menuCategories.id', '=', $categoryID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $category = menuCategories::where([['domainID', '=', $domain->id], ['id', '=', $categoryID]])->first();
        }

        if (is_null($category)) {abort(404);}

        return view('menu.menuCategoriesEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'category' => $category, 'domains' => $domains]);
        
    } //end menuCategoriesEdit


    /*-------------------- Menu Categories Edit Save ----------------------------------------*/
    public function menuCategoriesEditSave(Request $request, $urlPrefix, $categoryID) {

        $cat = menuCategories::where('id', '=', $categoryID)->first();
            $cat->domainID = $request->domainID;
            $cat->active = $request->active;
            $cat->name = $request->name;
            $cat->type = $request->type;
            $cat->description = $request->description;
        $chk = $cat->save();

        if ($chk) {
            session()->flash('categorySuccess', 'The new category name was successfully updated.');
        } else {
            session()->flash('categoryError', 'There was an error updating the category name. Please try again.');     
        }

        return redirect()->action('menuController@menuCategories', ['urlPrefix' => $urlPrefix]);


    } //end menuCategoriesEditSave


    /*-------------------- Menu Categories Delete - Admin Only ----------------------------------*/
    public function menuCategoriesDelete(Request $request, $urlPrefix, $categoryID) {

        $authLevel = \Request::get('authLevel');                
        if ($authLevel < 40) {
            session()->flash('categoryError', 'Method not allowed - please contact the system administrator for assistance.'); 
            return redirect()->action('menuController@menuCategories', ['urlPrefix' => $urlPrefix]);
        }

        $domain = \Request::get('domain');
        $chk = menuCategories::destroy($categoryID);
  //      $md = menuData::where()

        if ($chk) {
            session()->flash('categorySuccess', 'The category was successfully deleted.');
        } else {
            session()->flash('categoryError', 'There was an error deleting the category. Please try again.');     
        }

        return redirect()->action('menuController@menuCategories', ['urlPrefix' => $urlPrefix]);


    } //end menuCategoriesDelete







    /*-------------------- Menu Categories Detail View  ----------------------------------------*/
    public function menuCategoriesDetails($urlPrefix, $categoryID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');        
        $breadcrumb = ['menu', ''];


        // make sure correct domain
        if ($authLevel >= 35) {
            $menuCategoriesData = menuCategoriesData::where('menuCategoriesID', '=', $categoryID)->get();
        } else {
            $menuCategoriesData = menuCategoriesData::where([['domainID', '=', $domain->id],['menuCategoriesID', '=', $categoryID]])->get();
        }

        if (is_null($menuCategoriesData)) {abort(404);}

        $totalItemCount = [];
        foreach ($menuCategoriesData as $mcd) {
            $totalItemCount[$mcd->id] = menuData::where([['menuCategoriesDataID', '=', $mcd->id],['active', '=', 1]])->count();
        }
        
        return view('menu.menuCategoriesDetails')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menuCategoriesData' => $menuCategoriesData, 'categoryID' => $categoryID, 'totalItemCount' => $totalItemCount]);

    } //end menuCategoriesDetails


    /*-------------------- Menu Categories Delete ----------------------------------*/
    public function menuCategoriesDetailsDelete(Request $request, $urlPrefix, $categoryDataID) {

        $domain = \Request::get('domain');
        $data = menuCategoriesData::find($categoryDataID);

        //delete image file
        if(!is_null($data->fileName)) {
            $path = env('PUBLIC_CATEGORY_IMAGES') . '/' . $domain->id . '/' . $data->menuCategoriesID . '/';
            Storage::delete($path . $data->fileName); 
        }

        $chk = menuCategoriesData::destroy($categoryDataID);

        if ($chk) {
            session()->flash('categorySuccess', 'The category was successfully deleted.');
        } else {
            session()->flash('categoryError', 'There was an error deleting the category. Please try again.');     
        }

        return redirect()->action('menuController@menuCategoriesDetails', ['urlPrefix' => $urlPrefix, 'categoryID' => $data->menuCategoriesID]);


    } //end menuCategoriesDetailsDelete


    /*-------------------- Menu Categories Details Add New  ----------------------------------------*/
    public function menuCategoriesDetailsAddNew($urlPrefix, $categoryID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];

        return view('menu.menuCategoriesDetailsAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'categoryID' => $categoryID]);

    } //end menuCategoriesDetailsAddNew


    /*-------------------- Menu Categories Details Add New Save ----------------------------------------*/
    public function menuCategoriesDetailsAddNewSave(Request $request, $urlPrefix, $categoryID) {

        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $errorMessage = '';
  
        if (!is_null($request->file('img'))) {
            // add new filelist($width, $height, $type, $attr) = getimagesize($request->file('img'));
            $fileName = $request->file('img')->getClientOriginalName();
            $fileName = str_replace(' ', '', $fileName);
            $fileSize = $request->file('img')->getSize(); // file size in Kilobytes
        
            // check filesize
            if ($fileSize/1000 > env('MAX_IMAGE_SIZE')) {
                $errorMessage= " Please check your image file. The filesize cannot be larger than " . number_format(env('MAX_IMAGE_SIZE')) . "Kb. Please edit try again.";
            }

            // check filetype
            $fileTypes = ['png', 'jpg', 'jpeg'];
            $fileExt = $request->file('img')->guessClientExtension(); // file type
            if (!in_array($fileExt, $fileTypes)) {
                $errorMessage = " Please check your image file. The menu image filetype must be a jpg or png. Please edit and try again.";
            }

            // file metadata
            $path = env('PUBLIC_CATEGORY_IMAGES') . '/' . $domain->id . '/' . $categoryID . '/';
            list($width, $height, $type, $attr) = getimagesize($request->file('img'));

            // save new file
            $saveFile = $request->file('img')->storeAs($path, $fileName);
        } else {
            // no image
            $width = null;
            $height = null;
            $fileExt = null;
            $fileSize = null;
            $fileName = null;
        } // end if isset img

        $new = new menuCategoriesData();
            $new->domainID = $domain->id;
            $new->sortOrder = $request->sortOrder;            
            $new->menuCategoriesID = $categoryID;
            $new->status = $request->active;
            $new->categoryName = $request->name;
            $new->categoryDescription = $request->description;
            $new->categoryTax = $request->categoryTax;
            $new->categoryTaxRate = $request->categoryTaxRate ?? 0;
            $new->imageWidth = $width;
            $new->imageHeight = $height;
            $new->imageType = $fileExt;
            $new->fileSize = $fileSize;
            $new->fileName = $fileName;
        $chk = $new->save();

        if ($chk) {
            session()->flash('categorySuccess', 'The new category was successfully created.' . $errorMessage);
        } else {
            session()->flash('categoryError', 'There was an error creating the category. Please try again.');     
        }

        return redirect()->action('menuController@menuCategoriesDetails', ['urlPrefix' => $urlPrefix, 'categoryID' => $categoryID]);


    } //end menuCategoriesDetailsAddNewSave


    /*-------------------- Menu Categories Details Edit ----------------------------------------*/
    public function menuCategoriesDetailsEdit($urlPrefix, $categoryID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];

        $category = menuCategoriesData::where([['domainID', '=', $domain->id], ['id', '=', $categoryID]])->first();

        return view('menu.menuCategoriesDetailsEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'category' => $category]);
        

    } //end menuCategoriesDetailsEdit


    /*-------------------- Menu Categories Edit Save ----------------------------------------*/
    public function menuCategoriesDetailsEditSave(Request $request, $urlPrefix, $categoryID) {

        $domain = \Request::get('domain');
        $cat = menuCategoriesData::where([['domainID', '=', $domain->id], ['id', '=', $categoryID]])->first();
            $cat->domainID = $domain->id;
            $cat->status = $request->active;
            $cat->sortOrder = $request->sortOrder;
            $cat->categoryName = $request->name;
            $cat->categoryDescription = $request->description;
            $cat->categoryTax = $request->categoryTax;
            $cat->categoryTaxRate = $request->categoryTaxRate;
        $chk = $cat->save();

        if ($chk) {
            session()->flash('categorySuccess', 'The category was successfully updated.');
        } else {
            session()->flash('categoryError', 'There was an error updating the category. Please try again.');     
        }

        return redirect()->action('menuController@menuCategoriesDetails', ['urlPrefix' => $urlPrefix, 'categoryID' => $cat->menuCategoriesID]);


    } //end menuCategoriesDetailsEditSave





    /*-------------------- Menu Sides  ----------------------------------------*/
    public function menuSides($urlPrefix) {

        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $sides = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->get();  
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $sides = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('domains.parentDomainID', '=', $domain->id)            
            ->get();
        } else {
            $sides = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuSides.domainID', '=', $domain->id)            
            ->get();
        }

        return view('menu.menuSides')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'sides' => $sides]);

    } //end menuSides


    /*-------------------- Menu Sides Add New  ----------------------------------------*/
    public function menuSidesAddNew($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $domains = $this->getDomains($domain, $authLevel);

        return view('menu.menuSidesAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'domains' => $domains]);

    } //end menuSidesAddNew


    /*-------------------- Menu Sides Add New Save ----------------------------------------*/
    public function menuSidesAddNewSave(Request $request, $urlPrefix) {

        $new = new menuSides();
            $new->domainID = $request->domainID;
            $new->numberSides = $request->numberSides;
            $new->active = $request->active;
            $new->name = $request->name;
            $new->description = $request->description;
        $chk = $new->save();

        if ($chk) {
            session()->flash('sideSuccess', 'The new side name was successfully created.');
        } else {
            session()->flash('sideError', 'There was an error creating the side name. Please try again.');     
        }

        return redirect()->action('menuController@menuSides', ['urlPrefix' => $urlPrefix]);


    } //end menuSidesAddNewSave



    /*-------------------- Menu Sides Edit ----------------------------------------*/
    public function menuSidesEdit($urlPrefix, $sideID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');

        // make sure correct domain
        if ($authLevel >= 40) {
            $side = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuSides.id', '=', $sideID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $side = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuSides.id', '=', $sideID]])            
            ->first();
        } else {
            $side = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['menuSides.domainID', '=', $domain->id ], ['menuSides.id', '=', $sideID]])            
            ->first();
            //$side = menuSides::where([['domainID', '=', $domain->id], ['id', '=', $sideID]])->first();
        }

        return view('menu.menuSidesEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'side' => $side]);   

    } //end menuSidesAddNewSave


    /*-------------------- Menu Sides Edit Save ----------------------------------------*/
    public function menuSidesEditSave(Request $request, $urlPrefix, $sideID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');

        // make sure correct domain
        if ($authLevel >= 40) {
            $side = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuSides.id', '=', $sideID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $side = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuSides.id', '=', $sideID]])            
            ->first();
        } else {
            $side = menuSides::leftJoin('domains', 'menuSides.domainID', '=', 'domains.id')
            ->select('menuSides.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.id', '=', $domain->id ], ['menuSides.id', '=', $sideID]])            
            ->first();
        }

            $side->numberSides = $request->numberSides;
            $side->active = $request->active;
            $side->name = $request->name;
            $side->description = $request->description;
        $chk = $side->save();

        if ($chk) {
            session()->flash('sideSuccess', 'The new side group was successfully updated.');
        } else {
            session()->flash('sideError', 'There was an error updating the side group. Please try again.');     
        }

        return redirect()->action('menuController@menuSides', ['urlPrefix' => $urlPrefix]);


    } //end menuSidesAddNewSave


    /*-------------------- Menu Sides Delete - Admin Only ----------------------------------*/
    public function menuSidesDelete($urlPrefix, $sideID) {
        
        $authLevel = \Request::get('authLevel');        
        if ($authLevel < 40) {
            session()->flash('sideError', 'Method not allowed - please contact the system administrator for assistance.'); 
            return redirect()->action('menuController@menuSides', ['urlPrefix' => $urlPrefix]);
        }

        $domain = \Request::get('domain');
        $chk = menuSides::destroy($sideID);

        if ($chk) {
            session()->flash('sideSuccess', 'The side was successfully deleted.');
        } else {
            session()->flash('sideError', 'There was an error deleting the side. Please try again.');     
        }

        return redirect()->action('menuController@menuSides', ['urlPrefix' => $urlPrefix]);


    } //end menuSidesDelete




    /*-------------------- Menu Sides Detail ----------------------------------------*/
    public function menuSidesDetail($urlPrefix, $sideID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $menuSides = menuSides::find($sideID);

        // make sure correct domain
        if ($authLevel >= 40) {
            $sidesData = menuSidesData::where('menuSidesID', '=', $sideID)->get();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $sidesData = menuSidesData::leftJoin('domains', 'menuSidesData.domainID', '=', 'domains.id')
            ->select('menuSidesData.*',  'domains.name')
            ->where([['menuSidesData.menuSidesID', '=', $sideID],['domains.parentDomainID', '=', $domain->id]])
            ->get();
        } else {
            $sidesData = menuSidesData::where([['domainID', '=', $domain->id],['menuSidesID', '=', $sideID]])->get();
        }

        return view('menu.menuSidesDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'sidesData' => $sidesData, 'sideID' => $sideID, 'menuSides' => $menuSides]);

    } //end menuSidesDetail



    /*-------------------- Menu Sides Detail Edit ----------------------------------------*/
    public function menuSidesDetailEdit($urlPrefix, $sideID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $menuSides = menuSides::find($sideID);
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $side = menuSidesData::where('id', '=', $sideID)->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $side = menuSidesData::leftJoin('domains', 'menuSidesData.domainID', '=', 'domains.id')
            ->select('menuSidesData.*',  'domains.name')
            ->where([['menuSidesData.id', '=', $sideID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $side = menuSidesData::leftJoin('domains', 'menuSidesData.domainID', '=', 'domains.id')
            ->select('menuSidesData.*',  'domains.name')
            ->where([['menuSidesData.id', '=', $sideID],['domains.id', '=', $domain->id]])
            ->first();        }

        return view('menu.menuSidesDetailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'side' => $side, 'menuSides' => $menuSides]);
        
    } //end menuSidesDetailEdit



    /*-------------------- Menu Sides Detail Edit Save ----------------------------------------*/
    public function menuSidesDetailEditSave(Request $request, $urlPrefix, $sideID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $side = menuSidesData::where('id', '=', $sideID)->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $side = menuSidesData::leftJoin('domains', 'menuSidesData.domainID', '=', 'domains.id')
            ->select('menuSidesData.*',  'domains.name')
            ->where([['menuSidesData.id', '=', $sideID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $side = menuSidesData::leftJoin('domains', 'menuSidesData.domainID', '=', 'domains.id')
            ->select('menuSidesData.*',  'domains.name')
            ->where([['menuSidesData.id', '=', $sideID],['domains.id', '=', $domain->id]])
            ->first();        }

   //     $side = menuSidesData::where([['domainID', '=', $domain->id], ['id', '=', $sideID]])->first();

            $side->domainID = $domain->id;
            $side->upCharge = $request->upCharge;
            $side->sidesDefault = $request->sidesDefault;
            $side->sideName = $request->sideName;
            $side->printKitchen = $request->printKitchen;
            $side->printReceipt = $request->printReceipt;
        $chk = $side->save();

        if ($chk) {
            session()->flash('sideSuccess', 'The side item was successfully updated.');
        } else {
            session()->flash('sideError', 'There was an error updating the side item. Please try again.');     
        }

        return redirect()->action('menuController@menuSidesDetail', ['urlPrefix' => $urlPrefix, 'sideID' => $side->menuSidesID]);


    } //end menuSidesDetailEditSave

    /*-------------------- Menu Sides Detail Add New ----------------------------------------*/
    public function menuSidesDetailsAddNew($urlPrefix, $sideID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $menuSides = menuSides::find($sideID);


        return view('menu.menuSidesDetailAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'sideID' => $sideID, 'menuSides' => $menuSides]);
        

    } //end menuSidesDetailsAddNew


    /*-------------------- Menu Sides Detail Add New Save ----------------------------------------*/
    public function menuSidesDetailsAddNewSave(Request $request, $urlPrefix, $sideID) {
        
     //   $domain = \Request::get('domain');
        $menuSides = menuSides::find($sideID);
        $side = new menuSidesData();
            $side->domainID = $menuSides->domainID;
            $side->upCharge = $request->upCharge;
            $side->sidesDefault = $request->sidesDefault;
            $side->sideName = $request->sideName;
            $side->printKitchen = $request->printKitchen;
            $side->printReceipt = $request->printReceipt;
            $side->menuSidesID = $sideID;
        $chk = $side->save();

        if ($chk) {
            session()->flash('sideSuccess', 'The side item was successfully created.');
        } else {
            session()->flash('sideError', 'There was an error creating the side item. Please try again.');     
        }

        return redirect()->action('menuController@menuSidesDetail', ['urlPrefix' => $urlPrefix, 'sideID' => $sideID]);


    } //end menuSidesDetailsAddNewSave


    /*-------------------- Menu Sides Detail Delete ----------------------------------*/
    public function menuSidesDetailDelete(Request $request, $urlPrefix, $sideDataID) {
        
        $domain = \Request::get('domain');
        $data = menuSidesData::find($sideDataID);
        $chk = menuSidesData::destroy($sideDataID);

        if ($chk) {
            session()->flash('sideSuccess', 'The side item was successfully deleted.');
        } else {
            session()->flash('sideError', 'There was an error deleting the side item. Please try again.');     
        }

        return redirect()->action('menuController@menuSidesDetail', ['urlPrefix' => $urlPrefix, 'sideID' => $data->menuSidesID]);


    } //end menuSidesDetailsDelete















    








    /*-------------------- Menu Options  ----------------------------------------*/
    public function menuOptions($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $options = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->get();  
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $options = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('domains.parentDomainID', '=', $domain->id)            
            ->get();
        } else {  
            $options = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuOptions.domainID', '=', $domain->id)            
            ->get();
        }

        return view('menu.menuOptions')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'options' => $options]);

    } //end menuOptions


    /*-------------------- Menu Options Add New  ----------------------------------------*/
    public function menuOptionsAddNew($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $domains = $this->getDomains($domain, $authLevel);

        return view('menu.menuOptionsAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'domains' => $domains]);

    } //end menuOptionsAddNew


    /*-------------------- Menu Options Add New Save ----------------------------------------*/
    public function menuOptionsAddNewSave(Request $request, $urlPrefix) {

        $new = new menuOptions();
            $new->domainID = $request->domainID;
            $new->maxOptions = $request->maxOptions;            
            $new->active = $request->active;
            $new->name = $request->name;
            $new->description = $request->description;
        $chk = $new->save();

        if ($chk) {
            session()->flash('optionSuccess', 'The new option name was successfully created.');
        } else {
            session()->flash('optionError', 'There was an error creating the option name. Please try again.');     
        }

        return redirect()->action('menuController@menuOptions', ['urlPrefix' => $urlPrefix]);


    } //end menuOptionsAddNewSave



    /*-------------------- Menu Options Edit ----------------------------------------*/
    public function menuOptionsEdit($urlPrefix, $optionID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $option = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuOptions.id', '=', $optionID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $option = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuOptions.id', '=', $optionID]])            
            ->first();
        } else {
            $option = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['menuOptions.domainID', '=', $domain->id ], ['menuOptions.id', '=', $optionID]])            
            ->first();
        }

        return view('menu.menuOptionsEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'option' => $option]);
        

    } //end menuOptionsAddNewSave


    /*-------------------- Menu Options Edit Save ----------------------------------------*/
    public function menuOptionsEditSave(Request $request, $urlPrefix, $optionID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $option = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuOptions.id', '=', $optionID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $option = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuOptions.id', '=', $optionID]])            
            ->first();
        } else {
            $option = menuOptions::leftJoin('domains', 'menuOptions.domainID', '=', 'domains.id')
            ->select('menuOptions.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['menuOptions.domainID', '=', $domain->id ], ['menuOptions.id', '=', $optionID]])            
            ->first();
        }

            $option->maxOptions = $request->maxOptions;
            $option->active = $request->active;
            $option->name = $request->name;
            $option->description = $request->description;
        $chk = $option->save();

        if ($chk) {
            session()->flash('optionSuccess', 'The new option group was successfully updated.');
        } else {
            session()->flash('optionError', 'There was an error updating the option group. Please try again.');     
        }

        return redirect()->action('menuController@menuOptions', ['urlPrefix' => $urlPrefix]);


    } //end menuOptionsAddNewSave


    /*-------------------- Menu Options Delete - Admin Only ----------------------------------*/
    public function menuOptionsDelete(Request $request, $urlPrefix, $optionID) {
 
        $authLevel = \Request::get('authLevel');
        if ($authLevel < 40) {
            session()->flash('optionError', 'Method not allowed - please contact the system administrator for assistance.'); 
            return redirect()->action('menuController@menuOptions', ['urlPrefix' => $urlPrefix]);
        }

        $domain = \Request::get('domain');
        $chk = menuOptions::destroy($optionID);

        if ($chk) {
            session()->flash('optionSuccess', 'The option was successfully deleted.');
        } else {
            session()->flash('optionError', 'There was an error deleting the option. Please try again.');     
        }

        return redirect()->action('menuController@menuOptions', ['urlPrefix' => $urlPrefix]);


    } //end menuOptionsDelete












    /*-------------------- Menu Options Detail ----------------------------------------*/
    public function menuOptionsDetail($urlPrefix, $optionID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $menuOptions = menuOptions::find($optionID);

        // make sure correct domain
        $authLevel = \Request::get('authLevel');        
        if ($authLevel >= 40) {
            $optionsData = menuOptionsData::where('menuOptionsID', '=', $optionID)->get();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $optionsData = menuOptionsData::leftJoin('domains', 'menuOptionsData.domainID', '=', 'domains.id')
            ->select('menuOptionsData.*',  'domains.name')
            ->where([['menuOptionsData.menuOptionsID', '=', $optionID],['domains.parentDomainID', '=', $domain->id]])
            ->get();
        } else {
            $optionsData = menuOptionsData::leftJoin('domains', 'menuOptionsData.domainID', '=', 'domains.id')
            ->select('menuOptionsData.*',  'domains.name')
            ->where([['menuOptionsData.menuOptionsID', '=', $optionID],['domains.id', '=', $domain->id]])
            ->get();
        }
        
        return view('menu.menuOptionsDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'optionsData' => $optionsData, 'optionID' => $optionID, 'menuOptions' => $menuOptions]);

    } //end menuOptionsDetail


    /*-------------------- Menu Options Detail Edit ----------------------------------------*/
    public function menuOptionsDetailEdit($urlPrefix, $optionID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];

        // make sure correct domain
        $authLevel = \Request::get('authLevel');        
        if ($authLevel >= 40) {
            $option = menuOptionsData::where('id', '=', $optionID)->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $option = menuOptionsData::leftJoin('domains', 'menuOptionsData.domainID', '=', 'domains.id')
            ->select('menuOptionsData.*',  'domains.name')
            ->where([['menuOptionsData.id', '=', $optionID],['domains.parentDomainID', '=', $domain->id]])
            ->first();
        } else {
            $option = menuOptionsData::leftJoin('domains', 'menuOptionsData.domainID', '=', 'domains.id')
            ->select('menuOptionsData.*',  'domains.name')
            ->where([['menuOptionsData.id', '=', $optionID],['domains.id', '=', $domain->id]])
            ->first();
        }

        //$option = menuOptionsData::where([['domainID', '=', $domain->id], ['id', '=', $optionID]])->first();

        return view('menu.menuOptionsDetailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'option' => $option]);
        

    } //end menuOptionsDetailEdit

    /*-------------------- Menu Options Detail Edit Save ----------------------------------------*/
    public function menuOptionsDetailEditSave(Request $request, $urlPrefix, $optionID) {
        
        $domain = \Request::get('domain');
        
         // make sure correct domain
         $authLevel = \Request::get('authLevel');        
         if ($authLevel >= 40) {
             $option = menuOptionsData::where('id', '=', $optionID)->first();
         } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
             $option = menuOptionsData::leftJoin('domains', 'menuOptionsData.domainID', '=', 'domains.id')
             ->select('menuOptionsData.*',  'domains.name')
             ->where([['menuOptionsData.id', '=', $optionID],['domains.parentDomainID', '=', $domain->id]])
             ->first();
         } else {
            $option = menuOptionsData::leftJoin('domains', 'menuOptionsData.domainID', '=', 'domains.id')
            ->select('menuOptionsData.*',  'domains.name')
            ->where([['menuOptionsData.id', '=', $optionID],['domains.id', '=', $domain->id]])
            ->first();
         }       

            $option->upCharge = $request->upCharge;
            $option->optionsDefault = $request->optionsDefault;
            $option->optionName = $request->optionName;
            $option->printKitchen = $request->printKitchen;
            $option->printReceipt = $request->printReceipt;
        $chk = $option->save();

        if ($chk) {
            session()->flash('optionSuccess', 'The option item was successfully updated.');
        } else {
            session()->flash('optionError', 'There was an error updating the option item. Please try again.');     
        }

        return redirect()->action('menuController@menuOptionsDetail', ['urlPrefix' => $urlPrefix, 'optionID' => $option->menuOptionsID]);


    } //end menuOptionsDetailEditSave

    /*-------------------- Menu Options Detail Add New ----------------------------------------*/
    public function menuOptionsDetailsAddNew($urlPrefix, $optionID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $menuOptions = menuOptions::find($optionID);

        return view('menu.menuOptionsDetailAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'optionID' => $optionID, 'menuOptions' => $menuOptions]);
        

    } //end menuOptionsDetailsAddNew


    /*-------------------- Menu Options Detail Add New Save ----------------------------------------*/
    public function menuOptionsDetailsAddNewSave(Request $request, $urlPrefix, $optionID) {
        
        $menuOptions = menuOptions::find($optionID);
        $option = new menuOptionsData();
            $option->domainID = $menuOptions->domainID;
            $option->upCharge = $request->upCharge;
            $option->optionsDefault = $request->optionsDefault;
            $option->optionName = $request->optionName;
            $option->printKitchen = $request->printKitchen;
            $option->printReceipt = $request->printReceipt;
            $option->menuOptionsID = $optionID;
        $chk = $option->save();

        if ($chk) {
            session()->flash('optionSuccess', 'The option item was successfully created.');
        } else {
            session()->flash('optionError', 'There was an error creating the option item. Please try again.');     
        }

        return redirect()->action('menuController@menuOptionsDetail', ['urlPrefix' => $urlPrefix, 'optionID' => $optionID]);


    } //end menuOptionsDetailsAddNewSave


    /*-------------------- Menu Options Detail Delete ----------------------------------*/
    public function menuOptionsDetailDelete(Request $request, $urlPrefix, $optionDataID) {
        
        $domain = \Request::get('domain');
        $data = menuOptionsData::find($optionDataID);
        $chk = menuOptionsData::destroy($optionDataID);

        if ($chk) {
            session()->flash('optionSuccess', 'The option item was successfully deleted.');
        } else {
            session()->flash('optionError', 'There was an error deleting the option item. Please try again.');     
        }

        return redirect()->action('menuController@menuOptionsDetail', ['urlPrefix' => $urlPrefix, 'optionID' => $data->menuOptionsID]);


    } //end menuOptionsDetailsDelete


































    /*-------------------- Menu AddOns  ----------------------------------------*/
    public function menuAddOns($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $addOns = menuAddOns::leftJoin('domains', 'menuAddOns.domainID', '=', 'domains.id')
            ->select('menuAddOns.*', 'domains.parentDomainID', 'domains.name as location')
            ->get();  
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $addOns = menuAddOns::leftJoin('domains', 'menuAddOns.domainID', '=', 'domains.id')
            ->select('menuAddOns.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('domains.parentDomainID', '=', $domain->id)            
            ->get();
        } else {
            $addOns = menuAddOns::where('domainID', '=', $domain->id)->get();
        }
        
        // make sure correct domain

       // $addOns = menuAddOns::where('domainID', '=', $domain->id)->get();

        return view('menu.menuAddOns')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'addOns' => $addOns]);

    } //end menuAddOns


    /*-------------------- Menu AddOns Add New  ----------------------------------------*/
    public function menuAddOnsAddNew($urlPrefix) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $domains = $this->getDomains($domain, $authLevel);

        return view('menu.menuAddOnsAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'domains' => $domains]);

    } //end menuAddOnsAddNew


    /*-------------------- Menu AddOns Add New Save ----------------------------------------*/
    public function menuAddOnsAddNewSave(Request $request, $urlPrefix) {

        $new = new menuAddOns();
            $new->domainID = $request->domainID;
            $new->active = $request->active;
            $new->maxAddOns = $request->maxAddOns;
            $new->name = $request->name;
            $new->description = $request->description;
        $chk = $new->save();

        if ($chk) {
            session()->flash('addOnSuccess', 'The new add on group was successfully created.');
        } else {
            session()->flash('addOnError', 'There was an error creating the add on group. Please try again.');     
        }

        return redirect()->action('menuController@menuAddOns', ['urlPrefix' => $urlPrefix]);


    } //end menuAddOnsAddNewSave



    /*-------------------- Menu AddOns Edit ----------------------------------------*/
    public function menuAddOnsEdit($urlPrefix, $addOnID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $addOn = menuAddOns::leftJoin('domains', 'menuAddOns.domainID', '=', 'domains.id')
            ->select('menuAddOns.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuAddOns.id', '=', $addOnID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $addOn = menuAddOns::leftJoin('domains', 'menuAddOns.domainID', '=', 'domains.id')
            ->select('menuAddOns.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuAddOns.id', '=', $addOnID]])            
            ->first();
        } else {
            $addOn = menuAddOns::where([['domainID', '=', $domain->id], ['id', '=', $addOnID]])->first();
        }

       // $addOn = menuAddOns::where([['domainID', '=', $domain->id], ['id', '=', $addOnID]])->first();

        return view('menu.menuAddOnsEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'addOn' => $addOn]);
        

    } //end menuAddOnsAddNewSave


    /*-------------------- Menu AddOns Edit Save ----------------------------------------*/
    public function menuAddOnsEditSave(Request $request, $urlPrefix, $addOnID) {
        
        $authLevel = \Request::get('authLevel');
        
        // make sure correct domain
        if ($authLevel >= 40) {
            $addOn = menuAddOns::leftJoin('domains', 'menuAddOns.domainID', '=', 'domains.id')
            ->select('menuAddOns.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuAddOns.id', '=', $addOnID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $addOn = menuAddOns::leftJoin('domains', 'menuAddOns.domainID', '=', 'domains.id')
            ->select('menuAddOns.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuAddOns.id', '=', $addOnID]])            
            ->first();
        } else {
            $addOn = menuAddOns::where([['domainID', '=', $domain->id], ['id', '=', $addOnID]])->first();
        }            
        
            $addOn->active = $request->active;
            $addOn->name = $request->name;
            $addOn->description = $request->description;
        $chk = $addOn->save();

        if ($chk) {
            session()->flash('addOnSuccess', 'The add on group was successfully updated.');
        } else {
            session()->flash('addOnError', 'There was an error updating the add on group. Please try again.');     
        }

        return redirect()->action('menuController@menuAddOns', ['urlPrefix' => $urlPrefix]);


    } //end menuAddOnsAddNewSave


    /*-------------------- Menu AddOns Delete - Admin Only ----------------------------------*/
    public function menuAddOnsDelete(Request $request, $urlPrefix, $addOnID) {
        
        $authLevel = \Request::get('authLevel');        
        if ($authLevel < 40) {
            session()->flash('addOnError', 'Method not allowed - please contact the system administrator for assistance.'); 
            return redirect()->action('menuController@menuAddOns', ['urlPrefix' => $urlPrefix]);
        }

        $domain = \Request::get('domain');
        $chk = menuAddOns::destroy($addOnID);

        if ($chk) {
            session()->flash('addOnSuccess', 'The add on was successfully deleted.');
        } else {
            session()->flash('addOnError', 'There was an error deleting the add on. Please try again.');     
        }

        return redirect()->action('menuController@menuAddOns', ['urlPrefix' => $urlPrefix]);


    } //end menuAddOnsDelete












    /*-------------------- Menu AddOns Detail ----------------------------------------*/
    public function menuAddOnsDetail($urlPrefix, $addOnID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        $menuAddOns = menuAddOns::find($addOnID);

        
        // make sure correct domain
        if ($authLevel >= 40) {
            $addOnsData = menuAddOnsData::leftJoin('domains', 'menuAddOnsData.domainID', '=', 'domains.id')
            ->select('menuAddOnsData.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuAddOnsData.menuAddOnsID', '=', $addOnID)
            ->get();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $addOnsData = menuAddOnsData::leftJoin('domains', 'menuAddOnsData.domainID', '=', 'domains.id')
            ->select('menuAddOnsData.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuAddOnsData.menuAddOnsID', '=', $addOnID]])            
            ->get();
        } else {
            $addOnsData = menuAddOnsData::where([['domainID', '=', $domain->id], ['menuAddOnsID', '=', $addOnID]])->get();
        }   
        
        // make sure correct domain
        //$addOnsData = menuAddOnsData::where([['domainID', '=', $domain->id],['menuAddOnsID', '=', $addOnID]])->get();

        return view('menu.menuAddOnsDetail')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'addOnsData' => $addOnsData, 'addOnID' => $addOnID, 'menuAddOns' => $menuAddOns]);

    } //end menuAddOnsDetail


    /*-------------------- Menu AddOns Detail Edit ----------------------------------------*/
    public function menuAddOnsDetailEdit($urlPrefix, $addOnID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $authLevel = \Request::get('authLevel');
        
        if ($authLevel >= 40) {
            $addOn = menuAddOnsData::leftJoin('domains', 'menuAddOnsData.domainID', '=', 'domains.id')
            ->select('menuAddOnsData.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuAddOnsData.id', '=', $addOnID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $addOn = menuAddOnsData::leftJoin('domains', 'menuAddOnsData.domainID', '=', 'domains.id')
            ->select('menuAddOnsData.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuAddOnsData.id', '=', $addOnID]])            
            ->first();
        } else {
            $addOn = menuAddOnsData::where([['domainID', '=', $domain->id], ['id', '=', $addOnID]])->first();
        }   

     //   $addOn = menuAddOnsData::where([['domainID', '=', $domain->id], ['id', '=', $addOnID]])->first();

        return view('menu.menuAddOnsDetailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'addOn' => $addOn]);
        

    } //end menuAddOnsDetailEdit



    /*-------------------- Menu AddOns Detail Edit Save ----------------------------------------*/
    public function menuAddOnsDetailEditSave(Request $request, $urlPrefix, $addOnID) {
        
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        if ($authLevel >= 40) {
            $addOn = menuAddOnsData::leftJoin('domains', 'menuAddOnsData.domainID', '=', 'domains.id')
            ->select('menuAddOnsData.*', 'domains.parentDomainID', 'domains.name as location')
            ->where('menuAddOnsData.id', '=', $addOnID)
            ->first();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $addOn = menuAddOnsData::leftJoin('domains', 'menuAddOnsData.domainID', '=', 'domains.id')
            ->select('menuAddOnsData.*', 'domains.parentDomainID', 'domains.name as location')
            ->where([['domains.parentDomainID', '=', $domain->id ], ['menuAddOnsData.id', '=', $addOnID]])            
            ->first();
        } else {
            $addOn = menuAddOnsData::where([['domainID', '=', $domain->id], ['id', '=', $addOnID]])->first();
        }   

            $addOn->upCharge = $request->upCharge;
            $addOn->addOnsDefault = $request->addOnsDefault;
            $addOn->addOnsName = $request->addOnsName;
            $addOn->printKitchen = $request->printKitchen;
            $addOn->printReceipt = $request->printReceipt;
        $chk = $addOn->save();

        if ($chk) {
            session()->flash('addOnSuccess', 'The add on item was successfully updated.');
        } else {
            session()->flash('addOnError', 'There was an error updating the add on item. Please try again.');     
        }

        return redirect()->action('menuController@menuAddOnsDetail', ['urlPrefix' => $urlPrefix, 'addOnID' => $addOn->menuAddOnsID]);

    } //end menuAddOnsDetailEditSave



    /*-------------------- Menu AddOns Detail Add New ----------------------------------------*/
    public function menuAddOnsDetailsAddNew($urlPrefix, $addOnID) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['menu', ''];
        $menuAddOns = menuAddOns::find($addOnID);

        return view('menu.menuAddOnsDetailAddNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'menuAddOns' => $menuAddOns]);
        

    } //end menuAddOnsDetailsAddNew


    /*-------------------- Menu AddOns Detail Add New Save ----------------------------------------*/
    public function menuAddOnsDetailsAddNewSave(Request $request, $urlPrefix, $addOnID) {
        
        $menuAddOns = menuAddOns::find($addOnID);
        $addOn = new menuAddOnsData();
            $addOn->domainID = $menuAddOns->domainID;
            $addOn->upCharge = $request->upCharge;
            $addOn->addOnsDefault = $request->addOnsDefault;
            $addOn->addOnsName = $request->addOnsName;
            $addOn->printKitchen = $request->printKitchen;
            $addOn->printReceipt = $request->printReceipt;
            $addOn->menuAddOnsID = $addOnID;
        $chk = $addOn->save();

        if ($chk) {
            session()->flash('addOnSuccess', 'The add on item was successfully created.');
        } else {
            session()->flash('addOnError', 'There was an error creating the add on item. Please try again.');     
        }

        return redirect()->action('menuController@menuAddOnsDetail', ['urlPrefix' => $urlPrefix, 'addOnID' => $addOnID]);


    } //end menuAddOnsDetailsAddNewSave


    /*-------------------- Menu AddOns Detail Delete ----------------------------------*/
    public function menuAddOnsDetailDelete(Request $request, $urlPrefix, $addOnDataID) {
        
        $domain = \Request::get('domain');
        $data = menuAddOnsData::find($addOnDataID);
        $chk = menuAddOnsData::destroy($addOnDataID);

        if ($chk) {
            session()->flash('addOnSuccess', 'The add on item was successfully deleted.');
        } else {
            session()->flash('addOnError', 'There was an error deleting the add on item. Please try again.');     
        }

        return redirect()->action('menuController@menuAddOnsDetail', ['urlPrefix' => $urlPrefix, 'addOnID' => $data->menuAddOnsID]);


    } //end menuAddOnsDetailsDelete























    /*-------------------- Get Menu Categories  ----------------------------------------*/
    protected function getCategories($menuCategoriesID) {
        $menuCategoriesData = menuCategoriesData::where('menuCategoriesID', '=', $menuCategoriesID)->get();
        
        $categories = [];
        foreach ($menuCategoriesData as $mc) {
            $categories[$mc->id] = $mc->categoryName;
        } // end foreach

        return $categories;
    } // getCategories







    /*-------------------- Get Domains  ----------------------------------------*/
    protected function getDomains($domain, $authLevel) {
        
        if ($authLevel >= 40) {
            $domains = domain::select('id', 'name')->orderBy('name')->get();
        } elseif ($authLevel >= 35 && $authLevel < 40 && $domain->type < 8) {
            $domains = domain::select('id', 'name')->where('parentDomainID', '=', $domain->parentDomainID)->orderBy('name')->get();
        } else {
            $domains = domain::select('id', 'name')->where('id', '=', $domain->id)->orderBy('name')->get();
        }

        return $domains;
    } // end getDomains


} // end menuController
