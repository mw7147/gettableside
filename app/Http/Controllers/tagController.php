<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tags;

class tagController extends Controller {
    

    /*----------------  viewTags  ----------------------------------*/

    public function viewTags($urlPrefix) {
        
        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');

        $tags = tags::where('domainID', '=', $domain->id)->get();

        return view('tags.view')->with([ 'domain' => $domain, 'tags' => $tags, 'breadcrumb' => $breadcrumb ]);

    } // end viewTags



    /*----------------  newTag  ----------------------------------*/

    public function newTag() {
        
        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');

        return view('tags.new')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb ]);

    } // end newTag



    /*----------------  newTagDo  ----------------------------------*/

    public function newTagDo(Request $request, $urlPrefix) {
        
        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');

        $tag = new tags();
            $tag->domainID = $domain->id;
            $tag->parentDomainID = $domain->parentDomainID;
            $tag->tagName = $request->tagName;
            $tag->tagTier = $request->tagTier;
        $t = $tag->save();

        if ($t) {
            $request->session()->flash('tagSuccess', 'The tag was successfully created.');
            return redirect()->route( 'tags', [ 'urlPrefix' => $urlPrefix ] );
        } else {
            $request->session()->flash('tagError', 'There was an error creating the tag. Please check your data and try again.');
            return redirect()->back()->withInput();
        }


    } // end newTagDo


    /*----------------  deleteTag  ----------------------------------*/

    public function deleteTag( Request $request, $urlPrefix, $id) {

        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');
        $tag = tags::findOrFail($id);

        if ($tag->domainID == $domain->id) {
            // only delete your tags
            $tag->delete();
            $request->session()->flash('tagSuccess', 'The tag was successfully deleted.');
        } else {
            // not your tag
            $request->session()->flash('tagError', 'There was an error deleting the tag. Please check your data and try again.');
        }

        return redirect()->route( 'tags', [ 'urlPrefix' => $urlPrefix ] );


    } // end deleteTag



    /*----------------  editTag  ----------------------------------*/

    public function editTag( Request $request, $urlPrefix, $id) {
    
        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');
        $tag = tags::findOrFail($id);

        if ($tag->domainID == $domain->id) {
            // only edit your tags
            return view('tags.edit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb, 'tag' => $tag ]);
        } else {
            // not your tag
            abort(403);
        }

    } // end editTag



    /*----------------  editTagDo  ----------------------------------*/

    public function editTagDo( Request $request, $urlPrefix, $id) {

        $breadcrumb = ['reports', ''];    
        $domain = \Request::get('domain');
        $tag = tags::findOrFail($id);

        if ($tag->domainID == $domain->id) {
            // only edit your tags
            $tag->tagName = $request->tagName;
            $tag->tagTier = $request->tagTier;
            $t = $tag->save();
            if ($t) {
                $request->session()->flash('tagSuccess', 'The tag was successfully updated.');
                return redirect()->route( 'tags', [ 'urlPrefix' => $urlPrefix ] );
            } else {
                $request->session()->flash('tagError', 'There was an error updating the tag. Please check your data and try again.');
                return redirect()->back()->withInput();
            }  

        } else {
            // not your tag
            abort(403);
        }

    } // end editTagDo




} // end tagController
