<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\StationRepository;
use App\menuCategoriesData;

class stationController extends Controller
{
    protected $station;

    public function __construct(){
      $this->station = new StationRepository;
    }
    /*--------------------------------- Station Type List-----------------------------------------------*/

    public function stationType() {
        $breadcrumb = ['station', 'type'];
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        $stationTypeList = $this->station->stationTypeList();
        return view('stations.stationType')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'authLevel' => $authLevel, 'stationTypeList' => $stationTypeList]);
    } // end Type

    /*--------------------------------- Station Type Create-----------------------------------------------*/

    public function storeStationType(Request $request) {
      $stationType = $this->station->stationTypeCreate($request);
      return response($stationType);
    } // end storeStationType

    /*--------------------------------- Station List -----------------------------------------------*/

    public function stationList() {
        $breadcrumb = ['station', 'stations'];
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        $stationTypeList = $this->station->stationTypeList();
        $stationList = $this->station->stationList();
        return view('stations.stationsList')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'authLevel' => $authLevel, 'stationTypeList' => $stationTypeList, 'stationList' => $stationList]);;
    } // end stationList

    /*--------------------------------- Station Create-----------------------------------------------*/

    public function storeStation(Request $request) {
      $station = $this->station->stationCreate($request);
      return response($station);
    } // end storeStation

    /*--------------------------------- Station delete-----------------------------------------------*/

    public function deleteStation(Request $request, $id) {
      $station = $this->station->stationDelete($request, $id);
      return response($station);
    }
    
    /*--------------------------------- Station update-----------------------------------------------*/

    public function updateStation(Request $request, $id) {
      $station = $this->station->stationUpdate($request, $id);
      return response($station);
    }

    /*--------------------------------- Station Location -----------------------------------------------*/

    public function stationLocation() {
        $breadcrumb = ['station', 'locations'];
        $domain = \Request::get('domain');
        $authLevel = \Request::get('authLevel');
        return view('stations.stationLocation')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'authLevel' => $authLevel]);;
    } // end Location

    
    public function stationMenu() {
      $breadcrumb = ['station', 'menu'];
      $domain = \Request::get('domain');
      $authLevel = \Request::get('authLevel');

      //category Listing
      $menuCategoriesData = menuCategoriesData::where('domainId',18)->get();
      //Station Listing 
      //$stationList = $this->station->stationList();
      $stationList = \App\Station::where('domainId', $domain->parentDomainID)->get();
     
      $stationMenuArr = [];
      foreach ($stationList as $key => $station) {
        foreach ($station->menuDataList as $menu) {
          $stationMenuArr[] = $menu->id;
        }
      }
      return view('stations.stationMenu')->with(['stationMenuArr' => $stationMenuArr, 'domain' => $domain, 'breadcrumb' => $breadcrumb, 'authLevel' => $authLevel, 'menuCategoriesData' => $menuCategoriesData, 'stationList' => $stationList]);
    } // end Menu

    /* add menu to station */
    public function addMenuItem() {
      $categoryID = \Request::get('categoryID');
      $menuID = \Request::get('menuID');
      $stationID = \Request::get('stationID');
      $user = auth()->user();

      if($categoryID == null){
        $stationMenuItem =  \App\StationMenuItem::firstOrCreate(
          [
            'menu_item_id' => $menuID,
            'station_id' => $stationID,
            'domainId' => $user->domainID
          ],
          [
            'menu_item_id' => $menuID,
            'station_id' => $stationID,
            'domainId' => $user->domainID,
            'status' => 1,
            'created_by' => $user->id,
          ]
        );
      }else{
        $menuIDs =  \App\menuData::where('menuCategoriesDataID', $categoryID)->pluck('id');
        if($menuIDs){
          if(count($menuIDs->toArray())){
            foreach ($menuIDs as $menuID) {
              $stationMenuItem =  \App\StationMenuItem::firstOrCreate(
                [
                  'menu_item_id' => $menuID,
                  'station_id' => $stationID,
                  'domainId' => $user->domainID
                ],
                [
                  'menu_item_id' => $menuID,
                  'station_id' => $stationID,
                  'domainId' => $user->domainID,
                  'status' => 1,
                  'created_by' => $user->id,
                ]
              );
            }
          }
        }

      }

      return true;
    }

    /* remove menu from station */
    public function removeMenuItem() {
      $menuID = \Request::get('menuID');
      $stationID = \Request::get('stationID');
      $user = auth()->user();
      $stationMenuItem =  \App\StationMenuItem::where([
        ['station_id', '=', $stationID],
        ['menu_item_id', '=', $menuID]
      ])->delete();
      
      return true;
    }
}
