<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\CellNumberData;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use App\siteStyles;
use App\marketing;
use App\usersData;
use Carbon\Carbon;
use App\cartData;
use App\siteData;
use App\usStates;
use App\contacts;
use App\orders;
use App\ccBIN;
use App\User;

class customerController extends Controller
{
  	/*
  	|----------------------------------------------------------------------------------------------
  	| Customer Dashboard
  	|----------------------------------------------------------------------------------------------
  	|
  	*/
  	
  	public function dashboard(Request $request) {

      $domain = \Request::get('domain');
      $user = Auth::user();
      $sid = session()->getId();
      $data = siteData::where('domainID', '=', $domain->id)->first();
      $pageName = "dashboard";
      $states = usStates::all();
      $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();
      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();

      return view('customer.dashboard')->with([ 'domain' => $domain, 'styles' => $styles, 'html' => '', 'sid' => $sid, 'total' => '', 'states' => $states, 'defaultState' => '', 'tax' =>  '', 'subTotal' => '', 'domainSiteConfig' => $domainSiteConfig, 'pageName' => $pageName, 'data' => $data, 'user' => $user ]);

  	} // end dashboard



    /*----------------- update my information --------------------------------------*/

    
    public function updateMyInformation(Request $request) {

        $domain = \Request::get('domain');
        $userID = Auth::id();
        $numOnly = CellNumberData::numbersOnly($request->mobile);

        // get data for marketing system
        $marketing = marketing::findOrFail($domain->parentDomainID);

        // set dates
        if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
        if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }

        // update user
        $user = User::firstOrNew(['id' => $userID]);
        $user->email = $request->email;
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->mobile = $request->mobile;
        $user->unformattedMobile = $numOnly;
        $user->zipCode = $request->zipCode;
        $user->save();
        
        // update usersData        
        $usersData = usersData::firstOrNew(['usersID' => $userID]);
        $usersData->address1 = $request->address1 ?? '';
        $usersData->address2 = $request->address2 ?? '';
        $usersData->city = $request->city ?? '';
        $usersData->state = $request->state ?? '';
        $usersData->save();

        // update contact
        $contact = contacts::firstOrNew(['tgmUserID' => $userID]);
        $contact->tgmDomainID = $domain->parentDomainID;
        $contact->tgmLocationID = $domain->id;
        $contact->domainID = $marketing->mktDomainID;
        $contact->customerID = $marketing->mktCustomerID;
        $contact->userID = $marketing->mktUserID;
        $contact->repID = $marketing->mktRepID;
        $contact->email = $request->email;
        $contact->fname = $request->fname;
        $contact->lname = $request->lname;
        $contact->mobile = $request->mobile;
        $contact->unformattedMobile = $numOnly;
        $contact->address1 = $request->address1;
        $contact->address2 = $request->address2;
        $contact->city = $request->city;
        $contact->state = $request->state;
        $contact->zip = $request->zipCode;
        $contact->birthDay = $bd;
        $contact->anniversary = $an;
        $contact->save();


        session()->flash('updateSuccess', 'You have successfully updated your information.');

        return redirect('customer/dashboard');

    } // end update my information


    /*----------------- viewOrders --------------------------------------*/

    
    public function viewOrders(Request $request) {
      
      $domain = \Request::get('domain');
      $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
      $user = Auth::user();
      $contact = contacts::where('tgmUserID', '=', $user->id )->first();

      if (is_null($contact)) {
        $orders = null;
      } else {
        $orders = orders::where('contactID', '=', $contact->id)->get();
      }

      return view('customer.orders')->with([ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles, 'orders' => $orders ]);

    } // end viewOrders

    /*----------------- viewOrderHTML --------------------------------------*/

    
    public function viewOrderHTML($orderID) {
      
      $orders = orders::where('id', '=', $orderID)->first();
      if (empty($orders)) { abort(404); }

      $domain = \Request::get('domain');
      if ($orders->domainID != $domain->id ) { abort(404); }

      return view('customer.viewOrder')->with([ 'html' => $orders->emailData ]);

    } // end viewOrderHTML

    /*----------------- passwordReset --------------------------------------*/

    
    public function passwordReset(Request $request) {

      $domain = \Request::get('domain');
      $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
      $userID = Auth::id();      

      return view('customer.passwordReset')->with([ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles ]);

    } // end passwordReset


    /*----------------- passwordResetDo --------------------------------------*/

    
    public function passwordResetDo(Request $request) {

      $domain = \Request::get('domain');
      $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
      $userID = Auth::id();
      $oldSessionID = session()->getId();

      $user = User::where('id', '=', $userID)->update(['password' => Hash::make($request->password1)]);
      Auth::loginUsingId($userID);      
      $newSessionID = session()->getId();
      cartData::where('sessionID', '=', $oldSessionID)->update(['sessionID' => $newSessionID, 'userID' => Auth::id()]);
      session()->flash('updateSuccess', 'You have been successfully updated your password.');

      return view('customer.passwordReset')->with([ 'domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles ]);

    } // end passwordReset


    /*----------------- view payment information --------------------------------------*/

    
    public function viewPayments(Request $request) {

      $userID = Auth::id();
      $domain = \Request::get('domain');
      $styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
      $card = ccBIN::where('userID', '=', $userID)->first();

      $usercards = null;
			if($userID){
				$usercards = \App\UserCard::where('user_id', $userID)->get();
      }
      return view('customer.viewPayments')->with([ 'domain' => $domain, 'usercards' => $usercards, 'domainSiteConfig' => $domainSiteConfig, 'styles' => $styles, 'card' => $card ]);

    } // end viewPayments
    

} // end customerController
