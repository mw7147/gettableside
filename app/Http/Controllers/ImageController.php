<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\fileDownloads;
use App\Http\Requests;
use App\fileTypes;
use App\usersData;
use Carbon\Carbon;
use App\User;

class ImageController extends Controller {

/*---------------------- Attache View ---------------------------------------------------------------------*/   

  	public function customerView(Request $request, $dir) {

 // 		if (isset($_GET['location'])) { $ckeditor = $_GET['location']; } else { $ckeditor = ''; }
//		user->id => individual attache
//		user->domainID => domain level attache  		
  		$uid = Auth::user()->domainID;

		// test for no data and not home directory
		if ($dir == "home"){
	//		$path = env('PUBLIC_ROOT') . $dir;
			$rootDir = env('PUBLIC_ROOT') . env('CUSTOMER_ATTACHE_PATH') . '/' . $uid;

		} else {
//			$path = env('PUBLIC_ROOT') . $uid . "/" . $dir;
			$rootDir = env('PUBLIC_ROOT') . env('CUSTOMER_ATTACHE_PATH') . '/' . $uid . "/" . $dir;
		}

		// abort prying eyes
		if ( (!\File::isDirectory($rootDir)) && ($dir != "home")) {abort(404);}
		// first time user check
		if (!\File::isDirectory($rootDir)) {
			// create user home directory
			Storage::makeDirectory(env('CUSTOMER_ATTACHE_PATH') . "/" . $uid);
		}

		$directories = $this->getDirectories($uid);
		$files = $this->getFiles($uid, $dir);
		$domain = \Request::get('domain');
		$uid = Auth::user()->domainID;
		$breadcrumb = ['attache', 'view'];

		// get used and total storage
		$baseDir = env('PUBLIC_ROOT') . env('CUSTOMER_ATTACHE_PATH') . '/' . $uid;
		$totalStorageUsed = $this->get_total_storage($baseDir);
		$hts = $this->human_filesize($totalStorageUsed, 1);
		$storageUsed = ["storageUsed" => $hts, "maxStorage" => env('TOTAL_STORAGE_BASIC_HUMAN')];

/*
	    if ($ckeditor == 'ckeditor') {
    		return view('imageViewer.imageViewerBrowse')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'directories' => $directories, 'files' => $files, 'baseFolder' => basename($baseFolder), 'uid' => $uid]);
		}
*/

    	return view('images.customerImageView')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'directories' => $directories, 'files' => $files, 'uid' => $uid, 'dir' => $request->path(), 'homedir' => $dir, 'storageUsed' => $storageUsed]);

    }     



/*--------------------------------new File Upload -----------------------------------------------------------*/   


  	public function newFileUpload(Request $request) {
  		
  		$homeDir =basename($request->dir);

		$domain = \Request::get('domain');
		$uid = Auth::user()->domainID;

    	if (!is_null($request->file('newFile'))) {
      
	        // add new file
			$pictureName = $request->file('newFile')->getClientOriginalName();
	        $pictureSize = round($request->file('newFile')->getSize()/1024,1); // file size in Kilobytes
	        $ext = $request->file('newFile')->guessClientExtension();
	        $extensionCheck = filetypes::where('extension', '=', $ext)->first();
			if ($extensionCheck->extension == "jpeg") { $pictureName = str_replace('jpg', 'jpeg',$pictureName);}
	        if ($extensionCheck->download == 0 || $pictureSize > env('MAX_FILE_SIZE')) {
	            $request->session()->flash('fileError', 'The file ' . $pictureName . '. was not a JPG, PNG or PDF file or the file size exceeded 2 Mb. Please check your picture file and try again.');
	            $chk = FALSE;
	            $path = '';

	        } else {

		          //save new file
		          // path textMsgImages/user->id/filename
				// set base folder path
				if ($homeDir == "home") {
					$folderPath = env('CUSTOMER_ATTACHE_PATH') . "/" . $uid . "/";
				} else {
					$folderPath = env('CUSTOMER_ATTACHE_PATH') . "/" . $uid . "/" . $homeDir . "/";
				}
				// convert to URL friendly filename
				$pictureName = basename($pictureName, $ext);
				$imageName = camel_case(str_slug($pictureName));
				$imageName = $imageName . "." . $ext;

				// store image on server
		        $path = $request->file('newFile')->storeAs($folderPath, $imageName);

	        }
	    }
		
		if (isset($request->ckeditor)) {
			return \Redirect::action('ImageController@ckEditorBrowser', array('dir' => $dir));
		}

	 	// return \Redirect::action('ImageController@customerView', array('dir' => $dir));
	 	return redirect($request->dir);

    }     // end newFileUpload





/*---------------------------------New Folder----------------------------------------------------------*/   

  	public function customerNewImageFolder(Request $request) {
  		// dd($request);
		$domain = \Request::get('domain');
		$uid = Auth::user()->domainID;
		
		// convert name to URL friendly name - camel_case
		$folderName = camel_case(str_slug($request->newFolderName));
		$folderPath = env('CUSTOMER_ATTACHE_PATH') . "/" . $uid . "/" . $folderName;
		$newFolder = Storage::makeDirectory($folderPath);
/*
		if (isset($request->ckeditor)) {
			return \Redirect::action('ImageController@ckEditorBrowser', array('dir' => $request->baseFolder));
		}
*/
	 	return redirect()->action('ImageController@customerView', ['dir' => $folderName]);
    }     


/*--------------------------------------Delete Folder -----------------------------------------------------*/   


  	public function folderDelete(Request $request, $dir) {

		$uid = Auth::user()->domainID;
		if ($dir == 'home' ) {abort(405);}

		// set base folder path
		$folderPath = env('CUSTOMER_ATTACHE_PATH') . "/" . $uid . "/" . $dir;
		Storage::deleteDirectory($folderPath);

		return redirect()->action('ImageController@customerView', ['dir' => 'home']);


    }  

/*-------------------------------------- Select Images Only -----------------------------------------------------*/   


  	public function selectImagesOnly(Request $request, $dir) {

		

		return $dir;


    }  








/*-------------------------------------------------------------------------------------------*/   



  	public function fileDelete(Request $request, $homeDir, $fileName) {

		$uid = Auth::user()->domainID;

		// set image path
		if ($homeDir == "home") {
			// home directory
			$folderPath = env('CUSTOMER_ATTACHE_PATH') . "/" . $uid . "/" . $fileName;
		} else {
			// directory in home directory
			$folderPath = env('CUSTOMER_ATTACHE_PATH') . "/" . $uid . "/" . $homeDir . "/" . $fileName;
		}

		// delete the file
		Storage::delete($folderPath);
		
		return redirect()->action('ImageController@customerView', ['dir' => $homeDir]);

    }  


/*-------------------------------- CKEditor Browse -----------------------------------------------------------*/   
 




  	public function ckEditorBrowser(Request $request) {

		$domain = \Request::get('domain');
		$uid = Auth::user()->domainID;
		
		$files = $this->getAllFiles($uid);
		$breadcrumb = ['images', 'view'];

  
    	return view('images.ckeditorImage')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'files' => $files, 'uid' => $uid]);

    }  



/*----------------------------- Get Directories --------------------------------------------------------------*/   

  	public function getDirectories($uid) {

		// get all directories
		$dir = env('CUSTOMER_ATTACHE_PATH') . '/' . $uid;
  		$all = Storage::directories($dir);
  		if (count($all) == 0) {
  			return $all; // return empty array
  		}
 		// create directories array and return
 		// array => filename, basename
  		for ($row = 0; $row < count($all); $row++) {
  			$directories[$row] = array($all[$row], basename($all[$row]));
  		}
  		return $directories;

    }  // end getDirectories



/*----------------------------- Get Files In Directory --------------------------------------------------------------*/   

  	public function getFiles($uid, $dir) {

  		// get current directory
		if ($dir == 'home') {
			$dir = env('CUSTOMER_ATTACHE_PATH') . '/' . $uid;
		} else {
			$dir = env('CUSTOMER_ATTACHE_PATH') . '/' . $uid . '/' . $dir;
		}

		// get files in directory
  		$all = Storage::files($dir);

  		if (count($all) == 0) {
  			return $all; // return empty array
  		}
 		// create files array and return
 		// array => filename, basename
 		$counter = 1;
  		for ($row = 0; $row < count($all); $row++) {
  			if (basename($all[$row]) == ".DS_Store" ) {continue;}
  			$check = env('PUBLIC_ROOT') . $all[$row];
  			$ext = \File::mimetype($check);
  			$ts =  \File::lastModified($check);
  			$fs =  \File::size($check);
  			$hfs = $this->human_filesize($fs, 2);
  			$dt = Carbon::createFromTimestamp($ts)->toDateTimeString();
  			$fd = fileTypes::find($ext);

 			/*
			$files - array format:
			name
			fullPath
			extension
			download
			filetype
			fileclass
 			*/

			if (basename($dir) == $uid) { $homeDir = "home"; } else { $homeDir = basename($dir); }
  			
  			$files[$counter] = array(
  				'name' => basename($all[$row]),
  				'homeDir' => $homeDir,
  				'fullPath' => $all[$row],
  				'storagePath' => str_replace( "public", "/storage", $all[$row] ),
  				'extension' => $fd->extension,
  				'download' => $fd->download,
  				'filetype' => $fd->filetype,
  				'fileclass' => $fd->fileclass,
  				'dateTime' => $dt,
  				'fileSize' => $hfs,
  				'hashString' => md5(microtime() . basename($all[$row]))
  			);
  			$counter = $counter + 1;

		  }
  		return $files;

    }  // end getFiles




/*----------------------------- Get All Image Files In Attache Directory --------------------------------------------------------------*/   

  	public function getAllFiles($uid) {
		
		$dir = 'home';
		$allFiles = [];
  		// get all files in Attache
		$dir = env('CUSTOMER_ATTACHE_PATH') . '/' . $uid;

		// get files in directory
  		$all = Storage::AllFiles($dir);

  		if (count($all) == 0) {
  			return $all; // return empty array
  		}
 		// create files array and return
 		// array => filename, basename
 		$counter = 1;
  		for ($row = 0; $row < count($all); $row++) {
  			if (basename($all[$row]) == ".DS_Store" ) {continue;}
  			$check = env('PUBLIC_ROOT') . $all[$row];
  			$ext = \File::mimetype($check);
  			$ts =  \File::lastModified($check);
  			$fs =  \File::size($check);
  			$hfs = $this->human_filesize($fs, 2);
  			$dt = Carbon::createFromTimestamp($ts)->toDateTimeString();
  			$fd = fileTypes::find($ext);

 			/*
			$files - array format:
			name
			fullPath
			extension
			download
			filetype
			fileclass
 			*/

  			if ($fd->fileType == 'image') {
	  			$allFiles[$counter] = array(
	  				'name' => basename($all[$row]),
	  				'fullPath' => $all[$row],
	  				'storagePath' => str_replace( "public", "/storage", $all[$row] ),
	  				'extension' => $fd->extension,
	  				'filetype' => $fd->filetype,
	  				'fileclass' => $fd->fileclass,
	  				'dateTime' => $dt,
	  				'fileSize' => $hfs,
	  				'hashString' => md5(microtime() . basename($all[$row]))
	  			);
	  		$counter = $counter + 1;
  			}


  		}

  		return $allFiles;

    }  // end getAllFiles



/*----------------------------- Get All Files In Attache Directory --------------------------------------------------------------*/   

  	public function getAttacheFiles($uid) {
		
		$dir = 'home';
  		// get all files in Attache
		$dir = env('CUSTOMER_ATTACHE_PATH') . '/' . $uid;
		

		// get files in directory
  		$all = Storage::AllFiles($dir);

  		if (count($all) == 0) {
  			return $all; // return empty array
  		}
 		// create files array and return
 		// array => filename, basename
 		$counter = 1;
  		for ($row = 0; $row < count($all); $row++) {
  			if (basename($all[$row]) == ".DS_Store" ) {continue;}
  			$check = env('PUBLIC_ROOT') . $all[$row];
  			$ext = \File::mimetype($check);
  			$ts =  \File::lastModified($check);
  			$fs =  \File::size($check);
  			$hfs = $this->human_filesize($fs, 2);
  			$dt = Carbon::createFromTimestamp($ts)->toDateTimeString();
  			$fd = fileTypes::find($ext);

 			/*
			$files - array format:
			name
			fullPath
			extension
			download
			filetype
			fileclass
 			*/


  			$allFiles[$counter] = array(
  				'name' => basename($all[$row]),
  				'fullPath' => $all[$row],
  				'storagePath' => str_replace( "public", "/storage", $all[$row] ),
  				'extension' => $fd->extension,
  				'filetype' => $fd->filetype,
  				'fileclass' => $fd->fileclass,
  				'dateTime' => $dt,
  				'fileSize' => $hfs,
  				'hashString' => md5(microtime() . basename($all[$row]))
  			);
	  		$counter = $counter + 1;

  		}

  		return $allFiles;

    }  // end getAttacheFiles


    /*------------ Create Download Link ----------------------------*/

        function createLink(Request $request) {

        		$fd = new fileDownloads();
        		$fd->fileHash = $request->hashString;
        		$fd->filePath = $request->filePath;
        		$fd->fileName = basename($request->filePath);
        		$fd->usersID = $request->uid;

        		try {
			      $fd->save();
			      $data = json_encode(array('status' => "success", 'message' => 'The download link has been saved to your clipboard.'));
			      return $data;
			    } // end try

			    catch (Exception $e) {
			      $data = json_encode(array('status' => "error", 'message' => 'There was an error copying to your clipboard. Please try again.'));
			      return $data;
			    } // end catch
		
        } // end createLink

    /*------------ human filesize ----------------------------*/

		function human_filesize($bytes, $decimals) {
		    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
		    $factor = floor((strlen($bytes) - 1) / 3);
		    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];

		}


		/*   * *
		* Get the directory size
		* @param directory $directory
		* @return integer
		*/
    /*------------ get total storage ----------------------------*/

		function get_total_storage($directory) {
		    $size = 0;
		    foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file) {
		        $size += $file->getSize();
		    }
		    return $size;
		}



}  // end class ImageController