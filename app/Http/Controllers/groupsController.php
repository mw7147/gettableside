<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\MultipleSites;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\groupMember;
use App\groupName;
use Carbon\Carbon;
use App\contacts;
use App\User;

class groupsController extends Controller
{

  /*---------------------------------Groups View----------------------------------------------------------*/   

  public function view($urlPrefix) {
  
    $breadcrumb = ['groups', ''];
    $domain = \Request::get('domain');
    $authLevel = \Request::get('authLevel');
    $locations = MultipleSites::getDomains($domain->id, $authLevel);
    
    
    // make sure correct domain
    if ($authLevel >= 40) {
        $groups = groupName::leftJoin('domains', 'groupNames.domainID', '=', 'domains.id')
        ->leftJoin('groupMembers', 'groupNames.id', '=', 'groupMembers.groupID')
        ->select('groupNames.*', 'domains.parentDomainID', 'domains.name as location', DB::raw('count(groupMembers.id) as groupCount'))
        ->groupBy('groupNames.id', 'groupNames.groupName')
        ->get();  
    } elseif ($authLevel >= 35 && $authLevel < 40) {
        $groups = groupName::leftJoin('domains', 'groupNames.domainID', '=', 'domains.id')
        ->leftJoin('groupMembers', 'groupNames.id', '=', 'groupMembers.groupID')        
        ->select('groupNames.*', 'domains.parentDomainID', 'domains.name as location', DB::raw('count(groupMembers.id) as groupCount'))
        ->where('domains.parentDomainID', '=', $domain->id)
        ->groupBy('groupNames.id', 'groupNames.groupName')        
        ->get();
    } else {
        $groups = groupName::leftJoin('domains', 'groupNames.domainID', '=', 'domains.id')
        ->leftJoin('groupMembers', 'groupNames.id', '=', 'groupMembers.groupID')    
        ->select('groupNames.*', 'domains.name as location', DB::raw('count(groupMembers.id) as groupCount'))        
        ->where('groupNames.domainID', '=', $domain->id)
        ->groupBy('groupNames.id', 'groupNames.groupName')        
        ->get();
    }
    if (is_null($groups)) { abort(404);}    
 
    return view('groups.view')->with(['domain' => $domain, 'groups' => $groups, 'breadcrumb' => $breadcrumb, 'locations' => $locations]);
    
  }  // end ownerView
    



/*-------------------------------- New Group -----------------------------------------------------------*/   

	public function newGroup(Request $request, $urlPrefix) {

  		// set group data
  		$group = new groupName();
  		$group->domainID = $request->domainID;
  		$group->groupName = $request->newGroupName;
  
  		try {
        $group->save();    
        $request->session()->flash('groupSuccess', 'The group ' . $request->newGroupName . ' was successfully created.');        
  		}

	  	catch (Exception $e) {
	    	$request->session()->flash('groupError', 'There was an error creating ' . $request->newGroupName . '. Please try again.');
	  	}

	  	return redirect()->action('groupsController@view', ['urlPrefix' => $urlPrefix]);

  	}   // end New Group




	/*---------------------------------Delete Group----------------------------------------------------------*/   

  	public function deleteGroup(Request $request, $urlPrefix, $groupID) {
  
      $authLevel = \Request::get('authLevel');
      $domain = \Request::get('domain');  

      // make sure correct domain
      if ($authLevel >= 40) {
        $group = groupName::where('id', '=', $groupID)->first();  
      } elseif ($authLevel >= 35 && $authLevel < 40) {
        $group = groupName::leftJoin('domains', 'groupNames.domainID', '=', 'domains.id')
        ->select('groupNames.*', 'domains.parentDomainID', 'domains.name as location')
        ->where([['groupNames.id', '=', $groupID],['domains.parentDomainID', '=', $domain->id]])            
        ->first();
      } else {
        $group = groupName::where([['domainID', '=', $domain->id], ['id', '=', $groupID]])->first();
      }

      if (is_null($group)) { abort(404);}    

      $g = groupName::destroy($groupID);
      
      if (!$g) {
        $request->session()->flash('groupError', 'There was a problem deleting the group. Please try again.');   
      }     
      
      $request->session()->flash('groupSuccess', 'The group was successfully deleted.');

	  	return redirect()->action('groupsController@view', ['urlPrefix' => $urlPrefix]);      
    
    } // end Delete Group


  /*---------------------------------Delete Member----------------------------------------------------------*/   

    public function deleteMember(Request $request, $urlPrefix, $groupID, $contactID) {
    
      $group = groupMember::where([ ['contactID', '=', $contactID], ['groupID', '=', $groupID] ])->delete();

      return redirect()->back();

    
    } // end Delete Group






	/*-----------------------------------------edit Name------------------------------------------*/

   	public function editName(Request $request, $urlPrefix) {
        
      $group = groupName::find($request->groupID);
      if (is_null($group)) { abort(404);}   
      
      $group->groupName = $request->newGroupName;
      $g = $group->save();
      
      if (!$g) {
        $request->session()->flash('groupError', 'There was an error updating ' . $request->newGroupName . '. Please try again.');
      }	

      $request->session()->flash('groupSuccess', 'The group name was successfully updated.');
	  	return redirect()->action('groupsController@view', ['urlPrefix' => $urlPrefix]);
        
    } // end editName



	/*-----------------------------------------getGroupName------------------------------------------*/


   	public function getGroupName(Request $request) {
        
        $group = groupName::find($request->id)->groupName;
        
        // if group not found or does not exit
        if ($group == '') {
            $data = json_encode(array('status' => "error", 'message' => 'Group Name Not Found'));
            return $data;
        }

        $data = json_encode(array('status' => "success", 'message' => 'Group Name found', 'groupName' => $group));
        return $data;
        
    } // end getGroupName


  /*----------------------------Group Add Members ------------------------------------------------*/   

   public function addMembers($urlPrefix, $groupID) {
     
     $breadcrumb = ['groups', ''];
     $domain = \Request::get('domain');
     $group = groupName::where('id', '=', $groupID)->get()->toArray();

     if (empty($group)) {abort(404);}

     $contacts = contacts::select('id', 'fname', 'lname', 'mobile', 'email')->where('domainID', '=', $group[0]['domainID'])->orderBy('lname')->paginate(50);

     // create an array of group members from contacts
     $groupContactsID = contacts::join('groupMembers', 'contacts.id', '=', 'groupMembers.contactID')
     ->select('contacts.id')
     ->where('groupMembers.groupID', '=', $groupID)
     ->get()->toArray();
     $groupContactsID = array_flatten($groupContactsID);
 
     $group = groupName::where('id', '=', $groupID)->first();
     $groupCount = groupMember::select('id')->where('groupID', '=', $groupID)->count();

     return view('groups.addMembers')->with(['domain' => $domain, 'group' => $group, 'contacts' => $contacts, 'breadcrumb' => $breadcrumb, 'groupCount' => $groupCount, 'groupContactsID' => $groupContactsID]);
     
   }   



/*-------------------------Add Group Member----------------------------------------------------------*/


   public function addGroupMember(Request $request)
    {
        
      $groupMember = new groupMember();
      $groupMember->groupID = $request->groupID;
      $groupMember->domainID = $request->domainID;        
      $groupMember->contactID = $request->contactID;
      
      try {
          $groupMember->save();
          $groupCount = groupMember::select('id')->where('groupID', '=', $request->groupID)->count();
          $data = json_encode(array('status' => "success", 'message' => 'Group Member Added - ' . $request->contactID, 'groupCount' => $groupCount));
          return $data;
      }

      catch (Exception $e) {
          $data = json_encode(array('status' => "error", 'message' => 'Error Adding Group Member - ' . $request->contactID));
          return $data;
      }   

    } // end addGroupMember



/*----------------------------Delete Group Member-----------------------------------------------------*/


   public function deleteGroupMember(Request $request)
    {
        
        try {
            groupMember::where([['domainID', '=', $request->domainID], ['groupID', '=', $request->groupID], ['contactID', '=', $request->contactID]])->delete();
            $groupCount = groupMember::select('id')->where('groupID', '=', $request->groupID)->count();
            $data = json_encode(array('status' => "success", 'message' => 'Group Member Deleted - ' . $request->contactID, 'groupCount' => $groupCount));
            return $data;
        }

        catch (Exception $e) {
            $data = json_encode(array('status' => "error", 'message' => 'Error Deleting Group Member - ' . $request->contactID));
            return $data;
        }   


    } // end deleteGroupMember


/*--------------------------------------View Members-----------------------------------------------------*/   

  public function viewMembers($urlPrefix, $groupID) {

    $group = groupName::select('id', 'groupName')->where('id', '=', $groupID)->get()->toArray();
    if (empty($group)) {abort(404);}
    
    // create an array of group members from contacts
    $groupContacts = contacts::join('groupMembers', 'contacts.id', '=', 'groupMembers.contactID')
      ->select('contacts.id', 'contacts.fname', 'contacts.lname', 'contacts.mobile', 'contacts.email', 'groupMembers.id as gid')
      ->where('groupMembers.groupID', '=', $groupID)
      ->get();


    $breadcrumb = ['groups', ''];
    $domain = \Request::get('domain');
    $group = groupName::select('id', 'groupName')->where('id', '=', $groupID)->get();

    return view('groups.viewMembers')->with(['domain' => $domain, 'group' => $group, 'breadcrumb' => $breadcrumb, 'groupContacts' => $groupContacts, 'groupID' => $groupID]);
    
  } // viewMembers



/*-------------------------------Duplicate Groups------------------------------------------------------------*/

   public function duplicateGroup(Request $request, $urlPrefix, $groupID) {
    
    $gn = groupName::find($groupID);

    if (is_null($gn)) {
      $request->session()->flash('groupError', 'There was an error duplicating the group. Please try again.');
    }
    
    // set groupName
    $gn->groupName = $gn->groupName . " - Copy";
    $gn->id = null;
    $newGN = groupName::insertGetId($gn->toArray());

    // get group members
    $groupmembers = groupMember::where('groupID', '=', $groupID)->get();
    // process each groupmember
    $count = 0;
    foreach ($groupmembers as $groupmember) {
        $groupmember->id=null;
        $groupmember->groupID = $newGN;
        groupMember::insert($groupmember->toArray());
        $count = $count + 1;
    }

    $request->session()->flash('groupSuccess', 'The group was successfully duplicated.');

    return redirect()->back();

  } // end duplicateGroup



} // end groupsController
