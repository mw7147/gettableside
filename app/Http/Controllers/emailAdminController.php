<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\sendEmailDomains;
use App\sendEmailAdmin;
use Validator;


class emailAdminController extends Controller
{
    

    /*--------------------  Send View ----------------------------------------*/

    public function sendView(Request $request) {
        
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailboxes = sendEmailAdmin::where('transport', '!=', null)->get();   

        return view('emailAdmin.sendView')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailboxes' => $mailboxes]);

    } // end sendView



    /*--------------------  Send Edit ----------------------------------------*/

    public function sendEdit($emailID) {
        
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailbox = sendEmailAdmin::find($emailID);

        return view('emailAdmin.sendEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailbox' => $mailbox]);

    } // end sendEdit



   /*--------------------  Send Edit Do ----------------------------------------*/

    public function sendEditDo(Request $request, $emailID) {
    
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailbox = sendEmailAdmin::find($emailID);
            $mailbox->fname = $request->fname;
            $mailbox->lname = $request->lname;
            $mailbox->siteURL = $request->siteURL;
            $mailbox->domainID = $request->domainID;
            $mailbox->serverID = env('ORDERS_SERVER_ID');            
        $ok = $mailbox->save();


        if ($ok) {
            $request->session()->flash('updateSuccess', 'Your information was updated.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating your information. Please try again.');
        }

        return redirect()->route('sendEdit', ['emailID' => $emailID]);
        
    } // end sendEditDo



    /*--------------------  Password Reset ----------------------------------------*/

    public function passwordReset($mailtype, $emailID) {
    
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');

        return view('emailAdmin.password')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailtype' => $mailtype, 'emailID' => $emailID]);

    } // end passwordReset



    /*--------------------  Password Reset ----------------------------------------*/

    public function passwordResetDo(Request $request, $mailtype, $emailID) {
        
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');

        // dovecot password
        $query = "update mailboxes set password = ENCRYPT('" . $request->password1 . "', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16))) where id = " . $emailID;
        $ok = DB::connection('emailAdminDB')->update($query);
        
        if ($ok) {
            $request->session()->flash('emailSuccess', 'The password was successfully updated.');
        } else {
            $request->session()->flash('emailError', 'There was an error updating the password. Please try again.');
        }

        if ($mailtype == 'send') { return redirect()->action('emailAdminController@sendView', ['emailID' => $emailID]); }

        return redirect()->action('emailAdminController@receiveView', ['emailID' => $emailID]);
        
    } // end passwordResetDo



    /*--------------------  New Send Address ----------------------------------------*/

    public function sendNew() {
        // uses bounce transport - will not receive emails to mailbox
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');

        return view('emailAdmin.sendNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);

    } // end sendNew




    /*--------------------  New Send Address Do ----------------------------------------*/

    public function sendNewDo(Request $request) {

        // validate form fields
        Validator::make($request->all(), [
            'siteURL' => 'required|exists:domains,httpHost|max:255',
            'domainID' => 'required|exists:domains,id',
            'email' => 'required|email|max:255',
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
        ])->validate();
        
        // uses bounce transport - will not receive emails to mailbox
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $address = explode("@", $request->email);

        $new = new sendEmailAdmin();
            $new->active = "yes";
            $new->mailbox = $request->email;
            $new->fname = $request->fname;
            $new->lname = $request->lname;
            $new->domainID = $request->domainID;
            $new->siteURL = $request->siteURL;
            $new->localPart = trim($address[0]);
            $new->domainPart = trim($address[1]);
            $new->transport = "bounces";
            $new->ratePlan = "send";
            $new->billingStatus = "current";
            $new->serverID = env('ORDERS_SERVER_ID');
        $ok = $new->save();
 
        if ($ok) {
            $request->session()->flash('emailSuccess', 'The email address was successfully created. Remember to reset the password.');
        } else {
            $request->session()->flash('emailError', 'There was an error creating the email address. Please try again.');
        }

        return redirect()->action('emailAdminController@sendView');

    } // end sendNewDo




    /*-------------------- Send Mail Delete ----------------------------------------*/

    public function sendDelete(Request $request, $emailID) {
        
         $ok = sendEmailAdmin::destroy($emailID);
 
         if ($ok) {
             $request->session()->flash('emailSuccess', 'The email address was successfully deleted.');
         } else {
             $request->session()->flash('emailError', 'There was an error deleting the email address. Please try again.');
         }
 
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailboxes = sendEmailAdmin::where('transport', '!=', null)->get();   

        return view('emailAdmin.sendView')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailboxes' => $mailboxes]);
         
     } // end sendDelete
 



    /*--------------------  Receive View ----------------------------------------*/

    public function receiveView(Request $request) {
        
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailboxes = sendEmailAdmin::where('transport', '=', null)->get();

        return view('emailAdmin.receiveView')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailboxes' => $mailboxes]);

    } // end receiveView



    /*--------------------  Receive Edit ----------------------------------------*/

    public function receiveEdit($emailID) {
        
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailbox = sendEmailAdmin::find($emailID);

        return view('emailAdmin.receiveEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailbox' => $mailbox]);

    } // end receiveEdit



   /*--------------------  Receive Edit Do ----------------------------------------*/

    public function receiveEditDo(Request $request, $emailID) {
    
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailbox = sendEmailAdmin::find($emailID);
            $mailbox->fname = $request->fname;
            $mailbox->lname = $request->lname;
            $mailbox->siteURL = $request->siteURL;
            $mailbox->domainID = $request->domainID;
            $mailbox->serverID = env('ORDERS_SERVER_ID');            
        $ok = $mailbox->save();


        if ($ok) {
            $request->session()->flash('updateSuccess', 'Your information was updated.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating your information. Please try again.');
        }

        return redirect()->route('receiveEdit', ['emailID' => $emailID]);
        
    } // end receiveEditDo




    /*--------------------  New Receive Address ----------------------------------------*/

    public function receiveNew() {
        // uses bounce transport - will not receive emails to mailbox
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');

        return view('emailAdmin.receiveNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);

    } // end sendNew



    /*--------------------  New Receive Address Do ----------------------------------------*/

    public function receiveNewDo(Request $request) {

        // validate form fields
        Validator::make($request->all(), [
            'siteURL' => 'required|exists:domains,httpHost|max:255',
            'domainID' => 'required|exists:domains,id',
            'email' => 'required|unique:emailAdminDB.mailboxes,mailbox|email|max:255',
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
        ])->validate();
        
        // uses bounce transport - will not receive emails to mailbox
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $address = explode("@", $request->email);

        $new = new sendEmailAdmin();
        $new->active = "yes";
        $new->mailbox = $request->email;
        $new->fname = $request->fname;
        $new->lname = $request->lname;
        $new->domainID = $request->domainID;
        $new->siteURL = $request->siteURL;
        $new->localPart = trim($address[0]);
        $new->domainPart = trim($address[1]);
        $new->ratePlan = "receive";
        $new->billingStatus = "current";
        $new->serverID = env('ORDERS_SERVER_ID');
        $ok = $new->save();
 
        if ($ok) {
            $request->session()->flash('emailSuccess', 'The email address was successfully created. Remeber to set the password.');
        } else {
            $request->session()->flash('emailError', 'There was an error creating the email address. Please try again.');
        }

        return redirect()->action('emailAdminController@receiveView');

    } // end sendNewDo





    /*-------------------- Receive Mail Delete ----------------------------------------*/

    public function receiveDelete(Request $request, $emailID) {
        
         $ok = sendEmailAdmin::destroy($emailID);
 
         if ($ok) {
             $request->session()->flash('emailSuccess', 'The email address was successfully deleted.');
         } else {
             $request->session()->flash('emailError', 'There was an error deleting the email address. Please try again.');
         }
         
        $breadcrumb = ['dashboard', ''];
        $domain = \Request::get('domain');
        $mailboxes = sendEmailAdmin::where('transport', '=', null)->get();

        return view('emailAdmin.receiveView')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'mailboxes' => $mailboxes]);

     } // end receiveDelete
 
 


    /*----------------- Check Location Email --------------------------------------*/
    
    public function checkLocationEmail(Request $request) {

        // check valid email
        $request->validate([ 'email' => 'email' ]);
        // valid email

        // check domain
        $domain = explode("@", $request->email);
        $domainCheck = sendEmailDomains::where('name', '=', trim($domain[1]))->first();

        if (is_null($domainCheck)) {
            // email domain not serviced
            $message['status'] = "error";
            $message['message'] = "The email for this domain is not serviced by this server. Please check your email and try again."; 
            return json_encode($message);            
        }

        $check = sendEmailAdmin::where("mailbox", '=', $request->email)->first();
        if (is_null($check)) {
        
            // no account exists
        $message['status'] = "ok";
        $message['message'] = $request->email . " is valid system email address.";
        return json_encode($message);
        } else {
        
            // account exists
        $message['status'] = "error";
        $message['message'] = "An account with this email address already exists. Please check your email and try again.";
        return json_encode($message);
        }
    
    } // end checkLocationEmail







} // end emailAdminController
