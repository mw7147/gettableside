<?php

namespace App\Http\Controllers;

use Facades\App\Http\Controllers\ordersController;
use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\menuCategoriesData;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\menuOptionsData;
use App\deliverySession;
use App\menuAddOnsData;
use App\menuSidesData;
use App\ordersDetail;
use App\menuOptions;
use App\menuAddOns;
use App\siteStyles;
use App\menuSides;
use Carbon\Carbon;
use App\menuData;
use App\cartData;
use App\usStates;
use App\contacts;
use App\orders;
use App\domain;
use App\tables;
use App\ccBIN;
use App\menu;
use stdClass;

use Dompdf\Dompdf;

class cartController extends Controller
{


	/*---------------- Add Menu Item To Cart ----------------------------------*/

	public function addCart(Request $request)
	{

		if (empty($request->portion)) {
			return false;
		} // nothing selected - return error
		$portion = explode("|", $request->portion); // [0] = portion, [1] = price
		$menuItem = menuData::find($request->menuDataID);

		// get options
		$menuOptionsData = $this->menuOptions($request->menuOptionsData);
		$menuSidesData = $this->menuSides($request->menuSidesData);
		$menuCategoriesData = menuCategoriesData::find($menuItem->menuCategoriesDataID);

		// set printer menuItem text
		if (is_null($menuItem->printKitchen)) {
			$printOrder = $menuItem->menuItem;
		} else {
			$printOrder = $menuItem->printKitchen;
		}
		if (is_null($menuItem->printReceipt)) {
			$printReceipt = $menuItem->menuItem;
		} else {
			$printReceipt = $menuItem->printReceipt;
		}

		// save cart data
		$cart = new cartData;
			$cart->domainID = $request->domainID;
			$cart->ownerID = $request->ownerID;
			$cart->userID = $request->userID;
			$cart->sessionID = $request->sid;
			$cart->portion = $portion[0];
			// $cart->price = $portion[1] + $menuOptionsData;
			$cart->price = $portion[1];
			$cart->quantity = $request->quantity;
			//$cart->extendedPrice = number_format($request->quantity * ($portion[1] + $menuOptionsData), 2);
			$cart->extendedPrice = number_format($request->quantity * $portion[1], 2);

			$cart->tax = $this->calculateTax( $menuItem->id, ($request->quantity * $portion[1]) );
			$cart->printOrder = $printOrder;
			$cart->printReceipt = $printReceipt;
			$cart->menuDataID = $request->menuDataID;
			$cart->menuCategoriesDataID = $menuItem->menuCategoriesDataID;
			$cart->menuCategoriesDataSortOrder = $menuCategoriesData->sortOrder;
			$cart->menuDataSortOrder = $menuItem->sortOrder;
			$cart->menuItem = $menuItem->menuItem;
			$cart->menuDescription = $menuItem->menuItemDescription;
			$cart->instructions = strip_tags($request->instructions);
			$cart->orderIP = \Request::ip(); // Laravel provided ip address
			$cart->menuOptionsDataID = json_encode($request->menuOptionsData);
			$cart->menuAddOnsDataID = json_encode($request->menuAddOnsData);
			$cart->menuSidesDataID = json_encode($request->menuSidesData);
		$cart->save();

		$info = $menuItem->menuItem . " sucessfully added to cart";
		$addOns = $this->menuAddOns($request->menuAddOnsData, $request->domainID, $request->ownerID, $request->userID, $request->sid, $request->quantity);	
		$numberOfOrder = cartData::where('sessionID', '=', $request->sid)->count();
		$html = $this->orderCart($cart->sessionID);

		$data['numberOfOrder'] = $numberOfOrder;
		$data['html'] = $html;
		return $data;
	} // end addCart


	/*---------------- Calculate Menu Options Price ----------------------------------*/

	protected function menuOptions($ids)
	{

		if (empty($ids) || $ids == "null") {
			return;
		}
		if (!is_array($ids)) {
			$ids = json_decode($ids);
		} // if ajax - $ids is array - otherwise json
		$tl = 0;

		for ($i = 0; $i < count($ids); $i++) {
			$upCharge = menuOptionsData::where('id', '=', $ids[$i])->first()->upCharge;
			if (empty($upCharge)) {
				continue;
			}
			$tl = $tl + $upCharge;
		}

		return $tl;
	} // end menuOption


	/*---------------- Calculate Menu Add Ons Price ----------------------------------*/
	/*  DEPRECATED 
	protected function menuAddOns($ids) {

		if ( empty($ids) || $ids == "null" ) { return; }
		if (!is_array($ids)) { $ids = json_decode($ids); }  // if ajax - $ids is array - otherwise json
		$tl = 0;

		for ($i = 0; $i < count($ids); $i++) {
			$upCharge = menuAddOnsData::where('id', '=', $ids[$i])->first()->upCharge;
			if (empty($upCharge)) { continue; }
			$tl = $tl + $upCharge;
		}

		return $tl;

	} // end menuAddOns
	*/


	/*---------------- Add Menu Add Ons As New Cart Item ----------------------------------*/

	protected function menuAddOns($menuAddOnsData, $domainID, $ownerID, $userID, $sid, $quantity) {

		$tl = 0; // v1 returned total add on charge - total not required v2+

		if (empty($menuAddOnsData)) {
			return $tl;
		}  // nothing to do

		foreach ($menuAddOnsData as $aod) {

			$item = menuAddOnsData::find($aod);
			$menuData = menuData::find($item->menuDataID);
			$menuCategoriesData = menuCategoriesData::find($menuData->menuCategoriesDataID);

			// set printer menuItem text
			if (is_null($menuData->printKitchen)) {
				$printOrder = $menuData->menuItem;
			} else {
				$printOrder = $menuData->printKitchen;
			}
			if (is_null($menuData->printReceipt)) {
				$printReceipt = $menuData->menuItem;
			} else {
				$printReceipt = $menuData->printReceipt;
			}

			// save cart data
			$cart = new cartData;
				$cart->domainID = $domainID;
				$cart->ownerID = $ownerID;
				$cart->userID = $userID;
				$cart->sessionID = $sid;
				$cart->portion = $menuData->portion1;
				$cart->quantity = $quantity;
				$cart->extendedPrice = number_format($quantity * $menuData->price1, 2);
				//$cart->tax = $this->calculateTax( $menuData->id, ($quantity * $menuData->price1) );
				$cart->price = $menuData->price1;
				$cart->printOrder = $printOrder;
				$cart->printReceipt = $printReceipt;
				$cart->menuDataID = $menuData->id;
				$cart->menuCategoriesDataID = $menuData->menuCategoriesDataID;
				$cart->menuCategoriesDataSortOrder = $menuCategoriesData->sortOrder;
				$cart->menuDataSortOrder = $menuData->sortOrder;
				$cart->menuItem = $menuData->menuItem;
				$cart->menuDescription = $menuData->menuItemDescription;
				$cart->orderIP = \Request::ip(); // Laravel provided ip address
			$cart->save();
		} // end for each

		return $tl;
	} // end menuAddOns



	/*---------------- Calculate Menu Sides Price ----------------------------------*/

	protected function menuSides($ids)
	{

		if (empty($ids) || $ids == "null") {
			return;
		}
		if (!is_array($ids)) {
			$ids = json_decode($ids);
		}  // if ajax - $ids is array - otherwise json
		$tl = 0;

		for ($i = 0; $i < count($ids); $i++) {
			$upCharge = menuSidesData::where('id', '=', $ids[$i])->first()->upCharge;
			if (empty($upCharge)) {
				continue;
			}
			$tl = $tl + $upCharge;
		}

		return $tl;
	} // end menuSides


	/*---------------- Calculate Tax ----------------------------------*/

	protected function calculateTax($menuDataID, $itemPrice) {

		$item = menuData::findOrFail($menuDataID);
		
		// menu item tax rate
		if ($item->itemTax) {
			$tax = $itemPrice * $item->itemTaxRate/100;
			return number_format($tax, 2);
		}
		
		// category tax rate
		$category = menuCategoriesData::findOrFail($item->menuCategoriesDataID);
		if ($category->categoryTax) {
			$tax = $itemPrice * $category->categoryTaxRate/100;
			return number_format($tax, 2);
		}

		// global Tax Rate
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $item->domainID)->firstOrFail();
		$tax = $itemPrice * $domainSiteConfig->taxRate/100;
		return number_format($tax, 2);

	} // end calculateTax



	/*---------------- orderCart HTML ----------------------------------*/
	// returns html for orderCart HTML

	public function orderCart($sessionID)
	{

		$cart = cartData::where('sessionID', '=', $sessionID)->get();
		$html = '<table class="table table-striped menuTable">';
		$subTotal = 0;
		foreach ($cart as $item) {

					// calculate menu options price
					$addMenuOptions = $this->menuOptions($item->menuOptionsDataID);
				//	$addMenuAddOns = $this->menuAddOns($item->menuAddOnsDataID);
					$addMenuSides = $this->menuSides($item->menuSidesDataID);
			   //	 $price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;
					$price = $item->price + $addMenuOptions + $addMenuSides;
					$extendedPrice = $price * $item->quantity;


					$html .= '<tr><td class="orderCol1"><a onclick="deleteItem(\'' . $item->id . '\')"><i class="fa fa-times orderDeleteIcon" data-toggle="tooltip" title="Delete ' . $item->menuItem . '"></i></a></td>';
					$html .= '<td class="orderCol2">' . $item->quantity . '</td>';   	 
					$html .= '<td class="orderCol2">' . $item->menuItem . '</td>';   			
					$html .= '<td class="orderCol3">' . $item->portion . '</td>';
	 				$html .= '<td class="orderCol4 pull-right">$' . number_format($extendedPrice, 2) . '</td></tr>';

			$subTotal = $subTotal + $extendedPrice;
		} // end foreach $cart

		if ($subTotal > 0) {
			// don't display 0 total
			$html .= '<tr><td colspan="1"></td><td colspan="3">Sub-Total</td><td class="totalTop pull-right" colspan="1">$' . number_format($subTotal, 2) . '</td></tr>';
		}

		$html .= '</table>';
		return $html;
	} //end orderItem



/*

	$cart = cartData::where('sessionID', '=', $sessionID)->get();
	$html = '<table class="table table-striped menuTable">';
	$total = 0;
	foreach ($cart as $item) {

				// calculate menu options price
				 $addMenuOptions = $this->menuOptions($item->menuOptionsDataID);
			 //	$addMenuAddOns = $this->menuAddOns($item->menuAddOnsDataID);
				 $addMenuSides = $this->menuSides($item->menuSidesDataID);
			//	 $price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;
				 $price = $item->price + $addMenuOptions + $addMenuSides;
				 $total = $total + ($price * $item->quantity);

				$html .= '<tr><td class="orderCol1"><a onclick="deleteItem(\'' . $item->id . '\')"><i class="fa fa-times orderDeleteIcon" data-toggle="tooltip" title="Delete ' . $item->menuItem . '" ></i></a></td>';
				$html .= '<td class="orderCol2">' . $item->quantity . '</td>';   	 
				$html .= '<td class="orderCol2">' . $item->menuItem . '</td>';   			
				$html .= '<td class="orderCol3">' . $item->portion . '</td>';
				 $html .= '<td class="orderCol4 pull-right">$' . number_format($total, 2) . '</td></tr>';

				 
	} // end foreach $cart

	if ($total > 0) {
		// don't display 0 total
		$html .= '<tr><td colspan="1"></td><td colspan="3">Sub-Total</td><td class="totalTop pull-right" colspan="1">$' . number_format($total, 2) . '</td></tr>';
	}

	$html .= '</table>';
	return $html;


*/



















	/*---------------- Clear Cart Data ----------------------------------*/

	protected function clearCart(Request $request)
	{
		$sessionID = session()->getId();
		// delete all items in cart
		cartData::where('sessionID', '=', $sessionID)->delete();
		$domain = \Request::get('domain');
		session()->regenerate();
		if ($domain->type != 9) {
			return redirect('/order');
		} else {
			return redirect('/dine');
		}
		
	} // end clearCart


	/*---------------- Delete Cart Item ----------------------------------*/

	protected function deleteItem(Request $request)
	{

		// delete all items in cart
		cartData::where('id', '=', $request->itemID)->delete();

		return "Item successfully deleted";
	} // end deleteItem


	/*---------------- add Tip  ----------------------------------*/

	protected function addTip(Request $request)
	{

		// delete all items in cart
		$tip = cartData::where('sessionID', '=', $request->sid)->first();
		$tip->tipAmount = $request->tip;
		$tip->save();

		return redirect()->action('cartController@viewCart');
	} // end addTip



	/*---------------- View Order Cart ----------------------------------*/

	public function viewCart(Request $request) {
		$isDelivery = $request->isDelivery;
		$mobile = base64_decode($request->iq);
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain['id'])->first();
		$styles = siteStyles::where('domainID', '=', $domain->id)->first();
		$sid = session()->getId();
		$dc = deliverySession::where('sessionID', '=', $sid)->first();
		if (is_null($dc)) {
			$deliver = 'pickup';
			$deliveryCharge = 0;
		} else {
			$deliver = $dc->delivery;
			$deliveryCharge = $domainSiteConfig->deliveryCharge;
		}
		$transactionFee = $domainSiteConfig->transactionFee;
		$viewCartHTML = $this->viewCartHTML($sid, $domain['id'], $deliver, $deliveryCharge, $transactionFee, $mobile, $isDelivery);
		if($isDelivery == 1 || $isDelivery == '0.00' ){
			if($isDelivery == 1){
				if($viewCartHTML['subTotal'] < $domainSiteConfig->deliveryMinOrder){
					$data = [];
					$data['html'] = 'Unfortunately your order value is less than the minimum value. Please add more items.';
					$data['total'] = 0;
					$data['validDelivery'] = 0;
					return $data;
				}
				$viewCartHTML['deliveryFee'] = $domainSiteConfig->deliveryCharge;
			}else{
				$viewCartHTML['deliveryFee'] = 0;
			}
			$viewCartHTML['validDelivery'] = 1;
			return $viewCartHTML;
		}
		$states = usStates::all();
		$contact = null;
		$todayHours = ordersController::todayHours($domain->id);

		// get stored card info
		$card = null;
		$user = Auth::user();
		if (!is_null($user)) { $card = ccBIN::select('token', 'expireMonth', 'expireYear', 'brand', 'last4')->where('userID', '=', $user->id)->first(); }

		$tip = [];
		$tip['tip'] = $viewCartHTML['tip'];
		$tip['pickupTipAmount'] = $viewCartHTML['pickupTipAmount'];
		$tip['deliveryTipAmount'] = $viewCartHTML['deliveryTipAmount'];

		if ( $domainSiteConfig->orderAhead) {
			$hoursOfOperation = ordersController::hoursOfOperation($domain->id);
		} else {
			$hoursOfOperation = ordersController::todaysHoursOfOperation($domain->id);
		}

		$vueData = new stdClass;
		//$vueData->schedule = menu::where('domainID', '=', $domain->id)->first();
		$vueData->schedule = $hoursOfOperation;
		$vueData->states = usStates::all();
		$vueData->config = $domainSiteConfig;
		$vueData->config->type =$domain->type;
		
		if (Auth::check()) {
			$userID = Auth::id();
			$vueData->contact = contacts::where('tgmUserID', '=', $userID)->first();
			//so older views work?
			$contact = contacts::where('tgmUserID', '=', $userID)->first();
		}                    
		
		$vueData = json_encode($vueData);

		$data = ordersController::chooseMenu($domain->id);
		$open = $data['open'];

		if ($domainPaymentConfig->ccType == 1) {
			// no payment - pickup only
			return view('orders.viewCart')->with([ 'domain' => $domain, 'styles' => $styles, 'html' => $viewCartHTML['html'], 'sid' => $sid, 'total' => $viewCartHTML['total'], 'states' => $states, 'defaultState' => $viewCartHTML['state'], 'tax' =>  $viewCartHTML['tax'], 'deliveryTaxAmount' => $viewCartHTML['deliveryTaxAmount'], 'tip' =>  $viewCartHTML['tip'], 'subTotal' => $viewCartHTML['subTotal'], 'orderDiscount' => $viewCartHTML['orderDiscount'], 'orderDiscountPercent' => $viewCartHTML['orderDiscountPercent'], 'domainSiteConfig' => $domainSiteConfig, 'domainPaymentConfig' => $domainPaymentConfig, 'contact' => $contact, 'open' => $open ]);
		}

		if ($domainPaymentConfig->ccType == 2) {
			// stripe payment
			if ($domainPaymentConfig->mode == 'live') { $key = $domainPaymentConfig->stripeLivePublishableKey; } else { $key = $domainPaymentConfig->stripeTestPublishableKey; }
			return view('orders.viewCartStripe')->with([ 'domain' => $domain, 'styles' => $styles, 'html' => $viewCartHTML['html'], 'sid' => $sid, 'total' => $viewCartHTML['total'], 'states' => $states, 'defaultState' => $viewCartHTML['state'], 'tax' =>  $viewCartHTML['tax'], 'tip' =>  $viewCartHTML['tip'],  'deliveryTaxAmount' => $viewCartHTML['deliveryTaxAmount'], 'subTotal' => $viewCartHTML['subTotal'], 'orderDiscount' => $viewCartHTML['orderDiscount'], 'orderDiscountPercent' => $viewCartHTML['orderDiscountPercent'], 'domainSiteConfig' => $domainSiteConfig, 'domainPaymentConfig' => $domainPaymentConfig, 'contact' => $contact, 'key' => $key, 'tip' => $tip, 'todayHours' => $todayHours, 'vueData' => $vueData, 'open' => $open ]);
		}

		if ($domainPaymentConfig->ccType == 3) {
			// Card Connect payment

			if ($domainPaymentConfig->mode == 'live') {
				$ccGateway = config('services.cardConnect')["tokenizerLive"];
			} else {
				$ccGateway = config('services.cardConnect')["tokenizerTest"];
			} 

			$usercards = null;
			if($user){
				$usercards = \App\UserCard::where('user_id', $user->id)->get();
			}
			return view('orders.viewCartCardConnect')->with([ 'domain' => $domain, 'usercards' => $usercards,  'styles' => $styles, 'html' => $viewCartHTML['html'], 'card' => $card, 'sid' => $sid, 'total' => $viewCartHTML['total'], 'states' => $states, 'defaultState' => $viewCartHTML['state'], 'tax' =>  $viewCartHTML['tax'],  'deliveryTaxAmount' => $viewCartHTML['deliveryTaxAmount'], 'tip' =>  $viewCartHTML['tip'], 'subTotal' => $viewCartHTML['subTotal'], 'orderDiscount' => $viewCartHTML['orderDiscount'], 'orderDiscountPercent' => $viewCartHTML['orderDiscountPercent'], 'domainSiteConfig' => $domainSiteConfig, 'domainPaymentConfig' => $domainPaymentConfig,'contact' => $contact, 'ccGateway' => $ccGateway, 'tip' => $tip, 'todayHours' => $todayHours, 'vueData' => $vueData, 'open' => $open ]);
		}

		if ($domainPaymentConfig->ccType == 4) {
			// aptito payment
			return view('orders.viewCartAuthNet')->with([ 'domain' => $domain, 'styles' => $styles, 'html' => $viewCartHTML['html'], 'sid' => $sid, 'total' => $viewCartHTML['total'], 'states' => $states, 'defaultState' => $viewCartHTML['state'], 'tax' =>  $viewCartHTML['tax'],  'deliveryTaxAmount' => $viewCartHTML['deliveryTaxAmount'], 'tip' =>  $viewCartHTML['tip'], 'subTotal' => $viewCartHTML['subTotal'], 'orderDiscount' => $viewCartHTML['orderDiscount'], 'orderDiscountPercent' => $viewCartHTML['orderDiscountPercent'], 'domainSiteConfig' => $domainSiteConfig, 'domainPaymentConfig' => $domainPaymentConfig,'contact' => $contact, 'delivery' => $deliver, 'open' => $open ]);
		}


		// card processor setup problem - abort transaction
		abort(504);
	} // end clearCart




	/*---------------- View Cart - Dine In ----------------------------------*/

	public function dineInCart(Request $request) {

		$mobile = base64_decode($request->iq);
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->parentDomainID)->first();
		$styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();
		$sid = session()->getId();
		$user = Auth::user();

		// get stored card info
		$card = null;
		$usercards = null;
		if (!is_null($user)) { 
			$card = ccBIN::select('token', 'expireMonth', 'expireYear', 'brand', 'last4')->where('userID', '=', $user->id)->first(); 
			$usercards = \App\UserCard::where('user_id', $user->id)->get();
		}

		
		$dc = deliverySession::where('sessionID', '=', $sid)->first();
		
		if (is_null($dc)) {
			$deliver = 'pickup';
			$deliveryCharge = 0;
		} else {
			$deliver = $dc->delivery;
			$deliveryCharge = $domainSiteConfig->deliveryCharge;
		}

		$transactionFee = $domainSiteConfig->transactionFee;
		
		$viewCartHTML = $this->viewCartHTML($sid, $domain['id'], $deliver, $deliveryCharge, $transactionFee, $mobile);
		$states = usStates::all();
		$contact = null;
		$tables = tables::where('domainID', '=', $domain->id)->orderBy('name')->get();

		$todayHours = ordersController::todayHours($domain->id);

		$tip = [];
		$tip['tip'] = $viewCartHTML['tip'];
		$tip['pickupTipAmount'] = $viewCartHTML['pickupTipAmount'];
		$tip['deliveryTipAmount'] = $viewCartHTML['deliveryTipAmount'];

		if ( $domainSiteConfig->orderAhead) {
			$hoursOfOperation = ordersController::hoursOfOperation($domain->id);
		} else {
			$hoursOfOperation = ordersController::todaysHoursOfOperation($domain->id);
		}

		// Card Connect payment
		if ($domainPaymentConfig->mode == 'live') {
			$ccGateway = config('services.cardConnect')["tokenizerLive"];
		} else {
			$ccGateway = config('services.cardConnect')["tokenizerTest"];
		} 

		return view('orders.viewCartDineIn')->with([ 'domain' => $domain, 'styles' => $styles, 'tables' => $tables, 'user' => $user, 'card' => $card, 'html' => $viewCartHTML['html'], 'sid' => $sid, 'total' => $viewCartHTML['total'], 'states' => $states, 'defaultState' => $viewCartHTML['state'], 'tax' =>  $viewCartHTML['tax'],  'deliveryTaxAmount' => $viewCartHTML['deliveryTaxAmount'], 'tip' =>  $viewCartHTML['tip'], 'subTotal' => $viewCartHTML['subTotal'], 'orderDiscount' => $viewCartHTML['orderDiscount'], 'orderDiscountPercent' => $viewCartHTML['orderDiscountPercent'], 'domainSiteConfig' => $domainSiteConfig, 'domainPaymentConfig' => $domainPaymentConfig,'contact' => $contact, 'ccGateway' => $ccGateway, 'tip' => $tip, 'todayHours' => $todayHours, 'usercards' => $usercards ]);
		

	} // end dineInCart




	/*---------------- viewCart HTML ----------------------------------*/
	// returns html for viewCart HTML

	public function viewCartHTML($sessionID, $domainID, $deliver, $deliveryCharge, $transactionFee, $mobile=0, $isDelivery=null) {
		$cart = cartData::where('sessionID', '=', $sessionID)->get();
		$html = '<table class="table table-striped menuTable">';
		$siteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
		$paymentConfig = domainPaymentConfig::where('domainID', '=', $domainID)->first();
		$total = 0;
		$tax = 0;
		$tip = 0;
		$orderTotal = 0;
		$options = '';
		$addOns = '';
		$orderDiscount = 0;
		$orderDeliveryFee = 0;
		$globalDiscount = 1-($siteConfig->globalDiscount/100);
		$deliveryTaxAmount = $siteConfig->deliveryCharge * $siteConfig->deliveryTaxRate/100;
		(!is_null($transactionFee)) ?: $transactionFee = 0;
		$domain = domain::find($domainID);

		foreach ($cart as $item) {

			// $addMenuAddOns = $this->menuAddOns($item->menuAddOnsDataID);
			// menu options added into price of item
			$addMenuSides = $this->menuSides($item->menuSidesDataID);
			$price = $item->price + $addMenuSides;

			// calculate menu options price
			$addMenuOptions = $this->menuOptions($item->menuOptionsDataID);
			//	$addMenuAddOns = $this->menuAddOns($item->menuAddOnsDataID);
			$addMenuSides = $this->menuSides($item->menuSidesDataID);
			//	$price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;
			$price = $item->price + $addMenuOptions + $addMenuSides;
			$extendedPrice = $item->quantity * $price;
			
			// calculate and save tax
			$itemTax = $this->calculateTax( $item->menuDataID, ($extendedPrice * $globalDiscount) );
			$item->tax = $itemTax;
			$item->globalDiscountPercent = $siteConfig->globalDiscount;
			$item->globalDiscountAmount = number_format($item->extendedPrice * $siteConfig->globalDiscount/100, 2);
			$item->netExtendedPrice = number_format( $item->extendedPrice * $globalDiscount, 2 );
			$item->save();
			$tax = $tax + $itemTax;
			// use for debug
			// echo '<h3 style="color: white; padding-left: 5px;">' . $itemTax . '</h3>';

			$options = $this->viewOptions($item->menuOptionsDataID);
			//	$addOns = $this->viewAddOns($item->menuAddOnsDataID);
			$sides = $this->viewSides($item->menuSidesDataID);
			
			if (!empty($options) && (!empty($addOns))) { $addOns = ", " . $addOns;}
			if (!empty($options) || (!empty($addOns)) && !empty($sides)) { $sides = ", " . $sides;}

			//	 $allOptions = $options . $addOns . $sides;
			$allOptions = $options . $sides;

			// strip any comma's and spaces from right side
			$allOptions = rtrim($allOptions, ', ');

			if (!empty($item->instructions)) {
				if ($allOptions == '' ) {
					$allOptions = '<span>Customer Instructions: ' . $item->instructions . '<br></span>';
				} else {
					$allOptions = $allOptions . '<br><span>Customer Instructions: ' . $item->instructions . '<br></span>';
				}
			}

			$html .= '<tr><td style="width: 30px"><a onclick="deleteItem(\'' . $item->id . '\')"><i class="fa fa-times orderDeleteIcon" data-toggle="tooltip" title="Delete ' . $item->menuItem . '"></i></a></td>';
			$html .= '<td class="">' . $item->quantity . ' Qty</td>';  
			$html .= '<td class="orderCol2">' . $item->menuItem . '</td>';  
			
			if (!$mobile) {	
				// desktop - display portion
				$colspan = 4;	
				$html .= '<td class="orderCol3">' . $item->portion . '</td>';
			} else {
				// mobile - do not display portion
				$colspan = 3;	
			}

			$html .= '<td class="orderCol4">$' . number_format($price, 2) . ' ea</td>';
			$html .= '<td class="orderCol4 pull-right">$' . number_format($extendedPrice, 2) . '</td></tr>';
			$html .= '<tr class="m-b-lg"><td colspan="2"></td><td colspan="3" align="left"><i>' . $allOptions . '</i></td></tr>';
			$tip = number_format($tip + $item->tipAmount, 2);
			$total = number_format($total + $extendedPrice, 2);
					 
		} // end foreach $cart

		if ($total > 0) {
			// don't display 0 total
			$orderDiscount = $siteConfig->globalDiscount/100 * $total;
			//$tax = ($total - $orderDiscount + $siteConfig->transactionFee) * $siteConfig->taxRate/100;
			$pickupTip = number_format(($total - $orderDiscount) * $siteConfig->pickupRecommendedTipPercent/100, 2);
			$deliveryTip = number_format(($total - $orderDiscount) * $siteConfig->deliveryRecommendedTipPercent/100, 2);
			
			//$tip = number_format(($total - $orderDiscount) * $siteConfig->recommendedTipPercent/100, 2);

			
			$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '">Sub-Total</td><td class="totalTop pull-right" colspan="1" id="orderSubTotal">$' . number_format($total, 2) . '</td></tr>';
			
			if ($orderDiscount > 0) {
				$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '">Order Discount (' . $siteConfig->globalDiscount . ' Percent )</td><td class="pull-right" colspan="1" id="orderDiscount">($' . number_format($orderDiscount, 2) . ')</td></tr>';
				$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '">Sub Total After Discount</td><td class="pull-right" colspan="1" id="orderDiscountSubTotal">$' . number_format($total - $orderDiscount, 2) . '</td></tr>';
				
			}
			
			$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '">Tax</td><td class="pull-right" colspan="1" id="orderTax">$' . number_format($tax, 2) . '</td></tr>';
			if ($isDelivery == 1 && $domain->type != 9 ) {
				$orderDeliveryFee = $siteConfig->deliveryCharge;
				$html .= '<tr class="deliveryChargeRow"><td colspan="1"></td><td colspan="'. $colspan . '">Delivery</td><td class="pull-right" colspan="1" id="orderDeliveryFee">$'. number_format($orderDeliveryFee,2) .'</td></tr>';
			}
			$orderTotal = number_format($total + $tip + $tax + $orderDeliveryFee - $orderDiscount + $siteConfig->transactionFee, 2); // don't charge for delivery - default is pickup - delivery charge added in javascript

			if ($paymentConfig->ccType != 0) {

				$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '" style="vertical-align: middle;">Tip Amount</td><td class="pull-right" colspan="1" id="orderTip">$' . number_format(round($tip, 2), 2) . '</td></tr>';

				if ($tip > 0) {
					$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '" style="vertical-align: middle;"></td><td class="pull-right" style="margin-right: -5px;" colspan="1"><button type="button" class="btn btn-primary btn-xs" onclick="$(\'#tipModal\').modal(\'show\')">Change Tip</button></td></tr>';
				} else {
					$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '" style="vertical-align: middle;"></td><td class="pull-right" style="margin-right: -5px;" colspan="1"><button type="button" class="btn btn-primary btn-xs" onclick="$(\'#tipModal\').modal(\'show\')">Adjust Tip</button></td></tr>';
				}
			}



			if ( $transactionFee > 0 ) {
				$html .= '<tr class="transactionFeeRow"><td colspan="1"></td><td colspan="'. $colspan . '">Transaction Fee</td><td class="pull-right" colspan="1" id="transactionFee">$' . number_format(round($siteConfig->transactionFee, 2), 2) . '</td></tr>';
			}


			$html .= '<tr><td colspan="1"></td><td colspan="'. $colspan . '">Total</td><td class="pull-right" colspan="1" style="border-bottom: double;" id="orderTotal">$' . number_format(round($orderTotal, 2), 2) . '</td></tr>';


		}

		$html .= '</table>';

		$data['html'] = $html;
		$data['orderDiscount'] = round($orderDiscount, 2);
		$data['orderDiscountPercent'] = $siteConfig->globalDiscount;
	    $data['subTotal'] = round($total - $orderDiscount, 2);
	    $data['state'] = $siteConfig['state'];
		$data['tax'] = round($tax, 2);
		$data['deliveryTaxAmount'] = round($deliveryTaxAmount, 2);
		$data['tip'] = round($tip, 2);
		$data['deliveryTipAmount'] = $deliveryTip ?? 0;
		$data['pickupTipAmount'] = $pickupTip ?? 0;
		$data['deliveryCharge'] = round($siteConfig->deliveryCharge, 2);
		$data['transactionFee'] = round($transactionFee, 2);
		$data['total'] = round($orderTotal, 2);
		return $data;
	} //end orderItem



	/*---------------- Get Menu Options ----------------------------------*/

	protected function viewOptions($ids)
	{


		if (empty($ids) || $ids == "null") {
			return;
		}
		if (!is_array($ids)) {
			$ids = json_decode($ids);
		} // if ajax - $ids is array - otherwise json
		$options = '';

		for ($i = 0; $i < count($ids); $i++) {
			$tmp = menuOptionsData::where('id', '=', $ids[$i])->first()->optionName;

			if (empty($tmp)) {
				continue;
			}

			if ($i == 0) {
				$options = $tmp;
			} else {
				$options .= ", " . $tmp;
			} // end if

		} // end for

		return $options;
	} // end viewOptions



	/*---------------- Get Menu Add Ons  ----------------------------------*/

	protected function viewAddOns($ids)
	{

		if (empty($ids) || $ids == "null") {
			return;
		}
		if (!is_array($ids)) {
			$ids = json_decode($ids);
		} // if ajax - $ids is array - otherwise json
		$addOns = '';

		for ($i = 0; $i < count($ids); $i++) {
			$tmp = menuAddOnsData::where('id', '=', $ids[$i])->first()->addOnsName;

			if (empty($tmp)) {
				continue;
			}

			if ($i == 0) {
				$addOns = $tmp;
			} else {
				$addOns .= ", " . $tmp;
			} // end if

		} // end for

		return $addOns;
	} // end viewAddOns

	/*---------------- Get Side Items  ----------------------------------*/

	protected function viewSides($ids)
	{

		if (empty($ids) || $ids == "null") {
			return;
		}
		if (!is_array($ids)) {
			$ids = json_decode($ids);
		} // if ajax - $ids is array - otherwise json
		$sides = '';

		for ($i = 0; $i < count($ids); $i++) {
			$tmp = menuSidesData::where('id', '=', $ids[$i])->first()->sideName;

			if (empty($tmp)) {
				continue;
			}

			if ($i == 0) {
				$sides = $tmp;
			} else {
				$sides .= ", " . $tmp;
			} // end if

		} // end for

		return $sides;
	} // end viewSides


	/*---------------- Place Order  ----------------------------------*/

	public function completeOrder(Request $request)
	{

		$numOnly = CellNumberData::numbersOnly($request->mobile);
		$message = [];

		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $request->did)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $request->did)->first();

		$contact = $this->checkContact($request, $numOnly);

		// update or create contact based on email and domainID
		$contact = contacts::updateOrCreate([
			'email' => $request->email,
			'domainID' => $request->did
		], [
			'fname' => $request->fname,
			'lname' => $request->lname,
			'userID' => $request->userID,
			'mobile' => $request->mobile,
			'unformattedMobile' => $numOnly,
			'address1' => $request->address1,
			'address2' => $request->address2,
			'city' => $request->city,
			'state' => $request->State,
			'zipCode' => $request->zipCode
		]);

		$orderCheck = orders::where('sessionID', '=', $request->sid)->first();
		if (!empty($orderCheck)) {
			return "Your Order Has Already Been Processed.";
		}



		$order = new orders;
		$order->domainID = $request->did;
		$order->ownerID = $request->ownerID;
		$order->contactID = $contact->id;
		$order->subTotal = $request->subTotal;
		$order->tax = $request->tax;
		$order->restaurantEmailAddress = $domainSiteConfig->sendEmail;
		$order->customerEmailAddress = $request->email;
		$order->customerMobile = $request->mobile;
		$order->total = $request->total;
		$order->orderIP = \Request::ip();
		$order->sessionID = $request->sid;
		$order->ccType = $domainPaymentConfig->ccType;
		$order->userAgent = $request->userAgent;
		$order->orderInProcess = 1; //set In process flag
		$order->orderRefunded = 0; // order not refunded
		$order->save();

		$details = cartData::where('sessionID', '=', $request->sid)->get();
		foreach ($details as $detail) {
			$od = new ordersDetail;
			$od->domainID = $detail->domainID;
			$od->ownerID = $detail->ownerID;
			$od->contactID = $contact->id;
			$od->sessionID = $detail->sessionID;
			$od->menuDataID = $detail->menuDataID;
			$od->menuItem = $detail->menuItem;
			$od->menuDescription = $detail->menuDescription;
			$od->portion = $detail->portion;
			$od->printOrder = $detail->printOrder;
			$od->printReceipt = $detail->printReceipt;
			$od->price = $detail->price;
			$od->menuOptionsDataID = $detail->menuOptionsDataID;
			$od->menuAddOnsDataID = $detail->menuAddOnsDataID;
			$od->menuSidesDataID = $detail->menuSidesDataID;
			$od->instructions = $detail->instructions;
			$od->orderIP = $detail->orderIP;
			$od->save();
			cartData::destroy($detail->id);
		} // end foreach

		//create PDF - create pdf and return pdf file path
		$pdf = $this->createPDF($contact, $order, $domainSiteConfig);

		// send customer email
		$sendMail = $this->sendOrderEmail($contact, $order, $domainSiteConfig);

		$order->emailData = $sendMail['html'];
		$order->pdfPath = $pdf;
		$order->save();

		// send to hp ePrint service if subscribed
		if (!empty($domainSiteConfig->hpEprint)) {
			$sendPDF = $this->sendPDF($contact, $order, $domainSiteConfig);
		}

		session()->regenerate();
		$message['orderAlert'] = $domainSiteConfig->orderAlert;
		$message['orderID'] = $order->sessionID;
		$message['numSent'] = $sendMail['numSent'];
		return json_encode($message);
	} // end placeOrder 





	/*---------------- Send Order Email  ----------------------------------*/

	public function sendOrderEmail($contact, $order, $domainSiteConfig)
	{

		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();
		$domain = domain::where('id', '=', $order->domainID)->first();

		$orderHTML = $this->emailHTML($order->sessionID, $order->domainID, $order->orderType, $order->deliveryCharge, $order->tip);
		$orderName = $this->orderName($contact);

		$view = \View::make('orders.viewEmail', ['domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order]);

		// create the email html
		$type = "html";
		$textBody = '';
		$to['email'] = $order->customerEmailAddress;
		$to['name'] = $contact->fname . " " . $contact->lname;
		$sender = $domainSiteConfig->sendEmail;
		$orderReceiveEmail = $domainSiteConfig->orderReceiveEmail;
		$html = $view->render();
		$subject = $domainSiteConfig->locationName . "Online Order Confirmation";
		// Create the Transport using HWCTMailer
		$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

		// Create the Mailer using your created Transport
		$mailer = HWCTMailer::createMailer($transport);

		// Create a message, $subject, $from, $message
		$message = HWCTMailer::createOrderMessage($subject, $sender, $html, $textBody, $type, $orderReceiveEmail);

		$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email');

		$data = [];
		$data['numSent'] = $numSent;
		$data['html'] = htmlentities($html);
		/*
        echo $html;
		exit();
		*/


		return $data;
	} // end sendOrderEmail


	/*---------------- View Order Email  ----------------------------------*/

	public function viewEmail($ordersID)
	{

		$orders = orders::find($ordersID);
		if (empty($orders)) {
			abort(404);
		}

		$domain = \Request::get('domain');
		if ($orders->domainID != $domain['id']) {
			abort(404);
		}

		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
		//dd($domainSiteConfig);
		$orders = orders::find($ordersID);
		$contact = contacts::where('id', '=', $orders->contactID)->first();

		$orderHTML = $this->emailHTML($orders->sessionID, $orders->domainID);
		$orderName = $this->orderName($contact);

		$contact = contacts::find($orders->contactID);
		$orderName = $this->orderName($contact);

		// uncomment to test pdf
		//$pdf = $this->createPDF($contact, $orders, $domainSiteConfig);
		$sendPDF = $this->sendPDF($contact, $orders, $domainSiteConfig);

		return view('orders.viewEmail')->with(['domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $orders]);
	} // end sendOrderEmail


	/*---------------- orderName  ----------------------------------*/


	public function orderName($contact)
	{

		$orderName = $contact->fname . " " . $contact->lname . "<br>";
		$orderName .= $contact->address1 . "<br>";
		if (!empty($contact->address2)) {
			$orderName .= $contact->address2 . "<br>";
		}
		$orderName .= $contact->city . ", " . $contact->state . " " . $contact->zipCode . "<br>";
		$orderName .= $contact->mobile;

		return $orderName;
	} // end orderName






	/*----------------  Email HTML ----------------------------------*/
	// returns html for order email

	public function emailHTML($sessionID, $domainID, $deliver, $deliveryCharge, $tip)
	{

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="768" align="center" class="wrapper">';
		$siteConfig = domainSiteConfig::find($domainID)->first();
		$total = 0;
		$tax = 0;
		$orderTotal = 0;
		$options = '';
		$addOns = '';
		foreach ($cart as $item) {

			// calculate menu options price
			$addMenuOptions = $this->menuOptions($item->menuOptionsDataID);
			$addMenuAddOns = $this->menuAddOns($item->menuAddOnsDataID);
			$addMenuSides = $this->menuSides($item->menuSidesDataID);
			$price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;

			$options = $this->viewOptions($item->menuOptionsDataID);
			$addOns = $this->viewAddOns($item->menuAddOnsDataID);
			$sides = $this->viewSides($item->menuSidesDataID);

			if (!empty($options) && (!empty($addOns))) {
				$addOns = ", " . $addOns;
			}
			if (!empty($options) || (!empty($addOns)) && !empty($sides)) {
				$sides = ", " . $sides;
			}

			$allOptions = $options . $addOns . $sides;

			if (!empty($item->instructions)) {
				$allOptions .= '<br><br><span">Customer Instructions: ' . $item->instructions . '</span>';
			}

			$html .= '<tr>';
			$html .= '<td class="table-padding" align="left" width="35%" style="font-size: 14px; font-family: Arial, sans-serif;">' . $item->menuItem . '</td>';
			$html .= '<td class="table-padding" width="50%" align="left" style="font-size: 14px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
			$html .= '<td class="table-padding" align="right" width="15%" style="font-size: 14px; font-family: Arial, sans-serif;">$' . $price . '</td></tr>';
			$html .= '<tr></td><td class="table-bottom" style="padding-left: 30px; padding-bottom: 10px; font-size: 14px; font-family: Arial, sans-serif;" colspan="2"><i>' . $allOptions . '</i></td><td class="table-bottom"></tr>';

			$total = $total + $price;
		} // end foreach $cart

		if ($total > 0) {
			// don't display 0 total
			$tax = $total * $siteConfig->taxRate / 100;
			$orderTotal = number_format($total + $tax + $order->tip + $order->deliveryCharge, 2);
			$html .= '<tr><td style="padding-top: 28px; padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Sub-Total</td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . $total . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Tax</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tax, 2) . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Tip</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tip, 2) . '</td></tr>';

			if ($deliver == 'delivery') {
				$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
				<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Delivery</td>
				<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($deliveryCharge, 2) . '</td></tr>';
			}

			$html .= '<tr><td colspan="1"></td><td colspan="1" style="font-size: 14px; font-family: Arial, sans-serif;"><b>Total</b></td><td align="right" colspan="1" style="font-size: 14px; font-family: Arial, sans-serif;"><b>$' . $orderTotal . '</b></td></tr>';
		}

		$html .= '</table>';

		return $html;
	} //end orderItem




	/*---------------- Registered Number Check  ----------------------------------*/

	public function registeredNumber(Request $request)
	{

		// cell number check
		$cellCheck = CellNumberData::cellData($request->mobile);

		if ($cellCheck['status'] == 'error') {
			// phone number not cell number - return error
			$data = json_encode(array('status' => "error", 'message' => 'The mobile number given is not a valid mobile number. Please provide a valid mobile number.'));
			return $data;
		}

		$data = json_encode(array('status' => "registered", 'message' => 'The mobile number given is a valid mobile number.', 'fn' => $cellCheck['formattedNumber']));
		return $data;
	} // end registeredNumber



	/*---------------- Registered Number In Database  ----------------------------------*/

	public function rnCheck(Request $request)
	{

		// cell number check
        $numOnly = CellNumberData::numbersOnly($request->mobile);
        $cellCheck = registeredNumbers::find($numOnly);

		if ($cellCheck['status'] == 'error') {
			// phone number not cell number - return error
			$data = json_encode(array('status' => "error"));
			return $data;
		}

		$data = json_encode(array('status' => "valid"));
		return $data;
	} // end registeredNumber

	/*---------------- View Order Email  ----------------------------------*/

	public function viewOrder($sessionID)
	{

		$domain = \Request::get('domain');
		$orders = orders::where('sessionID', '=', $sessionID)->first();
		if (empty($orders)) {
			abort(404);
		}

		$domain = \Request::get('domain');
		if ($orders->domainID != $domain['id']) {
			abort(404);
		}

		return view('orders.viewOrder')->with(['html' => $orders->emailData, 'domain' => $domain]);
	} // end sendOrderEmail



	/*---------------- create PDF  ----------------------------------*/


	public function createPDF($contact, $order, $domainSiteConfig)
	{


		$orderHTML = $this->pdfHTML($order->sessionID, $order->domainID, $order->orderType, $order->deliveryCharge, $order->tip);
		$domain = domain::where('id', '=', $order->domainID)->first();
		$orderName = $this->orderName($contact);

		$pdf = \View::make('orders.pdfEmail', ['domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'orderHTML' => $orderHTML, 'orderName' => $orderName, 'orders' => $order]);

		$pdfHTML = $pdf->render();

		$dompdf = new Dompdf();
		$dompdf->loadHtml($pdfHTML);
		$dompdf->setPaper('letter', 'portrait');
		$dompdf->render();
		$fp = env('PDF_ROOT') . $order->domainID . "/" . $order->id . ".pdf";
		//file_put_contents($fp, $dompdf->output() );
		Storage::put($fp, $dompdf->output());
		unset($dompdf);

		return $fp;
	} // end createPDF




	/*----------------  pdf HTML ----------------------------------*/
	// returns html for order pdf

	public function pdfHTML($sessionID, $domainID, $delivery, $deliveryCharge, $tip)
	{

		$cart = ordersDetail::where('sessionID', '=', $sessionID)->get();
		$html = '<table border="0" cellpadding="0" cellspacing="0" width="475" align="center" class="wrapper">';
		$siteConfig = domainSiteConfig::find($domainID)->first();
		$total = 0;
		$tax = 0;
		$orderTotal = 0;
		$options = '';
		$addOns = '';
		foreach ($cart as $item) {

			// calculate menu options price
			$addMenuOptions = $this->menuOptions($item->menuOptionsDataID);
			$addMenuAddOns = $this->menuAddOns($item->menuAddOnsDataID);
			$addMenuSides = $this->menuSides($item->menuSidesDataID);
			$price = $item->price + $addMenuOptions + $addMenuAddOns + $addMenuSides;

			$options = $this->viewOptions($item->menuOptionsDataID);
			$addOns = $this->viewAddOns($item->menuAddOnsDataID);
			$sides = $this->viewSides($item->menuSidesDataID);

			if (!empty($options) && (!empty($addOns))) {
				$addOns = ", " . $addOns;
			}
			if (!empty($options) || (!empty($addOns)) && !empty($sides)) {
				$sides = ", " . $sides;
			}

			$allOptions = $options . $addOns . $sides;

			if (!empty($item->instructions)) {
				$allOptions .= '<br><br><span">Customer Instructions: ' . $item->instructions . '</span>';
			}

			$html .= '<tr>';
			$html .= '<td class="table-padding" align="left" width="35%" style="font-size: 13px; font-family: Arial, sans-serif;">' . $item->menuItem . '</td>';
			$html .= '<td class="table-padding" width="50%" align="left" style="font-size: 13px; font-family: Arial, sans-serif;">' . $item->portion . '</td>';
			$html .= '<td class="table-padding" align="right" width="15%" style="font-size: 13px; font-family: Arial, sans-serif;">$' . $price . '</td></tr>';
			$html .= '<tr></td><td class="table-bottom" style="padding-left: 30px; padding-bottom: 10px; font-size: 13px; font-family: Arial, sans-serif;" colspan="2"><i>' . $allOptions . '</i></td><td class="table-bottom"></tr><tr><td colspan="3"><hr></td></tr>';

			$total = $total + $price;
		} // end foreach $cart

		if ($total > 0) {
			// don't display 0 total
			$tax = $total * $siteConfig->taxRate / 100;
			$orderTotal = number_format($total + $tax + $tip + $deliveryCharge, 2);
			$html .= '<tr><td style="padding-top: 28px; padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 13px; font-family: Arial, sans-serif;" colspan="1">Sub-Total</td>
					<td style="padding-top: 28px; padding-bottom: 12px; font-size: 13px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . $total . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Tax</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tax, 2) . '</td></tr>';

			$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Tip</td>
					<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($tip, 2) . '</td></tr>';

			if ($deliver == 'delivery') {
				$html .= '<tr><td style="padding-bottom: 12px;" colspan="1"></td>
				<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" colspan="1">Delivery</td>
				<td style="padding-bottom: 12px; font-size: 14px; font-family: Arial, sans-serif;" align="right" colspan="1">$' . number_format($order->delivery, 2) . '</td></tr>';
			}

			$html .= '<tr><td colspan="1"></td><td colspan="1" style="font-size: 13px; font-family: Arial, sans-serif;"><b>Total</b></td><td align="right" colspan="1" style="font-size: 14px; font-family: Arial, sans-serif;"><b>$' . $orderTotal . '</b></td></tr>';
		}

		$html .= '</table>';

		return $html;
	} //end pdfHTML




	/*---------------- Send PDF to hp ePrint service  ----------------------------------*/

	public function sendPDF($contact, $order, $domainSiteConfig)
	{

		// create the swiftMessage
		$to['email'] = $domainSiteConfig->hpEprint;
		$to['name'] = $domainSiteConfig->locationName;
		$sender = $domainSiteConfig->sendEmail;
		$hpEprint = $domainSiteConfig->hpEprint;
		$pdfPath = env("PDF_PUBLIC_ROOT") . $order->pdfPath;
		$subject = $domainSiteConfig->locationName . "Online Order";

		// Create the Transport using HWCTMailer
		$transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

		// Create the Mailer using your created Transport
		$mailer = HWCTMailer::createMailer($transport);

		// Create a message, $subject, $from, $message
		$message = HWCTMailer::createPDFMessage($subject, $sender, $pdfPath);

		// Send the message
		$numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, 'email');

		return $numSent;
	} // end sendOrderEmail





	/*---------------- Ajax resend PDF to hp ePrint service  ----------------------------------*/

	public function reSendPDF(Request $request)
	{

		$order = orders::find($request->orderID);
		$contact = contacts::find($order->contactID);
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $order->domainID)->first();

		$sendPDF = $this->sendPDF($contact, $order, $domainSiteConfig);

		if ($sendPDF) {
			$message = "The order has been sent to the ePrint Service.";
		} else {
			$message = "There was a problem with the ePrint Service. Please try again";
		}

		return $message;
	} // end reSendPDF



	/*---------------- Check Contact - Update or Create Contact  ----------------------------------*/

	public function checkContact($contactInfo, $numOnly)
	{

		// update or create contact based on email and domainID
		$contact = contacts::updateOrCreate([
			'email' => $contactInfo->email,
			'domainID' => $contactInfo->did
		], [
			'fname' => $contactInfo->fname,
			'lname' => $contactInfo->lname,
			'userID' => $contactInfo->userID,
			'mobile' => $contactInfo->mobile,
			'unformattedMobile' => $numOnly,
			'address1' => $contactInfo->address1,
			'address2' => $contactInfo->address2,
			'city' => $contactInfo->city,
			'state' => $contactInfo->state,
			'zipCode' => $contactInfo->zipCode
		]);

		return $contact;
	} // end sendOrderEmail


	/*---------------- Update delivery session  ----------------------------------*/

	public function deliverySession(Request $request)
	{

		// update or create contact based on email and domainID
		if ($request->delivery == 'pickup') {
			// pickup
			deliverySession::where('sessionID', '=', $request->sid)->delete();
		} else {
			//delivery
			$ds = deliverySession::updateOrCreate([
				'sessionID' => $request->sid
			], [
				'domainID' => $request->domainID,
				'ownerID' => $request->ownerID,
				'userID' => $request->userID,
				'delivery' => $request->delivery
			]);
		}
		return "Delivery Set";
	} // end deliverySession

	/*---------------- Start checkCartDataStatus  ----------------------------------*/

	public function checkCartDataStatus(Request $request)
	{
		$inActiveMenuData = 0;
		$inActiveMenuDataId = [];
		$sessionID = session()->getId();
		$cart = cartData::where('sessionID', '=', $sessionID)->get();
		foreach($cart as $item){
			if($item->menuData->active != 1){
				$inActiveMenuData++;
				$inActiveMenuDataId[] = $item->id;
			}
		}
		cartData::whereIn('id', $inActiveMenuDataId)->delete();
		return $inActiveMenuData;

	} // end checkCartDataStatus


} // end cartController