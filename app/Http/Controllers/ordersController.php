<?php

namespace App\Http\Controllers;

use Facades\App\Http\Controllers\cartController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\menuCategoriesData;
use App\suspendOperation;
use App\domainSiteConfig;
use App\menuOptionsData;
use App\menuAddOnsData;
use App\menuCategories;
use App\menuSidesData;
use App\menuOptions;
use App\menuAddOns;
use App\siteStyles;
use App\menuImages;
use App\menuSides;
use Carbon\Carbon;
use App\siteData;
use App\menuData;
use App\usStates;
use App\contacts;
use App\tables;
use App\domain;
use App\ccBIN;
use App\menu;
use App\cartData;
use stdClass;

class ordersController extends Controller
{

	public $html;

	/*---------------- orders page getTableSide (/order) ----------------------------------*/

	public function order(Request $request)
	{

		$domain = \Request::get('domain');
		if ($domain->id == 1) {
			return view('auth.login')->with(['domain' => $domain]);
		}

		// splash page
		if ($domain->type == 7) {
			$locations = domain::select('domains.*', 'domainSiteConfig.*')
				->leftJoin('domainSiteConfig', 'domains.id', '=', 'domainSiteConfig.domainID')
				->where('parentDomainID', '=', $domain->parentDomainID)
				->where('type', '!=', 7)
				->get();

			$splash = domainSiteConfig::where('domainID', '=', $domain->id)->first();
			$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
			$data = siteData::where('domainID', '=', $domain->parentDomainID)->first();
			$styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();

			return view('orders.splash')->with(['domain' => $domain, 'domainSiteConfig' => $domainSiteConfig, 'data' => $data, 'styles' => $styles, 'locations' => $locations, 'splash' => $splash]);
		}  // end if splash page

		if($domain->type == 9){
			return redirect('/dine');
		}

		// child page - use parent domain and style
		$childTypes = array(8,9); // list of child domain types - id from type table
		if (in_array($domain->type, $childTypes)) {
			//dd("orders controller - line 65 - on /orders page!");
			$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
			$styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();
		} else {
			$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
			$styles = siteStyles::where('domainID', '=', $domain->id)->first();
		}

		// set sessionID
		$contact = [];
		$card = null;
		$userID = '';
		$user = Auth::user();
		$sid = session()->getId();
		$numberOfOrder = cartData::where('sessionID', '=', $sid)->count();
		if (!is_null($user)) { $card = ccBIN::select('token', 'expireMonth', 'expireYear', 'brand', 'last4')->where('userID', '=', $user->id)->first(); }
		
		if ($domain->useParentMenu) {
			$domainID = $domain->parentDomainID;
		} else {
			$domainID = $domain->id;
		}

		$data = $this->chooseMenu($domainID);

		if ($data['open'] == 'closed' && $data['menu'] == '' && empty($data['categories'])) {
			return redirect('/login');
		}
		if ( $domain->type != 9 ) {
			$menuHeader = $data['menuHeader'] ?? '';
		} else {
			$menuHeader = '';
		}

		$showHeaderClosed = $data['showHeaderClosed'];
		$menu = $data['menu'] ?? '';
		$open = $data['open'];
		$categories = $data['categories'] ?? '';
		$categoryType = $data['categoryType'] ?? '';
		$smallMenuImages = $data['smallMenuImages'] ?? '';
		$largeMenuImages = $data['largeMenuImages'] ?? '';
		$data = siteData::where('domainID', '=', $domainID)->first();
		$pageName = "order";
		

		if ( $domainSiteConfig->orderAhead) {
			$hoursOfOperation = $this->hoursOfOperation($domainID);
		} else {
			$hoursOfOperation = $this->todaysHoursOfOperation($domainID);
		}

		
		if (Auth::check()) {
			$contact = contacts::where('tgmUserID', '=', $user->id)->where('tgmDomainID', '=', $domainID)->first();
		}
		
		// getTableSide Vue Component
		if ($domain->type != 9) {
			$vueData = new stdClass;
			$vueData->schedule = $hoursOfOperation;
			$vueData->states = usStates::all();
			$vueData->config = $domainSiteConfig;
			$vueData->config->type =$domain->type;
			
			 if (Auth::check()) {
				$userID = Auth::id();
				$vueData->contact = contacts::where('tgmUserID', '=', $userID)->first();
			}
			
			$vueData = json_encode($vueData);
		} else {
			$vueData = '';
		}
		
		// get cart HTML
		if ($open == 'open') {

			$suspend = $this->checkSuspend($domain->id);
			if ($suspend['suspended']) {
				//operations suspended
				$orderHTML = '<p class="closed">Ordering is temporarily suspended.<br>Ordering will resume at ' . $suspend['resumeTime'] . '.<br>We apologize for the inconvenience.</p>';
				$open = "closed"; // remove order buttons on default menu items
			} else {
				// operations normal
				$orderHTML = cartController::orderCart($sid);
			}
		} else {
			//operations closed
			$orderHTML = cartController::orderCart($sid);
			$orderHTML = $orderHTML.'<p class="closed">' . $domainSiteConfig->locationName . ' is closed.</p>';
		}
		
		if ( $domain->type >= 4 && $domain->type <= 6 ) {
			return view('orders.orders')->with(['domain' => $domain, 'sid' => $sid, 'orderHTML' => $orderHTML, 'userID' => $userID, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'categories' => $categories, 'menu' => $menu, 'open' => $open, 'domainSiteConfig' => $domainSiteConfig, 'categoryType' => $categoryType, 'smallMenuImages' => $smallMenuImages, 'largeMenuImages' => $largeMenuImages, 'menuHeader' => $menuHeader, 'showHeaderClosed' => $showHeaderClosed, 'vueData' => $vueData, 'contact' => $contact, 'numberOfOrder' => $numberOfOrder]);
		}

		return view('orders.iframeOrder')->with(['domain' => $domain, 'sid' => $sid, 'orderHTML' => $orderHTML, 'userID' => $userID, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'categories' => $categories, 'menu' => $menu, 'open' => $open, 'domainSiteConfig' => $domainSiteConfig, 'categoryType' => $categoryType, 'smallMenuImages' => $smallMenuImages, 'largeMenuImages' => $largeMenuImages, 'menuHeader' => $menuHeader, 'showHeaderClosed' => $showHeaderClosed, 'vueData' => $vueData, 'contact' => $contact, 'numberOfOrder' => $numberOfOrder]);
	} // end orders



	/*---------------- order page dineTableSide (/dine) ----------------------------------*/

	public function dineIn(Request $request)
	{
		$contact = [];
		$domain = \Request::get('domain');
		$tableID = $request->table ?? "";
		$card = null;

		if ($domain->id == 1) {
			return view('auth.login')->with(['domain' => $domain]);
		}

		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->parentDomainID)->first();
		$styles = siteStyles::where('domainID', '=', $domain->parentDomainID)->first();
		$tables = tables::where('domainID', '=', $domain->id)->orderBy('name')->get();

		// set sessionID
		$user = Auth::user();
		if (!is_null($user)) { $card = ccBIN::select('token', 'expireMonth', 'expireYear')->where('userID', '=', $user->id)->first(); }
		$sid = session()->getId();
		$numberOfOrder = cartData::where('sessionID', '=', $sid)->count();
		if ($domain->useParentMenu) {
			$domainID = $domain->parentDomainID;
		} else {
			$domainID = $domain->id;
		}

		$data = $this->chooseMenu($domainID);
		if ($data['open'] == 'closed' && $data['menu'] == '' && empty($data['categories'])) {
			return redirect('/login');
		}

		$menuHeader = $data['menuHeader'] ?? '';
		
		if ( $domain->type != 9 ) {
			$menuHeader = $data['menuHeader'] ?? '';
		} else {
			$menuHeader = '';
		}

		$showHeaderClosed = $data['showHeaderClosed'];
		$menu = $data['menu'] ?? '';
		$open = ($domainSiteConfig->orderAhead) ? 'open' : $data['open'];
		$categories = $data['categories'] ?? '';
		$categoryType = $data['categoryType'] ?? '';
		$smallMenuImages = $data['smallMenuImages'] ?? '';
		$largeMenuImages = $data['largeMenuImages'] ?? '';
		$data = siteData::where('domainID', '=', $domainID)->first();
		$pageName = "order";

		if ( $domainSiteConfig->orderAhead) {
			$hoursOfOperation = $this->hoursOfOperation($domainID);
		} else {
			$hoursOfOperation = $this->todaysHoursOfOperation($domainID);
		}

		if (Auth::check()) {
			$contact = contacts::where('tgmUserID', '=', $user->id)->where('tgmDomainID', '=', $domainID)->first();
		}

		// get cart HTML
		if ($open == 'open') {

			$suspend = $this->checkSuspend($domain->id);
			if ($suspend['suspended']) {
				//operations suspended
				$orderHTML = '<p class="closed">Ordering is temporarily suspended.<br>Ordering will resume at ' . $suspend['resumeTime'] . '.<br>We apologize for the inconvenience.</p>';
				$open = "closed"; // remove order buttons on default menu items
			} else {
				// operations normal
				$orderHTML = cartController::orderCart($sid);
			}
		} else {
			//operations closed
			$orderHTML = '<p class="closed">' . $domainSiteConfig->locationName . ' is closed.</p>';
		}

		return view('orders.iframeOrder')->with(['domain' => $domain, 'tableID' => $tableID, 'tables' => $tables, 'sid' => $sid, 'card' => $card, 'orderHTML' => $orderHTML, 'user' => $user, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'categories' => $categories, 'menu' => $menu, 'open' => $open, 'domainSiteConfig' => $domainSiteConfig, 'categoryType' => $categoryType, 'smallMenuImages' => $smallMenuImages, 'largeMenuImages' => $largeMenuImages, 'menuHeader' => $menuHeader, 'showHeaderClosed' => $showHeaderClosed,'contact' => $contact, 'numberOfOrder' => $numberOfOrder]);
	
	} // end dine



	/*-----------------------------------Check Suspend-------------------------------------*/

	public function checkSuspend($domainID)
	{

		$so = [];
		$dt = Carbon::now();
		$suspend = suspendOperation::where('domainID', '=', $domainID)->first();

		if (is_null($suspend)) {
			// not suspended - return false
			$so['suspended'] = false;
			$so['resumeTime'] = '';
			return $so;
		}

		// suspended today - check time
		$dt2 = Carbon::parse($suspend->stopTime);
		$so['suspended'] = $dt->lessThanOrEqualTo($dt2);
		$so['resumeTime'] = $dt2->format('h:i A');

		return $so;
	} // end checkSuspend



	/*---------------- Open/Close/Menu Selection ----------------------------------*/


	public function chooseMenu($domainID)
	{

		$dt = Carbon::now();
		//  get menuID, open or closed based on day of week and time

		// need vacation check

		$data = $this->dowOpenClose($dt, $domainID);

		// if closed - return default menu, closed
		if ($data['open'] == 'closed') {

			$m = menu::where([['domainID', '=', $domainID], ['defaultMenu', '=', 'yes']])->first();

			if (is_null($m)) {
				// get first menu - use as default
				$m = menu::where('domainID', '=', $domainID)->first();
			}

			if (is_null($m)) {
				// no menu found
				$menu = ['open' => 'closed', 'menu' => '', 'categories' => []];
				return $menu;
			} // no menu

			// return default menu - restaurant closed
			$categoryType = menuCategories::where('id', '=', $m->menuCategoriesID)->value('type');

			if ($categoryType == "picture") {
				$smallMenuImages = menuImages::where([['menuID', '=', $m->id], ['imageSize', '=', 'Small']])->orderBy('menuDataID')->get();
				$largeMenuImages = menuImages::where([['menuID', '=', $m->id], ['imageSize', '=', 'Large']])->orderBy('menuDataID')->get();
			} else {
				$smallMenuImages = [];
				$largeMenuImages = [];
			}

			if (empty($m->menuHeader)) {
				$menuHeader = '';
			} else {
				$menuHeader = $m->menuHeader;
			}

			$categories = menuCategoriesData::where('menuCategoriesID', '=', $m->menuCategoriesID)->where('domainID', '=', $domainID)->where('status', '=', 1)->orderBy('sortOrder')->get();
			$menuItems = menuData::where([['menuID', '=', $m->id], ['active', '=', 1]])->orderBy('sortOrder')->get();
			$menu = ['open' => 'closed', 'menu' => $menuItems, 'categories' => $categories, 'categoryType' => $categoryType, 'smallMenuImages' => $smallMenuImages, 'largeMenuImages' => $largeMenuImages, 'menuHeader' => $menuHeader, 'showHeaderClosed' => $m->showHeaderClosed];

			return $menu;
		}

		// open - return menu
		$menuData = menu::find($data['menuID']);
		
		$menuHeader = $menuData->menuHeader;
		if (empty($menuHeader)) {
			$menuHeader = '';
		}

		$menuCategoriesID = menu::find($data['menuID'])->menuCategoriesID;
		$categoryType = menuCategories::where('id', '=', $menuCategoriesID)->value('type');

		if ($categoryType == "picture") {
			$smallMenuImages = menuImages::where([['menuID', '=', $data['menuID']], ['imageSize', '=', 'Small']])->orderBy('menuDataID')->get();
			$largeMenuImages = menuImages::where([['menuID', '=', $data['menuID']], ['imageSize', '=', 'Large']])->orderBy('menuDataID')->get();
		} else {
			$smallMenuImages = [];
			$largeMenuImages = [];
		}

		$categories = menuCategoriesData::where('menuCategoriesID', '=', $menuCategoriesID)->where('status', '=', 1)->orderBy('sortOrder')->get();
		$menuItems = menuData::where([['menuID', '=', $data['menuID']], ['active', '=', 1]])->orderBy('sortOrder')->get();

		$menu = ['open' => 'open', 'menu' => $menuItems, 'categories' => $categories, 'categoryType' => $categoryType, 'smallMenuImages' => $smallMenuImages, 'largeMenuImages' => $largeMenuImages, 'menuHeader' => $menuHeader, 'showHeaderClosed' => $menuData->showHeaderClosed ];


		return $menu;
	} //end menu




	/*---------------- humanDOW/startTime/stopTime ----------------------------------*/

	public function dowOpenClose($dt, $domainID)
	{
		// return menuID based on day of week and time of day
		switch (($dt->dayOfWeek)) {

			case 0:
				// Sunday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startSunday', '<=', $dt->toTimeString())
					->where('stopSunday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			case 1:
				// Monday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startMonday', '<=', $dt->toTimeString())
					->where('stopMonday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			case 2:
				// Tuesday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startTuesday', '<=', $dt->toTimeString())
					->where('stopTuesday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			case 3:
				// Wednesday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startWednesday', '<=', $dt->toTimeString())
					->where('stopWednesday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			case 4:
				// Thursday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startThursday', '<=', $dt->toTimeString())
					->where('stopThursday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			case 5:
				// Friday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startFriday', '<=', $dt->toTimeString())
					->where('stopFriday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			case 6:
				// Saturday
				$ss = menu::select('id')
					->where('domainID', '=', $domainID)
					->where('startSaturday', '<=', $dt->toTimeString())
					->where('stopSaturday', '>=', $dt->toTimeString())
					->first();
				if (empty($ss)) {
					$id = 0;
					$open = "closed";
				} else {
					$id = $ss->id;
					$open = 'open';
				}
				$data = ['menuID' => $id, 'open' => $open];
				return $data;
				break;

			default:
				$data = ['menuID' => 0, 'open' => 'closed'];
				return $data; // not found => closed
		} // end switch

	} // end





	/*---------------- hoursOfOperation ----------------------------------*/


	public function hoursOfOperation($domainID) {
		
		// returns hours of operation based on all active restaurant menus
		// does not return value if closed
		$hoursOfOperation = collect();

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startSunday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('startSunday', '<>', null)->get()->min() ); //start Sunday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopSunday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('stopSunday', '<>', null)->get()->max() ); //stop Sunday

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startMonday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('startMonday', '<>', null)->get()->min() ); //start Monday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopMonday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('stopMonday', '<>', null)->get()->max() ); //stop Monday

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startTuesday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('startTuesday', '<>', null)->get()->min() ); //start Tuesday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopTuesday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('stopTuesday', '<>', null)->get()->max() ); //stop Tuesday

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startWednesday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('startWednesday', '<>', null)->get()->min() ); //start Wednesday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopWednesday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('stopWednesday', '<>', null)->get()->max() ); //stop Wednesday

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startThursday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('startThursday', '<>', null)->get()->min() ); //start Thursday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopThursday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where('stopThursday', '<>', null)->get()->max() ); //stop Thursday

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startFriday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->get()->where('startFriday', '<>', null)->min() ); //start Friday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopFriday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->get()->where('stopFriday', '<>', null)->max() ); //stop Friday

		$hoursOfOperation = $hoursOfOperation->merge( menu::select('startSaturday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->get()->where('startSaturday', '<>', null)->min() ); //start Saturday
		$hoursOfOperation = $hoursOfOperation->merge( menu::select('stopSaturday')->where('domainID', '=', $domainID)->where('active', '=', 'yes')->get()->where('stopSaturday', '<>', null)->max() ); //stop Saturday

		// close check - remove option for today if restaurant is closed
		$hoursOfOperation = $this->closeCheck($hoursOfOperation);


		return $hoursOfOperation;



	} // end hoursOfOperation


	/*---------------- todays hoursOfOperation ----------------------------------*/


	public function todaysHoursOfOperation($domainID) {
		
		// returns today hours of operation based on all active restaurant menus
		// future days to closed are closed by default i.e. no return value
		
		//$todaysHoursOfOperation = collect();

		$dayOfWeek = Carbon::now()->englishDayOfWeek;
		$startDay = 'start' . $dayOfWeek;
		$stopDay = 'stop' . $dayOfWeek;

		$todaysHoursOfOperation = ([ $startDay => null, $stopDay => null]);

		//$todaysHoursOfOperation->merge([$startDay => null]);
		//$todaysHoursOfOperation->merge([$stopDay => null]);
		//$todaysHoursOfOperation = $todaysHoursOfOperation->merge( menu::select($startDay)->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where($startDay, '<>', null)->get()->min() ); //start
		//$todaysHoursOfOperation = $todaysHoursOfOperation->merge( menu::select($stopDay)->where('domainID', '=', $domainID)->where('active', '=', 'yes')->where($stopDay, '<>', null)->get()->max() ); //stop
		
		return $todaysHoursOfOperation;



	} // end hoursOfOperation


	/*---------------- todayHours ----------------------------------*/


	public function todayHours($domainID) {
	
		// returns today hours of operation based on all active restaurant menus
		
		$dayOfWeek = Carbon::now()->englishDayOfWeek;
		$startTime = 'closed';
		$stopTime = 'closed';
		
		$hoursOfOperation = $this->hoursOfOperation($domainID);
		$startDay = 'start' . $dayOfWeek;
		$stopDay = 'stop' . $dayOfWeek;

		$startCheck = $hoursOfOperation->has($startDay);
		$stopCheck = $hoursOfOperation->has($stopDay);

		if ($startCheck && $stopCheck) {
			$startTime = $hoursOfOperation->only($startDay)->toArray();
			if (is_null($startTime)) { $startTime = "00:00:00"; } else { $startTime = $startTime[$startDay]; }
			
			$stopTime = $hoursOfOperation->only($stopDay)->toArray();
			if (is_null($stopTime)) { $stopTime = "00:00:00"; } else { $stopTime = $stopTime[$stopDay]; }
		}

		$todayHours = ([ 'startTime' => $startTime, 'stopTime' => $stopTime]);

		return $todayHours;



	} // end todayHours




	/*---------------- orderItem ----------------------------------*/

	public function closeCheck($hoursOfOperation) {

		$now = Carbon::now();
		$dayOfWeek = Carbon::now()->englishDayOfWeek;
		$stopDay = 'stop' . $dayOfWeek;
		$startDay = 'start' . $dayOfWeek;

		$stopTime = $hoursOfOperation->only([$stopDay])->toArray();
		if (empty($stopTime)) {
			// return hoursOfOperation - restaurant closed
			return $hoursOfOperation;
		}

		// restaurant open today - determine when closed
		$orderStopTime = Carbon::parse( $stopTime[$stopDay] )->subMinutes(env('LAST_ORDER_MINUTES'));
		$closeCheck = $now->greaterThanOrEqualTo($orderStopTime);

		if ($closeCheck) {
			// time now is past last order time
			$hoursOfOperation->pull($startDay);
			$hoursOfOperation->pull($stopDay);
		}

		// ok to return
		return $hoursOfOperation;


	} // end close check






	/*---------------- orderItem ----------------------------------*/


	public function orderItem(Request $request)
	{

		$portionPrice = $this->portionPrice($request->menuItemID); // must be first processed
		$menuOptions = $this->menuOptions($request->menuOptionsID);
		$menuAddOns = $this->menuAddOns($request->menuAddOnsID);
		$menuSides = $this->menuSides($request->menuSidesID);

		$data['item'] = $portionPrice;
		$data['itemHTML'] = $menuSides;
		return $data;


	} //end orderItem


	/*---------------- Portion/Price HTML ----------------------------------*/
	// returns html for order modal price/portion

	public function portionPrice($menuItemID)
	{

		// set checked = checked for single item price
		$pp = menuData::find($menuItemID);
		if (is_null($pp->price2)) {
			$checked = 'checked';
		} else {
			$checked = '';
		}

		$this->html = '<table class="table table-striped menuTable">';

		
		for ($x = 1; $x <= 7; $x++) {
			$price = 'price' . $x;
			$portion = 'portion' . $x;

			if (!empty($pp->$price)) {

				$this->html .= '<tr><td class="col1"><input type="checkbox" class="faChkSqr portion" name="portion" ' . $checked . ' value="' . $pp->$portion . '|' . $pp->$price . '" /><label>';
				$this->html .= $pp->$portion . '</label</td>';
				$this->html .= '<td class="pull-right">$' . $pp->$price . '</td></tr>';
			} // end if !empty

		} // end for

		$this->html .= '</table><hr>';
		
		return $pp->menuItem;

	} //end orderItem


	/*---------------- Menu Options HTML ----------------------------------*/
	// returns html for order modal menu options

	public function menuOptions($menuOptionsID)
	{

		$menuOptions = explode(',', $menuOptionsID);

		if (count($menuOptions) == 1) {

			$heading = menuOptions::find($menuOptions)->first();
			if (empty($heading)) {
				return $this->html;
			}

			$this->html .= '<span class= modalHeading "' . $heading->name . '">' . $heading->name . ': (up to ' . $heading->maxOptions . ')</span>';
			$this->html .= '<table class="table table-striped menuTable">';

			$items = menuOptionsData::where('menuOptionsID', '=', $menuOptions)->get();
			if (empty($items)) {
				return $this->html;
			}
			$count = 1;
			foreach ($items as $item) {

				$this->html .= $this->menuOptionsHTML($item, $heading->name, $heading->maxOptions, $count);
				$count = $count + 1;
			} // end foreach $items

			$this->html .= '</table><hr>';
		} else {

			foreach ($menuOptions as $mo) {

				$heading = menuOptions::find($mo);
				if (empty($heading)) {
					continue;
				}

				$this->html .= '<span class= modalHeading "' . $heading->name . '">' . $heading->name . ': (up to ' . $heading->maxOptions . ')</span>';
				$this->html .= '<table class="table table-striped menuTable">';

				$items = menuOptionsData::where('menuOptionsID', '=', $mo)->get();
				if (empty($items)) {
					continue;
				}
				$count = 1;
				foreach ($items as $item) {

					$this->html .= $this->menuOptionsHTML($item, $heading->name, $heading->maxOptions, $count);
					$count = $count + 1;
				} // end foreach $items

				$this->html .= '</table><hr>';
			} // end for each $menuoptions

		} // end if count == 1

		return $this->html;
	} //end menuOptions


	/*---------------- Menu Options HTML ----------------------------------*/

	public function menuOptionsHTML($item, $className, $maxOptions, $count)
	{

		$className = preg_replace("/\W|_/", "", $className); // remove all non-word characters
		if ($item->optionsDefault == 'yes') {
			$checked = 'checked';
		} else {
			$checked = '';
		}
		$this->html .= '<tr><td class="col1"><input type="checkbox" ' . $checked . ' class="faChkSqr ' . $className . ' ' . $className . $count . '" onclick="clickCheck(\'' . $className . '\',\'' . $maxOptions . '\',\'' . $className . $count . '\')" name="menuOptionsData[]" value="' . $item->id . '" /><label>';
		$this->html .= $item->optionName . '</label</td>';

		if (empty($item->upCharge)) {
			$this->html .= '<td></td></tr>';
		} else {
			$this->html .= '<td class="pull-right">$' . $item->upCharge . '</td></tr>';
		} // end if

	} // end menuOptionsHTML




	/*---------------- Menu AddOns HTML ----------------------------------*/
	// returns html for order modal menu options

	public function menuAddOns($menuAddOnsID)
	{

		$menuAddOns = explode(',', $menuAddOnsID);


		if (count($menuAddOns) == 1) {

			$heading = menuAddOns::find($menuAddOns)->first();
			if (empty($heading)) {
				return $this->html;
			}

			$this->html .= '<span class= modalHeading "' . $heading->name . '">' . $heading->name . ': (up to ' . $heading->maxAddOns . ')</span>';
			$this->html .= '<table class="table table-striped menuTable">';

			$items = menuAddOnsData::where('menuAddOnsID', '=', $menuAddOns)->get();
			if (empty($items)) {
				return $this->html;
			}
			$count = 1;
			foreach ($items as $item) {

				$this->html .= $this->menuAddOnsHTML($item, $heading->name, $heading->maxAddOns, $count);
				$count = $count + 1;
			} // end foreach $items

			$this->html .= '</table><hr>';
		} else {

			foreach ($menuAddOns as $ma) {

				$heading = menuAddOns::find($ma);
				if (empty($heading)) {
					continue;
				}

				$this->html .= '<span class= modalHeading "' . $heading->name . '">' . $heading->name . ': (up to ' . $heading->maxAddOns . ')</span>';
				$this->html .= '<table class="table table-striped menuTable">';

				$items = menuAddOnsData::where('menuAddOnsID', '=', $ma)->get();
				if (empty($items)) {
					continue;
				}
				$count = 1;
				foreach ($items as $item) {

					$this->html .= $this->menuAddOnsHTML($item, $heading->name, $heading->maxAddOns, $count);
					$count = $count + 1;
				} // end foreach $items

				$this->html .= '</table><hr>';
			} // end for each $menuoptions

		} // end if count == 1

		return $this->html;
	} //end menuOptions


	/*---------------- Menu AddOns HTML ----------------------------------*/

	public function menuAddOnsHTML($item, $className, $maxAddOns, $count)
	{

		$className = preg_replace("/\W|_/", "", $className); // remove all non-word characters
		if ($item->optionsDefault == 'yes') {
			$checked = 'checked';
		} else {
			$checked = '';
		}
		$this->html .= '<tr><td class="col1"><input type="checkbox" ' . $checked . ' class="faChkSqr ' . $className . ' ' . $className . $count . '" onclick="clickCheck(\'' . $className . '\',\'' . $maxAddOns . '\',\'' . $className . $count . '\')" name="menuAddOnsData[]" value="' . $item->id . '" /><label>';
		$this->html .= $item->addOnsName . '</label</td>';

		if (empty($item->upCharge)) {
			$this->html .= '<td></td></tr>';
		} else {
			$this->html .= '<td class="pull-right">$' . $item->upCharge . '</td></tr>';
		} // end if

	} // end menuOptionsHTML





	/*---------------- Menu Sides HTML ----------------------------------*/
	// returns html for order modal menu options

	public function menuSides($menuSidesID)
	{

		$menuSides = explode(',', $menuSidesID);

		if (count($menuSides) == 1) {

			$heading = menuSides::find($menuSides)->first();
			if (empty($heading)) {
				return $this->html;
			}

			$this->html .= '<span class= modalHeading "' . $heading->name . '">' . $heading->name . ': (select ' . $heading->numberSides . ')</span>';
			$this->html .= '<table class="table table-striped menuTable">';

			$items = menuSidesData::where('menuSidesID', '=', $menuSides)->get();
			if (empty($items)) {
				return $this->html;
			}
			$count = 1;
			foreach ($items as $item) {

				$this->html .= $this->menuSidesHTML($item, $heading->name, $heading->numberSides, $count);
				$count = $count + 1;
			} // end foreach $items

			$this->html .= '</table><hr>';
		} else {

			foreach ($menuSides as $ms) {

				$heading = menuSides::find($ms);
				if (empty($heading)) {
					continue;
				}

				$this->html .= '<span class= modalHeading "' . $heading->name . '">' . $heading->name . ': (select ' . $heading->numberSides . ')</span>';
				$this->html .= '<table class="table table-striped menuTable">';

				$items = menuSidesData::where('menuSides', '=', $ms)->get();
				if (empty($items)) {
					continue;
				}
				$count = 1;
				foreach ($items as $item) {

					$this->html .= $this->menuSidesHTML($item, $heading->name, $heading->numberSides, $count);
					$count = $count + 1;
				} // end foreach $items

				$this->html .= '</table><hr>';
			} // end for each $menuoptions

		} // end if count == 1

		return $this->html;
	} //end menuSides


	/*---------------- Menu Sides HTML ----------------------------------*/

	public function menuSidesHTML($item, $className, $numberSides, $count)
	{

		$className = preg_replace("/\W|_/", "", $className); // remove all non-word characters
		if ($item->sidesDefault == 'yes') {
			$checked = 'checked';
		} else {
			$checked = '';
		}
		$this->html .= '<tr><td class="col1"><input type="checkbox" ' . $checked . ' class="faChkSqr ' . $className . ' ' . $className . $count . '" onclick="clickCheck(\'' . $className . '\',\'' . $numberSides . '\',\'' . $className . $count . '\')" name="menuSidesData[]" value="' . $item->id . '" /><label>';
		$this->html .= $item->sideName . '</label</td>';

		if (empty($item->upCharge)) {
			$this->html .= '<td></td></tr>';
		} else {
			$this->html .= '<td class="pull-right">$' . $item->upCharge . '</td></tr>';
		} // end if

	} // end menuSidesHTML




} // end ordersController