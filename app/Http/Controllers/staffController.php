<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class staffController extends Controller
{
    
    /*--------------------------------- Staff Dashboard -----------------------------------------------*/
  	
  	public function dashboard(Request $request) {
        
        $domain = \Request::get('domain');
        $breadcrumb = ['dashboard', ''];

        $totalContacts = 0;
        $monthlyContacts = 0;
        $textsSentCount = 0;
        //$userID = Auth::user();

        return view('staff.dashboard')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'totalContacts' => $totalContacts, 'monthlyContacts' => $monthlyContacts, 'textsSentCount' => $textsSentCount]);

    } // end dashboard
    

} // end staff controller
