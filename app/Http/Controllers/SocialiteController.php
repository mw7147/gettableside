<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use App\usersData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SocialiteController extends Controller
{
    /**
     * Redirect to specific social login
     */

    public function socialLogin($driver){
        session()->put('mobile',request()->mobile);
        return Socialite::driver($driver)->redirect();
    }

    public function userLogin($driver){
        $sid=session()->getId();
        $domain = \Request::get('domain');
        $user = Socialite::driver($driver)->user();
        if(isset($user) && isset($user->email)){
            $newUser = User::firstOrCreate([
                'email' => $user->email,
                'parentDomainID' => $domain->parentDomainID
                ], [
                'domainID' => $domain->id,
                'role_auth_level' => '10',
                'password' => Hash::make($user->token),
                'active' => 'yes',
                'fname' => $user->name ? $user->name : $user->nickname,
                'type'  => 'social',
                'source' => $driver
            ]);   
            Auth::login($newUser, true);
            session()->setId($sid);
            $mobile = session()->get('mobile');
            return redirect("/viewcart?iq=$mobile");
        }else{
            return redirect('order')->with('social_error', "Please check your $driver account privacy. Social Login not allowed");
        }
    }
}
