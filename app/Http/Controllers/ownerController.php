<?php

namespace App\Http\Controllers;

use Facades\App\Http\Controllers\ordersController;
use Illuminate\Http\Request;
use App\suspendOperation;
use Carbon\carbon;
use App\orders;

class ownerController extends Controller {

  	/*--------------------------------- Owner Dashboard -----------------------------------------------*/
  	
  	public function dashboard(Request $request) {

      $domain = \Request::get('domain');
      $breadcrumb = ['dashboard', ''];


      $totalOrders = orders::select('id')->where('domainID', '=', $domain->id)->whereDate('created_at', Carbon::now()->toDateString())->count();
      $revToday = orders::select('total')->where('domainID', '=', $domain->id)->whereDate('created_at', Carbon::now()->toDateString())->sum('total');
      $refundsToday = orders::select('orderRefundAmount')->where('domainID', '=', $domain->id)->whereDate('created_at', Carbon::now()->toDateString())->sum('orderRefundAmount');
      $status = $this->suspendStatus($domain->id);

      return view('owner.dashboard')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'totalOrders' => $totalOrders, 'revToday' => $revToday, 'refundsToday' => $refundsToday, 'status' => $status]);

    } // end dashboard


    /*--------------------------------- Suspend Status -----------------------------------------------*/
      
    public function suspendStatus($domainID) {

        $open = ordersController::dowOpenClose(Carbon::now(), $domainID);

        if ( $open['open'] == 'open' ) {
            $suspend = ordersController::checkSuspend($domainID);
            if ($suspend['suspended']) {
                // suspended
                return 'Suspended until ' . $suspend['resumeTime'];
            } else {
                // open
                return 'Open';  
            }
        } else {
            // closed
            return 'Closed';
        } 

    } // end suspendStatus



}
