<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\messageHelpers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\textsSentDetails;
use App\textMIDQueue;
use App\groupMember;
use App\textsSent;
use App\groupName;
use Carbon\Carbon;
use App\textQueue;
use App\contacts;




class textController extends Controller
{


	/*---------------------------------QuickText-----------------------------------------------------*/   

	public function quickText($urlPrefix, Request $request) {

	    $user = Auth::user();
	    $breadcrumb = ['text', 'quick'];
      $domain = \Request::get('domain');
      
      $groups = messageHelpers::groupNames($domain->id);
      $totalContacts = contacts::where("domainID", '=', $domain->id)->count();
	      
	    return view('text.quickText')->with(['domain' => $domain, 'groups' => $groups, 'breadcrumb' => $breadcrumb, 'totalContacts' => $totalContacts, 'cid' => $user->id]);
    
    } // end quickText 


	/*---------------------------------QuickText Confirm-----------------------------------------------------*/   

	public function quickTextConfirm($urlPrefix, Request $request) {

	// dd($request);

    if (!isset($request->group)) {abort(404);} //abort if no group found
	$domain = \Request::get('domain');
	$user = Auth::user();
    $groupID = $request->group;
    $groupID = $groupID[0];
    // Queue messages for sending on mail server
    if($groupID == 'all') {$groupID = 0;}
    $goodCount = 0; // message queued
    $badCount = 0;  // no mobile number
    $carbon = new Carbon();
    $dt = Carbon::now();


    if (!is_null($request->file('fileName'))) {
    
      // add new picture
      $pictureName = $request->file('fileName')->getClientOriginalName();
      $pictureSize = round($request->file('fileName')->getSize()/1024,1); // file size in Kilobytes
      $ext = $request->file('fileName')->guessClientExtension();
      $imageTypes= array("jpeg" => "jpeg", "jpg" => "jpg", "png" => "png", "pdf" => "pdf");
      $chk = array_search($ext, $imageTypes);

      if ($chk == FALSE || $pictureSize > 500) {
          $request->session()->flash('fileError', 'The file ' . $pictureName . '. was not a PDF, JPG or PNG file or the file size exceeded 500Kb. Please check your picture file and try again.');
          $chk = FALSE;
          $path = '';
          // go back to QuickText to process
          $request->session()->flash('groupID', $groupID);
          return redirect()->back()->withInput();
      } else {
        //save new file
        $pixFile = $request->file('fileName')->storeAs('public/customerImages/' . $domain->id, $pictureName);
        $request->pixFile = $pixFile;
        $request->picture = $pictureName;
      } 

    } else {
      // if no fileName - set pixFile to empty string
      if ($request->fileName == '') {$pixFile = '';} else {$pixFile = $request->pixFile;}
    } // end attachment

    if ($pixFile == '') {$type = 'sms';} else { $type = 'mms';}

    // send text text if text
    if ($request->testText == 'yes') {

    	$subject = '';
    	$numSent = $this->sendTextMessage($domain->id, $user->unformattedMobile, $subject, $request->message, $pixFile, $type);
      	$request->session()->flash('QuickTextTest', 'A test text message was sent to your registered mobile phone number.');
      	$request->session()->flash('groupID', $groupID);
      	$request->session()->flash('pixFile', $pixFile);

      	// go back to QuickText to process
      	$_POST = [];
      	return redirect()->back()->withInput();

    } // end if test

  
    if ($groupID === 0) {
      // get all contacts
      $contactList = contacts::select('contacts.id', 'contacts.fname', 'contacts.lname', 'contacts.mobile', 'contacts.unformattedMobile', 'registeredNumbers.carrierID', 'registeredNumbers.carrierName', 'registeredNumbers.smsGateway', 'registeredNumbers.mmsGateway')
      ->where('domainID', '=', $domain->id)
      ->leftJoin('registeredNumbers', 'contacts.unformattedMobile', '=', 'registeredNumbers.cellNumber')
      ->get();

    } else {

      // get group member contacts
      $contactList = groupMember::select('contacts.id', 'contacts.fname', 'contacts.lname', 'contacts.mobile', 'registeredNumbers.carrierID', 'registeredNumbers.carrierName', 'registeredNumbers.smsGateway', 'registeredNumbers.mmsGateway')
      ->where('groupMembers.groupID', '=', $groupID)
      ->leftJoin('contacts', 'groupMembers.contactID', '=', 'contacts.id')
      ->leftJoin('registeredNumbers', 'contacts.unformattedMobile', '=', 'registeredNumbers.cellNumber')     
      ->get();
    
    } // end if group - get group members

    // strip public/ for sending
    $pixFile = str_replace("public/", "", $pixFile);

    // insert record into textsSent
    $textSent = new textsSent();
    $textSent->domainID = $request->domainID;
    $textSent->userID = $request->userID;
    $textSent->parentDomainID = $user->parentDomainID;
    $textSent->groupID = $groupID;
    $textSent->templateID = $request->templateID;
    $textSent->messageType = $type;
    $textSent->messageBody = $request->message;
    $textSent->pixFile = $pixFile;
    $textSent->sendIP = $_SERVER['REMOTE_ADDR'];
    $textSent->totalQueue = 0;
    $textSent->totalBad = 0;
    $textSent->save();

    foreach ($contactList as $list) {
      
      if ($list->mobile != '') {

        if ($type == "sms") {$gateway = $list->smsGateway;} else {$gateway = $list->mmsGateway;}

        DB::connection('maildb')->table('textQueue')->insert([

          [
            'domainID' => $domain->id,
            'parentDomainID' => $domain->parentDomainID,
            'userID' => $user->id,
            'groupID' => $groupID,
            'templateID' => $request->templateID,
            'contactID' => $list->id,
            'messageID' => $textSent->id,
            'fname' => $list->fname,
            'lname' => $list->lname,
            'sendType' => 'text',
            'messageType' => $type,
            'message' => $request->message,
            'pixFile' => $pixFile,
            'mobile' => $list->mobile,
            'unformattedMobile' => $list->unformattedMobile,
            'carrierID' => $list->carrierID,
            'carrierName' => $list->carrierName,
            'gateway' => $gateway,
            'created_at' => $dt

          ]
        ]);
        // good message - message queued
        $goodCount = $goodCount +1;

      } else {
        // bad message - did not queue
        $badCount = $badCount +1;

      } // end if mobile != ''

    } // end foreach $contactList

    // insert record into textsSent

    $textSent->totalQueue = $goodCount;
    $textSent->totalBad = $badCount;
    
    $textSent->save();

    $midQueue = DB::connection('maildb')->table('textMIDQueue')->insert([
      [
        'mid' => $textSent->id,
        'domainID' => $domain->id,
        'created_at' => $dt
      ]
    ]);

    $breadcrumb = ['text', 'quick'];
      
    return view('text.quickTextConfirm')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'goodCount' => $goodCount, 'badCount' => $badCount, 'messageID' => $textSent->id]);
	      
    
    } // end quickTextConfirm 



	/*---------------------------------Send A Text Message--------------------------------------------*/   

	public function sendTextMessage($domainID, $unformattedMobile, $subject, $textMessage, $pictureFile, $type) {

      $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
      $to = registeredNumbers::find($unformattedMobile);

      // create the text
      $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
      
      // Create the Transport using HWCTMailer
      $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);

      // Create the Mailer using created Transport
      $mailer = HWCTMailer::createMailer($transport);

      // Create a message, $subject, $from, $message
      $message = HWCTMailer::createMessage($subject, $sender , $textMessage, $domainSiteConfig->sendEmail);

      if (!is_null($pictureFile)) {
        HWCTMailer::addAttachment($message, $pictureFile);
      }

      $numSent = HWCTMailer::sendMessage($mailer, $message, $to, $type );

      return $numSent;

    } // end sendTextMessage



                         
} // end text controller