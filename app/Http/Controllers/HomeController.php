<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\domainSiteConfig;
use App\siteStyles;
use App\siteData;
use App\menu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
 
    }

    /*----------------home page ----------------------------------*/

    public function home(Request $request) {

        $domain = \Request::get('domain');
        
        if (is_null($domain)) { abort(403); }
        if ( $domain->id == 1) { return view('auth.login')->with(['domain' => $domain]); }

        // dine in
        if ($domain->type == 9) { return redirect('dine'); }

        //redirect to login if no menu
        $menu = menu::where('domainID', '=', $domain['id'])->first();
  
        if (is_null($menu)) { return redirect('login'); }
        $styles = siteStyles::find($domain['id']);
        
        // redirect to order page if not website customer
        $data = siteData::find($domain['id']);
        if (is_null($data)) { return redirect('order'); }
        
        // got to website if website customer
        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
        $pageName = "home";
        return view('site.home')->with(['domain' => $domain, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'domainSiteConfig' => $domainSiteConfig ]);
    } 

    /*---------------- services page ----------------------------------*/

    public function services(Request $request) {
        $domain = \Request::get('domain');
        $styles = siteStyles::find($domain['id']);  
        $data = siteData::find($domain['id']);
        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
        $pageName = "services";
        return view('site.services')->with(['domain' => $domain, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'domainSiteConfig' => $domainSiteConfig ]);
    } 


    /*---------------- about page ----------------------------------*/

    public function about(Request $request) {
        $domain = \Request::get('domain');
        $styles = siteStyles::find($domain['id']);
        $data = siteData::find($domain['id']);
        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
        $pageName = "about";
        return view('site.about')->with(['domain' => $domain, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'domainSiteConfig' => $domainSiteConfig ]);
    } 

    /*---------------- contact page ----------------------------------*/

    public function contact(Request $request) {
        $domain = \Request::get('domain');
        $styles = siteStyles::find($domain['id']);
        $data = siteData::find($domain['id']);
        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain['id'])->first();
        $pageName = "contact";
        return view('site.contact')->with(['domain' => $domain, 'pageName' => $pageName, 'styles' => $styles, 'data' => $data, 'domainSiteConfig' => $domainSiteConfig ]);
    } 


} // end HomeController
