<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\messageHelpers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\textsSentDetails;
use App\textTemplates;
use App\textMIDQueue;
use App\groupMember;
use App\textsSent;
use App\groupName;
use Carbon\Carbon;
use App\textQueue;
use App\contacts;


class textTemplateController extends Controller
{
    

 /*-------------------------- Template View-----------------------------------------------------------------*/   

 public function templateView($urlPrefix) {

    $user = Auth::user();
    $breadcrumb = ['text', 'view'];
    $domain = \Request::get('domain');
    $texts = textTemplates::where('userID', '=', $user->id)->get();

    return view('text.textTemplates')->with(['domain' => $domain, 'texts' => $texts, 'breadcrumb' => $breadcrumb]);
    
  }     // end templateView


 /*--------------------------------New Text Template -----------------------------------------------------------*/   

 public function templateNew(Request $request) {

    $breadcrumb = ['text', 'new'];
    $domain = \Request::get('domain');

    return view('text.newTextTemplate')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
    
  } // end templateNew

  /*--------------------------------New Text Template Save -----------------------------------------------------------*/   

 public function templateNewSave($urlPrefix, Request $request) {

    $domain = \Request::get('domain');
    $image = $this->textImage($request->file('fileName'), $domain->id);

    $template = new textTemplates();
        $template->domainID = $domain->id;
        $template->userID = Auth::id();
        $template->status = $request->active;
        $template->name = $request->name;
        $template->description = $request->description;
        $template->message = $request->message;
        $template->picturePublic = $image['picturePublic'];
        $template->picturePrivate = $image['picturePrivate'];
    $template->save();

    $request->session()->flash('templateSuccess', 'Your text template was successfully created.');

    return redirect()->route('textTemplateView', ['urlPrefix' => $urlPrefix]);
    
  }     // end templateNewSave


 /*-------------------------------------- Text Template Edit-----------------------------------------------------*/   

 public function textTemplateEdit($urlPrefix, $id) {

    $domain = \Request::get('domain');
    $breadcrumb = ['text', 'view']; //breadcrumb is same as view - same menu item

    $template = textTemplates::find($id);
    if ($template == '') {abort(404);} // abort if no record found
   
    return view('text.editTextTemplate')->with(['domain' => $domain, 'template' => $template, 'id' => $id, 'breadcrumb' => $breadcrumb]);
  
  } // end textTemplateEdit


/*-------------------------------------- Text Template Edit Save-----------------------------------------------------*/   

public function textTemplateEditSave($urlPrefix, $id, Request $request) {
    //dd($request);
    $domain = \Request::get('domain');
    $template = textTemplates::find($id);
        if ($template == '') {abort(404);} // abort if no record found

        // update image
        if ($request->imgCheck != $template->picturePublic || is_null($request->imgCheck) ) { 
            $image = $this->textImage($request->file('fileName'), $domain->id);
            $imgCheck = 'new';
        } else {
            $imgCheck = 'existing';
        }

        
        $template->domainID = $domain->id;
        $template->userID = Auth::id();
        $template->status = $request->active;
        $template->name = $request->name;
        $template->description = $request->description;
        $template->message = $request->message;
        if ($imgCheck != 'existing') {
            $template->picturePublic = $image['picturePublic'];
            $template->picturePrivate = $image['picturePrivate'];
        }
    $template->save();

    $request->session()->flash('templateSuccess', 'Your text template was successfully updated.');
   
    return redirect()->route('textTemplateView', ['urlPrefix' => $urlPrefix]);
  
  } // end textTemplateEditSave




/*-------------------------------------- Text Template Copy -----------------------------------------------------*/   

public function textTemplateCopy($urlPrefix, $id, Request $request) {

    $domain = \Request::get('domain');

    $oldTemplate = textTemplates::find($id);
    if ($oldTemplate == '' || $oldTemplate->domainID != $domain->id) {abort(404);} // abort if no record found or wrong domain
   
    $request->session()->flash('templateSuccess', 'Your text template was successfully copied.');
    $template = new textTemplates();
        $template->domainID = $domain->id;
        $template->userID = Auth::id();
        $template->status = $oldTemplate->status;
        $template->name = $oldTemplate->name . ' - copy';
        $template->description = $oldTemplate->description;
        $template->message = $oldTemplate->message;
        $template->picturePublic = $oldTemplate->picturePublic;
        $template->picturePrivate = $oldTemplate->picturePrivate;
    $template->save();
   
    return redirect()->route('textTemplateView', ['urlPrefix' => $urlPrefix]);  
  } // end textTemplateCopy



  /*-------------------------------------- Text Template Delete -----------------------------------------------------*/   

public function textTemplateDelete($urlPrefix, $id, Request $request) {

    $domain = \Request::get('domain');

    $template = textTemplates::find($id);
    if ($template == '' || $template->domainID != $domain->id) {abort(404);} // abort if no record found or wrong domain
    $template->delete();
    
    $request->session()->flash('templateSuccess', 'Your text template was successfully deleted.');
   
    return redirect()->route('textTemplateView', ['urlPrefix' => $urlPrefix]);  
  } // end textTemplateDelete




 /*-------------------------------- Text Template Send - View ----------------------------------------------------------*/   

 public function textTemplateSend($urlPrefix) {
    
    $userID = Auth::id();
    $breadcrumb = ['text', 'send'];
    $domain = \Request::get('domain');
    $templates = textTemplates::select('id', 'name', 'description')->where('userID', '=', $userID)->where('status', '=', 'yes')->orderBy('id', 'desc')->get();

    return view('text.sendTextView')->with(['domain' => $domain, 'templates' => $templates, 'breadcrumb' => $breadcrumb]);
    
    }    // end textTemplateSend





/*------------------------------ Text Template Send - Do-------------------------------------------------------------*/   

public function textTemplateSendDo($urlPrefix, $id) {

    $breadcrumb = ['text', 'send'];
    $domain = \Request::get('domain');
    
    $template = textTemplates::find($id);
    if ($template == '') {abort(404);} // abort if no record found

    $userID = Auth::id();

    $groups = groupName::leftJoin('groupMembers', 'groupNames.id', '=', 'groupMembers.groupID')
    ->select('groupNames.id', 'groupNames.groupName', DB::raw('count(groupMembers.id) as groupCount'))
    ->where('groupNames.domainID', '=', $domain->id)
    ->groupBy('groupNames.id', 'groupNames.groupName')
    ->get();

    $totalContacts = contacts::where("domainID", '=', $domain->id)->count();
  
      
    return view('text.sendTextDo')->with(['domain' => $domain, 'groups' => $groups, 'breadcrumb' => $breadcrumb, 'template' => $template, 'totalContacts' => $totalContacts]);
    
    } // end textTemplateSendDo





/*------------------------------ Text Template Send - Queue -------------------------------------------------------------*/   

public function textTemplateQueue($urlPrefix, Request $request) {


    if (!isset($request->group)) {abort(404);} //abort if no group found
    $breadcrumb = ['text', 'send'];
	$domain = \Request::get('domain');
	$user = Auth::user();
    $groupID = $request->group;
    $groupID = $groupID[0];

    // get text template
    $template = textTemplates::find($request->templateID);
    if ($template == '' || $template->domainID != $domain->id) {abort(404);} // abort if no record found or wrong domain
   
    if (is_null($template->picturePrivate)) {$type = 'sms';} else { $type = 'mms';}

    // send text text if text
    if ($request->testText == 'yes') {

    	$subject = '';
    	$numSent = $this->sendTextMessage($domain->id, $user->unformattedMobile, $subject, $template->message, $template->picturePrivate, $type);
      	$request->session()->flash('textTest', 'A test text message was sent to your registered mobile phone number.');

      	// go back to textTemplate to process
      	$_POST = [];
      	return redirect()->back()->withInput();

    } // end if test


        
    // Queue messages for sending on mail server
    if($groupID == 'all') {$groupID = 0;}
    $goodCount = 0; // message queued
    $badCount = 0;  // no mobile number
    $carbon = new Carbon();
    $dt = Carbon::now();
  
    if ($groupID === 0) {
      // get all contacts
      $contactList = contacts::select('contacts.id', 'contacts.fname', 'contacts.lname', 'contacts.mobile', 'contacts.unformattedMobile', 'registeredNumbers.carrierID', 'registeredNumbers.carrierName', 'registeredNumbers.smsGateway', 'registeredNumbers.mmsGateway')
      ->where('domainID', '=', $domain->id)
      ->leftJoin('registeredNumbers', 'contacts.unformattedMobile', '=', 'registeredNumbers.cellNumber')
      ->get();

    } else {

      // get group member contacts
      $contactList = groupMember::select('contacts.id', 'contacts.fname', 'contacts.lname', 'contacts.mobile', 'registeredNumbers.carrierID', 'registeredNumbers.carrierName', 'registeredNumbers.smsGateway', 'registeredNumbers.mmsGateway')
      ->where('groupMembers.groupID', '=', $groupID)
      ->leftJoin('contacts', 'groupMembers.contactID', '=', 'contacts.id')
      ->leftJoin('registeredNumbers', 'contacts.unformattedMobile', '=', 'registeredNumbers.cellNumber')     
      ->get();
    
    } // end if group - get group members

    // strip public/ for sending
    $pixFile = str_replace("public/", "", $template->picturePrivate);

    // insert record into textsSent
    $textSent = new textsSent();
    $textSent->domainID = $domain->id;
    $textSent->userID = $user->id;
    $textSent->parentDomainID = $user->parentDomainID;
    $textSent->groupID = $groupID;
    $textSent->templateID = $request->templateID;
    $textSent->messageType = $type;
    $textSent->messageBody = $template->message;
    $textSent->pixFile = $pixFile;
    $textSent->sendIP = $_SERVER['REMOTE_ADDR'];
    $textSent->totalQueue = 0;
    $textSent->totalBad = 0;
    $textSent->save();

    foreach ($contactList as $list) {
      
      if ($list->mobile != '') {

        if ($type == "sms") {$gateway = $list->smsGateway;} else {$gateway = $list->mmsGateway;}

        DB::connection('maildb')->table('textQueue')->insert([

          [
            'domainID' => $domain->id,
            'parentDomainID' => $domain->parentDomainID,
            'userID' => $user->id,
            'groupID' => $groupID,
            'templateID' => $request->templateID,
            'contactID' => $list->id,
            'messageID' => $textSent->id,
            'fname' => $list->fname,
            'lname' => $list->lname,
            'sendType' => 'text',
            'messageType' => $type,
            'message' => $template->message,
            'pixFile' => $pixFile,
            'mobile' => $list->mobile,
            'unformattedMobile' => $list->unformattedMobile,
            'carrierID' => $list->carrierID,
            'carrierName' => $list->carrierName,
            'gateway' => $gateway,
            'created_at' => $dt

          ]
        ]);
        // good message - message queued
        $goodCount = $goodCount +1;

      } else {
        // bad message - did not queue
        $badCount = $badCount +1;

      } // end if mobile != ''

    } // end foreach $contactList

    // insert record into textsSent

    $textSent->totalQueue = $goodCount;
    $textSent->totalBad = $badCount;
    
    $textSent->save();

    $midQueue = DB::connection('maildb')->table('textMIDQueue')->insert([
      [
        'mid' => $textSent->id,
        'domainID' => $domain->id,
        'created_at' => $dt
      ]
    ]);

    return view('text.sendTextQueue')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'goodCount' => $goodCount, 'badCount' => $badCount, 'messageID' => $textSent->id]);
    
    }     // end textTemplateQueue



 /*-------------------------------- Text Image -----------------------------------------------------------*/   

 public function textImage($img, $domainID) {
    
    // No Image
    if (is_null($img)) {
        $image['pictureName'] = null;
        $image['pictureSize'] = null;
        $image['extension'] = null;
        $image['picturePublic'] = null;
        $image['picturePrivate'] = null;

        return $image;
    }

    // Add Text Image
    $pictureName = $img->getClientOriginalName();
    $pictureSize = round($img->getSize()/1024,1); // file size in Kilobytes
    $ext = $img->guessClientExtension();
    $imageTypes= array("jpeg" => "jpeg", "jpg" => "jpg", "png" => "png", "pdf" => "pdf");
    $chk = array_search($ext, $imageTypes);

    if ($chk == FALSE || $pictureSize > 250) {
        // just in case client does not catch large file or filetype
        $request->session()->flash('templateError', 'The file ' . $pictureName . '. was not a PDF, JPG or PNG file or the file size exceeded 250Kb. Please check your picture file and try again.');
        $chk = FALSE;
        $path = '';
        // go back to QuickText to process
        $request->session()->flash('groupID', $groupID);
        return redirect()->back()->withInput();
    } else {
        //save new file
        $pixFile = $img->storeAs('public/customerImages/' . $domainID, $pictureName);
        $picturePrivate = $pixFile;
        // create public storage path
        $picturePublic = '/storage/customerImages/' . $domainID . '/' . $pictureName;
    } 

    $image['pictureName'] = $pictureName;
    $image['pictureSize'] = $pictureSize;
    $image['extension'] = $ext;
    $image['picturePublic'] = $picturePublic;
    $image['picturePrivate'] = $picturePrivate;

    return $image;
    
  } // end textImage




  /*---------------------------------Send A Text Message--------------------------------------------*/   

	public function sendTextMessage($domainID, $unformattedMobile, $subject, $textMessage, $pictureFile, $type) {

        $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
        $to = registeredNumbers::find($unformattedMobile);
  
        // create the text
        $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
        
        // Create the Transport using HWCTMailer
        $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);
  
        // Create the Mailer using created Transport
        $mailer = HWCTMailer::createMailer($transport);
  
        // Create a message, $subject, $from, $message
        $message = HWCTMailer::createMessage($subject, $sender , $textMessage, $domainSiteConfig->sendEmail);
  
        if (!is_null($pictureFile)) {
          HWCTMailer::addAttachment($message, $pictureFile);
        }
  
        $numSent = HWCTMailer::sendMessage($mailer, $message, $to, $type );
  
        return $numSent;
  
      } // end sendTextMessage
  

} // end textTemplateController
