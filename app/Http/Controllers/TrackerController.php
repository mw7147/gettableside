<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Events\CampaignReadEvent;
use Illuminate\Http\Request;
use App\emailsSentDetail;
use App\CampaignTracker;
use App\Http\Requests;
use App\emailTracker;
use App\emailsSent;
use App\User;


class TrackerController extends Controller
{
     /*-------------------------------------------------------------------------------------------*/   

  	public function emailTracker(Request $request, $emailsSentID, $emailsSentDetailID) {
  		// track email reads
		$domain = \Request::get('domain');
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
	
		// only track first read
		$check = emailTracker::where([ ['emailsSentID', '=', $emailsSentID], ['emailsSentDetailID', '=', $emailsSentDetailID] ])->first();
		if (is_null($check)) {
			// save the read info
			$track = new emailTracker();
			$track->ipAddress = $ipAddress;
			$track->userAgent = $userAgent;
			$track->emailsSentID = $emailsSentID;
			$track->emailsSentDetailID = $emailsSentDetailID;
			$track->save();

			// increment total read counter
			$read = emailsSent::find($emailsSentID);
			if (!is_null($read)) {
				$read->totalReads = $read->totalReads +1;
				$read->save();
			} // end if $read
		} // end if $check

		return redirect('/img/hwctImage.png');
	    
	}     


  	public function campaignMailTracker(Request $request, $campaignEventDetailsID) {
  		// track email reads
		$domain = \Request::get('domain');
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
	
		$track = new campaignTracker();
		$track->ipAddress = $ipAddress;
		$track->userAgent = $userAgent;
		$track->campaignEventDetailsID = $campaignEventDetailsID;
		$track->save();

        $eventData = DB::table('campaignEventDetails')->select('campaignEventID', 'contactID')->where('id', '=', $campaignEventDetailsID)->first();

		// pass campaignEventSummaryID, contactID, $campaignEventDetailsID
		$readEvent = event(new CampaignReadEvent( $eventData->campaignEventID, $eventData->contactID, $campaignEventDetailsID ));


		return redirect('/img/hwctImage.png');
	    
	}     


} // end TrackerController