<?php

namespace App\Http\Controllers;

use Facades\App\Http\Controllers\ownerController;
use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\CellNumberData;
use App\suspendOperationHistory;
use App\domainPaymentConfig;
use Illuminate\Http\Request;
use App\registeredNumbers;
use App\domainSiteConfig;
use App\suspendOperation;
use App\managerPIN;
use App\signatures;
use Carbon\Carbon;
use App\usStates;
use App\ccType;
use App\domain;
use App\User;


class settingsController extends Controller
{
    
    
    /*------------------------- Settings Dashboard ------------------------------------------------------------------*/   
    
    public function dashboard(Request $request, $urlPrefix) {
    
        $breadcrumb = ['settings', ''];
        $domain = \Request::get('domain');

        if ( $urlPrefix == 'manager') {
            $status = ownerController::suspendStatus($domain->id);
            return view('settings.managerSettings')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'status' => $status]);
        } elseif ($urlPrefix == 'owner') {
            $status = ownerController::suspendStatus($domain->id);
            return view('settings.ownerSettings')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'status' => $status]);
        } elseif ($urlPrefix == 'hwct') {
            return view('settings.hwctSettings')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
        } elseif ($urlPrefix == 'admin') {
                return view('settings.adminSettings')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
        } else {
            abort(404);
        }
      
    } // end dashboard



    /*-------------------- Location Settings  ----------------------------------------*/

    public function location($urlPrefix) {
        
            // $domainID = domain ID of customer (domain) style to edit
            $breadcrumb = ['settings', ''];
            $domain = \Request::get('domain');
            $siteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
            $states = usStates::all();
        
            return view('settings.locationSettingsEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'siteConfig' => $siteConfig, 'states' => $states ]);
            
    } // end location

    /*-------------------- Domain (Customer) Site Config Save (Do) ----------------------------------------*/

    public function locationDo(Request $request, $urlPrefix) {

        $domain = \Request::get('domain');

        if ($request->taxDelivery) {
            // tax delivery
            $deliveryTaxRate = $request->deliveryTaxRate;
        } else {
            // don't tax delivery - set tax = 0
            $deliveryTaxRate = 0;
        }

        $siteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
            $siteConfig->managerFname = $request->managerFname;
            $siteConfig->managerLname = $request->managerLname;
            $siteConfig->locationName = $request->locationName;
            $siteConfig->address1 = $request->address1;
            $siteConfig->address2 = $request->address2;
            $siteConfig->city = $request->city;
            $siteConfig->state = $request->state;
            $siteConfig->zipCode = $request->zipCode;
            $siteConfig->taxRate = $request->taxRate;
            $siteConfig->globalDiscount = $request->globalDiscount;
            // $siteConfig->locationIPAddress = $request->locationIPAddress;
            // $siteConfig->locationOrder = $request->locationOrder;
            $siteConfig->cartClearTimer = $request->cartClearTimer;
            $siteConfig->dineInLastOrderTimeOffset = $request->dineInLastOrderTimeOffset;
            $siteConfig->locationPhone = $request->locationPhone;                
            $siteConfig->delivery = $request->delivery;
            $siteConfig->deliveryCharge = $request->deliveryCharge;
            $siteConfig->deliveryRadius = $request->deliveryRadius;
            $siteConfig->orderAhead = $request->orderAhead;
            $siteConfig->deliveryPrepMinutes = $request->deliveryPrepMinutes;            
            $siteConfig->pickupPrepMinutes = $request->pickupPrepMinutes;
            $siteConfig->deliveryRecommendedTipPercent = $request->deliveryRecommendedTipPercent;
            $siteConfig->deliveryRequireRecommendedTip = $request->deliveryRequireRecommendedTip;
            $siteConfig->pickupRecommendedTipPercent = $request->pickupRecommendedTipPercent;
            $siteConfig->pickupRequireRecommendedTip = $request->pickupRequireRecommendedTip;    
            $siteConfig->deliveryMinOrder = $request->deliveryMinOrder;
            $siteConfig->latitude = $request->latitude;
            $siteConfig->longitude = $request->longitude;
            $siteConfig->taxDelivery = $request->taxDelivery;
            $siteConfig->deliveryTaxRate = $deliveryTaxRate;
            $siteConfig->foodRunnerToken = $request->foodRunnerToken;
            $siteConfig->googleMapKey = $request->googleMapKey;
            $siteConfig->mapLink = $request->mapLink;
        $sc = $siteConfig->save();

        $d = domain::find($domain->id);
            $d->name = $request->locationName;
        $d->save();

        if ($sc) {
            $request->session()->flash('updateSuccess', 'Update Successful.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating the site configuration information. Please try again.');
        }

        return redirect()->action('settingsController@location', ['urlPrefix' => $urlPrefix]);
                
    } // end locationDo


    /*-------------------- System Settings  ----------------------------------------*/

    public function system($urlPrefix) {
    
        $breadcrumb = ['settings', ''];
        $domain = \Request::get('domain');
        $siteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
        $states = usStates::all();
    
    
        return view('settings.systemSettingsEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'siteConfig' => $siteConfig, 'states' => $states, 'urlPrefix' => $urlPrefix ]);
        
    }  // end system




    /*-------------------- System Settings Save (Do) ----------------------------------------*/

    public function systemDo(Request $request, $urlPrefix) {
        
        $domain = \Request::get('domain');
        $siteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->firstOrFail();
            $siteConfig->orderMessage = $request->orderMessage;
            $siteConfig->deliveryMessage = $request->deliveryMessage;
            $siteConfig->futureOrderMessage = $request->futureOrderMessage ?? null;
            $siteConfig->futureDeliveryMessage = $request->futureDeliveryMessage ?? null;
            $siteConfig->pickupOIP = $request->pickupOIP;
            $siteConfig->deliverOIP = $request->deliverOIP;
            $siteConfig->orderFooter = $request->orderFooter;
            $siteConfig->deliverOrderReady = $request->deliverOrderReady;
            $siteConfig->orderReady = $request->orderReady;
            $siteConfig->orderAlert = $request->orderAlert;
            $siteConfig->welcomeEmail = $request->welcomeEmail;
        $sc = $siteConfig->save();
    
        if ($sc) {
            $request->session()->flash('updateSuccess', 'System Email Update Successful.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating the email information. Please try again.');
        }

        return redirect()->action('settingsController@system', ['urlPrefix' => $urlPrefix]);
            
    } // end systemDo




    /*-------------------- Payment Settings  ----------------------------------------*/

    public function payment($urlPrefix) {
    
        $breadcrumb = ['settings', ''];
        $domain = \Request::get('domain');
        $paymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
        $ccTypes = ccType::orderBy('sortOrder', 'asc')->get();
    
        return view('settings.paymentSettingsEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'paymentConfig' => $paymentConfig, 'ccTypes' => $ccTypes, 'urlPrefix' => $urlPrefix ]);
        
    } // end payment
    
    
    /*-------------------- Payment Settings Save (Do) ----------------------------------------*/
    
    public function paymentDo(Request $request, $urlPrefix) {
    
        $domain = \Request::get('domain');
        $paymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
          $paymentConfig->accountName = $request->accountName;
          $paymentConfig->ccType = $request->ccType;
          $paymentConfig->mode = $request->mode;
          $paymentConfig->accountName = $request->accountName;
          $paymentConfig->stripeTestPublishableKey = $request->stripeTestPublishableKey;
          $paymentConfig->stripeTestSecretKey = encrypt($request->stripeTestSecretKey);
          $paymentConfig->stripeLivePublishableKey = $request->stripeLivePublishableKey;
          $paymentConfig->stripeLiveSecretKey = encrypt($request->stripeLiveSecretKey);
          $paymentConfig->stripeDataImage = $request->stripeDataImage;
          $paymentConfig->stripeStatementDescriptor = $request->stripeStatementDescriptor;
          $paymentConfig->stripeTransactionPercent = $request->stripeTransactionPercent;
          $paymentConfig->stripeTransactionFee = $request->stripeTransactionFee;
          $paymentConfig->togoMerchantID = $request->togoMerchantID;
          $paymentConfig->togoACHMerchantID = $request->togoACHMerchantID;
          $paymentConfig->togoTestUser = $request->togoTestUser;
          $paymentConfig->togoTestPassword = encrypt($request->togoTestPassword);
          $paymentConfig->togoLiveUser = $request->togoLiveUser;
          $paymentConfig->togoLivePassword = encrypt($request->togoLivePassword);
          $paymentConfig->togoMonthlyProcessingFee = $request->togoMonthlyProcessingFee;
          $paymentConfig->togoTransactionPercent = $request->togoTransactionPercent;
          $paymentConfig->togoTransactionFee = $request->togoTransactionFee;
          $paymentConfig->authNetTestUser = $request->authNetTestUser;
          $paymentConfig->authNetTestPass = encrypt($request->authNetTestPass);
          $paymentConfig->authNetLiveUser = $request->authNetLiveUser;
          $paymentConfig->authNetLivePass = encrypt($request->authNetLivePass);
        
        
          $pc = $paymentConfig->save();

        $ccTypes = ccType::orderBy('sortOrder', 'asc')->get();
    
        if ($pc) {
          $request->session()->flash('updateSuccess', 'Update Successful.');
        } else {
          $request->session()->flash('updateError', 'There was an error updating the payment information. Please try again.');
        }
    
        return redirect()->action('settingsController@payment', ['urlPrefix' => $urlPrefix]);
        
    } // end paymentDo
    


    /*-------------------- Manager PIN Settings  ----------------------------------------*/

    public function managerPIN($urlPrefix) {
        
            $breadcrumb = ['settings', ''];
            $domain = \Request::get('domain');
            $managerPIN = managerPIN::where([['domainID', '=', $domain->id], ['userID', '=', Auth::id()]])->first();

            return view('settings.managerPINEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'managerPIN' => $managerPIN ]);
            
        } // end managerPIN



    
    /*-------------------- Manager PIN Do  ----------------------------------------*/

    public function managerPINDo(Request $request, $urlPrefix) {
        
        $domain = \Request::get('domain');
        $managerPIN = managerPIN::updateOrCreate(
            ['domainID' => $domain->id, 'userID' => Auth::id()],
            ['managerPIN' => $request->managerPIN]
        );

        if (!is_null($managerPIN)) {
            $request->session()->flash('updateSuccess', 'Update Successful.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating the PIN. Please try again.');
        }
    
        return redirect()->action('settingsController@managerPIN', ['urlPrefix' => $urlPrefix]);
        
    } // end managerPINDo



    /*-------------------- Signature Settings  ----------------------------------------*/

    public function signature($urlPrefix) {
        
            $breadcrumb = ['settings', ''];
            $domain = \Request::get('domain');
            $signature = signatures::where('domainID', '=', $domain->id)->first();
        
            return view('settings.signatureEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'signature' => $signature ]);
            
        } // end signature
    

        
    /*-------------------- Signature Settings Do  ----------------------------------------*/

    public function signatureDo(Request $request, $urlPrefix) {
        
        $domain = \Request::get('domain');
        $signature = signatures::updateOrCreate(
            ['domainID' => $domain->id, 'userID' => Auth::id()],
            ['signature' => $request->signature, 'textSignature' => $request->textSignature]
        );

        if (!is_null($signature)) {
            $request->session()->flash('updateSuccess', 'Update Successful.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating your signature. Please try again.');
        }
    
        return redirect()->action('settingsController@signature', ['urlPrefix' => $urlPrefix]);
        
    } // end signatureDo

    
    /*-------------------- MyInfo Settings  ----------------------------------------*/

    public function myInfo($urlPrefix) {
        
            $breadcrumb = ['settings', ''];
            $domain = \Request::get('domain');
            $user = Auth::user();
        
            return view('settings.myInfo')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'user' => $user ]);
            
        } // end signature

        
    /*-------------------- MyInfo Settings Do  ----------------------------------------*/

    public function myInfoDo(Request $request, $urlPrefix) {

        $numOnly = CellNumberData::numbersOnly($request->mobile);
        
        $user = User::find(Auth::id());
            $user->fname = $request->fname;
            $user->lname = $request->lname;
            $user->mobile = $request->mobile;
            $user->unformattedMobile = $numOnly;
            $user->birthDay = $request->birthDay;
            $user->birthMonth = $request->birthMonth;
            $user->email = $request->email;
        $ok = $user->save();

        if ($ok) {
            $request->session()->flash('updateSuccess', 'Update Successful.');
        } else {
            $request->session()->flash('updateError', 'There was an error updating your information. Please try again.');
        }
    
        return redirect()->action('settingsController@myInfo', ['urlPrefix' => $urlPrefix]);
        
    } // end signatureDo


    /*-------------------- Reset Password  ----------------------------------------*/

    public function resetPassword($urlPrefix) {
        
            $breadcrumb = ['settings', ''];
            $domain = \Request::get('domain');
            $user = Auth::user();
        
            return view('settings.password')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'user' => $user ]);
            
        } // end resetPassword


    /*-------------------- MyInfo Settings Do  ----------------------------------------*/

    public function resetPasswordDo(Request $request, $urlPrefix) {
        
        $user = User::find(Auth::id());
            $user->password = bcrypt($request->password1);
        $ok = $user->save();
        
        if ($ok) {
            $request->session()->flash('updateSuccess', 'Your password was reset.');
        } else {
            $request->session()->flash('updateError', 'There was an error resetting your password. Please try again.');
        }
    
        return redirect()->action('settingsController@resetPassword', ['urlPrefix' => $urlPrefix]);
        
    } // end resetPasswordDo


        /*-------------------- Suspend One Hour  ----------------------------------------*/

        public function suspendOneHour(Request $request, $urlPrefix) {


        $domain = \Request::get('domain');
        $dt = Carbon::now();

        // remove suspension
        $old = $this->removeSuspension($domain->id);
        
        $suspend = suspendOperation::firstOrNew(['domainID' => $domain->id]);
            $suspend->startTime = $dt->toDateTimeString();
            $suspend->stopTime = $dt->addhour()->toDateTimeString();
            $suspend->numMinutes = 60;
            $suspend->userID = Auth::id();
        $ok = $suspend->save();

        if ($ok) {
            $request->session()->flash('updateSuccess', 'Online orders have been suspended for 60 minutes.');
        } else {
            $request->session()->flash('updateError', 'There was suspending online ordering. Please try again.');
        }

        // add random number to url to bust browser cache
        $url = '/settings/' . $urlPrefix . '/dashboard?num=' . rand(100000, 10000000);
        return redirect($url);
        
    } // end suspendOneHour


        /*-------------------- Suspend Settings  ----------------------------------------*/

        public function suspendSettings(Request $request, $urlPrefix) {


        $breadcrumb = ['settings', ''];
        $domain = \Request::get('domain');
        $user = Auth::user();
        $status = ownerController::suspendStatus($domain->id);
        $suspendOperation = suspendOperation::where('domainID', '=', $domain->id)->whereDate('startTime', '=', Carbon::now()->toDateString())->first();

        return view('settings.suspendSettings')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'user' => $user, 'status' => $status, 'suspendOperation' => $suspendOperation ]);
        
    } // end suspendSettings


    /*-------------------- Suspend Settings Do  ----------------------------------------*/

    public function suspendSettingsDo(Request $request, $urlPrefix) {

        $dt = Carbon::now();
        $startTime = $dt->toDateTimeString();
        $stopTime = $this->suspendStopTime($dt, $request->numMinutes);
        $domain = \Request::get('domain');
        
        // remove suspension
        $ok = $this->removeSuspension($domain->id); 
        
        if ($request->domainID == 0) { 
            
            if ($ok['status']) {
                $request->session()->flash('updateSuccess', $ok['message']);
            } else {
                $request->session()->flash('updateError', 'There was a problem resuming online ordering. Please try again.');
            }

        } else {

            // update suspension
            $suspend = suspendOperation::firstOrNew(['domainID' => $domain->id]);
                $suspend->startTime = $startTime;
                $suspend->stopTime = $stopTime['stopTime'];
                $suspend->numMinutes = $stopTime['numMinutes'];
                $suspend->userID = Auth::id();
            $ok = $suspend->save();
            if ($ok) {
                $request->session()->flash('updateSuccess', 'Online orders have been suspended for ' . $stopTime['numMinutes'] . ' minutes.');
            } else {
                $request->session()->flash('updateError', 'There was a problem suspending online ordering. Please try again.');
            }

        } // end if remove/update

        // add random number to url to bust browser cache
        $url = '/settings/' . $urlPrefix . '/suspend/settings?num=' . rand(100000, 10000000);
        return redirect($url);
    
    } // end suspendSettingsDo


    /*-------------------- Suspend Stop Time  ----------------------------------------*/

    public function suspendStopTime($dt, $numMinutes) {

        $stopTime = [];

        if ($numMinutes == 999) {
            // till midnight
            $midnight = Carbon::createMidnightDate($dt->year, $dt->month, $dt->day + 1);
            $stopTime['stopTime'] = $midnight->toDateTimeString();
            $stopTime['numMinutes'] = $dt->diffInMinutes($midnight);
        } else {
            // fixed time
            $stopTime['stopTime'] = $dt->addMinutes($numMinutes)->toDateTimeString();
            $stopTime['numMinutes'] = $numMinutes;
        }

        return $stopTime;
    
    } // end suspendSettingsDo


    /*-------------------- Remove Suspension  ----------------------------------------*/

    public function removeSuspension($domainID) {

        $ok = [];
        $status = suspendOperation::where('domainID', '=', $domainID)->first();

        if (is_null($status)) {
            $ok['status'] = true;
            $ok['message'] = 'Online operations are online. No action has been taken.';
            return $ok;
        }

        // move to history
        $ok = $this->suspendToHistory($status);

        // delete(remove) suspension
        $status->delete();

        return $ok;

    } // end removeSuspension


    /*-------------------- suspend to history  ----------------------------------------*/

    public function suspendToHistory($status) {

        $ok = [];

        // suspension minutes
        $dt = Carbon::parse($status->startTime);
        $dt2 = Carbon::now();
        $suspendMinutes = $dt->diffInMinutes($dt2);
        if ($suspendMinutes <= 0) { $suspendMinutes = 1; }

        // move to suspendOperationHistory
        $sh = new suspendOperationHistory;
            $sh->domainID = $status->domainID;
            $sh->userID = $status->userID;
            $sh->startTime = $status->startTime;
            $sh->stopTime = $dt2->toDatetimeString();
            $sh->numMinutes = $suspendMinutes;
        $ok['status'] = $sh->save();
        $ok['message'] = 'Online operations have been returned to normal. Online ordering is now available.';

        return $ok;


    }  // end suspendToHistory




            

} // end settingsController