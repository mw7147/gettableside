<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Facades\App\hwct\PrintOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\domainSiteConfig;
use App\stripeCustomer;
use App\ordersDetail;
use App\managerPIN;
use App\stripeCard;
use App\siteStyles;
use App\marketing;
use Carbon\Carbon;
use App\printers;
use App\cartData;
use App\usStates;
use App\contacts;
use App\orders;
use App\domain;



class cashController extends Controller
{
    

/*---------------- Cash - orderPayment  ----------------------------------*/

	public function orderPayment(Request $request) {

	

		// check sessionID - make sure order does not exist with same sessionID
		$orderCheck = orders::where('sessionID', '=', $request->sid)->first();
		if (!empty($orderCheck)) { 
			session()->flash('cardError', 'An order has already been processed for this session. Please clear your cart and try again.');
			return redirect()->action('cartController@viewCart');
		}

		$userID = Auth::id();
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
		$styles = siteStyles::where('domainID', '=', $domain->id)->first();
		$numOnly = CellNumberData::numbersOnly($request->mobile);

		// get data for marketing system
		$marketing = marketing::findOrFail($domain->parentDomainID);

		// update or create contact based on email and domainID
		$contact = contacts::updateOrCreate([
			'email' => $request->email,
			'tgmDomainID' => $domain->id
			], [
			'domainID' => $marketing->mktDomainID,
			'customerID' => $marketing->mktCustomerID,
			'userID' => $marketing->mktUserID,
			'repID' => $marketing->mktRepID,
			'fname' => $request->fname,
			'lname' => $request->lname,
			'tgmUserID' => $request->userID,
			'mobile' => $request->mobile,
			'unformattedMobile' => $numOnly,
			'address1' => $request->address1,
			'address2' => $request->address2,
			'city' => $request->city,
			'state' => $request->State,
			'zip' => $request->zipCode
		]);

		// cash payment - create transaction and order
		
		if ($request->delivery == 'delivery') {
			// delivery
			$delivery = "deliver";
		} else {
			// pickup
			$delivery = "pickup";
		}

		// update contact info with approved info
		$contact->lastDate = Carbon::now()->toDateTimeString();
		$contact->lastAmount = $request->total;
		$contact->save();

		// create new order
		$order = new orders;
			$order->domainID = $domain['id'];
			$order->ownerID = $domain['ownerID'];
			$order->contactID = $contact->id;
			$order->userID = $contact->userID;
			$order->sessionID = $request->sid;
			$order->fname = $contact->fname;
			$order->lname = $contact->lname;
			$order->unformattedMobile = $numOnly;
            $order->customerEmailAddress = $request->email;
			$order->customerMobile = $request->mobile;
			$order->address1 = $request->address1;
			$order->address2 = $request->address2;
			$order->city = $request->city;
			$order->state = $request->State;
			$order->zipCode = $request->zip;
			$order->subTotal = $request->subTotal;
			$order->discountedSubTotal = $request->subTotal - $request->orderDiscount;
			$order->discountAmount = $request->orderDiscount ?? 0;
			$order->discountPercent = $request->orderDiscountPercent ?? 0;
			$order->tax = $request->tax;
			$order->tip = $request->tip ?? 0;
			$order->deliveryCharge = $domainSiteConfig->deliveryCharge ?? 0;
			$order->transactionFee = $domainSiteConfig->transactionFee ?? 0;
			$order->total = $request->total;
            $order->ccType = $domainPaymentConfig->ccType;
            $order->paymentType = 'onReceipt';
			$order->orderType = $delivery; // add delivery and in restaurant options
            $order->waiterID = 0; // add waiters
            $order->tableID = 0; // add tables
            $order->userAgent = $request->userAgent;
            $order->orderIP = \Request::ip();
            $order->restaurantEmailAddress = $domainSiteConfig->sendEmail;
			$order->orderInProcess = 1; // set order in process
			$order->orderRefundAmount = 0; // set refund to 0
			$order->timezone = $domainSiteConfig->timezone;
			if ($delivery == "deliver") {
				$order->deliveryLatitude = $request->deliveryLatitude;
				$order->deliveryLongitude = $request->deliveryLongitude;
				$order->deliveryFeet = $request->deliveryFeet;
			}
		$order->save();

		$details = cartData::where('sessionID', '=', $request->sid)->get();
		foreach ($details as $detail) {
			$od = new ordersDetail;
				$od->domainID = $detail->domainID;
				$od->ownerID = $detail->ownerID;
				$od->contactID = $contact->id;
				$od->sessionID = $detail->sessionID;
				$od->menuDataID = $detail->menuDataID;
				$od->menuCategoriesDataID = $detail->menuCategoriesDataID;
				$od->menuCategoriesDataSortOrder = $detail->menuCategoriesDataSortOrder;
				$od->menuDataSortOrder = $detail->menuDataSortOrder;
				$od->menuItem = $detail->menuItem;
				$od->menuDescription = $detail->menuDescription;
				$od->portion = $detail->portion;
				$od->printOrder = $detail->printOrder;
				$od->printReceipt = $detail->printReceipt;
				$od->price = $detail->price;
				$od->menuOptionsDataID = $detail->menuOptionsDataID;
				$od->menuAddOnsDataID = $detail->menuAddOnsDataID;
				$od->menuSidesDataID = $detail->menuSidesDataID;
				$od->instructions = $detail->instructions;
				$od->orderIP = $detail->orderIP;
				$od->tipAmount = $detail->tipAmount;
				$od->discountPercent = $request->orderDiscountPercent ?? 0;

			$od->save();
			cartData::destroy($detail->id);
		} // end foreach


		// print receipt letter and return pdf file path
		$order->receiptLetterPath = PrintOrders::receiptLetterPDF($contact, $order, $domainSiteConfig);
	
		// print receipt thermal and return pdf file path
		$order->receiptThermalPath = PrintOrders::receiptThermalPDF($contact, $order, $domainSiteConfig);
		
		// print order letter and return pdf file path - kitchen
		$order->orderLetterPath = PrintOrders::orderLetterPDF($contact, $order, $domainSiteConfig);

		// print  order thermal and return pdf file path - kitchen
		$order->orderThermalPath = PrintOrders::orderThermalPDF($contact, $order, $domainSiteConfig);
	
		// send customer email
		$sendMail = ProcessOrders::sendOrderEmail($contact, $order, $domainSiteConfig);
		$order->emailData = $sendMail['html']; // send customer email
		
		// update order
		$order->save();	

		// print receipt
		if ( $domainSiteConfig->printReceipt > 0 ) {
			// get printer info
			$printer = printers::find($domainSiteConfig->printReceipt);
			
			// print receipt
			if (!is_null($printer)) { $printReceipt = PrintOrders::printReceipt($printer, $order, $domainSiteConfig, 'new'); }

		} // end print receipt

		// print order (kitchen)
		if ( $domainSiteConfig->printOrder > 0 ) {
			// get printer info
			$printer = printers::find($domainSiteConfig->printOrder);
			
			// print receipt
			if (!is_null($printer)) { $printOrder = PrintOrders::printOrder($printer, $order, $domainSiteConfig, 'new'); }

		} // end print order

		// Send Text Confirmation
		$textSent = HWCTMailer::sendOrderText($numOnly, $domainSiteConfig, $delivery);

		// Send Email to restaurant
		$emailSent = ProcessOrders::sendDomainEmail($contact, $order, $domainSiteConfig);

		session()->regenerate();
	    $message['orderAlert'] = $domainSiteConfig->orderAlert;
	    $message['orderID'] = $order->sessionID;
	    $message['numSent'] = $sendMail['numSent'];
		
	    return view('orders.viewOrder')->with([ 'html' => $order->emailData ]);


	} // end orderPayment


/*---------------- OnReceipt Refund   ----------------------------------*/
	
public function refundCharge(Request $request) {

	$order = orders::find($request->orderID);
	$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $order->domainID)->first();

	// manager PIN check
	$chk = managerPIN::where([['domainID', '=', $order->domainID], ['userID', '=', $request->userID], ['managerPIN', '=', $request->managerPIN]])->first();
	if (is_null($chk)) {
		$data=[];
		$data['status'] = 'error';
		$data['message'] = 'The Manager PIN was not correct. Please enter the correct PIN.';
		return $data;
	}

	// record order as refunded
	$order->orderRefunded = 1;
	if ($order->total == $order->refundAmount) {
		$order->orderInProcess = 0;
	}
	$order->orderRefundID = $order->id;
	$order->orderRefundAmount = $request->refundAmount;
	$order->save();
	

	$data=[];
	$data['status'] = $status;
	$data['message'] = $message;
	$data['stripe'] = $refund->id;
	
	return $data;

} // end refundCharge








} // end cashController
