<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\messageHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;
use App\Mail\sendTestEmailMsg;
use Illuminate\Http\Request;
use App\domainSiteConfig;
use App\Http\Requests;
use App\emailTemplate;
use App\groupMember;
use App\emailsSent;
use App\signatures;
use App\usersData;
use App\groupName;
use Carbon\Carbon;
use App\contacts;
use App\linkData;
use App\domain;
use App\User;


class emailController extends Controller
{


  /*-------------------------------- QuickMail ---------------------------------------*/   

  public function quickMail(Request $request) {

    $domain = \Request::get('domain');
    $breadcrumb = ['email', 'quick'];
    
    $groups = messageHelpers::groupNames($domain->id);
    $totalContacts = contacts::where("domainID", '=', $domain->id)->count();
      
    return view('email.quickMail')->with(['domain' => $domain, 'groups' => $groups, 'breadcrumb' => $breadcrumb, 'totalContacts' => $totalContacts]);
    
  }     

  /*--------------------------------QuickMail Queue (Process)---------------------------------------*/   

  public function quickMailConfirm(Request $request) {

    if (!isset($request->group)) {abort(404);} //abort if no group found
// dd($request);
    $domain = \Request::get('domain');
    $user = Auth::user();
    $groupID = $request->group;
    $groupID = $groupID[0];
    $uid = $request->userID;
    $did = $request->domainID;
    $emailTemplateID = $request->templateID;
    $htmlBody = $request->emailBody;
    $textBody = $request->textBody;
    $goodCount = 0; // message queued
    $badCount = 0;  // no email address
    $isTest = $request->sendTest;
    $carbon = new Carbon();
    $dt = Carbon::now();
    if ($htmlBody == '') {$type = "text";} else {$type="html";}
    // set groupID = all to 0 to store in db (integer required)
    if ($groupID == "all") {$groupID = 0;} 

    $messageType = "QuickMail";


    // save attachment for sending
    if (!is_null($request->file('attachment'))) {

        // add attachment
        $attachmentName = $request->file('attachment')->getClientOriginalName();
        $attachmentSize = round($request->file('attachment')->getSize()/1024,1); // file size in Kilobytes
        $ext = $request->file('attachment')->guessClientExtension();
        if($ext == 'jpeg') {$ext = 'jpg';}
        $imageTypes = array("jpg" => "jpg", "png" => "png", "pdf" => "pdf");
        $chk = array_search($ext, $imageTypes);

        if ($chk == FALSE || $attachmentSize > 10000) {
            $request->session()->flash('fileError', 'The file ' . $attachmentName . ' was not a JPG, PNG or PDF file or the file size exceeded 10Mb. Please check your attachment and try again.');
            $chk = FALSE;
            $path = '';
            // go back to QuickMail and redo

            // return selected group
            $request->session()->flash('groupID', $groupID);

            return redirect()->back()->withInput();
        } else {
          //save new file
          // convert to URL friendly filename
          $attachmentName = basename($attachmentName, $ext);
          $imageName = camel_case(str_slug($attachmentName));
          $imageName = $imageName . "." . $ext;

          // store image on server
          $folderPath = env('CUSTOMER_ATTACHMENT_PATH') . "/" . $did;
          $attachmentPrivate = $request->file('attachment')->storeAs($folderPath, $imageName);
       //   $attachmentPublic = env('PUBLIC_ATTACHMENT_PATH') . $did . '/' . $imageName;
       //   $attachmentPrivate = $path;
        }

    } else {
      $attachmentPrivate = $request->fileCheck;
    }

    // set textBody if not defined
    if ($textBody == '') {$textBody = strip_tags($htmlBody);}

    // get contactList, signature and repID
    $contactList = $this->getContactList($groupID, $did);
    $signature = $this->getSignature($request->signature, $uid);
    // $repID = getRepID($groupID, $did);

    // Add tracker placeholder
    if ($isTest == 'no') { $htmlBody = $htmlBody  . "<p>[[trackerIMG]]</p>"; }
    // get domainSiteConfig
    $domainSiteConfig = domainSiteConfig::select('managerFname', 'managerLname', 'sendEmail', 'locationName')->where('domainID', '=', $domain->id)->first();
  
    // add signature if required
    if (isset($signature)) {
      $htmlBody = $htmlBody . html_entity_decode($signature->signature);
      $textBody = $textBody . html_entity_decode($signature->textSignature);
    }

    if ($isTest == 'yes') {

      // send test email
      // replace mywebsite links
      $siteLink = '<a href="http://' . $domain['httpHost'] . '">' . $domain['sitename'] . '</a>'; 
      $htmlBody = str_replace("[[MyWebsiteLink]]", $siteLink, $htmlBody);
      $textBody = str_replace("[[MyWebsiteLink]]", $domain['httpHost'], $textBody);

       // replace placeholers - html
       $htmlBody = str_replace("[[FirstName]]", $domainSiteConfig->managerFname, $htmlBody);
       $htmlBody = str_replace("[[LastName]]", $domainSiteConfig->managerLname, $htmlBody);
       $htmlBody = str_replace("[[Company]]", $domainSiteConfig->locationName, $htmlBody);

        // replace placeholers - text
       $textBody = str_replace("[[FirstName]]", $domainSiteConfig->managerFname, $textBody);
       $textBody = str_replace("[[LastName]]", $domainSiteConfig->managerLname, $textBody);
       $textBody = str_replace("[[Company]]", $domainSiteConfig->locationName, $textBody);

       //load images to Amazon S3
       $processImages = $this->imgToS3($htmlBody, $did, $isTest);

       // add link tracker
       $mid = 0; // test MID
       $processLinks = $this->linkTracker($processImages, $did, $uid, $did, $mid, $emailTemplateID);

                // sendTestEmailMessage($domainID, $userID, $emailAddress, $subject, $htmlBody, $textBody, $attachment, $fname, $lname, $type)
       $numSent = $this->sendTestEmailMessage($did, $uid, $user->email, $request->subject, $processLinks, $textBody, $attachmentPrivate, $user->fname, $user->lname, $type);

        // return data
        $request->session()->flash('QuickMailTest', 'A test email was sent to your registered email address.');
        $request->session()->flash('attachmentPrivate', $attachmentPrivate);
        $request->session()->flash('groupID', $groupID);

         // go back to QuickMail to process
         return redirect()->back()->withInput();

    }  // end if isTest

    // create message ID - insert record into emailsSent
    $emailSent = new emailsSent();
    $emailSent->domainID = $did;
    $emailSent->userID = $uid;
    $emailSent->groupID = $groupID;
    $emailSent->templateID = $emailTemplateID;
    $emailSent->messageType = $messageType;
    $emailSent->fromEmailAddress = $domainSiteConfig->sendEmail;    
    $emailSent->fromName = $domainSiteConfig->managerFname . " " . $domainSiteConfig->managerLname;
    $emailSent->subject = $request->subject;
    $emailSent->emailBody = $htmlBody;
    $emailSent->textBody = $textBody;
    $emailSent->attachment = trim($attachmentPrivate);
    $emailSent->sendIP = $_SERVER['REMOTE_ADDR'];
    $emailSent->totalQueue = 0;
    $emailSent->totalBad = 0;
    $emailSent->totalSent = 0;
    $emailSent->totalSentFail = 0;
    $emailSent->save();

    // replace mywebsite links
    $siteLink = '<a href="http://' . $domain['httpHost'] . '">' . $domain['sitename'] . '</a>'; 
    $htmlBody = str_replace("[[MyWebsiteLink]]", $siteLink, $htmlBody);
    $textBody = str_replace("[[MyWebsiteLink]]", $domain['httpHost'], $textBody);

    //load images to Amazon S3
    $processImages = $this->imgToS3($htmlBody, $did, $isTest);

    // add link tracker
    $processLinks = $this->linkTracker($processImages, $did, $uid, $did, $emailSent->id, $emailTemplateID);

    // Queue messages for sending with mail server
    foreach ($contactList as $list) {
      
      if ($list->email != '') {

        // replace placeholers - html
       $htmlBodyFinal = str_replace("[[FirstName]]", $list->fname, $processLinks);
       $htmlBodyFinal = str_replace("[[LastName]]", $list->lname, $htmlBodyFinal);
       $htmlBodyFinal = str_replace("[[Company]]", $list->company, $htmlBodyFinal);
       $htmlBodyFinal = str_replace("[[CID]]", $list->id, $htmlBodyFinal);

        // replace placeholers - text
       $textBodyFinal = str_replace("[[FirstName]]", $list->fname, $textBody);
       $textBodyFinal = str_replace("[[LastName]]", $list->lname, $textBodyFinal);
       $textBodyFinal = str_replace("[[Company]]", $list->company, $textBodyFinal);

        DB::connection('maildb')->table('emailQueue')->insert([

          [
            'domainID' => $domain->id,
            'userID' => $request->userID,
            'groupID' => $groupID,
            'templateID' => $request->templateID,
            'contactID' => $list->id,
            'messageID' => $emailSent->id,
            'fname' => $list->fname,
            'lname' => $list->lname,
            'company' => $list->company,
            'messageType' => $messageType,
            'emailAddress' => $list->email,
            'subject' => $request->subject,
            'emailBody' => $htmlBodyFinal,
            'textBody' => $textBodyFinal,
            'attachment' => $attachmentPrivate,
            'created_at' => $dt
          ]
        ]);
        // good message - message queued
        $goodCount = $goodCount +1;

      } else {
        // bad message - did not queue
        $badCount = $badCount +1;

      } // end if mobile != ''

    } // end foreach $contactList

    // insert record into emailsSent
    $emailSent->totalQueue = $goodCount;
    $emailSent->totalBad = $badCount;
    $emailSent->save();

    $midQueue = DB::connection('maildb')->table('emailMIDQueue')->insert([
      [
        'mid' => $emailSent->id,
        'domainID' => $domain->id,
        'created_at' => $dt
      ]
    ]);

    $breadcrumb = ['email', 'quick'];

    return view('email.quickMailConfirm')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'goodCount' => $goodCount, 'badCount' => $badCount, 'messageID' => $emailSent->id]);

  }     // end quickMailConfirm



 /*-------------------------------------------------------------------------------------------*/   

  	public function customerEmailView(Request $request) {

		$cid = \Request::get('customerID');
		$breadcrumb = ['email', 'view'];
		$domain = \Request::get('domain');
	  	$templates = emailTemplate::where('customerID', '=', $cid)->get();

	    return view('customer.customerEmailView')->with(['domain' => $domain, 'templates' => $templates, 'breadcrumb' => $breadcrumb]);
    
    }      // end customerEmailView


 /*-------------------------------------------------------------------------------------------*/   


    public function customerEmailDelete(Request $request, $id) {

		$cid = \Request::get('customerID');

		$email = emailTemplate::find($id);
		// delete attachment if not used in another email template
		// determine if attachment used by another email template
		$count = emailTemplate::where('attachmentPrivate', $email->attachmentPrivate)->count();
		if ($count == 1) {
			// only used by this template - delete attachment
			Storage::delete($email->attachmentPrivate);
		}
    	emailTemplate::destroy($id);

    	$breadcrumb = ['email', 'view'];
    	$domain = \Request::get('domain');
	   	$templates = emailTemplate::where('customerID', '=', $cid)->get();
	   	return Redirect::to('/customers/emails/view')->with(['domain' => $domain, 'templates' => $templates, 'breadcrumb' => $breadcrumb]);
    
    }

 /*-------------------------------------------------------------------------------------------*/   


  	public function customerEmailNew(Request $request) {

	    $breadcrumb = ['email', 'new'];
		$domain = \Request::get('domain');

	    return view('customer.customerEmailNew')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb]);
    
    }     


 /*-------------------------------------------------------------------------------------------*/   

  	public function customerEmailNewSave(Request $request) {
  		//dd($request);
	    $breadcrumb = ['email', 'view'];
		  $domain = \Request::get('domain');
      $cid = \Request::get('customerID');
		  $emailMsg = new emailTemplate();

		  if (!is_null($request->file('attachment'))) {
      
	        // add attachment
	        $attachmentName = $request->file('attachment')->getClientOriginalName();
	        $attachmentSize = round($request->file('attachment')->getSize()/1024,1); // file size in Kilobytes
	        $ext = $request->file('attachment')->guessClientExtension();
	        if($ext == 'jpeg') {$ext = 'jpg';}
	        $imageTypes= array("jpg" => "jpg", "png" => "png", "pdf" => "pdf");
	        $chk = array_search($ext, $imageTypes);

        	if ($chk == FALSE || $attachmentSize > 10000) {

	            $request->session()->flash('fileError', 'The file ' . $attachmentName . '. was not a JPG, PNG or PDF file or the file size exceeded 10Mb. Please check your attachment and try again.');
	            $chk = FALSE;
	            $path = '';

        	} else {

	        	//save new file
				    // convert to URL friendly filename
				    $attachmentName = basename($attachmentName, $ext);
				    $imageName = camel_case(str_slug($attachmentName));
				    $imageName = $imageName . "." . $ext;

				    // store image on server
				    $folderPath = env('CUSTOMER_ATTACHMENT_PATH') . "/" . $cid;
		        $path = $request->file('attachment')->storeAs($folderPath, $imageName);

	          $emailMsg->attachmentPublic = env('PUBLIC_ATTACHMENT_PATH') . $cid . '/' . $imageName;
	          $emailMsg->attachmentPrivate = $path;
	        
	        }

    	}

      	$emailMsg->domainID = $domain['id'];
      	$emailMsg->customerID = $cid;

       	$emailMsg->status = $request->active;
       	$emailMsg->name = $request->name;
       	$emailMsg->description = $request->description;
       	$emailMsg->subject = $request->subject;
       	$emailMsg->body = $request->emailBody;
       	$emailMsg->textbody = strip_tags($request->emailBody);


       	try {
	        $emailMsg->save();
	        $request->session()->flash('saveSuccess', $emailMsg->name . ' was successfully created!');
      	}

      	catch (Exception $e) {
        	$request->session()->flash('saveError', 'There was an error creating the new email template. Please try again.');
      	}

	    return view('customer.customerEmailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'emailTemplate' => $emailMsg]);
    
    }     


 /*-------------------------------------------------------------------------------------------*/   

    public function customerEmailCopy(Request $request, $id) {

      	$domain = \Request::get('domain');
		    $cid = \Request::get('customerID');

      	$oldEmailMsg = emailTemplate::find($id);
      	if($oldEmailMsg == '') {abort(404);}

      	$emailMsg = new emailTemplate();

      	// create textTemplates record
      	$emailMsg->customerID = $cid;
      	$emailMsg->domainID = $domain['id'];
      	$emailMsg->status = $oldEmailMsg->status;
      	$emailMsg->name = $oldEmailMsg->name;
      	$emailMsg->description = $oldEmailMsg->description;
      	$emailMsg->subject = $oldEmailMsg->subject;
      	$emailMsg->body = $oldEmailMsg->body;
      	$emailMsg->textbody = $oldEmailMsg->textbody;
      	$emailMsg->attachmentPublic = $oldEmailMsg->attachmentPublic;
      	$emailMsg->attachmentPrivate = $oldEmailMsg->attachmentPrivate;

      	try {
        	$emailMsg->save();
        	$request->session()->flash('updateSuccess', 'Email Template ID: ' . $emailMsg->id . ' was successfully created!');
      	}

      	catch (Exception $e) {
        	$request->session()->flash('updateError', 'There was an error creating your new template. Please try again.');
      	}

      	$breadcrumb = ['email', 'view']; //breadcrumb is same as view - same menu item
      
	    return view('customer.customerEmailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'emailTemplate' => $emailMsg]);
    	//return Redirect::to('/customers/emails/edit/' . $emailMsg->id)->with(['domain' => $domain, 'templates' => $templates, 'breadcrumb' => $breadcrumb]);

    }

 /*-------------------------------------------------------------------------------------------*/   


    public function customerEmailEdit(Request $request, $id) {

      	$domain = \Request::get('domain');
		    $cid = \Request::get('customerID');

      	$emailMsg = emailTemplate::find($id);
      	if($emailMsg == '') {abort(404);}

      	$breadcrumb = ['email', 'view']; //breadcrumb is same as view - same menu item
      
	    return view('customer.customerEmailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'emailTemplate' => $emailMsg]);

    }

 /*-------------------------------------------------------------------------------------------*/   

   public function customerEmailEditSave(Request $request, $id) {

      	$domain = \Request::get('domain');
		    $cid = \Request::get('customerID');
		    if (trim($request->fileCheck) == "existing") { $attachmentCheck = "existing"; } else { $attachmentCheck = "none";}
 
      	$emailMsg = emailTemplate::find($id);
      	if($emailMsg == '') {abort(404);}

      	// if no attachment
		    if ($attachmentCheck == "none") {
      		// no attachment
      		$emailMsg->attachmentPublic = '';
      		$emailMsg->attachmentPrivate = '';
      	}

      	// check if attachment name has changed
      	if ( $attachmentCheck = "existing" && $request->file('attachment') != '') {
      		$attachmentName = $request->file('attachment')->getClientOriginalName();
      		if (basename($emailMsg->attachmentPrivate) != $attachmentName) { $attachmentCheck = "different"; }
      	}

      	if ($attachmentCheck == "different") {
      
        	// add new attachment
        	$attachmentName = $request->file('attachment')->getClientOriginalName();
        	$attachmentSize = round($request->file('attachment')->getSize()/1024,1); // file size in Kilobytes
        	$ext = $request->file('attachment')->guessClientExtension();
        	$mime = $request->file('attachment')->getMimeType();
	        if($ext == 'jpeg') {$ext = 'jpg';}
	        $imageTypes= array("jpg" => "jpg", "png" => "png", "pdf" => "pdf");
        	$chk = array_search($ext, $imageTypes);

        	if ($chk == FALSE || $attachmentSize > 10000) {

            	$request->session()->flash('fileError', 'The file ' . $attachmentName . '. was not a JPG, PNG or PDF file or the file size exceeded 10Mb. Please check your attachment file and try again.');
            	$chk = FALSE;
            	$path = '';

        	} else {

        		$count = emailTemplate::where('attachmentPrivate', $emailMsg->attachmentPrivate)->count();
				if ($count == 1) {
					// only used by this template - delete attachment
          			Storage::delete($emailMsg->attachmentPrivate);
				}
				// Log::info('Count is: ' . $count . " emailMsg->attachmentPrivate is: " . $emailMsg->attachmentPrivate);

	          	//save new file
          		// convert to URL friendly filename
				$attachmentName = basename($attachmentName, $ext);
				$imageName = camel_case(str_slug($attachmentName));
				$imageName = $imageName . "." . $ext;

	          	// path textMsgImages/user->id/filename
	          	$folderPath = env('CUSTOMER_ATTACHMENT_PATH') . "/" . $cid;

	          //	$path = $request->file('attachment')->storeAs($folderPath, $imageName);
				$path = Storage::putFileAs($folderPath, $request->file('attachment'), $imageName);
				
				/*
				AMAZON S3 STORAGE

				$file_handle = Storage::get($path);
				$nameHash = md5($attachmentName . microtime());
				$nameHash = $nameHash . "." . $ext;
				
				$s3file = Storage::disk('s3')
				   	->getDriver()
				   	->put(('emailImages/' . $nameHash), $file_handle,  ['visibility' => 'public', 'ContentType' => $mime]
				);
				*/

	          	// create public storage path
		        $emailMsg->attachmentPublic = env('PUBLIC_ATTACHMENT_PATH') . $cid . '/' . $imageName;
		        $emailMsg->attachmentPrivate = $path;


        
        	}

      	}

      	// create textTemplates record
      	$emailMsg->status = $request->active;
      	$emailMsg->name = $request->name;
      	$emailMsg->description = $request->description;
      	$emailMsg->subject = $request->subject;
      	$emailMsg->body = $request->emailBody;
       	$emailMsg->textbody = strip_tags($request->emailBody);


      	try {
        	$emailMsg->save();
        	$request->session()->flash('updateSuccess', 'Email Template ID: ' . $id . ' was successfully created!');
      	}

      	catch (Exception $e) {
        	$request->session()->flash('updateError', 'There was an error creating your new template. Please try again.');
      	}

      	$breadcrumb = ['email', 'view']; //breadcrumb is same as view - same menu item
      
	    return view('customer.customerEmailEdit')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'emailTemplate' => $emailMsg]);
    	//return Redirect::to('/customers/emails/edit/' . $emailMsg->id)->with(['domain' => $domain, 'templates' => $templates, 'breadcrumb' => $breadcrumb]);

  }

 /*-------------------------------------------------------------------------------------------*/   


  public function sendEmailView(Request $request) {
    
	   $cid = \Request::get('customerID');
    $breadcrumb = ['email', 'send'];
    $domain = \Request::get('domain');
      
    $templates = emailTemplate::select('id', 'name', 'description')->where('customerID', '=', $cid)->where('status', '=', 'yes')->orderBy('id', 'desc')->get();

    return view('customer.customerSendEmailView')->with(['domain' => $domain, 'templates' => $templates, 'breadcrumb' => $breadcrumb]);
    
  }     

 /*-------------------------------------------------------------------------------------------*/   


  public function sendEmailDo(Request $request, $id) {

    $template = emailTemplate::find($id);
    if ($template == '') {abort(404);} // abort if no record found

	   $cid = \Request::get('customerID');

    $groups = groupName::leftJoin('groupMembers', 'groupNames.id', '=', 'groupMembers.groupID')
    ->select('groupNames.id', 'groupNames.groupName', DB::raw('count(groupMembers.id) as groupCount'))
    ->where('groupNames.customerID', '=', $cid)
    ->groupBy('groupNames.id', 'groupNames.groupName')
    ->get();

    $totalContacts = contacts::where("customerID", '=', $cid)->count();

    $breadcrumb = ['email', 'send'];

    $domain = \Request::get('domain');
      
    return view('customer.customerSendEmailDo')->with(['domain' => $domain, 'groups' => $groups, 'breadcrumb' => $breadcrumb, 'template' => $template, 'totalContacts' => $totalContacts]);
    
  }     



  /*--------------------------------SEND CUSTOMER EMAIL TEST---------------------------------------*/   


  public function sendCustomerEmailTest(Request $request)
  {

    $userID = $request->userID;
    $cid = $request->customerID;
    $htmlBody = html_entity_decode($request->emailBody);
    $textBody = strip_tags($request->textBody);
    $emailTemplateID = $request->emailTemplateID;
    $subject = strip_tags($request->subject);
    $domain = domain::where('id', '=', $request->domainID)->first();
    $mailID = 0; //0 mailID is test
    // get attachment
    $attachment = emailTemplate::find($emailTemplateID)->attachmentPrivate;
    // get signature
    $signature = getSignature($request->signature, $userID);
    // add signature if required

    if ($signature != 'no') {
      $htmlBody = $htmlBody . html_entity_decode($signature->signature);
      $textBody = $textBody . html_entity_decode($signature->textSignature);
    }
    
    $domainSiteConfig = domainSiteConfig::select('managerFname', 'managerLname', 'sendEmail', 'locationName')->where('domainID', '=', $domain->id)->first();
    
    // replace mywebsite links
    $siteLink = '<a href="http://' . $domain->httpHost . '">' . $domain->sitename . '</a>'; 
    $htmlBody = str_replace("[[MyWebsiteLink]]", $siteLink, $htmlBody);
    $textBody = str_replace("[[MyWebsiteLink]]", $domain->httpHost, $textBody);

   // replace placeholers - html
   $htmlBody = str_replace("[[FirstName]]", $domainSiteConfig->managerFname, $htmlBody);
   $htmlBody = str_replace("[[LastName]]", $domainSiteConfig->managerLname, $htmlBody);
   $htmlBody = str_replace("[[Company]]", $domainSiteConfig->locationName, $htmlBody);

    // replace placeholers - text
   $textBody = str_replace("[[FirstName]]", $domainSiteConfig->managerFname, $textBody);
   $textBody = str_replace("[[LastName]]", $domainSiteConfig->managerLname, $textBody);
   $textBody = str_replace("[[Company]]", $domainSiteConfig->locationName, $textBody);

   //load images to Amazon S3
   $isTest = 'yes';
    $processImages = imgToS3($htmlBody, $cid, $isTest);
    $processLinks = linkTracker($processImages, $request->domainID, $userID, $cid, $mailID, $emailTemplateID);

   // Log::info($htmlBody . "  <------------------------->  " . $textBody);

   Mail::to($domainSiteConfig->sendEmail)->send(new sendTestEmailMsg($textBody, $processLinks, $subject, $domainSiteConfig->sendEmail, $attachment));

    $emailData = array("status" => "test email", "message" =>"test email sent" . $domainSiteConfig->sendEmail);

     

  }




/*-------------------------------------------------------------------------------------------*/   




  public function customerEmailQueue(Request $request) {

    if (!isset($request->group)) {abort(404);} //abort if no group found
    
    $domain = \Request::get('domain');
    $groupID = $request->group;
    $groupID = $groupID[0];
    $cid = $request->customerID;
    $uid = $request->userID;
    $did = $request->domainID;
    $emailTemplateID = $request->templateID;
    $htmlBody = $request->emailBody;
    $textBody = $request->textBody;
    $goodCount = 0; // message queued
    $badCount = 0;  // no email address
    $carbon = new Carbon();
    $dt = Carbon::now();

    // get contactList, signature and repID
    $contactList = getContactList($groupID, $cid);
    $signature = getSignature($request->signature, $uid);
    $repID = getRepID($groupID, $cid);

    // set groupID = all to 0 to store in db (integer required)
    if ($groupID = "all") {$groupID = 0;} 
    $messageType = "email";
  
    // add signature if required
    if (isset($signature)) {
      $htmlBody = $htmlBody . html_entity_decode($signature->signature);
      $textBody = $textBody . html_entity_decode($signature->textSignature);
    }

    // Add tracker placeholder
    $htmlBody = $htmlBody  . "<p>[[trackerIMG]]</p>";

    // create message ID - insert record into emailsSent
    $emailSent = new emailsSent();
    $emailSent->domainID = $did;
    $emailSent->userID = $uid;
    $emailSent->customerID = $cid;
    $emailSent->repID = $repID;
    $emailSent->groupID = $groupID;
    $emailSent->templateID = $emailTemplateID;
    $emailSent->messageType = $messageType;
    $emailSent->subject = $request->subject;
    $emailSent->emailBody = $htmlBody;
    $emailSent->textBody = $textBody;
    $emailSent->attachment = $request->attachment;
    $emailSent->sendIP = $_SERVER['REMOTE_ADDR'];
    $emailSent->totalQueue = 0;
    $emailSent->totalBad = 0;
    $emailSent->totalSent = 0;
    $emailSent->totalSentFail = 0;
    $emailSent->save();

    // replace mywebsite links
    $siteLink = '<a href="http://' . $domain['httpHost'] . '">' . $domain['sitename'] . '</a>'; 
    $htmlBody = str_replace("[[MyWebsiteLink]]", $siteLink, $htmlBody);
    $textBody = str_replace("[[MyWebsiteLink]]", $domain['httpHost'], $textBody);

    //load images to Amazon S3
    $isTest = 'no';
    $processImages = imgToS3($htmlBody, $cid, $isTest);

    // add link tracker
    $processLinks = linkTracker($processImages, $did, $uid, $cid, $emailSent->id, $emailTemplateID);

    // Queue messages for sending with mail server
    foreach ($contactList as $list) {
      
      if ($list->email != '') {

        // replace placeholers - html
       $htmlBodyFinal = str_replace("[[FirstName]]", $list->fname, $processLinks);
       $htmlBodyFinal = str_replace("[[LastName]]", $list->lname, $htmlBodyFinal);
       $htmlBodyFinal = str_replace("[[Company]]", $list->company, $htmlBodyFinal);
       $htmlBodyFinal = str_replace("[[CID]]", $list->id, $htmlBodyFinal);

        // replace placeholers - text
       $textBodyFinal = str_replace("[[FirstName]]", $list->fname, $textBody);
       $textBodyFinal = str_replace("[[LastName]]", $list->lname, $textBodyFinal);
       $textBodyFinal = str_replace("[[Company]]", $list->company, $textBodyFinal);

        DB::connection('maildb')->table('email_queue')->insert([

          [
            'domainID' => $request->domainID,
            'repID' => $list->repID,
            'userID' => $request->userID,
            'customerID' => $request->customerID,
            'groupID' => $groupID,
            'templateID' => $request->templateID,
            'contactID' => $list->id,
            'messageID' => $emailSent->id,
            'fname' => $list->fname,
            'lname' => $list->lname,
            'company' => $list->company,
            'messageType' => $messageType,
            'emailAddress' => $list->email,
            'subject' => $request->subject,
            'emailBody' => $htmlBodyFinal,
            'textBody' => $textBodyFinal,
            'attachment' => $request->attachment,
            'created_at' => $dt
          ]
        ]);
        // good message - message queued
        $goodCount = $goodCount +1;

      } else {
        // bad message - did not queue
        $badCount = $badCount +1;

      } // end if mobile != ''

    } // end foreach $contactList

    // insert record into emailsSent
    $emailSent->totalQueue = $goodCount;
    $emailSent->totalBad = $badCount;
    $emailSent->save();

    $midQueue = DB::connection('maildb')->table('emailMIDQueue')->insert([
      [
        'mid' => $emailSent->id,
        'created_at' => $dt
      ]
    ]);

    $breadcrumb = ['email', 'send'];
      
    return view('customer.customerSendEmailDone')->with(['domain' => $domain, 'breadcrumb' => $breadcrumb, 'goodCount' => $goodCount, 'badCount' => $badCount, 'messageID' => $emailSent->id]);
    
  }     





/*
|----------------------------------------------------------------------------------------------
| Get Contact List for emails
|----------------------------------------------------------------------------------------------
|
*/

function getContactList($groupID, $domainID) {

  if ($groupID == 'all') {
    // get all contacts
    $contactList = contacts::select('id', 'domainID', 'fname', 'lname', 'email', 'companyName', 'title')
    ->where('domainID', '=', $domainID)
    ->get();

  } else {

    // get group member contacts
    $contactList = groupMember::Join('contacts', 'groupMembers.contactID', '=', 'contacts.id')
    ->select('contacts.id', 'contacts.domainID', 'contacts.fname', 'contacts.lname', 'contacts.email', 'contacts.companyName', 'contacts.title')
    ->where('groupMembers.groupID', '=', $groupID)
    ->get();

  } // end if group - get group members

  return $contactList;
} // end getContactList




/*
|----------------------------------------------------------------------------------------------
| Get Signature for emails - if no signature for email return ''
|----------------------------------------------------------------------------------------------
|
*/

function getSignature($signature, $uid) {

    if ( $signature == 'signature') {
      $signature = signatures::where('userID', '=', $uid)->get();
      if ( $signature->count() != 0 ) {$signature = $signature[0];} else {$signature = null;}
    } 

    return $signature;

} // end getSignature



/*
|----------------------------------------------------------------------------------------------
| Get Rep ID for emails - needed for emailsSents table
|----------------------------------------------------------------------------------------------
|
*/

function getRepID($groupID, $customerID) {

  if ($groupID == 'all') {
    
    $repID = contacts::select('repID')
    ->where('customerID', '=', $customerID)
    ->first();
    
    $repID = $repID->repID;
    
  } else {

    $repID = groupMember::Join('contacts', 'groupMembers.contactID', '=', 'contacts.id')
    ->select('contacts.repID')
    ->where('groupMembers.groupID', '=', $groupID)
    ->first();
    
    $repID = $repID->repID;

  } // end if group - get group members

  return $repID;

} // end getRepID




/*
|----------------------------------------------------------------------------------------------
| Parse HTML for images. Stores images to Amazon S3 for sending and inserts S3 URL. Returns updated HTML.
|----------------------------------------------------------------------------------------------
|
*/
 
function imgToS3($html, $domainID, $isTest) {

  $dom = HtmlDomParser::str_get_html($html);
  $images = $dom->find('img');
  foreach ($images as $image) {

    $fileInfo = pathinfo($image->src);
    $fileName = $fileInfo['basename'];
    $fileExt = $fileInfo['extension'];
    $filePath = str_replace(env('PUBLIC_ATTACHE_PATH') . $domainID, "", $fileInfo['dirname']);

    if ($filePath == '') {
      // root directory
      $fileHandle = '/' . env('CUSTOMER_ATTACHE_PATH') . '/' . $domainID . '/' . $fileName;
    } else {
      // sub directories
      $fileHandle = '/' . env('CUSTOMER_ATTACHE_PATH') . '/' . $domainID . $filePath . '/' . $fileName;
    }

    $fileMime = mime_content_type("../storage/app". $fileHandle);
    $fileHandle = Storage::get($fileHandle);
    $nameHash = md5($fileName . microtime());
    $nameHash = $nameHash . "." . $fileExt;

    // save to s3
    if ($isTest == 'yes') {
      $s3file = Storage::disk('s3')
        ->getDriver()
        ->put(('testImages/' . $nameHash), $fileHandle,  ['visibility' => 'public', 'ContentType' => $fileMime]
      );
    } else {
     $s3file = Storage::disk('s3')
        ->getDriver()
        ->put(('emailImages/' . $nameHash), $fileHandle,  ['visibility' => 'public', 'ContentType' => $fileMime]
      );
    }

    // set img src URL
    if ($isTest == 'yes') {
      $imgSource = env('S3_IMAGE_PATH_TEST') . $nameHash;
    } else {
      $imgSource = env('S3_IMAGE_PATH') . $nameHash;
    }


    $html = str_replace($image->src, $imgSource, $html);


  } // end foreach


return $html;

} // end imgToS3

/*
|----------------------------------------------------------------------------------------------
| Store trackable links for tracking
|----------------------------------------------------------------------------------------------
|
*/

function linkTracker($html, $domainID, $userID, $contactID, $mailID, $templateID) {

  $dom = HtmlDomParser::str_get_html($html);
  $links = $dom->find('a');

  foreach ($links as $link) {

    // do not track URL - skip processing
    if ( strpos(trim(strtolower($link->class)), 'donottrack') !== false || strpos(trim(strtolower($link->class)), 'do not track') !== false )  {
        continue;
    }

    $ld = new linkData();
    $ld->linkHash = md5($link->href . microtime());
    $ld->linkAddress = $link->href;
    $ld->domainID = $domainID;
    $ld->userID = $userID;
    $ld->contactID = $contactID;
    $ld->mailID = $mailID;
    $ld->templateID = $templateID;
    $ld->ipAddress = $_SERVER['REMOTE_ADDR'];
    $ld->save();


    // set href
    if ($mailID == 0) {
      // test message - contactID = 0
      $linkHref = 'http://' . $_SERVER['HTTP_HOST'] . '/links/' . $ld->linkHash . '/0';
    } else {
      // live message - insert contact ID placeholder
      $linkHref = 'http://' . $_SERVER['HTTP_HOST'] . '/links/' . $ld->linkHash . '/[[CID]]';
    }

    // $link->href = $linkHref;
    $html = str_replace($link->href, $linkHref, $html);

    } // end foreach

  return $html;

} // end linkTracker





/*
|----------------------------------------------------------------------------------------------
| REPLACE PLACEHOLDERS IN EMAIL BODY FUNCTIONS
|----------------------------------------------------------------------------------------------
|
*/
 
function replaceLinkPlaceholder($emailData, $placeholder, $website) {
  
  $sitename = str_replace($website, "http://", '');
  $weblink = 'a href="' . $website . '" alt="' . $sitename . '">' . $sitename . '</a>';
  $htmlLink = str_replace($emailData, $placeholder, $weblink);

  return $htmlLink;

} // end replaceLinkPlaceholder



  /*---------------------------------Send A Test Email Message--------------------------------------------*/   

  public function sendTestEmailMessage($domainID, $userID, $emailAddress, $subject, $htmlBody, $textBody, $attachment, $fname, $lname, $type) {

    $domainSiteConfig = domainSiteConfig::where('domainID', '=', $domainID)->first();
    $to['name'] = $fname . " " . $lname;
    $to['email'] = $emailAddress;
    $sender = [$domainSiteConfig->sendEmail => $domainSiteConfig->locationName];
    $bounceAddress = $domainSiteConfig->sendEmail;

    // create the email
    $transport = HWCTMailer::createOrderTransport($domainSiteConfig->sendEmailUserName, $domainSiteConfig->sendEmailPassword);
    $mailer = HWCTMailer::createMailer($transport);
    $message = HWCTMailer::createEmailMessage($subject, $sender, $bounceAddress, $htmlBody, $textBody, $type);
    $message = HWCTMailer::setBounceHeaderInfo($message, $domainID, $userID, "0", "0", "test");

    if (!is_null($attachment)) {
      HWCTMailer::addAttachment($message, $attachment);
    }

    $numSent = HWCTMailer::sendEmailMessage($mailer, $message, $to, $type );

    return $numSent;

  } // end sendTextMessage



}  // end EmailMessageController




