<?php

namespace App\Http\Controllers;

use Facades\App\hwct\SwiftMailer\HWCTMailer;
use Illuminate\Support\Facades\Storage;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Facades\App\hwct\ProcessOrders;
use Facades\App\hwct\PrintOrders;
use Illuminate\Http\Request;
use App\domainPaymentConfig;
use App\stripeTransactions;
use App\domainSiteConfig;
use App\stripeCustomer;
use App\ordersDetail;
use App\managerPIN;
use App\stripeCard;
use App\siteStyles;
use App\marketing;
use Carbon\Carbon;
use App\printers;
use App\cartData;
use App\usStates;
use App\contacts;
use App\orders;
use App\domain;



class stripeController extends Controller
{
    

/*---------------- Stripe - orderPayment  ----------------------------------*/

	public function orderPayment(Request $request) {

		// check sessionID - make sure order does not exist with same sessionID
		$orderCheck = orders::where('sessionID', '=', $request->sid)->first();
		if (!empty($orderCheck)) { 
			session()->flash('cardError', 'An order has already been processed for this session. Please logout to reset your cart and try again.');
			return redirect()->action('cartController@viewCart');
		}
		
		$userID = Auth::id();
		$domain = \Request::get('domain');
		$domainSiteConfig = domainSiteConfig::where('domainID', '=', $domain->id)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $domain->id)->first();
		$styles = siteStyles::where('domainID', '=', $domain->id)->first();
		$numOnly = CellNumberData::numbersOnly($request->mobile);

		// get data for marketing system
		$marketing = marketing::findOrFail($domain->parentDomainID);

		// update or create contact based on email and domainID
		$contact = contacts::updateOrCreate([
			'email' => $request->email,
			'tgmDomainID' => $domain->id
			], [
			'domainID' => $marketing->mktDomainID,
			'customerID' => $marketing->mktCustomerID,
			'userID' => $marketing->mktUserID,
			'repID' => $marketing->mktRepID,
			'fname' => $request->fname,
			'lname' => $request->lname,
			'tgmUserID' => $request->userID,
			'mobile' => $request->mobile,
			'unformattedMobile' => $numOnly,
			'address1' => $request->address1,
			'address2' => $request->address2,
			'city' => $request->city,
			'state' => $request->state,
			'zip' => $request->zipCode
		]);

		if (empty($contact->merchantCustomerID)) {
			
			// new merchant customer - create new customer
			\Stripe\Stripe::setApiKey($this->setAPIKey($domainPaymentConfig));
			
			// Create a stripe Customer, TGM Customer and TGM Card
			$stripeCustomer = $this->createCustomer($request->email, $request->fname, $request->lname, $contact->id, $request->stripeToken);
			$TGMCustomer = $this->createTGMCustomer($stripeCustomer, $domain['id'], $userID, $contact->id);
			$TGMCard = $this->createTGMCard($stripeCustomer->sources->data[0], $domain['id'], $userID, $contact->id);

			// update contact with stripeID
			$contact->merchantCustomerID = $TGMCustomer->stripeID;
			$contact->save();

		} else {

			// existing customer
			\Stripe\Stripe::setApiKey($this->setAPIKey($domainPaymentConfig));
			$token = \Stripe\Token::retrieve($request->stripeToken);
			
			// get existing card info
			$existingCard = stripeCard::select('fingerprint')->where([ ['domainID', '=', $domain['id']], ['contactID', '=', $contact->id], ['fingerprint', '=', $token->card->fingerprint] ])->first();

			if (is_null($existingCard) || $token->card->fingerprint != $existingCard->fingerprint) {
				// new card
				\Stripe\Stripe::setApiKey($this->setAPIKey($domainPaymentConfig));

				// save new card to customer
				$customer = \Stripe\Customer::retrieve($contact->merchantCustomerID);
				$customerCard = $customer->sources->create(array("source" => $request->stripeToken));
				$newCard = $this->createTGMCard($customerCard, $domain['id'], $userID, $contact->id);
				$customer->default_source = $newCard->stripeCardID;
				$customer->save();
			} // end if null or not the same card - use the card if same card

		}

		// charge the card
		$charge = $this->stripeCharge($domainPaymentConfig, $contact->merchantCustomerID, $request->total, $contact->id, $request->fname, $request->lname, $request->sid);

		// charge not approved
		if ($charge['status'] != "approved") {
			//delete the card
			$customer = \Stripe\Customer::retrieve($contact->merchantCustomerID);
			$customer->sources->retrieve($newCard->stripeCardID)->delete();
			$badCard = stripeCard::where('stripeCardID', '=', $newCard->stripeCardID)->delete();

			// return to cart - display message
			session()->flash('cardError', $charge['charge']);
			return redirect()->action('cartController@viewCart');
		} // end if not approved

		// charge approved - create transaction and order
		
		$sd = $charge['charge']; // stripe charge object
		if ($request->delivery == 'delivery') {
			// delivery
			$delivery = "deliver";
		} else {
			// pickup
			$delivery = "pickup";
		}

		// update contact info with approved info
		$contact->lastDate = Carbon::now()->toDateTimeString();
		$contact->lastAmount = $request->total;
		$contact->save();
		
		$stripeFee = $this->calcStripeFee($domainPaymentConfig->stripeTransactionPercent, $domainPaymentConfig->stripeTransactionFee, $request->total);

		// create new order
		$order = new orders;
			$order->domainID = $domain['id'];
			$order->ownerID = $domain['ownerID'];
			$order->contactID = $contact->id;
			$order->userID = $contact->userID;
			$order->sessionID = $request->sid;
			$order->fname = $contact->fname;
			$order->lname = $contact->lname;
			$order->unformattedMobile = $numOnly;
            $order->customerEmailAddress = $request->email;
			$order->customerMobile = $request->mobile;
			$order->address1 = $request->address1;
			$order->address2 = $request->address2;
			$order->city = $request->city;
			$order->state = $request->state;
			$order->zipCode = $request->zip;
			$order->subTotal = $request->subTotal;
			$order->discountedSubTotal = $request->subTotal - $request->orderDiscount;
			$order->discountAmount = $request->orderDiscount ?? 0;
			$order->discountPercent = $request->orderDiscountPercent ?? 0;
			$order->tax = $request->tax;
			$order->tip = $request->tip ?? 0;
			$order->deliveryCharge = $domainSiteConfig->deliveryCharge ?? 0;
			$order->transactionFee = $domainSiteConfig->transactionFee ?? 0;
			$order->total = $request->total;
            $order->ccType = $domainPaymentConfig->ccType;
			$order->ccTypeTransactionID = $sd->id;
            $order->ccTypeCustomerID = $sd->customer;
            $order->ccTypeCardID = $sd->source->id;
            $order->ccTypeFunding = $sd->source->funding;
            $order->ccTypeLast4 = $sd->source->last4;
            $order->ccTypeTransactionFee = $stripeFee;
            $order->paymentType = $sd->source->brand;
            $order->orderType = $delivery; // add delivery and in restaurant options
            $order->waiterID = 0; // add waiters
            $order->tableID = 0; // add tables
            $order->userAgent = $request->userAgent;
            $order->orderIP = \Request::ip();
            $order->restaurantEmailAddress = $domainSiteConfig->sendEmail;
			$order->orderInProcess = 1; // set order in process
			$order->orderRefundAmount = 0; // set refund to 0
			$order->timezone = $domainSiteConfig->timezone;
			if ($delivery == "deliver") {
				$order->deliveryLatitude = $request->deliveryLatitude;
				$order->deliveryLongitude = $request->deliveryLongitude;
				$order->deliveryFeet = $request->deliveryFeet;
			}
		$order->save();

		$details = cartData::where('sessionID', '=', $request->sid)->get();
		foreach ($details as $detail) {
			$od = new ordersDetail;
				$od->domainID = $detail->domainID;
				$od->ownerID = $detail->ownerID;
				$od->contactID = $contact->id;
				$od->sessionID = $detail->sessionID;
				$od->menuDataID = $detail->menuDataID;
				$od->menuCategoriesDataID = $detail->menuCategoriesDataID;
				$od->menuCategoriesDataSortOrder = $detail->menuCategoriesDataSortOrder;
				$od->menuDataSortOrder = $detail->menuDataSortOrder;
				$od->menuItem = $detail->menuItem;
				$od->menuDescription = $detail->menuDescription;
				$od->portion = $detail->portion;
				$od->printOrder = $detail->printOrder;
				$od->printReceipt = $detail->printReceipt;
				$od->price = $detail->price;
				$od->menuOptionsDataID = $detail->menuOptionsDataID;
				$od->menuAddOnsDataID = $detail->menuAddOnsDataID;
				$od->menuSidesDataID = $detail->menuSidesDataID;
				$od->instructions = $detail->instructions;
				$od->orderIP = $detail->orderIP;
				$od->tipAmount = $detail->tipAmount;
				$od->discountPercent = $request->orderDiscountPercent ?? 0;

			$od->save();
			cartData::destroy($detail->id);
		} // end foreach

		// save stripe response
		$st = new stripeTransactions;
		    $st->domainID = $domain['id'];
		    $st->ownerID = $domain['ownerID'];
            $st->userID = $userID;
            $st->contactID = $contact->id;
            $st->sessionID = $request->sid;
            $st->orderID = $order->id;
            $st->stripeChargeID = $sd->id;
            $st->stripeCustomerID = $sd->customer;
            $st->stripeCardID = $sd->source->id;
            $st->brand = $sd->source->brand;
            $st->last4 = $sd->source->last4;
            $st->fname = $order->fname;
            $st->lname = $order->lname;
            $st->subTotal = $order->subTotal * 100;
            $st->tax = $order->tax * 100;
            $st->tip = $order->tip * 100;
            $st->deliveryCharge = $order->deliveryCharge * 100;
            $st->total = $order->total * 100;
            $st->stripeResponse = $sd;
		$st->save();
			
		// print receipt letter and return pdf file path
		$order->receiptLetterPath = PrintOrders::receiptLetterPDF($contact, $order, $domainSiteConfig);
	
		// print receipt thermal and return pdf file path
		$order->receiptThermalPath = PrintOrders::receiptThermalPDF($contact, $order, $domainSiteConfig);
		
		// print order letter and return pdf file path - kitchen
		$order->orderLetterPath = PrintOrders::orderLetterPDF($contact, $order, $domainSiteConfig);

		// print  order thermal and return pdf file path - kitchen
		$order->orderThermalPath = PrintOrders::orderThermalPDF($contact, $order, $domainSiteConfig);

		// send customer email
		$sendMail = ProcessOrders::sendOrderEmail($contact, $order, $domainSiteConfig);
		$order->emailData = $sendMail['html'];
		
		// update order
		$order->save();	

		// print receipt
		if (!is_null($domainSiteConfig->printReceipt)) {
			// get printer info
			$printer = printers::find($domainSiteConfig->printReceipt);
			
			// print receipt
			if (!is_null($printer)) { $printReceipt = PrintOrders::printReceipt($printer, $order, $domainSiteConfig, 'new'); }

		} // end print receipt

		// print order (kitchen)
		if (!is_null($domainSiteConfig->printOrder)) {
			// get printer info
			$printer = printers::find($domainSiteConfig->printOrder);
			
			// print receipt
			if (!is_null($printer)) { $printOrder = PrintOrders::printOrder($printer, $order, $domainSiteConfig, 'new'); }

		} // end print order

		// Send Text Confirmation
		$textSent = HWCTMailer::sendOrderText($numOnly, $domainSiteConfig, $delivery);

		// Send Email to restaurant
		$emailSent = ProcessOrders::sendDomainEmail($contact, $order, $domainSiteConfig);

		session()->regenerate();
	    $message['orderAlert'] = $domainSiteConfig->orderAlert;
	    $message['orderID'] = $order->sessionID;
	    $message['numSent'] = $sendMail['numSent'];
		
	    return view('orders.viewOrder')->with([ 'html' => $order->emailData ]);


	} // end orderPayment


/*---------------- Stripe - setApiKey - live or test  ----------------------------------*/
	
	public function setAPIKey($domainPaymentConfig) {
		
		if ($domainPaymentConfig->mode != "live") {
			$key = decrypt($domainPaymentConfig->stripeTestSecretKey);
		} else {
			$key = decrypt($domainPaymentConfig->stripeLiveSecretKey);				
		}

		return $key;

	} // end setAPIKey




/*---------------- Stripe - createCustomer w/charge token  ----------------------------------*/
	
	public function createCustomer($email, $fname, $lname, $contactID, $token) {
		
		// Create a stripe Customer:
		$stripeCustomer = \Stripe\Customer::create(array(
		  	"email" => $email,
		  	"source" => $token,
  			"description" => "CID: " . $contactID . " / " . $fname . " " . $lname
		));

		return $stripeCustomer;

	} // end createCustomer


/*---------------- Stripe - updateCustomer w/charge token  ----------------------------------*/
	
	public function updateCustomer($email, $fname, $lname, $contactID, $token) {
		
		// Create a stripe Customer:
		$stripeCustomer = \Stripe\Customer::create(array(
		  	"email" => $email,
		  	"source" => $token,
  			"description" => "CID: " . $contactID . " / " . $fname . " " . $lname
		));

		return $stripeCustomer;

	} // end updateCustomer





/*---------------- Stripe - createTGMCustomer   ----------------------------------*/
	
	public function createTGMCustomer($stripeCustomer, $domainID, $userID, $contactID) {
		
		// Create a TGM Stripe Customer:
		$TGMCustomer = new stripeCustomer;
		
		$TGMCustomer->stripeID = $stripeCustomer->id;
		$TGMCustomer->domainID = $domainID;
		$TGMCustomer->contactID = $contactID;
		$TGMCustomer->userID = $userID;
		$TGMCustomer->object = $stripeCustomer->object;
		$TGMCustomer->accountBalance = $stripeCustomer->account_balance;
		$TGMCustomer->currency = "usd";
		$TGMCustomer->created = $stripeCustomer->created;
		$TGMCustomer->defaultSource = $stripeCustomer->default_source;
		$TGMCustomer->delinquent = $stripeCustomer->delinquent;
		$TGMCustomer->description = $stripeCustomer->description;
		$TGMCustomer->discount = $stripeCustomer->discount;
		$TGMCustomer->email = $stripeCustomer->email;
		$TGMCustomer->shipping = $stripeCustomer->shipping;
		
		$TGMCustomer->save();	

		return $TGMCustomer;

	} // end createTGMCustomer



/*---------------- Stripe - createTGMCard   ----------------------------------*/
	
	public function createTGMCard($cardData, $domainID, $userID, $contactID) {
		
		// Create a TGM Stripe Customer:
		$TGMCard = new stripeCard;
		
		$TGMCard->stripeCardID = $cardData->id;
		$TGMCard->stripeCustomerID = $cardData->customer;
		$TGMCard->domainID = $domainID;
		$TGMCard->contactID = $contactID;
		$TGMCard->userID = $userID;
		$TGMCard->object = $cardData->object;
		$TGMCard->address1 = $cardData->address_line1;
		$TGMCard->address1Check = $cardData->address_line1_check;
		$TGMCard->address2 = $cardData->address_line2;
		$TGMCard->city = $cardData->address_city;
		$TGMCard->state = $cardData->address_state;
		$TGMCard->country = $cardData->address_country;
		$TGMCard->zipCode = $cardData->address_zip;
		$TGMCard->zipCodeCheck = $cardData->address_zip_check;
		$TGMCard->brand = $cardData->brand;
		$TGMCard->brandCountry = $cardData->country;
		$TGMCard->cvcCheck = $cardData->cvc_check;
		$TGMCard->dynamicLast4 = $cardData->dynamic_last4;
		$TGMCard->expireMonth = $cardData->exp_month;
		$TGMCard->expireYear = $cardData->exp_year;
		$TGMCard->fingerprint = $cardData->fingerprint;
		$TGMCard->funding = $cardData->funding;
		$TGMCard->last4 = $cardData->last4;
		$TGMCard->name = $cardData->name;
		$TGMCard->tokenizationMethod = $cardData->tokenization_method;

		$TGMCard->save();	

		return $TGMCard;

	} // end createTGMCard


/*---------------- Stripe - Charge Card   ----------------------------------*/
	
	public function stripeCharge($domainPaymentConfig, $merchantCustomerID, $total, $userID, $fname, $lname, $sid) {
		
		\Stripe\Stripe::setApiKey($this->setAPIKey($domainPaymentConfig));
		try {	
			$charge = \Stripe\Charge::create(array(
			  "amount" => $total * 100,
			  "currency" => "usd",
			  "customer" => $merchantCustomerID,
			  "description" => "UID: " . $userID . " | " . $fname . " " . $lname,
			  "metadata" => array("session_id" => $sid),
			  "statement_descriptor" => $domainPaymentConfig->statementDescriptor,
			));

			$status = 'approved';
		} // end try


		catch(\Stripe\Error\Base $e) {
			$status = 'declined';
			$charge = 'Your Card Was Declined. Please try another card or contact the card issuer.';
			Log::info('Stripe Validation Failed For User: '. $userID . " | " . $e);
		} // end catch

		$data=[];
		$data['status'] = $status;
		$data['charge'] = $charge;
		
		return $data;

	} // end stripeCharge




/*---------------- Stripe - calculate Stripe Transaction Fees   ----------------------------------*/
	
	public function calcStripeFee($stripePercent, $stripeFixed, $total) {

		$variableFee = number_format($total * $stripePercent/100, 2);
		$fixedFee = number_format($stripeFixed, 2);
		$stripeFee = $variableFee + $fixedFee;

		return $stripeFee;

	} // end calcStripeFee



	/*---------------- Delete stripe card ----------------------------------*/

	protected function deletecard(Request $request) {

		$sc = stripeCard::where('stripeCardID', '=', $request->cardID)->first();
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $sc->domainID)->first();
		$contact = contacts::where('id', '=', $sc->contactID)->first();

		// delete stripe card
		\Stripe\Stripe::setApiKey($this->setAPIKey($domainPaymentConfig));

		$customer = \Stripe\Customer::retrieve($contact->merchantCustomerID);
		$customer->sources->retrieve($sc->stripeCardID)->delete();
		$badCard = stripeCard::where('stripeCardID', '=', $sc->stripeCardID)->delete();

		return "Your payment method was successfully deleted.";

	} // end deleteItem







/*---------------- Stripe - Refund Charge   ----------------------------------*/
	
	public function refundCharge(Request $request) {

		$order = orders::find($request->orderID);
		$domainPaymentConfig = domainPaymentConfig::where('domainID', '=', $order->domainID)->first();

		// manager PIN check
		$chk = managerPIN::where([['domainID', '=', $order->domainID], ['userID', '=', $request->userID], ['managerPIN', '=', $request->managerPIN]])->first();
		if (is_null($chk)) {
			$data=[];
			$data['status'] = 'error';
			$data['message'] = 'The Manager PIN was not correct. Please enter the correct PIN.';
			return $data;
		}

		\Stripe\Stripe::setApiKey($this->setAPIKey($domainPaymentConfig));
		try {	
			$refund = \Stripe\Refund::create(array(
			  "charge" => $order->ccTypeTransactionID,
			  "metadata" => array("orderID" => $order->id),
			  "amount" => $request->refundAmount * 100
			));

			$status = "success";
			$message = "The refund was successfully processed.";

		} // end try

		catch(\Stripe\Error\Base $e) {
			$status = 'error';
			$message = 'There was an error refunding the charge. Please try again or check with Stripe.';
			$refund=$e;
		} // end catch

		if ($status == "success") {
			$order->orderRefunded = 1;
			if ($order->total == $order->refundAmount) {
				$order->orderInProcess = 0;
			}
			$order->orderRefundID = $refund->id;
			$order->orderRefundAmount = $request->refundAmount;
			$order->save();
		}

		$data=[];
		$data['status'] = $status;
		$data['message'] = $message;
		$data['stripe'] = $refund->id;
		
		return $data;

	} // end refundCharge


} // end stripeController
