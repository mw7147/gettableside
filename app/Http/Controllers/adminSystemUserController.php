<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Facades\App\hwct\CellNumberData;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\registeredNumber;
use App\Http\Requests;
use App\siteStyles;
use App\usersData;
use App\interests;
use App\marketing;
use App\contacts;
use App\usStates;
use App\Billing;
use App\domain;
use App\Client;
use App\roles;
use App\User;
use DateTime;
use App\type;

class adminSystemUserController extends Controller
{


	/*-------------------- System User View  ----------------------------------------*/

    public function systemUserView(Request $request, $urlPrefix) {

    $breadcrumb = ['systemuser', 'view'];
    $domain = \Request::get('domain');
    $authLevel = \Request::get('authLevel');

    if ($authLevel >= 40) {
      
      $systemUsers = User::select('users.id', 'users.fname', 'users.lname', 'users.active', 'users.email', 'users.mobile', 'users.role_auth_level', 'domains.name as customer', 'roles.name as role')
        ->leftJoin('domains', 'users.domainID', '=', 'domains.id')
        ->leftJoin('roles', 'users.role_auth_level', '=', 'roles.auth_level')
        ->get();

    }

    if ($authLevel == 35) {
      
      $systemUsers = User::select('users.id', 'users.fname', 'users.lname', 'users.active',  'users.email', 'users.mobile', 'users.role_auth_level', 'domains.name as customer', 'roles.name as role')
        ->where([['users.domainID', '=', $domain->id], ['users.role_auth_level', '<=', $authLevel]])
        ->leftJoin('domains', 'users.domainID', '=', 'domains.id')
        ->leftJoin('roles', 'users.role_auth_level', '=', 'roles.auth_level')
        ->get();

    }

    if ($authLevel == 30) {
      
      $systemUsers = User::select('users.id', 'users.fname', 'users.lname', 'users.active',  'users.email', 'users.mobile', 'users.role_auth_level', 'domains.name as customer', 'roles.name as role')
        ->where([['users.domainID', '=', $domain->id], ['users.role_auth_level', '<=', $authLevel]])
        ->leftJoin('domains', 'users.domainID', '=', 'domains.id')
        ->leftJoin('roles', 'users.role_auth_level', '=', 'roles.auth_level')
        ->get();

    }


    return view('admin.adminSystemUserView')->with(['domain' => $domain, 'systemUsers' => $systemUsers, 'breadcrumb' => $breadcrumb, 'authLevel' => $authLevel]);
    
    } // end systemUserView


  /*-------------------- System User Delete  ----------------------------------------*/

  public function adminSystemUserDelete(Request $request, $role, $userID) {


    $authLevel = \Request::get('authLevel');
    $domain = \Request::get('domain');
    $admin= Auth::user();

    if ($admin->role_auth_level > 35) {
      // togo admin or staff
    $user = User::where('id', '=', $userID)->where('role_auth_level', '<=', $admin->role_auth_level)->first();
    $contact = contacts::where('tgmUserID', '=', $userID)->first();
    
    } else {
      // owner or manager - only allow from their domain
      $user = User::where('id', '=', $userID)->where('domainID', '=', $domain->id)->where('role_auth_level', '<=', $admin->role_auth_level)->first();
      $contact = contacts::where('tgmUserID', '=', $userID)->where('tgmDomainID', '=', $domain->id)->first();
    }
    
    
    $u = $user->delete();
    (is_null($contact)) ? $c = true : $c = $contact->delete();

    if ($u && $c) {
      $request->session()->flash('deleteSuccess', 'The SystemUser was successfully deleted.');
    } else {
      $request->session()->flash('deleteError', 'There was an error deleting the user information. Please try again.');
    }

    return back();
          
  } // end adminSystemUserDelete


   	/*-------------------- System Password Reset  ----------------------------------------*/

    public function adminPasswordReset(Request $request, $urlPrefix, $userID) {
      
      $domain = \Request::get('domain');
      $user = User::find($userID);
      $admin= Auth::user();

      if ($admin->role_auth_level < 40) {
        // stop owner from viewing others
        if ($user->domainID != $domain->id ) { abort(403); }
      }

      $breadcrumb = ['systemuser', 'view'];

    return view('admin.adminPasswordReset')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'userID'=> $userID ]);
    
    } // end systemUserView




    /*----------------- System Password Reset Save (do) --------------------------------------*/

    
    public function adminPasswordResetDo(Request $request, $urlPrefix, $userID) {

      $domain = \Request::get('domain');
      $user = User::find($userID);
      $admin= Auth::user();

      if ($admin->role_auth_level < 40) {
        // stop owner from viewing others
        if ($user->domainID != $domain->id ) { abort(403); }
      }

      //proceed
      $user->password = Hash::make($request->password1);
      $user->save();

      session()->flash('deleteSuccess', 'The password has been successfully updated.');

      return redirect()->route('userView');

    } // end adminPasswordResetDo
 

    /*----------------- System User Edit --------------------------------------*/

    
    public function adminSystemUserEdit($urlPrefix, $userID) {

      $domain = \Request::get('domain');
      $user = User::findOrFail($userID);
      $admin= Auth::user();

      if ($admin->role_auth_level < 40) {
        // stop owner from viewing others
        if ($user->domainID != $domain->id ) { abort(403); }
      }

      //proceed
      $breadcrumb = ['systemuser', 'view'];
      $authLevel = \Request::get('authLevel');
      $states = usStates::all();
      $roles = roles::where([['auth_level', '<=', $authLevel], ['auth_level', '>', 0]])->orderBy('auth_level', 'desc')->get();
      $usersData = usersData::where('usersID', '=', $userID)->first();

    return view('admin.adminSystemUserEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'systemUser'=> $user, 'states' => $states, 'roles' => $roles, 'usersData' => $usersData ]);

    } // end adminSystemUserEdit

/*----------------- System User Edit --------------------------------------*/

    
    public function adminSystemUserEditDo(Request $request, $urlPrefix, $userID) {



      $domain = \Request::get('domain');
      $user = User::find($userID);
      $admin= Auth::user();

      if ($admin->role_auth_level < 40) {
        // stop owner from viewing others
        if ($user->domainID != $domain->id ) { abort(403); }
      }


      //proceed
    $breadcrumb = ['systemuser', 'view'];
    $authLevel = \Request::get('authLevel');
    $states = usStates::all();
    $roles = roles::where([['auth_level', '<=', $authLevel], ['auth_level', '>', 0]])->orderBy('auth_level', 'desc')->get();
    (isset($request->mobile)) ? $numOnly = CellNumberData::numbersOnly($request->mobile) : $numOnly = null ;

      $user->role_auth_level = $request->role_auth_level;
      $user->active = $request->active;
      $user->domainID = $user->domainID ?? $domain->id;
      $user->parentDomainID = $domain->parentDomainID;
      $user->email = $request->email;
      $user->mobile = $request->mobile ?? null;
      $user->unformattedMobile = $numOnly;
      $user->fname = $request->fname;
      $user->lname = $request->lname;
      $user->zipCode = $request->zipCode ?? null;
      $user->countryCode = 'US';
    $u = $user->save();

    if ($user->role_auth_level < 20) {
      // create contact for new user
      // get data for marketing system
      $marketing = marketing::findOrFail($user->domainID);

      // set dates
      if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
      if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }
      // update or create contact based on email and domainID
      $contact = contacts::updateOrCreate([
        'email' => $request->email,
        'tgmDomainID' => $domain->id
        ], [
        'tgmUserID' => $user->id,
        'domainID' => $marketing->mktDomainID,
        'customerID' => $marketing->mktCustomerID,
        'userID' => $marketing->mktUserID,   
        'repID' => $marketing->mktRepID,
        'fname' => $request->fname,
        'lname' => $request->lname,
        'mobile' => $request->mobile ?? null,
        'unformattedMobile' => $numOnly,
        'zip' => $request->zipCode ?? null,
        'birthDay' => $bd,
        'anniversary' => $an,
      ]);
    
    } // end if role_auth_level

    if ($u) {
      $request->session()->flash('updateSuccess', 'Update Successful.');
    } else {
      $request->session()->flash('updateError', 'There was an error updating the user information. Please try again.');
    }

    return view('admin.adminSystemUserEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'systemUser'=> $user, 'states' => $states, 'roles' => $roles ]);

    } // end adminSystemUserEditDo




/*----------------- System User New --------------------------------------*/

    
    public function adminSystemUserNew(Request $request) {

    $authLevel = \Request::get('authLevel');
    $breadcrumb = ['systemuser', 'view'];
    $domain = \Request::get('domain');
    $states = usStates::all();
    $roles = roles::where([['auth_level', '<=', $authLevel], ['auth_level', '>', 0]])->orderBy('auth_level', 'desc')->get();
    $user = Auth::user();

    return view('admin.adminSystemUserNew')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'states' => $states, 'roles' => $roles, 'user' => $user ]);

    } // end adminSystemUserEdit




/*----------------- System User New Save (D0) --------------------------------------*/

    
    public function adminSystemUserNewDo(Request $request) {

    $authLevel = \Request::get('authLevel');
    $breadcrumb = ['systemuser', 'view'];
    $domain = \Request::get('domain');
    $states = usStates::all();
    $roles = roles::where([['auth_level', '<=', $authLevel], ['auth_level', '>', 0]])->orderBy('auth_level', 'desc')->get();
    $numOnly = CellNumberData::numbersOnly($request->mobile);

    $user = new User;
      $user->role_auth_level = $request->role_auth_level;
      $user->active = $request->active;
      $user->domainID = $request->domainID ?? $domain->id;
      $user->email = $request->email;
      $user->mobile = $request->mobile;
      $user->unformattedMobile = $numOnly;
      $user->fname = $request->fname;
      $user->lname = $request->lname;
      $user->zipCode = $request->zipCode;
      $user->countryCode = 'US';
    $u = $user->save();

    // set domain owner if new user for domain
    /*
    if ($request->role_auth_level == 35) {
      $domain = domain::find($request->domainID);
      if (is_null($domain->ownerID)) {
        $domain->ownerID = $user->id;
        $domain->save();
      } // end if null
    } // end if role_auth_level
    */
    
    if ($user->role_auth_level < 20) {
      // create contact for new user
      // get data for marketing system
      $marketing = marketing::findOrFail($domain->parentDomainID);

      // set dates
      if ($request->birthday == '') { $bd = null; } else { $bd = Carbon::parse($request->birthday)->toDateTimeString(); }
      if ($request->anniversary == '') { $an = null; } else { $an = Carbon::parse($request->anniversary)->toDateTimeString(); }
      // update or create contact based on email and domainID
      $contact = contacts::updateOrCreate([
        'email' => $request->email,
        'tgmDomainID' => $domain->id
        ], [
        'tgmUserID' => $user->id,
        'domainID' => $marketing->mktDomainID,
        'customerID' => $marketing->mktCustomerID,
        'userID' => $marketing->mktUserID,   
        'repID' => $marketing->mktRepID,
        'fname' => $request->fname,
        'lname' => $request->lname,
        'mobile' => $request->mobile,
        'unformattedMobile' => $numOnly,
        'zip' => $request->zipCode,
        'birthDay' => $bd,
        'anniversary' => $an,
      ]);
    
    } // end if role_auth_level

    if ($u) {
      $request->session()->flash('updateSuccess', 'The New Customer Was Created. Please set the password for the new customer.');
      $url = '/admin/systemuser/password/' . $user->id;
      return redirect($url);
    } else {
      $request->session()->flash('updateError', 'There was an error creating the user information. Please try again.');
      return redirect('/admin/systemuser/new')->withInput();
    }



   // return view('admin.adminSystemUserEdit')->with([ 'domain' => $domain, 'breadcrumb' => $breadcrumb , 'systemUser'=> $user, 'states' => $states, 'roles' => $roles, 'usersData' => $usersData ]);

    } // end adminSystemUserNewDo








/*----------------- Check User Email --------------------------------------*/
    
    public function checkuseremail(Request $request) {

      // check valid email
      $request->validate([ 'email' => 'email' ]);
      // valid email
      $check = User::where("email", '=', $request->email)->where('domainID', '=', $request->did)->first();
      if (is_null($check)) {
        // no account exists
        $message['status'] = "ok";
        $message['message'] = "Email is valid email address.";
        return json_encode($message);
      } else {
        // account exists
        $message['status'] = "error";
        $message['message'] = "An account with this email address (username) already exists. Please check your email and try again.";
        return json_encode($message);
      }

    } // end checkuseremail


/*----------------- Check User Domain ID --------------------------------------*/
    
    public function checkDID(Request $request) {

      $check = domain::find($request->did);
      if (!is_null($check)) {
        // no account exists
        $message['status'] = "ok";
        $message['message'] = "Valid customer.";
        return json_encode($message);
      } else {
        // account exists
        $message['status'] = "error";
        $message['message'] = "This is not a valid Customer ID. Please check your Customer ID and try again.";
        return json_encode($message);
      }

    } // end checkDID


} // end adminSystemUserController
