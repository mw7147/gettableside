<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class authNetDetail extends Model
{
    protected $table = 'authNetDetail';
    protected $guarded = [];
}
