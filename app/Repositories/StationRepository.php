<?php
namespace App\Repositories;

use Illuminate\Http\Request;

class StationRepository{

    public function __construct(){

    }
     
    public function stationTypeList(){
        $user = auth()->user();
        $endpoint = '/v1/printer/station/type/list';
        $type = 'POST';
        $postData = array('locale' => 'en');
        $userId = $user->id;
        $domainId = $user->parentdomainID;
        $accessToken = session()->get('access_token');
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
            "Authorization: Bearer $accessToken",
        );
        return json_decode($this->executeApi($endpoint, $type, $postData, $headerData));
    }

    public function stationTypeCreate(Request $request){
        $user = auth()->user();
        $endpoint = '/v1/printer/station/type';
        $type = 'POST';
        $postData = array(
            'locale' => 'en',
            'title' => $request->title,
            'description' => $request->description,
            'icon' => 'fa-fa-icon',
            'status' => '1'
        );
        $userId = $user->id;
        $domainId = $user->parentdomainID;
        $accessToken = session()->get('access_token');
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
            "Authorization: Bearer $accessToken",
        );
        return $this->executeApi($endpoint, $type, $postData, $headerData);
    }

    public function stationList(){
        $user = auth()->user();
        $endpoint = '/v1/printer/station/list';
        $type = 'POST';
        $postData = array('locale' => 'en');
        $userId = $user->id;
        $domainId = $user->parentdomainID;
        $accessToken = session()->get('access_token');
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
            "Authorization: Bearer $accessToken",
        );
        return json_decode($this->executeApi($endpoint, $type, $postData, $headerData));
    }

    public function stationCreate(Request $request){
        $user = auth()->user();
        $endpoint = '/v1/printer/station';
        $type = 'POST';
        $postData = array(
            'locale' => 'en',
            'status' => '1',
            'station_type' => $request->stationType,
            'title' => $request->title,
            'description' => $request->description,
            'icon' => 'fa-fa-icon',
            'printer_model' => $request->printerModel,
            'printer_ip' => $request->printerIp,
            'printer_mac' => $request->printerMac,
        );
        if($request->parent){
            $postData['parent_id'] = $request->parent;
        }
        $userId = $user->id;
        $domainId = $user->parentdomainID;
        $accessToken = session()->get('access_token');
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
            "Authorization: Bearer $accessToken",
        );
        return $this->executeApi($endpoint, $type, $postData, $headerData);
    }


    public function stationUpdate(Request $request, $id){
        $user = auth()->user();
        $endpoint = '/v1/printer/station/'.$id;
        $type = 'POST';
        $postData = array(
            'locale' => 'en',
            'status' => '1',
            'station_type' => $request->stationType,
            'title' => $request->title,
            'description' => $request->description,
            'icon' => 'fa-fa-icon',
            'printer_model' => $request->printerModel,
            'printer_ip' => $request->printerIp,
            'printer_mac' => $request->printerMac,
        );
        if($request->parent){
            $postData['parent_id'] = $request->parent;
        }
        $userId = $user->id;
        $domainId = $user->parentdomainID;
        $accessToken = session()->get('access_token');
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
            "Authorization: Bearer $accessToken",
        );
        return $this->executeApi($endpoint, $type, $postData, $headerData);
    }


    public function stationDelete(Request $request, $id){
        $user = auth()->user();
        $endpoint = '/v1/printer/station/'.$id;
        $type = 'DELETE';
        $postData = array(
            'locale' => 'en',
        );
        $userId = $user->id;
        $domainId = $user->parentdomainID;
        $accessToken = session()->get('access_token');
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
            "Authorization: Bearer $accessToken",
        );
        return $this->executeApi($endpoint, $type, $postData, $headerData);
    }

    public function userLogin(array $data){
        $endpoint = '/v1/login';
        $type = 'POST';
        $postData = array(
            'email' => $data['email'],
            'password' => $data['password'],
            'locale' => 'en'
        );
        $userId = $data['userId'];
        $domainId = $data['domainId'];
        $headerData = array(
            "X-Requested-With: XMLHttpRequest",
            "userid: $userId",
            "domainId: $domainId",
        );
        return json_decode($this->executeApi($endpoint, $type, $postData, $headerData));
    }
    
    private function executeApi($endpoint,$type,array $postData, array $headerData){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => env('STATION_API_URL','https://api.kds.prowealthadvisor.com').$endpoint,
        //CURLOPT_URL => 'http://printer.local/'.$endpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => $postData,
        CURLOPT_HTTPHEADER => $headerData,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }




}