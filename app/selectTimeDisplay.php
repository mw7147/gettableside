<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class selectTimeDisplay extends Model
{
    protected $table = 'selectTimeDisplay';
    protected $primaryKey = 'timeValue';
}
