<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/login', 'UsersController@userLogin')->name('login');
// Auth::routes();

Route::get('/app/dashboard', 'authLevelController@authLevel');

Route::get('/t/m/{emailsSentID}/{emailsSentDetailID}', 'TrackerController@emailTracker');
Route::get('/t/c/{campaignEventDetailsID}', 'TrackerController@campaignMailTracker');

Route::get('/order', 'ordersController@order')->name('order');
Route::get('/dine', 'ordersController@dineIn')->name('dineIn');
Route::post('/clearcart', 'cartController@clearCart');
Route::get('/iframe', 'ordersController@order');
Route::get('/services', 'HomeController@services');
Route::get('/about', 'HomeController@about');
Route::get('/contact', 'HomeController@contact');

Route::get('/userlogin', 'UsersController@userLogin');
Route::post('/userlogin', 'UsersController@userLoginDo');
Route::get('/userlogout', 'UsersController@userLogout');
Route::post('/viewcartlogin', 'UsersController@cartLogin');
Route::post('/orderviewcartlogin', 'UsersController@orderCartLogin');
Route::get('/userregister', 'UsersController@userRegister');
Route::post('/userregister', 'UsersController@userRegisterDo');
Route::post('/guestuserregister', 'UsersController@guestUserRegister');
Route::get('/{token}/password/reset', 'UsersController@userPasswordReset');
Route::post('/{token}/password/reset', 'UsersController@userPasswordResetDo');
Route::get('/forgot/password', 'UsersController@forgotPassword');
Route::post('/forgot/password', 'UsersController@forgotPasswordDo');

Route::get('/viewcart', 'cartController@viewCart');
Route::get('/dineincart', 'cartController@dineInCart');
Route::get('/viewemail/{ordersID}', 'cartController@viewEmail');
Route::get('/vieworder/{sessionID}', 'cartController@viewOrder');
Route::post('/addtip', 'cartController@addTip');
Route::post('/cardsecure/tokenize/{type}', 'cartController@cardsecureTokenize');

Route::post('/deleteusercard', 'UsersController@deleteusercard');

Route::get('/add-station-menu', 'stationController@addMenuItem');
Route::get('/remove-station-menu', 'stationController@removeMenuItem');
Route::post('/check/cart-data/status', 'cartController@checkCartDataStatus');

// Payment Processing Routes based on payment processor
Route::group(['prefix' => 'orderpayment'], function () {
	Route::post('/onreceipt', 'cashController@orderPayment');
	Route::post('/stripe', 'stripeController@orderPayment');
	Route::post('/cardconnect', 'cardConnectController@orderPayment');
	Route::post('/authnet', 'authNetController@orderPayment');
});


// Link Tracking Routes
Route::group(['prefix' => 'links'], function () {
	
	Route::get('/{linkHash}/{contactID}', 'linkTrackerController@linkTrackRedirect');
	Route::get('/campaigns/{linkHash}/{contactID}', 'linkTrackerController@campaignLinkTrackRedirect');

});

// updated to 5.4+ syntax
Route::middleware('auth_level:10')->prefix('customer')->group(function () {

    Route::get('/dashboard', 'customerController@dashboard');
	Route::post('/dashboard', 'customerController@updateMyInformation');

    Route::get('/orders', 'customerController@viewOrders');
	Route::get('/view/orders/{orderID}', 'customerController@viewOrderHTML');

	Route::get('/password/reset', 'customerController@passwordReset');
	Route::post('/password/reset', 'customerController@passwordResetDo');

	Route::get('/viewpayments', 'customerController@viewPayments');
});

// updated to 5.4+ syntax
Route::middleware('auth_level:20')->prefix('staff')->group(function () {

	Route::get('/dashboard', 'staffController@dashboard');
	
	Route::get('/orderadmin/view', 'orderAdminController@locationView');
	Route::post('/orderadmin/view', 'orderAdminController@locationView');
	Route::get('/orderadmin/ordersview', 'orderAdminController@ordersView');
	Route::get('/orderadmin/pendingordersview', 'orderAdminController@pendingOrdersView');
	Route::get('/vieworder/{orderID}', 'adminController@viewOrder');
	Route::get('/vieworderonly/{orderID}', 'adminController@viewOrderOnly');	

});

// updated to 5.4+ syntax
Route::middleware('auth_level:20')->prefix('contacts')->group(function () {	
	
	Route::get('/{urlPrefix}/view', 'contactsController@ownerView');
	Route::get('/{urlPrefix}/new', 'contactsController@newContact');
	Route::post('/{urlPrefix}/new', 'contactsController@newContactSave');
	Route::get('/{urlPrefix}/delete/{contactid}', 'contactsController@contactDelete');
	Route::get('/{urlPrefix}/edit/{contactid}', 'contactsController@contactEdit')->name('contactEdit');
	Route::post('/{urlPrefix}/edit/{contactid}', 'contactsController@contactEditSave');		

});

// updated to 5.4+ syntax
Route::middleware('auth_level:30')->prefix('manager')->group(function () {

 	Route::get('/dashboard', 'managerController@dashboard');

	Route::get('/orderadmin/view', 'orderAdminController@locationView');
	Route::post('/orderadmin/view', 'orderAdminController@locationView');
	Route::get('/orderadmin/ordersview', 'orderAdminController@ordersView');
	Route::get('/orderadmin/pendingordersview', 'orderAdminController@pendingOrdersView');
	Route::get('/vieworder/{orderID}', 'adminController@viewOrder');
	Route::get('/vieworderonly/{orderID}', 'adminController@viewOrderOnly');

    // Route::get('/quicktext', 'textController@quickText');
	// Route::post('/quicktextconfirm', 'textController@quickTextConfirm');
    Route::get('/quickmail', 'emailController@quickMail');
	Route::post('/quickmailconfirm', 'emailController@quickMailConfirm');


	Route::get('/contacts/view', 'contactsController@ownerView');
	Route::get('/contacts/new', 'contactsController@newContact');
	Route::post('/contacts/new', 'contactsController@newContactSave');
	Route::get('/contacts/delete/{contactid}', 'contactsController@contactDelete');
	Route::get('/contacts/edit/{contactid}', 'contactsController@contactEdit');
	Route::post('/contacts/edit/{contactid}', 'contactsController@contactEditSave');	

});


// updated to 5.4+ syntax
Route::middleware('auth_level:30')->prefix('groups')->group(function () {	
	
	//Route::get('/{urlPrefix}/view', 'groupsController@ownerView');
	Route::get('/{urlPrefix}/view', 'groupsController@view');
	Route::post('/{urlPrefix}/view', 'groupsController@newGroup');
	Route::get('/{urlPrefix}/delete/{groupID}', 'groupsController@deleteGroup');
	Route::post('/{urlPrefix}/editname', 'groupsController@editName');
	Route::get('/{urlPrefix}/addmembers/{groupid}', 'groupsController@addMembers');
	Route::get('/{urlPrefix}/viewmembers/{groupid}', 'groupsController@viewMembers');
	Route::get('/{urlPrefix}/delete/{groupid}/{contactid}', 'groupsController@deleteMember');
	Route::get('/{urlPrefix}/duplicate/{groupid}', 'groupsController@duplicateGroup');

});


// images - updated to 5.4+ syntax
Route::middleware('auth_level:30')->prefix('attache')->group(function () {
	
	// Route::get('/view/{wildcard}', 'ImageController@customerView')->where('wildcard', '.+');

	Route::get('/view/{dir}', 'ImageController@customerView');
	Route::post('/new/folder', 'ImageController@customerNewImageFolder');
	Route::post('/new/file', 'ImageController@newfileUpload');
	Route::get('/delete/{dir}', 'ImageController@folderDelete');
	Route::get('/deletefile/{homedir}/{filename}', 'ImageController@fileDelete');
	Route::post('/createlink', 'ImageController@createLink');
	Route::get('/ckeditor/browse', 'ImageController@ckEditorBrowser');

	
});
	
	
	
// Reports
Route::middleware('auth_level:30')->prefix('reports')->group(function () {

	Route::get('/{urlPrefix}/dashboard', 'reportsController@dashboard');

	Route::get('/{urlPrefix}/salesreport', 'reportsController@salesReport');
	Route::post('/{urlPrefix}/salesreport', 'reportsController@salesReport');	
	
	Route::get('/{urlPrefix}/salesreportdaily/{orderDate}', 'reportsController@salesReportDaily');

	Route::get('/{urlPrefix}/dailysalessummary', 'reportsController@dailySalesSummary');
	Route::post('/{urlPrefix}/dailysalessummary', 'reportsController@dailySalesSummary');	

	Route::get('/{urlPrefix}/datedetail/{domainID}/{dayofyear}', 'reportsController@salesReportDetail');	
	Route::get('/{urlPrefix}/orderdetail/{orderID}', 'reportsController@orderReportDetail');	
	Route::get('/{urlPrefix}/viewpdf/{orderID}', 'reportsController@reportViewPDF');	

	Route::get('/{urlPrefix}/suspension', 'reportsController@suspensionReport');

	Route::get('/{urlPrefix}/item-aggregate', 'reportsController@itemAggregate');
	Route::post('/{urlPrefix}/item-aggregate', 'reportsController@itemAggregate');	
	Route::get('/{urlPrefix}/category-aggregate', 'reportsController@categoryAggregate');	
	Route::post('/{urlPrefix}/category-aggregate', 'reportsController@categoryAggregate');	

	Route::get('/{urlPrefix}/delivery-details', 'reportsController@deliveryDetails');
	Route::post('/{urlPrefix}/delivery-details', 'reportsController@deliveryDetails');	
	Route::get('/{urlPrefix}/pickup-details', 'reportsController@pickupDetails');	
	Route::post('/{urlPrefix}/pickup-details', 'reportsController@pickupDetails');
	Route::get('/{urlPrefix}/receipt/{id}', 'reportsController@viewReceipt');	

	Route::get('/{urlPrefix}/menuitemsales', 'reportsController@menuItemSales');	
	Route::post('/{urlPrefix}/menuitemsales', 'reportsController@menuItemSales');	

	
});


// tags
Route::middleware('auth_level:30')->prefix('tags')->group(function () {
	Route::get('/{urlPrefix}/tag-manager', 'tagController@viewTags')->name('tags');	
	Route::get('/{urlPrefix}/new', 'tagController@newTag');	
	Route::post('/{urlPrefix}/new', 'tagController@newTagDo');
	Route::get('/{urlPrefix}/delete/{id}', 'tagController@deleteTag');
	Route::get('/{urlPrefix}/edit/{id}', 'tagController@editTag');
	Route::post('/{urlPrefix}/edit/{id}', 'tagController@editTagDo');
});



	// tags
	Route::middleware('auth_level:30')->prefix('tables')->group(function () {
		Route::get('/{urlPrefix}/table-manager', 'tableController@viewTables')->name('tables');	
		Route::get('/{urlPrefix}/new', 'tableController@newTable');	
		Route::post('/{urlPrefix}/new', 'tableController@newTableDo');
		Route::get('/{urlPrefix}/delete/{id}', 'tableController@deleteTable');
		Route::get('/{urlPrefix}/edit/{id}', 'tableController@editTable');
		Route::post('/{urlPrefix}/edit/{id}', 'tableController@editTableDo');
		Route::get('/{urlPrefix}/qrcode/{id}', 'tableController@qrcode');
		Route::post('/{urlPrefix}/qrcode/{id}', 'tableController@qrcodeDo');
	});
	
	
	
// settings
Route::middleware('auth_level:30')->prefix('settings')->group(function () {
	
		Route::get('/{urlPrefix}/dashboard', 'settingsController@dashboard')->name('settingsDashboard');
		
		Route::get('/{urlPrefix}/location', 'settingsController@location');
		Route::post('/{urlPrefix}/location', 'settingsController@locationDo');
		
		Route::get('/{urlPrefix}/system', 'settingsController@system');
		Route::post('/{urlPrefix}/system', 'settingsController@systemDo');

		Route::get('/{urlPrefix}/payment', 'settingsController@payment');
		Route::post('/{urlPrefix}/payment', 'settingsController@paymentDo');		

		Route::get('/{urlPrefix}/managerpin', 'settingsController@managerPIN');
		Route::post('/{urlPrefix}/managerpin', 'settingsController@managerPINDo');		

		Route::get('/{urlPrefix}/signature', 'settingsController@signature');
		Route::post('/{urlPrefix}/signature', 'settingsController@signatureDo');
		
		Route::get('/{urlPrefix}/myinfo', 'settingsController@myInfo');
		Route::post('/{urlPrefix}/myinfo', 'settingsController@myInfoDo');
		
		Route::get('/{urlPrefix}/password', 'settingsController@resetPassword');
		Route::post('/{urlPrefix}/password', 'settingsController@resetPasswordDo');

		Route::get('/{urlPrefix}/suspend/one', 'settingsController@suspendOneHour');
		Route::get('/{urlPrefix}/suspend/settings', 'settingsController@suspendSettings');
		Route::post('/{urlPrefix}/suspend/settings', 'settingsController@suspendSettingsDo');
	
	});
	
	

	
// menu
Route::middleware('auth_level:30')->prefix('menu')->group(function () {
	
	Route::get('/{urlPrefix}/dashboard', 'menuController@dashboard');
	
	Route::get('/{urlPrefix}/menu/list', 'menuController@menuList');
	Route::get('/{urlPrefix}/edit/{menuID}', 'menuController@menuEdit');
	Route::post('/{urlPrefix}/edit/{menuID}', 'menuController@menuEditSave');
	Route::get('/{urlPrefix}/addnew', 'menuController@menuAddNew');
	Route::post('/{urlPrefix}/addnew', 'menuController@menuAddNewSave');
	Route::get('/{urlPrefix}/delete/{menuID}', 'menuController@menuDelete');
	
	Route::get('/{urlPrefix}/menuDetails/{menuID}', 'menuController@menuDetails');
	Route::get('/{urlPrefix}/menuData', 'menuController@menuData');
	
	Route::get('/{urlPrefix}/menu/itemlist', 'menuController@menuItem');
	Route::get('/{urlPrefix}/items/{menuID}', 'menuController@menuItemsEdit')->name('menuItems');
	
	Route::get('/{urlPrefix}/itemdetail/{itemID}', 'menuController@menuItemsDetailEdit');
	Route::post('/{urlPrefix}/itemdetail/{itemID}', 'menuController@menuItemsDetailEditSave');
	Route::get('/{urlPrefix}/itemdetail/delete/{itemID}', 'menuController@menuItemsDetailDelete');


	Route::get('/{urlPrefix}/additem/{menuID}', 'menuController@menuAddItem');
	Route::post('/{urlPrefix}/additem/{menuID}', 'menuController@menuAddItemSave');

	Route::get('/{urlPrefix}/categories/list', 'menuController@menuCategories');
	Route::get('/{urlPrefix}/categories/addnew', 'menuController@menuCategoriesAddNew');
	Route::post('/{urlPrefix}/categories/addnew', 'menuController@menuCategoriesAddNewSave');
	Route::get('/{urlPrefix}/categories/edit/{categoryID}', 'menuController@menuCategoriesEdit');
	Route::post('/{urlPrefix}/categories/edit/{categoryID}', 'menuController@menuCategoriesEditSave');
	Route::get('/{urlPrefix}/categories/delete/{categoryID}', 'menuController@menuCategoriesDelete');
	
	Route::get('/{urlPrefix}/categories/details/view/{categoryID}', 'menuController@menuCategoriesDetails');
	Route::get('/{urlPrefix}/categories/details/delete/{categoryDataID}', 'menuController@menuCategoriesDetailsDelete');	
	Route::get('/{urlPrefix}/categories/details/addnew/{categoryID}', 'menuController@menuCategoriesDetailsAddNew');
	Route::post('/{urlPrefix}/categories/details/addnew/{categoryID}', 'menuController@menuCategoriesDetailsAddNewSave');
	Route::get('/{urlPrefix}/categories/details/edit/{categoryID}', 'menuController@menuCategoriesDetailsEdit');
	Route::post('/{urlPrefix}/categories/details/edit/{categoryID}', 'menuController@menuCategoriesDetailsEditSave');
	
	Route::get('/{urlPrefix}/sides/list', 'menuController@menuSides');
	Route::get('/{urlPrefix}/sides/addnew', 'menuController@menuSidesAddNew');
	Route::post('/{urlPrefix}/sides/addnew', 'menuController@menuSidesAddNewSave');
	Route::get('/{urlPrefix}/sides/edit/{sideID}', 'menuController@menuSidesEdit');
	Route::post('/{urlPrefix}/sides/edit/{sideID}', 'menuController@menuSidesEditSave');
	Route::get('/{urlPrefix}/sides/delete/{sideID}', 'menuController@menuSidesDelete');

	Route::get('/{urlPrefix}/sides/details/view/{sideID}', 'menuController@menuSidesDetail');
	Route::get('/{urlPrefix}/sides/details/edit/{sideID}', 'menuController@menuSidesDetailEdit');
	Route::post('/{urlPrefix}/sides/details/edit/{sideID}', 'menuController@menuSidesDetailEditSave');
	Route::get('/{urlPrefix}/sides/details/addnew/{sideID}', 'menuController@menuSidesDetailsAddNew');
	Route::post('/{urlPrefix}/sides/details/addnew/{sideID}', 'menuController@menuSidesDetailsAddNewSave');
	Route::get('/{urlPrefix}/sides/details/delete/{sideDataID}', 'menuController@menuSidesDetailDelete');
	
	Route::get('/{urlPrefix}/options/list', 'menuController@menuOptions');
	Route::get('/{urlPrefix}/options/addnew', 'menuController@menuOptionsAddNew');
	Route::post('/{urlPrefix}/options/addnew', 'menuController@menuOptionsAddNewSave');
	Route::get('/{urlPrefix}/options/edit/{categoryID}', 'menuController@menuOptionsEdit');
	Route::post('/{urlPrefix}/options/edit/{categoryID}', 'menuController@menuOptionsEditSave');
	Route::get('/{urlPrefix}/options/delete/{categoryID}', 'menuController@menuOptionsDelete');

	Route::get('/{urlPrefix}/options/details/view/{optionID}', 'menuController@menuOptionsDetail');
	Route::get('/{urlPrefix}/options/details/edit/{optionID}', 'menuController@menuOptionsDetailEdit');
	Route::post('/{urlPrefix}/options/details/edit/{optionID}', 'menuController@menuOptionsDetailEditSave');
	Route::get('/{urlPrefix}/options/details/addnew/{optionID}', 'menuController@menuOptionsDetailsAddNew');
	Route::post('/{urlPrefix}/options/details/addnew/{optionID}', 'menuController@menuOptionsDetailsAddNewSave');
	Route::get('/{urlPrefix}/options/details/delete/{optionDataID}', 'menuController@menuOptionsDetailDelete');
	
	Route::get('/{urlPrefix}/addons/list', 'menuController@menuAddOns');
	Route::get('/{urlPrefix}/addons/addnew', 'menuController@menuAddOnsAddNew');
	Route::post('/{urlPrefix}/addons/addnew', 'menuController@menuAddOnsAddNewSave');
	Route::get('/{urlPrefix}/addons/edit/{addOnID}', 'menuController@menuAddOnsEdit');
	Route::post('/{urlPrefix}/addons/edit/{addOnID}', 'menuController@menuAddOnsEditSave');
	Route::get('/{urlPrefix}/addons/delete/{addOnID}', 'menuController@menuAddOnsDelete');
	
	Route::get('/{urlPrefix}/addons/details/view/{addOnID}', 'menuController@menuAddOnsDetail');
	Route::get('/{urlPrefix}/addons/details/edit/{addOnID}', 'menuController@menuAddOnsDetailEdit');
	Route::post('/{urlPrefix}/addons/details/edit/{addOnID}', 'menuController@menuAddOnsDetailEditSave');
	Route::get('/{urlPrefix}/addons/details/addnew/{addOnID}', 'menuController@menuAddOnsDetailsAddNew');
	Route::post('/{urlPrefix}/addons/details/addnew/{addOnID}', 'menuController@menuAddOnsDetailsAddNewSave');
	Route::get('/{urlPrefix}/addons/details/delete/{addOnDataID}', 'menuController@menuAddOnsDetailDelete');

});
	


// updated to 5.4+ syntax
Route::middleware('auth_level:30')->prefix('menuimages')->group(function () {
	
	Route::get('/{urlPrefix}/imagelist/{menuDataID}', 'menuImageController@imageList');
	
});

// text functions
Route::middleware('auth_level:30')->prefix('text')->group(function () {

	Route::get('/{urlPrefix}/quicktext', 'textController@quickText');
	Route::post('/{urlPrefix}/quicktextconfirm', 'textController@quickTextConfirm');
	Route::get('/{urlPrefix}/templates/view', 'textTemplateController@templateView')->name('textTemplateView');
	Route::get('/{urlPrefix}/templates/new', 'textTemplateController@templateNew');
	Route::post('/{urlPrefix}/templates/new', 'textTemplateController@templateNewSave');
	Route::get('/{urlPrefix}/templates/edit/{id}', 'textTemplateController@textTemplateEdit')->name('textTemplateEdit');
	Route::post('/{urlPrefix}/templates/edit/{id}', 'textTemplateController@textTemplateEditSave');
	Route::get('/{urlPrefix}/templates/copy/{id}', 'textTemplateController@textTemplateCopy');
	Route::get('/{urlPrefix}/templates/delete/{id}', 'textTemplateController@textTemplateDelete');
	Route::get('/{urlPrefix}/templates/send', 'textTemplateController@textTemplateSend');
	Route::get('/{urlPrefix}/templates/send/{id}', 'textTemplateController@textTemplateSendDo');
	Route::post('/{urlPrefix}/templates/queue', 'textTemplateController@textTemplateQueue');

});




// updated to 5.4+ syntax
Route::middleware('auth_level:35')->prefix('owner')->group(function () {

    Route::get('/dashboard', 'ownerController@dashboard');

  	// Route::get('/quicktext', 'textController@quickText');
	// Route::post('/quicktextconfirm', 'textController@quickTextConfirm');

	Route::get('/quickmail', 'emailController@quickMail');
	Route::post('/quickmailconfirm', 'emailController@quickMailConfirm');

	Route::get('/contacts/view', 'contactsController@ownerView');
	Route::get('/contacts/new', 'contactsController@newContact');
	Route::post('/contacts/new', 'contactsController@newContactSave');

	Route::get('/groups/view', 'groupsController@ownerView');
	Route::post('/groups/view', 'groupsController@newGroup');
	Route::get('/groups/delete/{id}', 'groupsController@deleteGroup');
	Route::post('/groups/editname', 'groupsController@editName');

	Route::get('/orderadmin/view', 'orderAdminController@locationView');
	Route::post('/orderadmin/view', 'orderAdminController@locationView');
	Route::get('/orderadmin/ordersview', 'orderAdminController@ordersView');
	Route::get('/orderadmin/pendingordersview', 'orderAdminController@pendingOrdersView');
	Route::get('/vieworder/{orderID}', 'adminController@viewOrder');
	Route::get('/vieworderonly/{orderID}', 'adminController@viewOrderOnly');
	Route::get('/station/type/list', 'stationController@stationType');
	Route::post('/station/type/create', 'stationController@storeStationType');
	Route::get('/station/list', 'stationController@stationList');
	Route::post('/station/create', 'stationController@storeStation');
	Route::post('/station/delete/{id}', 'stationController@deleteStation');
	Route::post('/station/update/{id}', 'stationController@updateStation');
	Route::get('/station/locations', 'stationController@stationLocation');
	Route::get('/station/menu', 'stationController@stationMenu');
});


// updated to 5.4+ syntax
// manage users
Route::middleware('auth_level:30')->group(function () {

	Route::get('/{urlPrefix}/systemuser/view', 'adminSystemUserController@systemUserView');
	Route::get('/{urlPrefix}/systemuser/password/{userID}', 'adminSystemUserController@adminPasswordReset');
	Route::post('/{urlPrefix}/systemuser/password/{userID}', 'adminSystemUserController@adminPasswordResetDo');
	Route::get('/{urlPrefix}/systemuser/edit/{userID}', 'adminSystemUserController@adminSystemUserEdit');
	Route::post('/{urlPrefix}/systemuser/edit/{userID}', 'adminSystemUserController@adminSystemUserEditDo');
	Route::get('/{urlPrefix}/systemuser/new', 'adminSystemUserController@adminSystemUserNew');
	Route::post('/{urlPrefix}/systemuser/new', 'adminSystemUserController@adminSystemUserNewDo');
	Route::get('/{urlPrefix}/systemuser/delete/{userID}', 'adminSystemUserController@adminSystemUserDelete');
	

});



// updated to 5.4+ syntax
Route::middleware('auth_level:40')->prefix('hwct')->group(function () {

  //  Route::get('/mysignature', 'SignatureController@viewSignature');
});

// updated to 5.4+ syntax
Route::middleware('auth_level:50')->prefix('admin')->group(function () {

	Route::get('/dashboard', 'adminController@dashboard');
	
	Route::get('/restore', 'reprintController@restore')->name('restoreReceipt');
	Route::post('/restore', 'reprintController@restoreDo');

	Route::get('/customer/view', 'adminController@customerView')->name('customerView');	;
	Route::get('/customer/create', 'adminController@customerCreate');
	Route::post('/customer/create', 'adminController@customerCreateDo');
	Route::get('/customer/new', 'adminController@customerNew');
	Route::post('/customer/new', 'adminController@customerCreateNew');
	Route::get('/customer/edit/{customerID}', 'adminController@customerEdit');
	Route::post('/customer/edit/{customerID}', 'adminController@customerEditSave');
	Route::get('/customer/style/{domainID}', 'adminController@adminStyle');
	Route::post('/customer/style/{domainID}', 'adminController@adminStyleDo');
	Route::get('/customer/siteconfig/{domainID}', 'adminController@adminSiteConfig');
	Route::post('/customer/siteconfig/{domainID}', 'adminController@adminSiteConfigDo');
	Route::get('/customer/paymentconfig/{domainID}', 'adminController@adminPaymentConfig');
	Route::post('/customer/paymentconfig/{domainID}', 'adminController@adminPaymentConfigDo');
	Route::get('/customer/delete/{domainID}', 'adminController@customerDelete');

	Route::get('/systemuser/view', 'adminSystemUserController@systemUserView')->name('userView');
	Route::get('/systemuser/password/{userID}', 'adminSystemUserController@adminPasswordReset');
	Route::post('/systemuser/password/{userID}', 'adminSystemUserController@adminPasswordResetDo');
	Route::get('/systemuser/edit/{userID}', 'adminSystemUserController@adminSystemUserEdit');
	Route::post('/systemuser/edit/{userID}', 'adminSystemUserController@adminSystemUserEditDo');
	Route::get('/systemuser/new', 'adminSystemUserController@adminSystemUserNew');
	Route::post('/systemuser/new', 'adminSystemUserController@adminSystemUserNewDo');
	Route::get('/systemuser/delete/{userID}', 'adminSystemUserController@adminSystemUserDelete');
	
	Route::get('/orderadmin/view', 'orderAdminController@locationView');
	Route::post('/orderadmin/view', 'orderAdminController@locationView');
	Route::get('/orderadmin/ordersview', 'orderAdminController@ordersView');
	Route::get('/orderadmin/pendingordersview', 'orderAdminController@pendingOrdersView');
	Route::get('/vieworder/{orderID}', 'adminController@viewOrder');
	Route::get('/vieworderonly/{orderID}', 'adminController@viewOrderOnly');

	Route::get('/sendemail/view', 'emailAdminController@sendView')->name('sendView');
	Route::get('/sendemail/edit/{emailID}', 'emailAdminController@sendEdit');
	Route::post('/sendemail/edit/{emailID}', 'emailAdminController@sendEditDo');
	Route::get('/sendemail/password/{mailtype}/{emailID}', 'emailAdminController@passwordReset');
	Route::post('/sendemail/password/{mailtype}/{emailID}', 'emailAdminController@passwordResetDo');
	Route::get('/sendemail/sendnew', 'emailAdminController@sendNew');
	Route::post('/sendemail/sendnew', 'emailAdminController@sendNewDo');
	Route::get('/sendemail/delete/{emailID}', 'emailAdminController@sendDelete');
	
	Route::get('/receiveemail/view', 'emailAdminController@receiveView')->name('receiveView');
	Route::get('/receiveemail/edit/{emailID}', 'emailAdminController@receiveEdit');
	Route::post('/receiveemail/edit/{emailID}', 'emailAdminController@receiveEditDo');
	Route::get('/receiveemail/password/{mailtype}/{emailID}', 'emailAdminController@passwordReset');
	Route::post('/receiveemail/password/{mailtype}/{emailID}', 'emailAdminController@passwordResetDo');
	Route::get('/receiveemail/receivenew', 'emailAdminController@receiveNew');
	Route::post('/receiveemail/receivenew', 'emailAdminController@receiveNewDo');
	Route::get('/receiveemail/delete/{emailID}', 'emailAdminController@receiveDelete');
});

Route::view('/viewordersonly', 'orderAdmin.viewOrdersOnly');
Route::view('/newordersonly', 'orderAdmin.newOrdersOnly');
Route::view('/stationtype', 'station.stationType');
Route::view('/stationlocation', 'station.stationLocation');
Route::view('/stationlist', 'station.stationList');

Route::get('social/{driver}', 'SocialiteController@socialLogin');
Route::get('social/{driver}/callback', 'SocialiteController@userLogin');

Route::get('order-tokenize-payment', 'cardConnectController@orderTokenizePayment');
Route::post('tokenize-payment', 'cardConnectController@cardsecureTokenizePayment');

	
