<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function() {

	Route::post('/deleteusercard', 'UsersController@deleteusercard');
//	Route::post('/editName','GroupsController@editGroupName');
	Route::post('/orderitem','ordersController@orderItem');
	Route::post('/addCart','cartController@addCart');
	Route::post('/clearCart','cartController@clearCart');
	Route::post('/deleteitem','cartController@deleteItem');
	Route::post('/completeorder','cartController@completeOrder');
	Route::post('/checkmobile','cartController@registeredNumber');
	Route::post('/rncheck', 'cartController@rnCheck');
	Route::post('/deletestripecard','stripeController@deletecard');
	Route::post('/deletecardconnectcard','cardConnectController@deletecard');
	Route::post('/resendeprint','cartController@reSendPDF');
	Route::post('/reprint','reportsController@rePrint');
	Route::post('/deliverySession','cartController@deliverySession');


	Route::post('/checkuseremail','adminSystemUserController@checkuseremail');
	Route::post('/checkdid','adminSystemUserController@checkDID');
	Route::post('/checklocationemail','emailAdminController@checkLocationEmail');
	
	Route::post('/admin/customer/detail','adminController@customerDetail');
	Route::post('/admin/checkhttphost','adminController@checkHttpHost');
	Route::post('/admin/stripedataimage','adminController@stripeDataImage');
	Route::post('/admin/orderdetail','adminController@orderDetail');
	Route::post('/admin/sendcontacttext','adminController@sendContactText');
	Route::post('/admin/sendorderready','adminController@sendOrderReady');
	Route::post('/admin/sendorderinprocess','adminController@sendOrderInProcess');



	Route::post('/getGroupName', 'groupsController@getGroupName');
	Route::post('/addgroupmember', 'groupsController@addGroupMember');
	Route::post('/deletegroupmember', 'groupsController@deleteGroupMember');

	Route::post('/admin/2/refundorder','stripeController@refundCharge');
	Route::post('/admin/3/refundorder','cardConnectController@refundCharge');
	Route::post('/admin/4/refundorder','authNetController@refundTransaction');

	Route::post('/admin/menuImage','menuImageController@uploadImage');
	Route::post('/admin/deleteMenuImage','menuImageController@deleteImage');
	
	Route::post('/admin/categoryImage','menuImageController@newCategoryImage');
	Route::post('/admin/deleteCategoryImage','menuImageController@deleteCategoryImage');
	
	Route::post('/getmessagedetail','reportsController@getMessageDetail');
	Route::post('/getemailmessagedetail','reportsController@getEmailMessageDetail');

	Route::post('/getbouncerawmessage','reportsController@getBounceRawMessage');
	Route::post('/getbouncecontactdetail','reportsController@getBounceContactDetail');
	Route::post('/bouncecontactrestore','reportsController@bounceContactRestore');
	Route::post('/bouncecontactdelete','reportsController@bounceContactDelete');


	// Card Connect Routes
	Route::group(['prefix' => 'cardconnect'], function () {
		
		Route::post('/authcapture', 'cardConnectController@authCapture');
		Route::post('/checkcartitems', 'cardConnectController@checkCartItems');


	});	

	Route::post('/order/status/confirm','orderAdminController@confirmOrder');

});
