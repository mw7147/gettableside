<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'cardConnect' => [
        'tokenizerLive' => 'https://boltgw.cardconnect.com/itoke/ajax-tokenizer.html',
        'tokenizerTest' => 'https://boltgw-uat.cardconnect.com/itoke/ajax-tokenizer.html',
        'testURL' => 'https://boltgw-uat.cardconnect.com/cardconnect/rest/',
        'testMID' => '820000000240',
        'testACHMID' => '542041',
        'liveURL' => 'https://boltgw.cardconnect.com/cardconnect/rest/',
        'liveMID' => '496357086883',
        'liveACHMID' => '',
        'testUsername' => 'testing',
        'testPassword' => 'testing123',
        'liveUsername' => 'hospital',
        'livePassword' => 'VLJW!k7Pw@HzyW#6vrJT',
        'testTokenizeURL' => 'https://boltgw-uat.cardconnect.com/cardsecure/api/v1/ccn/tokenize',
        'liveTokenizeURL' => 'https://boltgw.cardconnect.com/cardsecure/api/v1/ccn/tokenize',
    ],

    'cardConnectURL' => [
        'credentials' => ['type' =>'GET', 'endpoint' => ''],
        'inquireMerchant' => ['type' =>'GET', 'endpoint' => 'inquireMerchant'],
        'auth' => ['type' =>'PUT', 'endpoint' => 'auth'],
        'authCapture' => ['type' =>'PUT', 'endpoint' => 'auth'],
        'capture' => ['type' =>'PUT', 'endpoint' => 'capture'],
        'void' => ['type' =>'PUT', 'endpoint' => 'void'],
        'refund' => ['type' =>'PUT', 'endpoint' => 'refund'],
        'inquire' => ['type' =>'GET', 'endpoint' => 'inquire'],
        'inquireByOrderid' => 'inquireByOrderid',
        'voidByOrderId' => 'voidByOrderId/',
        'profileGet' => ['type' =>'GET', 'endpoint' => 'profile'],
        'profileDelete' => ['type' =>'DELETE', 'endpoint' => 'profile'],
        'profileUpdate' => ['type' =>'PUT', 'endpoint' => 'profile'],
        'bin' => ['type' =>'GET', 'endpoint' => 'bin']
    ],

    'foodRunner' => [
        'asBearer' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2IiwianRpIjoiODc2MWU4M2ZmY2I0NDY4NjJjY2E5MGUyMDQ5N2EwZTZhYjYyOWUyOGEwY2JiNjkyNzg2Y2M3N2U5MDk3ZjEwZjc4MzRiYjczOWRiZDViOWMiLCJpYXQiOjE1ODk0MTE4NTksIm5iZiI6MTU4OTQxMTg1OSwiZXhwIjoxNjIwOTQ3ODU5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rjk_W0LbCnkamhFUAuC_xJFomNvGqGVY-5h4Uq47Ell_CzkLzMRf6Hiv_6OsygCObXVusJQsQQDL3ikS4H8vH23kuId2Qi08ZcD5c8UPNXrenHjVjf8snbrN530n1zDzsX6O0A8yCmlvBzkRw6vdKXVLCXAK_sYjU0hgtjmLLvr4gjlvnSbqgfj7QzKw4nkd5EWbZiqs1SIxWx6ZsTq_wNNxn9jdNZd_-gKPsjCvk7p0FcfkMfnu8b2BMCk8jawNVlBRJ5EZUgdgxooseWOgghBMNL9HQUDnsVjMGmWZll4goDPsRWHTZ8_m5Cop7iuF0xqsmu_9I6AA-Dk_ggtpTZOEZXpczsoNyipAvB-rrHDqhSB-W-jtgMGkDsCCn1nEbDeKdhsKWKy2fUXJnz9wQfHGWXpZIMXTCOaPRIHbAwVCOUA3RLCbKiwxYVD7fV1D4gucaYtNHaIjPFQ84sg0zm8Y-CEXtpXFqa4OUC7kG61O-SOJtZxC5DxGGpkh6a6TbC2mmvPqvI0C2_dI9g0DPwpitOfMIzB1eAfc-jnTEsckP-m-FsJkr6Nh2AMDkzSwREUX2BIXlzxDF0Sx9JkGmNzmFvTtDGDTEhYqQ3FO6eK4oajdq52867or8qjv8C5gztN_pTpBxyzmsDY082TBUTNQaoMyJbBIVt_ittc50WM',
        'Bearer' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2IiwianRpIjoiYTVhZjEzYjRkNjdjMjQ4YzhjOWU3YjZkYTRiZWYwYmEzZjM4NDIzNmZmM2UxZTFmMjJhMmI3NDA4MWQ4ZGQzOGQzNjY1YWRhNTZjMDhiNTMiLCJpYXQiOjE1OTE5MDU3MTgsIm5iZiI6MTU5MTkwNTcxOCwiZXhwIjoxNjIzNDQxNzE4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.n10ZOqrW1Pto07aLZPqxOikOSyOstqvd33Y8jF8I6HalElieI8Tw7jt52hsPMt_5SthqXhR1rUAnwqI1eLlAxddaZNSkVqBVLRHLRT4lbbZJHrIr7PD-40CYNV_pakJy_8tniDrpM8ZaUEVbV7FeJoclYA8jtvLvkPhFRAsbnPp2uOAb5rEuXeYH-QbWTP7yg1-nfyC54xlyCTN0vKzPKx7b7rd-zyQlvmJIWRUHghZDNT6U42L1m88NCO1H_NIzDaE4Dmoad3LvmgSHM2_JptEYZJftiLiW4yL1muWHFJu9f5Yxu9u4o30iLi7M-WDDup6BQMH15bmKpx1w2vc-8_CHJgGX21xQj5aQ8WLlrov5kgY7f9ghwDqa0yUlvTTlRCozZ1h5QCa9g_IOdAhvUR-_Ra4bZi5kF83KaNaimOPU_wD043nnO18WJ6u4mw4ApPRhVhdEhELKIOV2iZVh1XF1nFsjvBHcpLhuCPQfGjFmq5Ucif7f8JGK1k3mJwCzdFQl1QAx2cCGHO-bG3v4F5yBiyP8A0etShEuooX0AsOqE9eJT8X0_2TjvFr4KIPvl2m4Bb2qu5V9_2rDNctivTj8m_LWvfcP-AoT1SxHBHn3ZAoKnljl9QycsW60WaTB8fFRCUicyAni7zmLQ7cWVGjs8UzOoSl3O77TLmRxRJk',
        'orderTestURL' => 'https://foodrunner.prowealthadvisor.com/api/foodrunner-orders',
        'orderLiveURL' => 'https://foodrunner.gettableside.com/api/foodrunner-orders',
    ],

    'github' => [
        'client_id'     => env('GITHUB_CLIENT_ID'),
        'client_secret' => env('GITHUB_CLIENT_SECRET'),
        'redirect'      => env('GITHUB_REDIRECT_URL'),
    ],
    
    'facebook' => [
        'client_id'     => env('FB_CLIENT_ID'),
        'client_secret' => env('FB_CLIENT_SECRET'),
        'redirect'      => env('FB_REDIRECT_URL'),
    ],

    'google' => [
        'client_id'     => env('GL_CLIENT_ID'),
        'client_secret' => env('GL_CLIENT_SECRET'),
        'redirect'      => env('GL_REDIRECT_URL'),
    ],

];
